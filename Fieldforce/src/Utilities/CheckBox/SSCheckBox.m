//
//  SCheckBox.m
//  CustomSegmentedControl
//
//  Created by SANDIP_ RANDEM on 16/03/11.
//  Copyright 2011 RANDEM IT. All rights reserved.
//

#import "SSCheckBox.h"


@implementation SSCheckBox

static const NSUInteger _normalState = 0;
static const NSUInteger _selectedState = 1;

@synthesize checked;

- (id)initWithFrame:(CGRect)frame 
{
    self = [super initWithFrame:frame];
    if (self) 
	 {
		self.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
		self.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
		self.backgroundColor=[UIColor clearColor];
		self.opaque = NO;
        self.clipsToBounds = YES;
		
		_backgroundImageView=[[UIImageView alloc]init];
		_backgroundImageView.backgroundColor=[UIColor clearColor];
		_backgroundImageView.contentMode=UIViewContentModeScaleAspectFit;
		[self addSubview:_backgroundImageView];
     }
	
    return self;
}

- (void)dealloc 
{
    
	[_backgroundImageView release];
	[_backgroundImages[_normalState] release];
	[_backgroundImages[_selectedState] release];
    
    [super dealloc];
}

-(void)_updateBackground
{
	if(checked)
	 {
		_backgroundImageView.image=_backgroundImages[_selectedState];
	 }
	else
	 {
		_backgroundImageView.image=_backgroundImages[_normalState];						   
	 }
}


-(void)setChecked:(BOOL)_checked
{
	if(checked!=_checked)
	 {
		[self willChangeValueForKey:@"isChecked"];
	    checked=_checked;
		[self didChangeValueForKey:@"isChecked"];
		
		[self _updateBackground];
		//[self sendActionsForControlEvents:UIControlEventValueChanged];
	 }
	
}


-(void)setBackgroundImageForNormalState:(UIImage *)normalImage selectedSate:(UIImage *)selectedImage
{
	[_backgroundImages[_normalState] release];
    _backgroundImages[_normalState] = [normalImage retain];
	
	[_backgroundImages[_selectedState] release];
    _backgroundImages[_selectedState] = [selectedImage retain];
	
	[self _updateBackground];
}


- (void)layoutSubviews 
{
    [super layoutSubviews];
	
	CGRect _bounds=self.bounds;
	_bounds.origin.x=_bounds.origin.y=0.1;
	_bounds.size.width-=0.1;
	_bounds.size.height-=0.1;
	
	_backgroundImageView.frame=_bounds;
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event 
{
    BOOL result = [super beginTrackingWithTouch:touch withEvent:event];
	
	//self.alpha-=0.2;
	CGPoint _point=[touch locationInView:self];
	
	if(CGRectContainsPoint(_backgroundImageView.frame, _point))
	 {
         [self sendActionsForControlEvents:UIControlEventValueChanged];
         
		//self.checked=!checked;
	 }
	
	return result;
}

/*
- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	//self.alpha=1;
	CGPoint _point=[touch locationInView:self];
	
	if(CGRectContainsPoint(_backgroundImageView.frame, _point))
	 {
		self.checked=!checked;
	 }
	
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
}
*/



@end
