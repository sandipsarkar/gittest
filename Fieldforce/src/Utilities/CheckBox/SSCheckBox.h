//
//  SCheckBox.h
//  CustomSegmentedControl
//
//  Created by SANDIP_ RANDEM on 16/03/11.
//  Copyright 2011 RANDEM IT. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SSCheckBox : UIControl 
{
	BOOL checked;

	@private
	UIImage *_backgroundImages[2];
	UIImageView *_backgroundImageView;
}

@property(nonatomic,assign,getter=isChecked) BOOL checked;

-(void)setBackgroundImageForNormalState:(UIImage *)normalImage selectedSate:(UIImage *)selectedImage;

@end
