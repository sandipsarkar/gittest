//
//  ParallelStackViewController.m
//  ParallelStackViewController
//  Created by RANDEM MAC on 07/05/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import "ParallelStackViewController.h"
#import <objc/runtime.h>

@interface ParallelStackViewController()
{
    CGFloat _controllerWidth;
    CGFloat _controllerHeight;
    
    UIViewController *_masterController;
    
  
}
@end

@implementation ParallelStackViewController
@synthesize masterController;
@synthesize detailController;
@synthesize masterControllerWidth;


-(void)dealloc
{
    [masterController release];
    [detailController release];
    
    _masterController=nil;
    masterController=nil;
    detailController=nil;
    
    [super dealloc];
}

-(id)initWithMasterController:(UIViewController *)aMasterController
{
    if(self=[super init])
    {
        //self.masterController=aMasterController;
        
        _masterController = aMasterController;
    }
    
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)resize
{
    CGFloat _boundWidth=CGRectGetWidth(self.view.bounds);
    
    _controllerWidth  = masterControllerWidth>50 ? masterControllerWidth : _boundWidth/2.0;
    _controllerHeight = CGRectGetHeight(self.view.bounds);
    
    masterController.view.frame = CGRectMake(0, 0, _controllerWidth, _controllerHeight);
    detailController.view.frame = CGRectMake(_controllerWidth, 0,_boundWidth-_controllerWidth, _controllerHeight);
    
    
    masterController.view.layer.shadowColor = [UIColor grayColor].CGColor;
    masterController.view.layer.shadowOffset = CGSizeMake(5, 5);
    masterController.view.layer.shadowRadius = 8;
    masterController.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:masterController.view.bounds].CGPath;
    
}

-(void)setMasterControllerWidth:(CGFloat)_masterControllerWidth
{
    if(_masterControllerWidth!=masterControllerWidth)
    {
        masterControllerWidth=_masterControllerWidth;
        [self resize];
    }
}

#pragma mark - View lifecycle
-(void)setMasterController:(UIViewController *)aMasterController
{
    if(aMasterController==masterController) return;
    
    if(masterController)
    {
        [masterController willMoveToParentViewController:nil];
        [masterController removeFromParentViewController];
    //[masterController viewWillDisappear:NO];
     [masterController.view removeFromSuperview];
    //[masterController viewDidDisappear:NO];
    
     [masterController release];
     masterController = nil;
    }
    
    masterController = [aMasterController retain];
    masterController.parallelStackViewController=self;
    
    masterController.view.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    
    //
    //[masterController.view addLeftBoderWithColor:[UIColor colorWithWhite:0.2 alpha:0.8] borderWidth:1.0];
    
     
    
   // [masterController viewWillAppear:NO];
    [self resize];
    [self addChildViewController:masterController];
    [self.view addSubview:masterController.view];
   // [masterController viewDidAppear:NO];
    [masterController didMoveToParentViewController:self];
    
   // [masterController.view addLeftBoderWithColor:[UIColor colorWithWhite:0.2 alpha:0.4] borderWidth:1.0];
    [masterController addDefaultBorder];
    
    //[masterController.view addRightBoderWithColor:[UIColor colorWithWhite:0.2 alpha:0.4] borderWidth:1.0];
}

-(void)setMasterController:(UIViewController *)aMasterController animated:(BOOL)animated
{
    
    [self setMasterController:aMasterController];
    
    if(animated)
    {
    [UIView animateWithDuration:0.3 animations:^{
        
        masterController.view.alpha=0.4;
        masterController.view.alpha=1;
        
    } completion:^(BOOL finished) {
        
    }];
    }
}


-(void)setDetailController:(UIViewController *)aDetailController
{
    if(aDetailController==detailController) return;
    
    [detailController willMoveToParentViewController:nil];
    [detailController removeFromParentViewController];
    
    //[detailController viewWillDisappear:NO];
    [detailController.view removeFromSuperview];
   // [detailController viewDidDisappear:NO];
    
    [detailController release];
    detailController = [aDetailController retain];
    detailController.parallelStackViewController=self;
    
   // [detailController viewWillAppear:NO];
    //detailController.view.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    
     //[detailController addDefaultBorder];
    detailController.view.autoresizingMask=UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin;
    detailController.view.frame=CGRectMake(0, 0, CGRectGetWidth(self.view.bounds)-CGRectGetWidth(masterController.view.bounds), CGRectGetHeight(masterController.view.bounds));
    [self.view insertSubview:detailController.view belowSubview:masterController.view];
    //[detailController viewDidAppear:NO];
    
    if(aDetailController)
    {
        [self addChildViewController:aDetailController];
        [aDetailController didMoveToParentViewController:self];
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif
    
    self.view.backgroundColor=[UIColor clearColor];
    
    //[self addDefaultBorder];
    
   // [self resize];
    
    if(_masterController)
    self.masterController = _masterController;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
  //  [self.masterController viewWillAppear:animated];
  //  [self.detailController viewWillAppear:animated];
}


-(void)showDetailController:(BOOL)animated
{
     detailController.view.hidden=NO;
    
   // _controllerHeight = CGRectGetHeight(self.view.bounds);
   // detailController.view.frame = CGRectMake(_controllerWidth, 0, _controllerWidth, _controllerHeight);
    
    [self resize];
    
    
    
    if(animated)
    {
        [self.view.layer removeAllAnimations];
        [UIView animateWithDuration:0.3 animations:^{ //0.4
            
            CGRect _detailFrame=detailController.view.frame;
            _detailFrame.origin.x-=_controllerWidth;
            //_detailFrame.origin.x=-_controllerWidth;
            detailController.view.frame=_detailFrame;
            
            _detailFrame.origin.x=CGRectGetMaxX(masterController.view.bounds);
            detailController.view.frame=_detailFrame;
            
        }
        completion:^(BOOL finished)
        {
            
            
        }];
        
        
        /*
        UIInterpolatingMotionEffect *effect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
        [effect setMinimumRelativeValue:@(-100.0)];
        [effect setMaximumRelativeValue:@(100.0)];
        [detailController.view addMotionEffect:effect];
        [effect release];
        */
        
    }
    else
    {
        CGRect _detailFrame=detailController.view.frame;
        _detailFrame.origin.x=CGRectGetMaxX(masterController.view.bounds);
        detailController.view.frame=_detailFrame;
    }
}

-(void)showDetailController:(UIViewController *)aDetailController animated:(BOOL)animated
{
   // if(self.detailController != aDetailController)
    {
       self.detailController = aDetailController;
      [self showDetailController:animated];
    }
    
}

-(void)hideDetailController:(UIViewController *)aDetailController
{
    //detailController.view.hidden=YES;
    
    if([self isDetailControllerVisible])
    [self hideDetailController:aDetailController animated:NO];
}

-(void)hideDetailController:(UIViewController *)aDetailController animated:(BOOL)animated
{
    if(animated)
    {
        
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect _detailFrame=detailController.view.frame;
        _detailFrame.origin.x-=_controllerWidth;
        //_detailFrame.origin.x=-_controllerWidth;
        detailController.view.frame=_detailFrame;
    } 
     
    completion:^(BOOL finished)
    {
       
        if(finished)
         detailController.view.hidden=YES;
    }];
    }
    
    else 
    {
        detailController.view.hidden=YES;
    }
    
    
}

-(BOOL)isDetailControllerVisible
{
    return !detailController.view.hidden;
}


-(void)didChangeInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    switch (orientation)
    {
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            _controllerWidth  = (CGRectGetWidth(self.view.frame)-4)/2.0;
            _controllerHeight = CGRectGetHeight(self.view.frame);
        }
        break;
            
        default:
        {
             _controllerWidth = (CGRectGetWidth(self.view.frame)-4)/2.0;
             _controllerHeight = CGRectGetHeight(self.view.frame);
        }
        break;
    }
    
    masterController.view.frame = CGRectMake(0, 0, _controllerWidth, _controllerHeight);
    detailController.view.frame = CGRectMake(_controllerWidth, 0, _controllerWidth, _controllerHeight);
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //[self didChangeInterfaceOrientation:interfaceOrientation];
     [self resize];
   return  UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end


@implementation UIViewController(ParallelStackViewController)
@dynamic parallelStackViewController;

static char parallelStackVCKey;

-(void)setParallelStackViewController:(ParallelStackViewController *)aParallelStackViewController
{
    if(aParallelStackViewController!=self.parallelStackViewController)
    {
    objc_setAssociatedObject (self,&parallelStackVCKey,aParallelStackViewController,
OBJC_ASSOCIATION_ASSIGN);
    }
}

-(ParallelStackViewController *)parallelStackViewController
{
    ParallelStackViewController *vc = (ParallelStackViewController *)objc_getAssociatedObject(self, &parallelStackVCKey);
    
    return vc;
}

@end 


