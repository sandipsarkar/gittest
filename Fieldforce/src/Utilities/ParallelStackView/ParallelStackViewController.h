//
//  ParallelStackViewController.h
//  ParallelStackViewController
//
//  Created by RANDEM MAC on 07/05/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParallelStackViewController : UIViewController

-(id)initWithMasterController:(UIViewController *)aMasterController;
-(void)showDetailController:(BOOL)animated;
-(void)showDetailController:(UIViewController *)aDetailController animated:(BOOL)animated;
-(void)setMasterController:(UIViewController *)aMasterController animated:(BOOL)animated;

@property (nonatomic,retain)  UIViewController *masterController;
@property (nonatomic,retain) UIViewController *detailController;
@property (nonatomic,assign)  CGFloat masterControllerWidth;

-(void)hideDetailController:(UIViewController *)aDetailController;
-(void)hideDetailController:(UIViewController *)aDetailController animated:(BOOL)animated;

-(BOOL)isDetailControllerVisible;

@end

@interface UIViewController(ParallelStackViewController)

@property(nonatomic,assign) ParallelStackViewController *parallelStackViewController;



@end
