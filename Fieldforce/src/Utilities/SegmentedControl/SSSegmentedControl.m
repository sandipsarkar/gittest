//
//  SSegmentedControl.m
//  SSegmentedControl

//////////////////////////////////////////////////////////////////** SSegment **//////////////////////////////////////////////////


#pragma mark SSegmentedControl
#import <QuartzCore/QuartzCore.h>
#import "SSSegmentedControl.h"


@interface SSSegmentedControl ()

- (void)_updateSegments;

@end

static const NSUInteger _NormalState = 0;
static const NSUInteger _SelectedState = 1;

@implementation SSSegmentedControl
@synthesize selectedSegmentIndex;
@synthesize segments;
@dynamic separator;
@synthesize separateSelectedItem;
@synthesize segmentedControlStyle=_segmentedControlStyle;
@synthesize separatorVisible;

- (void)dealloc
{
    [segments release];
    [_backgrounds[_NormalState] release];
    [_backgrounds[_SelectedState] release];
    [super dealloc];
}

+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)key {
    BOOL automatic = NO;
    
    if (![key isEqualToString:@"selectedSegmentIndex"]) {
        automatic = [super automaticallyNotifiesObserversForKey:key];
    }
    return automatic;
}

/*
-(void)setBackGround:(id)backGround
{
    if([backGround isKindOfClass:[UIColor class]])
    {
        self.backgroundColor=backGround;
    }
    else if([backGround isKindOfClass:[UIImage class]])
    {
        UIImageView *imgView=(UIImageView *)[self viewWithTag:NSIntegerMax];
        if(imgView==nil)
        {
        imgView=[[UIImageView alloc] initWithFrame:self.bounds];
        imgView.tag=NSIntegerMax;
        [self insertSubview:imgView atIndex:0];
        [imgView release];
        }
        
        imgView.image=backGround;
    }
}
*/

- (void)setSelectedSegmentIndex:(NSInteger)newSelectedSegmentIndex {
    if (selectedSegmentIndex != newSelectedSegmentIndex) 
	 {
		//NSLog(@"Prev:%d Now:%d ",selectedSegmentIndex,newSelectedSegmentIndex);
        [self willChangeValueForKey:@"selectedSegmentIndex"];
        selectedSegmentIndex = newSelectedSegmentIndex;
        [self _updateSegments];
        [_separators setNeedsDisplay];
        [self didChangeValueForKey:@"selectedSegmentIndex"];
        [self sendActionsForControlEvents:UIControlEventValueChanged];
	}
}

-(void)setSegmentedControlStyle:(SSegmentedControlStyle)aSegmentedControlStyle
{
	if(_segmentedControlStyle!=aSegmentedControlStyle)
	 {
		_segmentedControlStyle=aSegmentedControlStyle;
		
		if(_segmentedControlStyle==SSegmentedControlStyleRoundedCorner)
			self.layer.cornerRadius = 10.0f;
		else
			self.layer.cornerRadius = 0.0f;
	 }
}



- (void)setSeparator:(id)newSeparator 
{
    _separators.separator = newSeparator;
}


- (id)separator 
{
    return _separators.separator;
}



- (id)initWithItems:(NSArray *)items setupSegment:(SSegmentSetupBlock)block
{
    NSParameterAssert([items count] > 1);
    selectedSegmentIndex=-1;
    
	
    if ((self = [super initWithFrame:CGRectZero])) {
        self.backgroundColor = [UIColor clearColor];
        self.opaque = NO;
        self.clipsToBounds = YES;
		_segmentedControlStyle=SSegmentedControlStyleDefault;
        //self.layer.cornerRadius = 10.0f;
        segments = [[NSMutableArray alloc] initWithCapacity:[items count]];
        
        NSUInteger itemIndex=0;
        for (id item in items) 
        {
            SSegment *segment = nil;
            if ([item isKindOfClass:[SSegment class]]) 
            {
                segment=[item retain];
                // __builtin_unreachable();
            }
            else if ([item isKindOfClass:[NSString class]]) 
            {
                segment=[[SSegment alloc] initWithTitle:item image:nil];
                
            }
            else if ([item isKindOfClass:[UIImage class]]) 
            {
                segment=[[SSegment alloc] initWithTitle:nil image:item];
            }
            else if ([item isKindOfClass:[UIView class]]) 
            {
                segment = [[SSegment alloc] initWithFrame:CGRectZero];
                segment.contentView = item;
            }
            
            if(block) block(segment,itemIndex);
            
            [segments addObject:segment];
            [self addSubview:segment];
            [segment release];
            
            itemIndex++;
        }
        
        _separators = [[SSeparators alloc] initWithSegmentedControl:self];
        [self addSubview:_separators];
        [_separators release];
        
        self.separatorVisible=NO;
		
    }
    return self;
}

- (id)initWithItems:(NSArray *)items 
{
	selectedSegmentIndex=-1;
    if ((self = [self initWithItems:items setupSegment:nil]))
    {
        		
    }
    return self;
}




- (void)layoutSubviews 
{
    [super layoutSubviews];
		
    CGSize boundsSize = self.bounds.size;
    NSArray *customWidths = [[segments filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"width != 0.0"]] valueForKey:@"width"];
	
    CGFloat maxSegmentWidth = boundsSize.width;
    for (NSNumber *width in customWidths) {
        maxSegmentWidth -= [width floatValue];
    }
    maxSegmentWidth /= [segments count] - [customWidths count];
    maxSegmentWidth = ceil(maxSegmentWidth);
    
    // Calculate segments frames
    CGPoint origin = CGPointZero;
    NSUInteger index = 0;
    for (SSegment *segment in segments) 
    {
        CGRect frame = segment.frame;
        if (segment.width == 0.0f) {
            frame.size.width = maxSegmentWidth;
        }
        else {
            frame.size.width = segment.width;
        }
        frame.size.height = boundsSize.height;
        frame.origin = origin;
        origin.x += frame.size.width;
        segment.frame = frame;
        if (index == selectedSegmentIndex) {
			[segment setSegmentBackground:_backgrounds[_SelectedState]];
        }
        else {
			[segment setSegmentBackground:_backgrounds[_NormalState]];
        }
        ++index;
    }
    [[segments objectAtIndex:0] setSegmentPosition:SSegmentPositionLeft];
    [[segments lastObject] setSegmentPosition:SSegmentPositionRight];
	
	//for first time send the action
	if(!self.superview)
	[self sendActionsForControlEvents:UIControlEventValueChanged];
}

#pragma mark Get the touch
- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event 
{
    BOOL result = [super beginTrackingWithTouch:touch withEvent:event];
	int segCount=[segments count];
    if ([event type] == UIEventTypeTouches && segCount) 
	 {
        CGPoint _point = [touch locationInView:self];
        NSUInteger _index = 0;
		
		/// Checks whether the last selected segment is again selected
		if(selectedSegmentIndex<segCount && selectedSegmentIndex>=0)
		 {
		    if(CGRectContainsPoint([[segments objectAtIndex:self.selectedSegmentIndex] frame], _point)) 
			return result;
		 }
		//////////////////////////////////////////////////////////////
	   
        for (SSegment *_segment in segments) 
		 {
            if (CGRectContainsPoint(_segment.frame, _point)) 
			 {
                self.selectedSegmentIndex = _index;
                break;
             }
            ++_index;
         }
         return YES;
	 }
    else 
	 {
        return result;
     }
	
}
#pragma mark -

- (void)setWidth:(CGFloat)width forSegmentAtIndex:(NSUInteger)segment {
    [[segments objectAtIndex:segment] setWidth:width];
}


- (CGFloat)widthForSegmentAtIndex:(NSUInteger)segment {
    return [[segments objectAtIndex:segment] width];
}


- (void)setTitle:(NSString *)title forSegmentAtIndex:(NSUInteger)segment 
{
    SSegment *_segment=[segments objectAtIndex:segment];
    
    id view =_segment.textLabel;
   if([view isKindOfClass:[UILabel class]]) 
    {
      [view setText:title];
    }
    else
    {
     for(id subvw in [_segment subviews])
     {
		if([subvw isKindOfClass:[UILabel class]])
        {
			[subvw setText:title];
			break;
        }
     } 
    }
}


- (NSString *)titleForSegmentAtIndex:(NSUInteger)segment 
{
    SSegment *_segment=[segments objectAtIndex:segment];
    id view =_segment.textLabel;
    if([view isKindOfClass:[UILabel class]]) return [view text];
    else
    {
        for(id subvw in [_segment subviews])
        {
            if([subvw isKindOfClass:[UILabel class]])
            {
                view=subvw;
                break;
            }
        } 
    }
    
    return [view text];
}


- (void)setImage:(UIImage *)image forSegmentAtIndex:(NSUInteger)segment 
{
    SSegment *_segment=[segments objectAtIndex:segment];
    id view = _segment.imageView;

    if([view isKindOfClass:[UIImageView class]])
    {
        
    [view setImage:image];
        
    }
    else
    {
        for(id subvw in [_segment subviews])
        {
            if([subvw isKindOfClass:[UIImageView class]])
            {
                [subvw setImage:image];
                break;
            }
        } 

    }
}


- (UIImage *)imageForSegmentAtIndex:(NSUInteger)segment {
    SSegment *_segment=[segments objectAtIndex:segment];
    id view =_segment.imageView;
    
    if([view isKindOfClass:[UIImageView class]]) return [view image];
    else
    {
        for(id subvw in [_segment subviews])
        {
            if([subvw isKindOfClass:[UIImageView class]])
            {
                view=subvw;
                break;
            }
        } 
    }

    return [view image];
}


- (void)setTitleFont:(UIFont *)titleFont forSegmentAtIndex:(NSUInteger)segment 
{
	if(!titleFont) return;
	
   SSegment *_segment =[segments objectAtIndex:segment];
	for(id subvw in [_segment subviews])
	 {
		if([subvw isKindOfClass:[UILabel class]])
		 {
			[subvw setFont:titleFont];
			break;
		 }
	 }
}

- (void)setBackgroundForNormalState:(id)normalBackground forSelectedState:(id)selectedBackground 
{
    NSParameterAssert([normalBackground isKindOfClass:[UIColor class]] || [normalBackground isKindOfClass:[UIImage class]]);
    [_backgrounds[_NormalState] release];
    _backgrounds[_NormalState] = [normalBackground retain];
    
    NSParameterAssert([selectedBackground isKindOfClass:[UIColor class]] || [selectedBackground isKindOfClass:[UIImage class]]);
    [_backgrounds[_SelectedState] release];
    _backgrounds[_SelectedState] = [selectedBackground retain];
    
    [self _updateSegments];
}


- (id)normalStateBackground
{
    return [[_backgrounds[_NormalState] retain] autorelease];
}


- (id)selectedStateBackground {
    return [[_backgrounds[_SelectedState] retain] autorelease];
}

- (void)_updateSegments 
{
    NSUInteger index = 0;
    for (SSegment *segment in segments) 
	 {
        if (index == selectedSegmentIndex)
		 {
			[segment setSegmentBackground:_backgrounds[_SelectedState]];
         }
        else 
		 {
			[segment setSegmentBackground:_backgrounds[_NormalState]];
         }
		
        ++index;
    }
}

- (void)setSeparateSelectedItem:(BOOL)aSeparateSelectedItem 
{
    _separators.separateSelectedItem = aSeparateSelectedItem;
}

- (BOOL)separateSelectedItem 
{
    return _separators.separateSelectedItem;
}

-(void)setSeparatorVisible:(BOOL)_visible
{
	_separators.hidden=!_visible;
}



@end
#pragma mark -

///////////////////////////////////////////////////////// SSeparators //////////////////////////////////////////////////////
#pragma mark SSeparators

@implementation SSeparators
@synthesize segmentedControl;
@synthesize separator;
@synthesize separateSelectedItem;

- (void)setSeparator:(id)newSeparator {
    [newSeparator retain];
    [separator release];
    separator = newSeparator;
    [self setNeedsDisplay];
}

-(void)setSeparateSelectedItem:(BOOL)yesOrNo
{
	if(separateSelectedItem!=yesOrNo)
	 {
		separateSelectedItem=yesOrNo;
		[self setNeedsDisplay];
		
	 }
	
}


- (id)initWithSegmentedControl:(SSSegmentedControl *)aSegmentedControl
{
    if ((self = [super initWithFrame:aSegmentedControl.bounds]))
    {
        self.backgroundColor = [UIColor clearColor];
        self.opaque = NO;
        self.userInteractionEnabled = NO;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        separator = [[UIColor lightGrayColor] retain];
        segmentedControl = aSegmentedControl;
    }
    return self;
}


- (void)dealloc 
{
    [separator release];
    [super dealloc];
}


- (void)drawRect:(CGRect)rect 
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetShouldAntialias(context, FALSE);
    for (int i=1, end=[segmentedControl.segments count]-1; i<end; ++i) {
        if (i == segmentedControl.selectedSegmentIndex && !separateSelectedItem) {
            continue;
        }
        CGRect segmentFrame = [[segmentedControl.segments objectAtIndex:i] frame];
        if ([separator isKindOfClass:[UIColor class]]) {
            [separator setStroke];
            CGContextSetLineWidth(context, 1.0f);
            if (i != segmentedControl.selectedSegmentIndex + 1 || separateSelectedItem) {
                CGContextBeginPath(context);
                CGContextMoveToPoint(context, CGRectGetMinX(segmentFrame), CGRectGetMinY(segmentFrame));
                CGContextAddLineToPoint(context, CGRectGetMinX(segmentFrame), CGRectGetMaxY(segmentFrame));
                CGContextStrokePath(context);
            }
            if (i != segmentedControl.selectedSegmentIndex - 1 || separateSelectedItem) {
                CGContextBeginPath(context);
                CGContextMoveToPoint(context, CGRectGetMaxX(segmentFrame), CGRectGetMinY(segmentFrame));
                CGContextAddLineToPoint(context, CGRectGetMaxX(segmentFrame), CGRectGetMaxY(segmentFrame));
                CGContextStrokePath(context);
            }
        }
        else {
            if (i != segmentedControl.selectedSegmentIndex + 1 || separateSelectedItem) {
                CGRect rect = CGRectMake(CGRectGetMinX(segmentFrame) - [separator size].width/2, CGRectGetMinY(segmentFrame), 
                                         [separator size].width, CGRectGetHeight(segmentFrame));
                [separator drawInRect:rect];
            }
            if (i != segmentedControl.selectedSegmentIndex - 1 || separateSelectedItem) {
                CGRect rect = CGRectMake(CGRectGetMaxX(segmentFrame) - [separator size].width/2, CGRectGetMinY(segmentFrame),
                                         [separator size].width, CGRectGetHeight(segmentFrame));
                [separator drawInRect:rect];
            }
        }
    }
}

@end

//////////////////////////////////////////////////////// SSeparators ////////////////////////////////////////////////////
