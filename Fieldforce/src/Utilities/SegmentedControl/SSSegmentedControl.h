
//  SSegmentedControl.h
//  SSegmentedControl
//

/*//////////////////////// How to use //////////////////

NSArray *arr=[NSArray arrayWithObjects:[UIImage imageNamed:@"car1.png"]
			  ,[UIImage imageNamed:@"car2.png"]
			  ,[UIImage imageNamed:@"car3.png"]
			  ,[UIImage imageNamed:@"car3.png"]
			  ,[UIImage imageNamed:@"car4.png"],nil];

NSArray *arr2=[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",nil];

customSegControl=[[SSSegmentedControl alloc] initWithItems:arr];
customSegControl.frame=CGRectMake(10, 10, 300, 50);
//customSegControl.separateSelectedItem=YES;
customSegControl.separatorVisible=YES;
customSegControl.segmentedControlStyle=SSegmentedControlStyleRoundedCorner;
 
// UIImage , UIColor, UIImageView as background
[customSegControl setBackgroundForNormalState:[UIColor yellowColor] forSelectedState:[UIColor colorWithWhite:1 alpha:0.2]];

[customSegControl addTarget:self action:@selector(segmentedControlValChanged:) forControlEvents:UIControlEventValueChanged];
customSegControl.selectedSegmentIndex=4;
[self.view addSubview:customSegControl];
 
*/////////////////////////How to use //////////////////////////////


#import "SSegment.h"
#pragma mark SSeparators
@class SSSegmentedControl;

@interface SSeparators : UIView 
{
	
}
@property (nonatomic, readonly) SSSegmentedControl *segmentedControl;
/**
 @default [UIColor lightGrayColor]
 @discussion UIColor and UIImage are supported.
 */
@property (nonatomic, retain) id separator;
@property (nonatomic) BOOL separateSelectedItem; // default NO

- (id)initWithSegmentedControl:(SSSegmentedControl *)aSegmentedControl;

@end
#pragma mark -
#pragma mark SSegmentedControl

/*
 @discussion Default cornerRadius is 10.0f
 */

typedef enum
{
	SSegmentedControlStyleDefault=0,
	SSegmentedControlStyleRoundedCorner,
	
}SSegmentedControlStyle;

typedef void(^SSegmentSetupBlock) (SSegment *segment,NSUInteger index);

@interface SSSegmentedControl : UIControl {
    NSMutableArray *segments;
	SSegmentedControlStyle _segmentedControlStyle;
 @private
    id _backgrounds[2];
    SSeparators *_separators;
}

@property (nonatomic) NSInteger selectedSegmentIndex;
@property (nonatomic, readonly) NSArray *segments;
/**
 @default [UIColor lightGrayColor]
 @discussion UIColor and UIImage are supported.
 */
@property (nonatomic, retain) id separator;
@property (nonatomic) BOOL separateSelectedItem; // default NO
@property(nonatomic,getter=isSeparatorVisible) BOOL separatorVisible ; //default YES;

@property(nonatomic,assign)SSegmentedControlStyle segmentedControlStyle;

- (id)initWithItems:(NSArray *)items;
- (id)initWithItems:(NSArray *)items setupSegment:(SSegmentSetupBlock)block;

- (void)setWidth:(CGFloat)width forSegmentAtIndex:(NSUInteger)segment; // 0.0 width indicates that SSegmentedControl should calc width itself. Otherwise width will be used
- (CGFloat)widthForSegmentAtIndex:(NSUInteger)segment;

// UIColor and UIImage are supported.
// If object is class of UIImage then image is resized to fill SSegment background.
// If object is class of UIColor then color is used as backgroundColor of SSegment.
- (void)setBackgroundForNormalState:(id)normalBackground forSelectedState:(id)selectedBackground;
- (id)normalStateBackground;
- (id)selectedStateBackground;

- (void)setTitle:(NSString *)title forSegmentAtIndex:(NSUInteger)segment;
- (NSString *)titleForSegmentAtIndex:(NSUInteger)segment;
- (void)setImage:(UIImage *)image forSegmentAtIndex:(NSUInteger)segment;
- (UIImage *)imageForSegmentAtIndex:(NSUInteger)segment;
- (void)setTitleFont:(UIFont *)titleFont forSegmentAtIndex:(NSUInteger)segment ;

//////////////////////
//-(void)setBackGround:(id)backGround;

@end
#pragma mark -
