//
//  SSegment.h
//  CustomSegmentedControl
//
//  Created by SANDIP_ RANDEM on 06/04/11.
//  Copyright 2011 RANDEM IT. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum _SSegmentPosition 
{
    SSegmentPositionCenter,
    SSegmentPositionLeft,
    SSegmentPositionRight
} SSegmentPosition;

@interface SSegment : UIView {
}

//@property (nonatomic, retain) id contentView;
@property (nonatomic, retain) UIView * contentView;
@property (nonatomic) CGFloat width;
@property (nonatomic, retain) UIImage *backgroundImage;
@property(nonatomic,retain,readonly) UIImageView *imageView;
@property(nonatomic,retain,readonly) UILabel *textLabel;
/**
 @disctussion If backgroundImage is non-nil and position is either SSegmentPositionLeft or SSegmentPositionRight then 
 image is drawn so that to hide left/right cap.
 */
@property (nonatomic) SSegmentPosition segmentPosition;

-(void)setSegmentBackground:(id)background;
-(id)initWithTitle:(NSString *)aTitle image:(UIImage *)aImage;

@end

