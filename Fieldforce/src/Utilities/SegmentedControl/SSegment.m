//
//  SSegment.m
//  CustomSegmentedControl
//
//  Created by SANDIP_ RANDEM on 06/04/11.
//  Copyright 2011 RANDEM IT. All rights reserved.
//

#import "SSegment.h"


@implementation SSegment
@synthesize contentView;
@synthesize width;
@synthesize backgroundImage;
@synthesize segmentPosition;
@synthesize imageView;
@synthesize textLabel;

- (void)setContentView:(id)newContentView
{
    NSParameterAssert([newContentView isKindOfClass:[UIView class]]);
    
   // isDefaultSegment=NO;
    if(newContentView!=contentView)
    {
    [contentView removeFromSuperview];
    [self addSubview:newContentView];
    contentView = newContentView;
    
    if ([newContentView isKindOfClass:[UIImageView class]]) {
        [contentView setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin];
    } else 
    {
        [contentView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    }
    [contentView setBackgroundColor:[UIColor clearColor]];
    [contentView setOpaque:NO];
    }

}


- (void)setBackgroundImage:(UIImage *)newBackgroundImage {
    [newBackgroundImage retain];
    [backgroundImage release];
    backgroundImage = newBackgroundImage;
    [self setNeedsDisplay];
}


-(void)setSegmentBackground:(id)background
{
	if([background isKindOfClass:[UIColor class]]) 
    {
        self.backgroundColor = background;
        self.backgroundImage = nil;
    }
    else 
    {
        self.backgroundColor = [UIColor clearColor];
        self.backgroundImage = background;
    }
}

-(void)setWidth:(CGFloat)newWidth
{
	if(width!=newWidth)
    {
		width=newWidth;
		[self.superview layoutSubviews];
    }
}


-(id)initWithTitle:(NSString *)aTitle image:(UIImage *)aImage
{
    if ((self = [super init])) 
    {
        self.userInteractionEnabled = NO;
        
        contentView=[[UIView alloc] init];
        contentView.opaque=NO;
        contentView.backgroundColor=[UIColor clearColor];
        [self addSubview:contentView];
        
        if(imageView==nil)
        {
        imageView=[[UIImageView alloc] initWithImage:aImage];
        imageView.contentMode=UIViewContentModeScaleAspectFit;
        imageView.backgroundColor=[UIColor clearColor];
        [self addSubview:imageView];
        }
        
        if(textLabel==nil)
        {
        textLabel=[[UILabel alloc] init];
        textLabel.textAlignment=UITextAlignmentCenter;
        textLabel.text=aTitle;
        textLabel.textColor=[UIColor blackColor];
        textLabel.backgroundColor=[UIColor clearColor];
        [self addSubview:textLabel];
        }
    }
    return  self;
}


-(void)_configureDefaultSegment
{
    const float _labelHeight=20;
    
    CGRect imageRect =self.bounds;
    CGRect lebelRect=imageRect;
    
    
    if(!CGRectEqualToRect(contentView.frame, imageRect))
    {
        contentView.frame=imageRect;
    }
    
    
    if(imageView.image!=nil && textLabel.text.length)
    {
        imageRect.size.height -= _labelHeight;
        lebelRect.origin.y = CGRectGetMaxY(imageRect);
        lebelRect.size.height = _labelHeight;
        
        //text and image
    }
    else if(self.imageView.image!=nil && !self.textLabel.text.length)
    {
        lebelRect=CGRectZero;
        //image only
    }
    else if(self.imageView.image==nil && self.textLabel.text.length)
    {
        lebelRect=imageRect;
        imageRect=CGRectZero;
        //text only
    }
    
    imageView.frame = imageRect;
    textLabel.frame = lebelRect;
}


-(void)layoutSubviews
{
    [super layoutSubviews];
    
    //if(isDefaultSegment)
    [self _configureDefaultSegment];
}

- (void)dealloc {
    [backgroundImage release];
    [imageView release];
    [textLabel release];
    [super dealloc];
}


- (void)drawRect:(CGRect)rect 
{
    if (backgroundImage) 
    {
        rect = self.bounds;
        switch (segmentPosition) 
	    {
            case SSegmentPositionLeft:
                rect.origin.x -= backgroundImage.leftCapWidth;
                rect.size.width += backgroundImage.leftCapWidth;
                break;
            case SSegmentPositionRight:
                rect.size.width += backgroundImage.size.width - (backgroundImage.leftCapWidth + 1);
                break;
            default:
                break;
        }
		
        [backgroundImage drawInRect:rect];
    }
}


- (CGSize)sizeThatFits:(CGSize)size {
    size = [contentView sizeThatFits:size];
    if (width != 0.0f) {
        size.width = width;
    }
    return size;
}


@end

///////////////** SSegment **////////////
#pragma mark -


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

