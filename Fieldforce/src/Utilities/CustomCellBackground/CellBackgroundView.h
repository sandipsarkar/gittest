//
//  CellBackgroundView.h
//  Cubed
//
//  Created by Sandip Sarkar on 16/08/13.
//  Copyright (c) 2013 Somsankar Ray. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    CellPositionTop,
    CellPositionMiddle,
    CellPositionBottom,
    CellPositionSingle
} CellPosition;

@interface CellBackgroundView : UIView

@property(assign) CellPosition position;
@property(nonatomic,readwrite) CGFloat cornerRadius;
@property(nonatomic,assign) CGPoint contentInset;
@property(nonatomic,assign) UIEdgeInsets contentEdgeInset;
@property(strong) UIColor *fillColor;
@property(strong) UIColor *borderColor;

@end