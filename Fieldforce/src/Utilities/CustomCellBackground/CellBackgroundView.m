//
//  CellBackgroundView.m
//  Cubed
//
//  Created by Sandip Sarkar on 16/08/13.
//  Copyright (c) 2013 Somsankar Ray. All rights reserved.
//

#import "CellBackgroundView.h"

static const CGFloat kCornerRadius = 10;

@implementation CellBackgroundView

@synthesize position, fillColor, borderColor;

- (void)drawRect:(CGRect)rect
{
    if(_cornerRadius<=0)
    _cornerRadius = kCornerRadius;
    
    CGRect bounds = CGRectInset(self.bounds,
                               self.contentInset.x+ 0.5 / [UIScreen mainScreen].scale,
                               self.contentInset.y+ 0.5 / [UIScreen mainScreen].scale);
    
    if(!UIEdgeInsetsEqualToEdgeInsets(self.contentEdgeInset, UIEdgeInsetsZero))
    bounds = UIEdgeInsetsInsetRect(bounds, self.contentEdgeInset);
  
    
   
    
   // bounds = self.bounds;
    UIBezierPath *path;
    if (position == CellPositionSingle)
    {
        path = [UIBezierPath bezierPathWithRoundedRect:bounds cornerRadius:_cornerRadius];
    }
    else if (position == CellPositionTop)
    {
        bounds.size.height += 1;
        path = [UIBezierPath bezierPathWithRoundedRect:bounds
                                     byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                           cornerRadii:CGSizeMake(_cornerRadius, _cornerRadius)];
        
    }
    else if (position == CellPositionBottom)
    {
        path = [UIBezierPath bezierPathWithRoundedRect:bounds
                                     byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight
                                           cornerRadii:CGSizeMake(_cornerRadius, _cornerRadius)];
    }
    else
    {
        bounds.size.height += 1;
        path = [UIBezierPath bezierPathWithRect:bounds];
    }
    
    [self.fillColor setFill];
    [self.borderColor setStroke];
    [path fill];
    [path stroke];
    
    //------------
    
    /*
    if(position == CellPositionSingle || position == CellPositionBottom)
    {
    self.layer.shadowOpacity = 0.4;
    self.layer.shadowRadius = 4.0;
    self.layer.shadowOffset = CGSizeZero;
    self.layer.shadowPath = path.CGPath;
    }
    */
    
    /*
    /// INNER SHADOW
    
    {
        UIBezierPath *innerPath = path; // [UIBezierPath bezierPathWithRoundedRect:bounds cornerRadius:_cornerRadius];
        
        CAShapeLayer* shadowLayer = [CAShapeLayer layer];
        shadowLayer.frame = CGRectMake(0, 0, CGRectGetWidth(bounds), CGRectGetHeight(bounds));
        [shadowLayer setOpacity:1];
        [shadowLayer setShadowOpacity:1.0f];
        [shadowLayer setShadowColor:[[[UIColor lightGrayColor] colorWithAlphaComponent:0.5] CGColor]];
        [shadowLayer setShadowOffset:CGSizeMake(0.0f, 1.0f)];
        [shadowLayer setShadowRadius:2];
        [shadowLayer setFillRule:kCAFillRuleEvenOdd];
        
        // Create the larger rectangle path.
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathAddRect(path, NULL, CGRectInset(bounds, -2.0f, -2.0f));
        // Add the inner path so it's subtracted from the outer path.
        CGPathAddPath(path, NULL, innerPath.CGPath);
        CGPathCloseSubpath(path);
        
        [shadowLayer setPath:path];
        CGPathRelease(path);
        
        //[self.layer setMasksToBounds:YES];
        [self.layer addSublayer:shadowLayer];

    }
    */
}

-(void)setCornerRadius:(CGFloat)cornerRadius
{
    if(_cornerRadius!=cornerRadius)
    {
        _cornerRadius = cornerRadius;
        [self setNeedsDisplay];
    }
}
@end