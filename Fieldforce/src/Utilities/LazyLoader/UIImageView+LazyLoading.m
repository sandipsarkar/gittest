//
//  UIImageView+LazyLoading.m
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "UIImageView+LazyLoading.h"

#import "ASIHTTPRequest.h"

@implementation UIImageView (LazyLoading)

-(void)loadImageForUrl:(NSString *)imageURL completionHandler:(void(^)(UIImage *image,NSString *urlString,NSError *error))completionCallback
{
    
    [LazyImageLoader shouldSaveToDisk:YES];
    UIImage *_image=[LazyImageLoader loadImageForUrlString:imageURL indexPath:nil callBack:^(BOOL success, UIImage *image,NSString *urlString,NSIndexPath *indexPath,NSError *error)
                    {
                        if([imageURL isEqualToString:urlString])
                        {
                           // if(image)
                               if(completionCallback) completionCallback(image,urlString,error);
                        }
                    }];
    
    if(_image && completionCallback) completionCallback(_image,imageURL,nil);
    
}

-(void)loadImageForUrl:(NSString *)imageURL indexPath:(NSIndexPath *)indexPath delegate:(id<ImageLoaderDelegate>)delegate
{
    [LazyImageLoader loadImageForUrlString:imageURL indexPath:indexPath delegate:delegate];
}




@end
