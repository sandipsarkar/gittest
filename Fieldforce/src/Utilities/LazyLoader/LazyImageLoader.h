//
//  LazyImageLoader.h
//  ARonFBTest
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const  ImageLoaderDidLoadImageNotification;
extern NSString * const  ImageLoaderDidFailImageNotification;


@protocol ImageLoaderDelegate;
@class LazyImageLoader;

//typedef void(^LazyImageLoaderCallback)(BOOL success, LazyImageLoader *loader,NSError *error);
typedef void(^LazyImageLoaderCallback)(BOOL success, UIImage *image,NSString *urlString,NSIndexPath *indexPath,NSError *error);

@interface LazyImageLoader : NSObject


@property (nonatomic , retain)  NSString *urlString;
@property (nonatomic , retain)  NSIndexPath *indexPath;
@property (nonatomic , retain)  UIImage *image;
@property (nonatomic , copy)    NSString *identifier;
@property (nonatomic , retain)  NSString *targetPath;
@property (nonatomic , assign) __weak  id<ImageLoaderDelegate>delegate;
@property (nonatomic ,   copy)  LazyImageLoaderCallback loaderCallbacK;

-(UIImage *)imageForURLString:(NSString *)_urlString;

+(UIImage *)loadImageForUrlString:(NSString *)_urlString indexPath:(NSIndexPath *)indexPath delegate:(id<ImageLoaderDelegate>)callback;

+(UIImage *)_loadImageForUrlString:(NSString *)_urlString indexPath:(NSIndexPath *)indexPath delegate:(id<ImageLoaderDelegate>)callbackDelegate callBack:(LazyImageLoaderCallback)callback identifier:(NSString *)identifierString targetPath:(NSString *)targetPath;

+(UIImage *)loadImageForUrlString:(NSString *)_urlString indexPath:(NSIndexPath *)indexPath callBack:(LazyImageLoaderCallback)callback;

+(UIImage *)loadImageForUrlString:(NSString *)_urlString indexPath:(NSIndexPath *)indexPath delegate:(id<ImageLoaderDelegate>)callback targetPath:(NSString *)targetPath;

+(void)reset;
+(void)flushMemoryCache;
+(void)shouldSaveToDisk:(BOOL)disk;
+(void)downloadDirectlyToFile:(BOOL)yesNo;
+(void)cancelAllConnections;
+(void)cancelAllConnectionsForIdentifier:(NSString *)identifierString;

+(UIImage*)resizeImage:(UIImage*)image toSize:(CGSize)newSize;

+(UIImage *)_imageFromDiskForURL:(NSString *)url;

+(void)cancelRequestForURLString:(NSString *)urlString;

@end


@protocol ImageLoaderDelegate <NSObject>

@optional
-(void)imageLoaderDidLoad:(LazyImageLoader *)loader;
-(void)imageLoader:(LazyImageLoader *)loader didFail:(NSError *)error;
-(BOOL)imageLoaderShouldAvoidLoading;
@end

/*
 UIImage *image = [LazyImageLoader loadImageForUrlString:imageUrl indexPath:indexPath callBack:^(BOOL success, LazyImageLoader *loader, NSError *error)
 {
 if(success)
 {
 UITableViewCell *aCell=[self.tableView cellForRowAtIndexPath:loader.indexPath];
 if ((aCell && [aCell isEqual:cell]) && loader.image)
 cell.imageView.image = loader.image;
 }
 }]; 
 //[LazyImageLoader loadImageForUrlString:imageUrl indexPath:indexPath delegate:self];
 if(image)
 cell.imageView.image = image; 
 else
 cell.imageView.image=[UIImage imageNamed:@"blank-fb-user.gif"];
*/
