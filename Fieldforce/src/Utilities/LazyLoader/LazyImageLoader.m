//
//  LazyImageLoader.m
//  ARonFBTest
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import "LazyImageLoader.h"
#import "ASIHTTPRequest.h"

NSString * const ImageLoaderDidLoadImageNotification=@"ImageLoaderDidLoadImageNotificationKey";
NSString * const ImageLoaderDidFailImageNotification=@"ImageLoaderDidFailImageNotificationKey";

#define imageUrlKey(url) [NSString stringWithFormat:@"IMAGE_URL_KEY_%u", [url  hash]]
#define FILE_EXPIRATION_TIME_INTERVAL 1*60*60*24*365

// dispatch_queue_t _diskQueue ;

@interface LazyImageLoader ()
{
   
}


@property(nonatomic,retain) ASIHTTPRequest *imageRequest;

-(void)_downloadImageForURLString:(NSString *)_urlString;
-(void)_imageDownloadDidFinishWithImageData:(NSData *)imageData;
-(void)_imageDownloadDidFailWithError:(NSError *)error;

+(UIImage *)_cachedImageForKey:(NSString *)key;
+(void)_cacheImage:(UIImage *)image forKey:(NSString *)key;

+(UIImage *)_imageFromDiskForURL:(NSString *)url;
+(UIImage *)_imageFromDiskForPath:(NSString *)path;

+(BOOL)isFileExpired:(NSString *)filePath;
-(void)_imageDownloadDidFinish;

+(NSMutableDictionary *)connectionCache;;

-(void)writeData:(NSData*)data toPath:(NSString *)path;
-(void)writeImage:(UIImage*)anImage toPath:(NSString *)path;

+(UIImage *)_loadImageForUrlString:(NSString *)_urlString indexPath:(NSIndexPath *)indexPath delegate:(id<ImageLoaderDelegate>)callbackDelegate callBack:(LazyImageLoaderCallback)callback identifier:(NSString *)identifierString;

@end

@implementation LazyImageLoader
@synthesize     urlString,identifier;
@synthesize     indexPath,image,delegate;
@synthesize     loaderCallbacK;
@synthesize     imageRequest;
@synthesize     targetPath;

static NSCache *imageCache=nil;
static NSMutableDictionary *connectionCache=nil;
BOOL saveToDisk =NO;
BOOL saveDirectToFile = NO;

static NSString * _imageCacheDirectory=nil;;

static inline NSString* SSImageCacheDirectory() 
{
	if(!_imageCacheDirectory)
    {
		_imageCacheDirectory = [[NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"] copy];
	}
    
	return _imageCacheDirectory;
}


inline static NSString* keyForURL(NSString *url) 
{
	return [NSString stringWithFormat:@"SSImageCache-%u",[url hash]];
}


static inline NSString* cachePathForURL(NSString* key) 
{
	NSString *path= [SSImageCacheDirectory() stringByAppendingPathComponent:keyForURL(key)];
    
  //  return [path stringByAppendingPathExtension:key.pathExtension];
    
    return path;
}

static inline NSString* tempDownloadPathForURL(NSString* _urlString)
{
    NSString *fileName = [_urlString lastPathComponent];
    NSString *tempFileName =  [fileName stringByAppendingString:@".download"];
    NSString *tempDirPath  =  [NSTemporaryDirectory() stringByAppendingPathComponent:tempFileName];
    
    return tempDirPath;
}

- (void)writeData:(NSData*)data toPath:(NSString *)path
{
    if(!data || !path) return;
    
    [data retain];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    
	    [data writeToFile:path atomically:YES];
        
         dispatch_async(dispatch_get_main_queue(), ^{
             
             [data release];
         });

      });

}

- (void)writeImage:(UIImage*)anImage toPath:(NSString *)path
{
    [self writeData:[NSKeyedArchiver archivedDataWithRootObject:anImage] toPath:path];
}

-(void)dealloc
{
    delegate=nil;
    
    if(loaderCallbacK)
    [loaderCallbacK release];
    loaderCallbacK = nil;
  
    [image release];
     image = nil;
    [urlString release];
    urlString=nil;
    [indexPath release];
    [targetPath release];
    
    //===========================================
    if(imageRequest.isExecuting)
    {
        [imageRequest clearDelegatesAndCancel];
    }
    //==========================================
    
    imageRequest.delegate = nil;
    [imageRequest release];
    imageRequest = nil;
    [identifier release];
    
    [super dealloc];
}

-(id)init
{
    if(self=[super init])
    {
        if(!imageCache)
        {
           // imageCache = [[NSCache alloc] init];
        
      //  _diskQueue = dispatch_queue_create("com.sandiprandem.fieldforce.disk", DISPATCH_QUEUE_CONCURRENT);
        }

    }
    
    return self;
}

+(NSMutableDictionary *)connectionCache
{
    if(!connectionCache)
        connectionCache = [[NSMutableDictionary alloc] init];
    
    return connectionCache;
}

-(void)setLoaderCallbacK:(LazyImageLoaderCallback)_loaderCallbacK
{
    if(_loaderCallbacK!=loaderCallbacK)
    {
        if(loaderCallbacK)
        [loaderCallbacK release];
        loaderCallbacK=[_loaderCallbacK copy];
    }
}


+(BOOL)_shouldAvoid:(id<ImageLoaderDelegate>)_delegate
{
    BOOL b=NO;
    if([_delegate respondsToSelector:@selector(imageLoaderShouldAvoidLoading)])
    {
       b = [_delegate imageLoaderShouldAvoidLoading];
    }
    
    return b;
}

+(void)shouldSaveToDisk:(BOOL)disk
{
    saveToDisk = disk;
}

+(void)downloadDirectlyToFile:(BOOL)yesNo
{
    saveDirectToFile = yesNo;
}


/*
-(void)registerNotificationObserver:(id<ImageLoaderDelegate>)callback selector:(SEL)selector
{
    [[NSNotificationCenter defaultCenter] addObserver:callback selector:selector name:@"ImageLoadingNotification" object:nil];
    
}
 */

+(UIImage *)loadImageForUrlString:(NSString *)_urlString indexPath:(NSIndexPath *)indexPath delegate:(id<ImageLoaderDelegate>)callback 
{
  return  [self _loadImageForUrlString:_urlString indexPath:indexPath delegate:callback callBack:nil identifier:nil targetPath:nil];
}

+(UIImage *)loadImageForUrlString:(NSString *)_urlString indexPath:(NSIndexPath *)indexPath delegate:(id<ImageLoaderDelegate>)callback identifier:(NSString *)identifierString
{
   return  [self _loadImageForUrlString:_urlString indexPath:indexPath delegate:callback callBack:nil identifier:identifierString targetPath:nil];
}

+(UIImage *)loadImageForUrlString:(NSString *)_urlString indexPath:(NSIndexPath *)indexPath callBack:(LazyImageLoaderCallback)callback
{
    return  [self _loadImageForUrlString:_urlString indexPath:indexPath delegate:nil callBack:callback identifier:nil targetPath:nil];
}

+(UIImage *)loadImageForUrlString:(NSString *)_urlString indexPath:(NSIndexPath *)indexPath delegate:(id<ImageLoaderDelegate>)callback targetPath:(NSString *)targetPath
{
    return  [self _loadImageForUrlString:_urlString indexPath:indexPath delegate:callback callBack:nil identifier:nil targetPath:targetPath];
}

+(UIImage *)_loadImageForUrlString:(NSString *)_urlString indexPath:(NSIndexPath *)indexPath delegate:(id<ImageLoaderDelegate>)callbackDelegate callBack:(LazyImageLoaderCallback)callback identifier:(NSString *)identifierString targetPath:(NSString *)targetPath
{
    NSString *imageDataKey = imageUrlKey(_urlString);
    UIImage *image = [self _cachedImageForKey:imageDataKey];
    
    if(image)
    {
        return image;
    }
    else
    {
        if(saveToDisk)
        {
            //Request image from disk cache
            NSString *diskCachePath = targetPath ? targetPath :  cachePathForURL(_urlString);
            image=[self _imageFromDiskForPath:diskCachePath];
            //image=[self _imageFromDiskForURL:_urlString];
            
            if(image)
            {
                NSLog(@"image loaded from disk");
                // [LazyImageLoader _cacheImage:image forKey:imageDataKey];
                return image;
            }
        }
        
        LazyImageLoader *loader = [[self connectionCache] objectForKey:_urlString];
        
        if(loader && loader.image)
        {
            return loader.image;
        }
        else if(![self _shouldAvoid:callback])
        {
            if(!loader)
            {
                loader=[[LazyImageLoader alloc] init];
                loader.urlString = _urlString;
                
                if(indexPath)
                loader.indexPath=indexPath;
                
                if(callbackDelegate)
                loader.delegate=callbackDelegate;
                
                if(callback)
                loader.loaderCallbacK = callback;
                  
                if(identifierString)
                    loader.identifier = identifierString;
                
                if(targetPath)
                    loader.targetPath = targetPath;
                
                [loader performSelector:@selector(_downloadImageForURLString:) withObject:_urlString];
                
                if(loader.urlString)
                {
                    [[self connectionCache] setObject:loader forKey:loader.urlString];
                    [loader release];
                }
            }
        }
    }
    
    return nil;
}

  
+(void)cancelRequestForURLString:(NSString *)urlString
{
    if(!urlString) return;
    
    LazyImageLoader *loader = [[self connectionCache] objectForKey:urlString];
    if(loader)
    {
        [loader cancel];
    }
    
    [[LazyImageLoader connectionCache] removeObjectForKey:urlString];
    
}
 

+(void)reset
{
    if(connectionCache)
    {
        //[connectionCache removeAllObjects];
        [connectionCache release];
        connectionCache = nil;
    }
}

+(void)flushMemoryCache
{
    if(imageCache)
    {
     [imageCache release];
     imageCache = nil;
    }
}

+(void)cancelAllConnections
{
    NSMutableDictionary *_conCache = [self connectionCache];
    
    for(id key in _conCache)
    {
        LazyImageLoader *loader = [_conCache objectForKey:key];
        [loader cancel];
    }
    
    [self reset];
}

+(void)cancelAllConnectionsForIdentifier:(NSString *)identifierString
{
    NSMutableDictionary *_conCache = [self connectionCache];
    
    for(id key in _conCache)
    {
        LazyImageLoader *loader=[_conCache objectForKey:key];
      
        if([loader.identifier isEqualToString:identifierString])
        {
            [loader cancel];
        }
    }
    
    [self reset];
}

-(void)cancel
{
    delegate = nil;
    
    //if(self.imageRequest.isExecuting)
    [self.imageRequest clearDelegatesAndCancel];
    
    //========== TO BE TESTED LATER ========
    /*
    if(loaderCallbacK)
    {
       NSMutableArray *blocks=[NSMutableArray array];
        [blocks addObject:loaderCallbacK];
        [loaderCallbacK release];
        loaderCallbacK = nil;
        
        [[self class] performSelectorOnMainThread:@selector(releaseBlocks:) withObject:blocks waitUntilDone:[NSThread isMainThread]];
    }
    */
}
    
// Always called on main thread
+ (void)releaseBlocks:(NSArray *)blocks
{
// Blocks will be released when this method exits.
}


+(UIImage *)_cachedImageForKey:(NSString *)key
{
    return nil;
    
    /*
   UIImage *image= [imageCache objectForKey:key];
    
    return image;
  */
}

+(void)_cacheImage:(UIImage *)image forKey:(NSString *)key
{
    /*
    if(image)
    [imageCache setObject:image forKey:key];
    */
}

+(BOOL)isFileExpired:(NSString *)filePath
{
    NSDictionary *attbDict= [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
    NSDate *fileCreationDate=[attbDict valueForKey:NSFileCreationDate];
    NSTimeInterval interval=abs([fileCreationDate timeIntervalSinceNow]);
    
    //NSLog(@"Interval:%lf",interval);
    
    return  (interval>=FILE_EXPIRATION_TIME_INTERVAL);
    
}

+(UIImage *)_imageFromDiskForURL:(NSString *)url 
{
    NSString *_cachePath = cachePathForURL(url);
    
    NSLog(@"Cached_path:%@",_cachePath);
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:_cachePath]) return nil;
    
    if([self isFileExpired:_cachePath])
    {
        if([[NSFileManager defaultManager] removeItemAtPath:_cachePath error:nil])
            return nil;
    }
    else
    {
        /*
        NSData* picData = [[NSData alloc] initWithContentsOfFile:_cachePath options:NSDataReadingMappedIfSafe error:nil];
        UIImage* pic = [UIImage imageWithData:picData];
        [picData release];
        
        return pic;
        */
                        
        return [UIImage imageWithContentsOfFile:_cachePath];
        
       // return [NSKeyedUnarchiver unarchiveObjectWithFile:_cachePath];
    }
    
	return nil;
}

+(UIImage *)_imageFromDiskForPath:(NSString *)path
{
    NSString *_cachePath = path;
    
    NSLog(@"Cached_path:%@",_cachePath);
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:_cachePath]) return nil;
    
    if([self isFileExpired:_cachePath])
    {
        if([[NSFileManager defaultManager] removeItemAtPath:_cachePath error:nil])
            return nil;
    }
    else
    {
        /*
         NSData* picData = [[NSData alloc] initWithContentsOfFile:_cachePath options:NSDataReadingMappedIfSafe error:nil];
         UIImage* pic = [UIImage imageWithData:picData];
         [picData release];
         
         return pic;
         */
        
        return [UIImage imageWithContentsOfFile:_cachePath];
        
        // return [NSKeyedUnarchiver unarchiveObjectWithFile:_cachePath];
    }
    
	return nil;
}



-(UIImage *)imageForURLString:(NSString *)_urlString
{
    if(_urlString==nil) return nil;
    
	NSString *imageDataKey = imageUrlKey(_urlString);
	UIImage *cellImage = [LazyImageLoader _cachedImageForKey:imageDataKey];
    
    if(saveToDisk && !cellImage)
    {
        NSString *diskCachePath = self.targetPath ? self.targetPath :  cachePathForURL(_urlString);
        //============ Request image from disk cache ================
        //cellImage=[LazyImageLoader _imageFromDiskForURL:_urlString];
         cellImage=[LazyImageLoader _imageFromDiskForPath:diskCachePath];
    }
    
    if (cellImage == nil)
    {
		if(_urlString.length)
        { 
			[self _downloadImageForURLString:_urlString];
        }
    }
	
    return cellImage;
}


-(void)_destruct
{
    self.delegate = nil;
    
    NSLog(@"connection cache count before=%d",[[LazyImageLoader connectionCache] count]);
    
    self.imageRequest.delegate = nil;
    
    if(self.urlString)
    [[LazyImageLoader connectionCache] removeObjectForKey:self.urlString];
    
    NSLog(@"connection cache count after=%d",[[LazyImageLoader connectionCache] count]);
    
}

+(UIImage*)resizeImage:(UIImage*)image toSize:(CGSize)newSize
{
	CGSize size = newSize;
    
    if(CGSizeEqualToSize(size,image.size)) return image;
    
    if (NULL != UIGraphicsBeginImageContextWithOptions)
		UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    else
		UIGraphicsBeginImageContext(size);
	
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
	
    // Draw the original image to the context
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGContextDrawImage(context, CGRectMake(0.0, 0.0, size.width, size.height), image.CGImage);
	
    // Retrieve the UIImage from the current context
    UIImage *imageOut = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
    return imageOut;
}

-(void)_downloadImageForURLString:(NSString *)_urlString
{
    NSLog(@"Image request sent");
    
    self.urlString = _urlString;
    
    NSString *aUrlString=[_urlString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    aUrlString=[aUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:aUrlString];
    
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    request.timeOutSeconds = 30.0;
    request.delegate = self;
    
    if(saveDirectToFile)
    {
        NSString *_cachePath  = self.targetPath ? self.targetPath :  cachePathForURL(aUrlString);
        
        if([[NSFileManager defaultManager] fileExistsAtPath:_cachePath])
        {
            [[NSFileManager defaultManager] removeItemAtPath:_cachePath error:nil];
        }
        
        NSString *tempDirPath = tempDownloadPathForURL(aUrlString);
        
        [request setDownloadDestinationPath:_cachePath];
        [request setTemporaryFileDownloadPath:tempDirPath];
        
        request.allowResumeForFileDownloads = YES;
        
    }
    
    self.imageRequest = request;
    
     /*
    [request setCompletionBlock:^{
        
        if(request.responseStatusCode==200)
            {
                NSData *_responseData = request.responseData;
                
                [self _imageDownloadDidFinishWithImageData:_responseData];
                
                                
            }
           else
            {
                [self _imageDownloadDidFailWithError:request.error];
                
            }
    }];
    
    [request setFailedBlock:^{
        
        [self _imageDownloadDidFailWithError:request.error];
    }];
    */
    
    [request startAsynchronous];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    if(request.responseStatusCode==200)
    {
        if(saveDirectToFile)
        {
            [self _imageDownloadDidFinish];
        }
        else
        {
           NSData *_responseData = request.responseData;
           [self _imageDownloadDidFinishWithImageData:_responseData];
        }
        
        /*
         if(request.contentLength==_responseData.length)
         [self _imageDownloadDidFinishWithImageData:_responseData];
         else
         [self _imageDownloadDidFailWithError:request.error];
         */
    }
    else
    {
        [self _imageDownloadDidFailWithError:request.error];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
      [self _imageDownloadDidFailWithError:request.error];
}

-(void)_imageDownloadDidFinishWithImageData:(NSData *)imageData
{
    if(!imageData.length) return;
    
    UIImage *_currentImage = [[UIImage alloc] initWithData:imageData];
    
    if(_currentImage)
    {
        self.image = _currentImage;
        
        
      //  NSString *imageDataKey=imageUrlKey(self.urlString);
      //  [LazyImageLoader _cacheImage:self.image forKey:imageDataKey];
        
        if(saveToDisk && _currentImage!=nil)
        {
           //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

               // NSString *pathExtension  = self.urlString.pathExtension;
               // dispatch_async(dispatch_get_main_queue(), ^{
            
                    @try {
                        
                        NSString* cachePath_ =self.targetPath ? self.targetPath :  cachePathForURL(self.urlString);
                        [self writeData:imageData toPath:cachePath_];
                        
                        //[self writeImage:_currentImage toPath:cachePath_];
                        
                        if(self.delegate!=nil && [self.delegate respondsToSelector:@selector(imageLoaderDidLoad:)])
                            [self.delegate imageLoaderDidLoad:self];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:ImageLoaderDidLoadImageNotification object:self];
                        
                        
                        
                        if(self.loaderCallbacK) self.loaderCallbacK(YES,self.image,self.urlString,self.indexPath,nil);
                        
                        //[self writeData:imageData toPath:cachePath_];
                        //[self writeImage:_currentImage toPath:cachePath_];
                    }
                    @catch (NSException *exception) {
                        
                        [exception print];
                    }
                    @finally
                    {
                        
                        [self _destruct];
                    }
   
             //   });
                
           // });
        }
        else
        {
            @try
            {
                if(self.delegate!=nil && [self.delegate respondsToSelector:@selector(imageLoaderDidLoad:)])
                    [self.delegate imageLoaderDidLoad:self];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:ImageLoaderDidLoadImageNotification object:self];
                
                if(self.loaderCallbacK) self.loaderCallbacK(YES,self.image,self.urlString,self.indexPath,nil);
                
               
            }
            @catch (NSException *exception)
            {
                [exception print];
            }
            @finally
            {
                
                 [self _destruct];
            }
   
        }
        
        [_currentImage release];
    }
    
    NSLog(@"Image downloaded");
}

-(void)_imageDownloadDidFinish
{
    //NSString *cachePath = self.targetPath ? self.targetPath :  cachePathForURL(self.urlString);
    NSString *cachePath =  self.imageRequest.downloadDestinationPath;
    UIImage *_currentImage = [[NSFileManager defaultManager] fileExistsAtPath:cachePath] ? [UIImage imageWithContentsOfFile:cachePath] : nil;
    
    self.image = _currentImage;
    
    
    if( _currentImage!=nil)
    {
        
        if(self.delegate!=nil && [self.delegate respondsToSelector:@selector(imageLoaderDidLoad:)])
            [self.delegate imageLoaderDidLoad:self];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:ImageLoaderDidLoadImageNotification object:self];
        
        if(self.loaderCallbacK) self.loaderCallbacK(YES,self.image,self.urlString,self.indexPath,nil);
    }
    
    [self _destruct];
    
    NSLog(@"Image downloaded");
}

-(void)_imageDownloadDidFailWithError:(NSError *)error
{
    @try {
        
        if(self.delegate !=nil && [self.delegate respondsToSelector:@selector(imageLoader: didFail:)])
        {
            [self.delegate imageLoader:self didFail:error];
        }

        
        if(self.loaderCallbacK) self.loaderCallbacK(NO,nil,self.urlString,self.indexPath,error);
        
               
        [[NSNotificationCenter defaultCenter] postNotificationName:ImageLoaderDidFailImageNotification object:self];
    }
    @catch (NSException *exception) {
        
        [exception print];
    }
    @finally
    {
        
        [self _destruct];
    }
   
}


@end
