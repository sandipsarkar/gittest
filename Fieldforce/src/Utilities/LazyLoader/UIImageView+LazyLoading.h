//
//  UIImageView+LazyLoading.h
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LazyImageLoader.h"
@interface UIImageView (LazyLoading)<ImageLoaderDelegate>

-(void)loadImageForUrl:(NSString *)imageURL completionHandler:(void(^)(UIImage *image,NSString *urlString,NSError *error))completionCallback;

-(void)loadImageForUrl:(NSString *)imageURL indexPath:(NSIndexPath *)indexPath delegate:(id<ImageLoaderDelegate>)delegate;

@end
