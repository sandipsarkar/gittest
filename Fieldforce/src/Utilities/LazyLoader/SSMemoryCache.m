//
//  SSImageCache.m
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "SSMemoryCache.h"

@interface SSMemoryCache()
{
    NSMutableDictionary *_cacheDict;
}
@end

@implementation SSMemoryCache
@synthesize cacheCount;

-(id)init
{
    if(self = [super init])
    {
        _cacheDict = [[NSMutableDictionary alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clear) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    
    return self;
}

-(void)setObject:(id)obj forKey:(id)aKey
{
    if(!obj || !aKey) return;
    
    NSInteger _cacheCountAfter = [_cacheDict count]+1;
    
    if(_cacheCountAfter>cacheCount)
    {
        NSArray *arr=[_cacheDict allKeys];
        NSUInteger index = [arr count] ? MAX((arc4random() % ([arr count]+1)-1),0) : 0;
        id key = index<[arr count] ?  [arr objectAtIndex:index] : [arr objectAtIndex:0];
        if(key)
        [_cacheDict removeObjectForKey:key];
    }
    
    if(cacheCount==0 || [_cacheDict count]<=cacheCount)
    {
        [_cacheDict setObject:obj forKey:aKey];
    }
}

-(id)objectForKey:(id)aKey
{
    return aKey ?  [_cacheDict objectForKey:aKey] : nil;
}
-(void)removeObjectForKey:(id)aKey
{
    if(!aKey) return;
    
    [_cacheDict removeObjectForKey:aKey];
}

-(void)dealloc
{
    [_cacheDict release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [super dealloc];
}

-(void)clear
{
    [_cacheDict removeAllObjects];
}

-(NSInteger)currentCachedCount
{
    return [_cacheDict count];
}

@end
