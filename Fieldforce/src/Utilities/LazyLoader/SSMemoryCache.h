//
//  SSImageCache.h
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SSMemoryCache : NSObject

@property(nonatomic,assign) NSInteger cacheCount;
@property (nonatomic,readonly) NSInteger currentCachedCount;

-(void)setObject:(id)obj forKey:(id)aKey;
-(id)objectForKey:(id)aKey;
-(void)removeObjectForKey:(id)aKey;
-(void)clear;

@end
