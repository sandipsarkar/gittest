//
//  InsetTableViewCell.h
//  Fieldforce
//
//  Created by Randem IT on 12/09/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InsetTableViewCellDelegate;

@interface InsetTableViewCell : UITableViewCell

@property(assign)CGFloat inset;

@property (nonatomic,readonly) UITableViewCellStyle cellStyle;
@property (nonatomic,assign) id <InsetTableViewCellDelegate> delegate;
@end

@protocol InsetTableViewCellDelegate<NSObject>

@optional
-(void)insetTableViewCell:(InsetTableViewCell *)cell didSelect:(BOOL)selected;
@end