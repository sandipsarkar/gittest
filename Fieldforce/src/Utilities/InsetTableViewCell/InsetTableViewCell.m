//
//  InsetTableViewCell.m
//  Fieldforce
//
//  Created by Randem IT on 12/09/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "InsetTableViewCell.h"


@implementation InsetTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        _cellStyle = style;
        
        _inset =  SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? 10.0 : 0.0;
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    if(self.delegate && [self.delegate respondsToSelector:@selector(insetTableViewCell:didSelect:)])
    {
        [self.delegate insetTableViewCell:self didSelect:selected];
    }
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    CGRect _contentRect      = self.contentView.frame;
    _contentRect.origin.x   +=_inset;
    _contentRect.size.width -= _inset*2;
    self.contentView.frame   = _contentRect;
    
    _contentRect = self.detailTextLabel.frame;
    if(self.cellStyle != UITableViewCellStyleSubtitle)
    {
       _contentRect.origin.x -=_inset*2;

        if(_contentRect.origin.x-CGRectGetMaxX(self.textLabel.frame)<4)
        {
            _contentRect.origin.x = CGRectGetMaxX(self.textLabel.frame)+4;
            _contentRect.size.width -= _inset*2;
        }
    }
    self.detailTextLabel.frame = _contentRect;
    
    _contentRect = self.accessoryView.frame;
    _contentRect.origin.x     -=  _inset/2;
     self.accessoryView.frame = _contentRect;
    
    if(self.editingAccessoryView)
    {
     _contentRect = self.editingAccessoryView.frame;
     _contentRect.origin.x     -=  _inset;
     self.editingAccessoryView.frame = _contentRect;
    }
    

}

-(void)dealloc
{
    _delegate = nil;
    
    [super dealloc];
}

@end
