//
//  NonRotatingUIImagePickerController.m
//  RepVisitationTool
//
//  Created by Subhojit Dey on 1/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "NonRotatingUIImagePickerController.h"

@implementation NonRotatingUIImagePickerController

- (BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
