//
//  UnderlineButton.h
//  UnderLineButton
//
//  Created by Subhojit Dey on 1/9/13.
//  Copyright (c) 2013 Subhojit Dey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnderlineButton : UIButton
{
    
}
@property(nonatomic,retain)UIColor *underLineColor;

@end
