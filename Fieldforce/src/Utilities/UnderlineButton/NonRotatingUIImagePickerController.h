//
//  NonRotatingUIImagePickerController.h
//  RepVisitationTool
//
//  Created by Subhojit Dey on 1/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NonRotatingUIImagePickerController : UIImagePickerController

@end
