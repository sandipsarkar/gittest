//
//  SSTabBarController.m
//  CustomTabBarController
//
//  Created by Sandip Sarkar on 12/08/11.
//  Copyright 2011 RANDEM IT. All rights reserved.
//

#import "SSTabBarController.h"
#import <objc/runtime.h>

NSString *SSTabBarDidSelectViewControllerNotification=@"_SSTabBarDidSelectViewControllerNotification";
NSString *SSTabBarDidDeselectViewControllerNotification=@"_SSTabBarDidDeselectViewControllerNotification";

#define TABBAR_HEIGHT 49.0f
#define BUTTON_WIDTH 85.0f
#define OFFSET 29.0f

#define KVO_FRAME @"view.frame"

#define TABBAR_COLOR [UIColor colorWithRed:0.1 green:0.3 blue:0.7 alpha:1]

//static SSTabBarController *ssTabBarController;
static CGRect _currentFrame;


@implementation UIViewController (SSTabBarController)
@dynamic SSTabBarItem;

static char TabBarItemKey;
static char TabBarControllerKey;

//CHANGED 7 MAY 2012
- (SSTabBarController *)ssTabBarController
{
    SSTabBarController *tabbarController=(SSTabBarController *) objc_getAssociatedObject (self , &TabBarControllerKey);
	return tabbarController;
}

//CHANGED 7 MAY 2012
-(void)setSsTabBarController:(SSTabBarController *)aTabBarController
{
    if(aTabBarController!=self.ssTabBarController)
    objc_setAssociatedObject (self , &TabBarControllerKey , aTabBarController , OBJC_ASSOCIATION_ASSIGN);
    
}


-(CGRect)currentFrame
{
    CGRect frame=_currentFrame;
    if(self.navigationController!=nil)
    {
        UINavigationController *_navController=(UINavigationController *)self.navigationController;
        float h=CGRectGetHeight(_navController.navigationBar.frame);
        frame.size.height-=h;
    }

    return frame;
}

-(void)showModalViewController:(UIViewController *)viewController animated:(BOOL)yesOrNo
{
    if(viewController==nil) return;
    
    if(self.ssTabBarController!=nil)
        [self.ssTabBarController presentModalViewController:viewController animated:yesOrNo];
    else
        [self presentModalViewController:viewController animated:yesOrNo];
}


-(void)setSSTabBarItem:(UIButton *)_SSTabBarItem
{
    objc_setAssociatedObject (self , &TabBarItemKey , _SSTabBarItem , OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(UIButton *)SSTabBarItem
{
    UIButton *but=(UIButton *) objc_getAssociatedObject (self , &TabBarItemKey);
    
    if(but==nil)
    {
        but=[UIButton buttonWithType:UIButtonTypeCustom];
        but.exclusiveTouch=YES;
        [self setSSTabBarItem:but];
    }
    
    return but;
}

@end

@implementation UIButton(SSTabBarController)

-(void)setBGImageForNormalState:(UIImage *)normalImage selectedState:(UIImage *)selectedImage
{
    if(normalImage)
    [self setBackgroundImage:normalImage forState:UIControlStateNormal];
    
    if(selectedImage)
    [self setBackgroundImage:selectedImage forState:UIControlStateSelected];
}


@end

@interface SSTabBarController() <UIScrollViewDelegate>
{
    UIImageView *selectedTabView;
}

-(void)_manageSelectionOfTabAtIndex:(NSInteger)index select:(BOOL)yesOrNo;
-(void)_setupTabBar;
-(void)_tabBarDidSelectTab:(id)sender;


@end

@implementation SSTabBarController

@synthesize viewControllers=_viewControllers;
@synthesize selectedViewController=_selectedViewController;
@synthesize selectedIndex=_selectedIndex;
@synthesize numberOfViewControllers=_numberOfViewControllers;
@synthesize delegate=_delegate;

- (void)dealloc
{
    //[_selectedViewController removeObserver:self forKeyPath:KVO_FRAME];
    [_viewControllers release];
    _viewControllers=nil;
   // [_selectedViewController release],_selectedViewController=nil;
    [_tabBar release];
    _tabBar=nil;
    _delegate=nil;
    [selectedTabView release];
    
    [super dealloc];
}


-(void)_setCurrentFrame
{
    _currentFrame=_containerFrame;
}

-(id)init
{
    if(self = [super init])
    {
        _containerFrame=[[UIScreen mainScreen] applicationFrame];
        //_containerFrame.origin.y-=[UIApplication sharedApplication].statusBarFrame.size.height;
        _containerFrame.origin.x=0;
        _containerFrame.size.height-=TABBAR_HEIGHT;
        
        _selectedIndex= NSNotFound; //NSIntegerMax;
        
        
    }
    
    return self;
}
-(id)initWithViewControllers:(NSArray *)vControllers
{
    self=[self init];
    
    if(self)
    {
        self.viewControllers=vControllers;
        //_numberOfViewControllers=[vControllers count];
    }
    
    return self;
}

-(void)setViewControllers:(NSArray *)viewControllers
{
    if(_viewControllers!=viewControllers)
    {
        for(UIViewController *controller in _viewControllers)
        {
            [controller didMoveToParentViewController:nil];
            [controller.view removeFromSuperview];
            [controller didMoveToParentViewController:nil];
        }
        
        [_viewControllers release];
        _viewControllers=[viewControllers retain];
        
        _numberOfViewControllers=[viewControllers count];
        
        for(UIViewController *controller in _viewControllers)
        {
            [self addChildViewController:controller];
            [controller didMoveToParentViewController:self];
        }
    }
}
-(void)setSelectedViewController:(UIViewController *)selectedViewController
{
    NSUInteger aSelectedIndex=[self.viewControllers indexOfObject:selectedViewController];
    
    if(aSelectedIndex != NSNotFound)
    {
      self.selectedIndex=aSelectedIndex;
    }
}

-(void)setSelectedIndex:(int)aSelectedIndex
{
    if(_selectedIndex!=aSelectedIndex)
    {
        if(aSelectedIndex>=0 && aSelectedIndex<_numberOfViewControllers)
        {
            UIViewController *_controller=[self.viewControllers objectAtIndex:aSelectedIndex];
            
            BOOL shouldSelect=YES;
            if([self.delegate respondsToSelector:@selector(tabBarController:shouldSelectViewController:atIndex:)])
            {
                shouldSelect=[self.delegate tabBarController:self shouldSelectViewController:_controller atIndex:aSelectedIndex];
            }
            
            if(!shouldSelect) return;
            
            
            // ========= Deselect the last selected view controller before selting a new one ================
           
                if(_selectedViewController!=nil)
                {
                    [_selectedViewController viewWillDisappear:NO];
                    
                    //remove kvo observer
                    //[_selectedViewController removeObserver:self forKeyPath:KVO_FRAME];
                    
                   // [_selectedViewController.view removeFromSuperview];
                    
                    // Deselect the tab
                    [self _manageSelectionOfTabAtIndex:_selectedIndex select:NO];
                    
                    if([self.delegate respondsToSelector:@selector(tabBarController:didDeselectViewController:)])
                    {
                        [self.delegate tabBarController:self didDeselectViewController:_selectedViewController];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:SSTabBarDidDeselectViewControllerNotification object:_selectedViewController];
                    
                    _selectedViewController.view.hidden=YES;

                    [_selectedViewController viewDidDisappear:NO];
                    
//                    if([_selectedViewController respondsToSelector:@selector(removeFromParentViewController)])
//                        [_selectedViewController removeFromParentViewController];
                    
                    _selectedViewController=nil;
                    
                }
            
            // ========= Deselect the last selected view controller before selting a new one ================
            
            
            /* Send custom KVO notification for "selectedIndex" */
            
            //[self willChangeValueForKey:@"selectedIndex"];
            
            _selectedIndex=aSelectedIndex;
            
            //[self didChangeValueForKey:@"selectedIndex"];
            
            
                // ================    Select the  new ViewController  ====================
                
                //Add kvo observer
             //   [_controller addObserver:self forKeyPath:KVO_FRAME options:NSKeyValueObservingOptionNew context:_controller];
                
                _selectedViewController=_controller;
               // _selectedViewController.view.frame=_containerFrame;
            _selectedViewController.view.frame=CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-TABBAR_HEIGHT);
                
                 [self _setCurrentFrame];
                
                [_selectedViewController viewWillAppear:NO];
                
                if([_selectedViewController.view isDescendantOfView:self.view])
                {
                    _selectedViewController.view.hidden=NO;
                    [self.view bringSubviewToFront:_selectedViewController.view];
                }
                else
                {
                    //_selectedViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
                    _selectedViewController.view.autoresizesSubviews = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
                    
//                    if([self respondsToSelector:@selector(addChildViewController:)])
//                       [self addChildViewController:_selectedViewController];
                    
                    [self.view addSubview:_selectedViewController.view];
                }
                
                [_selectedViewController viewDidAppear:NO];
                
                 // Select the tab
                 [self _manageSelectionOfTabAtIndex:_selectedIndex select:YES];
                
                if([self.delegate respondsToSelector:@selector(tabBarController:didSelectViewController:)])
                {
                    [self.delegate tabBarController:self didSelectViewController:_controller];
                }
            
                 [[NSNotificationCenter defaultCenter] postNotificationName:SSTabBarDidSelectViewControllerNotification object:_controller];
                
                // ================    Select the  new ViewController  ====================
            
        }
    }
}


-(void)hideTabBar:(BOOL)yesOrNO animated:(BOOL)animated
{
    if (yesOrNO == YES)
	{
		if (_tabBar.frame.origin.y == self.view.frame.size.height)
		{
			return;
		}
	}
	else 
	{
		if (_tabBar.frame.origin.y == self.view.frame.size.height - TABBAR_HEIGHT)
		{
			return;
		}
	}
    
    CGRect _hideFrame = CGRectMake(_tabBar.frame.origin.x, _tabBar.frame.origin.y + TABBAR_HEIGHT, _tabBar.frame.size.width, _tabBar.frame.size.height);
    
    CGRect _visibleFrame = CGRectMake(_tabBar.frame.origin.x, _tabBar.frame.origin.y - TABBAR_HEIGHT, _tabBar.frame.size.width, _tabBar.frame.size.height);
	
	if (animated == YES)
	{
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.3f];
        
		if (yesOrNO == YES)
		{
			_tabBar.frame = _hideFrame;
            _containerFrame.size.height+=TABBAR_HEIGHT;
		}
		else 
		{
			_tabBar.frame =_visibleFrame;
            _containerFrame.size.height-=TABBAR_HEIGHT;
		}
        
        self.selectedViewController.view.frame=_containerFrame;
		[UIView commitAnimations];
	}
	else 
	{
		if (yesOrNO == YES)
		{
			_tabBar.frame = _hideFrame;
            _containerFrame.size.height+=TABBAR_HEIGHT;
		}
		else 
		{
			_tabBar.frame =_visibleFrame;
            _containerFrame.size.height-=TABBAR_HEIGHT;
		}
        
        self.selectedViewController.view.frame=_containerFrame;
	}
    
    [self _setCurrentFrame];
    
}


#pragma mark - View lifecycle


/*
-(UIImage *)tabBarImageForController:(UIViewController *)controller state:(UIControlState)state
{
     UIImage *_tabbarImage=nil;
    
    if([controller isKindOfClass:[UINavigationController class]])
        _tabbarImage=[[(UINavigationController *)controller topViewController] SSTabBarImageForState:state];
    else
        _tabbarImage=[controller SSTabBarImageForState:state];

    return _tabbarImage;
}
*/

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif
    
    //_numberOfViewControllers=[self.viewControllers count];
    
    CGRect tabbarRect;
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
     tabbarRect=CGRectMake(0, CGRectGetHeight(_containerFrame), CGRectGetWidth(_containerFrame)+20.0, TABBAR_HEIGHT);
else
     tabbarRect=CGRectMake(0, CGRectGetHeight(_containerFrame), CGRectGetWidth(_containerFrame), TABBAR_HEIGHT);

    
    _tabBar =  [[UIView alloc] initWithFrame:tabbarRect];
    _tabBar.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
    _tabBar.backgroundColor     =  LOGIN_BG_COLOR;
    _tabBar.autoresizesSubviews = YES;
    [self.view addSubview:_tabBar];
    
    [self _setupTabBar];
    
    //self.selectedIndex=0;
}



#pragma mark Setup TabBar
-(void)_setupTabBar
{
    if(!selectedTabView)
    {
    selectedTabView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 99, 65)];
    selectedTabView.contentMode = UIViewContentModeScaleAspectFill;
    selectedTabView.backgroundColor=[UIColor clearColor];
    selectedTabView.autoresizingMask=UIViewAutoresizingFlexibleTopMargin;
    selectedTabView.center=_tabBar.center;
    [self.view addSubview:selectedTabView];
    }
    selectedTabView.hidden=YES;

    const float _buttonWidth=BUTTON_WIDTH;
   // float startx=0.0;
    float starty=0.0;
    
    //For Center Alignment of Tabs
    float _tolWidth = _buttonWidth*[self.viewControllers count];
    float _startX   = (CGRectGetWidth(_tabBar.frame)-_tolWidth)/2.0;
    
    int index=0;
    for(UINavigationController *controller in self.viewControllers)
    {
        //CHANGED 7 MAY 2012
        controller.ssTabBarController=self;
        
        UIViewController *_vcontroller=([controller isKindOfClass:[UINavigationController class]])? [controller topViewController]: (UIViewController *)controller;
        
        UIButton *btnTab=[_vcontroller.SSTabBarItem retain];
        CGRect buttonRect=CGRectMake(_startX+(index)*_buttonWidth, starty, MAX(_buttonWidth,CGRectGetWidth(btnTab.frame)), TABBAR_HEIGHT); 
        
        btnTab.frame=buttonRect;
        btnTab.tag=index+1;
        btnTab.exclusiveTouch=YES;
        [_tabBar addSubview:btnTab];
        //[btnTab setAutoresizingMasks];
        
        [btnTab addTarget:self action:@selector(_tabBarDidSelectTab:) forControlEvents:UIControlEventTouchDown];
        
        if(index==_selectedIndex)
        {
            btnTab.userInteractionEnabled=NO;
            btnTab.selected=YES;
        }
        
        [btnTab release];
        
        index++;
    }
    
}

#pragma mark -

#pragma mark Select / Deselect Tab

-(void)_manageSelectionOfTabAtIndex:(NSInteger)index select:(BOOL)yesOrNo
{
    if(index != NSIntegerMax)
    {
        int tag=index+1;
        UIButton *btnTab=(UIButton *)[_tabBar viewWithTag:tag];
        btnTab.userInteractionEnabled=!yesOrNo;
        //btnTab.selected=yesOrNo;
        btnTab.selected=NO;

        //=============== MANAGE VIEW FOT SELECTED TAB ================
        selectedTabView.hidden=!yesOrNo;
        [self.view bringSubviewToFront:selectedTabView];
        
        UIImage *selectedImage = [btnTab backgroundImageForState:UIControlStateSelected];
        if(selectedImage)selectedTabView.image = selectedImage;
     
       [UIView animateWithDuration:0.2 animations:^{

         CGPoint aCenter = [_tabBar convertPoint:btnTab.center toView:self.view];
         selectedTabView.center = CGPointMake(aCenter.x, aCenter.y-8.1);  //7
        
       }];
        //=============== MANAGE VIEW FOT SELECTED TAB ================
    }
}
#pragma mark -



-(void)_tabBarDidSelectTab:(id)sender
{
    self.selectedIndex = [sender tag]-1;
    
}


#pragma mark -
#pragma mark KVO
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if(![keyPath isEqualToString:KVO_FRAME]) return;
    
    if([_selectedViewController isEqual:context])
    {
        //NSLog(@"Frame.Height:%f",_selectedViewController.view.frame.size.height);
        
        if(_containerFrame.size.height!=CGRectGetHeight(_selectedViewController.view.frame))
        {
            _selectedViewController.view.frame=_containerFrame;
            
            [self _setCurrentFrame];
        }
    }
    
}
#pragma mark -

-(void)didChangeInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    switch (orientation)
    {
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            _containerFrame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
            //_containerFrame.origin.y -= [UIApplication sharedApplication].statusBarFrame.size.height;
            _containerFrame.size.height -= TABBAR_HEIGHT;          
        }
            break;
            
        default:
        {
            _containerFrame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
            //_containerFrame.origin.y -= [UIApplication sharedApplication].statusBarFrame.size.height;
            _containerFrame.size.height -= TABBAR_HEIGHT;
        }
            break;
    }
    
     _selectedViewController.view.frame = _containerFrame;
    
   if( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
   {
     _tabBar.frame = CGRectMake(0, CGRectGetHeight(_containerFrame), CGRectGetWidth(_containerFrame)+20, TABBAR_HEIGHT);
   }
   else
   {
     _tabBar.frame = CGRectMake(0, CGRectGetHeight(_containerFrame), CGRectGetWidth(_containerFrame), TABBAR_HEIGHT);
    }
}



#pragma mark scrollViewDelegate

#pragma mark-



- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    
    [self didChangeInterfaceOrientation:interfaceOrientation];
    
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
