//
//  SSTabBarController.h
//  CustomTabBarController
//
//  Created by Sandip Sarkar on 12/08/11.
//  Copyright 2011 RANDEM IT. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *SSTabBarDidSelectViewControllerNotification;
extern NSString *SSTabBarDidDeselectViewControllerNotification;

@class SSTabBarController;
@protocol SSTabBarControllerDelegate  <NSObject>

@optional
- (BOOL)tabBarController:(SSTabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController atIndex:(int)index;
- (void)tabBarController:(SSTabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController;
- (void)tabBarController:(SSTabBarController *)tabBarController didDeselectViewController:(UIViewController *)viewController;

@end


@interface SSTabBarController : UIViewController 
{
    @private
    NSArray *_viewControllers;
    int _selectedIndex;
    int _numberOfViewControllers;
    UIViewController *_selectedViewController;
    id <SSTabBarControllerDelegate> _delegate;
    //UIScrollView *scView;
    
    UIView *_tabBar;
    CGRect _containerFrame;
    
}

@property(nonatomic,copy) NSArray *viewControllers;
@property(nonatomic,assign) UIViewController *selectedViewController;
@property(nonatomic,assign) int selectedIndex;
@property (nonatomic, readonly) int numberOfViewControllers;
@property(nonatomic,assign) id <SSTabBarControllerDelegate> delegate;


-(id)initWithViewControllers:(NSArray *)vControllers;
-(void)hideTabBar:(BOOL)yesOrNo animated:(BOOL)animated;

@end

@interface UIViewController (SSTabBarController)
@property(nonatomic, assign) SSTabBarController *ssTabBarController;
@property(nonatomic,assign,readonly)CGRect currentFrame;
@property(nonatomic,retain) UIButton *SSTabBarItem;

//-(void)setSSTabBarImageForNormalState:(UIImage *)normalImage selectedState:(UIImage *)selectedImage;
//-(UIImage *)SSTabBarImageForState:(UIControlState)state;
//-(void)showModalViewController:(UIViewController *)viewController animated:(BOOL)yesOrNo;

@end

@interface UIButton(SSTabBarController)

-(void)setBGImageForNormalState:(UIImage *)normalImage selectedState:(UIImage *)selectedImage;

@end

