//
//  weblinkView.h
//  MZR
//
//  Created by SANDIP on 25/02/10.
//  Copyright 2010 navsoft. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void (^EmailDidSelectCallback) (id sender);
@interface WebLinkViewController : UIViewController<UIWebViewDelegate> {

    UIWebView *webView;
	NSString *urlstring;
    UIActivityIndicatorView *_indicatorView;
}

@property (nonatomic,copy)    NSString *urlstring;
@property (nonatomic,retain)  UIWebView *webView;
@property (nonatomic,assign)  UIDataDetectorTypes dataDetectorTypes;
@property (nonatomic,assign)  BOOL shouldCache;
@property (nonatomic,copy)    NSString *cachePath;

@property (nonatomic,assign) BOOL showEmailButton;

- (id)initWithUrlString:(NSString *)aUrlString;

-(void)addEmailDidSelectCallback:(EmailDidSelectCallback)_emailDidSelectCallback;

@end
