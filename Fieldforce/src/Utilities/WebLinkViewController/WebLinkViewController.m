//
//  weblinkView.m

//
//  Created by SANDIP on 25/02/10.
//  Copyright 2010 navsoft. All rights reserved.
//

#import "WebLinkViewController.h"

@interface WebLinkViewController()

@property(nonatomic,copy) EmailDidSelectCallback emailDidSelectCallback;
@property (nonatomic,retain) ASIHTTPRequest *currentRequest;
@end

@implementation WebLinkViewController


@synthesize urlstring;
@synthesize webView;
@synthesize dataDetectorTypes;
@synthesize showEmailButton;
@synthesize emailDidSelectCallback;
@synthesize shouldCache;
@synthesize cachePath;
@synthesize currentRequest;

- (id)initWithUrlString:(NSString *)aUrlString
{
    if (self = [super init]) 
    {
        urlstring = [aUrlString copy];
        
        //BY DEFAULT DATA DETECTOR TYPE NONE
        dataDetectorTypes = UIDataDetectorTypeNone;
       // shouldCache = YES;
    }
    return self;
}

-(void)addEmailDidSelectCallback:(EmailDidSelectCallback)_emailDidSelectCallback
{
    if(_emailDidSelectCallback)
    {
        self.emailDidSelectCallback = _emailDidSelectCallback;
    }
}

#pragma mark Validate PDF Data
-(BOOL)isValidPDFData:(NSData *)pdfData
{
    NSData *validPDF = [@"%PDF" dataUsingEncoding:NSASCIIStringEncoding];
    
    NSRange _range   = [pdfData rangeOfData:validPDF options:NSDataSearchAnchored range:NSMakeRange(0, pdfData.length)];
    
    BOOL isValid = (_range.location!=NSNotFound);
    
 
    NSLog( isValid? @"Valid PDF" : @"Invalid PDF");
    
    
    return isValid;
}
#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif
    
    [self addBackButtonWithSelector:@selector(backButtonAction:)];
    
    self.view.backgroundColor = [UIColor whiteColor];
	
	webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
	webView.opaque=YES;
	webView.dataDetectorTypes=dataDetectorTypes;
	webView.backgroundColor=[UIColor whiteColor];
	webView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	webView.multipleTouchEnabled=YES;
	webView.scalesPageToFit=YES;
	webView.delegate=self;

	[self.view addSubview:webView];
	webView.userInteractionEnabled=YES;
    
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _indicatorView.frame = CGRectMake((CGRectGetWidth(self.view.bounds)-50.0)/2.0, (CGRectGetHeight(self.view.bounds)-50.0)/2.0, 50.0, 50.0);
    
    _indicatorView.hidesWhenStopped = YES;
    [self.view addSubview:_indicatorView];
    
    _indicatorView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
    
    if(self.urlstring)
    {
        NSURL *targetURL = [NSURL URLWithString:self.urlstring];
        
        if(shouldCache)
        {
            [_indicatorView startAnimating];
            
            if(self.cachePath.length && [[NSFileManager defaultManager] fileExistsAtPath:self.cachePath])
               {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
                    NSData *cachedData = self.cachePath ? [NSData dataWithContentsOfFile:self.cachePath] : nil;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        if(cachedData.length)
                        {
                            NSString *extension = [self.cachePath pathExtension];
                            NSString *mimeType = [NSString stringWithFormat:@"application/%@",extension];
                            
                            [webView loadData:cachedData MIMEType:mimeType textEncodingName:@"UTF-8" baseURL:nil];
                            [_indicatorView stopAnimating];
                        }

                    });
                });
            
              }
            else
            {
            
            __block WebLinkViewController *weakSelf = self;
                
            __block  ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:targetURL];
              self.currentRequest = request;
              [request startAsynchronous];
            
                [request setCompletionBlock:^{
                
                    //NSLog(@"status=%@",request.responseStatusMessage);
                    
                    NSData *requestData = request.responseData;
                
                     if(requestData.length && [weakSelf isValidPDFData:requestData])
                     {
                        NSString *extension = [weakSelf.urlstring pathExtension];
                        NSString *mimeType  = [NSString stringWithFormat:@"application/%@",extension?extension:@"pdf"];
                       
                        [weakSelf.webView loadData:requestData MIMEType:mimeType textEncodingName:@"UTF-8" baseURL:nil];
                
                         if(weakSelf.cachePath)
                         [requestData writeToFile:weakSelf.cachePath atomically:YES];
                     }
                     else
                     {
                         [UIAlertView showWarningAlertWithTitle:@"Page not found" message:PAGE_NOT_FOUND_MESSAGE];
                     }
                   
                
                     [weakSelf->_indicatorView stopAnimating];
                    weakSelf.currentRequest = nil;
            }];
            
             [request setFailedBlock:^{
                
                  [weakSelf->_indicatorView stopAnimating];
                 
                  [self webView:weakSelf.webView didFailLoadWithError:request.error];
                   weakSelf.currentRequest = nil;
             }];
          }
        }
        else
        {
            
          //  NSURLRequest *request = targetURL ?  [NSURLRequest requestWithURL:targetURL] : nil;
            
            NSURLRequest *request = targetURL ? [NSURLRequest requestWithURL:targetURL cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval: 10.0] : nil;
            
            if(request)
            {
              [_indicatorView startAnimating];
              [webView loadRequest:request];
            }
        }
    }
    
	[UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    
    }


-(void)setShowEmailButton:(BOOL)_showEmailButton
{
    showEmailButton = _showEmailButton;
    
    if(showEmailButton)
    {
        if(!self.navigationItem.rightBarButtonItem)
        {
            
           if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
           {
                UIButton *emailButton=[UIButton repToolButtonWithImageName:@"email_button_blank.png" text:@"Email"];
                emailButton.titleLabel.textColor = [UIColor whiteColor];
                emailButton.titleLabel.font = [UIFont subaruBoldFontOfSize:16.0];
                [emailButton addTarget:self action:@selector(emailAction:) forControlEvents:UIControlEventTouchUpInside];
                UIBarButtonItem *emailBarButton = [[UIBarButtonItem alloc] initWithCustomView:emailButton];
                self.navigationItem.rightBarButtonItem = emailBarButton;
                [emailBarButton release];
          }
          else
          {
               UIBarButtonItem *emailBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Email" style:UIBarButtonItemStyleDone target:self action:@selector(emailAction:)];
               self.navigationItem.rightBarButtonItem = emailBarButtonItem;
              [emailBarButtonItem release];
          }
       }
    }
    else
    {
        self.navigationItem.rightBarButtonItem =nil;
    }
}

-(void)emailAction:(id)sender
{
    if(self.emailDidSelectCallback) self.emailDidSelectCallback(sender);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden=NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationController.navigationBarHidden=NO;
}

-(void)backButtonAction:(UIButton *)sender
{
    
    sender.userInteractionEnabled = NO;
    
     self.webView.delegate=nil;
    
    if(self.webView !=nil && self.webView.isLoading)
    {
       [self.webView stopLoading];
    }
        
    if(self.currentRequest && self.currentRequest.isExecuting)
    {
        //[self.currentRequest cancel];
        [self.currentRequest clearDelegatesAndCancel];
    }
    
    [_indicatorView stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    
    [self.navigationController popViewControllerAnimated:YES];
}

/*
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
	
	return YES;
}
*/

- (void)webViewDidStartLoad:(UIWebView *)_webView
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
	
}

- (void)webViewDidFinishLoad:(UIWebView *)_webView
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    [_indicatorView stopAnimating];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:.75];
    [self.view setAlpha:0.4f]; 
    [self.view setAlpha:1.f]; 
    [UIView commitAnimations];

    //NSString *javaScript = @"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=5, maximum-scale=0.5, user-scalable=0.5\">";
    //[webView stringByEvaluatingJavaScriptFromString:javaScript];
    
    
	
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
	[_indicatorView stopAnimating];
}

/*
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}
*/

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}


- (void)didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    self.webView.delegate=nil;
    if(self.webView !=nil && self.webView.isLoading)
          [self.webView stopLoading];
	
    if(!DISABLE_LOG)
    {
        [UIAlertView showWarningAlertWithTitle:@"memory leak" message:@"AT pdf view controller"];
    }

}

- (void)viewDidUnload 
{
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
    
   // if(webView !=nil && webView.isLoading)
    //  [webView stopLoading];
}


- (void)dealloc 
{
    [webView release];
    [urlstring release];
    [_indicatorView release];
    [emailDidSelectCallback release];
    [cachePath release];
    [currentRequest release];
    
    [super dealloc];
}


@end
