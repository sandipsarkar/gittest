//
//  NSDate+Formatter.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 17/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "NSDate+Formatter.h"

@implementation NSDate (Formatter)

+(NSDateFormatter *)formatter
{
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
    formatter = [[NSDateFormatter alloc] init];
        
    });
    
    return formatter;
}

-(NSString *)stringFromDateWithFormat:(NSString *)dateFormat
{
    //NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    NSDateFormatter* formatter = [NSDate formatter];
    [formatter setLocale   : [NSLocale autoupdatingCurrentLocale]];
    [formatter setTimeZone : [NSTimeZone systemTimeZone]];
    
    if(dateFormat)
    [formatter setDateFormat:dateFormat];
    NSString *_dateString  = [formatter stringFromDate:self];
    //[formatter release];
    
    return _dateString;
}

-(NSString *)stringFromDateWithFormat:(NSString *)dateFormat forTimeZone:(NSTimeZone *)timeZone
{
   // NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    NSDateFormatter* formatter = [NSDate formatter];
    [formatter setLocale:[NSLocale autoupdatingCurrentLocale]];
    
    if(dateFormat)
    [formatter setDateFormat:dateFormat];
    
    if(timeZone)
    [formatter setTimeZone:timeZone];
    
   ;
    
    NSString *_dateString  = [formatter stringFromDate:self];
   // [formatter release];
    
    return _dateString;
}


+(NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)dateFormat
{
    if(!dateString.length) return nil;
    
   // NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    NSDateFormatter* formatter = [NSDate formatter];
     [formatter setLocale:[NSLocale autoupdatingCurrentLocale]];
     [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    if(dateFormat)
    [formatter setDateFormat:dateFormat];
    NSDate *_date  = [formatter dateFromString:dateString];
   // [formatter release];
    
    return _date;
}

+(NSString *)dateStringFromWCFTimeInterval:(double)interval withFormat:(NSString *)dateFormat
{
   if(interval==0.0) return nil;

  NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval/1000.0];
    
  NSLog(@"Test date=%@",date);
    
  NSString *dateString = [date stringFromDateWithFormat:dateFormat];
  return dateString;
}

+(NSString *)dateStringFromWCFTimeInterval:(double)interval withFormat:(NSString *)dateFormat forTimeZone:(NSString *)zoneAbbreviation
{
    if(interval==0.0) return nil;
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval/1000.0];
    
    NSLog(@"Test date=%@",date);
    
    NSTimeZone *timeZone = zoneAbbreviation ?  [NSTimeZone timeZoneWithName:zoneAbbreviation] : [NSTimeZone systemTimeZone] ;
    
    NSString *dateString = [date stringFromDateWithFormat:dateFormat forTimeZone:timeZone];
    return dateString;
}

+(NSDate *)dateFromWCFTimeInterval:(double)interval
{
    if(interval==0.0) return nil;
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval/1000.0];
  
    return date;
}

-(NSTimeInterval)GMTTimeIntervalSince1970
{
    NSTimeZone *timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    
    NSTimeInterval diffFromGMT = [timeZone secondsFromGMTForDate:self];
    NSTimeInterval _dateInterval = [self timeIntervalSince1970]-diffFromGMT;
    
    return _dateInterval;
}

+(NSDate *)today
{
    NSInteger dateFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:dateFlags fromDate:[NSDate date]];
    NSDateComponents *newComponents = [[NSDateComponents alloc] init];
    [newComponents setYear:[dateComponents year]];
    [newComponents setMonth:[dateComponents month]];
    [newComponents setDay:[dateComponents day]];
    [newComponents setHour:0.0];
    [newComponents setMinute:0.0];
    [newComponents setSecond:0.0];
    
    
    NSDate *_date= [[NSCalendar currentCalendar] dateFromComponents:newComponents];
    
    
    
    [newComponents release];
    
    return _date;
}

+(NSDate *)todayOnTimeZone:(NSTimeZone *)timeZone
{
    NSInteger dateFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:dateFlags fromDate:[NSDate date]];
    NSDateComponents *newComponents = [[NSDateComponents alloc] init];
    [newComponents setTimeZone:timeZone];
    [newComponents setYear:[dateComponents year]];
    [newComponents setMonth:[dateComponents month]];
    [newComponents setDay:[dateComponents day]];
    [newComponents setHour:0.0];
    [newComponents setMinute:0.0];
    [newComponents setSecond:0.0];
    
    
    NSDate *_date= [[NSCalendar currentCalendar] dateFromComponents:newComponents];
    
    [newComponents release];
    
    return _date;
}


-(NSDate *)dateEnd
{
    NSInteger dateFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:dateFlags fromDate:self];
    NSDateComponents *newComponents = [[NSDateComponents alloc] init];
    [newComponents setYear:[dateComponents year]];
    [newComponents setMonth:[dateComponents month]];
    [newComponents setDay:[dateComponents day]];
    [newComponents setHour:23.0];
    [newComponents setMinute:59.0];
    [newComponents setSecond:59.0];
    
    NSDate *_date= [[NSCalendar currentCalendar] dateFromComponents:newComponents];
    
    [newComponents release];
    
    return _date;
}

-(NSDate *)dateSecondEnd
{
    NSInteger dateFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:dateFlags fromDate:self];
    NSDateComponents *newComponents = [[NSDateComponents alloc] init];
    [newComponents setYear:[dateComponents year]];
    [newComponents setMonth:[dateComponents month]];
    [newComponents setDay:[dateComponents day]];
    [newComponents setSecond:59.0];
    
    NSDate *_date= [[NSCalendar currentCalendar] dateFromComponents:newComponents];
    
    [newComponents release];
    
    return _date;
}



-(NSDate *)currentDateHour
{
    NSInteger dateFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:dateFlags fromDate:self];
    NSDateComponents *newComponents = [[NSDateComponents alloc] init];
    
    [newComponents setYear:[dateComponents year]];
    [newComponents setMonth:[dateComponents month]];
    [newComponents setDay:[dateComponents day]];
    [newComponents setHour:[dateComponents hour]];
    [newComponents setMinute:0.0];
    [newComponents setSecond:0.0];
    
    NSDate *_date= [[NSCalendar currentCalendar] dateFromComponents:newComponents];
    
    [newComponents release];
    
    return _date;
}

-(NSDate *)currentDateHourMinute
{
    NSInteger dateFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:dateFlags fromDate:self];
    NSDateComponents *newComponents = [[NSDateComponents alloc] init];
    
    [newComponents setYear:[dateComponents year]];
    [newComponents setMonth:[dateComponents month]];
    [newComponents setDay:[dateComponents day]];
    [newComponents setHour:[dateComponents hour]];
    [newComponents setMinute:[dateComponents minute]];
    [newComponents setSecond:0.0];
    
    NSDate *_date= [[NSCalendar currentCalendar] dateFromComponents:newComponents];
    
    [newComponents release];
    
    return _date;
}

-(NSDateComponents *)dateComponents
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSInteger dateFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    
    NSDateComponents *dateComponents = [gregorian components:dateFlags fromDate:self];
    dateComponents.second = 0;
    dateComponents.timeZone = [NSTimeZone systemTimeZone];
    
    [gregorian release];
    
    return dateComponents;
}

-(NSDateComponents *)dateComponentsWithEndDate:(NSDate *)date
{
    NSInteger dateFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:dateFlags fromDate:self toDate:date options:0];
    dateComponents.timeZone = [NSTimeZone systemTimeZone];
    return dateComponents;
}


-(BOOL)isBetweenDate:(NSDate *)startDate andDate:(NSDate *)endDate
{
    NSTimeInterval res1 =[startDate timeIntervalSinceReferenceDate];
    NSTimeInterval res2 =[endDate timeIntervalSinceReferenceDate];
    NSTimeInterval res_cur = [self timeIntervalSinceReferenceDate];
    
    BOOL isCurrentDate = ((res_cur>=res1) && (res_cur <= res2));
    
    return isCurrentDate;
    
}


//CLR : COMMON LANGUAGE RUNTIME

+ (NSDate *)dateWithCLRTicks:(int64_t)ticks fromTimeZone:(NSTimeZone *)timeZone
{
    const double GMTOffset = [ timeZone ? timeZone : [NSTimeZone defaultTimeZone] secondsFromGMT];
    
     double tickFactor = 10000000.0;
    const int64_t CLROffset = 621355968000000000;
    double timeStamp = ((double)(ticks - CLROffset) / tickFactor) - GMTOffset ;
    
    return [NSDate dateWithTimeIntervalSince1970:timeStamp];
}

+ (NSDate *)dateWithCLRTicks:(int64_t)ticks fromTimeZoneName:(NSString *)timeZoneName
{
    NSTimeZone *timeZone = timeZoneName?  [NSTimeZone timeZoneWithName:timeZoneName] : nil;
    
    if(!timeZone) timeZone = [NSTimeZone defaultTimeZone];
    
    return [self dateWithCLRTicks:ticks fromTimeZone:timeZone];
}




-(long long)CLRTicksForTimeZone:(NSTimeZone *)timeZone
{
    double GMTOffset = timeZone ? [timeZone  secondsFromGMT] : 0.0;
    
    double tickFactor = 10000000;
    const int64_t CLROffset = 621355968000000000;
    
    double timeSince1970 = [self timeIntervalSince1970];
    
    long long ticks = (long long)floor((timeSince1970+GMTOffset)* tickFactor) + CLROffset;
    
    return ticks;
}

-(long long)CLRTicks
{
    return [self CLRTicksForTimeZone:nil];;
}


-(NSString *)distanceStringFromDate:(NSDate *)date
{
    NSString *Ago      = NSLocalizedString(@"ago", @"Denotes past dates");
    NSString *FromNow  = NSLocalizedString(@"from now", @"Denotes future dates");
    NSString *LessThan = NSLocalizedString(@"Less than", @"Indicates a less-than number");
    NSString *About    = NSLocalizedString(@"About", @"Indicates an approximate number");
    NSString *Over     = NSLocalizedString(@"Over", @"Indicates an exceeding number");
    NSString *Almost   = NSLocalizedString(@"Almost", @"Indicates an approaching number");
    //NSString *Second   = NSLocalizedString(@"second", @"One second in time");
    NSString *Seconds  = NSLocalizedString(@"seconds", @"More than one second in time");
    NSString *Minute   = NSLocalizedString(@"minute", @"One minute in time");
    NSString *Minutes  = NSLocalizedString(@"minutes", @"More than one minute in time");
    NSString *Hour     = NSLocalizedString(@"hour", @"One hour in time");
    NSString *Hours    = NSLocalizedString(@"hours", @"More than one hour in time");
    NSString *Day      = NSLocalizedString(@"day", @"One day in time");
    NSString *Days     = NSLocalizedString(@"days", @"More than one day in time");
    NSString *Month    = NSLocalizedString(@"month", @"One month in time");
    NSString *Months   = NSLocalizedString(@"months", @"More than one month in time");
    NSString *Year     = NSLocalizedString(@"year", @"One year in time");
    NSString *Years    = NSLocalizedString(@"years", @"More than one year in time");
    
    NSTimeInterval since = [self timeIntervalSinceDate:date];
    NSString *direction = since <= 0.0 ? Ago : FromNow;
    since = fabs(since);
    
    int seconds   = (int)since;
    int minutes   = (int)round(since / SECONDS_PER_MINUTE);
    int hours     = (int)round(since / SECONDS_PER_HOUR);
    int days      = (int)round(since / SECONDS_PER_DAY);
    int months    = (int)round(since / SECONDS_PER_MONTH);
    int years     = (int)floor(since / SECONDS_PER_YEAR);
    int offset    = (int)round(floor((float)years / 4.0) * 1440.0);
    int remainder = (minutes - offset) % 525600;
    
    int number;
    NSString *measure;
    NSString *modifier = @"";
    
    switch (minutes) {
        case 0 ... 1:
            measure = Seconds;
            switch (seconds) {
                case 0 ... 4:
                    number = 5;
                    modifier = LessThan;
                    break;
                case 5 ... 9:
                    number = 10;
                    modifier = LessThan;
                    break;
                case 10 ... 19:
                    number = 20;
                    modifier = LessThan;
                    break;
                case 20 ... 39:
                    number = 30;
                    modifier = About;
                    break;
                case 40 ... 59:
                    number = 1;
                    measure = Minute;
                    modifier = LessThan;
                    break;
                default:
                    number = 1;
                    measure = Minute;
                    modifier = About;
                    break;
            }
            break;
        case 2 ... 44:
            number = minutes;
            measure = Minutes;
            break;
        case 45 ... 89:
            number = 1;
            measure = Hour;
            modifier = About;
            break;
        case 90 ... 1439:
            number = hours;
            measure = Hours;
            modifier = About;
            break;
        case 1440 ... 2529:
            number = 1;
            measure = Day;
            break;
        case 2530 ... 43199:
            number = days;
            measure = Days;
            break;
        case 43200 ... 86399:
            number = 1;
            measure = Month;
            modifier = About;
            break;
        case 86400 ... 525599:
            number = months;
            measure = Months;
            break;
        default:
            number = years;
            measure = number == 1 ? Year : Years;
            if (remainder < 131400) {
                modifier = About;
            } else if (remainder < 394200) {
                modifier = Over;
            } else {
                ++number;
                measure = Years;
                modifier = Almost;
            }
            break;
    }
    if ([modifier length] > 0) {
        modifier = [modifier stringByAppendingString:@" "];
    }
    return [NSString stringWithFormat:@"%@%d %@ %@", modifier, number, measure, direction];
}

+ (NSInteger)daysBetweenDate:(NSDate*)date toDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:date];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

@end
