//
//  NSException+Log.h
//  RepVisitationTool
//
//  Created by Some Sankar Ray on 06/03/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSException (Log)

-(void)print;

@end
