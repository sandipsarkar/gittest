//
//  NSException+Log.m
//  RepVisitationTool
//
//  Created by Some Sankar Ray on 06/03/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "NSException+Log.h"

@implementation NSException (Log)


-(void)print
{
   __unused NSString *message = [NSString stringWithFormat:@"Exception name:%@ reason:%@ at %s %d",self.name,self.reason,__func__, __LINE__];
    
    NSLog(@"%@",message);
    
    if(!DISABLE_LOG)
    {
        [UIAlertView showWarningAlertWithTitle:@"Exception" message:message];
    }
}

@end
