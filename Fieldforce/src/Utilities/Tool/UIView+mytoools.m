//
//  UIView+sandip.m
//  TestLazyLoading
//
//  Created by SANDIP_ RANDEM on 02/06/11.
//  Copyright 2011 RANDEM IT. All rights reserved.
//

#import "UIView+mytools.h"
@implementation UIView (mytools)

-(void)setAutoresizingMasks
{
    self.autoresizingMask=(UIViewAutoresizingFlexibleLeftMargin |
                           UIViewAutoresizingFlexibleRightMargin |
                           UIViewAutoresizingFlexibleWidth |
                           UIViewAutoresizingFlexibleTopMargin |
                           UIViewAutoresizingFlexibleHeight |
                           UIViewAutoresizingFlexibleBottomMargin);
}

#pragma mark CreateImage
+(UIImageView *)imageWithFrame:(CGRect)frame fileName:(NSString *)imagefileName
{
	
    UIImageView	*image=[[UIImageView alloc] initWithFrame:frame];
    if(imagefileName.length)
	image.image=[UIImage imageNamed:imagefileName];    
	[image setAutoresizingMasks];
	
	return [image autorelease];
}

#pragma mark CreateLabel	
+(UILabel *)labelWithText:(NSString *)text 
						  frame:(CGRect)frame 
					 textColor:(UIColor *)color 
					   bgColor:(UIColor *)bgColor 
					   fontsize:(CGFloat)fontsize
{
	
	UILabel *label = [[UILabel alloc] initWithFrame:frame];
	label.font=[UIFont systemFontOfSize:fontsize];
	label.textAlignment = UITextAlignmentLeft;
	label.text = text;
	label.textColor =color;	
    label.backgroundColor =bgColor;
	label.lineBreakMode=UILineBreakModeTailTruncation;
	
	[label setAutoresizingMasks];
	
	return [label autorelease];
	
}

#pragma mark CreateButton	
+(UIButton *)buttonWithTitle:(NSString *)title 
							  type:(UIButtonType)type
							 frame:(CGRect)frame 
						   bgImage:(NSString *)bgImageName
						titleColor:(UIColor *)titleColor
							target:(id)object
						 action:(SEL)action
{
	
	UIButton *button=[UIButton buttonWithType:type];
	button.frame =frame;
	if(title)[button setTitle:title forState:UIControlStateNormal];
	if(titleColor)[button setTitleColor:titleColor forState:UIControlStateNormal];
	button.backgroundColor=[UIColor clearColor];
	if(bgImageName)[button setBackgroundImage:[UIImage imageNamed: bgImageName] forState:UIControlStateNormal];
	
	button.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
	button.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
	
   // [button setAutoresizingMasks];
	
	[button addTarget:object action:action forControlEvents:UIControlEventTouchUpInside];
	
	return button;
	
}


#pragma mark CreateTextField
+(UITextField *)textFieldWithFrame:(CGRect)frame
                                     font:(CGFloat)font
								fontColor:(UIColor *)fontColor
								  bgColor:(UIColor *)bgColor
                                     text:(NSString *)text
							  borderStyle:(UITextBorderStyle)borderStyle

{                                   
	UITextField *textField= [[UITextField alloc] initWithFrame:frame];
	textField.borderStyle=borderStyle;            //UITextBorderStyleBezel;
    textField.textColor =fontColor;
    if(font>0.0)
	textField.font = [UIFont systemFontOfSize:font];
	textField.text=text;
    textField.placeholder =@"Enter";
    textField.backgroundColor =bgColor;
	textField.autocorrectionType=UITextAutocorrectionTypeNo;
	textField.keyboardType = UIKeyboardTypeDefault;
	textField.returnKeyType = UIReturnKeyDone;	
	textField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
	//returnTextField.secureTextEntry = YES;	// make the text entry secure (bullets)
	
	textField.clearButtonMode = UITextFieldViewModeWhileEditing;	// has a clear 'x' button to the right
	
	[textField setAutoresizingMasks];
	
	return [textField autorelease];
}

#pragma mark CreateTextView
+(UITextView *)textViewWithFrame:(CGRect)frame
                              font:(CGFloat)font
							  fontColor:(UIColor *)fontColor
                              bgColor:(UIColor *)bgColor
                              text:(NSString *)text

{
	
	UITextView *textView = [[UITextView alloc] initWithFrame:frame];
    textView.textColor =fontColor;
    if(font>0.0)
    textView.font = [UIFont fontWithName:@"Arial" size:font];
    textView.backgroundColor =bgColor;
	
	textView.text =text;
	textView.returnKeyType = UIReturnKeyDefault;
	textView.keyboardType = UIKeyboardTypeDefault;	// use the default type input method (entire keyboard)
	textView.autocorrectionType = UITextAutocorrectionTypeNo;
	
	[textView setAutoresizingMasks];
	
	return [textView autorelease];
}

+(UISearchBar *)searchBarWithFrame:(CGRect)frame 
                          delegate:(id <UISearchBarDelegate>)delegate 
                          barStyle:(UIBarStyle)barStyle  
                          placeHolder:(NSString *)placeHolder
{
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:frame];
    searchBar.barStyle=barStyle;
    searchBar.delegate=delegate;
    if(placeHolder.length)
    searchBar.placeholder=placeHolder;
    [searchBar setAutoresizingMasks];
    
    return [searchBar autorelease];
}



#pragma mark CreateProgressBar
+(UIProgressView *)progressBarWithFrame:(CGRect)frame
									 progress:(CGFloat)progress
										style:(UIProgressViewStyle)style
{
	UIProgressView *progressBar = [[UIProgressView alloc] initWithFrame:frame];
	progressBar.progressViewStyle =style;
	progressBar.progress =progress;
	[progressBar setAutoresizingMasks];
	return [progressBar autorelease];
}

#pragma mark CreateScrollView
+(UIScrollView *)scrollViewWithFrame:(CGRect)frame
                               contentSize:(CGSize)size
						   vScrollVisibled:(BOOL)visible
						   hScrollVisibled:(BOOL)hVisible
								willBounce:(BOOL)no
{
	
	UIScrollView *scrollView=[[UIScrollView alloc] initWithFrame:frame];
	
	scrollView.contentSize =size;
	scrollView.scrollEnabled=YES;
	scrollView.showsVerticalScrollIndicator=visible;
	scrollView.showsHorizontalScrollIndicator=hVisible;
	[scrollView flashScrollIndicators];
	scrollView.bounces =no;
	[scrollView autoresizesSubviews];
	
	return [scrollView autorelease];
	
}

#pragma mark CreateUISwitch
+(UISwitch *)switchWithFrame:(CGRect)frame 
							  target:(id)object
							  action:(SEL)action
								isOn:(BOOL)yes
{
	UISwitch *switchCtl = [[UISwitch alloc] initWithFrame:frame];
	[switchCtl addTarget:object action:action forControlEvents:UIControlEventValueChanged];
	switchCtl.backgroundColor = [UIColor clearColor];
	switchCtl.on=yes;
	return [switchCtl autorelease];
}

#pragma mark DrawSlider
+(UISlider *)sliderWithFrame:(CGRect)frame maxVal:(float)maxVal minVal:(float)minVal bgColor:(UIColor *)bgColor target:(id)target action:(SEL)action
{
	UISlider *slider = [[UISlider alloc] initWithFrame:frame];
	[slider addTarget:target action:action forControlEvents:UIControlEventValueChanged];
	slider.backgroundColor = bgColor;
	slider.minimumValue =minVal;
	slider.maximumValue =maxVal;
	slider.continuous = YES;
	//[slider setShowValue:YES];
	//slider.value = MaxVal/ 2.0;
	
	return [slider autorelease];
}

#pragma mark Draw SegmentedControl
+(UISegmentedControl *)segmentedControlWithFrame:(CGRect)frame items:(NSArray *)items 
										 selectedIndex:(int)index  tintColor:(UIColor *)tintColor 
												target:(id)target action:(SEL)action style:(UISegmentedControlStyle)style
{
	UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:items];
	if(tintColor) segmentedControl.tintColor=tintColor;
	[segmentedControl addTarget:target action:action forControlEvents:UIControlEventValueChanged];
	segmentedControl.segmentedControlStyle =style;
    segmentedControl.backgroundColor = [UIColor clearColor];
	[segmentedControl sizeToFit];
	segmentedControl.frame = frame;
	segmentedControl.selectedSegmentIndex=index;
	
	return [segmentedControl autorelease];
}

#pragma mark Draw PageControl
+(UIPageControl *)pageControlWithFrame:(CGRect)frame numberOfPages:(int)pagesNo target:(id)target action:(SEL)action
{
	
	UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:frame];
	[pageControl addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
	[pageControl setBackgroundColor:[UIColor clearColor]];
	pageControl.numberOfPages = pagesNo;
	
	return [pageControl autorelease];
}

#pragma mark ShowMenuController
+(UIMenuController *)menuControllerWithItems:(NSArray *)items 
                                  targetRect:(CGRect)targetRect 
                                  inView:(UIView *)inView 
                                  arrowDirection:(UIMenuControllerArrowDirection)arrowDirection 
{
    UIMenuController *menuController = [UIMenuController sharedMenuController];
	menuController.menuItems =items; 
	menuController.arrowDirection=arrowDirection;
	[menuController setTargetRect:targetRect inView:inView];
    // [menuController setMenuVisible:YES animated:YES];
    
    return menuController;
}

+(UIActivityIndicatorView *)activityIndicatorViewWithFrame:(CGRect)frame style:(UIActivityIndicatorViewStyle)indicatorViewStyle 
{
   UIActivityIndicatorView *activityIndicator=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:indicatorViewStyle];
    activityIndicator.frame=frame;
    activityIndicator.hidesWhenStopped=YES;

    return [activityIndicator autorelease];

}

+(UITableView *)tableViewWithFrame:(CGRect)frame style:(UITableViewStyle)style delegate:(id<UITableViewDelegate>)delegate datasource:(id<UITableViewDataSource>)datasource
{
    UITableView *aTableview=[[UITableView alloc] initWithFrame:frame style:style];
    aTableview.delegate=delegate;
    aTableview.dataSource=datasource;
    [aTableview setAutoresizingMasks];
    return [aTableview autorelease];;
}

+(UIAlertView *)showWarningAlertWithTitle:(NSString *)title message:(NSString *)aMsg
{
    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:title message:aMsg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alertView show];
    [alertView release];
    
    return alertView;
}

/* only for UIWebView */
-(void)shouldBounce:(BOOL)yesOrNo
{
    for(UIView *_view in self.subviews)
    {
        if([_view isKindOfClass:[UIScrollView class]])
        {
            [(UIScrollView *)_view setBounces:yesOrNo];
        }
    }
}

-(UIView *)superViewOfType:(Class)aClass
{
    UIView *_superView = self;
    
    while ( nil != (_superView = [_superView superview]) && ![_superView isKindOfClass:aClass] )
    {
        
    }
    
    return _superView;
}

-(UIView *)subViewOfType:(Class)aClass
{
    UIView *_subView = nil;
    
    for(UIView *v in self.subviews)
    {
        if([v isKindOfClass:aClass])
        {
            _subView = v;
            break;
        }
        else
        {
            _subView = [v subViewOfType:aClass];
            
            if(_subView && [_subView isKindOfClass:aClass])
            {
                break;
            }
        }
    }

    
    return _subView;
}



- (CGFloat)left 
{
    return self.frame.origin.x;
}

- (CGFloat)top 
{
    return self.frame.origin.y;
}

- (CGFloat)right
{
    return CGRectGetMaxX(self.frame);
}

- (CGFloat)bottom 
{
    return CGRectGetMaxY(self.frame);
}

- (CGFloat)width 
{
    return CGRectGetWidth(self.bounds);
}

- (void)setWidth:(CGFloat)width 
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat)height 
{
    return CGRectGetHeight(self.bounds);
}

- (void)setHeight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (void)removeSubviews 
{
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
}


-(void)roundCorners:(UIRectCorner)corner withRedius:(CGSize)redius
{
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    UIBezierPath *roundedPath = 
    [UIBezierPath bezierPathWithRoundedRect:self.bounds
                          byRoundingCorners:corner
                                cornerRadii:redius];    
    maskLayer.fillColor = [[UIColor whiteColor] CGColor];
    maskLayer.backgroundColor = [[UIColor clearColor] CGColor];
    maskLayer.path = [roundedPath CGPath];
    self.layer.mask = maskLayer;
}
-(void)addLeftBoderWithColor:(UIColor *)color borderWidth:(CGFloat)width
{
    CALayer *leftBorder = [CALayer layer];
    leftBorder.frame = CGRectMake(0.0f, 0.0, width, CGRectGetHeight(self.bounds));
    leftBorder.backgroundColor = color.CGColor;
    [self.layer addSublayer:leftBorder];
}

-(void)addRightBoderWithColor:(UIColor *)color borderWidth:(CGFloat)width
{
    CALayer *leftBorder = [CALayer layer];
    leftBorder.frame = CGRectMake(CGRectGetWidth(self.bounds)-1.0f, 0.0, width, CGRectGetHeight(self.bounds));
    leftBorder.backgroundColor = color.CGColor;
    [self.layer addSublayer:leftBorder];
}

-(void)addInnerShadowWithColor:(UIColor *)color
{
    //==========================
    
    CAShapeLayer *prevLayer = nil;
    for(CAShapeLayer *addedLayer in self.layer.sublayers)
    {
        if([addedLayer.name isEqualToString:@"shadow"])
        {
            prevLayer = addedLayer;
            break;
        }
    }
    
    if(prevLayer) [prevLayer removeFromSuperlayer];
    
    UIBezierPath *innerPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.layer.cornerRadius];
    
    CAShapeLayer* shadowLayer = [CAShapeLayer layer];
    shadowLayer.name = @"shadow";
    shadowLayer.frame = self.bounds;
    //shadowLayer.cornerRadius = 8.0f;
    [shadowLayer setOpacity:1];
    // Standard shadow stuff
    [shadowLayer setShadowOpacity:1.0f];
    [shadowLayer setShadowColor:[color CGColor]];
    [shadowLayer setShadowOffset:CGSizeMake(0.5f, 1.0f)];
    [shadowLayer setShadowRadius:2];
    // Causes the inner region in this example to NOT be filled.
    [shadowLayer setFillRule:kCAFillRuleEvenOdd];
    
    // Create the larger rectangle path.
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectInset(self.bounds, -1.5f, -2.0f));
    // Add the inner path so it's subtracted from the outer path.
    CGPathAddPath(path, NULL, innerPath.CGPath);
    CGPathCloseSubpath(path);
    
    [shadowLayer setPath:path];
    CGPathRelease(path);
    
    [self.layer setMasksToBounds:YES];
    [self.layer addSublayer:shadowLayer];
    

    //======================== need to remove =========================//
    CAShapeLayer* maskLayer = [CAShapeLayer layer];
    [maskLayer setPath:innerPath.CGPath];
    [shadowLayer setMask:maskLayer];
    //===================================================//
   
}


-(void)addInnerShadowWithColor:(UIColor *)color rect:(CGRect)rect
{
    CAShapeLayer *prevLayer = nil;
    for(CAShapeLayer *addedLayer in self.layer.sublayers)
    {
        if([addedLayer.name isEqualToString:@"shadow"])
        {
            prevLayer = addedLayer;
            break;
        }
    }
    
    if(prevLayer) [prevLayer removeFromSuperlayer];
    //==========================
    UIBezierPath *innerPath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:self.layer.cornerRadius];
    
    CAShapeLayer* shadowLayer = [CAShapeLayer layer];
    shadowLayer.frame = self.bounds;
    shadowLayer.name = @"shadow";
    //shadowLayer.cornerRadius = 8.0f;
    [shadowLayer setOpacity:1];
    // Standard shadow stuff
    [shadowLayer setShadowOpacity:1.0f];
    [shadowLayer setShadowColor:[color CGColor]];
    [shadowLayer setShadowOffset:CGSizeMake(0.0f, 1.0f)];
    [shadowLayer setShadowRadius:2];
    // Causes the inner region in this example to NOT be filled.
    [shadowLayer setFillRule:kCAFillRuleEvenOdd];
    
    // Create the larger rectangle path.
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectInset(rect, -2.0f, -2.0f));
    // Add the inner path so it's subtracted from the outer path.
    CGPathAddPath(path, NULL, innerPath.CGPath);
    CGPathCloseSubpath(path);
    
    [shadowLayer setPath:path];
    CGPathRelease(path);
    
    [self.layer setMasksToBounds:YES];
    [self.layer addSublayer:shadowLayer];
    
    
    //======================== need to remove =========================//
    CAShapeLayer* maskLayer = [CAShapeLayer layer];
    [maskLayer setPath:innerPath.CGPath];
    [shadowLayer setMask:maskLayer];
    //===================================================//
    
}

/*
-(void)addInnerShadow
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // clip context so shadow only shows on the inside
    CGPathRef roundedRect = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:4].CGPath;
    CGContextAddPath(context, roundedRect);
    CGContextClip(context);
    
    CGContextAddPath(context, roundedRect);
    CGContextSetShadowWithColor(UIGraphicsGetCurrentContext(), CGSizeMake(0, 0), 3, [UIColor colorWithWhite:0 alpha:1].CGColor);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:0 alpha:1].CGColor);
    CGContextStrokePath(context);

}
*/


@end


@implementation UIView (animation)
#pragma mark -
#pragma mark Animation
-(void)animateFromAlpha:(float)fromAlpha toAlpha:(float)toAlpha duration:(NSTimeInterval)interval
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:interval];
	self.alpha=fromAlpha;
	self.alpha=toAlpha;
	[UIView commitAnimations];	
}

-(void)animateWithTransition:(UIViewAnimationTransition)transition
{   
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.70];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationTransition:transition forView:self cache:NO]; 
	[UIView commitAnimations];
}
-(void)animateWithTransition:(UIViewAnimationTransition)transition duration:(NSTimeInterval)interval
{   
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:interval];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationTransition:transition forView:self cache:NO]; 
	[UIView commitAnimations];
}

-(void)animateFromFrame:(CGRect)fromFrame toFrame:(CGRect)toFrame duration:(NSTimeInterval)interval
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:interval];
	self.frame=fromFrame;
	self.frame=toFrame;
	[UIView commitAnimations];	
}

-(void)animateFromPosition:(CGPoint)fromPosition toPosition:(CGPoint)toPosition duration:(NSTimeInterval)interval
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:interval];
	self.center=fromPosition;
	self.center=toPosition;
	[UIView commitAnimations];	
    
}



#pragma mark -
@end

