//
//  NSObject+mytools.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 25/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "NSObject+mytools.h"
#import <objc/runtime.h>

@implementation NSObject (mytools)
@dynamic identifier;

static char _identifierKey;

-(void)setIdentifier:(id)_identifier
{
    if(self.identifier!=_identifier)
        objc_setAssociatedObject (self , &_identifierKey , _identifier , OBJC_ASSOCIATION_ASSIGN);
}

-(id)identifier
{
    return  (id)objc_getAssociatedObject (self , &_identifierKey);
}

-(void)setAssociateObject:(id)_obj forKey:(NSString *)key
{
    //if(self.identifier!=_identifier)
        objc_setAssociatedObject (self , key , _obj , OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
-(void)setWeakAssociateObject:(id)_obj forKey:(NSString *)key
{
    //if(self.identifier!=_identifier)
    objc_setAssociatedObject (self , key , _obj , OBJC_ASSOCIATION_ASSIGN);
}

-(void)setCopyAssociateObject:(id)_obj forKey:(NSString *)key
{
    //if(self.identifier!=_identifier)
    objc_setAssociatedObject (self , key , _obj , OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(id)associateObjectForKey:(NSString *)key
{
    return  (id)objc_getAssociatedObject (self , key);
}

BOOL isClassAvailable(NSString *className)
{
    Class _class = NSClassFromString(className);
    
    return (_class!=nil);
}

-(void)performBlock:(void (^)(void))block afterDealy:(NSTimeInterval)delay
{
    double_t delayInSeconds = delay;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void)
    {
        if(block) block();
    });

}


@end
