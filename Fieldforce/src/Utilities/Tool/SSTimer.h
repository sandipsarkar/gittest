//
//  SSTimer.h
//  RepVisitationTool
//
//  Created by Sandip Sarkar on 07/05/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SSTimerCallbak) (void);
@interface SSTimer : NSObject

@property(nonatomic,readonly) id target;
@property(nonatomic,readonly) SEL action;
@property(nonatomic,assign) NSUInteger maxRepeatCount;

-(void)scheduleWithTimeInterval:(NSTimeInterval)interval shouldRepeat:(BOOL)repeat;

-(void)scheduleWithTimeInterval:(NSTimeInterval)interval shouldRepeat:(BOOL)repeat callback:(void(^)())_callback;

-(void)resume;
-(void)invalidate;

@end
