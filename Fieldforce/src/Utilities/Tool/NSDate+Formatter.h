//
//  NSDate+Formatter.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 17/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SECONDS_PER_MINUTE 60.0
#define SECONDS_PER_HOUR   3600.0
#define SECONDS_PER_DAY    86400.0
#define SECONDS_PER_MONTH  2592000.0
#define SECONDS_PER_YEAR   31536000.0

@interface NSDate (Formatter)

+(NSDateFormatter *)formatter;
-(NSString *)stringFromDateWithFormat:(NSString *)dateFormat;
-(NSString *)stringFromDateWithFormat:(NSString *)dateFormat forTimeZone:(NSTimeZone *)timeZone;

+(NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)dateFormat;
+(NSDate *)dateFromWCFTimeInterval:(double)interval;
+(NSString *)dateStringFromWCFTimeInterval:(double)interval withFormat:(NSString *)dateFormat;
+(NSString *)dateStringFromWCFTimeInterval:(double)interval withFormat:(NSString *)dateFormat forTimeZone:(NSString *)zoneAbbreviation;
-(NSTimeInterval)GMTTimeIntervalSince1970;

+(NSDate *)today;
+(NSDate *)todayOnTimeZone:(NSTimeZone *)timeZone;

-(NSDate *)currentDateHour;
-(NSDate *)currentDateHourMinute;
-(NSDate *)dateEnd;
-(NSDate *)dateSecondEnd;
-(NSDateComponents *)dateComponents;
-(NSDateComponents *)dateComponentsWithEndDate:(NSDate *)date;

-(BOOL)isBetweenDate:(NSDate *)startDate andDate:(NSDate *)endDate;

+ (NSDate *)dateWithCLRTicks:(int64_t)ticks fromTimeZone:(NSTimeZone *)timeZone;
+ (NSDate *)dateWithCLRTicks:(int64_t)ticks fromTimeZoneName:(NSString *)timeZoneName;

-(long long)CLRTicks;
-(long long)CLRTicksForTimeZone:(NSTimeZone *)timeZone;

-(NSString *)distanceStringFromDate:(NSDate *)date;
+(NSInteger)daysBetweenDate:(NSDate*)date toDate:(NSDate*)toDateTime;


@end
