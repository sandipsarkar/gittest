//
//  SSTimer.m
//  RepVisitationTool
//
//  Created by Sandip Sarkar on 07/05/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "SSTimer.h"

@interface SSTimer()
{
    BOOL shouldRepeat;
    
    NSTimeInterval _timerInterval;
}

@property(nonatomic,copy) SSTimerCallbak callback;
@end


@implementation SSTimer
@synthesize target=_target,action=_action;
@synthesize callback;

-(id)initWithTarget:(id)target action:(SEL)action
{
    if(self=[super init])
    {
        _target = target;
        _action = action;
    }
    
    return self;
}

-(void)scheduleWithTimeInterval:(NSTimeInterval)interval shouldRepeat:(BOOL)repeat
{
    shouldRepeat   = repeat;
    _timerInterval = interval;
    
    [self performSelector:@selector(_timerAction) withObject:nil afterDelay:interval];
}

-(void)scheduleWithTimeInterval:(NSTimeInterval)interval shouldRepeat:(BOOL)repeat callback:(void(^)())_callback
{
    shouldRepeat   = repeat;
    _timerInterval = interval;
    self.callback  = _callback;
    
    [self performSelector:@selector(_timerAction) withObject:nil afterDelay:interval];
}

-(void)_timerAction
{
    if((_target && _action) && [_target respondsToSelector:_action])
    {
        [_target performSelector:_action];
    }
    
    if(self.callback) self.callback();
    
    if(shouldRepeat)
        [self performSelector:@selector(_timerAction) withObject:nil afterDelay:_timerInterval];
}

-(void)invalidate
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_timerAction) object:nil];
}

-(void)resume
{
    [self performSelector:@selector(_timerAction) withObject:nil afterDelay:_timerInterval];
}

-(void)dealloc
{
    _target = nil;
    _action = nil;
    [callback release];
    callback = nil;
    [super dealloc];
}

@end
