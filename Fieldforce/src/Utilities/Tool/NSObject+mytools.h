//
//  NSObject+mytools.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 25/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>

#define OBJC_DESTROY(obj) { [obj release];obj=nil; }

#define NULL_NIL(_O) [_O isEqual:[NSNull null]] ? nil : _O

#define ARC_ENABLED __has_feature(objc_arc)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_7  ([[[UIDevice currentDevice] systemVersion] compare:@"7" options:NSNumericSearch] != NSOrderedAscending)


#if __has_feature(objc_arc)
#define OBJC_AUTORELEASE(exp) exp
#define OBJC_RELEASE(exp) exp
#define OBJC_RETAIN(exp) exp
#else
#define OBJC_AUTORELEASE(exp) [exp autorelease]
#define OBJC_RELEASE(exp) [exp release]
#define OBJC_RETAIN(exp) [exp retain]
#endif

#ifndef OBJC_INSTANCETYPE
#if __has_feature(objc_instancetype)
#define OBJC_INSTANCETYPE instancetype
#else
#define OBJC_INSTANCETYPE id
#endif
#endif

#ifndef OBJC_STRONG
#if __has_feature(objc_arc)
#define OBJC_STRONG strong
#else
#define OBJC_STRONG retain
#endif
#endif

#ifndef OBJC_WEAK
#if __has_feature(objc_arc_weak)
#define OBJC_WEAK weak
#elif __has_feature(objc_arc)
#define OBJC_WEAK unsafe_unretained
#else
#define OBJC_WEAK assign
#endif
#endif


@interface NSObject (mytools)

@property(nonatomic,OBJC_WEAK)   id identifier;



BOOL isClassAvailable(NSString *className);

-(void)performBlock:(void (^)(void))block afterDealy:(NSTimeInterval)delay;

-(void)setWeakAssociateObject:(id)_obj forKey:(NSString *)key;
-(void)setAssociateObject:(id)_obj forKey:(NSString *)key;
-(void)setCopyAssociateObject:(id)_obj forKey:(NSString *)key;
-(id)associateObjectForKey:(NSString *)key;

@end
