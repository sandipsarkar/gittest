//
//  UIView+sandip.h
//  TestLazyLoading
//
//  Created by SANDIP_ RANDEM on 02/06/11.
//  Copyright 2011 RANDEM IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <QuartzCore/QuartzCore.h>

static void showAlert(NSString *title, NSString *message)
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    [alert release];
}

@interface UIView (mytools)

-(void)setAutoresizingMasks;
+(UIImageView *)imageWithFrame:(CGRect)frame fileName:(NSString *)imagefileName;

+(UILabel *)labelWithText:(NSString *)text 
                    frame:(CGRect)frame 
                textColor:(UIColor *)color 
                  bgColor:(UIColor *)bgColor 
                 fontsize:(CGFloat)fontsize;

+(UIButton *)buttonWithTitle:(NSString *)title 
                        type:(UIButtonType)type
                       frame:(CGRect)frame 
                     bgImage:(NSString *)bgImageName
                  titleColor:(UIColor *)titleColor
                      target:(id)object
                      action:(SEL)action;

+(UIProgressView *)progressBarWithFrame:(CGRect)frame
                               progress:(CGFloat)progress
                                  style:(UIProgressViewStyle)style;

+(UITextField *)textFieldWithFrame:(CGRect)frame
                              font:(CGFloat)font
                         fontColor:(UIColor *)fontColor
                           bgColor:(UIColor *)bgColor
                              text:(NSString *)text
                       borderStyle:(UITextBorderStyle)borderStyle;

+(UITextView *)textViewWithFrame:(CGRect)frame
                            font:(CGFloat)font
                       fontColor:(UIColor *)fontColor
                         bgColor:(UIColor *)bgColor
                            text:(NSString *)text;

+(UISearchBar *)searchBarWithFrame:(CGRect)frame 
                          delegate:(id <UISearchBarDelegate>)delegate 
                          barStyle:(UIBarStyle)barStyle  
                       placeHolder:(NSString *)placeHolder;


+(UIScrollView *)scrollViewWithFrame:(CGRect)frame
                         contentSize:(CGSize)size
                     vScrollVisibled:(BOOL)visible
                     hScrollVisibled:(BOOL)hVisible
                          willBounce:(BOOL)no;

+(UISwitch *)switchWithFrame:(CGRect)frame 
                      target:(id)object
                      action:(SEL)action
                        isOn:(BOOL)yes;

+(UISlider *)sliderWithFrame:(CGRect)frame maxVal:(float)maxVal minVal:(float)minVal bgColor:(UIColor *)bgColor target:(id)target action:(SEL)action;

+(UISegmentedControl *)segmentedControlWithFrame:(CGRect)frame items:(NSArray *)items 
                                   selectedIndex:(int)index  tintColor:(UIColor *)tintColor 
                                          target:(id)target action:(SEL)action style:(UISegmentedControlStyle)style;

+(UIPageControl *)pageControlWithFrame:(CGRect)frame numberOfPages:(int)pagesNo target:(id)target action:(SEL)action;

+(UIMenuController *)menuControllerWithItems:(NSArray *)items 
                                  targetRect:(CGRect)targetRect 
                                      inView:(UIView *)inView 
                              arrowDirection:(UIMenuControllerArrowDirection)arrowDirection;
+(UIActivityIndicatorView *)activityIndicatorViewWithFrame:(CGRect)frame style:(UIActivityIndicatorViewStyle)indicatorViewStyle;

+(UITableView *)tableViewWithFrame:(CGRect)frame style:(UITableViewStyle)style delegate:(id<UITableViewDelegate>)delegate datasource:(id<UITableViewDataSource>)datasource;

+(UIAlertView *)showWarningAlertWithTitle:(NSString *)title message:(NSString *)aMsg;

/* only for UIWebView */
-(void)shouldBounce:(BOOL)yesOrNo;

- (void)removeSubviews;

-(UIView *)superViewOfType:(Class)aClass;
-(UIView *)subViewOfType:(Class)aClass;

-(void)roundCorners:(UIRectCorner)corner withRedius:(CGSize)redius;
-(void)addLeftBoderWithColor:(UIColor *)color borderWidth:(CGFloat)width;
-(void)addRightBoderWithColor:(UIColor *)color borderWidth:(CGFloat)width;
-(void)addInnerShadowWithColor:(UIColor *)color;
-(void)addInnerShadowWithColor:(UIColor *)color rect:(CGRect)rect;

@property(nonatomic,readonly) CGFloat top;
@property(nonatomic,readonly) CGFloat bottom;
@property(nonatomic,readonly) CGFloat left;
@property(nonatomic,readonly) CGFloat right;
@property(nonatomic,assign)   CGFloat width ;
@property(nonatomic,assign)   CGFloat height ;



@end


@interface UIView (animation)
-(void)animateFromAlpha:(float)fromAlpha toAlpha:(float)toAlpha duration:(NSTimeInterval)interval;
-(void)animateWithTransition:(UIViewAnimationTransition)transition duration:(NSTimeInterval)interval;
-(void)animateFromFrame:(CGRect)fromFrame toFrame:(CGRect)toFrame duration:(NSTimeInterval)interval;
-(void)animateFromPosition:(CGPoint)fromPosition toPosition:(CGPoint)toPosition duration:(NSTimeInterval)interval;
@end

