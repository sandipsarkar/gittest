//
//  LocationManager.h
//  RepVisitationTool
//
//  Created by Sandip Sarkar on 03/05/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

extern NSString *  LocationDidUpdate;
extern NSString *  LocationDidFail;

typedef void (^LocationDidUpdateCallback)(CLLocation *newLocation) ;

typedef void (^LocationDidFailCallback)(NSError *error) ;

@interface LocationManager : NSObject

@property (nonatomic,retain,readonly) CLLocationManager *locationManager;
@property (nonatomic,retain,readonly) CLLocation *currentLocation;
@property (nonatomic,assign) BOOL enableBackgroundTracking;
@property (nonatomic,readonly) BOOL isUpdatingLocation;

+(LocationManager *)sharedManager;
- (BOOL) startUpdatingLocation;
- (void) stopUpdatingLocation;
+(void)handleError:(NSError *)error;

-(void)setDistanceFilter:(CLLocationDistance)distanceFilter accuracy:(CLLocationAccuracy)accuracy;

-(BOOL)startMonitoringRegionForLocation:(CLLocation *)location;
-(void)resetLocation;

-(void)addLocationDidFailCallback:(LocationDidFailCallback)_locationDidFailCallback;
-(void)addLocationDidUpdateCallback:(LocationDidUpdateCallback)_locationDidUpdateCallback;

@end
