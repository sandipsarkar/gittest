//
//  LocationManager.m
//  RepVisitationTool
//
//  Created by Sandip Sarkar on 03/05/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "LocationManager.h"
#import "OfflineManager.h"

#define LOCATION_DISTANCE_FILTER 100

NSString  * LocationDidUpdate = @"SS_LocationDidUpdate";
NSString  * LocationDidFail   = @"SS_LocationDidFail"  ;

@interface LocationManager()<CLLocationManagerDelegate>
{
    BOOL showingErrorAlert;
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  

@property (nonatomic,retain,readwrite) CLLocationManager *locationManager;
@property (nonatomic,retain,readwrite) CLLocation *currentLocation;
@property (nonatomic,copy) LocationDidUpdateCallback locationDidUpdateCallback;
@property (nonatomic,copy) LocationDidFailCallback locationDidFailCallback;
@end

@implementation LocationManager
@synthesize locationManager;
@synthesize currentLocation;
@synthesize locationDidUpdateCallback,locationDidFailCallback;
@synthesize enableBackgroundTracking;
@synthesize isUpdatingLocation;

-(id)init
{
    self = [super init];
    
    if(self)
    {
        if(!locationManager)
        {
          locationManager = [[CLLocationManager alloc] init];
          locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation; //kCLLocationAccuracyHundredMeters;
		  locationManager.distanceFilter  = LOCATION_DISTANCE_FILTER;
          ///locationManager.pausesLocationUpdatesAutomatically = YES;
         // locationManager.activityType = CLActivityTypeAutomotiveNavigation;
        }
    }
    
    return self;
}

+(LocationManager *)sharedManager
{
    static LocationManager *_locationManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        _locationManager = [[self alloc] init];
        
#if TRACK_ERROR
        //BREADCRUMBS
        [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif
        
    });
    
    return _locationManager;
}



-(void)dealloc
{
    [locationManager release];
    [currentLocation release];
    
    [locationDidUpdateCallback release];
    [locationDidFailCallback release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}

-(void)setDistanceFilter:(CLLocationDistance)distanceFilter accuracy:(CLLocationAccuracy)accuracy
{
    self.locationManager.distanceFilter = distanceFilter;
    self.locationManager.desiredAccuracy = accuracy;
}


-(void)setEnableBackgroundTracking:(BOOL)_enableBackgroundTracking
{
    if(enableBackgroundTracking != _enableBackgroundTracking)
    {
        enableBackgroundTracking = _enableBackgroundTracking;
        
        if(enableBackgroundTracking)
        {
             [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
            
             [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
        }
        else
        {
             [[NSNotificationCenter defaultCenter] removeObserver:self];
        }
    }
}

-(void)addLocationDidFailCallback:(LocationDidFailCallback)_locationDidFailCallback
{
    self.locationDidFailCallback = _locationDidFailCallback;
}

-(void)addLocationDidUpdateCallback:(LocationDidUpdateCallback)_locationDidUpdateCallback
{
    self.locationDidUpdateCallback = _locationDidUpdateCallback;
}

-(void)showErrorAlert
{
    if(showingErrorAlert) return;
     showingErrorAlert = YES;
    
   // NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
   // NSString *alertTitle = [NSString stringWithFormat:@"Turn On Location Services to Allow \"%@\" to Determine Your Location",appName];
    
      UIAlertView *alert=  [UIAlertView alertViewWithTitle:LOCATION_SERVICE_DISABLED_HEADING message:LOCATION_SERVICE_DISABLED_MSG cancelButtonTitle:@"Ok" otherButtonTitles:nil onDismiss:^(int buttonIndex)
         {
             showingErrorAlert = NO;
         }
        onCancel:^{
                                   
            showingErrorAlert =NO;
                                   
        }];
    
    [alert release];
    
      // [UIAlertView showWarningAlertWithTitle:LOCATION_SERVICE_DISABLED_HEADING message:LOCATION_SERVICE_DISABLED_MSG];

}
#pragma mark stop StartUpdate location
- (BOOL)startUpdatingLocation
{
  //  if(isUpdatingLocation) return NO;
    
    BOOL started = NO;
    
  BOOL  locationServicesEnabled = [CLLocationManager locationServicesEnabled];

 CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
    
  BOOL locationServicesNotAuthorized = ((authorizationStatus != kCLAuthorizationStatusAuthorized) &&
    (authorizationStatus != kCLAuthorizationStatusNotDetermined));
     
	if (!locationServicesEnabled)
	{
        started = NO;
        
       // [self.locationManager startMonitoringSignificantLocationChanges];
       // [self.locationManager stopMonitoringSignificantLocationChanges];
        
         [self performSelector:@selector(showErrorAlert) withObject:nil afterDelay:0.1];
	}
    else if (locationServicesNotAuthorized)
    {
        started = NO;
        [self performSelector:@selector(showErrorAlert) withObject:nil afterDelay:0.1];
    }
	else
	{  
		if([[OfflineManager sharedManager] isNetworkAvailable])
        {
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startUpdatingLocation) object:nil];
            
            started = YES;
            
            [locationManager stopUpdatingLocation];
            [locationManager setDelegate:self];
            [locationManager startUpdatingLocation];
            isUpdatingLocation = YES;
        }
        else
        {
            isUpdatingLocation = NO;
            started = NO;
            
            [self performSelector:@selector(startUpdatingLocation) withObject:nil afterDelay:2.0];
          //  [UIAlertView showWarningAlertWithTitle:@"" message:@"Network not available."];
        }
	}
    
    self.enableBackgroundTracking = YES;
    
    return started;
}

#pragma mark Stop Update location
-(void)stopUpdatingLocation
{
	[locationManager stopUpdatingLocation];

    self.enableBackgroundTracking = NO;
    isUpdatingLocation = NO;
  
}

-(void)resetLocation
{
      self.currentLocation=nil;
}
#pragma mark

-(BOOL)startMonitoringRegionForLocation:(CLLocation *)location
{
    if (![CLLocationManager regionMonitoringAvailable]) return NO;
    
    // Check the authorization status
    if (([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized) &&
        ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusNotDetermined))
        return NO;
    
    // Clear out any old regions to prevent buildup.
    if ([self.locationManager.monitoredRegions count] > 0)
    {
        for (id obj in self.locationManager.monitoredRegions)
            [self.locationManager stopMonitoringForRegion:obj];
    }
    
    // so clamp the radius to the max value.
    CLLocationDegrees radius = LOCATION_DISTANCE_FILTER;
    if (radius > self.locationManager.maximumRegionMonitoringDistance)
    {
        radius = self.locationManager.maximumRegionMonitoringDistance;
    }
    
    NSString *identifier = [NSString stringWithFormat:@"%f, %f", location.coordinate.latitude, location.coordinate.longitude];
    
    // Create the region to be monitored.
    CLRegion* region = [[CLRegion alloc] initCircularRegionWithCenter:location.coordinate
                                                               radius:radius identifier:identifier];
    [self.locationManager startMonitoringForRegion:region];
    [region release];
    
    return YES;
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    // If it's a relatively recent event, turn off updates to save power
    CLLocation* location = [locations lastObject];
    
    //if (location.horizontalAccuracy < 0) return;
    
    NSDate* eventDate    = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    
    //if(abs(howRecent) > 15.0) return;
    
     if(abs(howRecent) <= 15.0)
     {
       // If the event is recent, do something with it.
       NSLog(@"latitude %+.8f, longitude %+.8f\n",
              location.coordinate.latitude,
              location.coordinate.longitude);
    
     if(self.currentLocation.coordinate.latitude!=location.coordinate.latitude || self.currentLocation.coordinate.longitude!=location.coordinate.longitude)
      {
         self.currentLocation = location;
         if(self.locationDidUpdateCallback) self.locationDidUpdateCallback(self.currentLocation);
         [[NSNotificationCenter defaultCenter] postNotificationName:LocationDidUpdate object:self.currentLocation userInfo:nil];
      }
    }

}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Error=%d",[error code]);
    
    //[self stopUpdatingLocation];
    
    isUpdatingLocation = NO;
    
    if(self.locationDidFailCallback) self.locationDidFailCallback(error);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:LocationDidFail object:nil userInfo:nil];
    
    if([error code] ==  kCLErrorDenied)
     {
         [self.locationManager stopUpdatingLocation];
     }
}


- (void)locationManager:(CLLocationManager *)manager
didStartMonitoringForRegion:(CLRegion *)region
{
    
}

- (void)locationManager:(CLLocationManager *)manager
         didEnterRegion:(CLRegion *)region
{
    
}

- (void)locationManager:(CLLocationManager *)manager
          didExitRegion:(CLRegion *)region
{
    
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
  
}


- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if(status==kCLAuthorizationStatusDenied)
    {
        [self performSelector:@selector(showErrorAlert) withObject:nil afterDelay:0.1];
    }
}

+(void)handleError:(NSError *)error
{
    
     NSString *msgHeading = nil;
     NSString *msgBody = nil;
     switch ([error code])
     {

     case kCLErrorLocationUnknown:
     {
    // msgHeading = MSG_GPS_DISABLED_HEADING;
    // msgBody = MSG_GPS_DISABLED;
     
     }
     break;
     
     // CL access has been denied (eg, user declined location use)
     case kCLErrorDenied:
     {
     msgHeading = LOCATION_SERVICE_DISABLED_HEADING;
     msgBody    = LOCATION_SERVICE_DISABLED_MSG;
     
     }
     break;
     
     // general, network-related error
     case kCLErrorNetwork:
     {
        //msgHeading = MSG_GPS_NETERR_HEADING;
        // msgBody = MSG_GPS_NETERR;
     
     }
     break;
     
     // heading could not be determined
     case kCLErrorHeadingFailure:
     {
        // msgHeading = MSG_GPS_ERROR_HEADING_HEADING;
        // msgBody = MSG_GPS_ERROR_HEADING;
     
     }
     break;
     
     // heading could not be determined
     case kCLErrorRegionMonitoringDenied:
     {
        // msgHeading = MSG_GPS_REGION_MONITORING_DENIED_HEADING;
        // msgBody = MSG_GPS_REGION_MONITORING_DENIED;
     
     }
     break;
     
     // registered region cannot be monitored
     case kCLErrorRegionMonitoringFailure:
     {
        // msgHeading = MSG_GPS_REGION_REGISTERED_HEADING;
        // msgBody = MSG_GPS_REGION_REGISTERED;
     
     }
     break;
     
     // CL could not immediately initialize region monitoring
     case kCLErrorRegionMonitoringSetupDelayed:
     {
      //  msgHeading = MSG_GPS_IMMEDIATE_REGION_ERROR_HEADING;
      //  msgBody = MSG_GPS_IMMEDIATE_REGION_ERROR;
     
     }
     break;
     
     default:
     {
      // msgHeading = MSG_GPS_DISABLED_HEADING;
      // msgBody = MSG_GPS_DISABLED;
     }
     break;
     }
    
    if(msgBody && msgHeading)
    {
       [UIAlertView showWarningAlertWithTitle:msgHeading message:msgBody];
    }

}

-(void)appDidEnterBackground
{
    [locationManager stopUpdatingLocation];
    
    if ([CLLocationManager significantLocationChangeMonitoringAvailable])
    {
        
        [self.locationManager startMonitoringSignificantLocationChanges];
    }
    else
    {
        NSLog(@"Significant location change monitoring is not available.");
    }
}

-(void)appDidBecomeActive
{
    if ([CLLocationManager significantLocationChangeMonitoringAvailable])
    {
        [self.locationManager stopMonitoringSignificantLocationChanges];
        
    }
    else
    {
        NSLog(@"Significant location change monitoring is not available.");
    }
    
    [self startUpdatingLocation];
}


- (id)retain {
    return self;
}
- (unsigned)retainCount
{
    return UINT_MAX;
}
- (oneway void)release {
    // never release
}
- (id)autorelease {
    return self;
}


@end
