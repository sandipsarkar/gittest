//
//  LoginViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "LoginViewController.h"
#import "ForgotPasswordControlller.h"
#import "Rep.h"
#import "DirectoryManager.h"
#import "WholesaleDealer.h"
#import "News.h"
#import "Offer.h"
#import "Customer.h"
#import "OfflineManager.h"

#import "SSCoreDataManager.h"


@interface LoginViewController()<UITextFieldDelegate>
{
    UITextField *userEmailTextField;
    UITextField *userPassTextField;
    
    UIImageView *loginFieldImageView;
    UIActivityIndicatorView *spinner;
    CGRect _originalFrame;
    
    UIButton *forgotPassButton;
}

-(void)reset;

@property(nonatomic,copy)NSString *userEmail;
@property(nonatomic,copy)NSString *userPassword;
@property (nonatomic,retain) ASIHTTPRequest *loginRequest;

@end

@implementation LoginViewController
@synthesize userEmail,userPassword;
@synthesize loginRequest;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        // Custom initialization
        //app=(AppDelegate *)[[UIApplication sharedApplication]delegate];
                            
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidAppear) name:UIKeyboardWillShowNotification object:nil];
        
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidDismiss) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}




-(void)dealloc
{
    [loginRequest clearDelegatesAndCancel];
    [loginRequest release];
    loginRequest  =nil;
    
    [spinner release];
    [loginFieldImageView release];
    
    [userEmail release];
    userEmail=nil;
    
    [userPassword release];
    userEmail=nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
     
     [super dealloc];
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

-(void)setupLoginPanel
{
    NSString *loginFieldImageName = @"login_field.png";
    
#if GENERIC_DEMO
    loginFieldImageName=@"login_field_demo.png";
#elif LIVE_TEST
    loginFieldImageName = @"login_field_test.png";
#else
    loginFieldImageName = @"login_field.png";
#endif
    
    UIImage *loginFieldImage=[UIImage imageNamedNoCache:loginFieldImageName];
   
    CGSize imageSize=loginFieldImage.size;
    if(!UIInterfaceOrientationIsLandscape(loginFieldImage.imageOrientation))
    {
        //imageSize=CGSizeMake(imageSize.height, imageSize.width);
        imageSize=CGSizeMake(375, 249);
    }
    
    //NSLog(@"%f %f",loginFieldImage.size.width,loginFieldImage.size.height);
    
   loginFieldImageView=[[UIImageView alloc] initWithImage:loginFieldImage];
    loginFieldImageView.frame=CGRectMake((CGRectGetHeight(self.view.frame)-imageSize.width)/2, (CGRectGetWidth(self.view.frame)-imageSize.height)/2, imageSize.width, imageSize.height);
    
    loginFieldImageView.backgroundColor=[UIColor clearColor];
    loginFieldImageView.contentMode=UIViewContentModeScaleAspectFit;
    loginFieldImageView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    // [loginFieldImageView setAutoresizingMasks];
    loginFieldImageView.userInteractionEnabled=YES;
    [self.view addSubview:loginFieldImageView];
    
    float _versionLabelWidth  = 70.0;
    float _versionLabelHeight = 20.0;
    
    UILabel* versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(285.0, 58.0, _versionLabelWidth, _versionLabelHeight)];
    versionLabel.text = [NSString stringWithFormat:@"version %@", APP_VERSION];
    versionLabel.font = [UIFont grotesqueFontOfSize:12.0];
    versionLabel.textAlignment = NSTextAlignmentRight;
    versionLabel.textColor = [UIColor whiteColor];
    versionLabel.backgroundColor = [UIColor clearColor];
    [loginFieldImageView addSubview:versionLabel];
    [versionLabel release];
    
#ifndef LIVE
    
    UILabel* envLabel = [[UILabel alloc] initWithFrame:CGRectMake(300.0, 35.0, 60, 20)];
    envLabel.font = [UIFont grotesqueFontOfSize:12.0];
    envLabel.textColor = [UIColor whiteColor];
    envLabel.backgroundColor = [UIColor clearColor];
    [loginFieldImageView addSubview:envLabel];
    
  #if LOCAL
    envLabel.text  = @"[LOCAL]";
  #elif LOCAL2
     envLabel.text = @"[LOCAL2]";
   #else
     envLabel.text = @" "; // @"[DEMO]";
   #endif

    [envLabel release];
    
#endif
   
    
    float _textFieldWidth=356;
    float _textFieldHeight=40;
    
    userEmailTextField=[UITextField textFieldWithFrame:CGRectMake(10, 111, _textFieldWidth, _textFieldHeight) font:14 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor]  text:nil borderStyle:UITextBorderStyleNone];
    userEmailTextField.tag=1;
    userEmailTextField.delegate=self;
    userEmailTextField.placeholder=@"Email";
    userEmailTextField.autocapitalizationType=UITextAutocapitalizationTypeNone;
    //[userEmailTextField setAutoresizingMasks];
    [loginFieldImageView addSubview:userEmailTextField];
    
    userPassTextField=[UITextField textFieldWithFrame:CGRectMake(10, CGRectGetMaxY(userEmailTextField.frame)+5, _textFieldWidth, _textFieldHeight) font:14 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor]  text:nil borderStyle:UITextBorderStyleNone];
    userPassTextField.tag=2;
    userPassTextField.delegate=self;
    userPassTextField.placeholder=@"Password";
    userPassTextField.autocapitalizationType=UITextAutocapitalizationTypeNone;
    userPassTextField.secureTextEntry=YES;
    [loginFieldImageView addSubview:userPassTextField];
    
    
    loginButton= [UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(6, CGRectGetMaxY(userPassTextField.frame)+2, 363, 44) bgImage:@"login_button.png" titleColor:nil target:self action:@selector(loginButtonAction:)];
    //[loginButton setAutoresizingMasks];
    [loginFieldImageView addSubview:loginButton];
    
    spinner=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.hidesWhenStopped=YES;
    //[spinner setAutoresizingMasks];
    [self.view addSubview:spinner];
    spinner.center=CGPointMake(loginFieldImageView.center.x, CGRectGetMaxY(loginFieldImageView.frame)+30);
    //[spinner startAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
 
    
    self.navigationController.navigationBarHidden=YES;
    
    self.view.backgroundColor=LOGIN_BG_COLOR;

    [self setupLoginPanel];
    
    UIImage *forgotPassImage = [UIImage imageNamedNoCache:@"forgotten_your_password.png"];
    
    forgotPassButton=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake((CGRectGetWidth(self.view.frame)-143)/2.0, CGRectGetHeight(self.view.frame)-50, forgotPassImage.size.width, forgotPassImage.size.height) bgImage:@"forgotten_your_password.png" titleColor:nil target:self action:@selector(forgotButtonAction:)];
    forgotPassButton.backgroundColor=[UIColor clearColor];
     //[forgotPassButton setAutoresizingMasks];
    forgotPassButton .autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:forgotPassButton];
    
    //forgotPassButton.hidden=YES;
    
}
                                                                                                                              
-(void)forgotButtonAction:(UIButton *)button
{
    ForgotPasswordControlller *forgotPassControler=[[ForgotPasswordControlller alloc] init];
    [self.navigationController pushViewController:forgotPassControler animated:YES];
    [forgotPassControler release];
    
}

-(void)loginButtonAction:(UIButton *)sender
{  
  
    BOOL shouldSendRequest=NO;
    
    for(UIView *subVw in loginFieldImageView.subviews)
    {
        if(![subVw isKindOfClass:[UITextField class]]) continue;
        
        UITextField *textField=(UITextField *)subVw;
        
        if([textField isFirstResponder])
            [textField resignFirstResponder];
        
        NSString *text=textField.text;
        if(textField.tag==1)
        {
            if(![text isValid] || ![text isValidEmail])
            {
                showAlert(EMAIL_ADDRESS_NOT_VALID);
                
                shouldSendRequest=NO;
                
                [textField becomeFirstResponder];
                
                break;
            }
            
            self.userEmail=text;
            shouldSendRequest=YES;
        }
        else if(textField.tag==2)
        {
            if(![text isValid])
            {
                showAlert(@"Invalid Password", @"The password entered is invalid. Please try again.");
                
                shouldSendRequest=NO;
                [textField becomeFirstResponder];
                
                break;
            }
            
            self.userPassword=text;
            shouldSendRequest=YES;
        }
    }
    
    if(!shouldSendRequest) return;
    
    
    
    [[sender superview] setUserInteractionEnabled:NO];
    forgotPassButton.userInteractionEnabled = NO;
    
    [spinner startAnimating];
    //now  send request for login.
    

    
    //NSLog(@"UserEmail:%@ UserPass:%@",self.userEmail,self.userPassword);
    
   ASIHTTPRequest *request = [ConnectionManager createLoginConnectionWithEmail:self.userEmail password:self.userPassword];
    request.delegate = self;
    self.loginRequest = request;
    
    /*
    [request setCompletionBlock:^{
        
        //NSData *str= request.responseData;
        NSDictionary *response = request.responseJSON;

        NSLog(@"Response : %@",request.responseString);

        NSDictionary *repDetailsDict = [response valueForKey:@"repdetails"];
        //app.loginDict=[response valueForKey:@"repdetails"];
        
        NSLog(@"repDetailsDict %@",repDetailsDict);
       // NSLog(@"login repDetailsDict %@",app.loginDict);

        if(repDetailsDict)
        {
                        
          //  app.strWdSiteId=[[repDetailsDict valueForKey:@"parentWdSite"]valueForKey:@"wdSiteID"];
          //  NSLog(@"%@",app.strWdSiteId);
            
                        
            NSString *lastLoginEmail = [[NSUserDefaults standardUserDefaults] objectForKey:LOGIN_EMAIL_KEY];
             /// ============ create login directory and setup persistent store coordinator ===========
            
            [[NSUserDefaults standardUserDefaults] setObject:self.userEmail forKey:LOGIN_EMAIL_KEY];
            [[NSUserDefaults standardUserDefaults] setObject:self.userPassword forKey:LOGIN_PASSWORD_KEY];
        
             if(!lastLoginEmail.length || ![lastLoginEmail isEqualToString:self.userEmail])
             {
                 [app setupCoreDataHandlerForloginEmail:self.userEmail];
             }
             
            Rep *lastRep = [CoreDataHandler currentRep];
            if(lastRep) [SSCoreDataManager deleteObject:lastRep];
            
            Rep *rep = [CoreDataHandler populateRepFromDict:repDetailsDict]; 
            rep.password = self.userPassword;
            if(!rep.email.length) rep.email = self.userEmail;
            
            NSLog(@"Rep object:%@",repDetailsDict);
                      

            
//            if([lastRep.repID isEqualToNumber:rep.repID])
//            {
//                rep.lastRequestedCustomersDate=lastRep.lastRequestedCustomersDate;
//                rep.lastRequestedNewsDate = lastRep.lastRequestedNewsDate;
//                rep.lastRequestedOffersDate = lastRep.lastRequestedOffersDate;
//                NSLog(@"last fetched date:%@",rep.lastRequestedCustomersDate);
//            }
            
            
            
            NSError*error = nil;
            [[SSCoreDataManager sharedManager] save:error];
            
            [self performSelector:@selector(didReceiveLoginResponse) withObject:nil];
        }
        else if([[response valueForKey:@"status"] intValue]==0)
        {
            [spinner stopAnimating];
            [[sender superview] setUserInteractionEnabled:YES];
            [UIAlertView showWarningAlertWithTitle:LOGIN_FAILED_HEADER message:INVALID_LOGIN_DETAILS_MSG];
            userPassTextField.text = nil;
        }
    }];
    [request setFailedBlock:^{
        
        NSError *error = [request error];
        NSLog(@"Error : %@",error);
        
          [spinner stopAnimating];
          [[sender superview] setUserInteractionEnabled:YES];
        
        //[ConnectionManager handleError:error];
        
        [UIAlertView showWarningAlertWithTitle:@"" message:NETWORK_NOT_AVAILABLE_MSG];
        
        userPassTextField.text = nil;
        
    }];
     
    */
    
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSDictionary *response = request.responseJSON;
    
    NSLog(@"Response : %@",request.responseString);
    
    response = NULL_NIL(response);
    
    NSNumber *isUnderMaintenance  = [response objectForKey:JSON_UNDER_MAINTENANCE_KEY];
    isUnderMaintenance = NULL_NIL(isUnderMaintenance);
    
    app.isServerUnderMaintenance = NO;
    
    if([isUnderMaintenance boolValue])
    {
        [app showUnderMaintainenceScreen:NO];
    }
    
    app.isServerUnderMaintenance = [isUnderMaintenance boolValue];
    
    if([isUnderMaintenance boolValue])
    {
       [spinner stopAnimating];
       [[loginButton superview] setUserInteractionEnabled:YES];
       forgotPassButton.userInteractionEnabled = YES;
       userPassTextField.text = nil;
    
       return;
    }
    
    
    NSDictionary *repDetailsDict = [response valueForKey:@"repdetails"];;
    
    // NSLog(@"login repDetailsDict %@",app.loginDict);
    
    if(repDetailsDict)
    {

        NSString *lastLoginEmail = [[NSUserDefaults standardUserDefaults] objectForKey:LOGIN_EMAIL_KEY];
        /// ============ Create login directory and setup persistent store coordinator ===========
        
        if(self.userEmail)
        [[NSUserDefaults standardUserDefaults] setObject:self.userEmail forKey:LOGIN_EMAIL_KEY];
        if(self.userPassword)
        [[NSUserDefaults standardUserDefaults] setObject:self.userPassword forKey:LOGIN_PASSWORD_KEY];
        
        if(!lastLoginEmail.length || ![lastLoginEmail isEqualToString:self.userEmail])
        {
            //[[OfflineManager sharedManager] saveToFile];
            [app setupCoreDataHandlerForloginEmail:self.userEmail];
            
            [[OfflineManager sharedManager] loadFromFile];
        }
        
        Rep *lastRep = [CoreDataHandler currentRep];
        if(lastRep) [SSCoreDataManager deleteObject:lastRep];
        
        Rep *rep = [CoreDataHandler populateRepFromDict:repDetailsDict];
        rep.password = self.userPassword;
        if(!rep.email.length) rep.email = self.userEmail;
        
        NSLog(@"Rep object:%@",repDetailsDict);
        
        /*
         if([lastRep.repID isEqualToNumber:rep.repID])
         {
         rep.lastRequestedCustomersDate=lastRep.lastRequestedCustomersDate;
         rep.lastRequestedNewsDate = lastRep.lastRequestedNewsDate;
         rep.lastRequestedOffersDate = lastRep.lastRequestedOffersDate;
         NSLog(@"last fetched date:%@",rep.lastRequestedCustomersDate);
         }
         */
        
        NSError*error = nil;
        [[SSCoreDataManager sharedManager] save:error];
        
        [self performSelector:@selector(didReceiveLoginResponse) withObject:nil];
    }
    else if([[response valueForKey:@"status"] intValue]==0)
    {
        [spinner stopAnimating];
        [[loginButton superview] setUserInteractionEnabled:YES];
        forgotPassButton.userInteractionEnabled = YES;
        [UIAlertView showWarningAlertWithTitle:LOGIN_FAILED_HEADER message:INVALID_LOGIN_DETAILS_MSG];
        userPassTextField.text = nil;
    }
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"Error : %@",[request error]);
    
    [spinner stopAnimating];
    [[loginButton superview] setUserInteractionEnabled:YES];
    forgotPassButton.userInteractionEnabled = YES;
    
    //[ConnectionManager handleError:error];
    
    [UIAlertView showWarningAlertWithTitle:@"" message:NETWORK_NOT_AVAILABLE_MSG];
    
    userPassTextField.text = nil;
}

-(void)didReceiveLoginResponse
{
    [spinner stopAnimating];
    [loginFieldImageView setUserInteractionEnabled:YES];
    forgotPassButton.userInteractionEnabled = YES;

    [app appDidLogIn];
    
    [self reset];
    
}

-(void)reset
{
    self.userPassword = nil;
    self.userEmail = nil;
    userEmailTextField.text = nil;
    userPassTextField.text  = nil;
}

-(void)_translateLoginPanelToTop
{
    if(CGRectEqualToRect(_originalFrame, CGRectZero))
        _originalFrame=loginFieldImageView.frame;
    
    CGRect _toFrame=_originalFrame;
    _toFrame.origin.y-=130;
    
    if(CGRectGetMinY(loginFieldImageView.frame)-CGRectGetMinY(_toFrame)>=130)
        [loginFieldImageView animateFromFrame:loginFieldImageView.frame toFrame:_toFrame duration:0.3];
}

-(void)_translateLoginPanelToOriginalPosition
{
    [loginFieldImageView animateFromFrame:loginFieldImageView.frame toFrame:_originalFrame duration:0.3];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text=[textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if(textField.text.length>text.length)
    {
        //FOR DELETE TEXT ACTION
        return YES;
    }
    else if(text.length)
    {
        if(textField==userEmailTextField)
        {
            return  [[string trimmedString] length]>0;
        }
        
        text=[text trimmedString];
    }
    
    return  text.length && text.length<=TEXT_LENGTH;
}


-(void)keyboardDidAppear
{
    [self _translateLoginPanelToTop];
}

-(void)keyboardDidDismiss
{
    [self _translateLoginPanelToOriginalPosition];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField 
{
  for(UITextField *_nextField in loginFieldImageView.subviews)
   {
       if(![_nextField isKindOfClass:[UITextField class]]) continue;
       
       if([_nextField isEqual:textField] || _nextField.text.length) continue;
       
       if([_nextField canBecomeFirstResponder])
       {
          // [textField resignFirstResponder];
            [_nextField becomeFirstResponder];
            break;
       }
   }
    
    if(textField.text.length)
    [textField resignFirstResponder];
    
    if(userEmailTextField.text.length && userPassTextField.text.length)
    {
        [self loginButtonAction:loginButton];
    }
    
   return YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
