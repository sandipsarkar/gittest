//
//  ForgotPasswordControlller.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 25/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "ForgotPasswordControlller.h"

@interface ForgotPasswordControlller()<UITextFieldDelegate>
{
    CGRect _originalFrame;
    UIImageView *loginFieldImageView;
    UIActivityIndicatorView *spinner;
}

@property (nonatomic,retain) ASIHTTPRequest *forgotPasswordRequest;

-(void)cancelButtonAction:(id)sender;
@end

@implementation ForgotPasswordControlller
@synthesize forgotPasswordRequest;

- (id)init
{
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidAppear) name:UIKeyboardWillShowNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidDismiss) name:UIKeyboardWillHideNotification object:nil];
        
    }
    return self;
}

-(void)dealloc
{
    [forgotPasswordRequest clearDelegatesAndCancel];
    [forgotPasswordRequest release];
    forgotPasswordRequest =nil;
    
    [loginFieldImageView release];
    loginFieldImageView=nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)setupForgotPassPanel
{
    UIImage *loginFieldImage=[UIImage imageNamedNoCache:@"forgotten_password_field.png"];
    
    CGSize imageSize=loginFieldImage.size;
    if(!UIInterfaceOrientationIsLandscape(loginFieldImage.imageOrientation))
    {
        imageSize=CGSizeMake(375, 205);
        //imageSize=CGSizeMake(imageSize.height, imageSize.width);
    }
    
    //NSLog(@"%f %f",loginFieldImage.size.width,loginFieldImage.size.height);
    
    loginFieldImageView=[[UIImageView alloc] initWithImage:loginFieldImage];
    loginFieldImageView.frame=CGRectMake((CGRectGetHeight(self.view.bounds)-imageSize.width)/2, (CGRectGetWidth(self.view.bounds)-imageSize.height)/2, imageSize.width, imageSize.height);
    
    loginFieldImageView.backgroundColor=[UIColor clearColor];
    loginFieldImageView.contentMode=UIViewContentModeScaleAspectFit;
   // [loginFieldImageView setAutoresizingMasks];
    loginFieldImageView.userInteractionEnabled=YES;
    [self.view addSubview:loginFieldImageView];
    
  
    float _textFieldWidth  = 358;
    float _textFieldHeight = 39;
    
    userEmailTextField=[UITextField textFieldWithFrame:CGRectMake(10.0, 98, _textFieldWidth, _textFieldHeight) font:14 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor]  text:nil borderStyle:UITextBorderStyleNone];
    userEmailTextField.tag=1;
    userEmailTextField.delegate=self;
    userEmailTextField.placeholder=@"Email";
    //[userEmailTextField setAutoresizingMasks];
    [loginFieldImageView addSubview:userEmailTextField];
    
    cancelButton= [UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(5, CGRectGetMaxY(userEmailTextField.frame)+2, 182.5, 44) bgImage:@"cancel_login_button.png" titleColor:nil target:self action:@selector(cancelButtonAction:)];
    //[sendPassButton setAutoresizingMasks];
    [loginFieldImageView addSubview:cancelButton];

    
   sendPassButton= [UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(CGRectGetMaxX(cancelButton.frame), CGRectGetMaxY(userEmailTextField.frame)+2, 182.5, 44) bgImage:@"send_password_button.png" titleColor:nil target:self action:@selector(sendPasswordAction:)];
    //[sendPassButton setAutoresizingMasks];
    [loginFieldImageView addSubview:sendPassButton];
    
        
    spinner=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.hidesWhenStopped=YES;
    [self.view addSubview:spinner];
    spinner.center=CGPointMake(loginFieldImageView.center.x, CGRectGetMaxY(loginFieldImageView.frame)+30);
}


-(void)sendPasswordAction:(UIButton *)sender
{
    NSString *text=userEmailTextField.text;
  
        if(![text isValid] || ![text isValidEmail])
        {
            showAlert(EMAIL_ADDRESS_NOT_VALID);
            
            [userEmailTextField becomeFirstResponder];
            
            return;
        }
    
    [[sender superview] setUserInteractionEnabled:NO];
    [userEmailTextField resignFirstResponder];
    [spinner startAnimating];
    
    //now  send request for login.
    
    NSLog(@"UserEmail:%@",userEmailTextField.text);
    
    sendPassButton.userInteractionEnabled = NO;
    cancelButton.userInteractionEnabled = NO;
    
   ASIHTTPRequest *request =  [ConnectionManager createForgotPasswordConnectionWithEmailID:userEmailTextField.text];
    request.delegate = self;
    self.forgotPasswordRequest = request;
    
    /*
    [request setCompletionBlock:^{
        
        NSDictionary * jsonResponse = request.responseJSON;
        
        NSLog(@"RESPONSE:%@",jsonResponse);
        
        NSNumber *status = [jsonResponse valueForKey:@"status"];
        
        [UIAlertView alertViewWithTitle:nil message: (status && [status boolValue]) ? MAIL_SENT_SUCCESSFULLY_TO_RESET_PWD : PLZ_ENTER_VALID_EMAIL_ADDRESS_MSG cancelButtonTitle:@"Ok" otherButtonTitles:nil onDismiss:^(int buttonIndex) 
        {
            
        } 
        onCancel:^{
            
            if(status && [status boolValue])
            [self cancelButtonAction:nil];
        }];
            
        [self performSelector:@selector(didReceiveResponse) withObject:nil];
    }];
    
    [request setFailedBlock:^{
        
       [self performSelector:@selector(didReceiveResponse) withObject:nil];
       // [ConnectionManager handleError:request.error];
        
        [UIAlertView alertViewWithTitle:CONN_NOT_AVBLE_HEADER message:@"" cancelButtonTitle:@"Ok"];
        
    }];
    */
    
    //[self performSelector:@selector(didReceiveResponse) withObject:nil afterDelay:1];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSDictionary * jsonResponse = request.responseJSON;
    
    NSLog(@"RESPONSE:%@",jsonResponse);
    

    NSNumber *isUnderMaintenance  = [jsonResponse objectForKey:JSON_UNDER_MAINTENANCE_KEY];
    isUnderMaintenance = NULL_NIL(isUnderMaintenance);
    
     app.isServerUnderMaintenance =NO;
    if([isUnderMaintenance boolValue])
    {
        [app showUnderMaintainenceScreen:NO];
    }
    
    app.isServerUnderMaintenance = [isUnderMaintenance boolValue];
    
     if([isUnderMaintenance boolValue])
     {
       [self performSelector:@selector(didReceiveResponse) withObject:nil];
    
        return;
     }

    
    NSNumber *status = [jsonResponse valueForKey:@"status"];
    
    [UIAlertView alertViewWithTitle:nil message: (status && [status boolValue]) ? MAIL_SENT_SUCCESSFULLY_TO_RESET_PWD : PLZ_ENTER_VALID_EMAIL_ADDRESS_MSG cancelButtonTitle:@"Ok" otherButtonTitles:nil onDismiss:^(int buttonIndex)
     {
         
     }
        onCancel:^{
                               
                if(status && [status boolValue])
                [self cancelButtonAction:nil];
        }];
    
    [self performSelector:@selector(didReceiveResponse) withObject:nil];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [self performSelector:@selector(didReceiveResponse) withObject:nil];
    // [ConnectionManager handleError:request.error];
    
    [UIAlertView alertViewWithTitle:NETWORK_NOT_AVAILABLE_MSG message:@"" cancelButtonTitle:@"Ok"];
}


-(void)cancelButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)didReceiveResponse
{
    sendPassButton.userInteractionEnabled = YES;
    cancelButton.userInteractionEnabled = YES;
    [spinner stopAnimating];
    [loginFieldImageView setUserInteractionEnabled:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.navigationController.navigationBarHidden=YES;
    self.view.backgroundColor=LOGIN_BG_COLOR;
    
    [self setupForgotPassPanel];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text=[textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if(textField.text.length>text.length)
    {
        //FOR DELETE TEXT ACTION
        return YES;
    }
    else if(text.length)
    {
        return  [[string trimmedString] length]>0;
        
        //text=[text trimmedString];
    }
    
    
    return  text.length && text.length<=TEXT_LENGTH;
}

-(void)_translateLoginPanelToTop
{
    if(CGRectEqualToRect(_originalFrame, CGRectZero))
        _originalFrame=loginFieldImageView.frame;
    
    CGRect _toFrame=_originalFrame;
    _toFrame.origin.y-=130;
    
    if(CGRectGetMinY(loginFieldImageView.frame)-CGRectGetMinY(_toFrame)>=130)
        [loginFieldImageView animateFromFrame:loginFieldImageView.frame toFrame:_toFrame duration:0.3];
}

-(void)_translateLoginPanelToOriginalPosition
{
    [loginFieldImageView animateFromFrame:loginFieldImageView.frame toFrame:_originalFrame duration:0.3];
}


-(void)keyboardDidAppear
{
    [self _translateLoginPanelToTop];
}

-(void)keyboardDidDismiss
{
    [self _translateLoginPanelToOriginalPosition];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
