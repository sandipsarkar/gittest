//
//  OrganizeTasksListViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 22/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TasksViewController.h"

typedef void(^OrganiseTasksDidLoad)(void) ;
@interface OrganizeTasksListViewController : TasksViewController

-(void)addOrganiseTasksDidLoadCallback:(OrganiseTasksDidLoad)callback;

-(void)refresh;

@end
