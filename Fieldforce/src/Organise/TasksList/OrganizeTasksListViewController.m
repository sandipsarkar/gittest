//
//  OrganizeTasksListViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 22/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "OrganizeTasksListViewController.h"
#import "TasksListViewController.h"
#import "Task.h"

@interface OrganizeTasksListViewController()
{
   
}

@property(nonatomic,copy)OrganiseTasksDidLoad organiseTasksDidLoadCallback; 


@end

@implementation OrganizeTasksListViewController
@synthesize organiseTasksDidLoadCallback;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)dealloc
{
    if(organiseTasksDidLoadCallback)
        [organiseTasksDidLoadCallback release];
    
    [super dealloc];
}

-(void)addOrganiseTasksDidLoadCallback:(OrganiseTasksDidLoad)callback
{
    if(callback)
        self.organiseTasksDidLoadCallback = callback;
}

#pragma mark - View lifecycle

-(BOOL)shouldShowCustomerProfile
{
    return YES;
}

-(BOOL)allowCustomerList
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.backgroundColor = [UIColor whiteColor];

     
    //UIImageView *headerView = [self pageHeaderViewForImageName:@"organise_tasks_heading.png"];
    //[self.taskListViewController tableView].tableHeaderView=headerView;
    
   // [self.taskListViewController setupTableHeaderViewWithImage:@"organise_tasks_heading.png"];
    //[self fetchTasks];
}

-(void)loadTasks
{
    if(_isFetching) return;
    
    _isFetching = YES;
    
    [Task fetchAsynWithPredicate:nil onAttributes:0 sortDescriptors:nil limit:0 startIndex:0 completion:^(NSArray *result, NSError *error) 
     {
         if(result)
         {
             NSMutableArray *tasks=[NSMutableArray arrayWithArray:result];
             self.taskListViewController.tasks=tasks;
         }
          _isFetching = NO;
         if(self.organiseTasksDidLoadCallback) self.organiseTasksDidLoadCallback();
     }];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



- (void)viewWillAppear:(BOOL)animated
{
  //  [super viewWillAppear:animated];
    
   [self loadTasks];
    
}

/*
-(void)newTaskDidAdd:(id)task
{
   // NSString *taskId=[task valueForKey:@"CUSTOMER_ID"];

    [self fetchTasks];
}
 */

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    ParallelStackViewController *stackVC = [self parallelStackViewController];
    
    if(stackVC.detailController)
    {
     [stackVC hideDetailController:stackVC.detailController  animated:NO];
    }
    
    if(self.organiseTasksDidLoadCallback) self.organiseTasksDidLoadCallback=nil;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


-(void)refresh
{
  
    [self.taskListViewController refresh];
}


#pragma mark - Table view data source


@end
