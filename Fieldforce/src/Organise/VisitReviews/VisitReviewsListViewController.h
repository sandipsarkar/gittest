//
//  VisitReviewsListViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReviewsDetailViewController,SSSegmentedControl;
@interface VisitReviewsListViewController : UIViewController<UISearchBarDelegate>

@property(nonatomic,readonly,retain)ReviewsDetailViewController *reviewsDetailViewController;
@property (nonatomic,retain)NSMutableArray *visits;
@property(nonatomic,retain) UITableView *tableView;

//-(void)filterTabDidSelect:(NSInteger)selectedIndex;
-(void)filterTab:(SSSegmentedControl *)tab didSelectAtIndex:(NSInteger)selectedIndex;

-(void)refresh;

@end
