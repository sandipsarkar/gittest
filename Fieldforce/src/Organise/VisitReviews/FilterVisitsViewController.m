//
//  FilterVisitsViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 20/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "FilterVisitsViewController.h"

@interface FilterVisitsViewController ()
{
    NSMutableDictionary *_setupDict;
     NSMutableArray *selectedIndexPaths;
}

@property(nonatomic,copy)FilterVisitCallback filterVisitCallback;
@property(nonatomic,copy)FilterVisitCancelCallback filterVisitCancelCallback;

@property(nonatomic,retain) NSIndexPath *selectedOrderIndexPath;
@property(nonatomic,retain) NSIndexPath *selectedTagIndexPath;
@property(nonatomic,retain) NSIndexPath *selectedSortIndexPath;

@end

@implementation FilterVisitsViewController
@synthesize filterVisitCallback,filterVisitCancelCallback;
@synthesize selectedOrderIndexPath,selectedTagIndexPath,selectedSortIndexPath;
@synthesize isFiltering;
@synthesize delegate;

- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) 
    {
        // Custom initialization
        
       
    }
    return self;
}


-(void)initialize
{
    if(!selectedIndexPaths) 
    {
        selectedIndexPaths=[[NSMutableArray alloc]init];
    }
    else 
    {
        [selectedIndexPaths removeAllObjects];
    }
    

    selectedSortIndexPath=[[NSIndexPath indexPathForRow:0 inSection:0] retain];
    selectedTagIndexPath=[[NSIndexPath indexPathForRow:0 inSection:1] retain];
    selectedOrderIndexPath=[[NSIndexPath indexPathForRow:0 inSection:2] retain];
    
    [selectedIndexPaths addObject:selectedSortIndexPath];
    [selectedIndexPaths addObject:selectedTagIndexPath];
    [selectedIndexPaths addObject:selectedOrderIndexPath];
    
    [self.tableView reloadData];
}


-(void)dealloc
{
    delegate = nil;
    [filterVisitCallback release];
    [filterVisitCancelCallback release];
    
    [_setupDict release];
    
    [selectedOrderIndexPath release];
    [selectedTagIndexPath release];
    [selectedSortIndexPath release];
    [selectedIndexPaths release];
   
    
    [super dealloc];
}

-(void)setupDict
{
    if(!_setupDict)
        _setupDict=[[NSMutableDictionary alloc]init];
    
    [_setupDict setObject:[NSArray arrayWithObjects:@"Customer Name",@"Visit Type", nil] forKey:@"0_SECTION_ITEMS"];

    NSArray *connectedOems=[CoreDataHandler connectedOEMS];
    NSArray *oemNames = [connectedOems valueForKey:@"name"];
    NSMutableArray *tagsArr = [NSMutableArray array];
    if([oemNames count])
    {
        [tagsArr addObjectsFromArray:oemNames];
        //============= SORT  ALPHABETICALLY ====================
        [tagsArr sortUsingSelector:@selector(caseInsensitiveCompare:)];
    }
    
    //INSERT all at index 0
    [tagsArr insertObject:@"All" atIndex:0];
    
    //[NSArray arrayWithObjects:@"All",@"Subaru",@"Suzuki",@"Honda", nil]
    [_setupDict setObject:tagsArr forKey:@"1_SECTION_ITEMS"];
    
    [_setupDict setObject:[NSArray arrayWithObjects:@"Ascending",@"Descending",nil] forKey:@"2_SECTION_ITEMS"];
    
    [_setupDict setObject:@"SORT BY" forKey:@"0_SECTION_TITLE"];
    [_setupDict setObject:@"TAGS"  forKey:@"1_SECTION_TITLE"];
    [_setupDict setObject:@"ORDER"  forKey:@"2_SECTION_TITLE"];
 
}

-(void)addFilterVisitCallback:(FilterVisitCallback)callback
{
    self.filterVisitCallback = callback;
}
-(void)addFilterVisitCancelCallback:(FilterVisitCancelCallback)callback
{
    self.filterVisitCancelCallback = callback;
}

-(void)setupFooterView
{
    UIView *tableFooterView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 59)];
    
    UIButton *cancelButton = [UIButton repToolButtonWithText:@"RESET"];
    CGRect cancelFrame = cancelButton.frame;
    cancelFrame.origin.y = 2.0;
    cancelFrame.origin.x = 8.0f;
    cancelButton.frame = cancelFrame;
    cancelButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    cancelButton.tag=1;
    cancelButton.titleLabel.font = [UIFont subaruBoldFontOfSize:20.0];
    [tableFooterView addSubview:cancelButton];
    cancelButton.autoresizingMask =UIViewAutoresizingFlexibleRightMargin;
    [cancelButton addTarget:self action: @selector(resetButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *filterButton = [UIButton repToolButtonWithText:@"CONFIRM"];
    CGRect filterFrame = filterButton.frame;
    filterFrame.origin.y = 2.0;
    filterFrame.origin.x = CGRectGetMaxX(cancelButton.frame)+5.0;
    filterButton.frame = filterFrame;
    filterButton.titleLabel.font = [UIFont subaruBoldFontOfSize:20.0];
    filterButton.autoresizingMask=UIViewAutoresizingFlexibleTopMargin;
    [tableFooterView addSubview:filterButton];
    [filterButton addTarget:self action: @selector(filterButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.tableView.tableFooterView=tableFooterView;
    [tableFooterView release];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
   if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
   {
       [self setupTableHeaderView];
       self.tableView.sectionFooterHeight = 10.0;
       self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
   }
    
    [self setupFooterView];
    
    [self setupDict];
    [self initialize];
    
    self.tableView.bounces = NO;

}

-(void)setupTableHeaderView
{
    CGRect frame = self.tableView.tableHeaderView.frame;
    frame.size.height = 10.0;
    UIView *headerView = [[UIView alloc] initWithFrame:frame];
    self.tableView.tableHeaderView = headerView;
    [headerView release];
}


-(void)resetButtonAction:(UIButton *)button
{
    isFiltering=NO;
    [self initialize];
    
    if(self.filterVisitCancelCallback)
    self.filterVisitCancelCallback();
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(filterVisitsViewControllerDidCancel:)])
    {
        [self.delegate filterVisitsViewControllerDidCancel:self];
    }

}

-(void)filterButtonAction:(UIButton *)button
{
    isFiltering = YES;
    NSMutableArray *customerSortbyKeyArr = [NSMutableArray array];
    NSMutableArray *tagsArr = [NSMutableArray array];
    NSNumber *ascending=nil;
    
    
    if(self.selectedSortIndexPath)
    {
        switch (selectedSortIndexPath.row) 
        {
            case 0:
                [customerSortbyKeyArr addObject:@"name"];
            break;
            case 1:
               // [customerSortbyKeyArr addObject:@"natureOfVisitID"];
            break;
                
            default: break;
        }
    }
    
    if(self.selectedTagIndexPath)
    {
        if(selectedTagIndexPath.row!=0)
        {
            NSArray *_tags          =  [_setupDict objectForKey:@"1_SECTION_ITEMS"];
            NSString *seletedTag    =  [_tags objectAtIndex:selectedTagIndexPath.row];
            NSInteger  seletedTagID =  [CoreDataHandler oemIDForOEMName:seletedTag];;
            [tagsArr addObject:[NSString stringWithFormat:@"%d",seletedTagID]];
        }
    }

    
    if(self.selectedOrderIndexPath)
    {
        ascending = [NSNumber numberWithBool:(self.selectedOrderIndexPath.row==0)];
    }

    NSDictionary *filterDict = [[NSDictionary alloc]initWithObjectsAndKeys:customerSortbyKeyArr,@"SORT",tagsArr,@"TAGS",ascending,@"ASCENDING",nil];

    if(self.filterVisitCallback)
    self.filterVisitCallback(filterDict);
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(filterVisitsViewController:didFilter:)])
    {
        [self.delegate filterVisitsViewController:self didFilter:filterDict];
    }

    [filterDict release];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *items=[_setupDict objectForKey:[NSString stringWithFormat:@"%d_SECTION_ITEMS",section]];
    
    return [items count];
}

/*
 -(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
 {
 return [_setupDict objectForKey:[NSString stringWithFormat:@"%d_SECTION_TITLE",section]];
 }
 */
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSString *title =[_setupDict objectForKey:[NSString stringWithFormat:@"%d_SECTION_TITLE",section]];
    
    return (title.length)?40.0:0;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *headerHeight = @"Header_cell";
    NSString *title =[_setupDict objectForKey:[NSString stringWithFormat:@"%d_SECTION_TITLE",section]];
    
    if(!title.length) return nil;
    
   UILabel *headerLabel  = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headerHeight];
    
    if(!headerLabel)
    {
     headerLabel   =  [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 40.0)] autorelease];
     headerLabel.backgroundColor =  [UIColor clearColor];
     headerLabel.textColor       =  [UIColor blackColor];
     headerLabel.font            =  [UIFont subaruBoldFontOfSize:23.0];
     headerLabel.shadowColor     =  [UIColor lightGrayColor];
     headerLabel.shadowOffset    =  CGSizeMake(1.0, 1.0);
    }
    
    headerLabel.text            =  [NSString stringWithFormat:@"    %@",title];
    
    return headerLabel;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cell.textLabel.font = [UIFont grotesqueFontOfSize:16.0];
    }
    
    NSArray *items=[_setupDict objectForKey:[NSString stringWithFormat:@"%d_SECTION_ITEMS",indexPath.section]];
    
    id obj= (indexPath.row<[items count]) ?  [items objectAtIndex:indexPath.row] : @"";
    
    NSString *title=([obj isKindOfClass:[NSDictionary class]]) ? [obj valueForKey:@"NAME"] : (NSString *)obj;
    cell.textLabel.text = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [NSString stringWithFormat:@"  %@",title] : title;
    
    BOOL selected=[selectedIndexPaths containsObject:indexPath];
    
    cell.accessoryType=selected?UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone;
    //cell.textLabel.textColor=selected?LOGIN_BG_COLOR:[UIColor blackColor];
    cell.textLabel.textColor=[UIColor blackColor];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor groupTableViewBackgroundColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.tag = 1;
    
    
    NSInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *indexPathsToReload = [NSMutableArray array];
    
    switch (indexPath.section) 
    {
        case 0:
        {
            if([self.selectedSortIndexPath isEqual:indexPath]) return;
            
            if(self.selectedSortIndexPath) 
            {
                [indexPathsToReload addObject:self.selectedSortIndexPath];
                [selectedIndexPaths removeObject:self.selectedSortIndexPath];
            }
            
            self.selectedSortIndexPath = indexPath;
            [indexPathsToReload addObject:indexPath];
            [selectedIndexPaths addObject:indexPath];
        }
        break;
            
        case 1:
        {
            if([self.selectedTagIndexPath isEqual:indexPath]) return;
            
            if(self.selectedTagIndexPath) 
            {
                [indexPathsToReload addObject:self.selectedTagIndexPath];
                [selectedIndexPaths removeObject:self.selectedTagIndexPath];
            }
            
            self.selectedTagIndexPath = indexPath;
            [indexPathsToReload addObject:indexPath];
            [selectedIndexPaths addObject:indexPath];
        }
        break;
            
        case 2:
        {
            if([self.selectedOrderIndexPath isEqual:indexPath]) return;
            
            if(self.selectedOrderIndexPath) 
            {
                [indexPathsToReload addObject:self.selectedOrderIndexPath];
                [selectedIndexPaths removeObject:self.selectedOrderIndexPath];
            }
            
            self.selectedOrderIndexPath = indexPath;
            [indexPathsToReload addObject:indexPath];
            [selectedIndexPaths addObject:indexPath];
        }
        break;
    }

    
    if(indexPathsToReload)
    [tableView reloadRowsAtIndexPaths:indexPathsToReload withRowAnimation:UITableViewRowAnimationFade];
}



@end
