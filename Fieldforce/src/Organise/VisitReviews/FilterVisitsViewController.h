//
//  FilterVisitsViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 20/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^FilterVisitCallback) (NSDictionary *filterOptions);
typedef void (^FilterVisitCancelCallback) (void);

@protocol FilterVisitDelegate;
@interface FilterVisitsViewController : UITableViewController

@property(nonatomic,readonly) BOOL isFiltering;
@property(nonatomic,assign) id < FilterVisitDelegate > delegate;

-(void)addFilterVisitCallback:(FilterVisitCallback)callback;
-(void)addFilterVisitCancelCallback:(FilterVisitCancelCallback)callback;

-(void)initialize;

@end

@protocol FilterVisitDelegate <NSObject>

@optional

-(void)filterVisitsViewController:(FilterVisitsViewController *)filterVisitVC didFilter:(NSDictionary *)filterOptions;
-(void)filterVisitsViewControllerDidCancel:(FilterVisitsViewController *)filterVisitVC;

@end

