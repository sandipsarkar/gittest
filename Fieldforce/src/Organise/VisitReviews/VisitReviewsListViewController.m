//
//  VisitReviewsListViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "VisitReviewsListViewController.h"
#import "Customer.h"
#import "Visit.h"
#import "ReviewsDetailViewController.h"
#import "ParallelStackViewController.h"
#import "SSSegmentedControl.h"
#import "FilterViewController.h"
#import "FilterVisitsViewController.h"
#import "InsetTableViewCell.h"

@interface VisitReviewsListViewController()<UITableViewDataSource,UITableViewDelegate,FilterVisitDelegate,InsetTableViewCellDelegate>
{
    BOOL isReviewed;
    UITableView *_tableView;
    BOOL _isRefreshing;
}

@property(nonatomic,readwrite,retain)ReviewsDetailViewController *reviewsDetailViewController;
@property (nonatomic,retain) NSMutableArray *searchArray;
@property (nonatomic,retain) UIPopoverController *filterPopover;
@property (nonatomic,retain) NSArray *sortDescriptors;

@property (nonatomic,retain) UIButton *filterButton;

@property (nonatomic,retain) NSPredicate *filterPredicate;
@property (nonatomic,retain) NSPredicate *searchPredicate;

-(void)filterForIndex:(NSInteger)selectedIndex fromArray:(NSMutableArray *)arr;

@end


@implementation VisitReviewsListViewController
@synthesize reviewsDetailViewController;
@synthesize visits;
@synthesize searchArray;
@synthesize filterPopover;
@synthesize tableView = _tableView;
@synthesize sortDescriptors;
@synthesize filterButton;

@synthesize filterPredicate,searchPredicate;

-(void)dealloc
{
    [searchArray release];
    [visits release];
    [reviewsDetailViewController release];
    [filterPopover release];
    [_tableView release];
    [sortDescriptors release];
    [filterPredicate release] ;
    [searchPredicate release] ;
    [filterButton release];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}

- (id)init
{
    //self = [super initWithStyle:UITableViewStyleGrouped];
    self = [super init];
    if (self)
    {
          ;
        
    }
    return self;
}

-(void)reloadData
{
    
    NSMutableArray *customersArr = [CoreDataHandler allCustomers];
   // NSMutableArray *visitsArr   = [CoreDataHandler allVisits];
    
    for(Customer *_customer in customersArr)
    {
        if(_customer.visits.count>0 && ![self.visits containsObject:_customer])
        {
            [self.visits addObject:_customer];
        }
    }
    
    
    {
        //[self filterTab:nil didSelectAtIndex:isReviewed?0:1];
        
    }
    
    
    [self.tableView reloadData];

}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.clipsToBounds = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:VisitReviewDidUpdate object:nil];
    

    UIImageView *imageView = [self pageHeaderViewForImageName:@"visit_reviews_heading.png"];
    [self.view addSubview:imageView];
    imageView.backgroundColor = [UIColor whiteColor];
    
    //self.tableView.tableHeaderView = imageView;
    
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(imageView.frame), CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-CGRectGetMaxY(imageView.frame)) style:UITableViewStyleGrouped];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    _tableView.delegate=self;
    _tableView.dataSource=self;
    //_tableView.backgroundColor   =   [UIColor clearColor];
    _tableView.separatorColor = [UIColor lightGrayColor];
    _tableView.rowHeight=60;
    _tableView.bounces=NO;
    [self.view addSubview:_tableView];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_tableView.bounds), 10.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        _tableView.tableHeaderView = _headerView;
        [_headerView release];
        
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }

    visits = [[NSMutableArray alloc] init];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //Need To refractor the logic.
   // [self reloadData];
   
}

-(void)refresh
{
    if(_isRefreshing) return;
    
    _isRefreshing = YES;
    
    self.searchPredicate = nil;
    self.filterPredicate = nil;
    self.filterPopover=nil;
    self.filterButton.selected = NO;
    
    
    [self reloadData];
    [self filterTab:nil didSelectAtIndex:isReviewed?0:1];
    
    _isRefreshing = NO;
}

#pragma mark filter Action
-(void)filterAction:(UIButton * )sender
{
    self.filterButton = sender;
    
    UIPopoverController *popovervc = self.filterPopover;
    
    if(!popovervc)
    {
        FilterVisitsViewController *filterViewcontroller = [[FilterVisitsViewController alloc]init];
        filterViewcontroller.delegate = self;
        
        //UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:filterViewcontroller];
        filterViewcontroller.contentSizeForViewInPopover=CGSizeMake(350, 590);
        
        
        popovervc= [[UIPopoverController alloc] initWithContentViewController:filterViewcontroller];
                    //presentInPopOverFrom:sender inView:self.view animated:YES];
        self.filterPopover=popovervc;
        [popovervc release];
        [filterViewcontroller release];
    }
    
        /*
        [filterViewcontroller addFilterVisitCallback:^(NSDictionary *filterOptions) 
         {
             sender.selected = YES;
             //  
             
             NSArray *sortKeys = [filterOptions valueForKey:@"SORT"];
             BOOL isAscending  = [[filterOptions valueForKey:@"ASCENDING"] boolValue];
             NSArray *oemTags  = [filterOptions valueForKey:@"TAGS"];
             NSMutableArray *_sortDescriptors = [NSMutableArray array];
             
             NSPredicate *_filterPredicte=nil;
             NSMutableArray *predicates=[NSMutableArray array];
             if([oemTags count])
             {
                 NSString *oemID=[oemTags objectAtIndex:0];
                 NSPredicate *oemPredicate = [NSPredicate predicateWithFormat:@"(self.dealingOEMs != nil) && self.dealingOEMs contains[c] %@",oemID];
                 [predicates addObject:oemPredicate];
             }
             
             for(NSString *sortByKey in sortKeys)
             {
                 NSSortDescriptor *_sortDescriptor=[[NSSortDescriptor alloc] initWithKey:sortByKey ascending:isAscending];
                 [_sortDescriptors addObject:_sortDescriptor];
                 self.sortDescriptors = _sortDescriptors;
                 [_sortDescriptor release];
             }
             
             if([predicates count])
                _filterPredicte = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
             
             self.filterPredicate = _filterPredicte;
             
             [self filterTab:nil didSelectAtIndex:isReviewed?0:1];
             
             [popovervc dismissPopoverAnimated:YES];
         }];  
        
        [filterViewcontroller addFilterVisitCancelCallback:^{
            
                self.filterPredicate=nil;
                self.sortDescriptors=nil;
                [self filterTab:nil didSelectAtIndex:isReviewed?0:1];

            sender.selected = NO;
            [popovervc dismissPopoverAnimated:YES];
            self.filterPopover=nil;
        }];
        
        [filterViewcontroller release];
        //[navController release];
    }
    */
    /*
    else 
    {
        FilterVisitsViewController *filterViewcontroller =(FilterVisitsViewController *)popovervc.contentViewController;
        
        if(!filterViewcontroller.isFiltering) [filterViewcontroller initialize];
    }
    */
    
    CGRect  targetRect =[sender frame] ;// [self.view.superview convertRect:[sender frame] toView:self.view];
    
    [popovervc presentPopoverFromRect:targetRect inView:[sender superview] permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(void)filterVisitsViewController:(FilterVisitsViewController *)filterVisitVC didFilter:(NSDictionary *)filterOptions
{
    self.filterButton.selected = YES;
    
    NSArray *sortKeys = [filterOptions valueForKey:@"SORT"];
    BOOL isAscending  = [[filterOptions valueForKey:@"ASCENDING"] boolValue];
    NSArray *oemTags  = [filterOptions valueForKey:@"TAGS"];
    NSMutableArray *_sortDescriptors = [NSMutableArray array];
    
    NSPredicate *_filterPredicte=nil;
    NSMutableArray *predicates=[NSMutableArray array];
    if([oemTags count])
    {
        NSString *oemID=[oemTags objectAtIndex:0];
        NSPredicate *oemPredicate = [NSPredicate predicateWithFormat:@"(self.dealingOEMs != nil) && self.dealingOEMs contains[c] %@",oemID];
        [predicates addObject:oemPredicate];
    }
    
    for(NSString *sortByKey in sortKeys)
    {
        NSSortDescriptor *_sortDescriptor=[[NSSortDescriptor alloc] initWithKey:sortByKey ascending:isAscending];
        [_sortDescriptors addObject:_sortDescriptor];
        self.sortDescriptors = _sortDescriptors;
        [_sortDescriptor release];
    }
    
    if([predicates count])
        _filterPredicte = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    
    self.filterPredicate = _filterPredicte;
    
    [self filterTab:nil didSelectAtIndex:isReviewed?0:1];
    [self.filterPopover dismissPopoverAnimated:YES];

}
-(void)filterVisitsViewControllerDidCancel:(FilterVisitsViewController *)filterVisitVC
{
    self.filterPredicate=nil;
    self.sortDescriptors=nil;
    [self filterTab:nil didSelectAtIndex:isReviewed?0:1];
    
    self.filterButton.selected = NO;
    [self.filterPopover dismissPopoverAnimated:YES];
    self.filterPopover=nil;
    
    /*
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     
     [self filterForIndex:isReviewed?0:1 fromArray:self.visits];
     
     dispatch_async(dispatch_get_main_queue(), ^{
     
     [self.tableView reloadData];
     
     [popovervc dismissPopoverAnimated:YES];
     self.filterPopover=nil;
     });
     
     });
     
     */
}


-(void)filterTab:(SSSegmentedControl *)tab didSelectAtIndex:(NSInteger)selectedIndex
{
  dispatch_async(dispatch_get_main_queue(), ^{
      

    if(![self.visits count])
    {
        [self reloadData];
    }
    
    NSMutableArray *_arr = [NSMutableArray arrayWithArray:self.visits];
    
    if(self.filterPredicate && self.searchPredicate)
    {
           ;
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:self.filterPredicate,self.searchPredicate, nil]];
        
        [_arr filterUsingPredicate:predicate];
    }
    else if(self.filterPredicate)
    {
           
        [_arr filterUsingPredicate:self.filterPredicate];
    }
    else if(self.searchPredicate)
    {
        [_arr filterUsingPredicate:self.searchPredicate];
    }
    

    
    if(!self.searchArray)
        self.searchArray=[NSMutableArray array];
    
    switch (selectedIndex) 
    {
        case 0://Reviewed
        {
            isReviewed=YES;
  
            
            NSMutableArray *arr=[[NSMutableArray alloc] init];
            for(Customer *_customer in _arr)
            {
                NSPredicate *_filterPredicate=[NSPredicate predicateWithFormat:@"self.isReportCompleted=%@",[NSNumber numberWithBool:YES]];
                
               NSSet *_filtered = [_customer.visits filteredSetUsingPredicate:_filterPredicate];
                
                if(_filtered.count)
                {
                    [arr addObject:_customer];
                   _customer.completeVisitCount=[NSNumber numberWithInteger:_filtered.count];
                }
            }
            /////////////////////////////////////////////////////////////////
            if([arr count] && self.sortDescriptors)  [arr sortUsingDescriptors:self.sortDescriptors];
            /////////////////////////////////////////////////////////////////
            
            [self.searchArray setArray:arr];
            [arr release];
            [self.tableView reloadData];
        }
        break;
        case 1:///Unreviewed
        {
            isReviewed=NO;
  
            
            NSMutableArray *arr=[[NSMutableArray alloc] init];
            
            for(Customer *_customer in _arr)
            {
                NSPredicate *_filterPredicate = [NSPredicate predicateWithFormat:@"self.isReportCompleted=%@",[NSNumber numberWithBool:NO]];
                
                NSSet *_filtered =  [_customer.visits filteredSetUsingPredicate:_filterPredicate];
                
                if(_filtered.count)
                {
                    [arr addObject:_customer];
                    _customer.incompleteVisitCount=[NSNumber numberWithInteger:_filtered.count];
                }
            }
            
            /////////////////////////////////////////////////////////////////
            if([arr count] && self.sortDescriptors)  [arr sortUsingDescriptors:self.sortDescriptors];
            /////////////////////////////////////////////////////////////////
            
            [self.searchArray setArray:arr];
            [arr release];
            [self.tableView reloadData];
        }
            
        break;
            
        default:
        break;
    }
    
  });
        [self.parallelStackViewController hideDetailController:self.reviewsDetailViewController];
}

-(void)filterForIndex:(NSInteger)selectedIndex fromArray:(NSMutableArray *)arr
{
    if(!self.searchArray)
        self.searchArray=[NSMutableArray array];
    
    switch (selectedIndex) 
    {
        case 0://Reviewed
        {
            isReviewed=YES;
             
            
            NSMutableArray *_visitsArr = arr;
            
            NSMutableArray *arr=[[NSMutableArray alloc] init];
            for(Customer *_customer in _visitsArr)
            {
                NSPredicate *_filterPredicate=[NSPredicate predicateWithFormat:@"self.isReportCompleted=%@",[NSNumber numberWithBool:YES]];
                
                NSSet *_filtered = [_customer.visits filteredSetUsingPredicate:_filterPredicate];
                
                if(_filtered.count)
                {
                    [arr addObject:_customer];
                    _customer.completeVisitCount=[NSNumber numberWithInteger:_filtered.count];
                }
            }
            [self.searchArray setArray:arr];
            [arr release];
            //[self.tableView reloadData];
        }
            break;
        case 1:///Unreviewed
        {
            isReviewed=NO;
             
            
            NSMutableArray *_visitsArr = arr;
            NSMutableArray *arr=[[NSMutableArray alloc] init];
            
            for(Customer *_customer in _visitsArr)
            {
                NSPredicate *_filterPredicate = [NSPredicate predicateWithFormat:@"self.isReportCompleted=%@",[NSNumber numberWithBool:NO]];
                
                NSSet *_filtered =  [_customer.visits filteredSetUsingPredicate:_filterPredicate];
                
                if(_filtered.count)
                {
                    [arr addObject:_customer];
                    _customer.incompleteVisitCount=[NSNumber numberWithInteger:_filtered.count];
                }
            }
            [self.searchArray setArray:arr];
            [arr release];
           // [self.tableView reloadData];
        }
            
            break;
            
        default:
              ;
            break;
    }
    
        [self.parallelStackViewController hideDetailController:self.reviewsDetailViewController];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.parallelStackViewController hideDetailController:self.reviewsDetailViewController];
    [_tableView reloadData];
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    _isRefreshing = NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

-(NSMutableArray *)currentArray
{
    
    return self.searchArray;
    //NSMutableArray *arr=   ? self.searchArray : self.visits;
    
    //return arr;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    int count=[[self currentArray] count];
    
    if(count==0) count=1;
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    static NSString *CellIdentifier =  @"Cell";
    static NSString *CellIdentifier2 = @"Cell_noitems";
    
    NSMutableArray *arr=[self currentArray];
    UITableViewCell *cell=nil;
    
    if(![arr count])
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil) 
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            
            cell.textLabel.font=[UIFont systemFontOfSize:17];
            cell.detailTextLabel.textColor=[UIColor redColor];
            
            cell.accessoryType=UITableViewCellAccessoryNone;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
           
        }
        
      
        cell.textLabel.text=@"No visits";

    }
    else 
    {
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        
        cell.textLabel.font=[UIFont grotesqueFontOfSize:17.0];
        cell.detailTextLabel.textColor=[UIColor redColor];
        cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:15.0];
        
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    }
    
    cell.detailTextLabel.textColor = isReviewed? [UIColor colorWithRed:0.0 green:0.6 blue:0.1 alpha:1.0] : [UIColor redColor];
    
    Customer *_customer=indexPath.row<[arr count]?[arr objectAtIndex:indexPath.row]:nil;
    
    cell.textLabel.text = _customer.name;
    NSInteger count = isReviewed ?[ _customer.completeVisitCount intValue] : [_customer.incompleteVisitCount intValue];
    
        if(count)
        {
          cell.detailTextLabel.text = isReviewed?[NSString stringWithFormat:@"%d Complete %@",count,(count ==1)?@"Visit":@"Visits"]:[NSString stringWithFormat:@"%d Unreviewed %@",count,(count ==1)?@"Visit":@"Visits"];
            
        }
        else 
        {
            cell.detailTextLabel.text = isReviewed?@"No Complete Visits":@"No Unreviewed Visits";
        }

    }

    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    InsetTableViewCell *_cell =(InsetTableViewCell *) cell;
    _cell.delegate = self;
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    
    
    CellBackgroundView *_selectedBgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _selectedBgView.backgroundColor = [UIColor clearColor];
    _selectedBgView.contentInset = CGPointMake(10.0, 0.0);
    _selectedBgView.cornerRadius = 8.0;
    _selectedBgView.borderColor = self.tableView.separatorColor;
    _selectedBgView.fillColor = [UIColor cellSelectedColorBlue];

    
    
    int numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    _selectedBgView.position = _bgView.position;
    
    cell.backgroundView = _bgView;
    cell.selectedBackgroundView = _selectedBgView;
    
    [_bgView release];
    [_selectedBgView release];
    
    //
    
   }

#pragma mark - InsetTableViewCellDelegate
-(void)insetTableViewCell:(InsetTableViewCell *)cell didSelect:(BOOL)selected
{
    NSMutableArray *arr=[self currentArray];
    
    cell.textLabel.textColor  = ([arr count] && selected) ?  [UIColor whiteColor] : [UIColor blackColor];
    cell.detailTextLabel.textColor = ([arr count] && selected) ? [UIColor whiteColor] : ( isReviewed? [UIColor colorWithRed:0.0 green:0.6 blue:0.1 alpha:1.0] : [UIColor redColor])  ;
}
#pragma mark -

/*
- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor  = [UIColor whiteColor];
    
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor  = [UIColor blackColor];
    
    cell.detailTextLabel.textColor = isReviewed? [UIColor colorWithRed:0.0 green:0.6 blue:0.1 alpha:1.0] : [UIColor redColor];
}
*/


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *arr=[self currentArray];
    Customer *_customer=indexPath.row<[arr count]?[arr objectAtIndex:indexPath.row]:nil;
    
    if(!_customer) return;
    
    NSMutableArray *_visits = isReviewed ? [CoreDataHandler reviewedVisitsForCustomer:_customer]  :  [CoreDataHandler unreviewedVisitsForCustomer:_customer];
    
    if(![_visits count]) return;
    
    if(self.parallelStackViewController.isDetailControllerVisible && [self.reviewsDetailViewController.visits isEqual:_visits]) return;
    
    ReviewsDetailViewController *reviews=[[ReviewsDetailViewController alloc] initWithVisits:_visits forCustomer:_customer];
    [self.parallelStackViewController showDetailController:reviews animated:YES];
    
    self.reviewsDetailViewController=reviews;
    [reviews release];

}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    // =searchText.length>0;
    
    
    if(searchText.length)
    {
    if(!self.searchArray) self.searchArray=[NSMutableArray array];
    
    NSPredicate *_searchPredicate=[NSPredicate predicateWithFormat:@"self.name beginswith[c] %@",searchText];
    //NSPredicate *_searchPredicate=[NSPredicate predicateWithFormat:@"self.name contains[c] %@",searchText];
    
    self.searchPredicate = _searchPredicate;
        
   // NSArray *arr= [self.visits filteredArrayUsingPredicate:_searchPredicate];
   // [self.searchArray setArray:arr];
        
    }
    else
    {
        self.searchPredicate = nil;
    }
    
    [self filterTab:nil didSelectAtIndex:isReviewed?0:1];
    
   // [self.tableView reloadData];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.parallelStackViewController hideDetailController:self.reviewsDetailViewController];
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
      ;
}


@end
