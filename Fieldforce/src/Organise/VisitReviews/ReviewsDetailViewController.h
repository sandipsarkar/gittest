//
//  ReviewsDetailViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewsDetailViewController : UITableViewController

@property (nonatomic,retain) NSMutableArray *visits;
@property(nonatomic,retain)  Customer *customer;

-(id)initWithVisits:(NSMutableArray *)_visits forCustomer:(Customer *)aCustomer;

@end
