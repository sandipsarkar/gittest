//
//  ReviewsDetailViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "ReviewsDetailViewController.h"
#import "Customer.h"
#import "Visit.h"
//#import "VisitReviewSessionViewController.h"
#import "VisitSessionViewController.h"


@implementation ReviewsDetailViewController
@synthesize visits,customer;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) 
    {
     
       
    }
    return self;
}

-(id)initWithVisits:(NSMutableArray *)_visits forCustomer:(Customer *)aCustomer
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if(self)
    {
        self.visits=_visits;
        self.customer=aCustomer;
    }
    
    return self;
}

-(void)dealloc
{
    [visits release];
    [customer release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif

    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.width, 40)];
    titleLabel.font = [UIFont subaruBoldFontOfSize:17.0];
    NSString *customerName=self.customer.name;
    if(customerName)
    titleLabel.text=[NSString stringWithFormat:@"   %@",[customerName uppercaseString]];
    
    titleLabel.backgroundColor=[UIColor clearColor];
    self.tableView.tableHeaderView=titleLabel;
    [titleLabel release];
    
    self.tableView.bounces = NO;
    
   // self.tableView.rowHeight = 50.0;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [self.visits count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.font = [UIFont grotesqueFontOfSize:14.0];
         cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:12.0];
        cell.detailTextLabel.textColor = [UIColor grayColor];
    }
    
    
    Visit *_visit = indexPath.row<[self.visits count] ? [self.visits objectAtIndex:indexPath.row] : nil;
    
    BOOL _isCompleted = [_visit.isReportCompleted boolValue];
    
    UIColor *greenColor = [UIColor colorWithRed:0.0 green:0.6 blue:0.1 alpha:1.0];
    cell.textLabel.textColor = _isCompleted ? greenColor : [UIColor redColor];

    cell.textLabel.text = [_visit.startDate stringFromDateWithFormat:VISIT_DATE_FORMAT];
    
    
    //------------------------------------------------------------------------------------------------
    NSString *startEndStr = [_visit startEndTimeString];
    cell.detailTextLabel.text = [@" " stringByAppendingString:startEndStr];
    
   
 
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    
    
    CellBackgroundView *_selectedBgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _selectedBgView.backgroundColor = [UIColor clearColor];
    _selectedBgView.contentInset = CGPointMake(10.0, 0.0);
    _selectedBgView.cornerRadius = 8.0;
    _selectedBgView.borderColor = self.tableView.separatorColor;
    _selectedBgView.fillColor = [UIColor cellSelectedColorBlue];
    
    
    NSUInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    _selectedBgView.position = _bgView.position;
    
    cell.backgroundView = _bgView;
    cell.selectedBackgroundView = _selectedBgView;
    
    [_bgView release];
    [_selectedBgView release];
    
    cell.textLabel.text = [NSString stringWithFormat:@"  %@",cell.textLabel.text];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // UINavigationController *controller= [self nextResponderNavigationController];

    Visit *_visit=indexPath.row<[self.visits count] ? [self.visits objectAtIndex:indexPath.row] : nil;
    if(!_visit) return;
    
     VisitSessionViewController *visitReviewSessionVC=[[VisitSessionViewController alloc] initWithVisit:_visit forCustomer:self.customer];
    
    //VisitReviewSessionViewController *visitReviewSessionVC=[[VisitReviewSessionViewController alloc] initWithVisit:_visit forCustomer:self.customer];
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:visitReviewSessionVC];
    [app.splitViewController presentModalViewController:navController animated:YES];
    [visitReviewSessionVC release];
    [navController release];

}

@end
