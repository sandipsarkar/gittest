//
//  VisitReviewsViewController.m
//  RepVisitationTool
//  Created by RANDEM MAC on 22/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "VisitReviewsViewController.h"
#import "VisitReviewsListViewController.h"
#import "ReviewsDetailViewController.h"
#import "SSSegmentedControl.h"
#import "UISearchBar+BackgroundImage.h"
//#import "FilterViewController.h"

@interface VisitReviewsViewController()
{
    CGRect _containerRect;
    ParallelStackViewController *_parallelStackViewController;
    
    SSSegmentedControl *customSegControl;
    UISearchBar *searchBar;
}
@property(nonatomic,retain)UIPopoverController *filterPopover;
@end


@implementation VisitReviewsViewController
@synthesize visitReviewsListViewController;
@synthesize filterPopover;


- (id)init
{
    self = [super init];
    if (self) 
    {
        visitReviewsListViewController = [[VisitReviewsListViewController alloc] init];
        _parallelStackViewController   = [[ParallelStackViewController alloc]initWithMasterController:visitReviewsListViewController];
    }
    return self;
}

-(void)dealloc
{
    [visitReviewsListViewController release];
    [_parallelStackViewController release];
    [customSegControl release]; 
    [searchBar release];
    [filterPopover release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)setupSegmentedControl
{
   filterButton=[UIButton buttonWithTitle:@"Filter" type:UIButtonTypeCustom frame:CGRectMake(0, 0, 117.0, 44) bgImage:@"segmented_button.png" titleColor:[UIColor whiteColor] target:self action:@selector(filterAction:)];
    filterButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"segmented_button_selected.png"] forState:UIControlStateHighlighted];
    [filterButton setBackgroundImage:[UIImage imageNamed:FILTER_BUTTON_IMAGE] forState:UIControlStateSelected];
    [self.view addSubview:filterButton];
    
    NSArray *_items=[NSArray arrayWithObjects:@"Reviewed",@"Unreviewed",nil];
    
    customSegControl=[[SSSegmentedControl alloc] initWithItems:_items setupSegment:^(SSegment *segment,NSUInteger index) 
    {
        segment.textLabel.textColor=[UIColor whiteColor];
        segment.textLabel.font  = [UIFont grotesqueBoldFontOfSize:13.0];
    }];
    
    customSegControl.frame=CGRectMake(CGRectGetMaxX(filterButton.frame), 0, 117.0*2, 44);
     
    
    [customSegControl setBackgroundForNormalState:[UIImage imageNamed:@"segmented_button.png"] forSelectedState:[UIImage imageNamed:@"segmented_button_selected.png"]];
    [customSegControl addTarget:self action:@selector(segmentedControlValChanged:) forControlEvents:UIControlEventValueChanged];
    customSegControl.selectedSegmentIndex=1;
    [self.view addSubview:customSegControl];
    
}

-(void)setupSearchBar
{
    //searchBar=[[UISearchBar alloc] initWithFrame:CGRectMake(CGRectGetMaxX(customSegControl.frame), 0, self.view.bounds.size.width-CGRectGetMaxX(customSegControl.frame)-45.0, 44.5)];
    searchBar=[[UISearchBar alloc] initWithFrame:CGRectMake(CGRectGetMaxX(customSegControl.frame), 0, self.view.bounds.size.width-CGRectGetMaxX(customSegControl.frame), 44.5)];
    searchBar.autoresizingMask=UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth ;
   // searchBar.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    searchBar.delegate=visitReviewsListViewController;
    searchBar.placeholder  = @"Search...";
    [self.view addSubview:searchBar];
    [searchBar addBackgroundImage:[UIImage imageNamed:@"search_bar_big.png"]];
}

-(void)filterAction:(UIButton *)sender
{
    if([searchBar isFirstResponder])[searchBar resignFirstResponder];
    [visitReviewsListViewController performSelector:@selector(filterAction:) withObject:sender];
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self setupSegmentedControl];
    [self setupSearchBar];
    
    _containerRect=CGRectMake(0, 44, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-44);
    
    _parallelStackViewController.view.frame = _containerRect;
    [self addChildViewController:_parallelStackViewController];
    [self.view addSubview:_parallelStackViewController.view];
    [_parallelStackViewController didMoveToParentViewController:self];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   // [_parallelStackViewController viewWillAppear:animated];
   // [visitReviewsListViewController viewWillAppear:animated];
    
}

-(void)segmentedControlValChanged:(SSSegmentedControl *)segControl
{
    NSInteger index = segControl.selectedSegmentIndex;
    
    NSLog(@"selected Index:%d",index);
    
    [visitReviewsListViewController filterTab:segControl didSelectAtIndex:index];
}

-(void)refresh
{
    searchBar.text = nil;
    [searchBar resignFirstResponder];
    
    if(visitReviewsListViewController)
    [visitReviewsListViewController refresh];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
