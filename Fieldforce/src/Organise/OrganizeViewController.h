//
//  OrganizeViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 21/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractTabBarController.h"
#import "OrganizeTasksListViewController.h"

@class  VisitReviewsViewController;

@interface OrganizeViewController : AbstractTabBarController
{
    
}

@property(nonatomic,readonly) OrganizeTasksListViewController *tasksViewController;
@property(nonatomic,readonly) VisitReviewsViewController *visitReviewsController;

@end
