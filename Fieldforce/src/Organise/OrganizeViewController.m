//
//  OrganizeViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 21/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "OrganizeViewController.h"
#import "VisitReviewsViewController.h"

@implementation OrganizeViewController
@synthesize tasksViewController,visitReviewsController;



-(NSArray *)viewControllers
{
   // taskListViewController = [[OrganizeTasksListViewController alloc] initw];
   // visitReviewsController = [[VisitReviewsViewController alloc] init];
    
    if(!tasksViewController)
    tasksViewController = [[OrganizeTasksListViewController alloc] initWithCustomer:nil];
    
    if(!visitReviewsController)
    visitReviewsController = [[VisitReviewsViewController alloc] init];

    
    UIButton *button=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(0, 0, 0, 0) bgImage:nil titleColor:nil target:nil action:nil];
    [button setBGImageForNormalState:[UIImage imageNamed:@"task_list_tab.png"] selectedState:[UIImage imageNamed:@"task_list_tab_selected.png"]];
    
    tasksViewController.SSTabBarItem=button;
    
    button=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(0, 0, 0, 0) bgImage:nil titleColor:nil target:nil action:nil];
    [button setBGImageForNormalState:[UIImage imageNamed:@"visit_reviews_tab.png"] selectedState:[UIImage imageNamed:@"visit_reviews_tab_selected.png"] ];
    
    visitReviewsController.SSTabBarItem=button;
    
    NSArray *vcs=[NSArray arrayWithObjects:tasksViewController,visitReviewsController, nil];
    
    return vcs;
}

-(void)didSelectViewController:(UIViewController *)controller
{
    if([controller isKindOfClass:[TasksViewController class]])
    {
        NSLog(@"%d",app.fromCustomerTask);
       // app.fro2mCustomerTask = 2;
        [self addTitleImage:@"task_list.png"];
    }
    else if([controller isKindOfClass:[visitReviewsController class]])
    {
        [self addTitleImage:@"visit_reviews.png"];
    }
}

#pragma mark Called when Refresh button is tapped.
-(void)willRefresh
{
    if(tasksViewController) [tasksViewController refresh];
    if(visitReviewsController) [visitReviewsController refresh];
}
#pragma mark -

-(void)dealloc
{
    [tasksViewController release];
    [visitReviewsController release];
    
     [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [tasksViewController viewWillAppear:animated];
    [visitReviewsController viewWillAppear:animated];
    app.fromCustomerTask = 2;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [tasksViewController viewDidAppear:animated];
    [visitReviewsController viewDidAppear:animated];

    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    //self.navigationController.navigationBarHidden=YES;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
 

@end
