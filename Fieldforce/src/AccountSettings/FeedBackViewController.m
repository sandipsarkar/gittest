//
//  FeedBackViewController.m
//  RepVisitationTool
//
//  Created by Subhojit Dey on 1/2/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "FeedBackViewController.h"
#import "FeedBackCategoryViewController.h"
#import "Rep.h"
#import "WholesaleDealer.h" 
#import "OfflineManager.h"
#import "Reachability.h"

#define FEEDBACK_VALIDATION_ALERT @"Please fill all the fields"

@interface FeedBackViewController ()<FeedbackCategoryDelegate>
{
    //BOOL isNotificationArrived;
    
    BOOL isKeyboardAppeared;
}

@property (nonatomic,retain) ASIHTTPRequest *submitFeedbackRequest;
@end

@implementation FeedBackViewController
@synthesize categoryPopover;
@synthesize categoryID,categoryName;
@synthesize submitFeedbackRequest;

- (id)init
{
    self = [super init];
    if (self)
    {
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectioninFeedbackControllerDidChange :) name:kReachabilityChangedNotification object:nil];
        
        //isNotificationArrived = NO;
    }
    return self;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    // [scrollView setContentSize:self.view.bounds.size];
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
-(void)dealloc
{
    [submitFeedbackRequest clearDelegatesAndCancel];
    [submitFeedbackRequest release];
    submitFeedbackRequest = nil;
    
    [scrollView release];
    [captionTextView release];
    [txtSubject release];
    [categoryPopover release];
    [categoryID release];
    [categoryName release];
    
   // [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    
    [super dealloc];
    
}
/*
-(void)connectioninFeedbackControllerDidChange:(NSNotification *)notification
{
    //Reachability *rechability = [notification object];
    if (isNotificationArrived) {
        return;
    }
    
    BOOL _isNetworkAvailable = [[OfflineManager sharedManager] isNetworkAvailable];
    if (!_isNetworkAvailable)
    {
        isNotificationArrived = YES;
        UIAlertView *alert=[UIAlertView alertViewWithTitle:nil message:POOR_CONNECTION_MSG_FOR_DATA_LOAD_FAILED cancelButtonTitle:@"Ok" otherButtonTitles:nil
             onDismiss:^(int buttonIndex)
            {
            }
              onCancel:^{
                  if (self.categoryPopover) {
                      [self.categoryPopover dismissPopoverAnimated:YES];
                  }
                  [self dismissModalViewControllerAnimated:YES];
              }];
        [alert show];
    }
}
*/
-(void)keyboardDidAppear:(NSNotification *)aNotification
{
    
    isKeyboardAppeared = YES;
    
    if(self.popoverController) return;
   

    if(isTextView)
    {
        CGRect keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
        
        //if(UIEdgeInsetsEqualToEdgeInsets(tableViewContentInsets, UIEdgeInsetsZero))
        //  tableViewContentInsets = self.tableView.contentInset;
        
        UIEdgeInsets _tableInset = scrollView.contentInset;
        _tableInset.bottom = keyboardRect.size.height;
        scrollView.contentInset=_tableInset;
        
        CGPoint scrollToOffset=captionTextView.superview.frame.origin;
        scrollToOffset.x= scrollView.contentOffset.x;
        scrollToOffset.y= scrollView.contentOffset.y+158;
        [scrollView setContentOffset:scrollToOffset animated:YES];
    //[scrollView scrollRectToVisible:captionTextView.superview.frame animated:YES];
        [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height-150)];

        //isTextView=NO;
    }
    else{
         [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+130)];
    }
}

-(void)keyboardDidDismiss:(NSNotification *)aNotification
{
    isKeyboardAppeared = NO;
    
    if(self.popoverController) return;
    
    [UIView animateWithDuration:0.3 animations:^{
          scrollView.contentInset=UIEdgeInsetsMake(0, 0, 0, 0);
    }];
  
        
     [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height)];
     isTextView=NO;
    //scrollView.frame=self.view.bounds;
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    isTextView=YES;
    CGRect rect = [textView bounds];
    rect = [textView convertRect:rect toView:scrollView];
    CGPoint point = rect.origin ;
    point.x = 0 ;
    point.y = 120 ;
    [scrollView setContentOffset:point animated:YES];
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView;
{
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
    if(isTextView)
    {
        CGRect rect = [textField bounds];
        rect = [textField convertRect:rect toView:scrollView];
        CGPoint point = rect.origin ;
        point.x = 0 ;
        point.y = 100 ;
        [scrollView setContentOffset:point animated:YES];
    }
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField;
{
    //CGPoint svos = scrollView.contentOffset;
    /*CGRect rect = [textField bounds];
     rect = [textField convertRect:rect toView:scrollView];
     CGPoint point = rect.origin ;
     point.x = 0 ;
     point.y = 120 ;
     [scrollView setContentOffset:point animated:YES];*/
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.backgroundColor=[UIColor whiteColor];
     [self addTitle:@"FEEDBACK" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    [self.navigationController.navigationBar addBackGroundImage:@"navigation_bar.png"];
    [self addBackButtonWithSelector:@selector(backButtonAction)];
    UIColor *_contentBGColor  = [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
    UIColor *_contentBorderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];

  

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidAppear:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidDismiss:) name:UIKeyboardWillHideNotification object:nil];
    
    CGRect _bounds     = self.view.bounds;
    
    locY=85.0;
    
    scrollView=[[UIScrollView alloc] initWithFrame:self.view.bounds];
    scrollView.autoresizingMask=UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.bounces=NO;
    scrollView.autoresizesSubviews=YES;
    scrollView.showsHorizontalScrollIndicator=NO;
    scrollView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:scrollView];
    /// [scrollView setContentSize:self.view.bounds.size];
    
    UIView *oemContainerview=[[UIView alloc]initWithFrame:CGRectMake(20.0, locY, _bounds.size.width-40.0, 50)];
    oemContainerview.backgroundColor=_contentBGColor;
    oemContainerview.layer.borderWidth  = 1.0;
    oemContainerview.layer.cornerRadius = 8.0;
    oemContainerview.layer.borderColor  = _contentBorderColor.CGColor;
    oemContainerview.autoresizingMask   =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
    //[oemContainerview addInnerShadowWithColor:[UIColor colorWithWhite:0.2 alpha:1]];
    //[oemContainerview addInnerShadowToTop:YES toLeft:YES];
    
    /*
     UIView *shadowLayer = [[UIView alloc] initWithFrame:CGRectInset(oemContainerview.frame, 2.0, 2.0)];
     shadowLayer.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowLayer.bounds].CGPath;
     shadowLayer.layer.shadowColor = [UIColor blackColor].CGColor;
     // shadowLayer.layer.shadowRadius = 8.0;
     shadowLayer.layer.shadowOffset = CGSizeMake(-15, 20);
     
     //shadowLayer.layer.shadowOffset = CGSizeMake(2.0, 2.0);
     [scrollView addSubview:shadowLayer];
     */
    
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(10.0, (CGRectGetHeight(oemContainerview.frame)-30)/2.0, 85, 30)];
    titleLabel.text=@"Section *";
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor=[UIColor blackColor];
    titleLabel.font=[UIFont grotesqueBoldFontOfSize:14.0];
    [oemContainerview addSubview:titleLabel];
    [titleLabel release];
    
    selectCategoryButton=[UIButton buttonWithType:UIButtonTypeCustom];
    selectCategoryButton.frame=CGRectMake(CGRectGetMaxX(titleLabel.frame), 0, CGRectGetWidth(oemContainerview.frame)-CGRectGetMaxX(titleLabel.frame)-10.0, CGRectGetHeight(oemContainerview.frame));
    selectCategoryButton.backgroundColor = [UIColor clearColor];
    [selectCategoryButton setTitle:@"Select" forState:UIControlStateNormal];
    //selectOemButton.titleLabel.text = self.visitPhoto.oemName.length?self.visitPhoto.oemName:@"Select OEM";
    selectCategoryButton.titleLabel.font = [UIFont grotesqueFontOfSize:14.0];
    [selectCategoryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    selectCategoryButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentRight;
    selectCategoryButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [selectCategoryButton addTarget:self action:@selector(selectCategoryAction:) forControlEvents:UIControlEventTouchUpInside];
    [oemContainerview addSubview:selectCategoryButton];
    //selectOemButton.enabled = isEditMode;
    
    [scrollView addSubview:oemContainerview];
    [oemContainerview release];

    locY=CGRectGetMaxY(oemContainerview.frame)+30;
    
    
    /*
    UIView *subjectContainerview=[[UIView alloc]initWithFrame:CGRectMake(20.0, locY+30, _bounds.size.width-40.0, 50)];
    subjectContainerview.backgroundColor=_contentBGColor;
    subjectContainerview.layer.borderWidth  = 1.0;
    subjectContainerview.layer.cornerRadius = 8.0;
    subjectContainerview.layer.borderColor  = _contentBorderColor.CGColor;
    subjectContainerview.autoresizingMask   =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
    
    UILabel *subLabel=[[UILabel alloc]initWithFrame:CGRectMake(22.0,locY, 70, 20)];
    subLabel.text=@"Subject: ";
    subLabel.backgroundColor = [UIColor clearColor];
    subLabel.textColor=[UIColor blackColor];
    subLabel.font=[UIFont grotesqueBoldFontOfSize:14.0];
    [scrollView addSubview:subLabel];
    [subLabel release];
    
    
    
    locX=CGRectGetWidth(subLabel.frame)+10;
    
    txtSubject=[[UITextField alloc]initWithFrame:CGRectMake(5.0, (CGRectGetHeight(subjectContainerview.frame)-20)/2.0, _bounds.size.width-35.0, 20)];
    txtSubject.delegate=self;
    txtSubject.contentVerticalAlignment=UIControlContentHorizontalAlignmentCenter;
    txtSubject.font = [UIFont grotesqueFontOfSize:14.0];
    [subjectContainerview addSubview:txtSubject];
    
    [scrollView addSubview:subjectContainerview];

    
    locY=CGRectGetMaxY(subjectContainerview.frame)+30;
    [subjectContainerview release];
    */

    UILabel *descLabel=[[UILabel alloc]initWithFrame:CGRectMake(22.0, locY, 110, 20)];
    descLabel.text=@"Description * ";
    descLabel.backgroundColor = [UIColor clearColor];
    descLabel.textColor=[UIColor blackColor];
    descLabel.font=[UIFont grotesqueBoldFontOfSize:14.0];
    [scrollView addSubview:descLabel];
    [descLabel release];
    
    
       
    locY=CGRectGetMaxY(descLabel.frame)+10;
    UIView *textViewContainerview=[[UIView alloc]initWithFrame:CGRectMake(20.0, locY,CGRectGetWidth(self.view.bounds)-40.0, 150+200)];
    textViewContainerview.backgroundColor=_contentBGColor;
    textViewContainerview.layer.borderWidth  = 1.0;
    textViewContainerview.layer.cornerRadius = 8.0;
    textViewContainerview.layer.borderColor  = _contentBorderColor.CGColor;
    textViewContainerview.autoresizingMask   =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
   [scrollView addSubview:textViewContainerview];
    
    

    locX=CGRectGetWidth(descLabel.frame)-10;
    
    captionTextView=[[UITextView alloc] initWithFrame:CGRectMake(0, 6.0,CGRectGetWidth(textViewContainerview.bounds), CGRectGetHeight(textViewContainerview.bounds)-10.0)];
    captionTextView.font = [UIFont grotesqueFontOfSize:15.0];
    //captionTextView.contentInset=UIEdgeInsetsMake(10.0, 8.0, 10.0, 8.0);
    captionTextView.autocorrectionType = UITextAutocorrectionTypeNo;
    captionTextView.showsHorizontalScrollIndicator= NO;
    captionTextView.bounces = NO;
    captionTextView.backgroundColor = [UIColor clearColor];
    captionTextView.delegate=self;
    captionTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
    [textViewContainerview addSubview:captionTextView];
    //[textViewContainerview addInnerShadowWithColor:[UIColor colorWithWhite:0.2 alpha:1]];
    [textViewContainerview release];
    //captionTextView.editable = isEditMode;
    
    locY=CGRectGetMaxY(textViewContainerview.frame)+40.0;
    
    cancelButton = [UIButton repToolCancelButton];
    //[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(10.0, locY+10.0,cancelButtonImage.size.width,cancelButtonImage.size.height) bgImage:@"cancel_popover_button.png" titleColor:nil target:self action:@selector(cancelAction:)];
    CGRect cancelFrame = cancelButton.frame;
    cancelFrame.origin.y = locY;
    cancelFrame.origin.x = 20.0f;
    cancelButton.frame = cancelFrame;
    cancelButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    cancelButton.tag=1;
    // [cancelButton setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [cancelButton setTitle:@"CANCEL" forState:UIControlStateNormal];
    [scrollView addSubview:cancelButton];
    
    cancelButton.autoresizingMask =UIViewAutoresizingFlexibleRightMargin;
    [cancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    //cancelButton.enabled = isEditMode;
    
    
    saveButton=[UIButton repToolButtonWithText:@"SEND"];
    CGRect saveFrame = saveButton.frame;
    saveFrame.origin.x = CGRectGetWidth(scrollView.bounds)-saveFrame.size.width-20.0;
    saveFrame.origin.y = CGRectGetMinY(cancelButton.frame);
    saveButton.frame = saveFrame;
    [saveButton addTarget:self action:@selector(sendAction:) forControlEvents:UIControlEventTouchUpInside];
    saveButton.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin;
    saveButton.tag=2;
    [scrollView addSubview:saveButton];
    //saveButton.enabled = isEditMode;
    //saveButton.autoresizingMask =  UIViewAutoresizingFlexibleRightMargin;
    
    locY=CGRectGetMaxY(cancelButton.frame)+10.0;

	// Do any additional setup after loading the view.
}
-(void)cancel
{
     [self dismissModalViewControllerAnimated:YES];
    
}
-(void)selectCategoryAction:(UIButton *)sender
{
    FeedBackCategoryViewController *categoryViewController = [[FeedBackCategoryViewController alloc] init];
    categoryViewController.delegate = self;
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:categoryViewController];
    popover.popoverContentSize = CGSizeMake(335, 700);
    self.categoryPopover = popover;
    
    /*
    [categoryViewController addDidSelectCategoryCallback:^(NSString *_categoryID,NSString *_categoryName) 
    {
          self.categoryID=_categoryID;
          self.categoryName=_categoryName;
         [selectCategoryButton setTitle:[self.categoryName capitalizedString] forState:UIControlStateNormal];
         [self.categoryPopover dismissPopoverAnimated:YES];
        
        //[categoryViewController addDidSelectCategoryCallback:nil];
        
     }];
    
    [categoryViewController addDidLoadCategoryCallback:^(BOOL success)
    {
        if (!success)
        {
            if (self.categoryPopover)
            {
                [self.categoryPopover dismissPopoverAnimated:YES];
            }
            [self dismissModalViewControllerAnimated:YES];
            
        }
        saveButton.enabled   = success;
        cancelButton.enabled = success;
       // captionTextView.editable = success;
        
      //  [categoryViewController addDidLoadCategoryCallback:nil];
    }];
    */
    
     [popover presentPopoverFromRect:sender.frame inView:sender.superview permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    [categoryViewController release];
    [popover release];

    
}

-(void)feedbackCategoryController:(FeedBackCategoryViewController *)feedbackCategoryVC didSelectCategory:(NSString *)aCategoryName categoryID:(NSString *)categoryId
{
    self.categoryID=categoryId;
    self.categoryName=aCategoryName;
    [selectCategoryButton setTitle:[self.categoryName capitalizedString] forState:UIControlStateNormal];
    [self.categoryPopover dismissPopoverAnimated:YES];

}

-(void)feedbackCategoryController:(FeedBackCategoryViewController *)feedbackCategoryVC didLoad:(BOOL)success
{
    if (!success)
    {
        if (self.categoryPopover)
        {
            [self.categoryPopover dismissPopoverAnimated:YES];
        }
        [self dismissModalViewControllerAnimated:YES];
        
    }
    saveButton.enabled   = success;
    cancelButton.enabled = success;
    
     // captionTextView.editable = success;
}

-(void)sendAction:(UIButton *)sender
{
    //================== IF EXTERNAL KEYBOARD IS CONNECTED ======================
    
    if(!isKeyboardAppeared)
    {
        [captionTextView resignFirstResponder];
        
        [UIView animateWithDuration:0.3 animations:^{
            scrollView.contentInset=UIEdgeInsetsMake(0, 0, 0, 0);
        }];
        
        scrollView.contentOffset = CGPointMake(0, 0);
        [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height)];
        isTextView=NO;
    }
    //===================================================================
    
    if([selectCategoryButton.titleLabel.text isEqualToString:@"Select"])
    {
        
        [UIAlertView showWarningAlertWithTitle:nil message:PLEASE_SELECT_SECTION_MSG];
        
        
        
    }
    else if(captionTextView.text.length==0)
    {
        [UIAlertView showWarningAlertWithTitle:nil message:PLZ_ENTER_DESCRIPTION_MSG];
    }
    
    else if(![[OfflineManager sharedManager] isNetworkAvailable])
    {
        UIAlertView *alert=[UIAlertView alertViewWithTitle:nil message:FEEDBACK_CANNOT_BE_SENT_DUE_TO_UNAVAILABILITY_OF_NETWORK cancelButtonTitle:@"Ok" otherButtonTitles:nil
             onDismiss:^(int buttonIndex)
            {
            }
              onCancel:^{
                  if (self.categoryPopover) {
                      [self.categoryPopover dismissPopoverAnimated:YES];
                  }
                  [self dismissModalViewControllerAnimated:YES];
              }];
        [alert release];
    }
    else
    {
        saveButton.enabled=NO;
        cancelButton.enabled=NO;
        self.view.userInteractionEnabled=NO;
        
        ProgressHUD *hud=[ProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Sending...";
        
        
    // NSString *deviceInfo=[self getDeviceInformation];
        
      
     ASIHTTPRequest *request = [ConnectionManager submitFeedbackConnectionWithRepID:nil wdSiteID:nil deviceInfo:nil   categoryID:self.categoryID subjet:nil description:captionTextView.text   categoryaName:self.categoryName];
        
     request.delegate = self;
    self.submitFeedbackRequest = request;

      /*  [request setCompletionBlock:^{
            
            [ProgressHUD hideHUDForView:self.view animated:YES];
            
            //NSString *jsonString = [request responseString];
            //NSLog(@"feedback json:%@",jsonString);
            NSDictionary *jsonDict = [request responseJSON];
            
            NSLog(@"feedback jsonDict:%@",jsonDict);
            saveButton.enabled=YES;
            cancelButton.enabled=YES;
            self.view.userInteractionEnabled=YES;
            
            if([[jsonDict valueForKey:@"status"] intValue]==1)
            {
                UIAlertView *alert=[UIAlertView alertViewWithTitle:nil message:FEEDBACK_SENT_MSG cancelButtonTitle:@"Ok" otherButtonTitles:nil onDismiss:^(int buttonIndex)
                {
                    
                    
                }
                onCancel:^{
                        
                   // [self dismissModalViewControllerAnimated:YES];
                    [self dismissViewControllerAnimated:YES completion:^{
                        
                    }];

                }];

                [alert show];
            }

        }];
     
    
        [request setFailedBlock:^{
            
          [ProgressHUD hideHUDForView:self.view animated:YES];
            
            saveButton.enabled=YES;
            cancelButton.enabled=YES;
            self.view.userInteractionEnabled=YES;
        }];
       */
        
    }
    
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [ProgressHUD hideHUDForView:self.view animated:YES];

    NSDictionary *jsonDict = [request responseJSON];
    
    NSLog(@"feedback jsonDict:%@",jsonDict);
    saveButton.enabled=YES;
    cancelButton.enabled=YES;
    self.view.userInteractionEnabled=YES;
    

     NSNumber *isUnderMaintenance  = [jsonDict objectForKey:JSON_UNDER_MAINTENANCE_KEY];
     
     isUnderMaintenance = NULL_NIL(isUnderMaintenance);
     
     if([isUnderMaintenance boolValue])
     {
         [app showUnderMaintainenceScreen:YES];
         
         return;
     }

    
    if([[jsonDict valueForKey:@"status"] intValue]==1)
    {
        UIAlertView *alert=[UIAlertView alertViewWithTitle:nil message:FEEDBACK_SENT_MSG cancelButtonTitle:@"Ok" otherButtonTitles:nil onDismiss:^(int buttonIndex){}
            onCancel:^{
                                                      
            // [self dismissModalViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:^{}];
            }];
        
        [alert show];
    }
    
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    [ProgressHUD hideHUDForView:self.view animated:YES];
    saveButton.enabled=YES;
    cancelButton.enabled=YES;
    self.view.userInteractionEnabled=YES;
}

-(void)backButtonAction
{
    [self dismissModalViewControllerAnimated:YES];
    
}

-(NSString *)getDeviceInformation
{
    NSString *deviceType = [[UIDevice currentDevice] model];
    NSLog(@"%@",deviceType);
    
    NSString *deviceIdentifier = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSLog(@"%@",deviceIdentifier);
    
    NSString *deviceOs = [[UIDevice currentDevice]systemVersion];
    
     NSLog(@"%@",deviceOs);
    
    NSString *ffVersion = APP_VERSION;
    
    NSString *deviceInformation = [NSString stringWithFormat:@"%@,%@,%@,%@",deviceType,deviceOs,deviceIdentifier,ffVersion];
    
    return deviceInformation;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
