//
//  AccountSettingsViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 21/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountSettingsViewController : UITableViewController
{
    NSInteger numberOfSections;
    BOOL isChangePassWordClicked;
    
    NSMutableDictionary *_resuseDict;
    
    
    Rep *rep;
}
@end
