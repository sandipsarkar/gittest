//
//  AccountSettingsViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 21/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "AccountSettingsViewController.h"
#import "InsetTableViewCell.h"
#import "Rep.h"




@interface AccountSettingsViewController()<UITextFieldDelegate>

@property (nonatomic,retain) ASIHTTPRequest *passwordChangeRequest;
@end

@implementation AccountSettingsViewController
@synthesize passwordChangeRequest;

- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self)
    {
       
         self.title = @"Account Settings";
        self.tableView.bounces=NO;
        numberOfSections=1;

        isChangePassWordClicked=NO;
        
        _resuseDict=[[NSMutableDictionary alloc] init];
    }
    return self;
}

-(void)dealloc
{
    [passwordChangeRequest clearDelegatesAndCancel];
    [passwordChangeRequest release];
    passwordChangeRequest = nil;
    
    [_resuseDict release];
    [rep release];
    
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



-(void)setupFooterView
{
    //CGSize sizeInPopover= self.contentSizeForViewInPopover;
    
    CGSize sizeInPopover= self.view.bounds.size;
    
    UIButton *submitButton=nil;
    UIButton *cancelButton=nil;
    UIButton *changePasswordButton=nil;
    
    if(self.tableView.tableFooterView==nil)
    {
        
    UIView *footerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 125)];
    
    float locX=0;
    
    cancelButton=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(10, 10, MAX(151.0,sizeInPopover.width/2-10), 54) bgImage:@"cancel_popover_button.png" titleColor:nil target:self action:@selector(cancelAction:)];
    cancelButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
        cancelButton.tag=1;
        [footerView addSubview:cancelButton];
    
    submitButton=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(CGRectGetMaxX(cancelButton.frame)+3, 10, MAX(151.0,sizeInPopover.width/2-10), 54) bgImage:@"submit_popover_button.png" titleColor:nil target:self action:@selector(submitAction:)];
    submitButton.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;;
        submitButton.tag=2;
    [footerView addSubview:submitButton];
        
        
     changePasswordButton=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake((sizeInPopover.width-310)/2.0, 10, 310, 54) bgImage:@"change_password_popover_button.png" titleColor:nil target:self action:@selector(changePasswordAction:)];
    changePasswordButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
    changePasswordButton.tag=3;
    [footerView addSubview:changePasswordButton];

    locX=CGRectGetMaxY(changePasswordButton.frame)+5;
    
    UIButton *signOutButton=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake((sizeInPopover.width-310)/2.0, locX, 310, 54) bgImage:@"sign_out_popover_button.png" titleColor:nil target:self action:@selector(signoutAction:)];
    signOutButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
    [footerView addSubview:signOutButton];
    self.tableView.tableFooterView=footerView;
    [footerView release];
        
    }
    else
    {
     submitButton=(UIButton *)[self.tableView.tableFooterView viewWithTag:2];
     cancelButton=(UIButton *)[self.tableView.tableFooterView viewWithTag:1];
     changePasswordButton=(UIButton *)[self.tableView.tableFooterView viewWithTag:3];
    }
    
    if(isChangePassWordClicked)
    {
        submitButton.hidden=NO;
        cancelButton.hidden=NO;
        changePasswordButton.hidden=YES;
    }
    else
    {
        submitButton.hidden=YES;
        cancelButton.hidden=YES;
        changePasswordButton.hidden=NO;
    }
}

-(void)manageInsertionOfNewSection:(BOOL)willInset
{
    @try 
    {
        [self.tableView beginUpdates];
        
        if(willInset)
        {
         [self.tableView insertSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationBottom];
        }
        else
        {
        
         [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationTop];
        }
        
        [self.tableView endUpdates];
        
    }
    @catch (NSException * e) 
    {
        NSLog(@"Exception:%@ reason:%@",e.name,e.reason);
    }
    @finally 
    {
        
    }

}

-(void)changePasswordAction:(id)sender
{
    CGSize popoverSize = self.contentSizeForViewInPopover;
    
    numberOfSections=2;
    isChangePassWordClicked=YES;
    [self setupFooterView];
    [self manageInsertionOfNewSection:YES];
    
    popoverSize.height+=163;
    self.contentSizeForViewInPopover=popoverSize;
}


-(void)cancelAction:(id)sender
{
    for(NSIndexPath *keyIndexPath in _resuseDict)
    {
        UITextField *textField = [_resuseDict objectForKey:keyIndexPath];
        textField.text = nil;
    }
    
    CGSize popoverSize = self.contentSizeForViewInPopover;
    
    numberOfSections=1;
    isChangePassWordClicked=NO;
    [self setupFooterView];
    [self manageInsertionOfNewSection:NO];
    
    popoverSize.height-=163;
    self.contentSizeForViewInPopover=popoverSize;
}

-(void)submitAction:(UIButton *)sender
{
    NSString *oldPasswordStr=nil;
    NSString *newPasswordStr= nil;
    NSString *confirmPaswordStr = nil;
    
    NSString *errorMessage = nil;
    
    for(NSIndexPath *keyIndexPath in _resuseDict)
    {
        UITextField *textField = [_resuseDict objectForKey:keyIndexPath];
        
        switch (keyIndexPath.row) 
        {
            case 0:
                oldPasswordStr = textField.text; 
                break;
                
            case 1:
                newPasswordStr = textField.text;
                break;
                
            case 2:
                confirmPaswordStr = textField.text;
                break;
                
            default:
                break;
        }
    }
    
    BOOL isValid = oldPasswordStr.length>0;
    if(!isValid)
    {
        NSLog(@"Please enter the Old password.");
        errorMessage = @"Please enter the Old password.";
        
        [UIAlertView showWarningAlertWithTitle:@"" message:errorMessage];
        return;
    }
    
    isValid = newPasswordStr.length>0;
    
    if(!isValid)
    {
        NSLog(@"Please enter a New password.");
        errorMessage = @"Please enter a New password.";
        [UIAlertView showWarningAlertWithTitle:@"" message:errorMessage];
        return;
    }
    
    isValid = confirmPaswordStr.length>0;
    
    if(!isValid)
    {
        NSLog(@"Please confirm the New password.");
        errorMessage = @"Please confirm the New password.";
        [UIAlertView showWarningAlertWithTitle:@"" message:errorMessage];
        return;
    }
    
    NSString *loggedInPasword = [[NSUserDefaults standardUserDefaults] objectForKey:LOGIN_PASSWORD_KEY];
    
    isValid = [loggedInPasword isEqualToString:oldPasswordStr];
    
    if(!isValid)
    {
        NSLog(@"Old password does not match the existing password.\nPlease try again.");
        errorMessage = @"Old password does not match the existing password.\nPlease try again.";
        [UIAlertView showWarningAlertWithTitle:@"" message:errorMessage];
        return;
    }
    
    isValid = [newPasswordStr isEqualToString:confirmPaswordStr];
    
    if(!isValid)
    {
        NSLog(@"Please confirm the New password.");
        
        errorMessage = @"The New password and Confirm password do not match.";
        [UIAlertView showWarningAlertWithTitle:@"" message:errorMessage];
        return;
    }
    
    
    /*
    isValid = [newPasswordStr isMatchedByRegularExpression:PASSWORD_REGX];

    if(!isValid)
    {
        NSLog(@"New password should include atleast one from each (A-Z), (0-9), (a-z) and special characters");
        errorMessage = @"New password should include atleast one from each (A-Z), (0-9), (a-z) and special characters";
        [UIAlertView showWarningAlertWithTitle:@"" message:errorMessage];
        return;
    }
    */
    
    isValid = newPasswordStr.length>=6 && newPasswordStr.length<=10;
    if(!isValid)
    {
        NSLog(@"New password should be between 6 to 10 characters.");
        errorMessage = @"New password should be between \n6 to 10 characters.";
        [UIAlertView showWarningAlertWithTitle:@"" message:errorMessage];
        return;
    }

    
   // [self cancelAction:sender];

    
    //======================= NOW SEND REQUEST FOR CHANGE PASSWORD ====================================
    
    ASIHTTPRequest *request = [ConnectionManager createResetPasswordConnectionWithPassword:newPasswordStr forUserID:rep.userID];
    request.delegate = self;
    self.passwordChangeRequest = request;
    
    if(request)
    {
        sender.enabled = NO;
        self.tableView.userInteractionEnabled = NO;
        ProgressHUD *hud = [ProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Changing..";
        
        [self setAssociateObject:newPasswordStr forKey:@"newPassword"];
    }
    
    /*
    __block AccountSettingsViewController  *weakSelf = self;
    [request setCompletionBlock:^{
        
        NSDictionary *json = request.responseJSON;
        NSLog(@"Response:%@",json);
        int status = [[json valueForKey:@"status"] intValue];
        
        if(status == 1)
        {
         weakSelf->rep.password = newPasswordStr;
         [[NSUserDefaults standardUserDefaults] setObject:newPasswordStr forKey:LOGIN_PASSWORD_KEY];
         [ProgressHUD hideHUDForView:weakSelf.view animated:YES];
         sender.enabled = YES;
         weakSelf.tableView.userInteractionEnabled = YES;
         [weakSelf cancelAction:sender];
            
        }
        else 
        {
            [UIAlertView showWarningAlertWithTitle:ERROR_IN_CHANGING_PWD_HEADER message:ERROR_IN_CHANGING_PWD];
        }
    }];
    
    [request setFailedBlock:^{
        
         NSLog(@"Please %@",request.error.description);
        
        [ConnectionManager handleError:request.error];
        
        [ProgressHUD hideHUDForView:weakSelf.view animated:YES];
         sender.enabled = YES;
         weakSelf.tableView.userInteractionEnabled = YES;
         //[self cancelAction:sender];
    }];
    */
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSDictionary *json = request.responseJSON;
    NSLog(@"Response:%@",json);
    
    json = NULL_NIL(json);
    

     NSNumber *isUnderMaintenance  = [json objectForKey:JSON_UNDER_MAINTENANCE_KEY];
     
     isUnderMaintenance = NULL_NIL(isUnderMaintenance);
     
     if([isUnderMaintenance boolValue])
     {
         [ProgressHUD hideHUDForView:self.view animated:YES];
         [app showUnderMaintainenceScreen:YES];
         
         return;
     }
    
    int status = [[json valueForKey:@"status"] intValue];
    
    if(status == 1)
    {
        UIButton *submitButton =(UIButton *)[self.tableView.tableFooterView viewWithTag:2];
        NSString *newPasswordStr = [self associateObjectForKey:@"newPassword"];
        rep.password = newPasswordStr;
        if(newPasswordStr)
        [[NSUserDefaults standardUserDefaults] setObject:newPasswordStr forKey:LOGIN_PASSWORD_KEY];
        [ProgressHUD hideHUDForView:self.view animated:YES];
        submitButton.enabled = YES;
        self.tableView.userInteractionEnabled = YES;
        [self setAssociateObject:nil forKey:@"newPassword"];
        [self cancelAction:submitButton];
    }
    else 
    {
         [UIAlertView showWarningAlertWithTitle:ERROR_IN_CHANGING_PWD_HEADER message:ERROR_IN_CHANGING_PWD];
         [ProgressHUD hideHUDForView:self.view animated:YES];
         UIButton *submitButton =(UIButton *)[self.tableView.tableFooterView viewWithTag:2];
         submitButton.enabled = YES;
         self.tableView.userInteractionEnabled = YES;
        
    }
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"Please %@",request.error.description);
    
     UIButton *submitButton =(UIButton *)[self.tableView.tableFooterView viewWithTag:2];
    
   // [ConnectionManager handleError:request.error];
   
    [ProgressHUD hideHUDForView:self.view animated:YES];
    submitButton.enabled = YES;
    self.tableView.userInteractionEnabled = YES;
    //[self cancelAction:sender];
     [UIAlertView showWarningAlertWithTitle:@"" message:NETWORK_NOT_AVAILABLE_MSG];
}

-(void)signoutAction:(id)sender
{
    [self.popoverController dismissPopoverAnimated:NO];
    [app appDidLogOut];
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;

    
   // [self.navigationController.navigationBar addBackGroundImage:@"header_blank.png"];
    
    [self addTitleImage:@"account_settings.png"];
    
    UIBarButtonItem *closeButton = nil;
    
  if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
  {
    UIButton *btnDone = [UIButton repToolButtonWithImageName:NAV_DONE_BUTTON text:@"Close"];
    btnDone.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
    [btnDone addTarget:self
                action:@selector(closeAction:)
      forControlEvents:UIControlEventTouchUpInside];
    closeButton=[[UIBarButtonItem alloc]initWithCustomView:btnDone];
  }
else
{
    closeButton=[[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeAction:)];
}
    self.navigationItem.rightBarButtonItem=closeButton;
    [closeButton release];

    
    [self setupFooterView];
    
    rep=[[CoreDataHandler currentRep] retain];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.tableView.sectionFooterHeight = 1.0;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 10.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        self.tableView.tableHeaderView = _headerView;
        [_headerView release];
    }


}

-(void)closeAction:(id)sender
{
    [self.popoverController dismissPopoverAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSInteger _numRows=3;
    return _numRows;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *title = (section==0) ? @"LOGGED IN AS" : @"CHANGE PASSWORD";
    if(!title.length) return nil;
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 40.0)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.font = [UIFont subaruBoldFontOfSize:23.0];
    headerLabel.shadowColor  = [UIColor lightGrayColor];
    headerLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    headerLabel.text =[NSString stringWithFormat:@"    %@",title];
    
    return [headerLabel autorelease];
    
}
/*
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return (section==0) ? @"LOGGED IN AS" : @"CHANGE PASSWORD";
}
*/
-(NSString *)titleTextForIndexPath:(NSIndexPath *)indexPath
{
    NSString *title=nil;
    
    switch (indexPath.section) 
    {
        case 0:
        {
            switch (indexPath.row) 
            {
                case 0:
                  title  = @"Email";  
                break;
                case 1:
                   title = @"Name"; 
                break;
                case 2:
                   title = @"Surname"; 
                break;
                    
                default:
                break;
            }
        }
        break;
            
        case 1:
        {
        
            switch (indexPath.row) 
            {
                case 0:
                    title = @"Old";
                break;
                case 1:
                    title = @"New";
                break;
                case 2:
                    title = @"Confirm";
                break;
                default:
                break;
            }
        }
        break;
    }

    return title;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
  static   NSString *CellIdentifier1  = @"Cell_1";
  static   NSString *CellIdentifier2  = @"Cell_2";
    
    UITableViewCell *cell=nil;
    
    if(indexPath.section==0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier1] autorelease];
            cell.detailTextLabel.textAlignment=NSTextAlignmentRight;
            cell.detailTextLabel.textColor=[UIColor lightGrayColor];
            cell.textLabel.textColor=[UIColor lightGrayColor];
            cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:17.0];
            cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:13.0];
        }
        
        
        switch (indexPath.row) 
        {
            case 0:
            {
                cell.detailTextLabel.text=rep? rep.email :  @"johnsmith@subaru.com";
            }
                break;
            case 1:
            {
                cell.detailTextLabel.text= rep? rep.firstName : @"N/A";
            }
                break;
            case 2:
            {
                cell.detailTextLabel.text= rep? rep.lastName : @"N/A";
            }
                break;
                
            default:
                break;
        }
    }
    else
    {
        UITextField *textField=nil;
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            
            cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:17.0];
            
            textField=[UITextField textFieldWithFrame:CGRectMake(0, 2, 200, CGRectGetHeight(cell.frame)-5) font:12 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
            textField.font = [UIFont grotesqueFontOfSize:13.0];
            textField.textAlignment   = NSTextAlignmentLeft;
            textField.delegate = self;
            textField.placeholder     = @"enter";
            textField.secureTextEntry = YES;
            
            [_resuseDict setObject:textField forKey:indexPath];
        }
        else
        {
            textField=(UITextField *)cell.accessoryView;
            [textField removeFromSuperview];
        }
        
        textField=(UITextField *)[_resuseDict objectForKey:indexPath];
         cell.accessoryView=textField;
        textField.text=textField.text;
    }   
    
    cell.textLabel.text = [self titleTextForIndexPath:indexPath];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.tag = 1;
    
    
    int numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
       
   // [app appDidLogOut];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text=textField.text;
    NSString *replacedString = [text stringByReplacingCharactersInRange:range withString:string];
    
    if(textField.text.length>replacedString.length)
    {
        //FOR DELETE TEXT ACTION
        return YES;
    }
    else if(replacedString.length)
    {
     replacedString = [replacedString trimmedString];
    
    if(!replacedString.length) return NO;
    
    if(replacedString.length>MAX_PASSWORD_LENGTH) return NO;
    }
    
    return YES;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField 
{
    BOOL _shouldReturn = YES;
    
    for(NSIndexPath *keyIndexPath in _resuseDict)
    {
        UITextField *nextTextField = [_resuseDict objectForKey:keyIndexPath];
        
        if([nextTextField isEqual:textField] || nextTextField.text.length) 
        {
            _shouldReturn = textField.text.length>0;
            continue;
        }
        
        if([nextTextField canBecomeFirstResponder])
        {
            [nextTextField becomeFirstResponder];
            _shouldReturn=NO;
            break;
        }
    }

    if(_shouldReturn)
    [textField resignFirstResponder];
    return YES;
}

@end
