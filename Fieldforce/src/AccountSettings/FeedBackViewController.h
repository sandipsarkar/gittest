//
//  FeedBackViewController.h
//  RepVisitationTool
//
//  Created by ; Dey on 1/2/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedBackViewController : UIViewController<UITextViewDelegate,UITextFieldDelegate>
{
    UITextView *captionTextView;
    UIButton *selectCategoryButton;
    float locY;
    float locX;
    UIScrollView *scrollView;
    UIPopoverController *categoryPopover;
    BOOL isTextView;
    UITextField *txtSubject;
    NSString *categoryID;
    NSString *categoryName;
    UIButton *cancelButton;
    UIButton  *saveButton;
    
}
@property(nonatomic,retain)UIPopoverController *categoryPopover;
@property(nonatomic,retain) NSString *categoryID;
@property(nonatomic,retain) NSString *categoryName;

@end
