//
//  FeedBackCategoryViewController.h
//  RepVisitationTool
//
//  Created by Subhojit Dey on 1/2/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^DidSelectCategoryCallback)(NSString *categoryId,NSString *categoryName);
typedef void (^DidLoadCategoryCallback)(BOOL success);

@protocol FeedbackCategoryDelegate;

//typedef void (^DidSelectCategoryCallback)(NSString *categoryName);
@interface FeedBackCategoryViewController : UITableViewController
{
    
      UIActivityIndicatorView *_activityIndicator;
    
      NSMutableArray *arrParentFilter;//USED FOR FILTER WITH PARENTID 0
    
      NSMutableArray *arrIsSelectable;//USED FOR FILTER WITH ISSELECTABLE 0

      NSMutableArray *arrIsSelectectableSelected;//USED FOR FILTER WITH ISSELECTABLE 1
    
      NSMutableArray *arrChild; //USED FOR FILTER WITH CHILED WHOSE PARENTID NOT 0

    
}
@property(nonatomic,retain) NSMutableArray *dataSource;//USED AS MAIN ARRAY
@property(nonatomic,retain) NSMutableArray *arrParentFilter;
@property(nonatomic,retain) NSMutableArray *arrIsSelectable;
@property(nonatomic,retain) NSMutableArray *arrIsSelectectableSelected;
@property(nonatomic,retain) NSMutableArray *arrChild;
@property(nonatomic,assign) id < FeedbackCategoryDelegate > delegate;



-(void)addDidSelectCategoryCallback:(DidSelectCategoryCallback)callback;
-(void)addDidLoadCategoryCallback:(DidLoadCategoryCallback )callback;

@end

@protocol FeedbackCategoryDelegate <NSObject>

@optional

-(void)feedbackCategoryController:(FeedBackCategoryViewController *)feedbackCategoryVC didSelectCategory:(NSString *)categoryName categoryID:(NSString *)categoryId;

-(void)feedbackCategoryController:(FeedBackCategoryViewController *)feedbackCategoryVC didLoad:(BOOL)success;

@end
