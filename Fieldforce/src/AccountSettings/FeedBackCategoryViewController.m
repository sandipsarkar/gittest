//
//  FeedBackCategoryViewController.m
//  RepVisitationTool
//
//  Created by Subhojit Dey on 1/2/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "FeedBackCategoryViewController.h"
#import "Rep.h"
#import "WholesaleDealer.h" 
#import "OfflineStore.h"
#import "InsetTableViewCell.h"

////NSPredicate *filterPredicate = nil;
// filterPredicate = [NSPredicate predicateWithFormat:@"self.createdByUserName = %@",oemName];

@interface FeedBackCategoryViewController ()

@property(nonatomic,copy) DidSelectCategoryCallback didSelectCategoryCallback;
@property(nonatomic,copy) DidLoadCategoryCallback didLoadCategoryCallback;

@property (nonatomic,retain) ASIHTTPRequest *connectionRequest;

@end

@implementation FeedBackCategoryViewController
@synthesize dataSource;
@synthesize arrParentFilter,arrIsSelectable,arrIsSelectectableSelected,arrChild;
@synthesize didSelectCategoryCallback;
@synthesize didLoadCategoryCallback;
@synthesize connectionRequest;
@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self)
    {
       
        
    }
    return self;
}
-(void)dealloc
{
  if(connectionRequest)
  {
      [connectionRequest clearDelegatesAndCancel];
      [connectionRequest release];
       connectionRequest = nil;
  }   
    
    delegate = nil;
    [dataSource release];
    [arrChild release];
    [arrParentFilter release];
    [arrIsSelectable release];
    [arrIsSelectectableSelected release];
    [_activityIndicator release];
    
    [didSelectCategoryCallback release];
    [didLoadCategoryCallback release];
    
    didSelectCategoryCallback = nil;
    didLoadCategoryCallback = nil;
    
    [super dealloc];
    
}
-(void)addDidSelectCategoryCallback:(DidSelectCategoryCallback)callback
{
    self.didSelectCategoryCallback = callback;
}

-(void)addDidLoadCategoryCallback:(DidLoadCategoryCallback )callback
{
    self.didLoadCategoryCallback = callback;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif

    

      //self.tableView.hidden=YES;
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _activityIndicator.hidesWhenStopped = YES;
    [self.tableView addSubview:_activityIndicator];
    _activityIndicator.center = self.tableView.center;
    _activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    [_activityIndicator startAnimating];
    
    // Custom initialization
    UIView *bgView = [[UIView alloc] init];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.opaque = YES;
    self.tableView.backgroundView = bgView;
    [bgView release];
    
     self.tableView.bounces = NO;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 10.0)];
        bgView.backgroundColor = [UIColor clearColor];
        self.tableView.tableHeaderView = bgView;
        [bgView release];
        
        self.tableView.backgroundColor=[UIColor groupTableViewBackgroundColor];;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }

    [self fetchCategoryFromServer];
    [self fetchCategory];
    
    
}

-(void)calculateSections
{
    if([self.dataSource count])
    {
        if(self.didLoadCategoryCallback) self.didLoadCategoryCallback(YES);
        
        if(self.delegate && [self.delegate respondsToSelector:@selector(feedbackCategoryController: didLoad:)])
        {
            [self.delegate feedbackCategoryController:self didLoad:YES];
        }
    }
    
    NSPredicate *filterPredicate=[NSPredicate predicateWithFormat:@"parentID=%@",[NSNumber numberWithInt:0]];
    
    self.arrParentFilter= (NSMutableArray *)[self.dataSource filteredArrayUsingPredicate:filterPredicate];
    
    NSPredicate *filterPredicateisSelected=[NSPredicate predicateWithFormat:@"isSelectable=%@",[NSNumber numberWithInt:0]];
    
    self.arrIsSelectable = (NSMutableArray *)[arrParentFilter filteredArrayUsingPredicate:filterPredicateisSelected];
    
    
    NSPredicate *filterPredicateisSelectedone=[NSPredicate predicateWithFormat:@"isSelectable=%@",[NSNumber numberWithInt:1]];
    
    NSLog(@" arrIsSelectable 0 %@",  self.arrIsSelectable);
    
    
    self.arrIsSelectectableSelected=(NSMutableArray *)[self.arrParentFilter filteredArrayUsingPredicate:filterPredicateisSelectedone];
    
    
    NSLog(@" arrIsSelectectableSelected %@",  self.arrIsSelectectableSelected);
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    self.arrChild=arr;
    [arr release];
    for(NSUInteger i=0;i<[self.arrIsSelectable count];i++)
    {
        NSPredicate *filter=[NSPredicate predicateWithFormat:@"parentID=%@",[NSNumber numberWithInt:[[[self.arrIsSelectable objectAtIndex:i]valueForKey:@"feedbackCategoryID"]intValue]]];
        
        //NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        
        NSMutableArray *arrLoc = (NSMutableArray *)[self.dataSource filteredArrayUsingPredicate:filter];
        
        [self.arrChild addObject:arrLoc];
        
    }
    
    [self.tableView reloadData];
}

-(void)fetchCategory
{
    OfflineStore *_offlineStore = [CoreDataHandler offlineStore];
    
    self.dataSource = [_offlineStore feedbackCategory];
    
    [self calculateSections];
    
   
}
-(void)fetchCategoryFromServer
{
   
    Rep *_rep = [CoreDataHandler currentRep];
    NSNumber *wdSiteID = _rep.parentWdSite.wdSiteID;
    
    ASIHTTPRequest *request = [ConnectionManager getFeedbackCategory:_rep.repID wdSiteID:wdSiteID];
    request.delegate = self;
    self.connectionRequest = request;
    
    /*
    [request setCompletionBlock:^{
        
        NSString *jsonString = [request responseString];
        NSLog(@"feedback json:%@",jsonString);
        
        NSDictionary *jsonDict = [request responseJSON];
        
         NSLog(@"feedback jsonDict:%@",jsonDict);
        BOOL _isSiteIDChanged = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
        if(_isSiteIDChanged)
        {
            [app appDidChangeSiteID];
        }
        else
        {
            
            self.dataSource = [jsonDict valueForKey:JSON_DETAILS_KEY];
            [self calculateSections];
              NSLog(@" self.dataSource %@",  self.dataSource);
            [_activityIndicator stopAnimating];
           
             OfflineStore *_offlineStore = [CoreDataHandler offlineStore];
            _offlineStore.feedbackCategory = self.dataSource;
        }

    }];
    
    [request setFailedBlock:^{
        
        [_activityIndicator stopAnimating];
        
        //[ConnectionManager handleError:request.error];
        if (!self.dataSource.count)
        {
            UIAlertView *alert=[UIAlertView alertViewWithTitle:nil message:POOR_CONNECTION_MSG_FOR_DATA_LOAD_FAILED cancelButtonTitle:@"Ok" otherButtonTitles:nil
                 onDismiss:^(int buttonIndex)
                {
                }
              onCancel:^{
                  if(self.didLoadCategoryCallback) self.didLoadCategoryCallback(NO);
              }];
            [alert release];
        }
    }];
     */

}

- (void)requestFinished:(ASIHTTPRequest *)request;
{
    NSString *jsonString = [request responseString];
    NSLog(@"feedback json:%@",jsonString);
    
    NSDictionary *jsonDict = [request responseJSON];
    
    NSLog(@"feedback jsonDict:%@",jsonDict);
    
    /*
    BOOL _isSiteIDChanged = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
    BOOL _isRepDeactivated = [[jsonDict valueForKey:JSON_REP_DEACTIVATE_KEY] boolValue];
    
    NSNumber *isUnderMaintenance  = [jsonDict objectForKey:JSON_UNDER_MAINTENANCE_KEY];
    
    isUnderMaintenance = NULL_NIL(isUnderMaintenance);
    
    if(_isSiteIDChanged)
    {
        [app appDidChangeSiteID];
    }
    else if(_isRepDeactivated)
    {
        [app repDidDeactivate];
    }

    else if([isUnderMaintenance boolValue])
    {
        [app showUnderMaintainenceScreen:YES];
    }
    else
    */
    BOOL _isValidResponse = [ConnectionManager isValidResponse:jsonDict];
    if(jsonDict && _isValidResponse)
    {
        self.dataSource = [jsonDict valueForKey:JSON_DETAILS_KEY];
        [self calculateSections];
        NSLog(@" self.dataSource %@",  self.dataSource);
        [_activityIndicator stopAnimating];
        
        OfflineStore *_offlineStore = [CoreDataHandler offlineStore];
        _offlineStore.feedbackCategory = self.dataSource;
    }
}


- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    [_activityIndicator stopAnimating];
    
    //[ConnectionManager handleError:request.error];
    if (!self.dataSource.count)
    {
        UIAlertView *alert=[UIAlertView alertViewWithTitle:nil message:POOR_CONNECTION_MSG_FOR_DATA_LOAD_FAILED cancelButtonTitle:@"Ok" otherButtonTitles:nil
                        onDismiss:^(int buttonIndex)
                            {
                            }
                            onCancel:^{
                                
                                if(self.didLoadCategoryCallback) self.didLoadCategoryCallback(NO);
                                
                                 if(self.delegate && [self.delegate respondsToSelector:@selector(feedbackCategoryController: didLoad:)])
                                 {
                                     [self.delegate feedbackCategoryController:self didLoad:NO];
                                 }
                            }];
        [alert release];
    }
}
    



-(void)hidHud
{
    [ProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.arrIsSelectable count]+1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if(section==0)
    {
      return [self.arrIsSelectectableSelected count];
    }
    else{
        
        //NSLog(@" hhh %d",[[arrChild objectAtIndex:section-1]count]);
        return [[self.arrChild objectAtIndex:section-1]count];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    if(section==0)
        return 0.0;
    else
        return 40.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 40.0)];
    if(section==0)
    {
        
    }
    else
    {
       
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.textColor = [UIColor blackColor];
        headerLabel.font = [UIFont subaruBoldFontOfSize:18.0];
        headerLabel.shadowColor  = [UIColor lightGrayColor];
        headerLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    
        NSString *strTitle=@"    ";
       headerLabel.text =[strTitle stringByAppendingString:[[self.arrIsSelectable objectAtIndex:section-1]valueForKey:@"categoryName"]];
       
    }
    return [headerLabel autorelease];

    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];

    
    static NSString *CellIdentifier = @"Cell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    UITableViewCell* cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(!cell)
    {
        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewStylePlain reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
    }
    
    
    if(indexPath.section==0)
    {
        cell.textLabel.text = [[[self.arrIsSelectectableSelected objectAtIndex:indexPath.row]valueForKey:@"categoryName"] capitalizedString];
    }
    else{
        
        cell.textLabel.text =[[[self.arrChild objectAtIndex:indexPath.section-1]objectAtIndex:indexPath.row]valueForKey:@"categoryName"];
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor groupTableViewBackgroundColor];

    
    CellBackgroundView *_selectedBgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _selectedBgView.backgroundColor = [UIColor clearColor];
    _selectedBgView.contentInset = CGPointMake(10.0, 0.0);
    _selectedBgView.cornerRadius = 8.0;
    _selectedBgView.borderColor = self.tableView.separatorColor;
    _selectedBgView.fillColor = [UIColor cellSelectedColorBlue];
    
    
    NSUInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    _selectedBgView.position = _bgView.position;
    
    cell.backgroundView = _bgView;
    cell.selectedBackgroundView = _selectedBgView;
    
    [_bgView release];
    [_selectedBgView release];
}

/*
- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor  = [UIColor whiteColor];
    
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor  = [UIColor blackColor];
}
*/

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.contentSizeForViewInPopover = CGSizeMake(335, MAX(300,self.tableView.contentSize.height)-2.0);
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
   // self.contentSizeForViewInPopover = CGSizeMake(320.0, MAX(250.0,self.tableView.contentSize.height));
    
    //self.contentSizeForViewInPopover = CGSizeMake(335, MAX(300,self.tableView.contentSize.height+self.tableView.contentInset.top));
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strID=nil;
    NSString *strName=nil;
    if(indexPath.section==0)
    {
        strName=[[[self.arrIsSelectectableSelected objectAtIndex:indexPath.row]valueForKey:@"categoryName"] capitalizedString];
        strID=[[self.arrIsSelectectableSelected objectAtIndex:indexPath.row]valueForKey:@"feedbackCategoryID"];
    }
    else{
        
        strName=[[[[self.arrChild objectAtIndex:indexPath.section-1]objectAtIndex:indexPath.row]valueForKey:@"categoryName"]capitalizedString];
        strID=[[[self.arrChild objectAtIndex:indexPath.section-1]objectAtIndex:indexPath.row]valueForKey:@"feedbackCategoryID"];

        
    }
 
   if(self.didSelectCategoryCallback) self.didSelectCategoryCallback(strID,strName);
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(feedbackCategoryController: didSelectCategory: categoryID:)])
    {
        [self.delegate feedbackCategoryController:self didSelectCategory:strName categoryID:strID];
    }
   
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
