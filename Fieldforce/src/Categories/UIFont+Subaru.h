//
//  UIFont+Subaru.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 21/08/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Subaru)

+(UIFont *)subaruThinFontOfSize:(CGFloat)fontSize;
+(UIFont *)subaruMediumFontOfSize:(CGFloat)fontSize;
+(UIFont *)subaruBookFontOfSize:(CGFloat)fontSize;
+(UIFont *)subaruBoldFontOfSize:(CGFloat)fontSize;
+(UIFont *)grotesqueFontOfSize:(CGFloat)fontSize;
+(UIFont *)grotesqueBoldFontOfSize:(CGFloat)fontSize;
+(UIFont *)grotesqueItalicFontOfSize:(CGFloat)fontSize;
+(UIFont *)helveticaNeueLTStdBDOfSize:(CGFloat)fontSize;
@end
