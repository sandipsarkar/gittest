//
//  NSDictionary+ValidateValue.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 21/02/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "NSDictionary+ValidateValue.h"



@implementation NSMutableDictionary (ValidateValue)

- (void)setObjectOrNull:(id)anObject forKey:(id)aKey
{
    id  goodObject = anObject ? anObject : [NSNull null];
    
    [self setObject:goodObject forKey:aKey];
}

@end

@implementation NSDictionary (ValidateValue)

- (id)objectOrNilForKey:(id)aKey;
{
    id  anObject = [self objectForKey:aKey];
    
    if (anObject == [NSNull null])
        return (nil);
    
    return (anObject);
}

@end



