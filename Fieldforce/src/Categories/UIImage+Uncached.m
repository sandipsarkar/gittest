//
//  UIImage+Uncached.m
//  RepVisitationTool
//
//  Created by Some Sankar Ray on 10/04/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "UIImage+Uncached.h"

@implementation UIImage (Uncached)

/*
- (id)initWithContentsOfResolutionIndependentFile:(NSString *)path
{
    BOOL isRetina = ([UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0);
        
       if(isRetina)
       {
           
        NSString *path2x = [[path stringByDeletingLastPathComponent]
                            stringByAppendingPathComponent:[NSString stringWithFormat:@"%@@2x.%@",
                                                            [[path lastPathComponent] stringByDeletingPathExtension],
                                                            [path pathExtension]]];
           NSLog(@"Image Path=%@",path2x);
        if ( [[NSFileManager defaultManager] fileExistsAtPath:path2x] )
        {
            NSData *imageData = [NSData dataWithContentsOfFile:path2x];
            UIImage *image = imageData.length ? [UIImage imageWithData:imageData] : nil;
            
            return image ? [self initWithCGImage:image.CGImage scale:2.0 orientation:UIImageOrientationUp] : nil;
        }
      }
    
    return [[UIImage alloc] initWithContentsOfFile:path];
}

+ (UIImage*)imageWithContentsOfResolutionIndependentFile:(NSString *)path
{
    return [[[UIImage alloc] initWithContentsOfResolutionIndependentFile:path] autorelease];
}
*/

+(UIImage *)imageNamedNoCache:(NSString *)imageName
{
    NSString *path= [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:imageName];;
    
    BOOL isRetina = ([UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0);
    
    if(isRetina)
    {
        
        NSString *path2x = [[path stringByDeletingLastPathComponent]
                            stringByAppendingPathComponent:[NSString stringWithFormat:@"%@@2x.%@",
                                                            [[path lastPathComponent] stringByDeletingPathExtension],
                                                            [path pathExtension]]];
        NSLog(@"Image Path=%@",path2x);
        if ( [[NSFileManager defaultManager] fileExistsAtPath:path2x] )
        {
            NSData *imageData = [NSData dataWithContentsOfFile:path2x];
            UIImage *image = imageData.length ? [UIImage imageWithData:imageData] : nil;
            
            return image ? [UIImage imageWithCGImage:image.CGImage scale:2.0 orientation:UIImageOrientationUp] : nil;
        }
    }

    
    return [UIImage imageWithContentsOfFile:path];
}


@end
