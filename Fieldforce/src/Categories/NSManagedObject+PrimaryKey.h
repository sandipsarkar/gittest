//
//  NSManagedObject+PrimaryKey.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 30/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (PrimaryKey)

-(NSString *)primaryKey;

@end
