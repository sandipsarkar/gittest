//
//  NSManagedObject+PrimaryKey.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 30/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "NSManagedObject+PrimaryKey.h"

@implementation NSManagedObject (PrimaryKey)

-(NSString *)primaryKey
{
    NSManagedObjectID *yourManagedObjectID = [self  objectID];
    NSString *yourManagedObject_PK = [[[[yourManagedObjectID URIRepresentation] absoluteString] lastPathComponent] substringFromIndex:1] ;
    return yourManagedObject_PK;
    
}


@end
