//
//  UIViewController+RepToolHeader.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 30/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "UIViewController+RepToolHeader.h"

@implementation UIViewController (RepToolHeader)

-(UIImageView *)pageHeaderViewForImageName:(NSString *)imageName
{
    //UIImage *image=[UIImage imageNamed:imageName];
    UIImage *image=[UIImage imageNamed:imageName];
    UIImageView *headerImageView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
   // headerImageView.contentMode=UIViewContentModeScaleAspectFit;
    headerImageView.image=image;

    return [headerImageView autorelease];
}

-(void)addBorderWithColor:(UIColor *)borderColor borderWidth:(float)borderWidth
{
    self.view.layer.borderWidth=borderWidth;
    self.view.layer.borderColor=borderColor.CGColor;
}

-(void)addDefaultBorder
{
    [self addBorderWithColor:[UIColor lightGrayColor] borderWidth:0.8];
}

-(void)addBackButtonWithSelector:(SEL)aSelector
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
   
    
    //if(![self isKindOfClass:[UINavigationController class]]) return;
    
    UIImage *backButtonImage =  [UIImage imageNamed:@"back_button.png"];
    UIButton *_backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    _backButton.frame=CGRectMake(0, 0, backButtonImage.size.width, backButtonImage.size.height);
 
    [_backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
    [_backButton addTarget:self action:(aSelector && ([self respondsToSelector:aSelector]))?aSelector:@selector(_backButtonAction) forControlEvents:UIControlEventTouchUpInside];
   
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc] initWithCustomView:_backButton];
    self.navigationItem.leftBarButtonItem=backButton;
    [backButton release];
}

-(void)addBackButtonWithImage:(UIImage *)image selector:(SEL)aSelector
{
    if(!image) return;
    //if(![self isKindOfClass:[UINavigationController class]]) return;
    
    UIButton *_backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    _backButton.frame=CGRectMake(0, 0, image.size.width, image.size.height);
    [_backButton setBackgroundImage:image forState:UIControlStateNormal];
    [_backButton addTarget:self action:(aSelector && ([self respondsToSelector:aSelector]))?aSelector:@selector(_backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc] initWithCustomView:_backButton];
    self.navigationItem.leftBarButtonItem=backButton;
    [backButton release];
}

-(void)addBackButtonWithImage:(UIImage *)image title:(NSString *)title selector:(SEL)aSelector
{
    if(!image) return;
    
    UIButton *_backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    
    _backButton.frame=CGRectMake(0, 0, image.size.width, image.size.height);
    [_backButton setBackgroundImage:image forState:UIControlStateNormal];
    _backButton.titleLabel.textColor = [UIColor whiteColor];
    [_backButton setTitle:title forState:UIControlStateNormal];
    _backButton.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
    _backButton.titleLabel.shadowColor = [UIColor colorWithWhite:0.1 alpha:0.5];
    _backButton.titleLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    _backButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _backButton.contentEdgeInsets = UIEdgeInsetsMake(0, 8.0, 0, 0);
    
    [_backButton addTarget:self action:(aSelector && ([self respondsToSelector:aSelector]))?aSelector:@selector(_backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc] initWithCustomView:_backButton];
    self.navigationItem.leftBarButtonItem=backButton;
    [backButton release];
}


-(void)addBackButton
{
    [self addBackButtonWithSelector:nil];
}

-(void)_backButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)addDefaultNavigationBackgroundImage
{
    if(self.navigationController)
    [self.navigationController.navigationBar addBackGroundImage:@"header_blank.png"];
}

-(void)addTitleImage:(NSString *)imageName
{
    UIImage *image=[UIImage imageNamed:imageName];
    
    UIImageView *imageView=(UIImageView *)self.navigationItem.titleView;
    
    if(!imageView)
    {
     imageView=[[UIImageView alloc] initWithImage:image];
     imageView.frame=CGRectMake(0, 0, image.size.width, image.size.height);
     self.navigationItem.titleView=imageView;
     [imageView release];
    }
    else if([imageView isKindOfClass:[imageView class]])
    {
         imageView.image=image;
    }
}

-(void)addTitle:(NSString *)title withFont:(UIFont *)font
{
    //self.navigationItem.title= title;
    
  if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
  {
    self.edgesForExtendedLayout = UIRectEdgeNone;
  }


   float  maxWidth =self.navigationController.navigationBar.frame.size.width-50.0;
    
    if(maxWidth<=0) maxWidth = 500;
    
    CGSize _textSize = [title sizeWithFont:font constrainedToSize:CGSizeMake(maxWidth, 40.0)];
    UILabel *label =(UILabel *) self.navigationItem.titleView;
    if(!label)
    {
        label=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, _textSize.width, 40.0)]; //450
        label.text = title;
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        label.font = font;
        label.shadowColor = [UIColor colorWithWhite:0.1 alpha:0.5];
        label.shadowOffset = CGSizeMake(1.0, 2.0);
        self.navigationItem.titleView = label;
        [label release];
    }
    else if([label isKindOfClass:[UILabel class]])
    {
        CGRect frm = label.frame;
        frm.size.width = _textSize.width;
        label.frame = frm;
        label.text =title;
    }
    
    
}

-(void)addTitle:(NSString *)title withFont:(UIFont *)font color:(UIColor *)color
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    float  maxWidth =self.navigationController.navigationBar.frame.size.width-50.0;
    
    if(maxWidth<=0) maxWidth = 500;
    
    CGSize _textSize = [title sizeWithFont:font constrainedToSize:CGSizeMake(maxWidth, 40.0)];
    UILabel *label =(UILabel *) self.navigationItem.titleView;
    if(!label)
    {
        label=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, _textSize.width, 40.0)]; //450
        label.text = title;
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        label.textColor = color ? color : [UIColor whiteColor];
        label.font = font;
        label.shadowColor = [UIColor colorWithWhite:0.1 alpha:0.5];
        label.shadowOffset = CGSizeMake(1.0, 2.0);
        self.navigationItem.titleView = label;
        [label release];
    }
    else if([label isKindOfClass:[UILabel class]])
    {
        CGRect frm = label.frame;
        frm.size.width = _textSize.width;
        label.frame = frm;
        label.text =title;
    }
    
}

-(void)addRefreshButtonWithSelector:(SEL)aSelector
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    
    UIButton *_backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    _backButton.frame=CGRectMake(0, 0, 61, 38);
    [_backButton setBackgroundImage:[UIImage imageNamed:@"refresh_button.png"] forState:UIControlStateNormal];
    
    if([self respondsToSelector:aSelector])
    [_backButton addTarget:self action:aSelector forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc] initWithCustomView:_backButton];
    self.navigationItem.rightBarButtonItem=backButton;
    [backButton release];

}

-(void)addEditButtonWithSelector:(SEL)aSelector
{
    UIImage *editButImage = [UIImage imageNamed:@"edit_blank.png"];
    //UIImage *editButImage = [UIImage imageNamed:@"edit_button_blue.png"];
    UIButton *editButton=[UIButton buttonWithType:UIButtonTypeCustom];
    editButton.frame=CGRectMake(0, 0, editButImage.size.width, editButImage.size.height);
    [editButton setBackgroundImage:editButImage forState:UIControlStateNormal];
     [editButton setBackgroundImage:[UIImage imageNamed:@"edit_blank_selected.png"] forState:UIControlStateSelected];

    editButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
    [editButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [editButton setTitle:@"Edit" forState:UIControlStateNormal];
    [editButton setTitle:@"Save" forState:UIControlStateSelected];
    
   
    if([self respondsToSelector:aSelector])
        [editButton addTarget:self action:aSelector forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc] initWithCustomView:editButton];
    self.navigationItem.rightBarButtonItem=backButton;
    [backButton release];
    
}

-(UINavigationController *)nextResponderNavigationController
{
    UINavigationController *navController=nil;
    id nextResponder=[self nextResponder];
  
    do{
        if([nextResponder isKindOfClass:[UIViewController class]] && [nextResponder navigationController])
        {
            navController=(UINavigationController *)[nextResponder navigationController];
            break;
        }
        
     } while ((nextResponder=[nextResponder nextResponder]));
    
    return navController;
}

/*
- (void) willMoveToParentViewController:(UIViewController *)parent;
{
    if (parent == nil)
    {
        [self.view removeFromSuperview];
    }
}
*/


@end
