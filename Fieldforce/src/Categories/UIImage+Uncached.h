//
//  UIImage+Uncached.h
//  RepVisitationTool
//
//  Created by Some Sankar Ray on 10/04/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Uncached)

//- (id)initWithContentsOfResolutionIndependentFile:(NSString *)path;
//+ (UIImage*)imageWithContentsOfResolutionIndependentFile:(NSString *)path;

+(UIImage *)imageNamedNoCache:(NSString *)imageName;


@end
