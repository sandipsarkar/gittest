//
//  UITableView+Reload.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 01/10/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Reload)

-(void)reloadDataWithRowAnimation:(UITableViewRowAnimation)rowAnimation;

@end
