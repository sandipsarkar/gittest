//
//  NSString+Validation.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "NSString+Validation.h"

#define USE_REGEX_VALIDATION 1

@implementation NSString (Validation)


#if USE_REGEX_VALIDATION

-(BOOL)isValidEmail
{
    //NSString *emailRegex =@"/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/";
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:self];
}

#else
-(BOOL)isValidEmail
{
	NSString *unWantedInUName = @"~!@#$^&*()={}[]|;’:\\”<>,?/`";
	NSString *unWantedInDomain = @"~!@#$%^&*()={}[]|;’:\\”<>,+?/`";
	NSString *unWantedInSub = @"`~!@#$%^&*()={}[]:\\”;’<>,?/1234567890";
	
	BOOL valid=NO;
	
	if(!self.length) return NO;
	
	//// Devides the self in two parts i.e. username and domain name
	NSArray *emailParts=[self componentsSeparatedByString:@"@"];
	
	if([emailParts count]==2) 
    {
		NSString *emailAccountName=[emailParts objectAtIndex:0];
		NSString *emailDomain=[emailParts objectAtIndex:1];
		NSArray *domainParts=[emailDomain componentsSeparatedByString:@"."];
		
		if(emailAccountName.length<1||[emailAccountName characterAtIndex:0]=='.' 
		   || [emailAccountName characterAtIndex:emailAccountName.length-1]=='.'
		   || emailAccountName.length>64)
			return NO;
		
		int partsCount=[domainParts count];
		if(partsCount>=2) // && partsCount<4
        {
			valid=YES;
			NSString *domainName=[domainParts objectAtIndex:0];
			
			if(!domainName.length || [emailAccountName rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:unWantedInUName]].location!=NSNotFound 
			   ||[domainName rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:unWantedInDomain]].location!=NSNotFound )
				return NO;
			
			for(NSString *subDomain in domainParts)
            {
				if(![subDomain isEqualToString:domainName])
                {
					if(subDomain.length<2 || subDomain.length>6) valid=NO;
					if([subDomain isEqualToString:@""]||[subDomain rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:unWantedInSub]].location!=NSNotFound)
						valid=NO;
                }
            }
                
        }
    }
	
	
	return valid;
}

#endif

-(NSString *)trimmedString
{
     NSString *trimmedString=[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return trimmedString;
}

-(BOOL)isValid
{
    if([self isEqual:[NSNull null]]) return NO;
    
    NSString *_trimmedString=[self trimmedString];
    
    return _trimmedString.length;
}

-(BOOL)isMatchedByRegularExpression:(NSString *)regex
{
    if(!regex || !regex.length) return NO;
    
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return  [regExPredicate evaluateWithObject:self];
    
    return NO;
}

+(NSString *)globalIDString
{
    // NSUUID  *identifierForVendor  = [[UIDevice currentDevice] identifierForVendor];
    
    int random = arc4random_uniform(999999);
    
    CFUUIDRef uuidObj = CFUUIDCreate(nil);
    
#if !__has_feature(objc_arc)
    NSString *newUUID = ( NSString*)CFUUIDCreateString(nil, uuidObj);
#else
    NSString *newUUID = (__bridge_transfer NSString*)CFUUIDCreateString(nil, uuidObj);;
#endif
    
    
    NSString  *uuistr   = newUUID;
    NSUInteger intHash  = [uuistr hash];
    
    //NSString *uniqueStr = [NSString stringWithFormat:@"%u%u",[identifierForVendor hash] ,intHash];
    NSString *uniqueStr = [NSString stringWithFormat:@"%u%d",intHash,random];
    
    //  NSLog(@"uniqueName=%@ length=%d",uniqueStr,uniqueStr.length);
    
    CFRelease(uuidObj);
    
#if !__has_feature(objc_arc)
    [newUUID release];
#endif
    
    return uniqueStr;
}

@end
