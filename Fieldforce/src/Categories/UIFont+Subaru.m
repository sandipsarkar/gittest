//
//  UIFont+Subaru.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 21/08/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "UIFont+Subaru.h"

@implementation UIFont (Subaru)


+(UIFont *)subaruThinFontOfSize:(CGFloat)fontSize
{
    UIFont *font = [UIFont fontWithName:@"Subaru-Thin" size:fontSize];
    
    return font;
}

+(UIFont *)subaruMediumFontOfSize:(CGFloat)fontSize
{
    UIFont *font = [UIFont fontWithName:@"Subaru-Medium" size:fontSize];
    
    return font;
}

+(UIFont *)subaruBookFontOfSize:(CGFloat)fontSize
{
    UIFont *font = [UIFont fontWithName:@"Subaru-Book" size:fontSize];
    
    return font;
}

+(UIFont *)subaruBoldFontOfSize:(CGFloat)fontSize
{
    UIFont *font = [UIFont fontWithName:@"Subaru-Bold" size:fontSize];
    
    return font;
}

+(UIFont *)grotesqueFontOfSize:(CGFloat)fontSize
{
    UIFont *font = [UIFont fontWithName:@"GrotesqueMT" size:fontSize];
    
    return font;
}

+(UIFont *)grotesqueBoldFontOfSize:(CGFloat)fontSize
{
    UIFont *font = [UIFont fontWithName:@"GrotesqueMT-Bold" size:fontSize];
    
    return font;
}

+(UIFont *)grotesqueItalicFontOfSize:(CGFloat)fontSize
{
    UIFont *font = [UIFont fontWithName:@"GrotesqueMT-Italic" size:fontSize];
    
    return font;
}

+(UIFont *)helveticaNeueLTStdBDOfSize:(CGFloat)fontSize
{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeueLTStd-Bd" size:fontSize];
    
    return font;
}

@end
