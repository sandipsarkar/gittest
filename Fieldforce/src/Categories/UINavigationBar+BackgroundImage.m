#import "UINavigationBar+BackgroundImage.h"
#import "objc/runtime.h"
#include <QuartzCore/QuartzCore.h>

#define BGIMAGE_TAG 6183746
#define kSCNavigationBarTintColor [UIColor colorWithRed:47.0/255.0 green:122.0/255.0 blue:153.0/255.0 alpha:1.0]

@interface UINavigationBar()

- (void)scInsertSubview:(UIView *)view atIndex:(NSInteger)index;
- (void)scSendSubviewToBack:(UIView *)view;
@end

@implementation UINavigationBar (BackgroundImage)


+ (void)replaceSelector:(SEL)orig ofClass:(Class)c withSelector:(SEL)new;
{
    Method origMethod = class_getInstanceMethod(c, orig);
    Method newMethod = class_getInstanceMethod(c, new);
	
    if (class_addMethod(c, orig, method_getImplementation(newMethod), method_getTypeEncoding(newMethod)))
	 {
        class_replaceMethod(c, new, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
	 }
    else
	 {
        method_exchangeImplementations(origMethod, newMethod);
	 }
}


- (void)scInsertSubview:(UIView *)view atIndex:(NSInteger)index
{
    [self scInsertSubview:view atIndex:index];
	
    UIView *backgroundImageView = [self viewWithTag:BGIMAGE_TAG];
    if (backgroundImageView != nil)
    {
        [self scSendSubviewToBack:backgroundImageView];
    }
}

- (void)scSendSubviewToBack:(UIView *)view
{
    [self scSendSubviewToBack:view];
	
    UIView *backgroundImageView = [self viewWithTag:BGIMAGE_TAG];
    if (backgroundImageView != nil)
    {
        [self scSendSubviewToBack:backgroundImageView];
    }
}

-(void)addImage:(NSString *)aImageName
{
    //[self setTintColor:kSCNavigationBarTintColor];
	
    UIImageView *imageView = (UIImageView *)[self viewWithTag:BGIMAGE_TAG];
    if(imageView == nil)
	 {
		[UINavigationBar replaceSelector:@selector(insertSubview:atIndex:) ofClass:[self class] withSelector:@selector(scInsertSubview:atIndex:)];
		[UINavigationBar replaceSelector:@selector(sendSubviewToBack:) ofClass:[self class] withSelector:@selector(scSendSubviewToBack:)];
		
        imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:aImageName]];
		[imageView setTag:BGIMAGE_TAG];
         imageView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [self insertSubview:imageView atIndex:0];
        [imageView release];
	 }
	else
	 {
		imageView.image=[UIImage imageNamed:aImageName];
	 }
}

-(void)addBackGroundImage:(NSString *)aImageName
{
    if(!aImageName)return;
    
    if([self respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)])
    {
        //UIInterfaceOrientation _orientation=[UIApplication sharedApplication].statusBarOrientation;
     
        
        [self setBackgroundImage:[UIImage imageNamed:aImageName] forBarMetrics:UIBarMetricsDefault];
    }
    else
    [self.layer setContents:(id)[UIImage imageNamed:aImageName].CGImage];
}

-(void)hideBackgroundImage
{
    if([self respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)])
    {
        //UIInterfaceOrientation _orientation=[UIApplication sharedApplication].statusBarOrientation;
        
        [self setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    }

}



@end