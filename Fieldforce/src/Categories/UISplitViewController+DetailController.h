//
//  UISplitViewController+DetailController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 21/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISplitViewController (DetailController)

-(void)setDetailController:(UIViewController *)aDetailController;
-(void)setDetailController:(UIViewController *)aDetailController isNavigationController:(BOOL)yesOrNo;

-(UIViewController *)masterController;
-(UIViewController *)detailController;

@property(nonatomic,assign)CGFloat masterControllerWidth;

@end
