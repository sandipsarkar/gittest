//
//  UILabel+verticalAlignmentTop.h
//  BradescoRIApp-iPad
//
//  Created by Everton Cunha on 8/31/11.
//  Copyright 2011 Everton Postay Cunha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EPCLabel : UILabel {
	float originalPointSize;
	CGSize originalSize;
}

@property (nonatomic, readwrite) BOOL alignTextOnTop;
@end
