//
//  UIButton+RepToolButton.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 19/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "UIButton+RepToolButton.h"
#import "GlossyButton.h"

@implementation UIButton (RepToolButton)

+(UIButton *)repToolButtonWithText:(NSString *)text
{
    UIButton *button = [UIButton repToolButtonWithImageName:@"cancel_blank.png" text:text];
     
    return button;
}
+(UIButton *)repToolButtonWithImageName:(NSString *)imageName text:(NSString *)text
{
    if(!imageName.length) return nil;
    
    UIImage *buttonImage = [UIImage imageNamed:imageName];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setTitle:text forState:UIControlStateNormal];
    button.titleLabel.textColor = [UIColor whiteColor];
    button.titleLabel.font = [UIFont subaruBoldFontOfSize:18.0];
    button.titleLabel.shadowColor = [UIColor colorWithWhite:0.1 alpha:0.5];
    button.titleLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    return button;
}


+(UIButton *)repToolCancelButton
{
 return [self repToolButtonWithText:@"CANCEL"];
}

+(UIButton *)repToolSaveButton
{
return [self repToolButtonWithText:@"SAVE"];
}

+(UIButton *)glossyButtonWithFrame:(CGRect)frame color:(UIColor *)color text:(NSString *)text
{
    UIButton *button  = [[GlossyButton alloc] initWithFrame:frame withBackgroundColor:color];
    [button setTitle:text forState:UIControlStateNormal];
    button.titleLabel.textColor = [UIColor whiteColor];
    button.titleLabel.font = [UIFont subaruBoldFontOfSize:14.0];
    button.titleLabel.shadowColor = [UIColor colorWithWhite:0.1 alpha:0.5];
    button.titleLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    return button;
}

/*
- (UIEdgeInsets)alignmentRectInsets
{
   UINavigationBar *navBar = (UINavigationBar *)[self superViewOfType:[UINavigationBar class]];
    
    if([navBar class])
    {
        
        UIEdgeInsets insets;
        
        // IF_ITS_A_LEFT_BUTTON
        insets = UIEdgeInsetsMake(0, 9.0f, 0, 0);
        // else // IF_ITS_A_RIGHT_BUTTON
        // insets = UIEdgeInsetsMake(0, 0, 0, 9.0f);
        
        // self.contentEdgeInsets = UIEdgeInsetsMake(0, 9.0f, 0, 0);;
        
        return insets;
    }
    
    return [super alignmentRectInsets];
}
*/


@end
