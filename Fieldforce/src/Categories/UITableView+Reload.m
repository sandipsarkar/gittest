//
//  UITableView+Reload.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 01/10/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "UITableView+Reload.h"

@implementation UITableView (Reload)

-(void)reloadDataWithRowAnimation:(UITableViewRowAnimation)rowAnimation
{
    if(rowAnimation==UITableViewRowAnimationNone)
    {
        [self reloadData];
    }
    else
    {
        
        [self reloadData];
               
        CATransition *animation = [CATransition animation];
		animation.type = kCATransitionFade;
		animation.duration = 0.5;
		animation.timingFunction = [CAMediaTimingFunction
                                    functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
		animation.fillMode = @"extended";
		[self.layer addAnimation:animation forKey:@"reloadAnimation"];
        
        /*
        [self beginUpdates];
        NSInteger _numSections=[self numberOfSections];
        [self reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, _numSections-1)] withRowAnimation:rowAnimation];
        [self endUpdates];
       */
    }
}

@end
