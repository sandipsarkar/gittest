//
//  UIColor+Fieldforce.m
//  Fieldforce
//
//  Created by Randem IT on 13/09/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "UIColor+Fieldforce.h"

@implementation UIColor (Fieldforce)

+(UIColor *)cellSelectedColorBlue
{
  
    return  [UIColor colorWithRed:3.0/255.0 green:114.0/255.0 blue:237.0/255.0 alpha:1.0];
}

@end
