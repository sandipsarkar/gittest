#import <UIKit/UIKit.h>

@interface UINavigationBar (BackgroundImage)

- (void)addImage:(NSString *)aImageName __OSX_AVAILABLE_BUT_DEPRECATED(__MAC_NA,__MAC_NA,__IPHONE_2_0,__IPHONE_5_0); 
-(void)addBackGroundImage:(NSString *)aImageName;
-(void)hideBackgroundImage;

@end
