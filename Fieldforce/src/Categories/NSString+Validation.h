//
//  NSString+Validation.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Validation)

-(BOOL)isValidEmail;
-(BOOL)isValid;
-(NSString *)trimmedString;
-(BOOL)isMatchedByRegularExpression:(NSString *)regex;

+(NSString *)globalIDString;

@end
