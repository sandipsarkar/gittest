//
//  UIViewController+RepToolHeader.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 30/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (RepToolHeader)

-(void)addDefaultBorder;
-(void)addBorderWithColor:(UIColor *)borderColor borderWidth:(float)borderWidth;
-(UIImageView *)pageHeaderViewForImageName:(NSString *)imageName;

-(void)addBackButtonWithImage:(UIImage *)image selector:(SEL)aSelector;
-(void)addBackButtonWithImage:(UIImage *)image title:(NSString *)title selector:(SEL)aSelector;
-(void)addBackButtonWithSelector:(SEL)aSelector;
-(void)addBackButton;

-(void)addEditButtonWithSelector:(SEL)aSelector;
-(void)addRefreshButtonWithSelector:(SEL)aSelector;

-(void)addDefaultNavigationBackgroundImage;
-(void)addTitleImage:(NSString *)imageName;
-(void)addTitle:(NSString *)title withFont:(UIFont *)font;
-(void)addTitle:(NSString *)title withFont:(UIFont *)font color:(UIColor *)color;

-(UINavigationController *)nextResponderNavigationController;

@end
