//
//  AbstractTabBarController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 23/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "AbstractTabBarController.h"
#import "Customer.h"

#define BACKGROUND_COLOR [UIColor colorWithRed:0.9 green:0.9 blue:1 alpha:1]

@interface AbstractTabBarController()

@property(nonatomic,copy)ViewDidLoadCallback viewDidLoadCallback;
@end

@implementation AbstractTabBarController
@synthesize parentTabbarControlller;;
@synthesize customer;
@synthesize viewDidLoadCallback;

-(id)init
{
    if(self=[super init])
    {
        //from
        //NSArray *arr=[self viewControllers];
        //parentTabbarControlller=[[SSTabBarController alloc] initWithViewControllers:arr];
       // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SSTabBarDidSelectViewController:) name://SSTabBarDidSelectViewControllerNotification object:nil];

    }
    
    return self;
}

-(id)initWithCustomer:(Customer *)aCustomer
{
    if(self=[super init])
    {
        self.customer=aCustomer;
        
        
        NSArray *arr=[self viewControllers];
        parentTabbarControlller=[[SSTabBarController alloc] initWithViewControllers:arr];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SSTabBarDidSelectViewController:) name:SSTabBarDidSelectViewControllerNotification object:nil];
    }
    
    return self;
}

-(void)SSTabBarDidSelectViewController:(NSNotification *)notification
{
    UIViewController *selectedViewController=[notification object];
    [self didSelectViewController:selectedViewController];
}

-(void)didSelectViewController:(UIViewController *)controller
{
    
}

-(NSArray *)viewControllers
{
    return nil;
}

-(void)addViewDidLoadCallback:(ViewDidLoadCallback)callback
{
    self.viewDidLoadCallback=callback;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor=BACKGROUND_COLOR;
    
    /*
    if([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)])
       [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header_blank.png"] forBarMetrics:UIBarMetricsDefault];
    else
     */
    
    [self.navigationController.navigationBar addBackGroundImage:@"header_blank.png"];
    [self addRefreshButtonWithSelector:@selector(_refreshButtonAction:)];
    
    /*
   /// ========================================================================
    NSArray *arr=[self viewControllers];
    parentTabbarControlller=[[SSTabBarController alloc] initWithViewControllers:arr];
     */
    
    //==========================================================================

    if(self.parentTabbarControlller)
    {
        if([self respondsToSelector:@selector(addChildViewController:)])
            [self addChildViewController:parentTabbarControlller];
        
        parentTabbarControlller.view.frame=self.view.bounds;
        
        [parentTabbarControlller.view setAutoresizingMasks];
        [self.view addSubview:parentTabbarControlller.view];
        //parentTabbarControlller.selectedIndex=0;
        
        [parentTabbarControlller didMoveToParentViewController:self];
    }
    
    if(self.viewDidLoadCallback) self.viewDidLoadCallback();
}

-(void)_refreshButtonAction:(id)sender
{
    [self willRefresh];    
}

-(void)willRefresh
{
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
//[parentTabbarControlller shouldAutorotateToInterfaceOrientation:interfaceOrientation]
    
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

-(void)dealloc
{
    if([parentTabbarControlller respondsToSelector:@selector(removeFromParentViewController)])
    {
        [parentTabbarControlller removeFromParentViewController];
    }
    
    if(viewDidLoadCallback)
        [viewDidLoadCallback release];
    viewDidLoadCallback=nil;
    
    [parentTabbarControlller release];
    parentTabbarControlller=nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SSTabBarDidSelectViewControllerNotification object:nil];
    [customer release];
    [super dealloc];
    
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


@end
