//
//  NSString+Separation.m
//  Fieldforce
//
//  Created by Randem IT on 12/02/14.
//  Copyright (c) 2014 RandemIT. All rights reserved.
//

#import "NSString+Separation.h"

@implementation NSString (Separation)


-(NSMutableArray *)numbersSeparatedByString:(NSString *)separator
{
    if(!separator) return nil;
    
    NSArray *components = [self componentsSeparatedByString:separator];
    
    NSMutableArray *_numbers = [NSMutableArray array];
    
    for(NSString *numberString in components)
    {
        if(![numberString integerValue]) continue;
        
        NSNumber *_number = [NSNumber numberWithInteger:[numberString integerValue]];
        [_numbers addObject:_number];
    }
    
    return _numbers;
    
}

@end
