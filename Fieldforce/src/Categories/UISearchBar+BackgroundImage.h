//
//  UISearchBar+BackgroundImage.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 04/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISearchBar (BackgroundImage)

-(void)hideDefaultBackground;
-(UITextField *)textField;

-(void)addBackgroundImage:(UIImage *)backgroundImage;

@end
