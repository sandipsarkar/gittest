//
//  UIViewController+PopOverController.h
//  TestIndexedTableView
//
//  Created by RANDEM MAC on 08/06/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (PopOverController)

-(UIPopoverController *)presentInPopOverFrom:(id)view inView:(UIView *)aView animated:(BOOL)animated arrowDirection:(UIPopoverArrowDirection)direction;
-(UIPopoverController *)presentInPopOverFrom:(id)view inView:(UIView *)aView animated:(BOOL)animated;

@property (nonatomic,assign,readwrite) UIPopoverController *popoverController;

@end
