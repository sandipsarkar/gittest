//
//  NSDictionary+ValidateValue.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 21/02/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (ValidateValue)

- (id)objectOrNilForKey:(id)aKey;

@end

@interface NSMutableDictionary (ValidateValue)

- (void)setObjectOrNull:(id)anObject forKey:(id)aKey;

@end
