//
//  SOLabel.h
//  testPicker
//
//  Created by Subhojit Dey on 02/01/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum
{
    VerticalAlignmentTop = 0, // default
    VerticalAlignmentMiddle,
    VerticalAlignmentBottom,
} VerticalAlignment;

@interface SOLabel : UILabel
{
@private
    VerticalAlignment _verticalAlignment;

}
@property (nonatomic, readwrite, assign) VerticalAlignment verticalAlignment;
@end
