//
//  UISearchBar+BackgroundImage.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 04/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "UISearchBar+BackgroundImage.h"

@implementation UISearchBar (BackgroundImage)

-(void)hideDefaultBackground
{
    for (UIView *subview in self.subviews) 
    {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) 
        {
            [subview removeFromSuperview];
            break;
        }
    }

}

-(UITextField *)textField
{
   
        for (UIView* v in self.subviews)
        {
            if ( [v isKindOfClass: [UITextField class]] )
                return (UITextField *)v;
        }
        
        return nil;
    
}

-(void)addBackgroundImage:(UIImage *)backgroundImage
{
    if(!backgroundImage) return;
    
    if([self respondsToSelector:@selector(setBackgroundImage:)])
    {
        [self setBackgroundImage:backgroundImage];
    }
    else
    {
    
    [self hideDefaultBackground];
    [self.layer setContents:(id)backgroundImage.CGImage];
    }
}
@end
