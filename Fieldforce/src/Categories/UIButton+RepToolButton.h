//
//  UIButton+RepToolButton.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 19/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (RepToolButton)

+(UIButton *)repToolButtonWithImageName:(NSString *)imageName text:(NSString *)text;
+(UIButton *)repToolButtonWithText:(NSString *)text;
+(UIButton *)repToolCancelButton;
+(UIButton *)repToolSaveButton;
+(UIButton *)glossyButtonWithFrame:(CGRect)frame color:(UIColor *)color text:(NSString *)text;

@end
