//
//  AbstractTabBarController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 23/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSTabBarController.h"

typedef void(^ViewDidLoadCallback)(void);
@class Customer;
@interface AbstractTabBarController : UIViewController

@property(nonatomic,readonly) SSTabBarController *parentTabbarControlller;
@property(nonatomic,retain)Customer *customer;

-(id)initWithCustomer:(Customer *)aCustomer;

//@ Overridable by subclass
-(NSArray *)viewControllers;
-(void)didSelectViewController:(UIViewController *)controller;

-(void)willRefresh;

-(void)addViewDidLoadCallback:(ViewDidLoadCallback)callback;

@end
