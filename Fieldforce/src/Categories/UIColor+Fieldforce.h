//
//  UIColor+Fieldforce.h
//  Fieldforce
//
//  Created by Randem IT on 13/09/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Fieldforce)

+(UIColor *)cellSelectedColorBlue;

@end
