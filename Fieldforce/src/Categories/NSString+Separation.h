//
//  NSString+Separation.h
//  Fieldforce
//
//  Created by Randem IT on 12/02/14.
//  Copyright (c) 2014 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Separation)

-(NSMutableArray *)numbersSeparatedByString:(NSString *)separator;

@end
