//
//  UIViewController+PopOverController.m
//  TestIndexedTableView
//
//  Created by RANDEM MAC on 08/06/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import "UIViewController+PopOverController.h"
#import <objc/runtime.h>

@interface UIViewController()

//@property(nonatomic,assign,readwrite)UIPopoverController *popoverController; 

@end

@implementation UIViewController (PopOverController)

@dynamic popoverController;

static char popoverVCKey;

-(UIPopoverController *)presentInPopOverFrom:(id)view inView:(UIView *)aView animated:(BOOL)animated arrowDirection:(UIPopoverArrowDirection)direction
{
    UIViewController *_controller=nil;
    if([self isKindOfClass:[UINavigationController  class]])
    {
        UINavigationController *controller = (UINavigationController *)self;
        _controller=controller.topViewController;
    }
    else
    {
        _controller=self;
    }
    
    UIPopoverController *popOverC =_controller.popoverController;
    [popOverC retain];
    if(!popOverC)
    {
        popOverC=[[UIPopoverController alloc]initWithContentViewController:self];
        popOverC.popoverContentSize=self.contentSizeForViewInPopover;
        _controller.popoverController = popOverC;
    }
    
    if([view isKindOfClass:[UIBarButtonItem class]])
    {
        [popOverC presentPopoverFromBarButtonItem:view permittedArrowDirections:direction animated:animated];
    }
    else if([view isKindOfClass:[UIView class]])
    {

      [popOverC presentPopoverFromRect:[view frame] inView:aView permittedArrowDirections:direction animated:animated];
    }
    
    return [popOverC autorelease];
}

-(UIPopoverController *)presentInPopOverFrom:(id)view inView:(UIView *)aView animated:(BOOL)animated
{
   return  [self presentInPopOverFrom:view inView:aView animated:animated arrowDirection:UIPopoverArrowDirectionAny];
}


-(void)setPopoverController:(UIPopoverController *)aPopoverController
{
    if(aPopoverController!=self.popoverController)
    {
        objc_setAssociatedObject (self, &popoverVCKey, aPopoverController, OBJC_ASSOCIATION_ASSIGN);
    }
}

-(UIPopoverController *)popoverController
{
    UIPopoverController *vc = (UIPopoverController *)objc_getAssociatedObject(self, &popoverVCKey);
    
    return vc;
}


@end 

