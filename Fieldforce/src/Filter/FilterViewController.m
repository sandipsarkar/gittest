//
//  FilterViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 07/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "FilterViewController.h"
#import "UIViewController+PopOverController.h"
#import "Rep.h"
#import "Territory.h"

@interface FilterViewController()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableDictionary *_setupDict;
    
    NSMutableArray *selectedIndexPaths;
}

@property(nonatomic,copy)FilterCallback filterCallback;
@property(nonatomic,copy)CancelCallback cancelCallback;

@property(nonatomic,retain) NSIndexPath *selectedCatIndexPath;
@property(nonatomic,retain) NSIndexPath *selectedGetGenuineIndexPath;
@property(nonatomic,retain) NSIndexPath *selectedMembershipIndexPath;
@property(nonatomic,retain) NSIndexPath *selectedOrderIndexPath;
@property(nonatomic,retain) NSIndexPath *selectedStatusIndexPath;
@property(nonatomic,retain) NSIndexPath *selectedTerritoryIndexPath;
@property(nonatomic,retain) NSIndexPath *selectedSortIndexPath;

@property(nonatomic,retain) NSMutableArray *selectedSortIndexPaths;

-(void)setupDict;

@end

@implementation FilterViewController
@synthesize tableView=_tableView;
@synthesize filterCallback;
@synthesize cancelCallback;

@synthesize selectedCatIndexPath,selectedStatusIndexPath,selectedSortIndexPath,selectedGetGenuineIndexPath;
@synthesize selectedOrderIndexPath;
@synthesize selectedSortIndexPaths;
@synthesize delegate;

- (id)init
{
    self = [super init];
    
    if (self) 
    {
        self.title=@"FILTER CUSTOMERS";
        
        [self setupDict];
        
    }
    return self;
}

-(void)_initialize
{
    if(!selectedIndexPaths) 
    {
        selectedIndexPaths=[[NSMutableArray alloc]init];
    }
    else 
    {
        [selectedIndexPaths removeAllObjects];
    }
    
//    self.selectedCatIndexPath    = [NSIndexPath indexPathForRow:0 inSection:0];
//    self.selectedStatusIndexPath = [NSIndexPath indexPathForRow:0 inSection:2];
//    self.selectedSortIndexPath   = [NSIndexPath indexPathForRow:0 inSection:3];
//    self.selectedOrderIndexPath  = [NSIndexPath indexPathForRow:0 inSection:4];
    
    self.selectedCatIndexPath    = [NSIndexPath indexPathForRow:0 inSection:0];
    self.selectedMembershipIndexPath  = [NSIndexPath indexPathForRow:0 inSection:1];
    self.selectedStatusIndexPath = [NSIndexPath indexPathForRow:0 inSection:2];
    self.selectedTerritoryIndexPath = [NSIndexPath indexPathForRow:0 inSection:3];
    self.selectedSortIndexPath   = [NSIndexPath indexPathForRow:0 inSection:4];
    self.selectedOrderIndexPath  = [NSIndexPath indexPathForRow:0 inSection:5];
    
    
    [selectedIndexPaths addObject:self.selectedCatIndexPath];
    [selectedIndexPaths addObject:self.selectedMembershipIndexPath];
    [selectedIndexPaths addObject:self.selectedStatusIndexPath];
    [selectedIndexPaths addObject:self.selectedTerritoryIndexPath];
    [selectedIndexPaths addObject:self.selectedSortIndexPath];
    [selectedIndexPaths addObject:self.selectedOrderIndexPath];
    
    [self.tableView reloadData];
}

-(void)dealloc
{
    delegate = nil;
     [_tableView release];
    
    if(filterCallback) [filterCallback release];
    if(cancelCallback) [cancelCallback release];
    
    [_setupDict release];
    
    [selectedIndexPaths release];
    selectedIndexPaths      = nil;
    
    [selectedCatIndexPath release];
    selectedCatIndexPath    = nil;
    
    [selectedOrderIndexPath release];
    selectedOrderIndexPath  = nil;
    
    [selectedSortIndexPaths release];
    selectedSortIndexPaths  = nil;
    
    [selectedStatusIndexPath release];
    selectedStatusIndexPath = nil;
    
    [selectedSortIndexPath release];
    selectedSortIndexPath   = nil;
    
    [selectedGetGenuineIndexPath release];
    selectedGetGenuineIndexPath = nil;
    
    [_selectedMembershipIndexPath release];
    _selectedMembershipIndexPath = nil;
    
    [_selectedTerritoryIndexPath release];
    _selectedTerritoryIndexPath = nil;
    
    [super dealloc];
}

-(void)setupDict
{
    if(!_setupDict)
        _setupDict=[[NSMutableDictionary alloc]init];
    
   // OLD
    [_setupDict setObject:[NSArray arrayWithObjects:@"All",@"Mechanical",@"Collision", nil] forKey:@"0_SECTION_ITEMS"];
    
    // CHANGED TO
     //[_setupDict setObject:[NSArray arrayWithObjects:@"All",@"Mechanical",@"Collision",@"Mechanical & Collision",@"Other", nil] forKey:@"0_SECTION_ITEMS"];
   
    
    //[_setupDict setObject:[NSArray arrayWithObjects:@"GetGenuineMember", nil] forKey:@"1_SECTION_ITEMS"];

    NSMutableArray *memberShipArray = [[NSMutableArray alloc] init];
    for(int i=0 ; i<4;i++)
    {
       NSDictionary *dict = [[NSMutableDictionary alloc] init];
        
        NSString *membershipName = nil;
        NSString *selectedMemberShipKey = nil;
        switch (i)
        {
            case 0:
            {
                membershipName = @"All";
                selectedMemberShipKey = nil;
            }
            break;
            case 1:
            {
                membershipName = @"GetGenuineMember";
                selectedMemberShipKey = @"getGenuineMember";
            }
            break;
            case 2:
            {
                membershipName = @"TradeClub";
                selectedMemberShipKey = @"tradeClub";
            }
            break;
            case 3:
            {
                membershipName = @"Capricorn";
                selectedMemberShipKey = @"capricorn";
            }
            break;
                
            default:
                break;
        }
        
      [dict setValue:membershipName forKey:@"NAME"];
      if(selectedMemberShipKey)
      [dict setValue:selectedMemberShipKey forKey:@"MEMBERSHIPKEY"];
      [memberShipArray addObject:dict];
      [dict release];
    }

    
    [_setupDict setObject:memberShipArray forKey:@"1_SECTION_ITEMS"];
    

    [_setupDict setObject:[NSArray arrayWithObjects:@"All",@"Confirmed Only",@"Unconfirmed Only", nil] forKey:@"2_SECTION_ITEMS"];
    
      Rep *rep = [CoreDataHandler currentRep];
    
    NSMutableArray *territoryNames = [[NSMutableArray alloc] init];
    
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:@"All",@"NAME", nil];
  //  [territoryNames addObject:dict];
    
      for(Territory *territory in rep.territories)
      {
           NSDictionary *dict = [[NSMutableDictionary alloc] init];
          [dict setValue:territory.territoryAbbreviation forKey:@"NAME"];
          [dict setValue:territory.territoryID forKey:@"territoryID"];
          [territoryNames addObject:dict];
          [dict release];
      }
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"NAME" ascending:YES];
    [territoryNames sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    [territoryNames insertObject:dict atIndex:0];
    
    [_setupDict  setObject:territoryNames  forKey:@"3_SECTION_ITEMS"];
    
    
    //[_setupDict setObject:[NSArray arrayWithObjects:@"Customer Name",@"Suburb",@"Territory",@"Last Visit",@"Next Visit", nil] forKey:@"4_SECTION_ITEMS"];
    
    [_setupDict setObject:[NSArray arrayWithObjects:@"Customer Name",@"Suburb",@"Post Code",@"Territory",@"Last Visit",@"Next Visit", nil] forKey:@"4_SECTION_ITEMS"];
   
    
     [_setupDict setObject:[NSArray arrayWithObjects:@"Ascending",@"Descending",nil] forKey:@"5_SECTION_ITEMS"];
    
    [_setupDict setObject:@"CATEGORY" forKey:@"0_SECTION_TITLE"];
    [_setupDict setObject:@"MEMBERSHIP" forKey:@"1_SECTION_TITLE"];
    [_setupDict setObject:@"CUSTOMER STATUS"  forKey:@"2_SECTION_TITLE"];
    [_setupDict setObject:@"TERRITORY"  forKey:@"3_SECTION_TITLE"];
    [_setupDict setObject:@"SORT BY"  forKey:@"4_SECTION_TITLE"];
    [_setupDict setObject:@"ORDER"    forKey:@"5_SECTION_TITLE"];
    
    [memberShipArray release];
    [territoryNames release];
    [descriptor release];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    //self.view.backgroundColor=[UIColor whiteColor];
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addTitle:self.title withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    self.navigationController.navigationBar.barStyle=UIBarStyleBlack;
    
    CGRect tableFrame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
    
    /*
     if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
     {
         tableFrame =CGRectMake(10.0, 0, CGRectGetWidth(self.view.bounds)-20.0, CGRectGetHeight(self.view.bounds));
     }
    else
    {
        tableFrame =CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
    }
    */
    
    UITableView *aTableView=[[UITableView alloc] initWithFrame:tableFrame style:UITableViewStyleGrouped];
    //aTableView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
    aTableView.bounces=NO;
   // aTableView.backgroundColor=[UIColor groupTableViewBackgroundColor];;
    aTableView.delegate=self;
    aTableView.dataSource=self;
    aTableView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    
    [self.view addSubview:aTableView];
    self.tableView=aTableView;
    [aTableView release];
    
     if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
     {
       self.tableView.backgroundColor=[UIColor groupTableViewBackgroundColor];;
       self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
      
     }


    UIView *tableHeaderView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 59)];
    
    UIButton *cancelButton = [UIButton repToolButtonWithText:@"RESET"];
    CGRect cancelFrame = cancelButton.frame;
    cancelFrame.origin.y = 2.0;
    cancelFrame.origin.x = 8.0f;
    cancelButton.frame = cancelFrame;
    cancelButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    cancelButton.tag=1;
    cancelButton.titleLabel.font = [UIFont subaruBoldFontOfSize:20.0];
     [tableHeaderView addSubview:cancelButton];
    cancelButton.autoresizingMask =UIViewAutoresizingFlexibleRightMargin;
    [cancelButton addTarget:self action: @selector(cancelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *filterButton = [UIButton repToolButtonWithText:@"CONFIRM"];
    CGRect filterFrame = filterButton.frame;
    filterFrame.origin.y = 2.0;
    filterFrame.origin.x = CGRectGetMaxX(cancelButton.frame)+5.0;
    filterButton.frame = filterFrame;
    filterButton.titleLabel.font = [UIFont subaruBoldFontOfSize:20.0];
    filterButton.autoresizingMask=UIViewAutoresizingFlexibleTopMargin;
    [tableHeaderView addSubview:filterButton];
    [filterButton addTarget:self action: @selector(filterButtonAction:) forControlEvents:UIControlEventTouchUpInside];

    self.tableView.tableFooterView=tableHeaderView;
    [tableHeaderView release];
    
    [self _initialize];
    
}

-(void)addFilterCallback:(FilterCallback)callBack
{
    //[self.popoverController dismissPopoverAnimated:YES];
    if(callBack) self.filterCallback=callBack;
}

-(void)addCancelCallback:(CancelCallback )callback
{
    if(callback) self.cancelCallback=callback;
}
-(void)cancelButtonAction:(id)sender
{
    //if(self.popoverController)[self.popoverController dismissPopoverAnimated:YES];
    
    [self _initialize];
    
    if(self.cancelCallback)
        self.cancelCallback();
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(filterViewControllerDidCancel:)])
    {
        [self.delegate filterViewControllerDidCancel:self];
    }
}

-(void)filterButtonAction:(id)sender
{
    NSMutableArray *customerCategoryArr  = [NSMutableArray array];
    NSMutableArray *customerSortbyKeyArr = [NSMutableArray array];
    NSNumber *ascending = nil;
    NSNumber *getGenuineOnly = nil;
    NSNumber *status = nil;

    if(self.selectedSortIndexPath)
    {
        switch (selectedSortIndexPath.row) //postCode
        {
            case 0:
                [customerSortbyKeyArr addObject:@"name"];
            break;
                
            case 1:
                [customerSortbyKeyArr addObject:@"suburb"];
            break;
            
            case 2:
                [customerSortbyKeyArr addObject:@"postCode"];
            break;
                
            case 3:
                [customerSortbyKeyArr addObject:@"territoryAbbreviation"];
            break;
                
            case 4:
                [customerSortbyKeyArr addObject:@"lastVisitDate"]; //@"lastVisit"
            break;
                
            case 5:
                [customerSortbyKeyArr addObject:@"nextVisitDate"];  //@"nextVisit"
            break;
                
            /*
            case 2:
                [customerSortbyKeyArr addObject:@"territoryAbbreviation"];
            break;
            
            case 3:
                [customerSortbyKeyArr addObject:@"lastVisit"];
            break;
            
            case 4:
                [customerSortbyKeyArr addObject:@"nextVisit"];
            break;
             
            */
                
            default:
            break;
        }
    }

    
    if(self.selectedCatIndexPath)
    {
      
        switch (self.selectedCatIndexPath.row) 
        {
            case 0:
            {
               
            }
            break;
                
            case 1: //Mechanical
            {
                  [customerCategoryArr addObject:[NSNumber numberWithInteger:CategoryTypeMechanical]];
                
                 //  [customerCategoryArr addObject:[NSNumber numberWithInteger:CategoryTypeAll]];
            }
            break;
                
            case 2: //Collision
            {
                  [customerCategoryArr addObject:[NSNumber numberWithInteger:CategoryTypeCollision]];
                
                 // [customerCategoryArr addObject:[NSNumber numberWithInteger:CategoryTypeAll]];
            }
            break;
                
            case 3: // Mechanical & Collision
            {
                  [customerCategoryArr addObject:[NSNumber numberWithInteger:CategoryTypeAll]];
            }
                
            break;
                
            case 4: // Other
            {
                [customerCategoryArr addObject:[NSNumber numberWithInteger:CategoryTypeOther]];
                
            }
            break;
                
            default:break;
        }
    }
    
    if(self.selectedStatusIndexPath)
    {
        int _status = (int) selectedStatusIndexPath.row;
        status = [NSNumber numberWithInt:_status];
    }

    
    if(self.selectedOrderIndexPath)
    {
      ascending = [NSNumber numberWithBool:(self.selectedOrderIndexPath.row==0)];
    }
    
    
    NSNumber *territoryID = nil;
    if(self.selectedTerritoryIndexPath)
    {
        NSArray *_territories  = [_setupDict objectForKey:@"3_SECTION_ITEMS"];
        
       NSDictionary *territoryDict =  [_territories objectAtIndex:self.selectedTerritoryIndexPath.row];
      
        territoryID  = [territoryDict valueForKey:@"territoryID"];
    }
    
//    getGenuineOnly = [NSNumber numberWithBool:self.selectedGetGenuineIndexPath!=nil];
//    NSDictionary *filterDict=[[NSDictionary alloc] initWithObjectsAndKeys:customerCategoryArr,@"CATEGORY",getGenuineOnly,@"GETGENUINE",status,@"STATUS", territoryID,@"TERRITORY", customerSortbyKeyArr,@"SORT",ascending,@"ASCENDING",nil];
    
    
    NSString *selectedMembershipKey = nil;
    if(self.selectedMembershipIndexPath)
    {
        NSArray *_memberships  = [_setupDict objectForKey:@"1_SECTION_ITEMS"];
        
        NSDictionary *_membershipDict =  [_memberships objectAtIndex:self.selectedMembershipIndexPath.row];
        
        selectedMembershipKey  = [_membershipDict valueForKey:@"MEMBERSHIPKEY"];
    }

    NSMutableDictionary *filterDict = [[NSMutableDictionary alloc] init];
    
    [filterDict setValue:customerCategoryArr forKey:@"CATEGORY"];
    [filterDict setValue:selectedMembershipKey forKey:@"MEMBERSHIP"];
    [filterDict setValue:status forKey:@"STATUS"];
    [filterDict setValue:territoryID forKey:@"TERRITORY"];
    [filterDict setValue:customerSortbyKeyArr forKey:@"SORT"];
    [filterDict setValue:ascending forKey:@"ASCENDING"];
    
    
    if(self.filterCallback) self.filterCallback(filterDict);
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(filterViewController: willFilterWithOptioins:)])
    {
        [self.delegate filterViewController:self willFilterWithOptioins:filterDict];
    }
    
    [filterDict release];
    
    [self.popoverController dismissPopoverAnimated:YES];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *items=[_setupDict objectForKey:[NSString stringWithFormat:@"%d_SECTION_ITEMS",section]];
    
    return [items count];
}

/*
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [_setupDict objectForKey:[NSString stringWithFormat:@"%d_SECTION_TITLE",section]];
}
 */
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSString *title =[_setupDict objectForKey:[NSString stringWithFormat:@"%d_SECTION_TITLE",section]];
    
    return (title.length)?40.0:0;

}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
   static NSString *headerIdentifier = @"cell_header";
    
    NSString *title =[_setupDict objectForKey:[NSString stringWithFormat:@"%d_SECTION_TITLE",section]];
    
    if(!title.length) return nil;
    
   UILabel *headerLabel = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headerIdentifier];
    
    if(!headerLabel)
    {
     headerLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 40.0)] autorelease];
     headerLabel.backgroundColor = [UIColor clearColor];
     headerLabel.textColor = [UIColor blackColor];
     headerLabel.font = [UIFont subaruBoldFontOfSize:23.0];
     headerLabel.shadowColor  = [UIColor lightGrayColor];
     headerLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    }
  
    
    headerLabel.text =[NSString stringWithFormat:@"    %@",title];
    
    return headerLabel;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cell.textLabel.font = [UIFont grotesqueFontOfSize:16.0];
    }
    
    NSArray *items=[_setupDict objectForKey:[NSString stringWithFormat:@"%d_SECTION_ITEMS",indexPath.section]];
    
    id obj= (indexPath.row<[items count]) ?  [items objectAtIndex:indexPath.row] : @"";
    
    NSString *title = ([obj isKindOfClass:[NSDictionary class]]) ? [obj valueForKey:@"NAME"] : (NSString *)obj;
    
    cell.textLabel.text = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [NSString stringWithFormat:@"  %@",title] : title;
    
    BOOL selected = [selectedIndexPaths containsObject:indexPath];
    
    cell.accessoryType=selected?UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone;
    //cell.textLabel.textColor=selected?LOGIN_BG_COLOR:[UIColor blackColor];
    cell.textLabel.textColor=[UIColor blackColor];
    
    return cell;
}


////FOR >= IOS 7 ONLY

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
        
    cell.backgroundColor= [UIColor groupTableViewBackgroundColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];

    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.tag = 1;
     
    
      NSInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
      if(numRows==1)
       {
         _bgView.position = CellPositionSingle;
       }
       else
       {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
       }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
}
///


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *indexPathsToReload = [NSMutableArray array];
    
    switch (indexPath.section) 
    {
        case 0:
        {
            if([self.selectedCatIndexPath isEqual:indexPath]) return;
            
            if(self.selectedCatIndexPath) 
            {
                [indexPathsToReload addObject:self.selectedCatIndexPath];
                [selectedIndexPaths removeObject:self.selectedCatIndexPath];
            }
            
             self.selectedCatIndexPath = indexPath;
             [indexPathsToReload addObject:indexPath];
             [selectedIndexPaths addObject:indexPath];
        }
        break;
            
        case 1:
        {
            /*
            if(self.selectedGetGenuineIndexPath) 
            {
                [indexPathsToReload addObject:self.selectedGetGenuineIndexPath];
                [selectedIndexPaths removeObject:self.selectedGetGenuineIndexPath];
                self.selectedGetGenuineIndexPath = nil;
            }
            else 
            {
                [indexPathsToReload addObject:indexPath];
                [selectedIndexPaths addObject:indexPath];
                self.selectedGetGenuineIndexPath = indexPath;
            }
            */
            
            if([self.selectedMembershipIndexPath isEqual:indexPath]) return;
            
            if(self.selectedMembershipIndexPath)
            {
                [indexPathsToReload addObject:self.selectedMembershipIndexPath];
                [selectedIndexPaths removeObject:self.selectedMembershipIndexPath];
            }
            
            self.selectedMembershipIndexPath = indexPath;
            [indexPathsToReload addObject:indexPath];
            [selectedIndexPaths addObject:indexPath];
           
        }
        break;
            
        case 2:
        {
            if([self.selectedStatusIndexPath isEqual:indexPath]) return;
            
            if(self.selectedStatusIndexPath) 
            {
                [indexPathsToReload addObject:self.selectedStatusIndexPath];
                [selectedIndexPaths removeObject:self.selectedStatusIndexPath];
            }
            
            self.selectedStatusIndexPath = indexPath;
            [indexPathsToReload addObject:indexPath];
            [selectedIndexPaths addObject:indexPath];
        }
            break;
            
        case 3:
        {
            if([self.selectedTerritoryIndexPath isEqual:indexPath]) return;
            
            if(self.selectedTerritoryIndexPath)
            {
                [indexPathsToReload addObject:self.selectedTerritoryIndexPath];
                [selectedIndexPaths removeObject:self.selectedTerritoryIndexPath];
            }
            
            self.selectedTerritoryIndexPath = indexPath;
            [indexPathsToReload addObject:indexPath];
            [selectedIndexPaths addObject:indexPath];
        }
            break;

        case 4:
        {
            if([self.selectedSortIndexPath isEqual:indexPath]) return;
            
            if(self.selectedSortIndexPath) 
            {
                [indexPathsToReload addObject:self.selectedSortIndexPath];
                [selectedIndexPaths removeObject:self.selectedSortIndexPath];
            }
            
            self.selectedSortIndexPath = indexPath;
            
            [indexPathsToReload addObject:indexPath];
            [selectedIndexPaths addObject:indexPath];
            
        }
            
        break;
            
        case 5:
        {
            if([self.selectedOrderIndexPath isEqual:indexPath]) return;
            
            if(self.selectedOrderIndexPath) 
            {
                [indexPathsToReload addObject:self.selectedOrderIndexPath];
                [selectedIndexPaths removeObject:self.selectedOrderIndexPath];
            }
            
            self.selectedOrderIndexPath = indexPath;
            
            [indexPathsToReload addObject:indexPath];
            [selectedIndexPaths addObject:indexPath];
        }
            
        break;
            
        default: break;
    }
    
    if(indexPathsToReload)
    [tableView reloadRowsAtIndexPaths:indexPathsToReload withRowAnimation:UITableViewRowAnimationFade];
}

@end
