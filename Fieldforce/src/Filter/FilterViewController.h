//
//  FilterViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 07/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^FilterCallback)(NSDictionary *dict);
typedef void(^CancelCallback)();

@protocol  CustomerFilterDelegate;

@interface FilterViewController : UIViewController
{
    @private 
    UITableView *_tableView;
}

@property(nonatomic,retain)UITableView *tableView;
@property(nonatomic,assign)id<CustomerFilterDelegate>delegate;

-(void)addFilterCallback:(FilterCallback)callBack;
-(void)addCancelCallback:(CancelCallback )callback;

@end

@protocol CustomerFilterDelegate <NSObject>

@optional

-(void)filterViewController:(FilterViewController *)filterVC willFilterWithOptioins:(NSDictionary *)filterOptions;
-(void)filterViewControllerDidCancel:(FilterViewController *)filterVC;

@end