//
//  CustomerListViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Customer;

typedef void(^CustomerSelectCallback) (Customer *customer);
typedef void(^DidSelectCustomersCallback) (NSMutableArray *selectedCustomers);

@interface CustomerListViewController : UIViewController
{
    UISearchBar *searchBar;
}
@property (nonatomic,retain) UITableView *tableView;
@property (nonatomic,retain) NSMutableArray *customers;
@property (nonatomic,copy)   NSString *selectedCustomerName;

@property(nonatomic,assign) BOOL allowMultipleSelection;

-(void)addCustomerSelectCallback:(CustomerSelectCallback)callback;
-(void)addDidSelectCustomersCallback:(DidSelectCustomersCallback)callback;

-(id)initWithCustomerName:(NSString *)name;

@end
