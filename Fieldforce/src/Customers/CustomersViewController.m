//
//  CustomersViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 21/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomersViewController.h"
#import "UISearchBar+BackgroundImage.h"
#import "Customer.h"
#import "WholesaleDealer.h"
#import "Rep.h"
#import "Task.h"
#import "CustomerProfileViewController.h"
#import "CustomerTableViewCell.h"
#import "FilterViewController.h"
//#import "CreateNewCustomerViewController.h"
#import "MenuViewController.h"
#import "OfflineManager.h"
#import "CustomerSummaryViewController.h"
#import "CustomerContact.h"
#import "ScheduledVisit.h"
#import "Territory.h"
#import "CMIndexBar.h"

//#import "UISearchBar+BackgroundImage.h"
//#import "SSCoreDataManager.h"
//#import "TaskListViewController.h"
//#import "TasksViewController.h"

#define TABLE_ROW_HEIGHT 44.0 //60.0
#define CREATE_NEW_CUSATOMER_CONTROLLER @"CreateNewCustomer"

@interface CustomersViewController()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate, CMIndexBarDelegate,CustomerFilterDelegate,ProfileInfoEditDelegate,OfflineManagerDelegate>
{
    NSCharacterSet  *charSet;
    NSArray *indexedArray;
    //BOOL isSearching;
    
    UISearchBar *searchBar;
    CMIndexBar *indexBar;
    
    


}

@property (nonatomic, retain) UIPopoverController *currentPopoverController;
@property (nonatomic, retain) UIPopoverController *customerPopoverController;
@property (nonatomic, retain) NSMutableArray *searchArray;
@property (nonatomic, assign) CustomerProfileViewController *selectedCustomerProfile;
@property (nonatomic, copy)   CustomersDidLoad customersDidLoadCallback;

@property (nonatomic,retain) NSPredicate *filterPredicte;
@property (nonatomic,retain) NSPredicate *searchPredicte;
@property (nonatomic,retain) NSArray *filterDescriptos;

@property (nonatomic,retain) ASIHTTPRequest *currentCustomerRequest;

@property (nonatomic,retain) NSManagedObjectContext *customerEditingContext;


-(void)filterCustomersByName;

@end

@implementation CustomersViewController
@synthesize tableView=_tableView;
@synthesize customers;
@synthesize currentPopoverController;
@synthesize customerPopoverController;
@synthesize searchArray;
@synthesize isSearching;
@synthesize selectedCustomerProfile;
@synthesize customersDidLoadCallback;

@synthesize filterPredicte;
@synthesize searchPredicte;
@synthesize filterDescriptos;

@synthesize currentCustomerRequest;
@synthesize customerEditingContext;

- (id)init
{
    self = [super init];
    if (self) 
    {
       isSearching=NO;
       charSet =[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZ"] retain];
       indexedArray=[[@"A B C D E F G H I J K L M N O P Q R S T U V W X Y Z #" componentsSeparatedByString:@" "] retain];
        _sectionArr = [[NSMutableArray alloc] init];
        
        self.tableView.rowHeight=50;
        
        //NSMutableArray *_customers=[CoreDataHandler allCustomers];
       // self.customers=_customers?_customers:[NSMutableArray array];
       // [self filterCustomersByName];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(customersDidFinishLoad:) name:CustomersDidFinishLoad object:nil];
    }
    return self;
}

-(void)filterCustomersByName
{
    
    [self.customers sortUsingComparator:^(id obj1, id obj2)
    {
        Customer* u1 = (Customer*)obj1;
        Customer* u2 = (Customer*)obj2;
        
        NSString *name1 = [u1 valueForKey:@"name"];
        NSString *name2 = [u2 valueForKey:@"name"];
        
        return [name1 caseInsensitiveCompare:name2];
        
    }];
}

-(void)setupSectionDict
{
    [_sectionArr removeAllObjects];
    
    NSMutableArray *allCustomers =[[NSMutableArray alloc] initWithArray:self.customers];
    
    int index=0;
    for(NSString *key in indexedArray) 
    {
        if([key isEqualToString:@"#"]) continue;
        
        NSPredicate *indexPredicate = [NSPredicate predicateWithFormat:@"self.name beginswith[c] %@",key];
        NSArray *indexedByKeyArr = [allCustomers filteredArrayUsingPredicate:indexPredicate];
        if([indexedByKeyArr count])
        {
          NSDictionary *dict  = [[NSMutableDictionary alloc] initWithObjectsAndKeys:indexedByKeyArr,@"row",key,@"index", nil];
           
          [_sectionArr addObject:dict];
          [allCustomers removeObjectsInArray:indexedByKeyArr];
          [dict release];
        }
        
        index++;
    }
    
   
    if([allCustomers count])
    {
         NSDictionary *dict  = [[NSMutableDictionary alloc] initWithObjectsAndKeys:allCustomers,@"row",@"#",@"index", nil];
        [_sectionArr addObject:dict];
        [dict release];
    }
    
    [allCustomers release];
}

-(void)dealloc
{
    // [ProgressHUD hideHUDForView:self.view animated:NO];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self setAssociateObject:nil forKey:CREATE_NEW_CUSATOMER_CONTROLLER];
    
    currentCustomerRequest.delegate = nil;
    [currentCustomerRequest clearDelegatesAndCancel];
    [currentCustomerRequest release];
    currentCustomerRequest = nil;
    
    [OfflineManager sharedManager].delegate = nil;
    
    [_tableView release];
    [charSet release];
    [indexedArray release];
    [_sectionArr release];
    [customers release];
    customers = nil;
    [currentPopoverController release];
    [customerPopoverController release];
    [searchArray release];
    searchArray = nil;
    [searchBar release];
    
    [filterPredicte release];
    [searchPredicte release];
    
    [filterDescriptos release];
    filterDescriptos = nil;
    
    selectedCustomerProfile=nil;
    
    if(customersDidLoadCallback)
     [customersDidLoadCallback release];
    
    [customerEditingContext release];
    customerEditingContext = nil;

    [super dealloc];
}

-(void)addCustomersDidLoadCallback:(CustomersDidLoad)callBack
{
    if(callBack)
    self.customersDidLoadCallback = callBack;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
#if LOCAL
    
    [UIAlertView showWarningAlertWithTitle:@"Memory Warning" message:@"At CustomesViewController"];
#endif
}

#pragma mark - View lifecycle

-(void)setupTableView
{
    CGFloat locY=88.0;
    self.view.backgroundColor=[UIColor lightGrayColor];
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, locY, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-locY) style:UITableViewStylePlain];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.bounces=NO;
    _tableView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:_tableView];
    _tableView.pagingEnabled=NO;
    
    //************** TO ENABLE SWIPE TO DELETE *****************
    // _tableView.allowsSelectionDuringEditing =YES;
    //**********************************************************
    
    indexBar = [[CMIndexBar alloc] initWithFrame:CGRectMake(671, 105, 28, 600)];
    [indexBar setIndexes:indexedArray];
    indexBar.delegate = self;
    [self.view addSubview:indexBar];
    [indexBar release];
}

-(void)setupHeaderBar
{
     filterButton=[UIButton buttonWithTitle:@"Filter" type:UIButtonTypeCustom frame:CGRectMake(0, 0, 117.0, 44.0) bgImage:@"segmented_button.png" titleColor:[UIColor whiteColor] target:self action:@selector(filterAction:)];
    filterButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"segmented_button_selected.png"] forState:UIControlStateHighlighted];
     [filterButton setBackgroundImage:[UIImage imageNamed:FILTER_BUTTON_IMAGE] forState:UIControlStateSelected];
     [self.view addSubview:filterButton];
    
    
    searchBar=[[UISearchBar alloc] initWithFrame:CGRectMake(filterButton.right, 0, self.view.width-(117+126), 44)];
    searchBar.autoresizingMask=UIViewAutoresizingFlexibleWidth;
    searchBar.placeholder = @"Search...";
    searchBar.delegate=self;
    [self.view addSubview:searchBar];
    [searchBar addBackgroundImage:[UIImage imageNamed:@"search_bar_big.png"] ];
    
    
    UIButton *addNewButton=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(self.view.width-126, 0, 126, 44) bgImage:@"create_new_button.png" titleColor:[UIColor whiteColor] target:self action:@selector(addNewCutomerAction:)];
    addNewButton.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin;
    [addNewButton setBackgroundImage:[UIImage imageNamed:@"create_new_button_selected.png"] forState:UIControlStateHighlighted];
    [self.view addSubview:addNewButton];
    
    UIImage *headerImage=[UIImage imageNamed:@"customer_table_header.png"];
    UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(addNewButton.frame), headerImage.size.width, headerImage.size.height)];
    imageView.image = headerImage;
    imageView.autoresizingMask=UIViewAutoresizingFlexibleWidth;
    
    CGRect _headerLabelFrame = CGRectMake(CGRectGetMinX(imageView.frame)+3, 0, 45.0, CGRectGetHeight(imageView.frame));
    UILabel* catLabel = [UILabel labelWithText:@" CAT" frame:_headerLabelFrame textColor:[UIColor whiteColor] bgColor:[UIColor clearColor] fontsize:0.0];
    catLabel.font = [UIFont grotesqueBoldFontOfSize:14.0];
    catLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleWidth;
    [imageView addSubview:catLabel];
    
    _headerLabelFrame = CGRectMake(CGRectGetMaxX(catLabel.frame)+60, CGRectGetMinY(catLabel.frame), 150.0, CGRectGetHeight(imageView.frame));
    UILabel* customerlabel = [UILabel labelWithText:@"Customer Name" frame:_headerLabelFrame textColor:[UIColor whiteColor] bgColor:[UIColor clearColor] fontsize:0.0];
    customerlabel.font = [UIFont grotesqueBoldFontOfSize:14.0];
    customerlabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleWidth;
    [imageView addSubview:customerlabel];
    
    _headerLabelFrame = CGRectMake(CGRectGetMaxX(customerlabel.frame)+75, CGRectGetMinY(catLabel.frame), 150.0, CGRectGetHeight(imageView.frame));
    UILabel* suburbLabel = [UILabel labelWithText:@"Suburb" frame:_headerLabelFrame textColor:[UIColor whiteColor] bgColor:[UIColor clearColor] fontsize:0.0];
    suburbLabel.font = [UIFont grotesqueBoldFontOfSize:14.0];
    suburbLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleWidth;
    suburbLabel.textAlignment = NSTextAlignmentCenter;
    [imageView addSubview:suburbLabel];
    
    _headerLabelFrame = CGRectMake(CGRectGetMaxX(suburbLabel.frame)-16, CGRectGetMinY(catLabel.frame), 80.0, CGRectGetHeight(imageView.frame));
    UILabel* territorylabel = [UILabel labelWithText:@"Territory" frame:_headerLabelFrame textColor:[UIColor whiteColor] bgColor:[UIColor clearColor] fontsize:0.0];
    territorylabel.font = [UIFont grotesqueBoldFontOfSize:14.0];
    territorylabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleWidth;
    territorylabel.textAlignment = NSTextAlignmentCenter;
    [imageView addSubview:territorylabel];
    
    _headerLabelFrame = CGRectMake(CGRectGetMaxX(territorylabel.frame)-12, CGRectGetMinY(catLabel.frame), 82.0, CGRectGetHeight(imageView.frame));
    UILabel* lastVisitLabel = [UILabel labelWithText:@"Last Visit" frame:_headerLabelFrame textColor:[UIColor whiteColor] bgColor:[UIColor clearColor] fontsize:0.0];
    lastVisitLabel.font = [UIFont grotesqueBoldFontOfSize:14.0];
    lastVisitLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleWidth;
    lastVisitLabel.lineBreakMode = UILineBreakModeWordWrap;
    lastVisitLabel.numberOfLines = 2;
    lastVisitLabel.textAlignment = NSTextAlignmentCenter;
    [imageView addSubview:lastVisitLabel];
    
    _headerLabelFrame = CGRectMake(CGRectGetMaxX(lastVisitLabel.frame)-5, CGRectGetMinY(catLabel.frame), 82.0, CGRectGetHeight(imageView.frame));
    UILabel* nextVisitLabel = [UILabel labelWithText:@"Next Visit" frame:_headerLabelFrame textColor:[UIColor whiteColor] bgColor:[UIColor clearColor] fontsize:0.0];
    nextVisitLabel.font = [UIFont grotesqueBoldFontOfSize:14.0];
    nextVisitLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleWidth;
    nextVisitLabel.lineBreakMode = UILineBreakModeWordWrap;
    nextVisitLabel.numberOfLines = 2;
    nextVisitLabel.textAlignment = NSTextAlignmentCenter;
    [imageView addSubview:nextVisitLabel];
    
    [self.view addSubview:imageView];
    [imageView release];
    
    /*
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(imageView.frame)-2, CGRectGetWidth(self.view.frame), 20)];
    sectionView.backgroundColor  = LOGIN_BG_COLOR;
    sectionView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    [self.view insertSubview:sectionView belowSubview:imageView];
     
    _sectionIndexLabel=[UILabel labelWithText:@"A" frame:CGRectMake(100, 2, 100, 15) textColor:[UIColor whiteColor] bgColor:[UIColor clearColor] fontsize:14];
    _sectionIndexLabel.font=[UIFont boldSystemFontOfSize:14];
    [sectionView addSubview:_sectionIndexLabel];
    
    [sectionView release];
    */
}

-(void)setupTableHeaderView
{
    
}

/*
-(void)fetchCustomersFromServer
{

    Rep *currentRep=[CoreDataHandler currentRep];
    
    NSNumber *wdSiteID= currentRep.parentWdSite.wdSiteID;
    //[NSNumber numberWithInteger:18];//
    
    ASIHTTPRequest *request = [ConnectionManager createCustomersConnectionWithRepID:currentRep.repID wdSiteID:wdSiteID];
    
    [request setCompletionBlock:^{
        
        NSLog(@"Response:%@",[request responseString]);
        
        NSDictionary *jsonDict = [request responseJSON];
        
        BOOL _isSiteIDChanged = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
        
        if(_isSiteIDChanged)
        {
            [app appDidChangeSiteID];
        }
        else 
        {
            
        app.isCustomerLoaded=YES;
        NSArray *_customers = [jsonDict valueForKey:JSON_DETAILS_KEY];
        NSArray *updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
        NSArray *deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
        
       
        NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
        [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_CUSTOMERS];
        
        [Customer populateAsyncPerformingDelete:deletedIDs update:updatedIDs insertOnArray:_customers onAttribute:@"customerID" completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
        {
            //[deletedIDs release];
             //[updatedIDs release];
            
            if([deletedResult count])
                [self.customers removeObjectsInArray:deletedResult];
            
                        
            if([insertedResult count])
                [self.customers addObjectsFromArray:insertedResult];
            
            [self filterCustomersByName];
            [self setupSectionDict];
            [self.tableView reloadData];
            // [ProgressHUD hideAnimated:YES];

             if(self.customersDidLoadCallback) self.customersDidLoadCallback();
        }];
        }
    }];
    
    [request setFailedBlock:^{
        
        //[ProgressHUD hideAnimated:YES];
    }];
    


}
*/

#pragma mark - FETCH CUSTOMERS
-(void)fetchCustomersFromServer
{
    if (_isFetchingFromServer)
    {
        return;
    }
    
    if(app.isCustomerFetching) return;
    
    _isFetchingFromServer = YES;
    
    __block CustomersViewController *weakSelf = self;
    
    self.currentCustomerRequest =  [ConnectionManager fetchAndProcessCustomersOnCompletion:^(BOOL success, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
                                    
    {
         CustomersViewController *strongSelf = weakSelf;
        
        if(success)
        {
            if([insertedResult count])
            [strongSelf.customers addObjectsFromArray:insertedResult];

            [strongSelf filterCustomersByName];
            [strongSelf setupSectionDict];

            strongSelf->_isFetchingFromServer = NO;
            [strongSelf.tableView reloadData];
            
            //if(self.customersDidLoadCallback) self.customersDidLoadCallback();
        }
        else
        {
            strongSelf->_isFetchingFromServer = NO;
            
            if(!strongSelf->_isFetchingLocally)
            {
              [strongSelf.tableView reloadData];
            }
        }
        
        [strongSelf performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
        
    } deletionBlock:^(NSMutableArray *deletedResult)
    {
        
        CustomersViewController *strongSelf = weakSelf;
        
        if([deletedResult count])
            [strongSelf.customers removeObjectsInArray:deletedResult];
        
        if([strongSelf respondsToSelector:@selector(searchArray)] && [strongSelf.searchArray count])
            [strongSelf.searchArray removeObjectsInArray:deletedResult];
        
    }];
}

-(void)_fetchCustomersFromServer
{
    if (_isFetchingFromServer)
    {
        return;
    }
    
    
    if(app.isCustomerFetching) return;
    
    _isFetchingFromServer = YES;
    

     Rep *currentRep=[CoreDataHandler currentRep];
     
     NSNumber *wdSiteID= currentRep.parentWdSite.wdSiteID;
     
     NSArray *activeTerritories = [Territory fetchAll:nil];
     NSArray *territories       = [activeTerritories valueForKey:@"territoryID"];
     NSString *territoryIDS     = [territories componentsJoinedByString:@","];
    
     NSLog(@"Territories=%@",territoryIDS);
    
    //===========================================  SEND AVAILABLE CUSTOMER IDS =============================
    
    NSMutableArray *allCustomers = [Customer fetchWithPredicate:nil onAttributes:[NSArray arrayWithObject:@"customerID"] sortDescriptors:nil limit:0 error:nil];
    NSArray *  _allCustomersIDs  = [allCustomers count]? [allCustomers valueForKey:@"customerID"] : nil;
    
    NSString *customerIDString   = [_allCustomersIDs count] ? [_allCustomersIDs componentsJoinedByString:@","] : @"";
    
    //customerIDString = @"";
    
    NSLog(@"CustomerIDs=%@",customerIDString);
    
     ASIHTTPRequest *request = [ConnectionManager createCustomersConnectionWithRepID:currentRep.repID wdSiteID:wdSiteID territoryIDs:territoryIDS customerIDs:customerIDString];
    request.delegate = self;
    self.currentCustomerRequest = request;
    

}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    Rep *currentRep = [CoreDataHandler currentRep];
    
    NSArray *activeTerritories = [Territory fetchAll:nil];
    
    NSArray *territories = [activeTerritories valueForKey:@"territoryID"];
    
   // [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
    
    _isFetchingFromServer = NO;
    NSLog(@"Response:%@",[request responseString]);
    
    NSDictionary *jsonDict = [request responseJSON];
    

    /*
    BOOL _isSiteIDChanged  = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
    
    BOOL _isRepDeactivated = [[jsonDict valueForKey:JSON_REP_DEACTIVATE_KEY] boolValue];
    
    NSNumber *isUnderMaintenance  = [jsonDict objectForKey:JSON_UNDER_MAINTENANCE_KEY];
    isUnderMaintenance = NULL_NIL(isUnderMaintenance);
    
     
    if(_isSiteIDChanged)
    {
        [app appDidChangeSiteID];
    }
    else if(_isRepDeactivated)
    {
        [app repDidDeactivate];
    }

    else if([isUnderMaintenance boolValue])
    {
        [app showUnderMaintainenceScreen:YES];
    }
    else
    */
    BOOL _isValidResponse = [ConnectionManager isValidResponse:jsonDict];
    if(_isValidResponse)
    {
        NSNumber *_status = [jsonDict valueForKey:JSON_STATUS_KEY];
        int status = [_status intValue];
        
        app.isCustomerLoaded=YES;
        
        NSArray *_customers = [jsonDict valueForKey:JSON_DETAILS_KEY];
       // NSArray *updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
        NSArray *deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
        
        
      //  int status = 1;
        
        
        NSLog(@"status=%d",status);
        
        if(status==1)
        {
            
            NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
            
            [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_CUSTOMERS];
            
            //========================= FETCH ACCORDING TO TERRITORY ============================
            
            NSArray *currentTerritories = [jsonDict valueForKey:@"repTerritoryDetails"];
            currentTerritories = NULL_NIL(currentTerritories);
            
            NSMutableArray *_currentTerritories = [NSMutableArray array];
            for(NSDictionary *territoryDict in currentTerritories)
            {
                Territory *aTerritory = (Territory *) [Territory createObjectFromDictionary:territoryDict inContext:DEFAULT_CONTEXT];
                
                [_currentTerritories addObject:aTerritory];
            }
            
            
            NSMutableArray *territoryToDelete = [NSMutableArray array];
            NSArray *delete = [_currentTerritories count] ?  [activeTerritories filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (self.territoryID IN %@)",[_currentTerritories valueForKey:@"territoryID"]]] : nil;
            
            if([delete count])
                [territoryToDelete addObjectsFromArray:[delete valueForKey:@"territoryID"]];
            
            //============== REMOVE PREVIOUS TERROTORIES AND ADD CURRENT TERRITORIES ===========
            
            if([_currentTerritories count])
            {
                for(Territory *territory in activeTerritories)
                {
                    [currentRep removeTerritoriesObject:territory];
                    
                    [SSCoreDataManager deleteObject:territory];
                }
                
                NSSet *territorySet = [NSSet setWithArray:_currentTerritories];
                if(territories)
                    [currentRep addTerritories:territorySet];
            }
            
            
            //============ DELETE CUSTOMER NOT FALLS INTO CURRENT TERRITORY =============== //
            
            if([territoryToDelete count])
            {
                NSPredicate *deleteCustomerPredicate = [NSPredicate predicateWithFormat:@"self.territoryID IN %@",territoryToDelete];
                
                NSMutableArray *customersToDelete = [Customer fetchWithPredicate:deleteCustomerPredicate onAttributes:nil sortDescriptors:nil limit:0 error:nil];
                
                // NSMutableArray *customersToDelete = [Customer fetchWithPredicate:deleteCustomerPredicate error:nil];
                
                NSArray *  _deletedIDs = [customersToDelete valueForKey:@"customerID"];
                
                NSMutableArray *idsToDelete = deletedIDs ? [NSMutableArray arrayWithArray:deletedIDs] : [NSMutableArray array];
                [idsToDelete addObjectsFromArray:_deletedIDs];
                
                deletedIDs =[NSArray arrayWithArray:idsToDelete];
                
                
            }
        }
        else if(status==2)
        {
            //#ifndef LIVE
            
            _customers = nil;
           // updatedIDs = nil;
            
            NSMutableArray *customersToDelete = [Customer fetchWithPredicate:nil onAttributes:[NSArray arrayWithObject:@"customerID"] sortDescriptors:nil limit:0 error:nil];
            
            deletedIDs = [customersToDelete valueForKey:@"customerID"];
            
            //#endif
        }
        
        //========================================================================
        
        
        if(status!=0)
        {
            
            if(!([deletedIDs count] || [_customers count]))
            {
                 [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
                [self.tableView reloadData];
            }
            
   
            /*
            [Customer populateAsyncPerformingDelete:deletedIDs?deletedIDs:[NSArray array] update:updatedIDs?updatedIDs:[NSArray array] insertOnArray:_customers?_customers:[NSArray array] onAttribute:@"customerID" completion:^(NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
             {
                 
                 if([insertedResult count])
                     [self.customers addObjectsFromArray:insertedResult];
                 
                 [self filterCustomersByName];
                 [self setupSectionDict];
                
                 [self.tableView reloadData];
                 // [ProgressHUD hideAnimated:YES];
                 
                 [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
                 
                 if(self.customersDidLoadCallback) self.customersDidLoadCallback();
             }
             
             deletionBlock:^(NSMutableArray *deletedResult)
             {
                 if([deletedResult count])
                     [self.customers removeObjectsInArray:deletedResult];
                 
                 if([self.searchArray count])
                     [self.searchArray removeObjectsInArray:deletedResult];
             }];
           */
            
            [Customer populateAsyncPerformingDelete:deletedIDs?deletedIDs:[NSArray array] update:nil insertOnArray:_customers?_customers:[NSArray array] onAttributes:[NSArray arrayWithObjects:@"customerID",@"oldCustomerID", nil] completion:^(NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
             {
                 //[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                 
                 if([insertedResult count])
                     [self.customers addObjectsFromArray:insertedResult];
                 
                 [self filterCustomersByName];
                 [self setupSectionDict];
                 
                 [self.tableView reloadData];
                 // [ProgressHUD hideAnimated:YES];
                 
                 [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
                 
                 if(self.customersDidLoadCallback) self.customersDidLoadCallback();
             }
             deletionBlock:^(NSMutableArray *deletedResult)
            {
                 
                if([deletedResult count])
                   [self.customers removeObjectsInArray:deletedResult];
                 
                if([self.searchArray count])
                   [self.searchArray removeObjectsInArray:deletedResult];
             }];

        }
        else if(!app.isCustomerFetching)
        {
            [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
            
            [self.tableView reloadData];
        }
    }
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
    _isFetchingFromServer = NO;
    [self.tableView reloadData];
}



-(void)hideProgressLoader
{
    //[ProgressHUD hideAllHUDsForView:self.view animated:NO];
    
     if([ProgressHUD hideHUDForView:self.view animated:YES])
     {
         NSLog(@"HUD Hidden");
     }

}

-(void)fetchCustomers
{
    //[ProgressHUD showWithTitle:@"Loading..."];
    _isFetchingLocally = YES;
    
    
    [CoreDataHandler fetchAllCustomersOnCompletion:^(NSArray *result, NSError *error)
    {
        _isFetchingLocally = NO;
        
        self.customers=result?[NSMutableArray arrayWithArray:result]:[NSMutableArray array];
        [self filterCustomersByName];
        [self setupSectionDict];
        [self.tableView reloadData];
        
                
        if(self.customersDidLoadCallback) self.customersDidLoadCallback();
        
        if([self.customers count])
        {
            //[self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
        }
        
        if(!app.isCustomerFetching)
        {
            //[ProgressHUD hideHUDForView:self.view animated:YES];
            [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:1.0];
        }

    }];
}

-(void)customersDidFinishLoad:(NSNotification *)notif
{
    if (_isFetchingFromServer || app.isCustomerFetching) return;
    
    NSNumber *success = [notif object];

    if([success boolValue])
    {
      [self fetchCustomers];
    }
    else
    {
        [self requestFailed:nil];
    }

}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    isSearching=NO;
    
    //self.navigationController.navigationBar.tintColor=BACKGROUND_COLOR;
     [self.navigationController.navigationBar addBackGroundImage:@"header_blank.png"];
     [self addTitleImage:@"customers.png"];
    [self addRefreshButtonWithSelector:@selector(refreshButtonAction:)];
    
    [self setupHeaderBar];
    [self setupTableView];
    [self setupTableHeaderView];
    
    [ProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    
    [self fetchCustomers];
    [self fetchCustomersFromServer];
    
    
    [self.view addLeftBoderWithColor:[UIColor colorWithWhite:0.8f alpha:1.0f] borderWidth:1.0f];
    

    [OfflineManager sharedManager].delegate = self;
    
    /*
    __block CustomersViewController *weakSelf = self;
    [[OfflineManager sharedManager] addCustomersLoadCallback:^(BOOL isEditMode)
    {
        if(!isEditMode)
        {
            [weakSelf fetchCustomers];
        }
    } ];
    */
    
}

-(void)offlineManagerDidSendCustomer:(BOOL)isEditMode isDeleted:(BOOL)isDeleted
{
    if(isDeleted)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:CustomerDidDeletedOnDuplicate object:nil];
    }
    
    if(!isEditMode || isDeleted)
    {
        [self fetchCustomers];
    }
}

-(void)filterDatasource
{
    if(!self.searchArray) self.searchArray = [NSMutableArray array];

    NSArray *result = nil;
    
    if(self.filterPredicte && self.searchPredicte)
    {
        isSearching = YES;
        
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:self.filterPredicte,self.searchPredicte, nil]];
        
        result =  [self.customers filteredArrayUsingPredicate:predicate];
    }
    else if(self.filterPredicte)
    {
        isSearching = YES;
        result =  [self.customers filteredArrayUsingPredicate:self.filterPredicte];
        
    }
    else if(self.searchPredicte)
    {
        isSearching = YES;
        result =  [self.customers filteredArrayUsingPredicate:self.searchPredicte];
    }
    else
    {
        isSearching = NO;
        result = self.customers;
    }
    
    if(result)
    {
        [self.searchArray setArray:result];
    }
    
    //isSearching = filterButton.selected;
    
    if([self.searchArray count] && [self.filterDescriptos count])
    {
        isSearching = YES;
        [self.searchArray sortUsingDescriptors:self.filterDescriptos];
    }
}


#pragma mark -
#pragma mark FILTER
-(void)filterAction:(UIButton *)sender
{
    if([searchBar isFirstResponder]) [searchBar resignFirstResponder];
    
    UIPopoverController *popovervc=self.currentPopoverController;
    
    if(!popovervc)
    {
        FilterViewController *filterViewcontroller = [[FilterViewController alloc]init];
        filterViewcontroller.delegate = self;
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:filterViewcontroller];
        filterViewcontroller.contentSizeForViewInPopover = CGSizeMake(350.0, 590.0);
        
        //popovervc = [navController presentInPopOverFrom:sender inView:self.view animated:YES];
        
        popovervc = [[UIPopoverController alloc]initWithContentViewController:navController];
        filterViewcontroller.popoverController = popovervc;

        
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        popovervc.popoverBackgroundViewClass = [MBPopoverBackgroundView class];
     }
        
        self.currentPopoverController = popovervc;
        
       [filterViewcontroller release];
       [navController release];
    }
    //else
    {
        [popovervc presentPopoverFromRect:[sender frame] inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

-(void)filterViewController:(FilterViewController *)filterVC willFilterWithOptioins:(NSDictionary *)filterOptions
{
    filterButton.selected = YES;
    
    NSMutableArray *sortDescriptors = [NSMutableArray array];
    
    NSArray *catArr   = [filterOptions valueForKey:@"CATEGORY"];
    NSArray *sortKeys = [filterOptions valueForKey:@"SORT"];
    BOOL isAscending  = [[filterOptions valueForKey:@"ASCENDING"] boolValue];
    //NSNumber *isGetGenuine = [filterOptions valueForKey:@"GETGENUINE"];
    int  status = [[filterOptions valueForKey:@"STATUS"] intValue];
    
     NSNumber *territoryID = [filterOptions valueForKey:@"TERRITORY"];
     NSString *membersipKey = [filterOptions valueForKey:@"MEMBERSHIP"];
    
    isSearching=YES;
    
    NSMutableArray *filterPredicates = [NSMutableArray array];
    NSPredicate *_filterPredicte=nil;
    
    if([catArr count])
    {
       // NSPredicate *catPredicte  = [NSPredicate predicateWithFormat:@"self.categoryName in %@",catArr];
        
        //NSPredicate *catPredicte  = [NSPredicate predicateWithFormat:@"self.categoryName in %@ OR self.categoryID=%@",catArr,[NSNumber numberWithInt:0]];
        
        NSPredicate *catPredicte  = [NSPredicate predicateWithFormat:@"self.categoryID IN %@",catArr];
        
        [filterPredicates addObject:catPredicte];
    }
    
    /*
    if([isGetGenuine boolValue])
    {
        NSPredicate *getGenuinePredicte  = [NSPredicate predicateWithFormat:@"self.getGenuineMember = %@",isGetGenuine];
        [filterPredicates addObject:getGenuinePredicte];
    }
    */
    
    if(membersipKey)
    {
        NSPredicate *membershipPredicte  = [NSPredicate predicateWithFormat:@"self.%K = %@",membersipKey,[NSNumber numberWithBool:YES]];
        [filterPredicates addObject:membershipPredicte];
    }

    
    if(status)
    {
        NSPredicate *statusPredicte  = [NSPredicate predicateWithFormat:@"self.isAccountConfirmed = %@",[NSNumber numberWithBool:(status==1)]];
        [filterPredicates addObject:statusPredicte];
    }
    
    if(territoryID)
    {
        NSPredicate *territoryPredicte  = [NSPredicate predicateWithFormat:@"territoryID = %@",territoryID];
        [filterPredicates addObject:territoryPredicte];
    }
    
    if([filterPredicates count])
        _filterPredicte = [NSCompoundPredicate andPredicateWithSubpredicates:filterPredicates];
    
    NSLog(@"FILTER CUSTOMER QUERY:%@",filterPredicte.predicateFormat);
    
    for(NSString *sortByKey in sortKeys)
    {
        BOOL _isDateKey =  ([sortByKey rangeOfString:@"Date"].location!=NSNotFound);
        
        NSSortDescriptor *sortDescriptor = _isDateKey ? [[NSSortDescriptor alloc] initWithKey:sortByKey ascending:isAscending]    : [[NSSortDescriptor alloc] initWithKey:sortByKey ascending:isAscending selector:@selector(localizedCaseInsensitiveCompare:)];
        [sortDescriptors addObject:sortDescriptor];
        [sortDescriptor release];
    }
    
    self.filterDescriptos = sortDescriptors;
    
    // NSPredicate *filterPredicte=[NSPredicate predicateWithFormat:@"self.CATEGORY in %@ AND self.STATE in %@",catArr,states];
    
    self.filterPredicte = _filterPredicte;

    [self filterDatasource];
    
    [self.tableView reloadData];
    
    [self.currentPopoverController dismissPopoverAnimated:YES];
}


-(void)filterViewControllerDidCancel:(FilterViewController *)filterVC
{
    if(isSearching)
    {
        filterButton.selected = NO;
        isSearching           = NO;
        self.filterPredicte   = nil;
        self.filterDescriptos = nil;
        [self.searchArray removeAllObjects];
        
        if(self.searchPredicte)
        {
            [self filterDatasource];
        }
        
        [self.tableView reloadData];
    }
    
    [self.currentPopoverController dismissPopoverAnimated:YES];
    
    if(isSearching)
    {
        filterVC.delegate = nil;
        self.currentPopoverController = nil;
    }
}

-(void)deleteContext:(NSManagedObjectContext *)context
{
    [context reset];
    [context release];
}


#pragma mark-
#pragma mark ADD NEW CUSTOMER



-(void)addNewCutomerAction:(UIButton *)sender
{
    app.shouldAvoidMenuSelection = YES;
    
    NSManagedObjectContext *newContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    // newContext.persistentStoreCoordinator = DEFAULT_CONTEXT.persistentStoreCoordinator;
    //[newContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    newContext.parentContext = DEFAULT_CONTEXT;
    newContext.undoManager=nil;

    self.customerEditingContext = newContext;
    
    Customer *newCustomer = (Customer *)[Customer insertNewObjectInContext:newContext];
    newCustomer.customerID =  [NSNumber numberWithUnsignedInteger:[[NSDate date] hash]];
    newCustomer.oldCustomerID =  newCustomer.customerID;
    
    
     CustomerSummaryViewController *summaryController = [[CustomerSummaryViewController alloc] initWithCustomer:newCustomer];
    summaryController.profileInfoEditViewController.isNewCustomer  = YES;
    summaryController.profileInfoEditViewController.editingContext = newContext;
    [summaryController enableEditMode:YES];
    [summaryController addTitle:@"Create New Customer" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    summaryController.profileInfoEditViewController.delegate = self;
    [self setAssociateObject:summaryController forKey:CREATE_NEW_CUSATOMER_CONTROLLER];

    [self.navigationController pushViewController:summaryController animated:YES];
    [summaryController release];
    
    [newContext release];
}

-(void)profileInfoDidEditViewController:(ProfileInfoEditViewController *)controller didSaveCustomer:(Customer *)customer
{
    CustomerSummaryViewController *summaryController = [self associateObjectForKey:CREATE_NEW_CUSATOMER_CONTROLLER];
    //[summaryController.customerSummaryDetailController reload];
    
    NSManagedObjectContext *newContext = self.customerEditingContext;
    customer.parentRep =  [CoreDataHandler currentRepInContext:newContext];
    
  __block  NSError *error = nil;
    

    //************* MERGE CHANGES TO MAIN CONTEXT **********
    // if([newContext hasChanges])
      // [newContext save:&error];
    
    [newContext performBlockAndWait:^{
        
        if([newContext hasChanges])
        [newContext save:&error];
    }];
    
    if(!error)
    {
        [[SSCoreDataManager sharedManager] save:nil];
        Customer *_newCustomer = (Customer *) [customer inContext:DEFAULT_CONTEXT];
        
        if(_newCustomer)
        {
            [self.customers addObject:_newCustomer];
            
            [[OfflineManager sharedManager] addCustomer:_newCustomer];
            
            [self filterCustomersByName];
            [self setupSectionDict];
            [self.tableView reloadData];
        }
        
        
        
        [summaryController.navigationController popViewControllerAnimated:YES];
    }
    
 
    [self.customerEditingContext reset];
    self.customerEditingContext = nil;
    
    app.shouldAvoidMenuSelection = NO;
    
     [self setAssociateObject:nil forKey:CREATE_NEW_CUSATOMER_CONTROLLER];
}

-(void)profileInfoDidEditViewControllerDidCancel:(ProfileInfoEditViewController *)controller
{
    /*
    CustomerSummaryViewController *summaryController = [self associateObjectForKey:CREATE_NEW_CUSATOMER_CONTROLLER];
    
    // [summaryController.customerSummaryDetailController cancel];
    
    [UIAlertView alertViewWithTitle:@"" message:CUSTOMER_EXIT_MESSAGE cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObject:@"Yes"] onDismiss:^(int buttonIndex)
     {
          [summaryController.customerSummaryDetailController cancel];
         
         [self.customerEditingContext reset];
         self.customerEditingContext = nil;
       
         [summaryController.navigationController popViewControllerAnimated:YES];
         app.shouldAvoidMenuSelection = NO;
         [self setAssociateObject:nil forKey:CREATE_NEW_CUSATOMER_CONTROLLER];
         
        
     }
    onCancel:^{
    
    }];
    */
    
    
    CustomerSummaryViewController *summaryController = [self associateObjectForKey:CREATE_NEW_CUSATOMER_CONTROLLER];
    
    [summaryController.customerSummaryDetailController cancel];

    [self.customerEditingContext reset];
    self.customerEditingContext = nil;

    [summaryController.navigationController popViewControllerAnimated:YES];
    app.shouldAvoidMenuSelection = NO;
    [self setAssociateObject:nil forKey:CREATE_NEW_CUSATOMER_CONTROLLER];

}

#pragma mark -
-(void)refreshButtonAction:(id)sender
{
    [self fetchCustomersFromServer];
    
    //====== Reset on Refresh==========
    if(isSearching)
    {
        self.searchPredicte = nil;
        self.filterPredicte = nil;
        self.filterDescriptos=nil;
        
        searchBar.text=nil;
        [searchBar resignFirstResponder];
       // isSearching=NO;
        
        filterButton.selected = NO;
        self.currentPopoverController = nil;
        
        isSearching=NO;
        [self.searchArray removeAllObjects];
        [self.tableView reloadData];
    }
}
#pragma mark -
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
   // [self setupSectionDict];
    
    
    MenuViewController *_menuVC=(MenuViewController *)[app.splitViewController masterController];  
    [_menuVC showVisitingCustomerName:nil];
    
    
    //------------------ RELOAD FILTERED DATA IF AVAILABLE ---------------
    if(self.filterPredicte)
    {
      [self filterDatasource];
      [self.tableView reloadData];
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

-(ScheduledVisit *)loadNextUpcommingScheduledVisitsForCustomer:(Customer *)aCustomer
{
    NSDate *_currentDate=[NSDate date];
    
    // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(self.customer.customerID=%@ AND self.reviewedVisitID=%@) AND (self.visitStartDate<=%@ AND self.visitEndDate>=%@)",self.customer.customerID,[NSNumber numberWithLong:0] ,_currentDate,_currentDate];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((self.customer.customerID=%@ OR customerID=%@) AND self.reviewedVisitID=%@) AND (self.visitEndDate>=%@)",aCustomer.customerID,aCustomer.customerID,[NSNumber numberWithLong:0] ,_currentDate,_currentDate];    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"visitStartDate" ascending:YES];
    
    NSMutableArray *scheduleVisits = [ScheduledVisit fetchWithPredicate:predicate onAttributes:[NSArray arrayWithObject:@"visitStartDate"] sortDescriptors:[NSArray arrayWithObject:sortDescriptor] limit:0 error:nil];
    
    [sortDescriptor release];
    
    return [scheduleVisits count] ? [scheduleVisits objectAtIndex:0] : nil;
}


-(ScheduledVisit *)loadLastScheduledVisitsForCustomer:(Customer *)aCustomer
{
    NSDate *_currentDate=[NSDate date];
    
  
   // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((self.customer.customerID=%@ OR customerID=%@) AND self.reviewedVisitID!=%@) AND (self.visitEndDate<%@ AND self.isVisitPublished=%@)",aCustomer.customerID,aCustomer.customerID,[NSNumber numberWithLong:0] ,_currentDate,_currentDate,[NSNumber numberWithBool:YES]];
    
     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((self.customer.customerID=%@ OR customerID=%@)) AND (self.visitStartDate<%@ AND self.isVisitPublished=%@)",aCustomer.customerID,aCustomer.customerID ,_currentDate,[NSNumber numberWithBool:YES]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"visitEndDate" ascending:NO];
    
    NSMutableArray *scheduleVisits = [ScheduledVisit fetchWithPredicate:predicate onAttributes:nil sortDescriptors:[NSArray arrayWithObject:sortDescriptor] limit:0 error:nil];
    
    //[NSArray arrayWithObject:@"visitEndDate"]
    
    [sortDescriptor release];
    
    return [scheduleVisits count] ? [scheduleVisits objectAtIndex:0] : nil;
}


-(NSMutableArray *)currrentArray
{
    return isSearching?self.searchArray:self.customers;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    
    //NSInteger count=[[self currrentArray] count];
    
    indexBar.hidden = isSearching;
    
    NSInteger count= isSearching ? [[self currrentArray] count] : [_sectionArr count];
    return MAX(1,count);
    
    //return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSInteger count=[[self currrentArray] count];
    
    NSDictionary *sectionInfo = section<[_sectionArr count] ?  [_sectionArr objectAtIndex:section] : nil;
    NSInteger count = isSearching ? 1 : [[sectionInfo objectForKey:@"row"] count];
    
    return MAX(1,count);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TABLE_ROW_HEIGHT;
}

- (void)indexSelectionDidChange:(CMIndexBar *)IndexBar atIndex:(int)index IndexTitle:(NSString*)title
{
    if(isSearching) return;
    
    NSUInteger _section = [_sectionArr indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop)
        {
            NSDictionary *sectionDict=(NSDictionary *)obj;
            NSString *key = [sectionDict valueForKey:@"index"];
            
            if([key isEqualToString:title])
            {
                NSLog(@"ScrollToSection:%d",idx);
                
                *stop=YES;
                return YES;
            }
            
            return NO;
        }];
    
    
    if (_section!=NSNotFound && _section < [_sectionArr count])
    {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:_section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

/*
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 2.0;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerLineSeparator = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 2.0)] autorelease];
    footerLineSeparator.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.15];
    
    return footerLineSeparator;
}
*/

/*
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView;                                                    // 
{
    return  isSearching ? nil : indexedArray;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if(isSearching) return -1;
    
    //==================================== SCROLLS TO SELECTED INDEX ==============================
   int _section = [_sectionArr indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop)
      {
        NSDictionary *sectionDict=(NSDictionary *)obj;
        NSString *key = [sectionDict valueForKey:@"index"];
        
        if([key isEqualToString:title])
        {
            NSLog(@"ScrollToSection:%d",idx);
            
            *stop=YES;
            return YES;
        }

        return NO;
    }];
    
    
    return _section ;
    
    //===================================================================================================
    
    return index;
}
*/
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(isSearching) return nil;
    
    NSDictionary *sectionInfo = section < [_sectionArr count] ?  [_sectionArr objectAtIndex:section] : nil;
    
    if(!sectionInfo) return nil;
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 20)];
    sectionView.backgroundColor  = LOGIN_BG_COLOR;
    sectionView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    
    UILabel *sectionIndexLabel=[UILabel labelWithText:@"" frame:CGRectMake(10.0, 2, 100, 15) textColor:[UIColor whiteColor] bgColor:[UIColor clearColor] fontsize:14];
    sectionIndexLabel.text = [sectionInfo objectForKey:@"index"];
    sectionIndexLabel.font=[UIFont boldSystemFontOfSize:14];
    [sectionView addSubview:sectionIndexLabel];
    
    return [sectionView autorelease];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSDictionary *sectionInfo = section < [_sectionArr count] ?  [_sectionArr objectAtIndex:section] : nil;
    NSMutableArray *arr= isSearching ?  [self currrentArray] : [sectionInfo valueForKey:@"row"] ;
    
    return isSearching || ![arr count] ? 0.0 : 20.0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier =  @"Cell";
    static NSString *CellIdentifier2 = @"Cell_NoCustomer";
    
     NSDictionary *sectionInfo = indexPath.section < [_sectionArr count] ?  [_sectionArr objectAtIndex:indexPath.section] : nil;
     NSMutableArray *arr= isSearching ?  [self currrentArray] : [sectionInfo valueForKey:@"row"] ;
    
    NSUInteger count=[arr count];
    
    if(count==0 )
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        
        if (cell == nil) 
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.textLabel.textColor = [UIColor lightGrayColor];
        }
        
        BOOL isFetching =  (_isFetchingFromServer || app.isCustomerFetching);
        
        //cell.textLabel.text = (indexPath.row==0) ? _isFetchingFromServer? @"Loading.." : @"No Customer(s)" : nil;
        
        cell.textLabel.text = (indexPath.row==0) ?  isFetching? @"" : @"No Customer(s)" : nil;
        // cell.textLabel.text = (indexPath.row==0) ?  @"No Customer(s)" : nil;
        
        return cell;

    }
    else
    {
        CustomerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

        if (cell == nil) 
        {
            cell = [[[CustomerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            
            
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
            UIView * selectedBackgroundView = [[UIView alloc] initWithFrame:cell.bounds];
            [selectedBackgroundView setBackgroundColor: [UIColor cellSelectedColorBlue]];
            [cell setSelectedBackgroundView:selectedBackgroundView];
            [selectedBackgroundView release];
            }
            
        }
        
       NSInteger index = isSearching ? indexPath.section : indexPath.row;
        
        Customer *_customer=(index < count) ? [arr objectAtIndex:index] : nil;
        cell.customerInfo=_customer;
      
    
        {
          ScheduledVisit *scheduledVisit = [self loadNextUpcommingScheduledVisitsForCustomer:_customer];
            
          if(scheduledVisit && scheduledVisit.visitStartDate)
          {
              _customer.nextVisit = [scheduledVisit.visitStartDate stringFromDateWithFormat:OFFER_DATE_FORMAT];
              cell.nextVisitLabel.text = _customer.nextVisit;
          }
          else
          {
                cell.nextVisitLabel.text = @"N/A";
           }
            
            
            ScheduledVisit *lastScheduledVisit = [self loadLastScheduledVisitsForCustomer:_customer];
            
            if(lastScheduledVisit && lastScheduledVisit.visitEndDate)
            {
                _customer.lastVisit = [lastScheduledVisit.visitEndDate stringFromDateWithFormat:OFFER_DATE_FORMAT];
                cell.lastVisitLabel.text = _customer.lastVisit;
            }
            else
            {
                cell.lastVisitLabel.text = @"N/A";
            }

        }

        
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    CustomerTableViewCell *_cell = (CustomerTableViewCell *)cell;
    if([_cell isKindOfClass:[CustomerTableViewCell class]])
       [_cell reset];

}

/*
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 return YES;
 }
 
 
 - (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 return UITableViewCellEditingStyleDelete;
 }
 -(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if(editingStyle==UITableViewCellEditingStyleDelete)
 {
     NSDictionary *sectionInfo = indexPath.section < [_sectionArr count] ?  [_sectionArr objectAtIndex:indexPath.section] : nil;
     NSMutableArray *arr= isSearching ?  [self currrentArray] : [sectionInfo valueForKey:@"row"] ;
     
     int count=[arr count];
     int index = isSearching ? indexPath.section : indexPath.row;
     
     NSLog(@"isSearching=%d",isSearching);
     
      Customer *_customer=(index < count) ? [arr objectAtIndex:index] : nil;
     [[OfflineManager sharedManager]  addCustomer:_customer isEditMode:NO willDelete:YES];
     
     [self.customers removeObject:_customer];
    // [arr removeObject:_customer];
     [self filterCustomersByName];
     [self setupSectionDict];
     [self.tableView reloadData];
     
     [SSCoreDataManager deleteObject:_customer];
 }
 }
*/ 

/*
-(void)showCustomer:(Customer *)_customer animated:(BOOL)animated
{
    app.fromCustomerTask=1;

    CustomerProfileViewController *_customerProfileVc=[[CustomerProfileViewController alloc] initWithCustomer:_customer];
      self.selectedCustomerProfile=_customerProfileVc;
    [self.navigationController pushViewController:_customerProfileVc animated:animated];
  
    [_customerProfileVc release];

    MenuViewController *_menuVC=(MenuViewController *)[app.splitViewController masterController];  
    [_menuVC showVisitingCustomerName:_customer.name];
}
*/

-(void)showCustomer:(Customer *)_customer animated:(BOOL)animated selectedTabIndex:(int)index
{
    app.fromCustomerTask=1;
    
    CustomerProfileViewController *_customerProfileVc=[[CustomerProfileViewController alloc] initWithCustomer:_customer];
    self.selectedCustomerProfile=_customerProfileVc;
    _customerProfileVc.parentTabbarControlller.selectedIndex=index;
    [self.navigationController pushViewController:_customerProfileVc animated:animated];
    
    [_customerProfileVc release];
    
    //MenuViewController *_menuVC=(MenuViewController *)[app.splitViewController masterController];  
    //[_menuVC showVisitingCustomerName:_customer.name];
}

-(void)selectCustomerForTask:(Task *)aTask
{
     Customer *_customer = aTask.parentCustomer;
     NSMutableArray *_customers = [self currrentArray];
    
    if([_customers containsObject:_customer])
    {
        [self showCustomer:_customer animated:NO selectedTabIndex:1];
        [self performSelector:@selector(setIndexOfController:) withObject:aTask afterDelay:0.15];//3
    }
}

-(void)setIndexOfController:(Task *)_task
{
    if(!self.selectedCustomerProfile.parentTabbarControlller) return;
    
     // self.selectedCustomerProfile.parentTabbarControlller.selectedIndex=1;
      //[self.selectedCustomerProfile.tasksViewController.taskListViewController selectTask:_task animated:NO];
    
      [self.selectedCustomerProfile.tasksViewController selectTask:_task animated:NO];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *sectionInfo = indexPath.section < [_sectionArr count] ?  [_sectionArr objectAtIndex:indexPath.section] : nil;
    NSMutableArray *arr= isSearching ?  [self currrentArray] : [sectionInfo valueForKey:@"row"] ;
    
    NSUInteger count=[arr count];
    NSInteger index = isSearching ? indexPath.section : indexPath.row;
    
    NSLog(@"isSearching=%d",isSearching);
    
    Customer *_customer=(index < count) ? [arr objectAtIndex:index] : nil;
    
    
    NSLog(@"Customer name=%@",_customer.isCustomerSentToServer);
    
    if(!_customer) return;
    
   // [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.tableView.userInteractionEnabled=NO;
    //[self showCustomer:_customer animated:YES];
    [self showCustomer:_customer animated:YES selectedTabIndex:0];
     //self.selectedCustomerProfile.parentTabbarControlller.selectedIndex=0;
    self.tableView.userInteractionEnabled=YES;
}

#pragma mark SearchBarDelegate

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    isSearching = searchText.length>0;
    
    if(searchText.length)
    {
      if(!self.searchArray) self.searchArray=[NSMutableArray array];
    
    //NSPredicate *_searchPredicate=[NSPredicate predicateWithFormat:@"self.name beginswith[c] %@",searchText];
        
   // NSPredicate *_searchPredicate=[NSPredicate predicateWithFormat:@"self.name contains[c] %@",searchText];
    
    //[NSPredicate predicateWithFormat:@"self.NAME beginswith[c] %@ OR self.CATEGORY beginswith[c] %@ OR self.STATE beginswith[c] %@",searchText,searchText,searchText];
        
    NSPredicate *_searchPredicate=[NSPredicate predicateWithFormat:@"self.name beginswith[c] %@ OR self.suburb beginswith[c] %@",searchText,searchText];

    
      self.searchPredicte = _searchPredicate;
    
       //NSArray *arr= [self.customers filteredArrayUsingPredicate:_searchPredicate];
       //[self.searchArray setArray:arr];
        
       
    }
    else
    {
         self.searchPredicte = nil;
    }
    
     [self filterDatasource];
    [self.tableView reloadData];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
   // isSearching=NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}


@end

