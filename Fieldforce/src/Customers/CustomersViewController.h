//
//  CustomersViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 21/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Customer,Task;

typedef void(^CustomersDidLoad) (void);
@interface CustomersViewController : UIViewController
{
    UITableView *_tableView;
    UILabel     *_sectionIndexLabel;
    NSMutableArray *_sectionArr;
    
    BOOL _isFetchingFromServer;
    BOOL _isFetchingLocally;
    UIButton *filterButton;
}

@property(nonatomic,retain) UITableView *tableView;
@property(nonatomic,retain) NSMutableArray *customers;
@property(nonatomic,assign) BOOL isSearching;



-(void)setupSectionDict;
-(void)selectCustomerForTask:(Task *)aTask;
-(void)addCustomersDidLoadCallback:(CustomersDidLoad)callBack;

@end
