//
//  CustomerTableViewCell.m
//  TestIndexedTableView
//
//  Created by RANDEM MAC on 29/05/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import "CustomerTableViewCell.h"
#import "Customer.h"

@interface CustomerTableViewCell()
{
    BOOL didLayout;
    
}
@property (readonly) int _logoCount;
@end

@implementation CustomerTableViewCell
@synthesize categoryLabel=_categoryLabel;
@synthesize nameLabel=_nameLabel;
@synthesize suburbLabel=_suburbLabel;
//@synthesize stateLabel=_stateLabel;
@synthesize lastVisitLabel=_lastVisitLabel;
@synthesize nextVisitLabel=_nextVisitLabel;

@synthesize customerInfo;
@synthesize _logoCount;



-(void)dealloc
{
    [_categoryLabel release];
    [_nameLabel release];
    [_suburbLabel release];
    //[_stateLabel release];
    [_lastVisitLabel release];
    [_nextVisitLabel release];
    [customerInfo release];
     customerInfo = nil;
    
    [_categoryImageView release];
    [_genuineCustomerImageView release];
    [_membershipImageView release];
    [_territoryLabel release];
    
    [super dealloc];
}
//categoty:47.0  genuine:47.0  name:260.0  suburb:126.0  territory:65.0  last visit:65.0  next Visit:65.0
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) 
    {
        self.opaque = YES;
      // CGFloat  lineSeparatorPosition[]={44.0,97.0,359.0,494.0,560.0,630.0,690.0};
        CGFloat  lineSeparatorPosition[]={44.0,97.0,359.0-27,494.0-27,560.0-27,630.0-27,690.0-27};
        
        UIView *contentView=self.contentView;
               
        _categoryImageView=[[UIImageView alloc] init];
        _categoryImageView.backgroundColor = [UIColor clearColor];
        _categoryImageView.opaque=YES;
        _categoryImageView.contentMode = UIViewContentModeScaleAspectFit;
        _categoryImageView.opaque=YES;
        [contentView addSubview:_categoryImageView];
        
        _genuineCustomerImageView=[[UIImageView alloc] init];
        _genuineCustomerImageView.backgroundColor = [UIColor clearColor];
        _genuineCustomerImageView.contentMode = UIViewContentModeScaleAspectFit;
        [contentView addSubview:_genuineCustomerImageView];
        
        _membershipImageView=[[UIImageView alloc] init];
        _membershipImageView.backgroundColor = [UIColor clearColor];
        _membershipImageView.contentMode = UIViewContentModeScaleAspectFit;
        [contentView addSubview:_membershipImageView];
        _membershipImageView.hidden = YES;
        
        _nameLabel=[[UILabel alloc] init];
        _nameLabel.textColor=[UIColor darkGrayColor];
        //_nameLabel.font=[UIFont systemFontOfSize:13];
         _nameLabel.font=[UIFont grotesqueFontOfSize:12];
        _nameLabel.backgroundColor=[UIColor clearColor];
        _nameLabel.textAlignment=NSTextAlignmentLeft;
        _nameLabel.numberOfLines=2;
        [contentView addSubview:_nameLabel];
        
        _suburbLabel=[[UILabel alloc] init];
        _suburbLabel.textColor=[UIColor darkGrayColor];
       // _suburbLabel.font=[UIFont systemFontOfSize:14];
        _suburbLabel.font=[UIFont grotesqueFontOfSize:13];
        _suburbLabel.backgroundColor=[UIColor clearColor];
        _suburbLabel.textAlignment=NSTextAlignmentCenter;
        [contentView addSubview:_suburbLabel];
         
        
        /*
        _stateLabel=[[UILabel alloc] init];
        _stateLabel.textColor=[UIColor darkGrayColor];
        //_stateLabel.font=[UIFont systemFontOfSize:14];
        _stateLabel.font=[UIFont grotesqueFontOfSize:13];
        _stateLabel.backgroundColor=[UIColor clearColor];
        _stateLabel.textAlignment=UITextAlignmentCenter;
       [contentView addSubview:_stateLabel];
        */
        
        _territoryLabel=[[UILabel alloc] init];
        _territoryLabel.textColor=[UIColor darkGrayColor];
        //_stateLabel.font=[UIFont systemFontOfSize:14];
        _territoryLabel.font=[UIFont grotesqueFontOfSize:13];
        _territoryLabel.backgroundColor=[UIColor clearColor];
        _territoryLabel.textAlignment=NSTextAlignmentCenter;
        [contentView addSubview:_territoryLabel];
        
        _lastVisitLabel=[[UILabel alloc] init];
       // _lastVisitLabel.font=[UIFont systemFontOfSize:14];
         _lastVisitLabel.font=[UIFont grotesqueFontOfSize:12];
        _lastVisitLabel.textColor=[UIColor darkGrayColor];
        _lastVisitLabel.backgroundColor=[UIColor clearColor];
        _lastVisitLabel.textAlignment=NSTextAlignmentCenter;
        _lastVisitLabel.numberOfLines = 2;
       [contentView addSubview:_lastVisitLabel];
        
        _nextVisitLabel=[[UILabel alloc] init];
        _nextVisitLabel.textColor=[UIColor darkGrayColor];
        //_nextVisitLabel.font=[UIFont systemFontOfSize:14];
        _nextVisitLabel.font=[UIFont grotesqueFontOfSize:12];
        _nextVisitLabel.backgroundColor=[UIColor clearColor];
        _nextVisitLabel.textAlignment=NSTextAlignmentCenter;
        _nextVisitLabel.numberOfLines = 2;
       [contentView addSubview:_nextVisitLabel];
        

        for(int i=0;i<6;i++)
        {
            CGFloat locX=lineSeparatorPosition[i];
            CGRect _frame= CGRectMake(locX, 0, 1.0,CGRectGetHeight(contentView.bounds));
            UIView *lineSeparator =[[UIView alloc] initWithFrame:_frame];
            lineSeparator.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.15];
            lineSeparator.autoresizingMask = UIViewAutoresizingFlexibleHeight;
            lineSeparator.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
            [contentView addSubview:lineSeparator];
            lineSeparator.opaque=YES;
            [lineSeparator release];
          
        }

       if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            self.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
 //   if(didLayout) return;
    
    CGRect _bounds = self.contentView.bounds;
    
    CGRect _categoryImageViewFrame = CGRectMake(0.0, (CGRectGetHeight(_bounds)-20.0)/2.0, 46.0, 20.0);
    
    
    CGRect _genuineCustomerImageViewFrame = CGRectMake(CGRectGetMaxX(_categoryImageViewFrame)+18.0, (CGRectGetHeight(_bounds)-20.0)/2.0, 20.0, 20.0);
    
   // CGRect _categoryLabelFrame = CGRectMake(0, 0, 100, CGRectGetHeight(_bounds));
    CGRect _nameLabelFrame=CGRectMake(CGRectGetMaxX(_genuineCustomerImageViewFrame)+26.0, 0, 225.0, CGRectGetHeight(_bounds));
    
    CGRect _suburbLabelFrame =_nameLabelFrame;
    _suburbLabelFrame.origin.x=CGRectGetMaxX(_nameLabelFrame);
    _suburbLabelFrame.size.width=127;
    
   // CGRect _stateLabelFrame =_nameLabelFrame;
    //_stateLabelFrame.origin.x=CGRectGetMaxX(_nameLabelFrame)+10;
    //_stateLabelFrame.size.width=121;
    
    CGRect _territoryLabelFrame = _nameLabelFrame;
    _territoryLabelFrame.origin.x=CGRectGetMaxX(_suburbLabelFrame)+5;
    _territoryLabelFrame.size.width=68;
    
    CGRect _lastVisitLabelFrame =_territoryLabelFrame;
    _lastVisitLabelFrame.origin.x=CGRectGetMaxX(_territoryLabelFrame);
    //_lastVisitLabelFrame.size.width=60;
    
    CGRect _nextVisitLabelFrame =_lastVisitLabelFrame;
    _nextVisitLabelFrame.origin.x=CGRectGetMaxX(_lastVisitLabelFrame);
    
    
    if(!CGRectEqualToRect(_categoryImageViewFrame, _categoryImageView.frame))
       _categoryImageView.frame = _categoryImageViewFrame;
    
  //  if(!CGRectEqualToRect(_genuineCustomerImageViewFrame, _genuineCustomerImageView.frame))
    //    _genuineCustomerImageView.frame = _genuineCustomerImageViewFrame;
    
    //if(!CGRectEqualToRect(_categoryLabelFrame, _categoryLabel.frame))
       // _categoryLabel.frame = _categoryLabelFrame;
    
    if(!CGRectEqualToRect(_nameLabelFrame, _nameLabel.frame))
        _nameLabel.frame  = _nameLabelFrame;
    
     
    if(!CGRectEqualToRect(_suburbLabelFrame, _suburbLabel.frame))
        _suburbLabel.frame  = _suburbLabelFrame;

   // if(!CGRectEqualToRect(_stateLabelFrame, _stateLabel.frame))
        //_stateLabel.frame  = _stateLabelFrame;
    
    if(!CGRectEqualToRect(_territoryLabelFrame, _territoryLabel.frame))
        _territoryLabel.frame  = _territoryLabelFrame;
    
    if(!CGRectEqualToRect(_lastVisitLabelFrame, _lastVisitLabel.frame))
        _lastVisitLabel.frame =_lastVisitLabelFrame;
    
    if(!CGRectEqualToRect(_nextVisitLabelFrame, _nextVisitLabel.frame))
        _nextVisitLabel.frame =_nextVisitLabelFrame;
    

    //_logoCount=[[self imageNamesForMembershipFromCustomer:self.customerInfo] count];
    if(_logoCount==1)
    {
        if(!CGRectEqualToRect(_genuineCustomerImageViewFrame, _genuineCustomerImageView.frame))
            _genuineCustomerImageView.frame = _genuineCustomerImageViewFrame;
        
        _membershipImageView.hidden = YES;
    }
    else if(_logoCount>=2)
    {
        _genuineCustomerImageViewFrame = CGRectMake(CGRectGetMaxX(_categoryImageViewFrame)+4.0, CGRectGetMinY(_genuineCustomerImageViewFrame), 20.0, 20.0);
        
        if(!CGRectEqualToRect(_genuineCustomerImageViewFrame, _genuineCustomerImageView.frame))
            _genuineCustomerImageView.frame = _genuineCustomerImageViewFrame;

        
     // CGRect _membershipImageViewFrame = CGRectMake(CGRectGetMaxX(_categoryImageViewFrame)+26.0, CGRectGetHeight(_bounds)-24.0, 20.0, 20.0);
        
         CGRect _membershipImageViewFrame = CGRectMake(CGRectGetMaxX(_categoryImageViewFrame)+26.0, CGRectGetMinY(_genuineCustomerImageViewFrame), 20.0, 20.0);
        if(!CGRectEqualToRect(_membershipImageViewFrame, _membershipImageView.frame))
            _membershipImageView.frame = _membershipImageViewFrame;
        
        _membershipImageView.hidden = NO;
    }
    else if(_logoCount==0)
    {
        if(!CGRectEqualToRect(_genuineCustomerImageViewFrame, _genuineCustomerImageView.frame))
            _genuineCustomerImageView.frame = _genuineCustomerImageViewFrame;
        
        _membershipImageView.hidden = YES;
    }
      
    //didLayout=YES;
}

#pragma mark -
#pragma mark Image name for Customer membership
-(NSMutableArray *)imageNamesForMembershipFromCustomer:(Customer *)aCustomer
{
    NSMutableArray *_logoNames = [NSMutableArray array];
    
    /*
    if([aCustomer.getGenuineMember boolValue])
    {
        [_logoNames addObject:@"genuine_logo.png"];
    }
    
    if([aCustomer.tradeClub boolValue])
    {
         [_logoNames addObject:@"tradeclub_logo.png"];
    }
    
    if([aCustomer.capricorn boolValue])
    {
        [_logoNames addObject:@"capricorn_logo.png"];
    }
    */
    
    
    if(aCustomer.membershipNumber.length)
    {
        [_logoNames addObject:@"genuine_logo.png"];
    }
    
    if(aCustomer.tradeClubNumber.length)
    {
        [_logoNames addObject:@"tradeclub_logo.png"];
    }
    
    if([aCustomer.capricornNumber intValue])
    {
        [_logoNames addObject:@"capricorn_logo.png"];
    }
   
    
    return _logoNames;
}
#pragma mark - 
-(void)setCustomerInfo:(Customer *)aCustomerInfo
{
    
  // if(![customerInfo isEqual:aCustomerInfo])
   {
       [customerInfo release];
       customerInfo = [aCustomerInfo retain];
   }
    
    NSLog(@"Customer info:%@",aCustomerInfo);
    
    NSString *customerCat= [aCustomerInfo getCategoryName];
    //_categoryLabel.text=[customerCat stringByAppendingString:@" "];
        
    _nameLabel.text   = aCustomerInfo.name;
    _suburbLabel.text = aCustomerInfo.suburb.length? aCustomerInfo.suburb : @"N/A";
    
    //_stateLabel.text=aCustomerInfo.state;
   // BOOL isGenuineCustomer = [aCustomerInfo.getGenuineMember boolValue];
   // _genuineCustomerImageView.image = isGenuineCustomer ? [UIImage imageNamed:[self imageNameForMembership]] : nil;
    
        
    _categoryImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_logo.png",[customerCat lowercaseString]]];
    
    NSString *lastVisitStr=aCustomerInfo.lastVisit;
    NSString *nextVisitStr=aCustomerInfo.nextVisit;
    
    _lastVisitLabel.text = lastVisitStr.length? lastVisitStr: @"N/A";
    _nextVisitLabel.text = nextVisitStr.length? nextVisitStr: @"N/A";
    
//    int territory = [aCustomerInfo.territory intValue];
//    _territoryLabel.text = territory ? [NSString stringWithFormat:@"%d",territory] : @"N/A";
    
 
    _territoryLabel.text = aCustomerInfo.territoryAbbreviation ? aCustomerInfo.territoryAbbreviation:  @"N/A";
    
    _isACConfirmed = aCustomerInfo.isAccountConfirmed.boolValue;
      NSLog(@" chedck %d",aCustomerInfo.isAccountConfirmed.boolValue);

    [self setTextColor];
    
    
    //================ SETUP LOGO HERE ===================
    _genuineCustomerImageView.image = nil;
    _membershipImageView.image = nil;
    
    NSMutableArray *imageNames = [self imageNamesForMembershipFromCustomer:aCustomerInfo];
    _logoCount = [imageNames count];
    
    //======== SET DEFAULT MEMBERSHIP LOGO HERE ==================
    //if(!_logoCount) [imageNames addObject:@"membership_logo.png"];
    
    int i=0;
    for(NSString *imageName in imageNames)
    {
        if(i==0)
        {
            _genuineCustomerImageView.image = [UIImage imageNamed:imageName];
        }
        else
        {
            _membershipImageView.image =  [UIImage imageNamed:imageName];
            break;
        }
        
        i++;
    }
      //================ SETUP LOGO HERE ===================
    
    /*
    _logoCount=0;
    if([aCustomerInfo.getGenuineMember boolValue])
    {
        _genuineCustomerImageView.image = [UIImage imageNamed:@"genuine_logo.png"];
        _logoCount++;
    }
    
    if([aCustomerInfo.tradeClub boolValue])
    {
        _membershipImageView.image =  [UIImage imageNamed:@"tradeclub_logo.png"];
        
        _logoCount++;
    }
    
    if([aCustomerInfo.capricorn boolValue])
    {
       // [_logoNames addObject:@"capricorn_logo.png"];
        
        _membershipImageView.image =  [UIImage imageNamed:@"capricorn_logo.png"];
        _logoCount++;
    }
     */
}

-(void)reset
{
    _genuineCustomerImageView.image=nil;
    _membershipImageView.image = nil;
    _categoryImageView.image = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    
    
   
   // BOOL _isACConfirmed = customerInfo.isAccountConfirmed.boolValue;
    
      //NSLog(@" chedck2 %d",_isACConfirmed);
   // [self manageBGColorOnSelection:selected accountConfirmed:_isACConfirmed];
    
    [self manageTextColorOnSelection:selected];
    
}

/*-(void)manageBGColorOnSelection:(BOOL)selected accountConfirmed:(BOOL)_isACConfirmed
{
    NSLog(@"_isACConfirmed = %d", _isACConfirmed);
    _nameLabel.textColor      =  _isACConfirmed  ? [UIColor darkGrayColor] : selected ? [UIColor whiteColor] :  [UIColor redColor];
    _suburbLabel.textColor    =  _isACConfirmed  ? [UIColor darkGrayColor] : selected ? [UIColor whiteColor] : [UIColor redColor];
    _territoryLabel.textColor =  _isACConfirmed  ? [UIColor darkGrayColor] : selected ? [UIColor whiteColor] :  [UIColor redColor];
    _lastVisitLabel.textColor =  _isACConfirmed  ? [UIColor darkGrayColor] : selected ? [UIColor whiteColor] : [UIColor redColor];
    _nextVisitLabel.textColor =  _isACConfirmed  ? [UIColor darkGrayColor] : selected ? [UIColor whiteColor] : [UIColor redColor];

}*/

-(void)setTextColor
{
    _nameLabelTextColor         = _nameLabel.textColor      =  _isACConfirmed  ? [UIColor darkGrayColor] : [UIColor redColor];
    _suburbLabelTextColor       = _suburbLabel.textColor    =  _isACConfirmed  ? [UIColor darkGrayColor] : [UIColor redColor];
    _territoryLabelTextColor    = _territoryLabel.textColor =  _isACConfirmed  ? [UIColor darkGrayColor] : [UIColor redColor];
    _lastVisitLabelTextColor    = _lastVisitLabel.textColor =  _isACConfirmed  ? [UIColor darkGrayColor] : [UIColor redColor];
    _nextVisitLabelTextColor    = _nextVisitLabel.textColor =  _isACConfirmed  ? [UIColor darkGrayColor] : [UIColor redColor];
    
}

-(void)manageTextColorOnSelection:(BOOL)selected
{
    _nameLabel.textColor      =   selected ? [UIColor whiteColor] : _nameLabelTextColor;
    _suburbLabel.textColor    =   selected ? [UIColor whiteColor] : _suburbLabelTextColor;
    _territoryLabel.textColor =   selected ? [UIColor whiteColor] : _territoryLabelTextColor;
    _lastVisitLabel.textColor =   selected ? [UIColor whiteColor] : _lastVisitLabelTextColor;
    _nextVisitLabel.textColor =   selected ? [UIColor whiteColor] : _nextVisitLabelTextColor;
    
}


#if LOCAL
#endif


@end
