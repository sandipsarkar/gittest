//
//  CustomerTableViewCell.h
//  TestIndexedTableView
//
//  Created by RANDEM MAC on 29/05/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Customer;
@interface CustomerTableViewCell : UITableViewCell
{
    UILabel *_categoryLabel;
    UIImageView *_categoryImageView;
    UILabel *_nameLabel;
    UILabel *_suburbLabel;
    //UILabel *_stateLabel;
    UILabel *_lastVisitLabel;
    UILabel *_nextVisitLabel;
    UIImageView *_genuineCustomerImageView;
    UIImageView *_membershipImageView;
    UILabel *_territoryLabel;
    
    UIColor* _nameLabelTextColor;
    UIColor* _suburbLabelTextColor;
    UIColor* _territoryLabelTextColor;
    UIColor* _lastVisitLabelTextColor;
    UIColor* _nextVisitLabelTextColor;
    
    BOOL _isACConfirmed;

    //UIView *catContainer;
    
    
}

@property (nonatomic,readonly)UILabel *categoryLabel;
@property (nonatomic,readonly)UILabel *nameLabel;
@property (nonatomic,readonly)UILabel *suburbLabel;
//@property (nonatomic,readonly)UILabel *stateLabel;
@property (nonatomic,readonly)UILabel *lastVisitLabel;
@property (nonatomic,readonly)UILabel *nextVisitLabel;


@property(nonatomic,retain)Customer *customerInfo;

-(void)manageBGColorOnSelection:(BOOL)selected accountConfirmed:(NSNumber* )_isACConfirmed;

-(void)reset;

@end
