//
//  CustomerListViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomerListViewController.h"
#import "Customer.h"
#import "Rep.h"
#import "WholesaleDealer.h"


#define SEARCHBAR_HEIGHT 52.0

@interface CustomerListViewController ()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSInteger selectedRow;
    BOOL isSearching;
    
    NSMutableArray *_selectedCustomers;
}

@property(nonatomic,copy)   CustomerSelectCallback customerSelectCallback;
@property(nonatomic,copy)   DidSelectCustomersCallback didSelectCustomersCallback;
@property(nonatomic,retain) NSArray *searchedArray;

-(void)sortCustomersByName;

@end

@implementation CustomerListViewController
@synthesize customers;
@synthesize selectedCustomerName;
@synthesize customerSelectCallback;
@synthesize didSelectCustomersCallback;
@synthesize searchedArray;
@synthesize allowMultipleSelection;

-(void)dealloc
{
    [_tableView release];
    [searchBar release];
    [customers release];
    customers = nil;
    [selectedCustomerName release] ;
    selectedCustomerName=nil;
    [searchedArray release];
    searchedArray = nil;
    
    //if(customerSelectCallback) [customerSelectCallback release];
    [_selectedCustomers release];
    [customerSelectCallback release];
    [didSelectCustomersCallback release];
    
    [super dealloc];
}

-(id)init
{
    //self = [super initWithStyle:UITableViewStylePlain];
    self = [super init];
    if (self)
    {
        selectedRow=NSNotFound;
        isSearching=NO;
    }
    
    return self;
}

-(id)initWithCustomerName:(NSString *)name
{
    self = [self init];
    if (self) 
    {
        selectedCustomerName=[name copy];
        selectedRow=NSNotFound;
        isSearching=NO;
    }
    
    return self;
}

-(void)addCustomerSelectCallback:(CustomerSelectCallback)callback
{
    if(callback)
        self.customerSelectCallback=callback;
}

-(void)addDidSelectCustomersCallback:(DidSelectCustomersCallback)callback
{
    if(callback)
        self.didSelectCustomersCallback=callback;
}

-(void)fetechCustomersFromServer
{
    if([customers count]) return;
    
    Rep *currentRep=[CoreDataHandler currentRep];
    
   __block ASIHTTPRequest *request = [ConnectionManager createCustomersConnectionWithRepID:currentRep.repID wdSiteID:currentRep.parentWdSite.wdSiteID];
    
   __block CustomerListViewController *weakSelf =self;
    
   [request setCompletionBlock:^{
    
    NSLog(@"Response:%@",[request responseString]);
    
    NSDictionary *jsonDict = [request responseJSON];
    
    BOOL _isSiteIDChanged = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
    BOOL _isRepDeactivated = [[jsonDict valueForKey:JSON_REP_DEACTIVATE_KEY] boolValue];
       
    if(_isSiteIDChanged)
    {
           [app appDidChangeSiteID];
    }
    else if(_isRepDeactivated)
    {
           [app repDidDeactivate];
    }
    else 
    {
        NSArray *_customers = [jsonDict valueForKey:JSON_DETAILS_KEY];
        NSArray *updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
        NSArray *deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
    
        NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
        [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_CUSTOMERS];
        
        [Customer populateAsyncPerformingDelete:deletedIDs update:updatedIDs insertOnArray:_customers onAttribute:@"customerID" completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error) 
         {
             //[deletedIDs release];
             //[updatedIDs release];
             
             if([deletedResult count])
                 [weakSelf.customers removeObjectsInArray:deletedResult];
             
          
             
             if([insertedResult count])
                 [weakSelf.customers addObjectsFromArray:insertedResult];
            
             
             [weakSelf sortCustomersByName];
             [weakSelf.tableView reloadData];
             // [ProgressHUD hideAnimated:YES];
             
             //if(self.customersDidLoadCallback) self.customersDidLoadCallback();
         }];
       }
  }];
    
    [request setFailedBlock:^{
        
        //[ProgressHUD hideAnimated:YES];
    }];
    
}

-(void)sortCustomersByName
{
    if(![self.customers count]) return;
    
    /*
    NSSortDescriptor *_descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    [self.customers sortUsingDescriptors:[NSArray arrayWithObject:_descriptor]];
    [_descriptor release];
    */
    
        [self.customers sortUsingComparator:^(id obj1, id obj2)
         {
             Customer* u1 = (Customer*)obj1;
             Customer* u2 = (Customer*)obj2;
             NSString *name1=[u1 valueForKey:@"name"];
             NSString *name2=[u2 valueForKey:@"name"];
             
             return [name1 caseInsensitiveCompare:name2];
         }];
    
}

-(void)loadCustomerNames
{
    if(!customers)
    {
    
        NSMutableArray *_customers = [CoreDataHandler loadCustomersWithProperties:[NSArray arrayWithObjects:@"name",@"suburb", nil]];
        customers = _customers ? [[NSMutableArray alloc] initWithArray:_customers] : [[NSMutableArray alloc] init];
    
        [self sortCustomersByName];
    }
    
    //if(!customers)
    //[self fetechCustomersFromServer];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    /*
    SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")
    [self addTitle:@"CUSTOMERS" withFont:[UIFont subaruBoldFontOfSize:23.0] color:[UIColor blackColor]];
    #else
    [self addTitle:@"CUSTOMERS" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    #endif
     */
    
    [self addTitle:@"CUSTOMERS" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    CGRect _tableViewRect = CGRectMake(0, SEARCHBAR_HEIGHT, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-SEARCHBAR_HEIGHT);
    
    _tableView = [[UITableView tableViewWithFrame:_tableViewRect style:UITableViewStylePlain delegate:self datasource:self] retain];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleHeight| UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:_tableView];

    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    //if(!self.navigationController.navigationItem.hidesBackButton)
    if([self.navigationController.viewControllers count]>1)
    [self addBackButtonWithImage:[UIImage imageNamed:NAV_BACK_BUTTON] title:@"Back" selector:nil];
    self.tableView.backgroundColor = [UIColor whiteColor];
}
    
    
    
    
    self.tableView.bounces=NO;
   // self.tableView.contentInset=UIEdgeInsetsMake(SEARCHBAR_HEIGHT, 0.0, 0.0, 0.0);
    
     searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, SEARCHBAR_HEIGHT)];
     searchBar.tintColor =LOGIN_BG_COLOR;
     searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
     //searchBar.backgroundImage  = [UIImage imageNamed:@"calendar_search_bar.png"];
     searchBar.delegate = self;
     searchBar.placeholder = @"Search by customer name.";
     //self.tableView.tableHeaderView = searchBar;
     [self.view addSubview:searchBar];
    
    
    if(self.allowMultipleSelection)
    {
        UIBarButtonItem *doneButton =nil;
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
           UIButton *btnDone= [UIButton repToolButtonWithImageName:NAV_DONE_BUTTON text:@"Done"];
           btnDone.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
           [btnDone addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
           doneButton = [[UIBarButtonItem alloc] initWithCustomView:btnDone];
        }
        else
        {
          doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
            
        }
        
        self.navigationItem.rightBarButtonItem = doneButton;
        [doneButton release];
    
      self.navigationItem.rightBarButtonItem.enabled = [_selectedCustomers count]>0;
    }
     
    
    [self loadCustomerNames];
    
    
    //===================== TRACK INDEX FOR SELECTED CUSTOMER NAME ============================
    
    if(self.selectedCustomerName.length)
    {
        [self.customers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
        {
            Customer *_customer=(Customer *)obj;
            
            NSString *name=_customer.name;
            
            if([[name uppercaseString] isEqualToString:[self.selectedCustomerName uppercaseString]])
            {
                selectedRow=idx;
                
                *stop=YES;
            }
        }];
       //selectedRow = [self.customers indexOfObject:self.selectedCustomerName];
    }
    
    //===================== TRACK INDEX FOR SELECTED CUSTOMER NAME ============================
    
    NSLog(@"Selected Customer:%@",self.selectedCustomerName);
    
    [self.tableView reloadData];
    
    
}

-(void)setSelectedCustomerName:(NSString *)_selectedCustomerName
{
    if(selectedCustomerName !=_selectedCustomerName)
    {
        [selectedCustomerName release];
        selectedCustomerName = [_selectedCustomerName copy];
        
        NSIndexPath *lastSelectedIndexPath = (selectedRow!=NSNotFound) ?  [NSIndexPath indexPathForRow:selectedRow inSection:0] : nil;
        
        
        if(selectedCustomerName.length)
        {
            [[self currentArray] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
             {
                 Customer *_customer=(Customer *)obj;
                 
                 NSString *name=_customer.name;
                 
                 if([[name uppercaseString] isEqualToString:[self.selectedCustomerName uppercaseString]])
                 {
                     selectedRow=idx;
                     
                     *stop=YES;
                 }
             }];
        }
        
        NSMutableArray *indexPathsToReload = [NSMutableArray array];
        
        if(lastSelectedIndexPath)
        [indexPathsToReload addObject:lastSelectedIndexPath];
        
        if(selectedRow!=NSNotFound)
        [indexPathsToReload addObject:[NSIndexPath indexPathForRow:selectedRow inSection:0]];
        
//        if([indexPathsToReload count])
//        {
//          [self.tableView reloadRowsAtIndexPaths:indexPathsToReload withRowAnimation:UITableViewRowAnimationNone];
//        }

    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
     self.contentSizeForViewInPopover = CGSizeMake(335+15.0, MAX(300,self.tableView.contentSize.height)-1);
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
   // self.popoverController.popoverContentSize=CGSizeMake(320, self.tableView.contentSize.height+self.tableView.contentInset.top);
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    self.preferredContentSize = CGSizeMake(335+15.0, MAX(300,self.tableView.contentSize.height+self.tableView.contentInset.top));
else
    self.contentSizeForViewInPopover = CGSizeMake(335+15.0, MAX(300,self.tableView.contentSize.height+self.tableView.contentInset.top));

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    isSearching =NO;

    [self.tableView reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(NSArray *)currentArray
{
   return  isSearching?self.searchedArray : self.customers;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

-(void)currentSelectedOEMIndex
{
    if(![[self currentArray] count] || !self.selectedCustomerName.length) return;
    NSPredicate* custPredicate = [NSPredicate predicateWithFormat:@"self.name = %@", self.selectedCustomerName];
    NSArray* custArray = [[self currentArray] filteredArrayUsingPredicate:custPredicate];
    
    if([custArray count])
    {
        NSDictionary *custDict = [custArray objectAtIndex:0];
        NSUInteger index=  [[self currentArray] indexOfObject:custDict];

        if(index!=NSNotFound)
        {
            self.selectedCustomerName = [custDict valueForKey:@"name"];
            selectedRow = index;
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger count = [[self currentArray] count];
    
    return MAX(1,count);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arr=[self currentArray];
    NSUInteger count = [arr count];
    
    static NSString *CellIdentifier = @"Cell";
    static NSString *CellIdentifier2 = @"Cell_NoData";
    
    UITableViewCell *cell=nil;
    if(count==0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil) 
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            cell.textLabel.font = [UIFont systemFontOfSize:15.0];
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.text = @"No Customer";
        }

    }
    else
    {
        
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.font = [UIFont systemFontOfSize:15.0];
        cell.textLabel.textColor = LOGIN_BG_COLOR;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    
    Customer *_customer=(indexPath.row<[arr count]) ? [arr objectAtIndex:indexPath.row] : nil;
        
    NSString *customerName = _customer.name;
    cell.textLabel.text = [customerName uppercaseString];
    
        if(self.allowMultipleSelection)
        {
            BOOL _selected=([_selectedCustomers containsObject:_customer]);
            
            cell.accessoryType = _selected? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
        }
        else
        {
            BOOL _isMatched = ([[self.selectedCustomerName lowercaseString] isEqualToString:[_customer.name lowercaseString]]);
            
        cell.accessoryType = (_isMatched)? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
        }
    }
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    NSArray *arr = [self currentArray];
    
    if(![arr count]) return;
    
    NSMutableArray *indexPathsToSelect=[NSMutableArray array];
  
    
    Customer *_customer = (indexPath.row < [arr count]) ? [arr objectAtIndex:indexPath.row] : nil;
    NSString *customerName = _customer.name;
    
    
    if(allowMultipleSelection)
    {
        if(!_selectedCustomers) _selectedCustomers = [[NSMutableArray alloc] init];
        
        if(![_selectedCustomers containsObject:_customer])
            [_selectedCustomers addObject:_customer];
        else
        {
            [_selectedCustomers removeObject:_customer];
        }
        
        @try
        {
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        @catch (NSException *exception)
        {
            [exception print];
        }
        @finally
        {
            self.navigationItem.rightBarButtonItem.enabled = [_selectedCustomers count]>0;
        }

    }
    else if(self.selectedCustomerName)
    {
        
        if ([[self.selectedCustomerName lowercaseString] isEqualToString:[customerName lowercaseString]])
        {
            [indexPathsToSelect addObject:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
            selectedRow=NSNotFound;
            self.selectedCustomerName=nil;
            
            @try
            {
                if([indexPathsToSelect count])
                {
                  [tableView reloadRowsAtIndexPaths:indexPathsToSelect withRowAnimation:UITableViewRowAnimationNone];
                }
            }
            @catch (NSException *exception)
            {
                [exception print];
            }
            @finally
            {
                 if(self.customerSelectCallback) self.customerSelectCallback(nil);
            }
        }
        else
        {
            if(selectedRow!=NSNotFound)
            {
                [indexPathsToSelect addObject:[NSIndexPath indexPathForRow:selectedRow inSection:0]];
            }
            
            selectedRow=indexPath.row;
            self.selectedCustomerName=customerName;
            [indexPathsToSelect addObject:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
            
            @try
            {
                if([indexPathsToSelect count])
                {
                   [tableView reloadRowsAtIndexPaths:indexPathsToSelect withRowAnimation:UITableViewRowAnimationNone];
                   //[self currentSelectedOEMIndex];
                }
            }
            @catch (NSException *exception)
            {
                [exception print];
            }
            @finally
            {
                if(self.customerSelectCallback) self.customerSelectCallback(_customer);
            }
        }
    }
    else
    {
        selectedRow=indexPath.row;
        self.selectedCustomerName=customerName;
        [indexPathsToSelect addObject:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
        
        @try
        {
            if([indexPathsToSelect count])
            {
              [tableView reloadRowsAtIndexPaths:indexPathsToSelect withRowAnimation:UITableViewRowAnimationNone];
            }
        }
        @catch (NSException *exception)
        {
            [exception print];
        }
        @finally
        {
           if(self.customerSelectCallback) self.customerSelectCallback(_customer); 
        }

    }
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    isSearching=searchText.length>0;
    
   // NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"self.name contains[c] %@",searchText];
    
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"self.name beginswith[c] %@",searchText];
    
    NSArray *resArr = [self.customers filteredArrayUsingPredicate:searchPredicate];
    self.searchedArray = resArr;
    
    if(isSearching) selectedRow = NSNotFound;
    
    [self.tableView reloadData];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    selectedRow=NSNotFound;
    isSearching=NO;
}

-(void)doneAction:(id)sender
{
    if(self.didSelectCustomersCallback) self.didSelectCustomersCallback(_selectedCustomers);
}



@end
