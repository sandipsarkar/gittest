//
//  CustomerImagesViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 31/08/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//


#import <UIKit/UIKit.h>

@class Customer;
@interface CustomerImagesViewController : UIViewController<UIGestureRecognizerDelegate>
{
    UIScrollView *_pagingScrollview;
    UIButton *reuploadButton;
    BOOL _isEditMode;
}

@property (nonatomic,retain)   NSMutableArray *images;
@property (nonatomic,readonly) NSInteger selectedIndex;
@property (nonatomic,assign)   Customer *customer;
@property (nonatomic,assign)   NSManagedObjectContext *editingContext;

- (id)initWithCustomer:(Customer *)aCustomer;
-(void)reload;
-(void)selectImageAtIndex:(NSInteger)index;
-(void)enableEditMode:(BOOL)enable;
-(void)cancel;
-(void)backAction;

-(void)sendPendingCustomerPhotosWithCompletion:(void (^)(BOOL success))completion;

-(void)clear;

@end
