//
//  CustomerSummaryViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 23/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParallelStackViewController.h"
#import "ProfileInfoEditViewController.h"

@class ProfileInfoViewController,Customer,CustomerSummaryDetailController;
@class CustomerProfileViewController;

@interface CustomerSummaryViewController : UIViewController

@property (nonatomic, readonly) ProfileInfoViewController * customerProfileViewController;
@property (nonatomic, readonly) CustomerSummaryDetailController * customerSummaryDetailController;
@property (nonatomic, readonly) ProfileInfoEditViewController   * profileInfoEditViewController;
@property (nonatomic, assign)   CustomerProfileViewController   * customerProfileController;


-(id)initWithCustomer:(Customer *)aCustomer;

//-(void)enableEditMode:(BOOL)editmode;
-(void)enableEditMode:(BOOL)editmode;
-(void)addCustomerSaveCallback:(CustomerSaveCallback)callback;
-(void)addCustomerCancelCallback:(CustomerCancelCallback)callback;
-(void)setEditingContext:(NSManagedObjectContext *)aContext;

-(void)backAction:(id)sender;
@end
