//
//  CustomerPhotoView.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 06/02/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomerPhoto,SSMemoryCache;
@interface CustomerPhotoView : UIImageView

@property(nonatomic,assign) CustomerPhoto *photo;
@property(nonatomic,assign) SSMemoryCache *imageCache;

-(void)loadImageFromPhoto:(CustomerPhoto *)aPhoto;
-(void)hideCrossimage:(BOOL)hide;

//@property (nonatomic,copy) NSString *currentImageName;

-(void)addTarget:(id)target action:(SEL)selector;

-(void)unregisterObservers;

@end
