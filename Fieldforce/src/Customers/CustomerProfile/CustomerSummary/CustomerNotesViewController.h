//
//  CustomerNotesViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 27/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  Customer,KTTextView;

typedef void(^NoteActionCallback)(NSString *actionName) ;
@interface CustomerNotesViewController : UIViewController

@property(nonatomic,retain) NSString *note;
@property(nonatomic,assign) Customer *customer;

- (id)initWithCustomer:(Customer *)aCustomer;

-(void)addActionCallback:(NoteActionCallback)callback;

@end
