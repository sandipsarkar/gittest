//
//  CustomerProfileViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "ProfileInfoViewController.h"
#import "Customer.h"
#import "ViewOnMapViewController.h"
//#import "CreateNewCustomerViewController.h"
#import "SSCoreDataManager.h"
#import "CustomerNotesViewController.h"
#import "OfflineManager.h"
#import "CustomerContact.h"
#import "WholesaleDealer.h"
#import "OEM.h"
#import "Rep.h"
#import "InsetTableViewCell.h"


@interface ProfileInfoViewController()<UITextFieldDelegate>
{
    NSMutableArray *dataSource;
    NSMutableDictionary *dataDict;
    BOOL isEditMode;
    NSMutableArray *selectedOEMs;

}

-(void)setupDataSource;
-(NSString *)addressForCustomer:(Customer *)customer;
-(void)showNotePopoverAtIndexPath:(NSIndexPath *)indexPath;
-(NSString *)formatPhoneNumber:(NSString *)phoneNumber;

@property(nonatomic,retain) NSIndexPath *selectedIndexPath;
@property(nonatomic,retain) UIPopoverController *customerPopoverController;
@property(nonatomic,retain) UIPopoverController *customerNotePopoverController;
@property(nonatomic,retain) NSMutableArray *contacts;

@end

@implementation ProfileInfoViewController
@synthesize customer;
@synthesize selectedIndexPath;
@synthesize customerPopoverController;
@synthesize customerNotePopoverController;
@synthesize contacts;

- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    if (self)
    {
        
        isEditMode=NO;
        
    }
    return self;
}

-(id)initWithCustomer:(Customer *)theCustomer
{
    self=[self init];
    
    if(self)
    {
        isEditMode = (theCustomer!=nil);
        self.customer=theCustomer;
        
     
        
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidAppear:) name:UIKeyboardWillShowNotification object:nil];
        
       // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidDismiss:) name:UIKeyboardWillHideNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contactsDidLoad:) name:CustomerContactsDidLoad object:nil];

       
    }
    
    return self;
}

-(void)enableEditMode:(BOOL)_isEditMode
{
    self.view.hidden = _isEditMode;
}

-(NSString *)formatPhoneNumber:(NSString *)phoneNumber
{
    if(!phoneNumber.length) return nil;
    
    NSString *numberString = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if(numberString.length<4 || numberString.length>10)
    {
        //NSString *numberString = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        return numberString;
    }
    
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    //[formatter setNumberStyle:NSNumberFormatterNoStyle];
    //[formatter setPositiveFormat:@"###-###-####"];
    [formatter setGroupingSeparator:@" "];
    [formatter setMaximumSignificantDigits:11];
    [formatter setGroupingSize:4];
    [formatter setUsesGroupingSeparator:YES];
    [formatter setSecondaryGroupingSize:4];
    //[formatter setLenient:YES];
    
    
    NSNumber *number = [formatter numberFromString:numberString];
    NSString *resString= [formatter stringFromNumber:number];
    [formatter release];
    
    return resString;
}

/*
-(void)keyboardDidAppear:(NSNotification *)aNotification
{
    CGRect keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
    
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.tableView.contentInset=UIEdgeInsetsMake(0, 0, keyboardRect.size.height, 0);
    [UIView commitAnimations];
    self.tableView.contentInset=UIEdgeInsetsMake(0, 0, keyboardRect.size.height, 0);
    
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:1];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void)keyboardDidDismiss:(NSNotification *)aNotification
{
    //if(!_isKeyboardAppearsFormNoteSummaryView) return;
    CGRect keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
    NSTimeInterval animationDuration = [[[aNotification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.tableView.contentInset=UIEdgeInsetsMake(0, 0, -keyboardRect.size.height, 0);
    [UIView commitAnimations];
    
    //[self.tableView scrollToRowAtIndexPath:self.selectedIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}
*/

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [customer release];
    [selectedIndexPath release];
    [dataSource release];
    [contacts release];
    [selectedOEMs release];
    
    [customerPopoverController release];
    [customerNotePopoverController release];

    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - View lifecycle

-(void)loadContacts
{
    NSArray *_contacts=[self.customer.contacts allObjects];
    
    if([_contacts count])
    {
        if(!self.contacts) self.contacts = [NSMutableArray array];

        [self.contacts setArray:_contacts];
        
        NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"self.createdDate.doubleValue" ascending:YES];
        [self.contacts sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        [sortDescriptor release];
    }
    else
    {
        self.contacts=nil;
    }
    

    
}

-(void)contactsDidLoad:(NSNotification *)notif
{
    NSMutableDictionary *dict = [notif object];
    
    NSArray *deletedResult =  [dict objectForKey:@"deleted"];
    NSArray *insertedResult =  [dict objectForKey:@"inserted"];
    NSArray *updatedResult =  [dict objectForKey:@"updated"];
    
    [self loadContactsPeformingDelete:deletedResult update:updatedResult insert:insertedResult];
    
}

-(void)loadContactsPeformingDelete:(NSArray *)deleteArr update:(NSArray *)updateArr insert:(NSArray *)insertArr
{
   // BOOL isChanged = [deleteArr count]> 0 || [updateArr count]>0 || [insertArr count]>0;
    
    //if(!isChanged) return;
    
    
    if(!self.contacts) self.contacts = [NSMutableArray array];
    
    /* old logic
    if([deleteArr count])
    [self.contacts removeObjectsInArray:deleteArr];
    
    for(CustomerContact *contact in insertArr)
    {
        [self.customer addContactsObject:contact];
        [self.contacts addObject:contact];
    }
   // [self.contacts addObjectsFromArray:insertArr];
    
    //========================= CONTACT UPDATE ARRAY PENDING ==========================================
    for(CustomerContact *contact in updateArr)
    {
       NSInteger index= [self.contacts indexOfObject:contact];
        if(index!=NSNotFound)
        {
            [self.contacts replaceObjec012tAtIndex:index withObject:contact];
        }
    }
    */
    
    // ----------- CHANGED ON 4TH OCT 2013 -------------------
    
    [self.contacts setArray:self.customer.contacts.allObjects];
    
    
     NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"createdDate" ascending:YES];
    [self.contacts sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [sortDescriptor release];
    [self setupDataSource];
    [self.tableView reloadData];

}
-(void)reloadData
{
    BOOL isNewCustomer = !self.customer.name.length;
    
    if(!isNewCustomer)
    {
       __unused  NSString * categoryName = [self.customer getCategoryName];
    }
  

    /*
    if(!isNewCustomer  && (!categoryName.length && self.customer.categoryID))
    {
        if([self.customer.categoryID isEqualToNumber:[NSNumber numberWithInteger:0]])
        {
            categoryName = @"All";
        }
        else if([self.customer.categoryID isEqualToNumber:[NSNumber numberWithInteger:1]])
        {
            categoryName       = @"Mechanical";
        }
        else if([self.customer.categoryID isEqualToNumber:[NSNumber numberWithInteger:2]])
        {
            categoryName       = @"Collision";
        }
        
        self.customer.categoryName = categoryName;
    }
    */
    
    [self loadSelectedOEMs];
    [self loadContacts];
    [self setupDataSource];
    
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif

    UIView *bgView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds))];
    bgView.backgroundColor = [UIColor whiteColor];
    self.tableView.backgroundView = bgView;
    [bgView release];

    self.tableView.bounces=NO;
    //self.view.backgroundColor=[UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1];
    self.view.backgroundColor = [UIColor whiteColor];
    
   if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
   {
       self.tableView.sectionFooterHeight = 1.0;
       
       UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 10.0)];
       _headerView.backgroundColor = [UIColor clearColor];
       self.tableView.tableHeaderView = _headerView;
       [_headerView release];
       
       self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
   }
    
    //[self setupHeaderView];
    //[self setupFooterView];
    

    [self reloadData];
    
    NSLog(@"customer ID=%@",customer.customerID);
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
  //  [[SSCoreDataManager sharedManager] save:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

-(void)loadSelectedOEMs
{
    NSArray *lastSelectedOems = [self.customer.dealingOEMs componentsSeparatedByString:@","];
    if([lastSelectedOems count])
    {
        if(selectedOEMs) [selectedOEMs release];
        
        NSString *str=[lastSelectedOems objectAtIndex:0];
        if(str.length)
        {
            selectedOEMs =[[NSMutableArray alloc] init];
            
            for(NSString *oemID in lastSelectedOems)
            {
                if([oemID integerValue])
                {
                    [selectedOEMs addObject:oemID];
                }
            }
            
            //selectedOEMs =[[NSMutableArray alloc] initWithArray:lastSelectedOems];
        }
        else 
        {
            selectedOEMs =[[NSMutableArray alloc] init];
        }
    }
    else 
    {
        if(selectedOEMs) [selectedOEMs removeAllObjects];
        
        selectedOEMs =[[NSMutableArray alloc] init]; 
    }
    
}



-(void)setupDataSource
{
    if(!dataSource) 
    {
        dataSource = [[NSMutableArray alloc] init];
    }
    else 
    {
        [dataSource removeAllObjects];
    }
    
    NSMutableArray *rows=[NSMutableArray array];
    
    NSDictionary *rowInfo1 = [NSDictionary dictionaryWithObjectsAndKeys:@"Business Name",@"title",[NSIndexPath indexPathForRow:0 inSection:0],@"location",nil];
     [rows addObject:rowInfo1];
    
    /*
    if(self.customer.accountNo.length)
    {
      NSDictionary *rowInfoAccountName = [NSDictionary dictionaryWithObjectsAndKeys:@"Account Number",@"title",[NSIndexPath indexPathForRow:1 inSection:0],@"location",nil];
      [rows addObject:rowInfoAccountName];
        
        NSDictionary *rowInfoAccountStatus = [NSDictionary dictionaryWithObjectsAndKeys:@"Status",@"title",[NSIndexPath indexPathForRow:2 inSection:0],@"location",nil];
        [rows addObject:rowInfoAccountStatus];

    }
    */
    
    
    NSDictionary *rowInfo2 = [NSDictionary dictionaryWithObjectsAndKeys:@"Business Type",@"title",[NSIndexPath indexPathForRow:1 inSection:0],@"location",nil];
    [rows addObject:rowInfo2];
    
    //OPTIONAL
    
    //if([self.customer.categoryName isEqualToString:@"Mechanical"])
    if(self.customer.categoryName.length)
    {
        //if([self.customer.categoryName isEqualToString:@"Mechanical"] || [self.customer.categoryName isEqualToString:@"All"])
            
        if(self.customer.categoryID.integerValue == 0 || self.customer.categoryID.integerValue == 1)
        {
           NSDictionary *rowInfo4 = [NSDictionary dictionaryWithObjectsAndKeys:@"Workshop Bays",@"title",[NSIndexPath indexPathForRow:2 inSection:0],@"location",nil];
           NSDictionary *rowInfo5 = [NSDictionary dictionaryWithObjectsAndKeys:@"Repairs per Week(Average)",@"title",[NSIndexPath indexPathForRow:3 inSection:0],@"location",nil];
    
           //if(self.customer.workshopBays)
           [rows addObject:rowInfo4];
   
           //if(self.customer.repairsPerWeek)
            [rows addObject:rowInfo5];
        }
        else if(self.customer.categoryID.integerValue == 2)
            
            //if([self.customer.categoryName isEqualToString:@"Collision"])
        {
            NSDictionary *rowInfo5 = [NSDictionary dictionaryWithObjectsAndKeys:@"Repairs per Week(Average)",@"title",[NSIndexPath indexPathForRow:3 inSection:0],@"location",nil];
             [rows addObject:rowInfo5];
        }
    }
    
   
    /*
    if(self.customer.getGenuineMember)
    {
        NSDictionary *rowInfo3 = [NSDictionary dictionaryWithObjectsAndKeys:@"GetGenuineMember",@"title",[NSIndexPath indexPathForRow:4 inSection:0],@"location", nil];

        [rows addObject:rowInfo3];
    }
    
    if(self.customer.getGenuineMember.boolValue)
    {
        NSDictionary *rowInfoMemberShip = [NSDictionary dictionaryWithObjectsAndKeys:@"Membership Number",@"title",[NSIndexPath indexPathForRow:5 inSection:0],@"location", nil];
        [rows addObject:rowInfoMemberShip];
    }
     */
    
    NSDictionary *rowInfo3 = [NSDictionary dictionaryWithObjectsAndKeys:@"GetGenuine",@"title",[NSIndexPath indexPathForRow:4 inSection:0],@"location", nil];
    [rows addObject:rowInfo3];
    
    NSDictionary *rowInfoCapricorn = [NSDictionary dictionaryWithObjectsAndKeys:@"Trade Club",@"title",[NSIndexPath indexPathForRow:5 inSection:0],@"location", nil];
    [rows addObject:rowInfoCapricorn];
    
    NSDictionary *rowInfoMemberShip = [NSDictionary dictionaryWithObjectsAndKeys:@"Capricorn",@"title",[NSIndexPath indexPathForRow:6 inSection:0],@"location", nil];
    [rows addObject:rowInfoMemberShip];

    
    [dataSource  addObject:rows];
    
    
     rows=[NSMutableArray array];
   // if(self.customer.accountNo.length)
    {
        NSDictionary *rowInfoAccountName = [NSDictionary dictionaryWithObjectsAndKeys:@"Account Number",@"title",[NSIndexPath indexPathForRow:0 inSection:1],@"location",nil];
        [rows addObject:rowInfoAccountName];
        
        NSDictionary *rowInfoAccountStatus = [NSDictionary dictionaryWithObjectsAndKeys:@"Status",@"title",[NSIndexPath indexPathForRow:1 inSection:1],@"location",nil];
        [rows addObject:rowInfoAccountStatus];
        [dataSource  addObject:rows];
    }
    
    
    rows=[NSMutableArray array];
    
    NSDictionary *rowInfo6 = [NSDictionary dictionaryWithObjectsAndKeys:@"Address",@"title",[NSIndexPath indexPathForRow:0 inSection:2],@"location",nil];
    //NSDictionary *rowInfo7 = [NSDictionary dictionaryWithObjectsAndKeys:@"map",@"title",[NSIndexPath indexPathForRow:1 inSection:1],@"location",nil];
    [rows addObject:rowInfo6];
    //[rows addObject:rowInfo7];

    [dataSource  addObject:rows];

    
    rows=[NSMutableArray array];
    NSDictionary *rowInfo8 = [NSDictionary dictionaryWithObjectsAndKeys:@"Main",@"title",[NSIndexPath indexPathForRow:0 inSection:3],@"location",nil];
    NSDictionary *rowInfo9 = [NSDictionary dictionaryWithObjectsAndKeys:@"Email",@"title",[NSIndexPath indexPathForRow:1 inSection:3],@"location",nil];
    
    if(self.customer.mainContactNo.length)
        [rows addObject:rowInfo8];
    if(self.customer.email.length)
        [rows addObject:rowInfo9];
    
    if([rows count])
    [dataSource  addObject:rows];
    
    //=================================== CONTACT ===========================================
    rows=[NSMutableArray array];
    int _row=0;
    for(CustomerContact *contact in self.contacts)
    {
    
        NSDictionary *rowInfo = [NSDictionary dictionaryWithObjectsAndKeys:contact.name.length?contact.name:@"",@"title",[NSIndexPath indexPathForRow:_row inSection:4],@"location",nil];
        
         if(rowInfo)
         [rows addObject:rowInfo];
        
         
        _row++;
    }
    
    if([rows count])
    [dataSource addObject:rows];    
    
    //=================================== OEMS ==========================================
    
  //  NSArray *notDealingOEMList = nil;
    NSArray  *dealingOEMList   = nil;
    
    BOOL _isAnyDealingOEM=NO;
    
    if(selectedOEMs)
    {
        NSMutableArray *_oems = [NSMutableArray array];
        
        Rep *rep = [CoreDataHandler currentRep];
        
        NSArray *oems = rep.parentWdSite.parentOem.allObjects; 
        
        if([oems count])
        {
          //NSSortDescriptor *_descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
            
          NSSortDescriptor *_descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
            
          oems = [oems sortedArrayUsingDescriptors:[NSArray arrayWithObject:_descriptor]];
          [_descriptor release];
        }
        
        for(OEM *oem in oems)
        {
            NSString *oemID = [oem.oemID description];
            
            if(!_isAnyDealingOEM)
                _isAnyDealingOEM  = [selectedOEMs containsObject:oemID];
            
            
            NSDictionary *oemDict=[NSDictionary dictionaryWithObjectsAndKeys:oem.name.length?oem.name:@"",@"title",oemID,@"oemID",[NSIndexPath indexPathForRow:[_oems count] inSection:5],@"location",nil];
            [_oems addObject:oemDict];
        }

                
        dealingOEMList =_isAnyDealingOEM ?[[NSArray alloc] initWithArray:_oems]:[[NSArray alloc] init];
    }

    /*
    NSArray *_dealingOEMs = [NSArray arrayWithObjects:@"Subaru",@"Mitsubishi",@"Honda", nil];
    for(NSString *oemName in _dealingOEMs)
    {
        _isAnyDealingOEM = [selectedOEMs containsObject:oemName];
        if(_isAnyDealingOEM) break;
    }

    if(_isAnyDealingOEM)
    {
        NSDictionary *oemDict=[NSDictionary dictionaryWithObjectsAndKeys:@"Subaru",@"title",[NSIndexPath indexPathForRow:0 inSection:4],@"location",nil];
        NSDictionary *oemDict2=[NSDictionary dictionaryWithObjectsAndKeys:@"Mitsubishi",@"title",[NSIndexPath indexPathForRow:1 inSection:4],@"location",nil];
        NSDictionary *oemDict3=[NSDictionary dictionaryWithObjectsAndKeys:@"Honda",@"title",[NSIndexPath indexPathForRow:2 inSection:4],@"location",nil];
        
      dealingOEMList = [[NSArray alloc] initWithObjects:oemDict,oemDict2,oemDict3,nil];
    }
    else
    {
        dealingOEMList = [[NSArray alloc] init];
    }
    
    
     BOOL _isAnyOtherOEM=NO;
      NSArray *_notDealingOEMs = [NSArray arrayWithObjects:@"Holden",@"Hyundai",@"Suzuki", nil];
      for(NSString *oemName in _notDealingOEMs)
      {
         _isAnyOtherOEM = [selectedOEMs containsObject:oemName];
          if(_isAnyOtherOEM) break;
      }
    */
    /*
    if(_isAnyOtherOEM)
    {
        NSDictionary *oemDict4=[NSDictionary dictionaryWithObjectsAndKeys:@"Holden",@"title",[NSIndexPath indexPathForRow:0 inSection:5],@"location",nil];
        NSDictionary *oemDict5=[NSDictionary dictionaryWithObjectsAndKeys:@"Hyundai",@"title",[NSIndexPath indexPathForRow:1 inSection:5],@"location",nil];
        NSDictionary *oemDict6=[NSDictionary dictionaryWithObjectsAndKeys:@"Suzuki",@"title",[NSIndexPath indexPathForRow:2 inSection:5],@"location",nil];
        
       notDealingOEMList = [[NSArray alloc] initWithObjects:oemDict4,oemDict5,oemDict6, nil]; 
    }
    else
       notDealingOEMList = [[NSArray alloc] init];
     */
    
    rows=[NSMutableArray array];
   // NSMutableArray *rows2=[NSMutableArray array];
    
    /*
    NSDictionary *rowInfo11 = [NSDictionary dictionaryWithObjectsAndKeys:@"OEMs Dealt With Us",@"title",[NSIndexPath indexPathForRow:0 inSection:4],@"location",nil];
    
    NSDictionary *rowInfo12 = [NSDictionary dictionaryWithObjectsAndKeys:@"OEMs Dealt With Others",@"title",[NSIndexPath indexPathForRow:0 inSection:5],@"location",nil];
   */
    
    //if([selectedOEMs count])
    {
        [rows addObjectsFromArray:dealingOEMList];
        //[rows2 addObjectsFromArray:notDealingOEMList];
    }
    
    if([rows count])
    [dataSource  addObject:rows]; 
    
    //if([rows2 count])
   // [dataSource  addObject:rows2]; 
    
    [dealingOEMList release];
   // [notDealingOEMList release];
    //=================================== NOTES ===========================================

    
    NSDictionary *rowInfo30 = [NSDictionary dictionaryWithObjectsAndKeys:@"Customer Notes",@"title",[NSIndexPath indexPathForRow:0 inSection:6],@"location", nil];
    
     rows=[NSMutableArray arrayWithObject:rowInfo30];
    
    //if(self.customer.notes)
    [dataSource  addObject:rows]; 
    
    NSLog(@"Email:%@",self.customer.email);

}



-(NSString *)addressForCustomer:(Customer *)customer
{
    NSMutableArray *arr=[NSMutableArray array];
    
    NSString *add1   = self.customer.address;
    NSString *add2   = self.customer.address2;
    NSString *suburb = self.customer.suburb;
    NSString *city   = self.customer.city;
    NSString *state  = self.customer.state;
    NSString *postCode  = self.customer.postCode;
    
    if(add1.length) [arr addObject:add1];
    if(add2.length) [arr addObject:add2];
    if(city.length) [arr addObject:city];
    
    if(suburb.length) [arr addObject:suburb];
    if(state.length)  [arr addObject:state];
    if(postCode.length)  [arr addObject:postCode];
    
    NSString *addressString=[arr componentsJoinedByString:@", "];
    
    return addressString;
}

/*
-(NSString *)valueForIndexPath:(NSIndexPath *)indexPath
{
    NSString *key=@"";
    
    if(indexPath.section==0)
    {
        NSMutableArray *arr=[NSMutableArray array];
   
        NSString *add1   = self.customer.address;
        NSString *add2   = self.customer.address2;
        NSString *suburb = self.customer.suburb;
        NSString *city   = self.customer.city;
        NSString *state  = self.customer.state;
        
        if(add1.length)
        [arr addObject:add1];
        
        if(add2.length)
        [arr addObject:add2];
        
        if(city.length)
        [arr addObject:city];

        
        if(suburb.length)
        [arr addObject:suburb];
        
        if(state.length)
        [arr addObject:state];
        
        NSString *addressString=[arr componentsJoinedByString:@","];

        return addressString;
        
    }
    else 
    {
        switch (indexPath.row) 
        {
            case 0:
                key = @"categoryName";
                break;
                
            case 1:
                key=@"phone";
                break;
                
            case 2:
                key=@"tollFree";
                break;
                
            case 3:
                key=@"fax";
                break;
            case 4:
                key=@"email";
                break;
                
            default:
                break;
        }
    }
    
    return [self.customer valueForKey:key];
}
*/
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return [dataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    NSMutableArray *rows = (section<[dataSource count]) ?  [dataSource objectAtIndex:section] : nil;
    return [rows count];
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //return (indexPath.section==0)?104:tableView.rowHeight;
    
    if(indexPath.section == 2)
    {
       NSString *addressStr = [self addressForCustomer:self.customer];
       CGSize _size =  [addressStr sizeWithFont:[UIFont systemFontOfSize:12.0] constrainedToSize:CGSizeMake(200, CGFLOAT_MAX)];
        
        return  MAX(_size.height,104.0);
    }
    
    NSMutableArray *rows = (indexPath.section < [dataSource count]) ?  [dataSource objectAtIndex:indexPath.section] : nil;
    NSDictionary *rowInfo = (indexPath.row < [rows count]) ? [rows objectAtIndex:indexPath.row] : nil;
     NSIndexPath *actualIndexPath = [rowInfo valueForKey:@"location"];
    
    if(actualIndexPath.section == 4)
    {
       return  105.0;
    }
    

    return [rows count] ? tableView.rowHeight : 0.0;
}


-(NSString *)membershipLogoImageForIndexPath:(NSIndexPath *)indexPath
{
    NSString *logoImageName = nil;
    
    switch (indexPath.row)
    {
        case 4:
            logoImageName=@"genuine_logo.png";
            break;
            
        case 5:
            logoImageName=@"tradeclub_logo.png";
            break;
            
        case 6:
            logoImageName=@"capricorn_logo.png";
            break;
            
        default:
            break;
    }
    return logoImageName;
}

-(UITableViewCell *)configureCellForIndexPath:(NSIndexPath *)indexPath withRowInfo:(NSDictionary *)rowInfo
{
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    NSIndexPath *actualIndexPath = [rowInfo objectForKey:@"location"];
    
    NSString *text=[rowInfo objectForKey:@"title"];
    
    UITableViewCell *cell =nil;
    
    NSString *CellIdentifier = @"Cell";
    
    switch (actualIndexPath.section) 
    {
        case 0:
        {
            switch (actualIndexPath.row) 
            {
                case 0:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
          
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                        
                        cell.detailTextLabel.textColor = [UIColor blackColor];
                        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:12.0];
                    }
                    
                    cell.imageView.image = [UIImage imageNamed:[self membershipLogoImageForIndexPath:actualIndexPath]];
                    cell.textLabel.text = text;
                    
                    switch (actualIndexPath.row) 
                    {
                        case 0:
                        {
                            cell.detailTextLabel.text = self.customer.name;
                        }
                        break;
                        case 2:
                        {
                            cell.detailTextLabel.text = self.customer.workshopBays.length ? self.customer.workshopBays : @"N/A";
                        }
                        break;
                        case 3:
                        {
                            cell.detailTextLabel.text = self.customer.repairsPerWeek.length ? self.customer.repairsPerWeek : @"N/A";
                        }
                        break;
                        case 4:
                        {
                            cell.detailTextLabel.text = self.customer.membershipNumber.length ? self.customer.membershipNumber : @"N/A";
                           
                        }
                            break;
                        case 5:
                        {
                             cell.detailTextLabel.text = self.customer.tradeClubNumber.length ? self.customer.tradeClubNumber : @"N/A";
                            
                           
                        }
                        break;
                        case 6:
                        {
                             cell.detailTextLabel.text = self.customer.capricornNumber.length ? self.customer.capricornNumber : @"N/A";
                        }
                        break;
                            
                        default:break;
                    }
                    
                }
                break;
                      
                case 1:
                {
                    UIImageView *catLogoView=nil;
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,3];
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                        
                        catLogoView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(cell.contentView.bounds)-90.0, (CGRectGetHeight(cell.contentView.bounds)-20.0)/2.0, 20.0, 20.0)];
                        catLogoView.tag = 3;
                        [cell.contentView addSubview:catLogoView];
                        [catLogoView release];
                        
                        cell.detailTextLabel.textColor = [UIColor blackColor];
                        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:12.0];
                    }
                    else 
                    {
                        catLogoView=(UIImageView *)[cell.contentView viewWithTag:3];
                    }
                    
                    NSString *categoryName=self.customer.categoryName;
                    cell.textLabel.text = text;
                    cell.detailTextLabel.text = categoryName?categoryName:@"N/A";
                    
                    catLogoView.image = categoryName ? [UIImage imageNamed:[NSString stringWithFormat:@"%@_logo.png",[categoryName lowercaseString]]] : nil;
                }
                break;
               
                /*
                case 4:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,6];
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                         cell.imageView.image = [UIImage imageNamed:@"genuine_logo.png"];
                        
                         cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                    }
                    
                    cell.textLabel.text = text;
                    cell.accessoryType = [self.customer.getGenuineMember boolValue]? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
                }
                break;
                */
                    
                default: break;
            }
            
        }
        break;
        case 1:
        {
            switch (actualIndexPath.row) 
            {
                case 0:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                        
                        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:12.0];
                        cell.detailTextLabel.textColor = [UIColor blackColor];
                        
                    }
                    
                    cell.textLabel.text = text;
                    cell.detailTextLabel.text =(self.customer.accountNo.length) ? self.customer.accountNo : @"N/A";
                }
                break;
                    
                case 1:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                        
                        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:12.0];
                        cell.detailTextLabel.textColor = (self.customer.isAccountConfirmed.boolValue)?LOGIN_BG_COLOR:[UIColor redColor];
                        
                    }
                    
                    cell.textLabel.text = text;
                    cell.detailTextLabel.text =(self.customer.isAccountConfirmed.boolValue) ? @"Confirmed" : @"Not Confirmed";
                    
                }
              break;
            }
        }
        break;  
      
                    
        //ADDRESS
        case 2:
        {
            switch (actualIndexPath.row) 
            {
                case 0:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
       
                    UITextView *addressTextView=nil;
                    
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                    
                     
                    //cell.textLabel.numberOfLines = 3;
                    //cell.textLabel.text =[self addressForCustomer:customer];
                    
                    
                    UILabel *_addressLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, 65, 20)];
                    _addressLabel.font=[UIFont boldSystemFontOfSize:15];
                    _addressLabel.backgroundColor=[UIColor clearColor];
                    _addressLabel.text=@"Address";
                    _addressLabel.textColor=[UIColor blackColor];
                    _addressLabel.font= [UIFont grotesqueBoldFontOfSize:14.0];
                    [cell.contentView addSubview:_addressLabel];
                    [_addressLabel release];
                    
                    
                    
                    addressTextView=[[UITextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_addressLabel.frame)+5.0, 10.0, 220 , 43)];
                    addressTextView.tag = 1;
                    addressTextView.textColor=[UIColor blackColor];
                    addressTextView.backgroundColor = [UIColor clearColor];
                    addressTextView.textAlignment   = NSTextAlignmentLeft;
                    addressTextView.font = [UIFont grotesqueFontOfSize:14.0];
                        addressTextView.contentInset = UIEdgeInsetsMake(-5.0, 0, 0, 0);
                    [cell.contentView addSubview:addressTextView];
                    [addressTextView release];
                    
                    UIButton *getDirections=[UIButton buttonWithType:UIButtonTypeCustom];
                    getDirections.frame=CGRectMake(0, CGRectGetMaxY(addressTextView.frame)+5, 164, 46);
                    [getDirections setBackgroundImage:[UIImage imageNamed:@"get_directions.png"] forState:UIControlStateNormal];
                    [cell.contentView addSubview:getDirections];
                    
                    [getDirections addTarget:self action:@selector(getDirectionsAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    UIButton *viewOnMapButton=[UIButton buttonWithType:UIButtonTypeCustom];
                    viewOnMapButton.frame=CGRectMake(CGRectGetMaxX(getDirections.frame)+1, CGRectGetMaxY(addressTextView.frame)+5, 166, 46);
                    [viewOnMapButton setBackgroundImage:[UIImage imageNamed:@"view_on_map.png"] forState:UIControlStateNormal];
                    [cell.contentView addSubview:viewOnMapButton];
                    [viewOnMapButton addTarget:self action:@selector(viewOnMapAction:) forControlEvents:UIControlEventTouchUpInside];

                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                }
                else
                {
                    addressTextView = (UITextView *)[cell.contentView viewWithTag:1];
                }
                    
                    addressTextView.editable = NO;
                   // nameTextView.editable  = NO;
                    
                    addressTextView.text=[self addressForCustomer:self.customer];

                }
                break;
                    
                default: break;
            }
        }
        break;
            
            //TEXT
        case 3:
        {
            switch (actualIndexPath.row) 
            {
                case 0:
                case 1:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];               
                    
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];

                        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:12.0];
                        cell.detailTextLabel.textColor = [UIColor blackColor];
                        
                    }
                    
                    cell.textLabel.text = text;
                    
                  
                    NSString *mainContact = self.customer.mainContactNo;
                    
                    /*
                    if(actualIndexPath.row==0)
                    {
                       if(mainContact.length)
                       {
                         mainContact = [self formatPhoneNumber:mainContact];
                       }
                    }
                    */
                    
                    cell.detailTextLabel.text = (actualIndexPath.row==0) ? mainContact : self.customer.email;
                }
                break;
            }
            
        }
        break;
            
        case 5:
        {
            CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,4];
            cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                
                cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
            }
            
            /*
            NSArray *arr= (actualIndexPath.section == 24) ? dealingOEMList : notDealingOEMList;
            NSDictionary *infoDict = (actualIndexPath.row<[arr count]) ? [arr objectAtIndex:actualIndexPath.row] : nil;;
            NSString *title  = [infoDict objectForKey:@"title"];
            cell.textLabel.text =title;
            */
            
            NSLog(@"Title:%@",text);
            
            NSString *oemID = [rowInfo objectForKey:@"oemID"];
            
             //BOOL isSelected = ([selectedOEMs containsObject:text]);
            
             BOOL isSelected = ([selectedOEMs containsObject:oemID]);
            
            cell.accessoryType = isSelected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;

            cell.textLabel.text =text;
        }
            break; 
            
            //============================== NOTES ================================
        case 6:
        {
             CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,6];
             cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
             if (cell == nil)
             {
             cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
             UIImageView *documentIcon=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"document_icon.png"]];
             cell.accessoryView =documentIcon;
             [documentIcon release];
                 
                 cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
             }
             
             
             BOOL _isNotesAvailable=self.customer.notes.length;
             cell.accessoryView.hidden= !_isNotesAvailable;
             cell.textLabel.text =text;
        }
        break;  
            
        case 4:
        {
            UILabel *nameLabel   = nil;
            UILabel *titleLabel  = nil;
            UILabel *directLabel = nil;
            UILabel *mobileLabel = nil;
            UILabel *emailLabel  = nil;
            
            CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
            cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            
            if (cell == nil)
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                
                float locY=5.0;
                float locX=5.0;
                float _titleWidth = 50.0;
    
                
                UILabel *_nameLabel = [UILabel labelWithText:@"Name" frame:CGRectMake(5.0, locY, _titleWidth, 15.0) textColor:[UIColor blackColor] bgColor:[UIColor clearColor] fontsize:12.0];
                _nameLabel.font=[UIFont grotesqueBoldFontOfSize:12.0];
                _nameLabel.textAlignment = NSTextAlignmentRight;
                [cell.contentView addSubview:_nameLabel];
                _nameLabel.autoresizingMask = UIViewAutoresizingNone;
                
                locX=CGRectGetMaxX(_nameLabel.frame);
                
                nameLabel = [UILabel labelWithText:@"" frame:CGRectMake(locX+15.0, locY, CGRectGetWidth(cell.contentView.bounds)-_titleWidth-10.0, 14.0) textColor:[UIColor blackColor] bgColor:[UIColor clearColor] fontsize:12.0];
                nameLabel.tag = 1;
                nameLabel.font=[UIFont grotesqueFontOfSize:12.0];
                nameLabel.textAlignment = NSTextAlignmentLeft;
                [cell.contentView addSubview:nameLabel];
                nameLabel.autoresizingMask = UIViewAutoresizingNone;
                
                
                locY=CGRectGetMaxY(nameLabel.frame)+5.0;
               
                
                UILabel *_titleLabel= [UILabel labelWithText:@"Title" frame:CGRectMake(5.0, locY, _titleWidth, 15.0) textColor:[UIColor blackColor] bgColor:[UIColor clearColor] fontsize:12.0];
                _titleLabel.font=[UIFont grotesqueBoldFontOfSize:12.0];
                _titleLabel.textAlignment = NSTextAlignmentRight;
                [cell.contentView addSubview:_titleLabel];
                _titleLabel.autoresizingMask = UIViewAutoresizingNone;
                
                 locX=CGRectGetMaxX(_titleLabel.frame);
                
                titleLabel = [UILabel labelWithText:@"" frame:CGRectMake(locX+15.0, locY, CGRectGetWidth(cell.contentView.bounds)-_titleWidth-10.0, 14.0) textColor:[UIColor blackColor] bgColor:[UIColor clearColor] fontsize:14.0];
                titleLabel.tag=2;
                titleLabel.font = [UIFont grotesqueFontOfSize:12.0];
                titleLabel.textAlignment = NSTextAlignmentLeft;
                [cell.contentView addSubview:titleLabel];
                titleLabel.autoresizingMask = UIViewAutoresizingNone;
                
                
                locY=CGRectGetMaxY(titleLabel.frame)+5.0;
                
                UILabel *_directLabel= [UILabel labelWithText:@"Direct" frame:CGRectMake(5.0, locY, _titleWidth, 15.0) textColor:[UIColor blackColor] bgColor:[UIColor clearColor] fontsize:12.0];
                _directLabel.font=[UIFont grotesqueBoldFontOfSize:12.0];
                _directLabel.textAlignment = NSTextAlignmentRight;
                [cell.contentView addSubview:_directLabel];
                _directLabel.autoresizingMask = UIViewAutoresizingNone;
                
                locX=CGRectGetMaxX(_directLabel.frame);
                
                directLabel = [UILabel labelWithText:@"" frame:CGRectMake(locX+15.0, locY, CGRectGetWidth(cell.contentView.bounds)-_titleWidth-10.0, 14.0) textColor:[UIColor blackColor] bgColor:[UIColor clearColor] fontsize:12.0];
                directLabel.tag = 3;
                directLabel.font = [UIFont grotesqueFontOfSize:12.0];
                directLabel.textAlignment = NSTextAlignmentLeft;
                [cell.contentView addSubview:directLabel];
                directLabel.autoresizingMask = UIViewAutoresizingNone;
                
                
                locY=CGRectGetMaxY(directLabel.frame)+5.0;
                
                UILabel *_mobileLabel= [UILabel labelWithText:@"Mobile" frame:CGRectMake(5.0, locY, _titleWidth, 15.0) textColor:[UIColor blackColor] bgColor:[UIColor clearColor] fontsize:12.0];
                _mobileLabel.font=[UIFont grotesqueBoldFontOfSize:12.0];
                [cell.contentView addSubview:_mobileLabel];
                _mobileLabel.textAlignment = NSTextAlignmentRight;
                _mobileLabel.autoresizingMask = UIViewAutoresizingNone;
                
                locX = CGRectGetMaxX(_directLabel.frame);
                
                mobileLabel = [UILabel labelWithText:@"" frame:CGRectMake(locX+15.0, locY, CGRectGetWidth(cell.contentView.bounds)-_titleWidth-10.0, 14.0) textColor:[UIColor blackColor] bgColor:[UIColor clearColor] fontsize:12.0];
                mobileLabel.tag = 4;
                mobileLabel.font = [UIFont grotesqueFontOfSize:12.0];
                mobileLabel.textAlignment = NSTextAlignmentLeft;
                [cell.contentView addSubview:mobileLabel];
                mobileLabel.autoresizingMask = UIViewAutoresizingNone;
                
                
                locY = CGRectGetMaxY(mobileLabel.frame)+5.0;
                
                UILabel *_emailLabel= [UILabel labelWithText:@"Email" frame:CGRectMake(5.0, locY, _titleWidth, 15.0) textColor:[UIColor blackColor] bgColor:[UIColor clearColor] fontsize:12.0];
                _emailLabel.font=[UIFont grotesqueBoldFontOfSize:12.0];
                [cell.contentView addSubview:_emailLabel];
                _emailLabel.textAlignment = NSTextAlignmentRight;
                _emailLabel.autoresizingMask = UIViewAutoresizingNone;
                
                locX = CGRectGetMaxX(_directLabel.frame);
                
                emailLabel = [UILabel labelWithText:@"" frame:CGRectMake(locX+15.0, locY, CGRectGetWidth(cell.contentView.bounds)-_titleWidth-10.0, 14.0) textColor:[UIColor blackColor] bgColor:[UIColor clearColor] fontsize:12.0];
                emailLabel.tag  = 5;
                emailLabel.font = [UIFont grotesqueFontOfSize:12.0];
                emailLabel.textAlignment   = NSTextAlignmentLeft;
                [cell.contentView addSubview:emailLabel];
                emailLabel.autoresizingMask = UIViewAutoresizingNone;

            }
            else
            {
                nameLabel   = (UILabel *)[cell.contentView viewWithTag:1];
                titleLabel  = (UILabel *)[cell.contentView viewWithTag:2];
                directLabel = (UILabel *)[cell.contentView viewWithTag:3];
                mobileLabel = (UILabel *)[cell.contentView viewWithTag:4];
                emailLabel  = (UILabel *)[cell.contentView viewWithTag:5];
            }

            CustomerContact *contact = (indexPath.row <[self.contacts count]) ? [self.contacts objectAtIndex:indexPath.row] : nil;
            
               NSString *contactNo = contact.mobile;
                //if(contactNo.length)contactNo = [self formatPhoneNumber:contactNo];
                
               NSString *directNo = contact.direct;
               //if(directNo.length)directNo = [self formatPhoneNumber:directNo];
               
            nameLabel.text   = contact.name.length   ? contact.name   : @"n/a";
            titleLabel.text  = contact.title.length  ? contact.title  : @"n/a";
            directLabel.text = directNo.length       ? directNo       : @"n/a";
            mobileLabel.text = contactNo.length      ? contactNo      : @"n/a";
            emailLabel.text  = contact.email.length  ? contact.email  : @"n/a";
            
            nameLabel.textColor = contact.name.length ? [UIColor blackColor] : [UIColor lightGrayColor];
            titleLabel.textColor = contact.title.length ? [UIColor blackColor] : [UIColor lightGrayColor];
            directLabel.textColor = contact.direct.length ? [UIColor blackColor] : [UIColor lightGrayColor];
            mobileLabel.textColor = contact.mobile.length ? [UIColor blackColor] : [UIColor lightGrayColor];
            emailLabel.textColor = contact.email.length ? [UIColor blackColor] : [UIColor lightGrayColor];
        }
        break;
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor =  [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
    
    return cell;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *rows = (indexPath.section < [dataSource count]) ?  [dataSource objectAtIndex:indexPath.section] : nil;
    
    NSDictionary *rowInfo = (indexPath.row < [rows count]) ? [rows objectAtIndex:indexPath.row] : nil;
    
    UITableViewCell *cell = [self configureCellForIndexPath:indexPath withRowInfo:rowInfo];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSMutableArray *rows = (section < [dataSource count]) ?  [dataSource objectAtIndex:section] : nil;
    NSDictionary *rowInfo = ([rows count]) ? [rows objectAtIndex:0] : nil;
    NSIndexPath *actualIndexPath = [rowInfo valueForKey:@"location"];
    
    if(actualIndexPath.section == 4 || actualIndexPath.section == 5 || actualIndexPath.section == 5) return 40.0;
    
    return 0.0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *title=nil;
    NSMutableArray *rows = (section < [dataSource count]) ?  [dataSource objectAtIndex:section] : nil;
    NSDictionary *rowInfo = ([rows count]) ? [rows objectAtIndex:0] : nil;
    NSIndexPath *actualIndexPath = [rowInfo valueForKey:@"location"];
    
    if(actualIndexPath.section == 4 )      title      = @"    CONTACTS";
    else if(actualIndexPath.section == 5)  title      = @"    PURCHASES"; //@"    OEMs Dealt With Us";
    //else if(actualIndexPath.section == 5)  title      = @"    OEMs Dealt With Others";
       
    UILabel *label=nil;
    if(title)
    {
        label=[[UILabel alloc] initWithFrame:CGRectMake(2.0, 0, CGRectGetWidth(tableView.bounds)-2.0, 40.0)];
        label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont subaruBoldFontOfSize:23.0];
        label.textColor = [UIColor blackColor];
        label.shadowOffset = CGSizeMake(1, 1);
        label.shadowColor = [UIColor lightGrayColor];
        label.text = title;
    }
    
    return [label autorelease];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];;
    _bgView.tag = 1;
    
    
    NSUInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
}

-(void)viewOnMapAction:(id)sender
{
#ifdef __IPHONE_5_0 
    ViewOnMapViewController *viewOnMapController = [[ViewOnMapViewController alloc]initWithCutomerInfo:self.customer];

    UINavigationController *_navController = [self nextResponderNavigationController];
    if(_navController)
     [_navController pushViewController:viewOnMapController animated:NO];
    
    [viewOnMapController release];
#endif
    
    
}

-(void)getDirectionsAction:(id)sender
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:GET_DIRECTIONS_MSG delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
    [alert release];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0)
    {
        ;
    }
    else
    {
          [self launchDefaultMapApplication];
    }
}
-(void)launchDefaultMapApplication
{
	double selectedLat=[self.customer.latitude  doubleValue];
	double selectedLong=[self.customer.longitude doubleValue];
	
    CLLocationCoordinate2D destination;
	destination.latitude  = selectedLat;
	destination.longitude = selectedLong;
    
#ifdef __IPHONE_6_0
    
    
    Class mapItemClass = NSClassFromString(@"MKMapItem");//;[MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        NSMutableDictionary *placemarkDict=[[NSMutableDictionary alloc]init];
        if(self.customer.name.length)
            [placemarkDict setValue:self.customer.name forKey:@"title"];
        if(self.customer.address.length)
            [placemarkDict setValue:self.customer.address forKey:@"Street"];
        if(self.customer.suburb.length)
            [placemarkDict setValue:self.customer.suburb forKey:@"SubLocality"];
        if(self.customer.state.length)
            [placemarkDict setValue:self.customer.state forKey:@"State"];
        if(self.customer.postCode.length)
            [placemarkDict setValue:self.customer.postCode forKey:@"ZIP"];
        [placemarkDict setValue:@"Australia" forKey:@"Country"];
        [placemarkDict setValue:@"AU" forKey:@"CountryCode"];
        
        NSLog(@" placemarkDict %@",placemarkDict);
        
        
        MKPlacemark *placemark = [[MKPlacemark alloc]
                                  initWithCoordinate:destination  addressDictionary:placemarkDict];
        
        // Create a map item for the geocoded address to pass to Maps app
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:[placemarkDict valueForKey:@"title"]];
        
        // Set the directions mode to "Driving"
      // NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
        
        NSDictionary* launchOptions = [[NSDictionary alloc] initWithObjectsAndKeys:
                                   MKLaunchOptionsDirectionsModeDriving,
                                       MKLaunchOptionsDirectionsModeKey,
                                   nil];
        
      
    
        // Set the direction mode in the launchOptions dictionary
        [MKMapItem openMapsWithItems:[NSArray arrayWithObject:mapItem] launchOptions:launchOptions];
        [launchOptions release];
        [placemarkDict release];
        [mapItem release];
        [placemark release];

    }
    else 
#endif
    {
        
        //NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%lf,%lf&daddr=%lf,%lf",
        //								 start.latitude, start.longitude, destination.latitude, destination.longitude];
        
        NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=Current+Location&daddr=%lf,%lf",
                                         destination.latitude, destination.longitude];
        // Current Location
        //NSLog(@"googleMapsURLString = %@", googleMapsURLString);
        
        
        NSURL *url = [NSURL URLWithString:[googleMapsURLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        if(url!=nil)
        {
            [[UIApplication sharedApplication] openURL:url];
        }
    }
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UITableViewCell *cell=(UITableViewCell *)textField.superview;
    NSIndexPath *indexPath=[self.tableView indexPathForCell:cell];
    self.selectedIndexPath=indexPath;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.selectedIndexPath=nil;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"Row:%d Section:%d",self.selectedIndexPath.row,self.selectedIndexPath.section);

  __unused  NSString *text=textField.text;
    if(text.length)
     text=[text stringByReplacingCharactersInRange:range withString:string];
    
   // NSString *key=[self keyForIndexPath:self.selectedIndexPath];
   // [dataDict setValue:text forKey:key];
    //[self.profileDict setValue:text forKey:key];
    
    return YES;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *rows = (indexPath.section < [dataSource count]) ?  [dataSource objectAtIndex:indexPath.section] : nil;
    NSDictionary *rowInfo = ([rows count]) ? [rows objectAtIndex:0] : nil;
    NSIndexPath *actualIndexPath = [rowInfo valueForKey:@"location"];

    if(actualIndexPath.section==3)
    {
       
        [self showEmailClient];
    }
    if(actualIndexPath.section==6)
    {
        NSLog(@"Customer Notes");
        [self showNotePopoverAtIndexPath:indexPath];
    }
}
-(void)showEmailClient
{
     Class mailclass = NSClassFromString(@"MFMailComposeViewController");
    
    if(mailclass!=nil && [mailclass canSendMail])
    {
    
    NSLog(@"%@",self.customer.email);
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate=self;
    picker.navigationBar.tintColor=[UIColor blackColor];
	[picker setSubject:@""];
	[picker setMessageBody:@"" isHTML:NO];
	[picker setToRecipients:[NSArray arrayWithObjects:self.customer.email, nil]];
        
    //NSString *pdfPath =[AppToolBox getDoccumentPath:[NSString stringWithFormat:@"%@.pdf",@"Report"]];
    //NSString *sFileName=[NSString stringWithFormat:@"%@.pdf",@"Report"];
    //NSData *pdfData = [NSData dataWithContentsOfFile:pdfPath];
    //[picker addAttachmentData:pdfData mimeType:@"application/pdf" fileName:sFileName];
        
     [self presentModalViewController:picker animated:YES];
    [picker release];
        
    }
    else
    {
        [UIAlertView showWarningAlertWithTitle:@"" message:NO_EMAIL_CLIENT_MESSAGE];

    }
	
}
- (void)mailComposeController:(MFMailComposeViewController*)controller
		  didFinishWithResult:(MFMailComposeResult)result
						error:(NSError*)error 
{
	if (result == MFMailComposeResultSent) 
    {
		NSString *success = [[NSString alloc] initWithString:@"Success"];
		UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Email Send"
                                                         message:success
                                                        delegate:self
                                               cancelButtonTitle:nil
                                               otherButtonTitles:@"Ok",nil];
		[alert1 show];
        [alert1 release],alert1=nil;
        [success release];
		
	}
	
	[self dismissModalViewControllerAnimated:YES];
}

-(void)showNotePopoverAtIndexPath:(NSIndexPath *)indexPath
{
    CustomerNotesViewController *customerNotesViewController=[[CustomerNotesViewController alloc]initWithCustomer:self.customer];
    
    UINavigationController *navController= [[UINavigationController alloc] initWithRootViewController:customerNotesViewController];
    customerNotePopoverController = [[UIPopoverController alloc]initWithContentViewController:navController];
    customerNotesViewController.popoverController = customerNotePopoverController;
    customerNotePopoverController.popoverContentSize = CGSizeMake(320.0f, 200.0f);
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    customerNotePopoverController.popoverBackgroundViewClass = [MBPopoverBackgroundView class];

    
    CGRect popoverRect = [self.tableView rectForRowAtIndexPath:indexPath];
    
    [customerNotePopoverController presentPopoverFromRect:popoverRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    
    __block ProfileInfoViewController *weakSelf = self;
    [customerNotesViewController addActionCallback:^(NSString *actionName) 
    {
        NSLog(@"Action Name %@",actionName);
        
        if([actionName isEqualToString:@"SAVE"])
        {
            if(indexPath)
            [weakSelf.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            
          __block  ASIHTTPRequest *_request = [ConnectionManager saveCustomerNotes:weakSelf.customer.notes forCustomerID:weakSelf.customer.customerID];
            [_request setCompletionBlock:^{
                
                NSLog(@"Response=%@",[_request responseJSON]);
            }];
        }
    }];
    
    
    [customerNotesViewController release];
    [navController release];
}

@end
