//
//  CustomerSummaryDetailController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 28/08/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ViewOnMapViewController,CustomerImagesViewController;
@class Customer;
@interface CustomerSummaryDetailController : UIViewController
{
    ViewOnMapViewController *viewOnMapController;
    CustomerImagesViewController *customerImagesViewController;
}
@property(nonatomic,assign)Customer *customer;
@property(nonatomic,assign) NSManagedObjectContext *editingContext;

- (id)initWithCustomerInfo:(Customer *)aCustomer;

-(void)enableEditMode:(BOOL)editMode;

-(void)reload;
-(void)cancel;
-(void)backAction;


@end
