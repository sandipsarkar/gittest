//
//  CustomerNotesViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 27/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomerNotesViewController.h"
#import "Customer.h"
#import "KTTextView.h"

#define PLACEHOLDER_TEXT @"Add note"

@interface CustomerNotesViewController ()<UITextViewDelegate>
{
    KTTextView *_textView;
}

@property(nonatomic,retain) UIBarButtonItem *currentBarButtonItem;
@property (nonatomic,copy) NoteActionCallback noteActionCallback;

@end

@implementation CustomerNotesViewController
@synthesize note;
@synthesize currentBarButtonItem;
@synthesize customer;
@synthesize noteActionCallback;


- (id)initWithCustomer:(Customer *)aCustomer
{
    self = [super init];
    if (self) 
    {
        self.customer=aCustomer;
        self.note = aCustomer.notes;
    }
    return self;
}

-(void)dealloc
{
    [note release];
    [_textView release];
    [currentBarButtonItem release];
 
    customer = nil;
    
    if(noteActionCallback)
    [noteActionCallback release];
    
    [super dealloc];
}

-(void)setupTextView
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
     _textView = [[KTTextView alloc] initWithFrame:CGRectInset(self.view.bounds, 5.0, 5.0)];
    _textView.contentInset = UIEdgeInsetsMake(0, 0, 10.0, 0);
    }
    else
    {
     _textView = [[KTTextView alloc] initWithFrame:self.view.bounds];
   }
    
    _textView.backgroundColor = [UIColor whiteColor];
    _textView.font = [UIFont grotesqueFontOfSize:14.0];
    _textView.placeholderText = PLACEHOLDER_TEXT;
    _textView.delegate = self;
    _textView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:_textView];
    
    _textView.layer.cornerRadius = 5.0;
    _textView.layer.borderWidth = 5.0 ;
    _textView.layer.borderColor = [UIColor clearColor].CGColor;
    
    _textView.editable=!self.note.length>0;
    _textView.text = self.note;
}

-(void)addActionCallback:(NoteActionCallback)callback
{
    if(callback)
        self.noteActionCallback = callback;
}

-(void)editAction:(id)sender
{
    BOOL isEditable = _textView.editable;
    _textView.editable = !isEditable;
    
    UIBarButtonItem *editButton = self.navigationItem.rightBarButtonItem;
    self.navigationItem.rightBarButtonItem = self.currentBarButtonItem;
    self.currentBarButtonItem = editButton;
    
    //EDIT
    if(_textView.isEditable) 
    {
        self.modalInPopover=YES;
        [_textView becomeFirstResponder];
    }
    
    ///DONE
    if(!_textView.isEditable)
    {
        self.note = [_textView.text trimmedString];
        self.customer.notes=self.note;
        
         NSError *error = nil;
        [[SSCoreDataManager sharedManager] save:error];
        
        self.modalInPopover=NO;
        [self.popoverController dismissPopoverAnimated:YES];
        if(self.noteActionCallback) self.noteActionCallback(@"SAVE");
    }
}

-(void)cancelAction:(id)sender
{
    [self.popoverController dismissPopoverAnimated:YES];
    
    if(self.noteActionCallback) self.noteActionCallback(@"CANCEL");
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    UIBarButtonItem *rightBarButton = nil;
    UIBarButtonItem *doneBarButton = nil;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    UIButton *btnCancel = [UIButton repToolButtonWithImageName:NAV_DONE_BUTTON text:@"Cancel"];
    btnCancel.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
    [btnCancel addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelBarButton=[[UIBarButtonItem alloc]initWithCustomView:btnCancel];
     self.navigationItem.leftBarButtonItem = cancelBarButton;
     [cancelBarButton release];
    
    UIButton *btnRightDone = [UIButton repToolButtonWithImageName:NAV_DONE_BUTTON text:@"Edit"];
    btnRightDone.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
    [btnRightDone addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
   rightBarButton=[[UIBarButtonItem alloc]initWithCustomView:btnRightDone];
    
    
    UIButton *btnDone = [UIButton repToolButtonWithImageName:@"email_button_blank.png" text:@"Done"];
    btnDone.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
    [btnDone addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
   doneBarButton=[[UIBarButtonItem alloc]initWithCustomView:btnDone];


    self.view.backgroundColor = [UIColor whiteColor];
    }
    else
    {
    
    UIBarButtonItem *cancelBarButton  = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction:)];
    self.navigationItem.leftBarButtonItem = cancelBarButton;
    [cancelBarButton release];
    
   rightBarButton  = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editAction:)];
   
   doneBarButton  = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(editAction:)];
   
    }
     
   if(self.note.length)
   {
     self.navigationItem.rightBarButtonItem =  rightBarButton;
     self.currentBarButtonItem              =  doneBarButton;
   }
   else 
   {
       self.navigationItem.rightBarButtonItem =  doneBarButton;
       self.currentBarButtonItem              =  rightBarButton;
   }

    
     [rightBarButton release];
     [doneBarButton release];
	
    [self setupTextView];
}



-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
   if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
   {
    //self.contentSizeForViewInPopover = CGSizeMake(320, 250.0);
     self.preferredContentSize=CGSizeMake(330, 254.0);
   }
  else
  {
      self.contentSizeForViewInPopover = CGSizeMake(320, MAX(200.0,_textView.contentSize.height));
      self.popoverController.popoverContentSize = self.contentSizeForViewInPopover;
    
   }

    
   // [self performSelector:@selector(makeFirstResponder) withObject:nil afterDelay:0.3];
}

-(void)makeFirstResponder
{
    if(_textView.isEditable && !self.note.length)
    {
        [_textView becomeFirstResponder];
    }
}

-(BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if(text.length)
    {
        NSRange _range = [text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:ESCAPE_CHARSET]];
        BOOL valid = (_range.location ==NSNotFound);
        if(!valid) return NO;
    }

    return YES;
}

/*
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    self.modalInPopover=YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    self.modalInPopover=NO;
}
 */
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
