//
//  CustomerPhotoView.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 06/02/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//
#define UPLOAD_FAILED_ICON @"warning.png" // @"Crystal_button_cancel.png"

#import "CustomerPhotoView.h"
#import "CustomerPhoto.h"
#import "UIImage+Resize.h"
#import "UIImage+UIImage_Orientation.h"
#import "UIImageView+LazyLoading.h"
#import "LazyImageLoader.h"
#import "SSMemoryCache.h"

#define IMAGE_COMPRESSED_SIZE CGSizeMake(332.0, 249.0)

@interface CustomerPhotoView()<ImageLoaderDelegate>
{
    UIImageView *crossImageView;
    UIActivityIndicatorView *_activityIndicator;
}



@end

@implementation CustomerPhotoView

@synthesize photo;
@synthesize imageCache;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) 
    {
         UIImage *crossImage = [UIImage imageNamed:UPLOAD_FAILED_ICON];
        
        crossImageView = [[UIImageView alloc] initWithImage:crossImage];
        crossImageView.frame = CGRectMake(CGRectGetWidth(frame)-crossImage.size.width-2.0, 2.0, crossImage.size.width, crossImage.size.height);
        //crossImageView.frame = CGRectMake(2.0, 2.0, crossImage.size.width, crossImage.size.height);
        crossImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:crossImageView];
       
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicator.hidesWhenStopped = YES;
        [self addSubview:_activityIndicator];
        
        _activityIndicator.frame = CGRectMake((CGRectGetWidth(frame)-CGRectGetWidth(_activityIndicator.frame))/2.0, (CGRectGetHeight(frame)-CGRectGetHeight(_activityIndicator.frame))/2.0, CGRectGetWidth(_activityIndicator.frame), CGRectGetHeight(_activityIndicator.frame));
        
        _activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDidLoad:) name:ImageLoaderDidLoadImageNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDidFail:) name:ImageLoaderDidFailImageNotification object:nil];
       
        
        UITapGestureRecognizer* tapGesture  =   [[UITapGestureRecognizer alloc] initWithTarget:nil action:nil];
        [(UITapGestureRecognizer *)tapGesture setNumberOfTouchesRequired:1];
        //tapGesture.delegate=self;
        [self addGestureRecognizer:tapGesture];
        [tapGesture release];

    }
    return self;
}

-(void)addTarget:(id)target action:(SEL)selector
{
   for(UITapGestureRecognizer *tapGesture in self.gestureRecognizers)
   {
       if([tapGesture isKindOfClass:[UITapGestureRecognizer class]])
       {
           if(target && selector)
           [tapGesture addTarget:target action:selector];
           break;
       }
   }
}

-(void)loadImageFromPhoto:(CustomerPhoto *)aPhoto
 {
     if(self.image && (self.photo && [self.photo.imageName isEqualToString:aPhoto.imageName]))
     {
           [_activityIndicator stopAnimating];
         
         return;
     }
     
     self.photo = aPhoto;
     crossImageView.hidden = aPhoto.isUploaded.boolValue;
     
     UIImage *image = self.imageCache ?  [self.imageCache objectForKey:self.photo.imageName] : nil;
     
     if(!image)
     {
        NSString *imagePath = photo.imageName ? [DirectoryManager customerImagePathForName:photo.imageName] : nil;
         
         BOOL isFileExists = (imagePath && [[NSFileManager defaultManager] fileExistsAtPath:imagePath]);
        image = isFileExists ? [UIImage imageWithContentsOfFile:imagePath] : nil;
         
         /*
         if(isFileExists)
         {
             NSData *imageData = [[NSData alloc] initWithContentsOfFile:imagePath options:NSDataReadingMappedIfSafe error:nil];

             image  = [UIImage imageWithData:imageData];
             [imageData release];
         }
         else
         {
             image = nil;
         }
         */
         
         if(image)
         {
             image = [UIImage imageWithImage:image scaledToMaxWidth:IMAGE_COMPRESSED_SIZE.width maxHeight:IMAGE_COMPRESSED_SIZE.height];
             
             // image = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:IMAGE_COMPRESSED_SIZE interpolationQuality:kCGInterpolationDefault];
             
             NSLog(@"%f %f",image.size.width,image.size.height);
             
             if(self.imageCache)
             [self.imageCache setObject:image forKey:self.photo.imageName];
         }
     }
     
     if(image)
     {
         self.image = image;
         [_activityIndicator stopAnimating];

     }
     else
     {
          [_activityIndicator startAnimating];
         
         [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(stopAnimating) object:nil];
         
         [self performSelector:@selector(stopAnimating) withObject:nil afterDelay:35.0];
         
         if(self.photo.isDownloading) return;
         
         NSString *_imageName = photo.imageName? photo.imageName : nil;
         
         if(_imageName)
         {
             NSString *imageUrl = [CUSTOMER_IMAGE_DOWNLOAD_LINK stringByAppendingString:_imageName];
             NSLog(@"imageUrl=%@",imageUrl);
             
             NSString *imagePath=[DirectoryManager customerImagePathForName:_imageName];
             
             self.photo.isDownloading = YES;
             //[LazyImageLoader loadImageForUrlString:imageUrl indexPath:nil delegate:nil];
              [LazyImageLoader loadImageForUrlString:imageUrl indexPath:nil delegate:nil targetPath:imagePath];
         }
     }     
     
 }

-(void)hideCrossimage:(BOOL)hide
{
     crossImageView.hidden = hide;
}

-(void)stopAnimating
{ 
    @try
    {
        [_activityIndicator stopAnimating];
        self.photo.isDownloading = NO;
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}


-(void)imageDidLoad:(NSNotification *)notif
{
    LazyImageLoader *loader = notif.object;
    
    UIImage *_dloadedImage = loader.image;
    
    if(!_dloadedImage) return;
    
    NSString *imageName = [loader.urlString lastPathComponent];
    
    NSString *imagePath=[DirectoryManager customerImagePathForName:imageName];
    
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:imagePath])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
        NSData *imageData=UIImageJPEGRepresentation(_dloadedImage, 0.9);

       // NSError *error =nil;
       // BOOL written=imagePath?[imageData  writeToFile:imagePath options:NSDataWritingFileProtectionNone error:&error]:NO;
         BOOL written=imagePath?[imageData  writeToFile:imagePath atomically:YES]:NO;
        NSLog(@"written=%d",written);
            
        });
    }
    
    
    if(![imageName isEqualToString:self.photo.imageName])
    {
        
        return;
    }
    
    // _dloadedImage = [_dloadedImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:IMAGE_COMPRESSED_SIZE interpolationQuality:kCGInterpolationDefault];
    
    _dloadedImage = [UIImage imageWithImage:_dloadedImage scaledToMaxWidth:IMAGE_COMPRESSED_SIZE.width maxHeight:IMAGE_COMPRESSED_SIZE.height];
    
    NSLog(@"%f %f",_dloadedImage.size.width,_dloadedImage.size.height);
    self.image=_dloadedImage;
    
    if(self.imageCache)
    [self.imageCache setObject:_dloadedImage forKey:self.photo.imageName];
    
    [_activityIndicator stopAnimating];
    
    self.photo.isDownloading = NO;
}

-(void)imageDidFail:(NSNotification *)notif
{
    LazyImageLoader *loader = notif.object;
    
     if(![[loader.urlString lastPathComponent] isEqualToString:self.photo.imageName]) return;
    
     [_activityIndicator stopAnimating];
    
    self.photo.isDownloading = NO;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)unregisterObservers
{
    photo.isDownloading = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(stopAnimating) object:nil];
   
}

-(void)dealloc
{
    //[NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(stopAnimating) object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ImageLoaderDidLoadImageNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ImageLoaderDidFailImageNotification object:nil];
     
    photo = nil;
    imageCache = nil;
    
    [crossImageView release];
    [_activityIndicator release];
    _activityIndicator=nil;
    
    [super dealloc];
}
@end
