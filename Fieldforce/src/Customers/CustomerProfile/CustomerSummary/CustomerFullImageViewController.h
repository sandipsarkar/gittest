//
//  CusromerFullImageViewController.h
//  RepVisitationTool
//
//  Created by Subhojit Dey on 12/17/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerFullImageViewController : UIViewController
{
    UIImage *customerImage;
    int currentIndex;
    NSMutableArray *arrImages;
    UIView *fullImageContainerView;
    UIImageView *customerimgView;
    UISwipeGestureRecognizer *oneFingerSwipeRight;
    UISwipeGestureRecognizer *oneFingerSwipeLeft;
    UIActivityIndicatorView *_activityIndicator;
    UIPageControl* pageControl;
}

@property(nonatomic,assign) BOOL isVisitPhoto;
//-(id)initWithCustomerPhotos:(UIImage *)image;
-(id)initWithCustomerPhotos:(NSMutableArray *)arr selectedImageTag:(int)selectedTag;

-(id)initWithCustomerVisitPhotos:(NSMutableArray *)arr selectedIndex:(int)selectedTag;

@end
