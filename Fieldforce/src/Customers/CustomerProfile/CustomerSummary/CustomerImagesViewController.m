//
//  CustomerImagesViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 31/08/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomerImagesViewController.h"
#import "CustomerFullImageViewController.h"
#import "TakePhotoViewController.h"
#import "UIImage+Resize.h"
#import "UIImage+UIImage_Orientation.h"
#import "CustomerPhoto.h"
#import "Customer.h"
#import "CoreDataHandler.h"
#import "WholesaleDealer.h" 
#import "UIImageView+LazyLoading.h"
#import "Rep.h" 
#import "OfflineManager.h"  
#import "CustomerPhotoView.h"
#import "SSMemoryCache.h"

#define IMAGE_COMPRESSED_SIZE CGSizeMake(332.0, 249.0)
#define IMAGE_INTERPOLATION 0.5
#define MAX_IMAGE_COUNT 5

//#define IMAGE_DOWNLOAD_LINK @"http://192.168.1.143/AutoProfiler/Resource/Visit/"
//#define IMAGE_DOWNLOAD_LINK @"http://autoprofiler.randemgroup.com.au/Resource/Visit/"

#define NO_IMAGES_IMAGE         @"blank_image_field.png"
#define DELETE_IMAGE            @"minus_button.png"
#define GALLERY_IMAGE           @"gallery.png"
#define UPLOAD_FAILED_ICON      @"button-cross.png"
#define REUPLOAD_BUTTON_IMAGE   @"mid_segment.png"


const CGFloat kScrollObjWidth	= 332.0; //340.0
const CGFloat kScrollObjHeight	= 249.0;

@interface CustomerImagesViewController ()<UIScrollViewDelegate>
{
  NSInteger totalImageCount;
  UIView *headerView;
  UIPageControl *pageControl;
  NSMutableDictionary *cacheDict;
  UIButton *deleteButton;
  UIImageView *noImagesImageView;
    
    BOOL isImageUploadCancelled;
    
  SSMemoryCache  *_imageCache;
    
    NSUInteger lastSelectedIndex;

}
@property(nonatomic,retain) ASIHTTPRequest *currentImageRequest;
@property (nonatomic,retain) NSMutableArray *uploadingPhotos;
@property(nonatomic,retain) ASIHTTPRequest *currentGetImageRequest;

-(NSInteger)numberOfImagesInScroller;

-(void)layoutScrollImages;

-(UIImage *)imageAtIndex:(NSInteger)index;
-(void)selectImageAtIndex:(NSInteger)index animated:(BOOL)animated;
-(void)saveImage:(UIImage *)image;

//-(void)sendCustomerPhotos:(NSString *)imagePath completion:(void (^)(BOOL success))completion;

-(void)sendCustomerPhotos:(CustomerPhoto *)photo completion:(void (^)(BOOL success))completion;

-(void)manageReUploadButton;

@end

@implementation CustomerImagesViewController
@synthesize customer;
@synthesize images;
@synthesize selectedIndex;
@synthesize editingContext;
@synthesize currentImageRequest;
@synthesize currentGetImageRequest;
@synthesize uploadingPhotos;

- (id)initWithCustomer:(Customer *)aCustomer
{
    self = [super init];
    
    if (self)
    {
        customer  = aCustomer;
        cacheDict = [[NSMutableDictionary alloc] init];
        images    = [[NSMutableArray alloc] init];

        selectedIndex = 0;
        _isEditMode   = NO;
        lastSelectedIndex = NSNotFound;
        
        //app.arrCustomerPhoto=[[NSMutableArray alloc]init];
        
        [LazyImageLoader shouldSaveToDisk:NO];
        [LazyImageLoader downloadDirectlyToFile:YES];
    }
    return self;
}

-(void)enableEditMode:(BOOL)enable
{
    _isEditMode=enable;
    
    deleteButton.hidden = ![self.images count] ||  !enable ;
}

-(SSMemoryCache *)imageCache
{
    if(!_imageCache)
    {
        _imageCache = [[SSMemoryCache alloc] init];
        _imageCache.cacheCount = 5;
    }
    
    return _imageCache;
}
-(void)cancelImageLoading
{
    //====================================
    [LazyImageLoader cancelAllConnections];
    //====================================
    
    /*
    for(CustomerPhotoView * photoView in [_pagingScrollview subviews])
    {
        if(photoView && [photoView isKindOfClass:[CustomerPhotoView class]])
        {
            [photoView unregisterObservers];
        }
    }
     */
    
    
    for(CustomerPhoto *photo in self.images)
    {
        photo.isDownloading = NO;
    }
    
}
-(void)backAction
{
   //  [LazyImageLoader cancelAllConnections];
}

-(void)dealloc
{
     [self clear];
     [super dealloc];
    
}

-(void)clear
{
    editingContext = nil;
    customer = nil;
    
    [self cancelImageLoading];
        
    [cacheDict release];
    cacheDict=nil;
    
    [_imageCache release];
    _imageCache = nil;
    
    //[_pagingScrollview removeFromSuperview];
    [_pagingScrollview release];
    [images release];
    [headerView release];
    [noImagesImageView release];
    
    currentGetImageRequest.delegate = nil;
    [currentGetImageRequest clearDelegatesAndCancel];
    [currentGetImageRequest release];
    currentGetImageRequest = nil;
    
    
    currentImageRequest.delegate = nil;
    [currentImageRequest clearDelegatesAndCancel];
    [currentImageRequest release];
    currentImageRequest = nil;
    
    [uploadingPhotos release];
    
    _pagingScrollview= nil;
    images = nil;
    headerView=nil;
    noImagesImageView = nil;
    uploadingPhotos = nil;
}

-(void)captureAction:(UIButton *)sender
{
    if([self.images count] >= MAX_IMAGE_COUNT)
    {
        [UIAlertView showWarningAlertWithTitle:@"" message: [NSString stringWithFormat:CUSTOMER_GALLERY_MAX_IMAGE_MESSAGE,MAX_IMAGE_COUNT]];
        return;
    }
    
    TakePhotoViewController *takePhotoVc=[[TakePhotoViewController alloc] initWithPhotos:nil];
    takePhotoVc.allowCaptionEntry=NO;
    takePhotoVc.isLaunchedFromCustomerSection = YES;
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:takePhotoVc];
      
    [app.splitViewController presentViewController:navController animated:YES completion:^{}];
    
    [takePhotoVc addApproveActionCallback:^(UIImage *image) 
    {
        [takePhotoVc dismissViewControllerAnimated:YES completion:^{}];
        
         [self saveImage:image];
    }];
    
    [takePhotoVc release];
    [navController release];
    
}

-(void)reUploadAction:(UIButton *)sender
{
    /*
    Customer *_parentCustomer = self.editingContext ? (Customer *) [self.customer inContext:self.editingContext] : self.customer;
    if(![_parentCustomer.isSentToServer boolValue]) 
    {
        //SHOW PROPER ALERT HERE
        
        [UIAlertView showWarningAlertWithTitle:@"" message:@"Images failed to upload.\nCustomer is not synched with server.\n"];
        return;
    }
    */
        
    [self sendPendingCustomerPhotosWithCompletion:^(BOOL success) 
    {
        if(!success)
        {
            
        }
        else
        {
            [self sortCustomerImages];
            [self reload];
        }
    }];
}

/*-(void)deleteAction:(UIButton *)sender
{
    CustomerPhoto *photo = selectedIndex<[self.images count ] ? [self.images objectAtIndex:selectedIndex] : nil;
    if(!photo) return;
    
    NSLog(@"%@",photo.imagePath);
    ASIHTTPRequest *request =  [ConnectionManager deleteImagesForCustomerID:self.customer.customerID  imageName:photo.imagePath];
    
    [request setCompletionBlock:^{
        
        NSDictionary * jsonResponse = request.responseJSON;
        
        NSLog(@"delete RESPONSE:%@",jsonResponse);
        
        [self.customer removePhotosObject:photo];
        if(self.editingContext)
        {
            [SSCoreDataManager deleteObject:photo inContext:self.editingContext];
        }
        else
        {
            [SSCoreDataManager deleteObject:photo];
            // [DEFAULT_CONTEXT processPendingChanges];
        }
        
        [self.images removeObjectAtIndex:selectedIndex];
        //[app.arrCustomerPhoto removeObjectAtIndex:selectedIndex];
        //if(app.arrCustomerPhoto.count > selectedIndex)
        // [app.arrCustomerPhoto removeObjectAtIndex:selectedIndex];
        
        [self reload];
        [self selectImageAtIndex:MAX(0,self.images.count-1)];
        
        
        deleteButton.hidden =![self.images count] || !_isEditMode ;
        
    }];
    [request setFailedBlock:^{
        
        
        
    }];
    

    /*
     //================= REMOVE IMAGE FROM DIRECTORY ===============================
     NSString *photoName=photo.imagePath;
     NSString *imagePath=[DirectoryManager customerImagePathForName:photoName];
     
     if([[NSFileManager defaultManager] fileExistsAtPath:imagePath])
     {
     [[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
     }
     //================= REMOVE IMAGE FROM DIRECTORY ===============================
    
}*/

-(void)deleteAction:(UIButton *)sender
{
    sender.userInteractionEnabled = NO;
    [sender superview].userInteractionEnabled = NO;
    
     CustomerPhoto *photo = (selectedIndex < [self.images count ] && selectedIndex>=0) ? [self.images objectAtIndex:selectedIndex] : nil;
    if(!photo)
    {
        sender.userInteractionEnabled = YES;
        [sender superview].userInteractionEnabled = YES;
        return;
    }
    
    ProgressHUD *hud =  [ProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Deleting..";
    
    BOOL _isNewCustomer = !self.customer.name.length;
    if(!photo.isUploaded.boolValue || _isNewCustomer)
    {
        if(photo!=nil && ![photo isFault])
        {
            [self.customer removePhotosObject:photo];
            if(self.editingContext)
            {
                [SSCoreDataManager deleteObject:photo inContext:self.editingContext];
            }
            else
            {
                [SSCoreDataManager deleteObject:photo];
                //[DEFAULT_CONTEXT processPendingChanges];
            }
            
           // [self.images removeObject:photo];
            [self.images removeObjectAtIndex:selectedIndex];
            
            for(CustomerPhotoView *subview in [_pagingScrollview subviews])
            {
                if(subview && [subview superview])
                {
                    if([subview.photo.customerImageID isEqualToNumber:photo.customerImageID])
                    {
                        [subview unregisterObservers];
                    }
                    
                   [subview removeFromSuperview];
                }
            }
            
            [self sortCustomerImages];
            [self reload];
            [self selectImageAtIndex:MAX(0,self.images.count-1)];
            
            deleteButton.hidden =![self.images count] || !_isEditMode ;
        }

        sender.userInteractionEnabled             = YES;
        [sender superview].userInteractionEnabled = YES;
        [ProgressHUD hideHUDForView:self.view animated:YES];
        return;
    }
    
    if([[OfflineManager sharedManager] isNetworkAvailable])
    {
        NSLog(@"%@",photo.imageName);
        
        Rep *currentRep = [CoreDataHandler currentRep];
        
        NSNumber *wdSiteID = self.customer.wdSiteID ?self.customer.wdSiteID : currentRep.parentWdSite.wdSiteID;
        
      __block ASIHTTPRequest *request = [ConnectionManager deleteImagesForCustomerID:self.customer.customerID siteID:wdSiteID photo:photo repID:currentRep.repID];
        
        self.currentImageRequest = request;
        
      __block CustomerImagesViewController *weakSelf = self;
        
        [request setCompletionBlock:^{
            
            weakSelf.currentImageRequest = nil;
            
            sender.userInteractionEnabled             = YES;
            [sender superview].userInteractionEnabled = YES;
            
            NSLog(@"delete RESPONSE:%@",request.responseString);
            
            NSDictionary *jsonDict = [request responseJSON];
            NSLog(@"feedback jsonDict:%@",jsonDict);
            
            jsonDict = NULL_NIL(jsonDict);
            
            NSNumber *isUnderMaintenance  = [jsonDict objectForKey:JSON_UNDER_MAINTENANCE_KEY];
            
            isUnderMaintenance = NULL_NIL(isUnderMaintenance);
            
            if([isUnderMaintenance boolValue])
            {
                [app showUnderMaintainenceScreen:YES];
            }
            else
            {
            
            if(jsonDict && [[jsonDict valueForKey:@"status"] intValue]==1)
            {
                if(photo!=nil && ![photo isFault])
                {
                  [weakSelf.customer removePhotosObject:photo];
                    
                if(weakSelf.editingContext)
                {
                    [SSCoreDataManager deleteObject:photo inContext:weakSelf.editingContext];
                }
                else
                {
                    [SSCoreDataManager deleteObject:photo];
                    // [DEFAULT_CONTEXT processPendingChanges];
                }
                
                [weakSelf.images removeObject:photo];
               // [self.images removeObjectAtIndex:selectedIndex];
            
                
                for(CustomerPhotoView *subview in [weakSelf->_pagingScrollview subviews])
                {
                    if(subview && [subview superview])
                    {
                        if([subview.photo.customerImageID isEqualToNumber:photo.customerImageID])
                        {
                            [subview unregisterObservers];
                        }
                        
                        [subview removeFromSuperview];
                    }

                }
                
                [weakSelf sortCustomerImages];
                [weakSelf reload];
                [weakSelf selectImageAtIndex:MAX(0,weakSelf.images.count-1)];
                 weakSelf->deleteButton.hidden =![weakSelf.images count] || !weakSelf->_isEditMode ;
                    
                }
            }
            else
            {
                sender.userInteractionEnabled = YES;
                [sender superview].userInteractionEnabled = YES;
                
                if(![isUnderMaintenance boolValue])
                [UIAlertView showWarningAlertWithTitle:@"" message:POOR_CONNECTION_MSG_IMAGE_DELETION_FAILED_MSG];
            }
                
            }
            
            [ProgressHUD hideHUDForView:weakSelf.view animated:YES];
            
        }];
        
        [request setFailedBlock:^{
            
            weakSelf.currentImageRequest = nil;
            
              [ProgressHUD hideHUDForView:weakSelf.view animated:YES];
            sender.userInteractionEnabled             = YES;
            [sender superview].userInteractionEnabled = YES;
            
            [UIAlertView showWarningAlertWithTitle:@"" message:POOR_CONNECTION_MSG_IMAGE_DELETION_FAILED_MSG];
            
        }];
        
        /*
         //================= REMOVE IMAGE FROM DIRECTORY ===============================
         NSString *photoName=photo.imagePath;
         NSString *imagePath=[DirectoryManager customerImagePathForName:photoName];
         
         if([[NSFileManager defaultManager] fileExistsAtPath:imagePath])
         {
         [[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
         }
         //================= REMOVE IMAGE FROM DIRECTORY ===============================
         */
        
        
    }
    else
    {
          [ProgressHUD hideHUDForView:self.view animated:YES];
        
        sender.userInteractionEnabled             = YES;
        [sender superview].userInteractionEnabled = YES;
        
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NETWORK_NOT_AVAILABLE_MSG delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [alert release],alert=nil;
        
    }
}



-(void)setupHeaderView
{
    headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 30.0)];
    headerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:headerView];
     
    /*
    UIImage *galleryImage = [UIImage imageNamed:GALLERY_IMAGE];
    UIImageView *galleryImageView = [[UIImageView alloc] initWithImage:                     galleryImage];
    galleryImageView.frame = CGRectMake(20.0, 1.0, galleryImage.size.width, galleryImage.size.height);
    galleryImageView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    galleryImageView.backgroundColor = [UIColor clearColor];
 //   [headerView addSubview:galleryImageView];
    [galleryImageView release];
    */
    
    UILabel *galleryLabel = [UILabel labelWithText:@"GALLERY" frame: CGRectMake(20.0, 1.0, 84.0, 18.0) textColor:[UIColor blackColor] bgColor:[UIColor clearColor] fontsize:0];
    galleryLabel.font = [UIFont subaruBoldFontOfSize:22.0];
     galleryLabel.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [headerView addSubview:galleryLabel];
    galleryLabel.shadowColor = [UIColor colorWithWhite:0.1 alpha:0.4];
    galleryLabel.shadowOffset = CGSizeMake(1.0, 2.0);
    
    
    UIImage *cameraImage=[UIImage imageNamedNoCache:@"camera_button.png"];
    
    UIImage *reuploadButtonImage = [UIImage imageNamed:REUPLOAD_BUTTON_IMAGE];
    
    reuploadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    reuploadButton.frame = CGRectMake(140.0, (CGRectGetHeight(headerView.bounds)-reuploadButtonImage.size.height)/2.0, reuploadButtonImage.size.width, reuploadButtonImage.size.height);
    [reuploadButton setTitle:@"Resend" forState:UIControlStateNormal];
    reuploadButton.titleLabel.font = [UIFont subaruBoldFontOfSize:16.0];
    //reuploadButton.titleLabel.shadowColor = [UIColor lightGrayColor];
    
    if(reuploadButtonImage)
        [reuploadButton setBackgroundImage:reuploadButtonImage forState:UIControlStateNormal];
    
    [headerView addSubview:reuploadButton];
    reuploadButton.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
    [reuploadButton addTarget:self action:@selector(reUploadAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
   // reuploadButton.layer.cornerRadius = 12.0;
    
    /*
    reuploadButton.layer.masksToBounds = YES;
    reuploadButton.layer.borderWidth = 2.0;
    reuploadButton.layer.borderColor = LOGIN_BG_COLOR.CGColor;
    reuploadButton.backgroundColor = LOGIN_BG_COLOR;
    */
  
    UIButton *cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cameraButton.frame = CGRectMake(290.0, (CGRectGetHeight(headerView.bounds)-cameraImage.size.height)/2.0, cameraImage.size.width, cameraImage.size.height);
    [cameraButton setBackgroundImage:cameraImage forState:UIControlStateNormal];
    [headerView addSubview:cameraButton];
    cameraButton.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
    [cameraButton addTarget:self action:@selector(captureAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.backgroundColor = [UIColor clearColor];
    self.view.autoresizesSubviews = YES;
    self.view.clipsToBounds = YES;
	// Do any additional setup after loading the view.
    
    [self setupHeaderView];
    
    CGFloat locY=CGRectGetMaxY(headerView.frame)+5.0;
    
    UIImage *noImagesImage = [UIImage imageNamedNoCache:NO_IMAGES_IMAGE];
    CGRect _pagingScrollviewFrame = CGRectMake(10.0, locY, noImagesImage.size.width, noImagesImage.size.height);
    
    if(!_pagingScrollview)
    {
    _pagingScrollview=[[UIScrollView alloc] initWithFrame:_pagingScrollviewFrame];
    _pagingScrollview.autoresizesSubviews =YES;
       _pagingScrollview.showsHorizontalScrollIndicator = NO;
    _pagingScrollview.showsVerticalScrollIndicator = NO;
    _pagingScrollview.bounces = NO;
    _pagingScrollview.backgroundColor = [UIColor clearColor];
    _pagingScrollview.pagingEnabled = YES;
    _pagingScrollview.delegate =self;
    [self.view addSubview:_pagingScrollview];
    }
    
    //_pagingScrollview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin ;
    _pagingScrollview.layer.cornerRadius = 6.0;
    _pagingScrollview.layer.masksToBounds = YES;
    
    noImagesImageView=[[UIImageView alloc] initWithFrame:_pagingScrollviewFrame];
    noImagesImageView.backgroundColor = [UIColor clearColor];
    //noImagesImageView.autoresizingMask = _pagingScrollview.autoresizingMask;
    noImagesImageView.image = noImagesImage;
    [self.view addSubview:noImagesImageView];
    noImagesImageView.layer.cornerRadius = 6.0;
    noImagesImageView.layer.masksToBounds = YES;
    
    
    UIImage *deleteImage = [UIImage imageNamed:DELETE_IMAGE];
    deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteButton.frame = CGRectMake(10.0, locY+2.0, deleteImage.size.width, deleteImage.size.height);
    [deleteButton setBackgroundImage:deleteImage forState:UIControlStateNormal];
    [self.view addSubview:deleteButton];
    [deleteButton addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
    deleteButton.hidden = !_isEditMode;
    
    /*
    pageControl = [UIPageControl pageControlWithFrame:CGRectMake((CGRectGetWidth(self.view.bounds)-100.0)/2.0, CGRectGetHeight(self.view.bounds)-25.0, 100, 20) numberOfPages:[self.images count] target:nil action:@selector(pageControlAction:)];
    [self.view addSubview:pageControl];
    pageControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
    */
    
    
   
    pageControl = [UIPageControl pageControlWithFrame:CGRectMake((CGRectGetWidth(self.view.bounds)-100.0)/2.0, CGRectGetMaxY(_pagingScrollviewFrame)-16.0, 100, 20) numberOfPages:[self.images count] target:nil action:@selector(pageControlAction:)];
    [self.view addSubview:pageControl];
    pageControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin ;
    
    
    pageControl.userInteractionEnabled=NO;
    
   // NSMutableArray *arrVisitphoto=[CustomerPhoto fetchAll:nil];
    
   // NSLog(@"%@",arrVisitphoto);
    
    NSLog(@" cust id %@",self.customer.customerID);
    
    
// [images setArray:self.customer.photos.allObjects];
    [images setArray:self.customer.photos.allObjects];
    
    NSString *customerName = self.customer.name;
    BOOL  _isNewCustomer=!customerName.length;
    
    //if(![images count])
    
    if(!_isNewCustomer)
    [self getCustomerImages];
    
    NSLog(@"images count:%d",[images count]);
        

    [self sortCustomerImages];
    [self layoutScrollImages];
    [self selectImageAtIndex:0 animated:NO];
    
   // NSArray *pendingPhotos = [self.images filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.isUploaded=%@",[NSNumber numberWithBool:NO]]];
   // reuploadButton.hidden = ![pendingPhotos count];
    
    [self manageReUploadButton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [LazyImageLoader shouldSaveToDisk:NO];
}

-(void)manageReUploadButton
{
     NSArray *pendingPhotos = [self.images filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.isUploaded=%@",[NSNumber numberWithBool:NO]]];
    
     reuploadButton.hidden = ![pendingPhotos count];
}

-(void)sortCustomerImages
{
    if(![self.images count]) return;
    
   // NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate" ascending:YES];
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"customerImageID" ascending:YES];
    [self.images sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    [descriptor release];
    
}


-(void)getCustomerImages
{
    _pagingScrollview.userInteractionEnabled=NO;
    self.view.userInteractionEnabled = NO;
    
    Rep *rep=[CoreDataHandler currentRep];
    
    NSLog(@"%@",rep.parentWdSite.wdSiteID);

    
    [ProgressHUD showHUDAddedTo:self.view animated:YES];
    //hud.mode = MBProgressHUDModeText;
    
    ASIHTTPRequest *request =  [ConnectionManager getImagesForCustomer:self.customer  repId:rep.repID wdSiteID:rep.parentWdSite.wdSiteID];
    request.delegate = self;
    self.currentGetImageRequest = request;
    
    /*
    [request setCompletionBlock:^{
        
        [ProgressHUD hideHUDForView:self.view animated:YES];
        //[_activityIndicator stopAnimating];
        
        _pagingScrollview.userInteractionEnabled=YES;
        self.view.userInteractionEnabled = YES;
        
        NSDictionary * jsonResponse = request.responseJSON;
        NSLog(@" photo RESPONSE:%@",request.responseString);
        
        NSNumber *lastRequestedDate = [jsonResponse objectForKey:JSON_LAST_REQUESTED_KEY];
        lastRequestedDate = NULL_NIL(lastRequestedDate);
        self.customer.photoLastRequestedFromServer = lastRequestedDate;
        
        NSNumber *number=[jsonResponse objectForKey:@"hasImage"];
        number = NULL_NIL(number);
        
        
        NSArray *arrPhoto   = [jsonResponse valueForKey:JSON_DETAILS_KEY];
        NSArray *updatedIDs = [jsonResponse valueForKey:JSON_UPDATED_KEY];
        NSArray *deletedIDs = [jsonResponse valueForKey:JSON_DELETED_KEY];
        
        arrPhoto   = NULL_NIL(arrPhoto);
        updatedIDs = NULL_NIL(updatedIDs);
        deletedIDs = NULL_NIL(deletedIDs);
        
        //if(number && [number intValue]>0)
        //if(1)
        {
            [CustomerPhoto populateAsyncPerformingDelete:deletedIDs?deletedIDs:[NSArray array] update:updatedIDs?updatedIDs:[NSArray array] insertOnArray:arrPhoto ?arrPhoto:[NSArray array] onAttribute:@"customerImageID" completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
            {
                 Customer *_parentCustomer = self.editingContext ? (Customer *) [self.customer inContext:self.editingContext] : self.customer;
                
                
                for(CustomerPhoto *photo in deletedResult)
                {
                    if(photo)
                    [_parentCustomer removePhotosObject:photo];
                }
                
                //================================================================
                
               for(CustomerPhoto *photo in insertedResult)
               {
                   NSLog(@"Photo imageName=%@",photo.imageName);
                   NSLog(@"Photo imageID=%@",photo.customerImageID);
                   
                    photo.isUploaded = [NSNumber numberWithBool:YES];
                   
                   BOOL _exists = NO;
                   for(CustomerPhoto *aPhoto in self.images)
                   {
                      
                       NSLog(@"OLD Photo imageName=%@",aPhoto.imageName);
                       NSLog(@"OLD Photo imageID=%@",aPhoto.customerImageID);
                       
                       BOOL _isPhotoExists =([aPhoto.customerImageID isEqualToNumber:photo.customerImageID] || [aPhoto.imageName isEqualToString:photo.imageName]);
                       
                       if(_isPhotoExists)
                       {
                           _exists = YES;
                           break;
                       }
                   }
                   
                   //if(![self.images containsObject:photo])
                   if(!_exists)
                   {
                    
                     [_parentCustomer addPhotosObject:photo];
                    // [self.images addObject:photo];
                   }
                   
                   else
                   {
                       [_parentCustomer removePhotosObject:photo];
                       
                       if(self.editingContext)
                       {
                           [SSCoreDataManager deleteObject:photo inContext:self.editingContext];
                       }
                       else
                       {
                          [SSCoreDataManager deleteObject:photo];
                           
                       }
                       
                   }
                   
               }
                
                [images setArray:self.customer.photos.allObjects];
                
                [self sortCustomerImages];
                [self reload];
            }];
        }
        // [request setCompletionBlock:nil];
    }];
    [request setFailedBlock:^{
        
       
        
        _pagingScrollview.userInteractionEnabled = YES;
        self.view.userInteractionEnabled = YES;

        if(![self.images count])
        {
            hud.mode = MBProgressHUDModeText;
            hud.labelText = POOR_CONN_MSG_UNABLE_TO_RETRIEVE_IMAGES;
        }
        
        [self performSelector:@selector(hideHud) withObject:nil afterDelay:3.0];
         //[request setCompletionBlock:nil];
    }];
    */
}

-(void)hideHud
{
    [ProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    [ProgressHUD hideHUDForView:self.view animated:YES];
    //[_activityIndicator stopAnimating];
    
    _pagingScrollview.userInteractionEnabled=YES;
    self.view.userInteractionEnabled = YES;
    
    NSDictionary * jsonResponse = request.responseJSON;
    NSLog(@" photo RESPONSE:%@",request.responseString);
    
    NSNumber *lastRequestedDate = [jsonResponse objectForKey:JSON_LAST_REQUESTED_KEY];
    lastRequestedDate = NULL_NIL(lastRequestedDate);
    self.customer.photoLastRequestedFromServer = lastRequestedDate;
    
    NSNumber *number = [jsonResponse objectForKey:@"hasImage"];
    number = NULL_NIL(number);
    
    
    NSArray *arrPhoto   = [jsonResponse valueForKey:JSON_DETAILS_KEY];
    NSArray *updatedIDs = [jsonResponse valueForKey:JSON_UPDATED_KEY];
    NSArray *deletedIDs = [jsonResponse valueForKey:JSON_DELETED_KEY];
    
    arrPhoto   = NULL_NIL(arrPhoto);
    updatedIDs = NULL_NIL(updatedIDs);
    deletedIDs = NULL_NIL(deletedIDs);
    
    /*
    [CustomerPhoto populateAsyncPerformingDelete:deletedIDs?deletedIDs:[NSArray array] update:updatedIDs?updatedIDs:[NSArray array] insertOnArray:arrPhoto ?arrPhoto:[NSArray array] onAttribute:@"customerImageID" completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
         {
             Customer *_parentCustomer = self.editingContext ? (Customer *) [self.customer inContext:self.editingContext] : self.customer;
             
             
             for(CustomerPhoto *photo in deletedResult)
             {
                 if(photo)
                     [_parentCustomer removePhotosObject:photo];
             }
             
             //================================================================
             
             for(CustomerPhoto *photo in insertedResult)
             {
                 NSLog(@"Photo imageName=%@",photo.imageName);
                 NSLog(@"Photo imageID=%@",photo.customerImageID);
                 
                 photo.isUploaded = [NSNumber numberWithBool:YES];
                 
                 BOOL _exists = NO;
                 for(CustomerPhoto *aPhoto in self.images)
                 {
                     
                     NSLog(@"OLD Photo imageName=%@",aPhoto.imageName);
                     NSLog(@"OLD Photo imageID=%@",aPhoto.customerImageID);
                     
                     BOOL _isPhotoExists =([aPhoto.customerImageID isEqualToNumber:photo.customerImageID] || [aPhoto.imageName isEqualToString:photo.imageName]);
                     
                     if(_isPhotoExists)
                     {
                         _exists = YES;
                         break;
                     }
                 }
                 
                 //if(![self.images containsObject:photo])
                 if(!_exists)
                 {
                     
                     [_parentCustomer addPhotosObject:photo];
                     // [self.images addObject:photo];
                 }
                 
                 else
                 {
                     [_parentCustomer removePhotosObject:photo];
                     
                     if(self.editingContext)
                     {
                         [SSCoreDataManager deleteObject:photo inContext:self.editingContext];
                     }
                     else
                     {
                         [SSCoreDataManager deleteObject:photo];
                         
                     }
                 }
             }
             
             if(self.editingContext)
             {
                [[SSCoreDataManager sharedManager] saveContext:self.editingContext error:nil];
             }
             else
             {
                 [[SSCoreDataManager sharedManager] save:nil];
                 
             }
             
             [images setArray:self.customer.photos.allObjects];
             
             [self sortCustomerImages];
             [self reload];
         }];
    */
    
    Customer *_parentCustomer = self.editingContext ? (Customer *) [self.customer inContext:self.editingContext] : self.customer;
    
    [CustomerPhoto populateAsyncPerformingDelete:deletedIDs insertOnArray:arrPhoto onAttributes:[NSArray arrayWithObject:@"customerImageID"] completion:^(NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error) {
        
        for(CustomerPhoto *photo in insertedResult)
        {
            NSLog(@"Photo imageName=%@",photo.imageName);
            NSLog(@"Photo imageID=%@",photo.customerImageID);
            
            photo.isUploaded = [NSNumber numberWithBool:YES];
            
            BOOL _exists = NO;
            for(CustomerPhoto *aPhoto in self.images)
            {
                
                NSLog(@"OLD Photo imageName=%@",aPhoto.imageName);
                NSLog(@"OLD Photo imageID=%@",aPhoto.customerImageID);
                
                BOOL _isPhotoExists =([aPhoto.customerImageID isEqualToNumber:photo.customerImageID] || [aPhoto.imageName isEqualToString:photo.imageName]);
                
                if(_isPhotoExists)
                {
                    _exists = YES;
                    break;
                }
            }
            
            //if(![self.images containsObject:photo])
            if(!_exists)
            {
                [_parentCustomer addPhotosObject:photo];
                // [self.images addObject:photo];
            }
            
            else
            {
                [_parentCustomer removePhotosObject:photo];
                
                if(self.editingContext)
                {
                    [SSCoreDataManager deleteObject:photo inContext:self.editingContext];
                }
                else
                {
                    [SSCoreDataManager deleteObject:photo];
                    
                }
            }
        }
        
        if(self.editingContext)
        {
            [[SSCoreDataManager sharedManager] saveContext:self.editingContext error:nil];
        }
        else
        {
            [[SSCoreDataManager sharedManager] save:nil];
            
        }
        
        [images setArray:self.customer.photos.allObjects];
        
        [self sortCustomerImages];
        [self reload];

        
    } deletionBlock:^(NSMutableArray *deletedResult) {
        
        for(CustomerPhoto *photo in deletedResult)
        {
            if(photo)
                [_parentCustomer removePhotosObject:photo];
        }

    }];
   
    // [request setCompletionBlock:nil];
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    _pagingScrollview.userInteractionEnabled = YES;
    self.view.userInteractionEnabled = YES;
    
    if(![self.images count])
    {
         ProgressHUD *hud = [ProgressHUD progressHUDAddedToView:self.view];
        
        hud.mode = MBProgressHUDModeText;
        hud.labelText = POOR_CONN_MSG_UNABLE_TO_RETRIEVE_IMAGES;
    }
    
    [self performSelector:@selector(hideHud) withObject:nil afterDelay:3.0];
    //[request setCompletionBlock:nil];
}

-(void)selectImageAtIndex:(NSInteger)index
{
    [self selectImageAtIndex:index animated:NO];
}

-(void)selectImageAtIndex:(NSInteger)index animated:(BOOL)animated
{
    @try
    {
        
    CGPoint _offset =  _pagingScrollview.contentOffset;
    _offset.x = kScrollObjWidth*index;
    [_pagingScrollview setContentOffset:_offset animated:animated];
    
    pageControl.currentPage = index;
    selectedIndex = index;
        
    }
    
    @catch (NSException *exception)
    {
        NSLog(@"Exception name=%@ reason=%@",exception.name,exception.reason);
    }
    @finally 
    {
        
    }
}


-(void)reload
{
    if(![self.images count]) lastSelectedIndex = NSNotFound;
    
    deleteButton.hidden = ![self.images count] ||  !_isEditMode ;
    [self layoutScrollImages];
    [self manageReUploadButton];
}

-(void)cancel
{
    //============ DELETE IMAGES FROM FOLDER. ONLY FOR CREATE CUSTOMER =========
    
    for(CustomerPhoto *photo in self.customer.photos)
    {
        NSString *photoName = photo.imageName;
        NSString *imagePath = [DirectoryManager customerImagePathForName:photoName];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:imagePath])
        {
            NSError *error=nil;
           [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
        }
    }
    
    //============ DELETE IMAGES FROM FOLDER. ONLY FOR CREATE CUSTOMER =========
}
                                  
-(void)pageControlAction:(id)sender
{
    //[pageControl updateCurrentPageDisplay];
}





- (void)layoutScrollImages
{
    NSInteger _numberOfImages = [self numberOfImagesInScroller];
    totalImageCount     = _numberOfImages;
    
    _pagingScrollview.hidden = !_numberOfImages;
    pageControl.hidden = !_numberOfImages;
    
    CGFloat width   = kScrollObjWidth;
	CGFloat curXLoc = 0;
    
    CGRect visibleRect;
    visibleRect.origin = _pagingScrollview.contentOffset;
    visibleRect.size   = _pagingScrollview.frame.size;
    
    
    int numPhotosNotUploaded=0;
    
	for(NSInteger i=0; i<_numberOfImages; i++)
	{
        CustomerPhoto *photo = (i < [images count]) ? [images objectAtIndex:i] : nil;
        
        CGRect viewRect = CGRectMake(curXLoc, 0, CGRectGetWidth(_pagingScrollview.bounds), CGRectGetHeight(_pagingScrollview.bounds));
        
        curXLoc += (width);
        
        if(CGRectIntersectsRect(visibleRect, viewRect))
        {
            CustomerPhotoView * view =nil;   // [cacheDict objectForKey:[NSNumber numberWithInt:i]];
            if(!view) view =(CustomerPhotoView *)[_pagingScrollview viewWithTag:i+1];
            
            // view.hidden = NO;
            
            if(!view)
            {
                view = [[CustomerPhotoView alloc] init];
                view.userInteractionEnabled = YES;
                view.contentMode            = UIViewContentModeScaleAspectFit;
                view.autoresizingMask       = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
                view.backgroundColor        = [UIColor lightGrayColor];
               // view.imageCache = [self imageCache];
                
                UITapGestureRecognizer* tapGesture  =   [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToFullImage:)];
                [(UITapGestureRecognizer *)tapGesture setNumberOfTouchesRequired:1];
                tapGesture.delegate=self;
                [view addGestureRecognizer:tapGesture];
                [tapGesture release];
                
              //  [view hideCrossimage:photo.isUploaded.boolValue];
                
                [_pagingScrollview addSubview:view];
                [view release];
                
                if(photo)
                {
                   [view loadImageFromPhoto:photo];
                }
                else
                {
                    view.image = nil;
                }
                
              
            }
            else 
            {
                //[view retain];
                
                if(photo)
                [view loadImageFromPhoto:photo];
            }
            
            view.frame = viewRect;
            view.tag = i+1; 
 
            //[cacheDict removeObjectForKey:[NSNumber numberWithInt:i]];
            
            if(!photo.isUploaded.boolValue)
            {
                numPhotosNotUploaded++;
            }
            
            [view hideCrossimage:photo.isUploaded.boolValue];
             
        }
        else
        {
            
           // CustomerPhotoView *view =(CustomerPhotoView *)[_pagingScrollview viewWithTag:i+1];
           // view.hidden=YES;
            
            /*
            if(view)
            {
               // [cacheDict setObject:view forKey:[NSNumber numberWithInt:i]];
                [view removeFromSuperview];
                view = nil;
            }
            */
        }  
	}
	
	// Set the content size so it can be scrollable
    CGSize _contentSize = _pagingScrollview.contentSize;
    _contentSize.width  = (_numberOfImages * width);
	[_pagingScrollview setContentSize:_contentSize];
    
    pageControl.numberOfPages =_numberOfImages;
  
    // reuploadButton.hidden  = !numPhotosNotUploaded;
}

-(void)tapToFullImage:(UITapGestureRecognizer *)recognizer
{
    UIImageView *imgView=(UIImageView *)recognizer.view;
    
    CustomerFullImageViewController *_fullImage = [[CustomerFullImageViewController alloc]initWithCustomerPhotos:self.images selectedImageTag:imgView.tag];
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:_fullImage];
    
    //[app.splitViewController presentModalViewController:navController animated:YES];
   // [self presentModalViewController:navController animated:YES];
    
    [app.splitViewController presentViewController:navController animated:YES completion:^{
        
    }];
    
    [navController release];
    [_fullImage release];
}
-(NSInteger)numberOfImagesInScroller
{
    NSUInteger count= [images count];
    
    noImagesImageView.hidden = count>0;
    _pagingScrollview.hidden = !count;
    
    return count;
}


-(UIImage *)imageAtIndex:(NSInteger)index
{
    CustomerPhoto *photo=( index < [images count]) ? [images objectAtIndex:index] : nil;
    
    NSString *imagePath = [DirectoryManager customerImagePathForName:photo.imageName];
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    
    return image;
    
}

#pragma mark ScrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger _curentIndex = ceil(scrollView.contentOffset.x/scrollView.bounds.size.width);
    
    if(selectedIndex!=_curentIndex)
    {
        selectedIndex = _curentIndex;
        pageControl.currentPage = _curentIndex;
    }
    
    if(lastSelectedIndex!=_curentIndex)
    {
        lastSelectedIndex = _curentIndex;
        [self layoutScrollImages];
    }
    
}
#pragma mark -

-(NSMutableArray *)uploadingPhotos
{
    if(!uploadingPhotos)
        uploadingPhotos = [[NSMutableArray alloc] init];
    
    return uploadingPhotos;
}

#pragma mark Upload image to server
-(void)saveImage:(UIImage *)image
{
    deleteButton.hidden = YES;
    
    //NSString *fileName  = [[NSDate date] stringFromDateWithFormat:@"yyMMddhhmmssSS"];
    
    NSString *imageName   = [app globalString];
    imageName = [imageName stringByAppendingString:@".jpg"];
    
    NSString *imagePath = [DirectoryManager customerImagePathForName:imageName];
    
    NSLog(@" imagePath 1 %@",imagePath);
    NSLog(@" imageName %@",imageName);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
       // NSError *error=nil;
        
        UIImage *_originalImage = [image fixOrientation];
        
        NSLog(@"Original: w=%f H=%f",_originalImage.size.width,_originalImage.size.height);
        
        NSData *imageData=UIImageJPEGRepresentation(_originalImage, IMAGE_INTERPOLATION);
        BOOL written=imagePath?[imageData  writeToFile:imagePath atomically:YES]:NO;
        
               
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(written)
            {
                CustomerPhoto *photo = self.editingContext ?  (CustomerPhoto *)[CustomerPhoto insertNewObjectInContext:self.editingContext] : (CustomerPhoto *)[CustomerPhoto insertNewObject];
                
                NSUInteger _custID = [[NSDate date] hash];
                photo.customerImageID = [NSNumber numberWithUnsignedInteger:_custID];
                
                NSTimeInterval _dateInterval = [[NSDate date] GMTTimeIntervalSince1970];
                photo.createdDate = [NSNumber numberWithDouble:_dateInterval];
                
                photo.imageName =imageName;
                NSLog(@"%@",photo.imageName);
                
                
                Customer *_parentCustomer = self.editingContext ? (Customer *) [self.customer inContext:self.editingContext] : self.customer;
                [_parentCustomer addPhotosObject:photo];
                
                [self.images addObject:photo];
       
                
                [self.uploadingPhotos addObject:photo];
                 pageControl.currentPage = pageControl.currentPage+1;
                
                //==============================//
                if([self.images count]==1)
                {
                    [self reload];
                    reuploadButton.hidden=YES;
                }
                //=============================//
                    
              //  NSString *customerName = self.customer.name;
              // BOOL  _isNewCustomer=!customerName.length;
                
                //if(_isNewCustomer)
                {
                    
                    
                }
               // else
                {
                    if([[OfflineManager sharedManager] isNetworkAvailable])
                    {

                       // [self sendCustomerPhotos:imagePath];
                        
                        [self sendCustomerPhotos:photo completion:^(BOOL success) 
                        {
                                                        
                            [self.uploadingPhotos removeAllObjects];
                            photo.isUploaded = [NSNumber numberWithBool:success]; 
                        
                            [self sortCustomerImages];
                            [self reload];
                            
                            if(success)
                            pageControl.currentPage = pageControl.currentPage+1;
                            
                        }];
                    }
                    else
                    {
                        //TODO: =========  IF FAILED THE IMAGE SHOULD BE REMOVED FROM HERE =========================
                     
                                               
                          [self.uploadingPhotos removeAllObjects];
                        
                        [self sortCustomerImages];
                        [self reload];
               
                        
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:POOR_CONNECTION_MSG_IMAGE_UPLOAD_FAILED_MSG delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alert show];
                        [alert release],alert=nil;
                    }
                }
                
             
               // [self reload];
                [self selectImageAtIndex:MAX(0,self.images.count-1) animated:YES];
            }
            
        });
        
    });
}



-(void)sendCustomerPhotos:(CustomerPhoto *)photo
{
    UIView *uploadingView = [[UIView alloc] initWithFrame:app.window.bounds];
    uploadingView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    uploadingView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [app.window addSubview:uploadingView];
    
    ProgressHUD *progress = [ProgressHUD showHUDAddedTo:self.view animated:YES];
    progress.mode = MBProgressHUDModeDeterminate;
    progress.tag=NSIntegerMax;
    progress.labelText = @"Uploading image...";
    
    NSString *imageName = photo.imageName;
    NSString *imagePath=[DirectoryManager customerImagePathForName:imageName];
    
  __block  ASIHTTPRequest *imageRequest =[ConnectionManager uploadImagesForCustomerEdit:imagePath];
    imageRequest.showAccurateProgress = YES;
    [imageRequest setUploadProgressDelegate:self];
    imageRequest.timeOutSeconds = 20.0;
    
    [imageRequest setCompletionBlock:^{
        
        progress.progress = 1.0;
        NSString *res = [imageRequest responseString];
        NSLog(@"res= %@",res);
        res= [res stringByReplacingOccurrencesOfString:@"^%#*" withString:@"="];
        NSLog(@"res1= %@",res);
        NSString *response= [res stringByReplacingOccurrencesOfString:@"*%@$" withString:@","];
        
        NSArray *arr = [response componentsSeparatedByString:@"originalFileName="];
        
        NSLog(@"Arr= %@",arr);
        
        
        NSMutableSet *allPhotos= [NSMutableSet setWithSet:self.customer.photos];
        
        for(NSString *str in arr)
        {
            if(!str.length) continue;
            
            NSArray *infos = [str componentsSeparatedByString:@","];
            
            if([infos count]< 3) continue;
            
            NSString *originalImageName = [infos objectAtIndex:0];
            NSString *uploadedImageLink = [infos objectAtIndex:2];
            uploadedImageLink = [uploadedImageLink stringByReplacingOccurrencesOfString:@"path=" withString:@""];
            
            
            // ================== LOGIC SHOULD BE OPTIMIZED HERE =====================
            
            // CustomerPhoto *currentPhoto=nil;
            for(CustomerPhoto *photo in allPhotos)
            {
                NSString *imageFilePath  = photo.imageName;
                NSString *imageFileName  = [imageFilePath lastPathComponent];
                
                if([[NSFileManager defaultManager] fileExistsAtPath:imageFilePath])
                {
                    [[NSFileManager defaultManager] removeItemAtURL:[NSURL fileURLWithPath:imageFilePath] error:nil];
                }
                
                if([imageFileName isEqualToString:originalImageName])
                {
                    //photo.serverImageLink = uploadedImageLink;
                    photo.imageName = [uploadedImageLink lastPathComponent];
                    break;
                    
                }
            }
            
            //Remove the already processed object so that we dont need to check it in the above loop.
            //if(currentPhoto) [allPhotos removeObject:currentPhoto];
            // ============== LOGIC SHOULD BE OPTIMIZED HERE =====================
            
            NSLog(@"ImageLink = %@",uploadedImageLink);
            NSLog(@"Original File Name = %@",originalImageName);
            
        }
        
        [ProgressHUD hideHUDForView:self.view animated:YES];
        [uploadingView removeFromSuperview];
        [uploadingView release];
        
        
        NSMutableArray *arrImageDetails = [[NSMutableArray alloc]init];
        
        // NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        // NSString *imageFileName =[imagePath lastPathComponent];
        // [dict setValue:imageFileName?imageFileName:@"" forKey:@"imageName"];
        
        NSMutableDictionary *dict = [photo uploadDict];
        [arrImageDetails addObject:dict];
       
        
     __block   ASIHTTPRequest *request =[ConnectionManager saveNewCustomerImagesConnectionWithCustomerImageJson:arrImageDetails forCustomer:self.customer];
        request.timeOutSeconds = 20.0;
        
        [request setCompletionBlock:^{

         [arrImageDetails release];
            
        }];
        
        [request setFailedBlock:^{
           
            [arrImageDetails release];
            
        }];
    }];
    
    [imageRequest setFailedBlock:^{
        
        [UIAlertView showWarningAlertWithTitle:@"" message:POOR_CONNECTION_MSG_IMAGE_UPLOAD_FAILED_MSG];
        [ProgressHUD hideHUDForView:self.view animated:YES];
        [uploadingView removeFromSuperview];
        [uploadingView release];
    }];
}

-(void)sendCustomerPhotos:(CustomerPhoto *)photo completion:(void (^)(BOOL success))completion
{
    CGRect uploadingFrame = app.window.rootViewController.view.bounds;
   __block UIView *uploadingView = [[UIView alloc] initWithFrame:uploadingFrame];
    uploadingView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    uploadingView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [app.window.rootViewController.view addSubview:uploadingView];
    
    
   __block ProgressHUD *progress = [ProgressHUD showHUDAddedTo:self.view animated:YES];
    progress.mode = MBProgressHUDModeIndeterminate;
    //progress.mode = MBProgressHUDModeAnnularDeterminate;
    progress.tag  = NSIntegerMax;
    progress.labelText = @"Uploading image...";
    
    CGPoint _point = [uploadingView convertPoint:progress.center fromView:self.view];
    
    UIProgressView *progressIndicator=[[UIProgressView alloc]initWithProgressViewStyle:UIProgressViewStyleDefault];
    progressIndicator.frame = CGRectMake(_point.x-CGRectGetWidth(progress.frame)/2.0+20.0, _point.y-70.0, CGRectGetWidth(progress.frame)-40.0, 30);
    [uploadingView addSubview:progressIndicator];
    [progressIndicator release];
    
    //============  ADD CANCEL BUTTON ================
    
    UIButton *requestCancelButton =[UIButton repToolCancelButton];
    //[UIButton buttonWithType:UIButtonTypeRoundedRect];
    //requestCancelButton.frame = CGRectMake(CGRectGetMinX(progress.frame), CGRectGetMaxY(progress.frame)+30.0, 100, 50);
    
    requestCancelButton.center = CGPointMake(_point.x, _point.y+90.0);
    [requestCancelButton addTarget:self action:@selector(cancelUploadAction:) forControlEvents:UIControlEventTouchUpInside];
    requestCancelButton.autoresizingMask= UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin |UIViewAutoresizingFlexibleBottomMargin;
    
    [uploadingView addSubview:requestCancelButton];
    //===================================================
    
    NSString *imagePath=[DirectoryManager customerImagePathForName:photo.imageName];
    
   __block ASIHTTPRequest *imageRequest =[ConnectionManager uploadImagesForCustomerEdit:imagePath];
    imageRequest.showAccurateProgress = YES;
     [imageRequest setUploadProgressDelegate:progressIndicator];
    imageRequest.timeOutSeconds = 20.0;
    
    self.currentImageRequest = imageRequest;
    
    __block CustomerImagesViewController *weakSelf = self;
   
    [imageRequest setCompletionBlock:^{
        
        weakSelf.currentImageRequest = nil;
        
        progress.progress = 1.0;
        NSString *res = [imageRequest responseString];
        NSLog(@"res= %@",res);
        res= [res stringByReplacingOccurrencesOfString:@"^%#*" withString:@"="];
        NSLog(@"res1= %@",res);
        NSString *response= [res stringByReplacingOccurrencesOfString:@"*%@$" withString:@","];
        
        NSArray *arr = [response componentsSeparatedByString:@"originalFileName="];
        
        NSLog(@"Arr= %@",arr);
        
        
        NSMutableSet *allPhotos= [NSMutableSet setWithSet:weakSelf.customer.photos];
        
        for(NSString *str in arr)
        {
            if(!str.length) continue;
            
            NSArray *infos = [str componentsSeparatedByString:@","];
            
            if([infos count]< 3) continue;
            
            NSString *originalImageName = [infos objectAtIndex:0];
            NSString *uploadedImageLink = [infos objectAtIndex:2];
            uploadedImageLink = [uploadedImageLink stringByReplacingOccurrencesOfString:@"path=" withString:@""];
            
            
            // ================== LOGIC SHOULD BE OPTIMIZED HERE =====================
            
            // CustomerPhoto *currentPhoto=nil;
            for(CustomerPhoto *photo in allPhotos)
            {
                NSString *imageFilePath  = photo.imageName;
                NSString *imageFileName  = [imageFilePath lastPathComponent];
                
                if([[NSFileManager defaultManager] fileExistsAtPath:imageFilePath])
                {
                    [[NSFileManager defaultManager] removeItemAtURL:[NSURL fileURLWithPath:imageFilePath] error:nil];
                }
                
                if([imageFileName isEqualToString:originalImageName])
                {
                    //photo.serverImageLink = uploadedImageLink;
                    photo.imageName = [uploadedImageLink lastPathComponent];
                    break;
                    
                }
            }
            
            //Remove the already processed object so that we dont need to check it in the above loop.
            //if(currentPhoto) [allPhotos removeObject:currentPhoto];
            // ============== LOGIC SHOULD BE OPTIMIZED HERE =====================
            
            NSLog(@"ImageLink = %@",uploadedImageLink);
            NSLog(@"Original File Name = %@",originalImageName);
            
        }
        
        [ProgressHUD hideHUDForView:self.view animated:YES];
        [uploadingView removeFromSuperview];
        [uploadingView release];
        
        
        BOOL _isNewCustomer = !self.customer.name.length;
        
        if(_isNewCustomer)
        {
            if(completion) completion(YES);
        }
        else
        {
            NSMutableArray *arrImageDetails=[[NSMutableArray alloc]init];
            
           //  NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
           //  NSString *imageFileName =[imagePath lastPathComponent];
           //  [dict setValue:imageFileName?imageFileName:@"" forKey:@"imageName"];
           //  [dict release];
            
            NSMutableDictionary *dict=[photo uploadDict];
            [arrImageDetails addObject:dict];
           
            
            NSLog(@"image json=%@",[arrImageDetails JSONRepresentation]);
            
          __block  ASIHTTPRequest *request =[ConnectionManager saveNewCustomerImagesConnectionWithCustomerImageJson:arrImageDetails forCustomer:self.customer];
            request.timeOutSeconds = 20.0;
            
            [request setCompletionBlock:^{
                
                NSDictionary *imageResponse = [request responseJSON];
                NSLog(@"Response=%@",[request responseString]);
                
                imageResponse = NULL_NIL(imageResponse);
               

                NSNumber *isUnderMaintenance  = [imageResponse objectForKey:JSON_UNDER_MAINTENANCE_KEY];
                
                isUnderMaintenance = NULL_NIL(isUnderMaintenance);
                
                if([isUnderMaintenance boolValue])
                {
                    [app showUnderMaintainenceScreen:YES];
                }

             
                BOOL success =[[imageResponse valueForKey:@"status"] boolValue];
                
                [arrImageDetails release];
                
                if(success)
                {
                    NSArray *customerImageDetails =  [imageResponse valueForKey:@"customerImageDetails"];
                    
                    customerImageDetails = NULL_NIL(customerImageDetails);
                    
                    for(NSDictionary *imageResponseDict in customerImageDetails)
                    {
                      NSNumber *imageID = [imageResponseDict valueForKey:@"customerImageID"];
                      NSNumber *oldImageID = [imageResponseDict valueForKey:@"oldCustomerImageID"];
                    
                      if(oldImageID && [photo.customerImageID isEqualToNumber:oldImageID])
                      {
                          photo.customerImageID = imageID;
                          photo.isUploaded = [NSNumber numberWithBool:YES];
                      }
                    }
                }
                
                if(completion) completion(success);
                
            }];
            
            [request setFailedBlock:^{
                
                if(completion) completion(NO);
            }];
            
        }
    }];
    
    [imageRequest setFailedBlock:^{
        
        weakSelf.currentImageRequest = nil;
        
        if(weakSelf->isImageUploadCancelled)
        {
            progress.labelText = @"Cancelling..";
            isImageUploadCancelled = NO;
            
            [self performSelector:@selector(hideProgressHUD) withObject:nil afterDelay:1.0];
        }
        else
        {
            //progress.labelText = POOR_CONNECTION_MSG_IMAGE_UPLOAD_FAILED_MSG;
            
            [UIAlertView showWarningAlertWithTitle:@"" message:POOR_CONNECTION_MSG_IMAGE_UPLOAD_FAILED_MSG];
            
            [weakSelf performSelector:@selector(hideProgressHUD) withObject:nil afterDelay:0.0];
        }
        
        [uploadingView removeFromSuperview];
        [uploadingView release];
        
        if(completion) completion(NO);
    }];
}

-(void)hideProgressHUD
{
    [ProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)sendPendingCustomerPhotosWithCompletion:(void (^)(BOOL success))completion
{
   NSArray *pendingPhotos = [self.images filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.isUploaded=%@",[NSNumber numberWithBool:NO]]];
    
    [self.uploadingPhotos addObjectsFromArray:pendingPhotos];
    
    if(![pendingPhotos count]) return;
    
    CGRect uploadingFrame = app.window.rootViewController.view.bounds;
   __block UIView *uploadingView = [[UIView alloc] initWithFrame:uploadingFrame];
    uploadingView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    uploadingView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [app.window.rootViewController.view addSubview:uploadingView];

    
   __block ProgressHUD *progress = [ProgressHUD showHUDAddedTo:self.view animated:YES];
    progress.mode = MBProgressHUDModeCustomView;
    progress.tag=NSIntegerMax;
    progress.labelText = @"Uploading image...";
    
      CGPoint _point = [uploadingView convertPoint:progress.center fromView:self.view];
    
   __block UIProgressView *progressIndicator=[[UIProgressView alloc]initWithProgressViewStyle:UIProgressViewStyleDefault];
    progressIndicator.frame = CGRectMake(_point.x-CGRectGetWidth(progress.frame)/2.0+20.0, _point.y-70.0, CGRectGetWidth(progress.frame)-40.0, 30);
     // progressIndicator.frame = CGRectMake(0, 2.0, CGRectGetWidth(progress.frame)-20.0, 30);
    //progress.customView=progressIndicator;
    [uploadingView addSubview:progressIndicator];
    [progressIndicator release];

    
    UIButton *requestCancelButton = [UIButton repToolCancelButton];
    //[UIButton buttonWithType:UIButtonTypeRoundedRect];
    //requestCancelButton.frame = CGRectMake(CGRectGetMinX(progress.frame), CGRectGetMaxY(progress.frame)+30.0, 100, 50);
  
    requestCancelButton.center = CGPointMake(_point.x, _point.y+90.0);
    [requestCancelButton addTarget:self action:@selector(cancelUploadAction:) forControlEvents:UIControlEventTouchUpInside];
    requestCancelButton.autoresizingMask= UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin |UIViewAutoresizingFlexibleBottomMargin;
    
    [uploadingView addSubview:requestCancelButton];

    
   __block ASIHTTPRequest *imageRequest =[ConnectionManager uploadPendingCustomerImages:pendingPhotos];
    imageRequest.showAccurateProgress = YES;
    [imageRequest setUploadProgressDelegate:progressIndicator];
    imageRequest.timeOutSeconds = 20.0;
    
    self.currentImageRequest = imageRequest;
    
    __block CustomerImagesViewController *weakSelf = self;
    [imageRequest setCompletionBlock:^{
        
        weakSelf.currentImageRequest = nil;
        progress.progress = 1.0;
        
        NSMutableArray *arrImageDetails=[[NSMutableArray alloc]init];
        NSArray *allPhotos= [pendingPhotos retain];
        
            // ================== LOGIC SHOULD BE OPTIMIZED HERE =====================
            
            // CustomerPhoto *currentPhoto=nil;
            for(CustomerPhoto *photo in allPhotos)
            {
                NSMutableDictionary *dict=[photo uploadDict];
                [arrImageDetails addObject:dict];
            }
        
        [allPhotos release];
           NSLog(@"arr image details=%@",arrImageDetails);
            
                 
        [ProgressHUD hideHUDForView:weakSelf.view animated:YES];
        [uploadingView removeFromSuperview];
        [uploadingView release];
        
        BOOL _isNewCustomer = !weakSelf.customer.name.length;
        
        if(_isNewCustomer)
        {
               [arrImageDetails release];
            if(completion) completion(YES);
        }
        else
        {
            
       __block  ASIHTTPRequest *request = [ConnectionManager saveNewCustomerImagesConnectionWithCustomerImageJson:arrImageDetails forCustomer:weakSelf.customer];
            
           request.timeOutSeconds = 20.0;
        
           [request setCompletionBlock:^{
            
            [arrImageDetails release];
               
            NSDictionary *jsonDict = [request responseJSON];
               
            jsonDict = NULL_NIL(jsonDict);
               
            
            NSNumber *isUnderMaintenance  = [jsonDict objectForKey:JSON_UNDER_MAINTENANCE_KEY];
            isUnderMaintenance = NULL_NIL(isUnderMaintenance);
            if([isUnderMaintenance boolValue])
            {
                [app showUnderMaintainenceScreen:YES];
            }
               
            BOOL _success = [[jsonDict valueForKey:@"status"] boolValue];
            
            if(_success)
            {
                NSArray *customerImageDetails =  [jsonDict valueForKey:@"customerImageDetails"];
                
                customerImageDetails = NULL_NIL(customerImageDetails);
                
                for(NSDictionary *imageResponseDict in customerImageDetails)
                {
                    NSNumber *imageID = [imageResponseDict valueForKey:@"customerImageID"];
                    NSNumber *oldImageID = [imageResponseDict valueForKey:@"oldCustomerImageID"];
                    
                    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"self.customerImageID=%@",oldImageID];
                    
                    NSArray *photoObjects =  [allPhotos filteredArrayUsingPredicate:filterPredicate];
                    
                    for(CustomerPhoto *photo in photoObjects)
                    {
                        photo.customerImageID = imageID;
                        photo.isUploaded = [NSNumber numberWithBool:YES];
                    }

                }

                /*
                for(CustomerPhoto *photo in allPhotos)
                {
                    photo.isUploaded = [NSNumber numberWithBool:YES];
                }
                */
            }
            
        if(completion) completion(_success);
            
        }];
        
            [request setFailedBlock:^{
                
            if(completion) completion(NO);
                
            }];
            
       }
    }];
    
    [imageRequest setFailedBlock:^{
        
        weakSelf.currentImageRequest = nil;
        
        if(weakSelf->isImageUploadCancelled)
        {
            progress.labelText = @"Cancelling..";
            weakSelf->isImageUploadCancelled = NO;
            [weakSelf performSelector:@selector(hideProgressHUD) withObject:nil afterDelay:1.0];
        }
        else
        {
           [UIAlertView showWarningAlertWithTitle:@"" message:POOR_CONNECTION_MSG_IMAGE_UPLOAD_FAILED_MSG];
        }
        
        [ProgressHUD hideHUDForView:weakSelf.view animated:YES];
        [uploadingView removeFromSuperview];
        [uploadingView release];
        
         
        if(completion) completion(NO);
    }];
}

-(void)cancelUploadAction:(UIButton *)sender
{
    isImageUploadCancelled = YES;
    
    if(self.currentImageRequest)
    [self.currentImageRequest  cancel];
    
    self.currentImageRequest = nil;
    
    Customer *_parentCustomer = self.editingContext ? (Customer *) [self.customer inContext:self.editingContext] : self.customer;
    
   
    
    for(CustomerPhoto *photo in self.uploadingPhotos)
    {
       [_parentCustomer removePhotosObject:photo];
        [self.images removeObject:photo];
        
         NSManagedObjectContext *context = self.editingContext ? self.editingContext: DEFAULT_CONTEXT;
        [SSCoreDataManager deleteObject:photo inContext:context];
       
    }
    
    [self.uploadingPhotos removeAllObjects];
    
     //NSNumber *custID = _parentCustomer.customerID;
    
    for(UIScrollView *subview in [_pagingScrollview subviews])
    {
        if(subview && [subview superview])
            [subview removeFromSuperview];
    }
    
    [self sortCustomerImages];
    [self reload];
  
    
   // [UIAlertView showWarningAlertWithTitle:@"" message:@"Image upload cancelled"];
    
   
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    
    //[_pagingScrollview release];
   // _pagingScrollview = nil;
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
