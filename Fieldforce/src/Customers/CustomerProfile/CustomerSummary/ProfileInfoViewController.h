//
//  CustomerProfileViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileInfoEditViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
@class Customer;
@interface ProfileInfoViewController : UITableViewController<UIAlertViewDelegate,MFMailComposeViewControllerDelegate>
{
    //UIImageView *profileImageView;
    //UIButton *editButton;
}

@property(nonatomic,retain)Customer *customer;

-(id)initWithCustomer:(Customer *)theCustomer;
-(void)enableEditMode:(BOOL)_isEditMode;

-(void)launchDefaultMapApplication;

-(void)reloadData;

-(void)loadContactsPeformingDelete:(NSArray *)deleteArr update:(NSArray *)updateArr insert:(NSArray *)insertArr;

@end
