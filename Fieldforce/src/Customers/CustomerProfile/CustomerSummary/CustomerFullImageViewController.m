//
//  CusromerFullImageViewController.m
//  RepVisitationTool
//
//  Created by Subhojit Dey on 12/17/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomerFullImageViewController.h"
#import "CustomerPhoto.h"
#import "VisitPhoto.h"
#import "LazyImageLoader.h"
#import "UIImage+Resize.h"
//#import <ImageIO/ImageIO.h>
#import "SSMemoryCache.h"

#define IMAGE_COMPRESSED_SIZE CGSizeMake(332.0, 249.0)

@interface CustomerFullImageViewController ()<ImageLoaderDelegate>
{
   // NSCache *_imageCache;
    SSMemoryCache *_imageCache;
    
    NSMutableSet *loadingURLS;
}

-(void)showNextImage;
-(void)showPrevImage;
-(UIImage *)imageAtIndex:(NSInteger)index;
//-(UIImage *)visitPhotoAtIndex:(NSInteger)index;

-(void)loadImageForIndex:(NSInteger)index;

@end

@implementation CustomerFullImageViewController
@synthesize isVisitPhoto;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}
-(id)initWithCustomerPhotos:(NSMutableArray *)arr selectedImageTag:(int)selectedTag
{
    
    if((self=[super init]))
    {

        arrImages=[arr retain];
        currentIndex=selectedTag-1;
        NSLog(@"%d",currentIndex);
      
    
        NSLog(@" fff %@",arrImages);
        
        isVisitPhoto=NO;
        
        loadingURLS = [[NSMutableSet alloc] init];
    }
    
    return self;
}

-(id)initWithCustomerVisitPhotos:(NSMutableArray *)arr selectedIndex:(int)selectedTag
{
    
    if((self=[super init]))
    {
        
        arrImages=[arr retain];
        currentIndex=selectedTag;
        NSLog(@"%d",currentIndex);

        NSLog(@" fff %@",arrImages);
        
        isVisitPhoto=YES;
    }
    
    return self;
}

-(SSMemoryCache *)imageCache
{
    if(!_imageCache)
    {
        //_imageCache = [[NSCache alloc] init];
        //_imageCache.name = @"FullImage";
        // [_imageCache setCountLimit:6];
        
        //[_imageCache setTotalCostLimit:1024*1024*2];
        //[imageCache setEvictsObjectsWithDiscardedContent:YES];
        
        _imageCache = [[SSMemoryCache alloc] init];
        _imageCache.cacheCount = 5;
    }
    
    return _imageCache;
}

-(void)dealloc
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ImageLoaderDidLoadImageNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ImageLoaderDidFailImageNotification object:nil];
    
    [fullImageContainerView release];
    [customerimgView release];
    [arrImages release];
    [_activityIndicator release];
    [oneFingerSwipeRight release];
    [oneFingerSwipeLeft release];
    

    [_imageCache release];
    _imageCache=nil;
    
    [loadingURLS release];
    
    [super dealloc];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    //app.window.backgroundColor = [UIColor whiteColor];
    self.view.backgroundColor=[UIColor whiteColor];
    
    //[self.navigationController.navigationBar addBackGroundImage:@"autoprofiler_banner.png"];
    //[self addTitle:@"dskdjs" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    [self.navigationController.navigationBar addBackGroundImage:@"navigation_bar.png"];
    [self addBackButtonWithSelector:@selector(backButtonAction)];
    
    if(!fullImageContainerView)
    {
    fullImageContainerView  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds))];
    fullImageContainerView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin;
    fullImageContainerView.backgroundColor = self.view.backgroundColor;
    [self.view addSubview:fullImageContainerView];
        
    //self.view.clipsToBounds = YES;
    
    if(!customerimgView)
    {
     customerimgView=[[UIImageView alloc]init];
     customerimgView.frame=fullImageContainerView.bounds;
     customerimgView.contentMode=UIViewContentModeScaleAspectFit;
     customerimgView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
     customerimgView.backgroundColor = fullImageContainerView.backgroundColor;
     [fullImageContainerView addSubview:customerimgView];
    }
    
    }
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _activityIndicator.tag=1001;
    _activityIndicator.hidesWhenStopped = YES;
    [customerimgView addSubview:_activityIndicator];
    _activityIndicator.center = customerimgView.center;
    _activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
     [_activityIndicator startAnimating];
    
    oneFingerSwipeRight =[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onOneFingerSwipeLeft:)];
    [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [fullImageContainerView addGestureRecognizer :oneFingerSwipeRight];
   
	// --------------------------------
	// One finger, swipe left
	// --------------------------------
    oneFingerSwipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onOneFingerSwipeRight:)];
    [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [fullImageContainerView addGestureRecognizer:oneFingerSwipeLeft];


    
	// Do any additional setup after loading the view.
    
    pageControl = [UIPageControl pageControlWithFrame:CGRectMake((CGRectGetWidth(self.view.bounds)-100.0)/2.0, CGRectGetHeight(self.view.bounds)-25.0, 100, 20) numberOfPages:[arrImages count] target:nil action:nil];
    [self.view addSubview:pageControl];
    pageControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
    pageControl.currentPage = currentIndex;
    pageControl.userInteractionEnabled=NO;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDidLoad:) name:ImageLoaderDidLoadImageNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDidFail:) name:ImageLoaderDidFailImageNotification object:nil];
    
     [self loadImageForIndex:currentIndex];
    
}

#pragma mark - TEST
-(void)viewWillAppear:(BOOL)animated
{
    app.window.backgroundColor = [UIColor whiteColor];
}

-(void)viewWillDisappear:(BOOL)animated
{
    app.window.backgroundColor = [UIColor blackColor];
}
#pragma mark -

-(void)onOneFingerSwipeLeft:(UISwipeGestureRecognizer *)recognizer
{
    if(currentIndex<=0) return;
    
    CATransition *animation = [CATransition animation];
    [animation setDelegate:self];
    [animation setDuration:0.40f];
    //[animation setTimingFunction: UIViewAnimationCurveEaseInOut];
     animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [animation setType: kCATransitionPush];
    [animation setSubtype: kCATransitionFromLeft];
    // animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = YES;
    
    
    [[fullImageContainerView layer] removeAllAnimations];
    [[fullImageContainerView layer] addAnimation:animation forKey:@"swipeAnimationLeft"];
    
     //[[self.view layer] removeAllAnimations];
    // [[self.view layer] addAnimation:animation forKey:@"swipeAnimationLeft"];

   
    [self showPrevImage];
    pageControl.currentPage = currentIndex;
}

-(void)onOneFingerSwipeRight:(UISwipeGestureRecognizer *)recognizer
{
    NSUInteger totalOffersCount=[arrImages count];
    
    if(currentIndex>=totalOffersCount-1) return;
    
    CATransition *animation = [CATransition animation];
    [animation setDelegate:self];
    [animation setDuration:0.40f];
    //[animation setTimingFunction: UIViewAnimationCurveEaseInOut];
     animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [animation setType: kCATransitionPush];
    [animation setSubtype: kCATransitionFromRight];
   // animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = YES;
    
    [[fullImageContainerView layer] removeAllAnimations];
    [[fullImageContainerView layer] addAnimation:animation forKey:@"swipeAnimationRight"];
    
    //[[self.view layer] removeAllAnimations];
    //[[self.view layer] addAnimation:animation forKey:@"swipeAnimationRight"];
    
    [self showNextImage];
    pageControl.currentPage = currentIndex;
}
-(void)showPrevImage
{
    [_activityIndicator startAnimating];

    currentIndex--;
    
    currentIndex = MAX(0,currentIndex);
    
    NSLog(@"Prev=%d",currentIndex);
    
    if(currentIndex < [arrImages count] && currentIndex>=0)
    {
        [self loadImageForIndex:currentIndex];
         pageControl.currentPage = currentIndex;
    }
}

-(void)showNextImage
{
    [_activityIndicator startAnimating];

    NSUInteger totalOffersCount=arrImages.count;
    currentIndex++;
    currentIndex=MIN(currentIndex,totalOffersCount);
    
    NSLog(@"Next=%d",currentIndex);
    
    if(currentIndex < [arrImages count]  && currentIndex>=0)
    {
        [self loadImageForIndex:currentIndex];
         pageControl.currentPage = currentIndex;
    }
}


-(NSString *)imageCacheKeyForIndex:(NSInteger)index
{
    NSString *imageName = nil;
    
    if(isVisitPhoto)
    {
        VisitPhoto *photo=( index>=0 && index < [arrImages count]) ? [arrImages objectAtIndex:index] : nil;
        imageName = [photo.imagePath lastPathComponent];
        
    }
    else
    {
        CustomerPhoto *photo=(index>=0 && index < [arrImages count]) ? [arrImages objectAtIndex:index] : nil;
        imageName  =photo.imageName;
    }

    return imageName;
}

-(void)loadImageForIndex:(NSInteger)index
{
    UIImage *image = [self imageAtIndex:index];
    
    if(image)
    {
        //customerimgView.image = nil;
        
       // if([UIScreen mainScreen].scale == 2.0)
       // image = [UIImage imageWithCGImage:image.CGImage scale:2.0 orientation:image.imageOrientation];
        
        customerimgView.image = image;
        
        [_activityIndicator stopAnimating];
        
        if(!isVisitPhoto)
        {
          CustomerPhoto *photo=(index>=0 && index < [arrImages count]) ? [arrImages objectAtIndex:index] : nil;
          photo.isDownloading = NO;
        }
      
    }
    else
    {
            customerimgView.image=nil;
        
             if(isVisitPhoto) return;
        
            CustomerPhoto *photo=(index>=0 && index < [arrImages count]) ? [arrImages objectAtIndex:index] : nil;
        
             if(photo.isDownloading) return;
        
              NSString *imageName = photo.imageName;
              
              //=========== LOAD IMAGE FROM SERVER ===========
              NSString *imageUrl = imageName.length ? [CUSTOMER_IMAGE_DOWNLOAD_LINK stringByAppendingString:imageName] : nil;
        
               if(imageUrl)
               {
                 if([loadingURLS containsObject:imageUrl])
                  {
                    return;
                  }
                   
                 [loadingURLS addObject:imageUrl];
               }
        
               [_activityIndicator startAnimating];
        
               photo.isDownloading = YES;
        
                NSString *imagePath=[DirectoryManager customerImagePathForName:imageName];
              [LazyImageLoader loadImageForUrlString:imageUrl indexPath:nil delegate:nil targetPath:imagePath];

              //============================================
          
    }
}

- (UIImage *)imageFromCachedFile:(NSString *)path
{
    if(![[NSFileManager defaultManager] fileExistsAtPath:path]) return nil;
    
    /*
    CFURLRef pathURL = (CFURLRef)[NSURL fileURLWithPath:path];
    CGImageSourceRef imageSource = CGImageSourceCreateWithURL(pathURL, NULL);
    
    CGImageRef imageRef = CGImageSourceCreateImageAtIndex(imageSource, 0, NULL);
    UIImage *image = [[UIImage alloc] initWithCGImage:imageRef];
    
    CGImageRelease(imageRef);
    return [image autorelease];
    */
    /*
    NSData *imageData = [[NSData alloc] initWithContentsOfFile:path options:NSDataReadingMappedIfSafe error:nil];
    UIImage *image  = [UIImage imageWithData:imageData];
    [imageData release];
    */
    
    UIImage *image  = [UIImage imageWithContentsOfFile:path];
    
    return image;
}

-(UIImage *)imageAtIndex:(NSInteger)index
{
    UIImage *image = nil;
    NSString *imageFilePath = nil;
      NSString *imageName = nil;
    
    if(isVisitPhoto)
    {
        VisitPhoto *photo=( index>=0 && index < [arrImages count]) ? [arrImages objectAtIndex:index] : nil;

        if(!photo) return nil;

         imageFilePath = photo.imagePath;
         imageName = [imageFilePath lastPathComponent];
    } 
    else
    {
        CustomerPhoto *photo=(index>=0 && index < [arrImages count]) ? [arrImages objectAtIndex:index] : nil;
        if(!photo) return nil;
        
        
        imageName  = photo.imageName;
        imageFilePath  = [DirectoryManager customerImagePathForName:imageName];
    }
    
      image = [[self imageCache] objectForKey:imageName];
    
      if(!image)
      {
          
            image = [self imageFromCachedFile:imageFilePath];

            if(image)
            {
               [[self imageCache] setObject:image forKey:imageName];
            }
      }
    
     pageControl.currentPage = index;
    
    return image;
}

/*
-(UIImage *)visitPhotoAtIndex:(NSInteger)index
{
    VisitPhoto *photo=( index>=0 && index < [arrImages count]) ? [arrImages objectAtIndex:index] : nil;
    
    if(!photo) return nil;
    
    NSString *imageFilePath = photo.imagePath;
    UIImage *image = [UIImage imageWithContentsOfFile:imageFilePath];
    pageControl.currentPage = index;
    return image;

}
*/

-(void)imageDidLoad:(NSNotification *)notif
{
    
    NSLog(@"is main thread = %d",[NSThread isMainThread]);
    
     LazyImageLoader *loader = notif.object;
    
    UIImage *_dloadedImage = loader.image;
    
    if(!_dloadedImage) return;
    
     NSString *key = [loader.urlString lastPathComponent];
    
     CustomerPhoto *photo=(currentIndex>=0 && currentIndex < [arrImages count]) ? [arrImages objectAtIndex:currentIndex] : nil;
    
    
    if([key isEqualToString:photo.imageName])
    {
        photo.isDownloading = NO;
        
       // _dloadedImage = [UIImage imageWithImage:_dloadedImage scaledToMaxWidth:IMAGE_COMPRESSED_SIZE.width maxHeight:IMAGE_COMPRESSED_SIZE.height];
        
        NSLog(@"%f %f",_dloadedImage.size.width,_dloadedImage.size.height);
        customerimgView.image =_dloadedImage;
        
        [_activityIndicator stopAnimating];
        
         CATransition *anim = [CATransition animation];
         [anim setDuration:0.3f];
         [anim setType:kCATransitionPush];
         [anim setSubtype:kCATransitionFade];
         [anim setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
         [[customerimgView layer] addAnimation:anim forKey:@"fade in"];
         
    }
 
            
    if(_dloadedImage)
    {
        [[self imageCache] setObject:_dloadedImage forKey:key];
        
        
        NSString *imagePath=[DirectoryManager customerImagePathForName:key];
        
        if(imagePath && ![[NSFileManager defaultManager] fileExistsAtPath:imagePath])
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            //@autoreleasepool
            {
              NSData *imageData = UIImageJPEGRepresentation(_dloadedImage, 0.9);
              //NSError *error = nil;
              BOOL written = [imageData  writeToFile:imagePath atomically:YES];
              NSLog(@"written=%d",written);
            }
                
            });
        }
        

    }
    
    if([loadingURLS containsObject:loader.urlString])
    [loadingURLS removeObject:loader.urlString];
}

-(void)imageDidFail:(NSNotification *)notif
{
     LazyImageLoader *loader = notif.object;
    
    NSString *key = [loader.urlString lastPathComponent];
    
    CustomerPhoto *photo=(currentIndex>=0 && currentIndex < [arrImages count]) ? [arrImages objectAtIndex:currentIndex] : nil;
    
    if([key isEqualToString:photo.imageName])
    {
        photo.isDownloading = NO;
        [_activityIndicator stopAnimating];
    }
    
    if([loadingURLS containsObject:loader.urlString])
        [loadingURLS removeObject:loader.urlString];
}

-(void)backButtonAction
{
    if(_imageCache)
        //[_imageCache removeAllObjects];
        [_imageCache clear];
    
    if(isVisitPhoto)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
      //  [LazyImageLoader cancelAllConnectionsForIdentifier:NSStringFromClass([self class])];
    
      [self dismissModalViewControllerAnimated:YES];
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [fullImageContainerView release];
    fullImageContainerView = nil;
    
    [customerimgView release];
    customerimgView = nil;
}

- (void)didReceiveMemoryWarning
{
#if LOCAL
    
    [UIAlertView showWarningAlertWithTitle:@"Memory warning" message:@"At CustomerFullImageViewController"];
#endif
    
    
    if(_imageCache)
   // [_imageCache removeAllObjects];
    [_imageCache clear];

    
    //================================================================================
   // [LazyImageLoader cancelAllConnectionsForIdentifier:NSStringFromClass([self class])];
    //================================================================================
    
     [super didReceiveMemoryWarning];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}
@end
