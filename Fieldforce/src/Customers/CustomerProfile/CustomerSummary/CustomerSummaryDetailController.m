//
//  CustomerSummaryDetailController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 28/08/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomerSummaryDetailController.h"
#import "ViewOnMapViewController.h"
#import "CustomerImagesViewController.h"
#import "Customer.h"
#import "LazyImageLoader.h"


@interface CustomerSummaryDetailController ()
{
    UIImageView *noPreviewImageView;
}

@end

@implementation CustomerSummaryDetailController
@synthesize customer;
@synthesize editingContext;

- (id)initWithCustomerInfo:(Customer *)aCustomer
{
    self = [super init];
    if (self) 
    {
        self.customer  = aCustomer;
    }
    return self;
}

-(void)enableEditMode:(BOOL)editMode
{
    [customerImagesViewController enableEditMode:editMode];
    [viewOnMapController enableEditMode:editMode];
}

-(void)reload
{
    if(self.customer.suburb.length && self.customer.state.length)
    {
        noPreviewImageView.hidden       = YES;
        viewOnMapController.view.hidden = NO;
        
        //Calculate Lat,Long from address
        [viewOnMapController reloadMap];
        
    }
    else 
    {
        noPreviewImageView.hidden       = NO;
        viewOnMapController.view.hidden = YES;
    }
}

-(void)cancel
{
    [customerImagesViewController cancel];
}

-(void)backAction
{
    [customerImagesViewController backAction];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
	// Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
    
      CGRect _bounds = self.view.bounds;
    
    UIImage *noPreviewImage=[UIImage imageNamed:@"blank_map_field.png"];
    CGRect viewOnMapControllerImageFrame = CGRectMake(10.0, 10.0, noPreviewImage.size.width,noPreviewImage.size.height);
    
    if(!viewOnMapController && self.customer.name.length)
    {
      viewOnMapController = [[ViewOnMapViewController alloc] initWithCutomerInfo:self.customer];
      viewOnMapController.view.frame = CGRectMake(10.0, 10.0, 720.0, 500);
      viewOnMapController.view.layer.cornerRadius=6.0;
      viewOnMapController.view.layer.masksToBounds=YES;
      viewOnMapController.view.autoresizingMask=UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.view addSubview:viewOnMapController.view];
    viewOnMapController.view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    viewOnMapController.view.layer.borderWidth = 0.3;
    
    [self addChildViewController:viewOnMapController];
    [viewOnMapController didMoveToParentViewController:self];
    }
    
    noPreviewImageView = [[UIImageView alloc] initWithFrame:viewOnMapControllerImageFrame];
    noPreviewImageView.image = noPreviewImage;
    //noPreviewImageView.autoresizingMask=UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.view addSubview:noPreviewImageView];

    
    if(!customerImagesViewController)
    {
    CGFloat locY=CGRectGetMaxY(viewOnMapControllerImageFrame)+20.0;
    CGRect customerImagesViewControllerFrame = CGRectMake(0.0, locY, CGRectGetWidth(self.view.bounds), CGRectGetHeight(_bounds)-locY);
    
    customerImagesViewController = [[CustomerImagesViewController alloc] initWithCustomer:self.customer];
    customerImagesViewController.editingContext = self.editingContext;
    customerImagesViewController.view.frame =customerImagesViewControllerFrame;
    [self.view addSubview:customerImagesViewController.view];
   /// customerImagesViewController.view.autoresizingMask=UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin;
    
    [self addChildViewController:customerImagesViewController];
    [customerImagesViewController didMoveToParentViewController:self];
    }

    BOOL _addressAvailable =  (self.customer.suburb.length && self.customer.state.length);
    viewOnMapController.view.hidden = !_addressAvailable;
    noPreviewImageView.hidden = _addressAvailable;
}

-(void)viewWillAppear:(BOOL)animated
{
    //[customerImagesViewController viewWillAppear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

-(void)dealloc
{
    customer = nil;
    editingContext = nil;
    
   // ====================================
    [LazyImageLoader cancelAllConnections];
    //====================================
   
    [viewOnMapController release];
    [noPreviewImageView release];
    
    //[customerImagesViewController clear];
    
    //[customerImagesViewController.view removeFromSuperview];
    [customerImagesViewController release];
    
    noPreviewImageView =nil;
    viewOnMapController = nil;
    customerImagesViewController=nil;
    
    [super dealloc];
}

@end
