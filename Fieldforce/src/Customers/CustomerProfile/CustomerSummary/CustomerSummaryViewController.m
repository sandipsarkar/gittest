//
//  CustomerSummaryViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 23/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomerSummaryViewController.h"
#import "ProfileInfoViewController.h"
#import "ViewOnMapViewController.h"
#import "CustomerSummaryDetailController.h"
#import "CustomerProfileViewController.h"
#import "CustomersViewController.h"
#import "Customer.h"
#import "Rep.h"
#import "OfflineManager.h"
#import "CustomerContact.h"

@interface CustomerSummaryViewController()<ProfileInfoEditDelegate>
{
    BOOL _isNewCustomer;
    BOOL isEditMode;
    CGRect _containerRect;
    ParallelStackViewController *_parallelStackViewController;
}

@property(nonatomic,retain) ASIHTTPRequest *currentContactRequest;
@end

@implementation CustomerSummaryViewController
@synthesize customerProfileViewController;
@synthesize profileInfoEditViewController;
@synthesize customerSummaryDetailController;
@synthesize customerProfileController;

@synthesize currentContactRequest;

-(void)dealloc
{
    //
    currentContactRequest.delegate = nil;
    [currentContactRequest clearDelegatesAndCancel];
    [currentContactRequest release];
    currentContactRequest = nil;
    
    customerProfileController = nil;
    
    [profileInfoEditViewController release];
    profileInfoEditViewController   = nil;

    
    [customerProfileViewController release];
    customerProfileViewController   = nil;
    
    [customerSummaryDetailController release];
    customerSummaryDetailController = nil;
    
    [_parallelStackViewController release];
    _parallelStackViewController     = nil;
    

    

    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) 
    {
//        customerProfileViewController = [[ProfileInfoViewController alloc] init];
//        _parallelStackViewController  = [[ParallelStackViewController alloc]initWithMasterController:customerProfileViewController];
//        profileInfoEditViewController = [[ProfileInfoEditViewController alloc] init];
    }
    return self;
}



-(id)initWithCustomer:(Customer *)aCustomer
{
    self = [super init];
    
    if (self) 
    {
         NSString *customerName = aCustomer.name;
         _isNewCustomer=!customerName.length;
        
        customerProfileViewController  = [[ProfileInfoViewController alloc] initWithCustomer:aCustomer];
         profileInfoEditViewController = [[ProfileInfoEditViewController alloc] initWithCustomer:aCustomer];
        //_parallelStackViewController   = [[ParallelStackViewController alloc] initWithMasterController:customerProfileViewController];
        
       // [_parallelStackViewController setMasterController:isEditMode? profileInfoEditViewController : customerProfileViewController];
    }
    return self;
}

-(void)enableEditMode:(BOOL)editmode
{
    if(isEditMode!=editmode)
    {
            
    isEditMode=editmode;
    [customerSummaryDetailController enableEditMode:editmode];
    [customerProfileViewController enableEditMode:editmode];
        
        if(isEditMode)
        {
            profileInfoEditViewController.delegate = self;
            [profileInfoEditViewController reloadData];
        }
        
    [_parallelStackViewController setMasterController:isEditMode? profileInfoEditViewController : customerProfileViewController animated:YES];
        
        if(isEditMode)
        {
           // [customerProfileViewController viewWillDisappear:NO];
            [profileInfoEditViewController viewWillAppear:NO];
        }
        else
        {
            [profileInfoEditViewController viewWillDisappear:NO];
           // [customerProfileViewController viewWillAppear:NO];
        }
     
    }
}

-(void)loadContacts
{


    
    if(_isNewCustomer) return;

   // Customer *_customer = customerProfileViewController.customer;
    Rep *_currentRep =[CoreDataHandler currentRep]; 
    NSNumber *_repID = _currentRep.repID;
    
    ASIHTTPRequest *request = [ConnectionManager createCustomerContactsConnectionForCustomer:customerProfileViewController.customer repID:_repID];
    request.delegate = self;
    self.currentContactRequest = request;
    
   /* [request setCompletionBlock:^{
        
        NSLog(@"Response:%@",[request responseString]);
        
        NSDictionary *jsonDict = [request responseJSON];
        
        jsonDict = NULL_NIL(jsonDict);
        
        NSArray *_contacts  = [jsonDict valueForKey:JSON_DETAILS_KEY];
        NSArray *updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
        NSArray *deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
        
       
        NSLog(@"Contacts:%@",_contacts);
        
        NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
        
        lastRequested = NULL_NIL(lastRequested);
        
        //[ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_CONTACTS];
        _customer.lastRequestedFromServer = lastRequested;
        
        [CustomerContact populateAsyncPerformingDelete:deletedIDs?deletedIDs:[NSArray array] update:updatedIDs?updatedIDs:[NSArray array] insertOnArray:_contacts onAttribute:@"contactID" completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error) 
         {
                //[customerProfileViewController loadContactsPeformingDelete:deletedResult update:updatedResult insert:insertedResult];
                //[profileInfoEditViewController loadContactsPeformingDelete:deletedResult update:updatedResult insert:insertedResult];
             
             NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
             [dict setObject:deletedResult  forKey:@"deleted"];
             [dict setObject:insertedResult forKey:@"inserted"];
             [dict setObject:updatedResult  forKey:@"updated"];
             
             [[NSNotificationCenter defaultCenter] postNotificationName:CustomerContactsDidLoad object:dict userInfo:nil];
             
             [dict release];

         }];
    }];*/


}

/*
- (void)requestFinished:(ASIHTTPRequest *)request
{
    Customer *_customer = customerProfileViewController.customer;
    NSLog(@"Response:%@",[request responseString]);
    
    NSDictionary *jsonDict = [request responseJSON];
    
    jsonDict = NULL_NIL(jsonDict);
    
    NSArray *_contacts  = [jsonDict valueForKey:JSON_DETAILS_KEY];
    NSArray *updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
    NSArray *deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
    
    
    NSLog(@"Contacts:%@",_contacts);
    
    NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
    
    lastRequested = NULL_NIL(lastRequested);
    
    //[ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_CONTACTS];
    
    NSNumber *_customerContactPrevRequest = _customer.lastRequestedFromServer ;
    _customer.lastRequestedFromServer = lastRequested;
    
    [CustomerContact populateAsyncPerformingDelete:deletedIDs?deletedIDs:[NSArray array] update:updatedIDs?updatedIDs:[NSArray array] insertOnArray:_contacts onAttribute:@"contactID" completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error) 
     {
    
         Customer *__customer = customerProfileViewController.customer;
         
         NSMutableArray *allContacts = [NSMutableArray array];
         if([insertedResult count]) [allContacts addObjectsFromArray:insertedResult];
         if([updatedResult count])  [allContacts addObjectsFromArray:updatedResult];
         
         for(CustomerContact * contact in allContacts)
         {
             if([__customer.contacts containsObject:contact]) continue;
             [__customer addContactsObject:contact];
         }

         
         NSError *_dbError = nil;
         [[SSCoreDataManager sharedManager] save:_dbError];
         
         [customerProfileViewController loadContactsPeformingDelete:deletedResult update:updatedResult insert:insertedResult];
         [profileInfoEditViewController loadContactsPeformingDelete:deletedResult update:updatedResult insert:insertedResult];
         
         if(error || _dbError)
         {
             _customer.lastRequestedFromServer = _customerContactPrevRequest;
         }
         
             }];
}
*/

- (void)requestFinished:(ASIHTTPRequest *)request
{
    Customer *_customer = customerProfileViewController.customer;
    NSLog(@"Response:%@",[request responseString]);
    
    NSDictionary *jsonDict = [request responseJSON];
    
    jsonDict = NULL_NIL(jsonDict);
    
    NSArray *_contacts  = [jsonDict valueForKey:JSON_DETAILS_KEY];
    //NSArray *updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
    NSArray *deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
    
    
    NSLog(@"Contacts:%@",_contacts);
    
    NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
    
    lastRequested = NULL_NIL(lastRequested);
    
    //[ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_CONTACTS];
    
    NSNumber *_customerContactPrevRequest = _customer.lastRequestedFromServer ;
    _customer.lastRequestedFromServer = lastRequested;
    
    [CustomerContact populateAsyncPerformingDelete:deletedIDs?deletedIDs:[NSArray array] update:nil insertOnArray:_contacts onAttribute:@"contactID" completion:^(NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
    {
        Customer *__customer = customerProfileViewController.customer;
        
        NSMutableArray *allContacts = [NSMutableArray array];
        if([insertedResult count]) [allContacts addObjectsFromArray:insertedResult];
        if([updatedResult count])  [allContacts addObjectsFromArray:updatedResult];
        
        NSNumber *_customerID = __customer.customerID;
        
        for(CustomerContact * contact in allContacts)
        {
            if(![contact isKindOfClass:[CustomerContact class]]) continue;
            
           
            if([contact.customerID longLongValue])
            {
                if([contact.customerID isEqualToNumber:_customerID] || ![__customer.isCustomerSentToServer boolValue])
                {
                    if(![__customer.contacts containsObject:contact])
                    {
                        //if(contact.customer)[contact.customer removeContactsObject:contact];
                        
                       [__customer addContactsObject:contact];
                    }
                }
                else if([__customer.isCustomerSentToServer boolValue])
                {
                    [__customer removeContactsObject:contact];
                }
            }
            else
            {
               if(![__customer.contacts containsObject:contact])
               {
                   contact.customerID = _customerID;
                  [__customer addContactsObject:contact];
               }
            }
        }
        
        
        NSError *_dbError = nil;
        [[SSCoreDataManager sharedManager] save:_dbError];
        
        [customerProfileViewController loadContactsPeformingDelete:nil update:updatedResult insert:insertedResult];
        [profileInfoEditViewController loadContactsPeformingDelete:nil update:updatedResult insert:insertedResult];
        
        if(error || _dbError)
        {
            _customer.lastRequestedFromServer = _customerContactPrevRequest;
        }

    }
    deletionBlock:^(NSMutableArray *deletedResult)
    {
        
    }];
}


- (void)requestFailed:(ASIHTTPRequest *)request
{
    
}

-(void)addCustomerSaveCallback:(CustomerSaveCallback)callback
{
    //if(callback)
    //[profileInfoEditViewController addCustomerSaveCallback:callback];
    
    __block CustomerSummaryViewController *weakSelf =self;
    [profileInfoEditViewController addCustomerSaveCallback:^(Customer *customer)
     {
          [weakSelf.customerSummaryDetailController reload];
         if(callback) callback(customer);
    }];
}

-(void)addCustomerCancelCallback:(CustomerCancelCallback)callback
{
    //if(callback)
      //  [profileInfoEditViewController addCustomerCancelCallback:callback];
    
    __block CustomerSummaryViewController *weakSelf =self;
    
    [profileInfoEditViewController addCustomerCancelCallback:^{
        
        [weakSelf.customerSummaryDetailController cancel];
        if(callback) callback();
    }];
}

-(void)setEditingContext:(NSManagedObjectContext *)aContext
{
     profileInfoEditViewController.editingContext = aContext;
     [customerSummaryDetailController setEditingContext:aContext];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    //NSString *customerName=customerProfileViewController.customer.name;
   // self.title = customerName.length ? customerName : @"Create New Customer";
    
    
    [self addBackButtonWithSelector:@selector(backAction:)];
    
    self.view.backgroundColor=[UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1];
    
   // if(isEditMode) profileInfoEditViewController.delegate = self;
    
    _parallelStackViewController   = [[ParallelStackViewController alloc]initWithMasterController:isEditMode? profileInfoEditViewController : customerProfileViewController];

    
     [self addChildViewController:_parallelStackViewController];
    
    _containerRect=CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
    
    _parallelStackViewController.view.frame = _containerRect;
    [self.view addSubview:_parallelStackViewController.view];
    [_parallelStackViewController didMoveToParentViewController:self];
    
   
    
    if(!customerSummaryDetailController)
    customerSummaryDetailController = [[CustomerSummaryDetailController alloc]initWithCustomerInfo:customerProfileViewController.customer];
    [customerSummaryDetailController setEditingContext:profileInfoEditViewController.editingContext];
    
    [_parallelStackViewController setDetailController:customerSummaryDetailController];
    [_parallelStackViewController showDetailController:NO];
    [customerSummaryDetailController enableEditMode:isEditMode];
    
    
   // [customerSummaryDetailController addDefaultBorder];    
    /// profileInfoEditViewController
    
    [self loadContacts];
    
}

-(void)profileInfoDidEditViewController:(ProfileInfoEditViewController *)controller didSaveCustomer:(Customer *)customer
{
    // [self enableEditMode:NO];
    
    [self.customerProfileController editAction];//
    [customerProfileViewController reloadData];
    
    [customerSummaryDetailController reload];
    
    //================================ Reload CustomersList ===========================
    UINavigationController *nextResponder = [self nextResponderNavigationController];
    CustomersViewController *customerVc= [[nextResponder viewControllers] objectAtIndex:0];
    
    if([customerVc isKindOfClass:[CustomersViewController class]])
    {
        [customerVc setupSectionDict];
        [customerVc.tableView reloadData];
    }
    //================================ Reload CustomersList ==============================
    
    if(customer.isUpdated)
    {
        NSError *error=nil;
    [[SSCoreDataManager sharedManager] save:error];

    [[OfflineManager sharedManager] addCustomer:customer isEditMode:YES];
    }

    
  
    
    // [[SSCoreDataManager sharedManager] saveContext:newContext error:error];
    
    
    //================= UPDATE TITLE OF NAVIGATION BAR ==============
    if(![[customer.name uppercaseString] isEqualToString:self.customerProfileController.title])
    {
        [self.customerProfileController addTitle:[customer.name uppercaseString] withFont:[UIFont subaruBoldFontOfSize:23.0]];
    }
    
}

-(void)profileInfoDidEditViewControllerDidCancel:(ProfileInfoEditViewController *)controller
{
    [self.customerProfileController editAction];
    //[customerSummaryDetailController cancel];
    
}

/*
-(void)backAction:(id)sender
{
    //Need to add back callback to customers
    [profileInfoEditViewController cancel];
   // [customerSummaryDetailController backAction];
    app.shouldAvoidMenuSelection=NO;
   // [self.navigationController popViewControllerAnimated:YES];    
}
*/


-(void)backAction:(id)sender
{
    if(self.profileInfoEditViewController.isNewCustomer)
    {
       // [self.profileInfoEditViewController customerDidCancelCallback];
        
     UIAlertView *_alert =  [UIAlertView alertViewWithTitle:@"" message:CUSTOMER_EXIT_MESSAGE cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObject:@"Yes"] onDismiss:^(int buttonIndex)
         {
               [profileInfoEditViewController cancel];
                app.shouldAvoidMenuSelection=NO;
             
             //[self.profileInfoEditViewController customerDidCancelCallback];
             
         }
       onCancel:^{
           
       }];
        
       [_alert release];
    }
    else
    {
        //Need to add back callback to customers
        [profileInfoEditViewController cancel];
        // [customerSummaryDetailController backAction];
        app.shouldAvoidMenuSelection=NO;
        // [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //[customerSummaryDetailController viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[SSCoreDataManager sharedManager] save:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
