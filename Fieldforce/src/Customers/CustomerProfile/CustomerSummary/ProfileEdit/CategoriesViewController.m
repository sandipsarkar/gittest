//
//  CategoriesViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 05/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CategoriesViewController.h"

#define TOTAL_CATEGORIES_COUNT 3

@interface CategoriesViewController()

@property(nonatomic,retain) NSArray *categoryArr;
@property(nonatomic,copy)CategoryDidSelectCallback categoryDidSelectCallback;
@end

@implementation CategoriesViewController
@synthesize categoryArr;
@synthesize categoryDidSelectCallback;
@synthesize delegate;

/*
+(NSArray *)categories
{
    NSString *path  = [[NSBundle mainBundle] pathForResource:@"Category" ofType:@"plist"];
        
    NSData *plistData = [NSData dataWithContentsOfFile:path];
        
    NSString *error; NSPropertyListFormat format;
    NSMutableDictionary *_catDict = [NSPropertyListSerialization propertyListFromData:plistData
                                                mutabilityOption:NSPropertyListImmutable
                                                          format:&format
                                                errorDescription:&error];
    if (!_catDict)
    {
        NSLog(@"Unable to read category data: %@", error);
    }
        
    NSArray *cats = [_catDict valueForKey:@"categories"];

    
    
    return cats;
}
*/


+(NSArray *)categories
{

    
    NSMutableArray *cats = [NSMutableArray array];
    
    for(int i=0; i<TOTAL_CATEGORIES_COUNT; i++)
    {
        NSString *catName = nil;
        NSInteger catID;
        
        switch (i)
        {
            case 0:
            {
//                catName = @"Collision";
//                catID = 2;
                
                catName = @"Mechanical";
                catID = 1;
            }
            break;
                
            case 1:
            {
//                catName = @"Mechanical";
//                catID = 1;
                
                catName = @"Collision";
                catID = 2;
            }
            break;
                
            case 2:
            {
               // catName = @"Collision & Mechanical";
                 catName = @"All";
                catID = 0;
            }
            break;
                
            case 3:
            {
                catName = @"Other";
                catID = 3;
            }
            break;
                
            default:
                break;
        }
        
        NSNumber *_catID = [NSNumber numberWithInteger:catID];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:catName,@"categoryName",_catID,@"categoryID",nil];
        
        [cats addObject:dict];
        
        [dict release];
        
    }
    
    
    
    return cats;
}


- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    if (self)
    {
        self.title = @"Select Business Type";
    }
    return self;
}

-(void)addCategoryDidSelectCallback:(CategoryDidSelectCallback)_categoryDidSelectCallback
{
    if(_categoryDidSelectCallback)
        self.categoryDidSelectCallback = _categoryDidSelectCallback;
}

-(void)dealloc
{
    delegate = nil;
    if(categoryDidSelectCallback) [categoryDidSelectCallback release];
    
    [categoryArr release] ;categoryArr = nil;
    [super dealloc];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    
    if(!self.categoryArr)
    self.categoryArr =[CategoriesViewController categories];
    
    self.tableView.bounces = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 15.0)];
        headerView.backgroundColor = [UIColor clearColor];
        self.tableView.tableHeaderView = headerView;
        [headerView release];
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if([self respondsToSelector:@selector(preferredContentSize)])
    {
      self.preferredContentSize = CGSizeMake(320.0, self.tableView.contentSize.height);
    }
    else
    {
        self.contentSizeForViewInPopover = CGSizeMake(320.0, self.tableView.contentSize.height);
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [self.categoryArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
    }
    
    NSDictionary *dict = (indexPath.row < [self.categoryArr count] ) ? [self.categoryArr objectAtIndex:indexPath.row] : nil;
    
    cell.textLabel.text = [dict valueForKey:@"categoryName"];
    
    
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor groupTableViewBackgroundColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.tag = 1;
    
    
    NSInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
    
    cell.textLabel.text = [NSString stringWithFormat:@"  %@",cell.textLabel.text];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *dict = (indexPath.row < [self.categoryArr count] ) ? [self.categoryArr objectAtIndex:indexPath.row] : nil;
  
    if(self.categoryDidSelectCallback) self.categoryDidSelectCallback(dict);
    
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(categoriesViewController: didSelectCategory:)])
    {
        [self.delegate categoriesViewController:self didSelectCategory:dict];
    }
}

@end
