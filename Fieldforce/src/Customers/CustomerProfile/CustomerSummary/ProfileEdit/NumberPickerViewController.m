//
//  NumberPickerViewController.m
//  RepVisitationTool
//
//  Created by Subhojit Dey on 11/26/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "NumberPickerViewController.h"

@interface NumberPickerViewController ()

@property(nonatomic,copy)NumberCallback numberCallback;

-(void)createPicker;
@end

@implementation NumberPickerViewController
@synthesize numberCallback;
@synthesize numberArr;
@synthesize selectedNumber;


- (id)initWithDataSource:(NSMutableArray *)dataSource
{
    self = [super init];
    
    if (self) 
    {
        self.numberArr = dataSource;
    }
    return self;
}

-(void)dealloc
{
    [pickerView release];
    [numberArr release] ; numberArr  = nil;
    [numberArr release] ; numberArr = nil;
    [selectedNumber release] ; selectedNumber = nil;
    
    [numberCallback release];
    numberCallback = nil;
    
    [super dealloc];
    
}

-(void)setNumberArr:(NSMutableArray *)numbers
{
    if(numberArr!=numbers)
    {
        [numberArr release];
        numberArr = [numbers retain];
        [pickerView reloadAllComponents];
    }
}
-(void)addNumberCallback:(NumberCallback)callBack
{
    
    if(callBack) self.numberCallback=callBack;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
    UIView *__view =  [[UIView alloc] initWithFrame:self.view.bounds];
    __view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:__view];
    [__view release];
    }
   
    [self createPicker];
    
    if(!numberArr)
    {
        numberArr=[[NSMutableArray alloc]init];
        [numberArr addObject:@"N/A"];
        
        for(int i=0;i<100;i++)
        {
            [numberArr addObject:[NSString stringWithFormat:@"%d",i+1]];
        }
    }
        
}

-(void)setSelectedNumber:(NSString *)aSelectedNumber
{
    if(aSelectedNumber!=selectedNumber)
    {
        [selectedNumber  release];
        selectedNumber = [aSelectedNumber retain];
        
        //if(self.selectedNumber.length)
        {
            NSInteger index = [self.numberArr indexOfObject:self.selectedNumber];
            
            if(index !=NSNotFound && index>=0)
            {
                [pickerView selectRow:index inComponent:0 animated:NO];
            }
            else if([self.numberArr count])
            {
                [pickerView selectRow:0 inComponent:0 animated:NO];
            }
        }
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.contentSizeForViewInPopover = CGSizeMake(320.0, CGRectGetMaxY(pickerView.frame));
    
    [pickerView reloadAllComponents];
}

-(void)createPicker
{
    pickerView = [[UIPickerView alloc]init];
    pickerView.delegate   = self;
    pickerView.dataSource = self;
    
    pickerView.showsSelectionIndicator = NO;
   // pickerView.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    [self.view addSubview:pickerView];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return  1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
     return [numberArr count];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 300.0;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40.0;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
	NSString *str = row < [numberArr count] ?  [numberArr objectAtIndex:row] : nil;
    NSLog(@"SELECTED:%@",str);
    
    self.selectedNumber = str;
    if(self.numberCallback)self.numberCallback(str);
    

}

/*
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *str = row < [numberArr count] ?  [numberArr objectAtIndex:row] : nil;
    
    return str;
}
*/

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view 
{
    UITableViewCell *cell = (UITableViewCell *)view;
    
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setBounds: CGRectMake(0, 0, cell.frame.size.width -20 , 44)];
       
        
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleSelection:)];
        singleTapGestureRecognizer.numberOfTapsRequired = 1;
        [cell addGestureRecognizer:singleTapGestureRecognizer];
        [singleTapGestureRecognizer release];
    }
    
    cell.userInteractionEnabled = YES;
    
     cell.tag = row;
    
     NSString *str = row < [numberArr count] ?  [numberArr objectAtIndex:row] : nil;
    cell.textLabel.text = str;
    
    return cell;
}

- (void)toggleSelection:(UITapGestureRecognizer *)recognizer
{
    int row = recognizer.view.tag;
    NSString *str = row < [numberArr count] ?  [numberArr objectAtIndex:row] : nil;
    
   // UITableViewCell *cell = (UITableViewCell *)(recognizer.view);
    
    self.selectedNumber = str;
    if(self.numberCallback)self.numberCallback(str);
}


// note: custom picker doesn't care about titles, it uses custom views

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
