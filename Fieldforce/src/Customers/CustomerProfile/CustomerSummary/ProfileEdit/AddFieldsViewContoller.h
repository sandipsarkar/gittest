//
//  AddFieldsViewContoller.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 04/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^WillSelectFieldCallback) (NSDictionary *fieldInfo,NSIndexPath *indexPath);
typedef void (^DidSelectFieldCallback) (NSIndexPath *indexPath);

@interface AddFieldsViewContoller : UITableViewController

@property(nonatomic,readonly) NSMutableArray * dataSource;

- (id)initWithOptionalFields:(NSMutableArray *)optionalFields;
-(void)setOptionalFields:(NSMutableArray *)optionalFields;

-(void)addDidSelectFieldCallback:(DidSelectFieldCallback)aDidSelectFieldCallback;
-(void)addWillSelectFieldCallback:(WillSelectFieldCallback)callback;
@end
