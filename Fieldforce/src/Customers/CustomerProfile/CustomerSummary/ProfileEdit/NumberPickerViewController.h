//
//  NumberPickerViewController.h
//  RepVisitationTool
//
//  Created by Subhojit Dey on 11/26/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ NumberCallback)(NSString *strNumber);


@interface NumberPickerViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource>
{
    UIPickerView *pickerView ;
}
@property(nonatomic,retain)NSMutableArray *numberArr;
@property(nonatomic,retain) NSString *selectedNumber;
-(void)addNumberCallback:(NumberCallback )callBack;

- (id)initWithDataSource:(NSMutableArray *)dataSource;

@end
