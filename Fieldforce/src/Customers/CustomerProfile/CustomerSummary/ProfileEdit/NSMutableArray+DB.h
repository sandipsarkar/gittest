//
//  NSMutableArray+Chunk.h
//  Locator
//
//  Created by Soumya Maitra on 23/12/10.
//  Copyright 2010 Randem[IT] India. All rights reserved.
//

/*
 This a Lacal Data Base built with the concept of NSMutableArray category.
 We made a category of NSMutableArray so that we can handle all data came form remote server.
 We will set new data to local dictionary and edit it.
 
 Methods:
 - (NSMutableArray *) addDistanceForArray: (NSMutableArray *) a_array;
 - (NSMutableArray *) sortByDistanceForArray: (NSMutableArray *) a_array;
 */

#define INVALID_DISTANCE	9999999999
#define NS_MUTABLE_ARRAY_DEBUG NO
#define DISTANCE @"DISTANCE"
#define SUBURB_DISTANCE @"SUBURB_DISTANCE"
#import <Foundation/Foundation.h>

typedef enum
{
	previousChunk = 0,
	firstChunk,
	nextChunk,
} control;

@interface NSMutableArray(DB)
- (NSMutableArray *) sortedArrayByID;
- (NSMutableArray *) sortedArrayByDistance;
- (NSMutableArray *) sortedArrayBySuburbDistance;
- (NSMutableArray *) arrayByAddingDistanceWithLatTag: (NSString *) latTag  WithLongTag: (NSString *) longTag;
- (NSMutableArray *) arrayBySuburbAddingDistanceWithLatTag: (NSString *) latTag  WithLongTag: (NSString *) longTag FromLat: (NSString *) fromLat  FromLong: (NSString *) fromLong;
- (BOOL ) isValidLatitude:(NSString *)latitudeStr;
- (BOOL ) isValidLongitude:(NSString *)longitudeStr;

- (void) manageChunkSize: (int) size;
- (void) initializeChunk;
- (void) updatePagingWithStartIndex:(int) sIndex endIndex:(int) eIndex pageSize: (int) pSize AndIsNext: (BOOL) iNext;

- (int) returnPageNo;
- (int) returnPageSize;
- (int) returnStartIndex;
- (int) returnEndIndex;
- (BOOL) returnIsNext;

- (NSMutableArray* ) arrayChunkOfSize:(int) size andControl:(control) state;
- (NSMutableArray* ) first;
- (NSMutableArray* ) next;
- (NSMutableArray* ) previous;
- (BOOL) isNext;
- (BOOL) isPrevious;

- (void) setAllIndex;
- (int) showStartIndex;
- (int) showEndIndex;
@end