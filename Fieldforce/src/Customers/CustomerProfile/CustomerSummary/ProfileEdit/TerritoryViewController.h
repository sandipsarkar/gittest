//
//  TerritoryViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 04/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Territory;
typedef void (^TerritorySelectedCallback)(Territory  *territory);

@protocol TerritoryDelegate;
@interface TerritoryViewController : UITableViewController


@property(nonatomic,assign) id < TerritoryDelegate > delegate;

+(NSArray *)territories;
+(NSArray *)territoriesForRep:(Rep *)rep;

-(void)addTerritorySelectedCallback:(TerritorySelectedCallback)aTerritorySelectedCallback;

@end



@protocol TerritoryDelegate <NSObject>

@optional

-(void)territoryViewController:(TerritoryViewController *)territoryViewController didSelectTerritory:(Territory  *)territory;

@end
