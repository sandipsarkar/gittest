//
//  StatesViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 04/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "StatesViewController.h"

@interface StatesViewController ()

@property(nonatomic,retain) NSArray *statesName;
@property(nonatomic,retain) NSIndexPath *selectedIndexPath;
@property(nonatomic,copy) StateSelectedCallback stateSelectedCallback;
@end

@implementation StatesViewController
@synthesize statesName;
@synthesize selectedIndexPath;
@synthesize stateSelectedCallback;

- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    if (self)
    {
        self.title = @"Select State";
    }
    return self;
}

-(void)dealloc
{
    
    [selectedIndexPath release],selectedIndexPath=nil;
    [statesName release];
    [stateSelectedCallback release] , stateSelectedCallback = nil;
    
    [super dealloc];
}

+(NSArray *)states
{
    NSString *statePlistPath=[[NSBundle mainBundle] pathForResource:@"States" ofType:@"plist"];
    NSDictionary *dict=[NSDictionary dictionaryWithContentsOfFile:statePlistPath];
    
    if(!dict) return nil;
    
    return [dict valueForKey:@"States"];

}

-(void)addStateSelectedCallback:(StateSelectedCallback)aStateSelectedCallback
{
    if(aStateSelectedCallback) self.stateSelectedCallback = aStateSelectedCallback;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    //[self addTitle:self.title withFont:[UIFont subaruBoldFontOfSize:23.0]];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.tableView.bounces = NO;
    
    self.statesName = [StatesViewController states];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.contentSizeForViewInPopover = CGSizeMake(320.0, MAX(300.0,self.tableView.contentSize.height));
}

- (void)viewDidUnload
{
    [super viewDidUnload];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.statesName count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *stateDict = (indexPath.row < [self.statesName count]) ? [self.statesName objectAtIndex:indexPath.row] : nil;
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }

  
    cell.textLabel.text =[NSString stringWithFormat:@"%@ (%@)",[stateDict valueForKey:@"NAME"], [stateDict valueForKey:@"SHORT_NAME"]];
    
    cell.accessoryType = [self.selectedIndexPath isEqual:indexPath] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.selectedIndexPath isEqual:indexPath]) return;
    
    NSMutableArray *indexPathsToReload=[NSMutableArray array];
    
    if(self.selectedIndexPath) [indexPathsToReload addObject:self.selectedIndexPath];
    
    [indexPathsToReload addObject:indexPath];
    
    [tableView reloadRowsAtIndexPaths:indexPathsToReload withRowAnimation:UITableViewRowAnimationFade];
    
    NSDictionary *stateDict = (indexPath.row < [self.statesName count]) ? [self.statesName objectAtIndex:indexPath.row] : nil;
    if(self.stateSelectedCallback) self.stateSelectedCallback( [stateDict valueForKey:@"SHORT_NAME"]);
    
}

@end
