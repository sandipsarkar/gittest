//
//  AddFieldsViewContoller.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 04/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "AddFieldsViewContoller.h"

@interface AddFieldsViewContoller ()
{
    NSMutableArray *dataSource;
    UILabel *noFieldsLabel;
}
@property(nonatomic,copy)DidSelectFieldCallback didSelectFieldCallback;
@property(nonatomic,copy) WillSelectFieldCallback willSelectFieldCallback;

-(void)addDidSelectFieldCallback:(DidSelectFieldCallback)aDidSelectFieldCallback;
@end

@implementation AddFieldsViewContoller
@synthesize didSelectFieldCallback;
@synthesize willSelectFieldCallback;
@synthesize dataSource;

- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self)
    {
        self.title = @"Add Field";
        dataSource = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)dealloc
{
    [dataSource release];
    
    if(willSelectFieldCallback) [willSelectFieldCallback release];
    if(didSelectFieldCallback)  [didSelectFieldCallback release];
    
    [super dealloc];
}

- (id)initWithOptionalFields:(NSMutableArray *)optionalFields
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self)
    {
        self.title = @"Add Field";
        dataSource = [optionalFields retain];
    }
    return self;
}

-(void)setOptionalFields:(NSMutableArray *)optionalFields
{
       if(dataSource)[dataSource release];
        dataSource = [optionalFields retain];
        [self.tableView reloadData];
    
}

-(void)addDidSelectFieldCallback:(DidSelectFieldCallback)aDidSelectFieldCallback
{
    if(aDidSelectFieldCallback)
      self.didSelectFieldCallback = aDidSelectFieldCallback;
}

-(void)addWillSelectFieldCallback:(WillSelectFieldCallback)callback
{
    if(callback)
        self.willSelectFieldCallback = callback;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    //[self addTitle:self.title withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    self.tableView.bounces = NO;
    
    CGSize popoverSize = self.contentSizeForViewInPopover;
    CGRect labelFrame = CGRectMake((CGRectGetWidth(self.view.bounds)-600.0)/2.0, (CGRectGetHeight(self.view.bounds)-popoverSize.height)/2.0, 600.0, popoverSize.height);
    noFieldsLabel = [UILabel labelWithText:@"No more fields to add" frame:labelFrame textColor:[UIColor blackColor] bgColor:[UIColor clearColor] fontsize:15.0];
    noFieldsLabel.font = [UIFont grotesqueBoldFontOfSize:18.0];
    noFieldsLabel.textAlignment = UITextAlignmentCenter;
    noFieldsLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:noFieldsLabel];
    noFieldsLabel.hidden = YES;
    
    
   // [self.tableView reloadData];
    
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.popoverController.popoverContentSize = CGSizeMake(320.0,MAX(self.tableView.rowHeight+44.0,self.tableView.contentSize.height+44.0));
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger count = [dataSource count];
    noFieldsLabel.hidden = count>0;
    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    NSMutableArray *rowsInfo =(section<[dataSource count]) ?  [dataSource objectAtIndex:section] : nil;
    return MAX(1,[rowsInfo count]);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSMutableArray *rowsInfo = [dataSource objectForKey:[NSString stringWithFormat:@"%d",indexPath.section]];
    
    NSMutableArray *rowsInfo =(indexPath.section<[dataSource count]) ?  [dataSource objectAtIndex:indexPath.section] : nil;

    return [rowsInfo count] ? tableView.rowHeight :0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   // NSMutableArray *rowsInfo = [dataSource objectForKey:[NSString stringWithFormat:@"%d",indexPath.section]];
    
    static NSString *CellIdentifier = @"Cell";
        
    NSMutableArray *rowsInfo =(indexPath.section<[dataSource count]) ?  [dataSource objectAtIndex:indexPath.section] : nil;
    
    NSMutableDictionary *dict = (indexPath.row<[rowsInfo count]) ? [rowsInfo objectAtIndex:indexPath.row] : nil;
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }

    cell.textLabel.text = [dict valueForKey:@"title"];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //NSString *key=[NSString stringWithFormat:@"%d",indexPath.section];
    //NSMutableArray *rowsInfo  = [dataSource objectForKey:key];
    
    NSMutableArray *rowsInfo =(indexPath.section<[dataSource count]) ?  [dataSource objectAtIndex:indexPath.section] : nil;
    NSMutableDictionary *dict = (indexPath.row<[rowsInfo count]) ? [rowsInfo objectAtIndex:indexPath.row] : nil;
    
    if(self.willSelectFieldCallback) self.willSelectFieldCallback(dict,indexPath);
    
    if([rowsInfo count])
    {
       [rowsInfo removeObjectAtIndex:indexPath.row];
        
        if([rowsInfo count])
        {
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        else
        {
            [dataSource removeObjectAtIndex:indexPath.section];
            
            [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationTop];
        }
    }
    
     if(self.didSelectFieldCallback) self.didSelectFieldCallback(indexPath);

}

@end
