//
//  NSMutableArray+Chunk.m
//  Locator
//
//  Created by Soumya Maitra on 23/12/10.
//  Copyright 2010 Randem[IT] India. All rights reserved.
//

#include <math.h>
#import "NSMutableArray+DB.h"
#import <CoreLocation/CoreLocation.h>    //<--For Location
#import "LocatorAppDelegate.h"

@implementation NSMutableArray(DB)

#pragma mark Constants
BOOL isNext	= YES;
static int pageNo = 0;			//Page Number
static int startIndex = 0;		//Starting Index Number
static int endIndex = 0;		//End Index Number
static int pageSize = 0;		//Each Page Size

static int _startIndex = 0;		//Starting Index Number
static int _endIndex = 0;		//End Index Number
#pragma mark -


#pragma mark Shorting
- (NSMutableArray *) sortedArrayByID
{
	NSMutableArray *arr=[[NSMutableArray arrayWithArray:self] retain];
	int c  = [arr count];
	if(c)
	{
		NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:TAG_ID ascending:YES] ;
		NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
		[arr  sortUsingDescriptors:sortDescriptors]; 
		if(NS_MUTABLE_ARRAY_DEBUG)  NSLog(@"RETURN SORTED ARRAY OF ID%@",arr);
		[sortDescriptor release];
		return [arr autorelease];
	}
	else
	{
		[arr release];
		return nil;
	}
	
}

- (NSMutableArray *) sortedArrayByDistance
{
	NSMutableArray *arr=[[NSMutableArray arrayWithArray:self] retain];
	int c  = [arr count];
	if(c)
	{
		NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:DISTANCE ascending:YES] ;
		NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
		[arr  sortUsingDescriptors:sortDescriptors]; 
		if(NS_MUTABLE_ARRAY_DEBUG)  NSLog(@"RETURN SORTED ARRAY %@",arr);
		[sortDescriptor release];
		return [arr autorelease];
	}
	else
	{
		[arr release];
		return nil;
	}
		
}

- (NSMutableArray *) sortedArrayBySuburbDistance
{
	NSMutableArray *arr=[[NSMutableArray arrayWithArray:self] retain];
	int c  = [arr count];
	if(c)
	{
		NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:SUBURB_DISTANCE ascending:YES] ;
		NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
		[arr  sortUsingDescriptors:sortDescriptors]; 
		if(NS_MUTABLE_ARRAY_DEBUG)  NSLog(@"RETURN SORTED ARRAY %@",arr);
		[sortDescriptor release];
		return [arr autorelease];
	}
	else
	{
		[arr release];
		return nil;
	}
	
}

#pragma mark -

#pragma mark Adding Distance
- (NSMutableArray *) arrayByAddingDistanceWithLatTag: (NSString *) latTag  WithLongTag: (NSString *) longTag
{
	//get data dictionary from  outlets array
	//find lat long 
	//find distance
	//put in inside the dictionary using a tag
	int c  = [self count];
	//Distance
	if(c)
	{
		NSMutableArray *returnArray = [NSMutableArray array] ;
        CLLocation *fromLocation = [[CLLocation alloc] initWithLatitude:app.myCurrentLat  longitude:app.myCurrentLong ]; 

		for (int i =0; i<c; i++) 
		{
			NSMutableDictionary *outlet =  [[self objectAtIndex:i] retain];
			NSString *latStr = [outlet objectForKey:latTag];
			NSString *longStr = [outlet objectForKey:longTag];
			if( [self isValidLatitude: latStr] && [self isValidLongitude: longStr ] )
			{
				CLLocation *toLocation = [[CLLocation alloc] initWithLatitude:[latStr  doubleValue] longitude:[longStr doubleValue] ];
				///NSLog(@"From Location lat %lf lang %lf To lat %lf long %lf",app.myCurrentLat, app.myCurrentLong, [[outlet objectForKey:latTag]  doubleValue], [[outlet objectForKey:longTag]  doubleValue] );
				CLLocationDistance distance = 0.0;
				#if __IPHONE_OS_VERSION_MIN_REQUIRED <= __IPHONE_3_2
								distance = [fromLocation getDistanceFrom:toLocation];
				#else
								distance = [fromLocation distanceFromLocation:toLocation];
				#endif
				//Distance found as meter and we set it as km
				[outlet setObject:[NSNumber numberWithDouble:distance/1000] forKey:DISTANCE]; 
				[toLocation release];
				[returnArray addObject:outlet];
			}
			[outlet release];
		}
        [fromLocation release];
		if(NS_MUTABLE_ARRAY_DEBUG)  NSLog(@"RETURN ARRAY %@",returnArray);
		return returnArray;
	}
	else
		return nil;
}

- (NSMutableArray *) arrayBySuburbAddingDistanceWithLatTag: (NSString *) latTag  WithLongTag: (NSString *) longTag FromLat: (NSString *) fromLat  FromLong: (NSString *) fromLong
{
	//get data dictionary from  outlets array
	//find lat long 
	//find distance
	//put in inside the dictionary using a tag
	int c  = [self count];
	//Distance
	if(c)
	{
		NSMutableArray *returnArray = [NSMutableArray array] ;
        CLLocation *fromLocation = [[CLLocation alloc] initWithLatitude:[fromLat doubleValue]  longitude:[fromLong doubleValue] ]; 

		for (int i =0; i<c; i++) 
		{
			NSMutableDictionary *outlet =  [[self objectAtIndex:i] retain];
			NSString *latStr = [outlet objectForKey:latTag];
			NSString *longStr = [outlet objectForKey:longTag];
			if( [self isValidLatitude: latStr] && [self isValidLongitude: longStr ] )
			{
                CLLocation *toLocation = [[CLLocation alloc] initWithLatitude:[latStr  doubleValue] longitude:[longStr doubleValue] ];
				CLLocationDistance distance = 0.0;
#if __IPHONE_OS_VERSION_MIN_REQUIRED <= __IPHONE_3_2
				distance = [fromLocation getDistanceFrom:toLocation];
#else
				distance = [fromLocation distanceFromLocation:toLocation];
#endif
				//Distance found as meter and we set it as km
				[outlet setObject:[NSNumber numberWithDouble:distance/1000] forKey:SUBURB_DISTANCE]; 
				[returnArray addObject:outlet];
                [toLocation release];
			}
			[outlet release];
		}        
        [fromLocation release];
		if(NS_MUTABLE_ARRAY_DEBUG)  NSLog(@"RETURN ARRAY %@",returnArray);
		return returnArray;
	}
	else
		return nil;
}

#pragma -

#pragma mark Checks a latitude is valid
- (BOOL ) isValidLatitude:(NSString *)latitudeStr
{
	if(!latitudeStr.length) return NO;
	double latitude=[latitudeStr doubleValue];
	return (latitude > -90.0f && latitude < 90.0f); // || latitude!=0.0f
}
#pragma mark -

#pragma mark Checks a longitude is valid
- (BOOL ) isValidLongitude:(NSString *)longitudeStr
{
	if(!longitudeStr.length) return NO;
	double longitude=[longitudeStr doubleValue];
	return (longitude > -180.0f && longitude < 180.0f); //) || longitude!=0.0f
}
#pragma mark -


#pragma mark Array Paging Set Up
- (void) manageChunkSize: (int) size
{
	int c = [self count];
	int modifiedSize = (size<=c) ?size :c;
	pageNo = (modifiedSize>0) ?ceil( (float) c/modifiedSize ) :0;
	startIndex = 0;
	endIndex = 0;
	[self setAllIndex];
	pageSize = size;
	isNext = YES;
}
#pragma mark -

#pragma mark Array Paging Update
- (void) updatePagingWithStartIndex:(int) sIndex endIndex:(int) eIndex pageSize: (int) pSize AndIsNext: (BOOL) iNext
{
	startIndex = sIndex;
	endIndex = eIndex;
	pageSize = pSize;
	isNext = iNext;
}
#pragma mark -

#pragma mark Array Paging Initialization
- (void) initializeChunk
{
	startIndex = 0;
	endIndex = 0;
	[self setAllIndex];
	isNext = YES;
}
#pragma mark -

#pragma mark The Returns
- (int) returnPageSize
{
	return pageSize;
}

- (int) returnPageNo
{
	return pageNo;
}

- (int) returnStartIndex
{
	return startIndex;
}

- (int) returnEndIndex
{
	return endIndex;
}

- (BOOL) returnIsNext
{
	return isNext;
}

- (void) setAllIndex
{
	_startIndex = startIndex;
	_endIndex = endIndex;
	if(DEVELOPER_DEBUG)NSLog(@"Saved start Index =%d End Index =%d", _startIndex, _endIndex);
}
- (int) showStartIndex
{
	return _startIndex;
}
- (int) showEndIndex
{
	return _endIndex;
}
#pragma mark -

#pragma mark Main Paging method
- (NSMutableArray* ) arrayChunkOfSize:(int) size andControl:(control) state
{
	if(size <= 0) return nil;
	int c = [self count];
	if( c  && size )
	{
		size = size>c ? c: size;
		NSArray * returnArray = nil;
		if(state == firstChunk && ( startIndex == 0) )
		{
			if(!isNext) startIndex += size<c ?size :0 ;
			int k = startIndex+size-1;
			endIndex = k<c?k:c-1;
			int length = abs(endIndex-startIndex)+1;
			NSRange arrayRange = NSMakeRange ( 0, length ); 
			returnArray = [self subarrayWithRange:arrayRange];
			if(NS_MUTABLE_ARRAY_DEBUG) NSLog(@"First  Found From %d To %d",startIndex, endIndex);
			[self setAllIndex];

			
			isNext = NO;
		}
		if(state == nextChunk && ( startIndex < c ) && ( endIndex >= 0 ) ) 
		{
			if(!isNext) startIndex += size<c ?size :0 ;
			int k = startIndex+size-1;
			endIndex = k<c?k:c-1;
			
			int length = abs(endIndex-startIndex)+1;
			NSRange arrayRange = NSMakeRange ( startIndex, length ); 
			returnArray = [self subarrayWithRange:arrayRange];
			
			if(NS_MUTABLE_ARRAY_DEBUG) NSLog(@"Next  Found From %d To %d",startIndex, endIndex);
			[self setAllIndex];
			
			startIndex = endIndex+1;
			isNext = YES;
		}
		if(state == previousChunk && ( startIndex > 0 ) && pageSize < c ) 
		{
			int s=endIndex-size;
			endIndex = s>=size ?endIndex-size : (size<c?size-1:c-1) ;
			int k = endIndex-size+1;
			startIndex= k>0 ?k :0 ;
			
			int length = abs(endIndex-startIndex)+1;
			NSRange arrayRange = NSMakeRange ( startIndex, length ); 
			returnArray = [self subarrayWithRange:arrayRange];
			
			if(NS_MUTABLE_ARRAY_DEBUG)  NSLog(@"Prev  Found From %d To %d",startIndex, endIndex);
			[self setAllIndex];
			
			isNext = NO;
		}
		return [[[NSMutableArray arrayWithArray:returnArray] retain] autorelease];
	}
	else
		return nil;	
}


- (NSMutableArray* ) first
{
	return  [self arrayChunkOfSize:pageSize andControl:firstChunk];
}


- (NSMutableArray* ) next
{
	return  [self arrayChunkOfSize:pageSize andControl:nextChunk];
}

- (NSMutableArray* ) previous
{
	return  [self arrayChunkOfSize:pageSize andControl:previousChunk];
}

- (BOOL) isNext
{
	if( startIndex >= 0 && startIndex< [self count] && pageSize >0 )
	{
		if(NS_MUTABLE_ARRAY_DEBUG)  NSLog(@"NEXT------------YES");
		return YES;
	}
	else 
	{
		if(NS_MUTABLE_ARRAY_DEBUG)  NSLog(@"NEXT------------NO");
		return NO;
	}
}

- (BOOL) isPrevious
{
	if( startIndex > 0 && pageSize < [self count] )
	{
		if(NS_MUTABLE_ARRAY_DEBUG)  NSLog(@"PREV------------YES");
		return YES;
	}
	else 
	{
		if(NS_MUTABLE_ARRAY_DEBUG)  NSLog(@"PREV------------NO");
		return NO;
	}
}
#pragma mark -
@end