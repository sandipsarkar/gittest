//
//  CategoriesViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 05/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CategoryDidSelectCallback) (NSDictionary *category);

@protocol CategoriesViewDelegate;
@interface CategoriesViewController : UITableViewController

@property(nonatomic,assign) id < CategoriesViewDelegate > delegate;

+(NSArray *)categories;
-(void)addCategoryDidSelectCallback:(CategoryDidSelectCallback)_categoryDidSelectCallback;

@end


@protocol CategoriesViewDelegate <NSObject>

@optional

-(void)categoriesViewController:(CategoriesViewController *)categories didSelectCategory:(NSDictionary *)category;

@end
