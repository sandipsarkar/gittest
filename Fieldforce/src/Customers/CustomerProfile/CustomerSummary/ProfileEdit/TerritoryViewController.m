//
//  TerritoryViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 04/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "TerritoryViewController.h"
#import "Rep.h"
#import "Territory.h"

@interface TerritoryViewController ()

@property(nonatomic,retain) NSMutableArray *territoryArr;
@property(nonatomic,copy) TerritorySelectedCallback territorySelectedCallback;
@end

@implementation TerritoryViewController
@synthesize territoryArr;
@synthesize territorySelectedCallback;
@synthesize delegate;

- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self)
    {
       self.title = @"Select Territory";
    }
    return self;
}

+(NSArray *)territories
{
    NSMutableArray *_arr  = [Territory fetchAll:nil];
    
    if(![_arr count])
    {
      _arr=[NSMutableArray arrayWithCapacity:10];
    for(int i=1;i<11;i++)
    {
        Territory *territory =(Territory *)[Territory insertNewObject];
        territory.territoryPosition = [NSNumber numberWithInt:i];
        [_arr addObject:territory];
    }
    }
    
    return _arr;
}

+(NSArray *)territoriesForRep:(Rep *)rep
{
    return rep.territories.allObjects;
}

-(void)addTerritorySelectedCallback:(TerritorySelectedCallback)aTerritorySelectedCallback
{
    if(aTerritorySelectedCallback)
        self.territorySelectedCallback  = aTerritorySelectedCallback;
}

-(void)dealloc
{
    delegate = nil;
    
    [territoryArr release];
    if(territorySelectedCallback) [territorySelectedCallback release];
    
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    //self.navigationController.navigationBar.barTintColor = LOGIN_BG_COLOR;
    
    //[self addTitle:self.title withFont:[UIFont subaruBoldFontOfSize:23.0]];

    
    self.tableView.bounces = NO;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 10.0)];
        headerView.backgroundColor = [UIColor clearColor];
        self.tableView.tableHeaderView = headerView;
        [headerView release];
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    

   // self.territoryArr = [TerritoryViewController territories];

    Rep *currentRep = [CoreDataHandler currentRep];
    NSArray *_territoryArr = [TerritoryViewController territoriesForRep:currentRep];
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"territoryAbbreviation" ascending:YES];
    _territoryArr=[_territoryArr sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    self.territoryArr = [NSMutableArray arrayWithArray:_territoryArr];
    [descriptor release];
    

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return MAX(1,[territoryArr count]);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier  = @"Cell";
    static NSString *CellIdentifier2 = @"Cell_nil";
    
    UITableViewCell *cell=nil;
    
    if([territoryArr count])
    {
       cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
      if (cell == nil)
         {
           cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
           cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
           
         }
    
        // NSString *territoryString = (indexPath.row < [territoryArr count]) ? [territoryArr objectAtIndex:indexPath.row] : @"";
        
        /*
        if(indexPath.row==0)
        {
           cell.textLabel.text = @"N/A";
        }
        else 
        {
            
         Territory *territory = (indexPath.row-1>=0 && indexPath.row-1 < [territoryArr count]) ? [territoryArr objectAtIndex:indexPath.row-1] : nil;
    
        cell.textLabel.text = [territory.territoryPosition description];
        }
        */
        
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        
        Territory *territory = (indexPath.row < [territoryArr count]) ? [territoryArr objectAtIndex:indexPath.row] : nil;
        
       // cell.textLabel.text = [territory.territoryPosition description];
        cell.textLabel.text = territory.territoryAbbreviation;
        
    }
    else 
    {
       cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        
        if (cell == nil)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.textColor = [UIColor grayColor];
            cell.textLabel.font = [UIFont subaruBoldFontOfSize:20.0];
            cell.textLabel.text = @"NO TERRITORY";
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor groupTableViewBackgroundColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.tag = 1;
    
    
    int numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
    
    cell.textLabel.text = [NSString stringWithFormat:@"  %@",cell.textLabel.text];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(![territoryArr count]) return;
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    /*
    if(indexPath.row==0)
    {
        self.territorySelectedCallback(nil);
    }
    else 
    {
       Territory *territory = (indexPath.row-1>= 0 && indexPath.row-1 < [territoryArr count]) ? [territoryArr objectAtIndex:indexPath.row-1] : nil;
        
      self.territorySelectedCallback(territory);
    }
    */
    
    Territory *territory = (indexPath.row < [territoryArr count]) ? [territoryArr objectAtIndex:indexPath.row] : nil;
    
    if(self.territorySelectedCallback)
    self.territorySelectedCallback(territory);
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(territoryViewController: didSelectTerritory:)])
    {
        [self.delegate territoryViewController:self didSelectTerritory:territory];
    }
}

@end
