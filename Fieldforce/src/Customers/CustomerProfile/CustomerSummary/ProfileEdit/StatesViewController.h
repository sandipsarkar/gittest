//
//  StatesViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 04/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^StateSelectedCallback)(NSString  *stateName);
@interface StatesViewController : UITableViewController


+(NSArray *)states;
-(void)addStateSelectedCallback:(StateSelectedCallback)aStateSelectedCallback;
@end
