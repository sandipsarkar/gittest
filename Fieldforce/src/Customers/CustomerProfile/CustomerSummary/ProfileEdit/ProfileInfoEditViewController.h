//
//  ProfileInfoEditViewController.h
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Customer,ProfileInfoEditViewController;

typedef void (^CustomerSaveCallback) (Customer *customer);
typedef void (^CustomerCancelCallback) ();

@protocol ProfileInfoEditDelegate <NSObject>

@optional

-(void)profileInfoDidEditViewController:(ProfileInfoEditViewController *)controller didSaveCustomer:(Customer *)customer;
-(void)profileInfoDidEditViewControllerDidCancel:(ProfileInfoEditViewController *)controller;

@end


@interface ProfileInfoEditViewController : UITableViewController
{
    int addedSection;
    NSMutableArray *arrOEM;
    int swipedSection;
}

@property (nonatomic,assign) Customer *customer;
@property (nonatomic,assign) BOOL isNewCustomer;
@property (nonatomic,assign) NSManagedObjectContext *editingContext;
@property (nonatomic,assign) id <ProfileInfoEditDelegate> delegate;

-(id)initWithCustomer:(Customer *)aCustomer;

-(void)addCustomerSaveCallback:(CustomerSaveCallback)callback;
-(void)addCustomerCancelCallback:(CustomerCancelCallback)callback;

-(void)reloadData;
-(void)save;
-(void)cancel;

-(void)loadContactsPeformingDelete:(NSArray *)deleteArr update:(NSArray *)updateArr insert:(NSArray *)insertArr;

-(void)customerDidCancelCallback;

@end
