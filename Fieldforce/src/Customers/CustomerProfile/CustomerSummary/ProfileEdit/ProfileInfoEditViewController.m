//
//  ProfileInfoEditViewController.m
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "ProfileInfoEditViewController.h"
#import "Customer.h"
#import "OEM.h"
#import "Rep.h"
#import "WholesaleDealer.h"
#import "Territory.h"
#import "AddFieldsViewContoller.h"
#import "TerritoryViewController.h"
#import "Territory.h"
#import "CategoriesViewController.h"
#import "StatesViewController.h"
#import "KTTextView.h"
#import "CustomerContact.h"
#import "NumberPickerViewController.h"
#import "AddressViewController.h"
#import "InsetTableViewCell.h"

//#define DUPLICATE_CUSTOMER_PREDICATE_FORMAT @"self.name = [c]%@ AND (self.mainContactNo=%@ OR self.email= [c]%@ OR self.address = [c]%@)"

#define DUPLICATE_CUSTOMER_PREDICATE_FORMAT @"self.name = [c]%@ AND (((self.mainContactNo.length!=0 AND self.mainContactNo=%@) OR (self.email.length!=0 AND self.email= [c]%@)) OR (self.address.length!=0 AND self.address = [c]%@))"


#define MAX_TEXT_LENGTH 45
#define DATADICT_TITLE_KEY @"title"
#define DATADICT_TYPE_KEY @"type"
#define CONTACT_SECTION_START 6 //6
#define CONTACT_SECTION_END 206 // 23

@interface ProfileInfoEditViewController()<UITextFieldDelegate,UITextViewDelegate,AddressViewDelegate,TerritoryDelegate,CategoriesViewDelegate>
{
    BOOL _isNewCustomer;
    NSMutableArray *dataSource;
    NSMutableArray *optionalFields;
    UIButton *addFieldButton;
    NSMutableDictionary *textfieldContainerDict;
    
    BOOL _isNotesExpanded;
    
    CGFloat _textViewHeight;
    
    NSArray *dealingOEMList;
    NSMutableArray *notDealingOEMList;
    NSMutableArray *selectedOEMs;
    int numberOfContacts;
    int _tolContacts;
    
    NSMutableArray *_newContactSections;
    NSMutableArray *_addedContactSections;
    NSMutableSet *_deletedContacts;
    
  
    NSInteger _currentContactSection;
    
    BOOL _allowSave;
    BOOL _isKeyboardAppeared;
    
    BOOL _shouldScrollToSelectedField;
    
    BOOL _isKeyboardNotificationRegistered;

}

@property (nonatomic,copy)   CustomerSaveCallback   customerSaveCallback;
@property (nonatomic,copy)   CustomerCancelCallback customerCancelCallback;
@property (nonatomic,retain) UIPopoverController *addFieldPopover;
@property (nonatomic,retain) UIPopoverController *territoryPopover;
@property (nonatomic,retain) UIPopoverController *statePopover;
@property (nonatomic,retain) UIPopoverController *categoryPopover;
@property (nonatomic,retain) UIPopoverController *numberPickerPopover;
@property (nonatomic,retain) UIPopoverController *suburbPopover;

@property (nonatomic,retain) NSMutableArray *addedCustomerContacts;

@property(nonatomic,copy)    NSString *categoryName;
@property(nonatomic,copy)    NSNumber *categoryID;
@property (nonatomic,copy)   NSString *customerNotes;
@property (nonatomic,copy)   NSString *workshopBays;
@property (nonatomic,copy)   NSString *repairsPerWeek;
//@property (nonatomic,retain) NSNumber *customerTerritory;
@property (nonatomic,retain) NSNumber *customerTerritoryID;
@property (nonatomic,retain) NSString *customerTerritoryAbbreviation;
@property (nonatomic,retain) NSNumber *isGenuineMember;
@property (nonatomic,retain) NSString *customerMembership;
@property (nonatomic,copy)   NSString *customerState;
@property (nonatomic,copy)   NSString *customerSuburb;
@property (nonatomic,copy)   NSString *customerPostCode;

@property (nonatomic,retain) UITextField *selectedTextField;

@property (nonatomic,retain) NSIndexPath *theNewContactLastIndexPath;

-(void)selectCategoyForIndexPath:(NSIndexPath *)indexPath;
-(void)selectTerritoryForIndexPath:(NSIndexPath *)indexPath;
-(void)selectGenuineMemberForIndexPath:(NSIndexPath *)indexPath;
-(void)selectNumberPickerForIndexPath:(NSIndexPath *)indexPath;
-(void)selectRepairsPerWeekForIndexPath:(NSIndexPath *)indexPath;
-(void)selectSuburbForIndexPath:(NSIndexPath *)indexPath;
-(void)selectMembershipForIndexPath:(NSIndexPath *)indexPath;

-(void)setupFooterView:(BOOL)isNewFieldsAvailable;
-(void)loadContacts;
-(NSString *)formatPhoneNumber:(NSString *)phoneNumber;


@end

@implementation ProfileInfoEditViewController

@synthesize editingContext;
@synthesize customer;
@synthesize customerSaveCallback;
@synthesize customerCancelCallback;
@synthesize addFieldPopover;
@synthesize territoryPopover;
@synthesize statePopover;
@synthesize categoryPopover,numberPickerPopover,suburbPopover;
@synthesize customerNotes;
//@synthesize customerTerritory;
@synthesize isGenuineMember,customerMembership;
//@synthesize isAccountConfimed;// added by Som on 5.12.12
@synthesize customerState;
@synthesize categoryName;
@synthesize selectedTextField;
@synthesize addedCustomerContacts;
@synthesize isNewCustomer;
@synthesize workshopBays,repairsPerWeek;
@synthesize customerSuburb,customerPostCode;

@synthesize theNewContactLastIndexPath;
@synthesize customerTerritoryID;
@synthesize customerTerritoryAbbreviation;
@synthesize delegate;

- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    if (self) 
    {
        _allowSave=YES;
        dataSource = [[NSMutableArray alloc] init];
        optionalFields = [[NSMutableArray alloc] init];
        textfieldContainerDict = [[NSMutableDictionary alloc] init];
        _newContactSections=[[NSMutableArray alloc] init];
        _addedContactSections=[[NSMutableArray alloc] init];
        _deletedContacts = [[NSMutableSet alloc] init];
        
        _textViewHeight = 50.0;
        _currentContactSection = NSNotFound;
        _shouldScrollToSelectedField = YES;
        
        swipedSection = NSNotFound;
        

       // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidAppear:) name:UIKeyboardDidShowNotification object:nil];
        
       // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidDismiss:) name:UIKeyboardDidHideNotification object:nil];
        
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contactsDidLoad:) name:CustomerContactsDidLoad object:nil];
        
       // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancel) name:CustomerDidDeletedOnDuplicate object:nil];
        
    }
    return self;
}

-(void)loadSelectedOEMs
{
    NSArray *lastSelectedOems = [self.customer.dealingOEMs componentsSeparatedByString:@","];
    
    if([lastSelectedOems count])
    {
        if(selectedOEMs) [selectedOEMs release];
        
        NSString *str=[lastSelectedOems objectAtIndex:0];
        if(str.length)
        {
            selectedOEMs =[[NSMutableArray alloc] init];
            
            for(NSString *oemID in lastSelectedOems)
            {
                if([oemID integerValue])
                {
                    [selectedOEMs addObject:oemID];
                }
            }
            
            //selectedOEMs =[[NSMutableArray alloc] initWithArray:lastSelectedOems];
        }
        else 
        {
            // Rep *currrentRep = [CoreDataHandler currentRep];
           //  NSSet *oemIds = [currrentRep.parentWd.parentOem valueForKey:@"oemID"];
           // selectedOEMs =[[NSMutableArray alloc] initWithArray:oemIds.allObjects];
            
            selectedOEMs =[[NSMutableArray alloc] init];
        }
    }
    else 
    {
        if(selectedOEMs) [selectedOEMs release];
        
        if(_isNewCustomer)
        {
           selectedOEMs=[[NSMutableArray alloc] init];
            
           Rep *rep = [CoreDataHandler currentRep];
            for(OEM *oem in rep.parentWdSite.parentOem)
            {
             if(oem.oemID)
              [selectedOEMs addObject:[oem.oemID description]];
            }
        }
        else 
        {
           selectedOEMs =[[NSMutableArray alloc] init]; 
        }
        
       // selectedOEMs =[[NSMutableArray alloc] initWithObjects:@"Subaru",@"Suzuki",@"Honda",nil];
       // selectedOEMs =[[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",nil];
    }

}

-(void)loadContacts
{
    NSArray *_contacts=[self.customer.contacts allObjects];
    if([_contacts count])
    {
        NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"createdDate" ascending:YES];
        self.addedCustomerContacts=[NSMutableArray arrayWithArray:[_contacts sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]]];
        [sortDescriptor release];
    }
    else
    {
        self.addedCustomerContacts=nil;
    }
}

-(void)contactsDidLoad:(NSNotification *)notif
{
    NSMutableDictionary *dict = [notif object];
    
    NSArray *deletedResult  =  [dict objectForKey:@"deleted"];
    NSArray *insertedResult =  [dict objectForKey:@"inserted"];
    NSArray *updatedResult  =  [dict objectForKey:@"updated"];
    
    [self loadContactsPeformingDelete:deletedResult update:updatedResult insert:insertedResult];
}


-(void)loadContactsPeformingDelete:(NSArray *)deleteArr update:(NSArray *)updateArr insert:(NSArray *)insertArr
{
    BOOL isChanged = [deleteArr count]> 0 || [updateArr count]>0 || [insertArr count]>0;
    
    if(!isChanged) return;
    
    if(!self.addedCustomerContacts) self.addedCustomerContacts = [NSMutableArray array];
    
    /*
    [self.addedCustomerContacts addObjectsFromArray:insertArr];
     [self.addedCustomerContacts removeObjectsInArray:deleteArr];
     */
    
     Customer *_parentCustomer = self.editingContext ? (Customer *) [self.customer inContext:self.editingContext] : self.customer;
    
        
    // LOGIC CHANGED ON 4TH OCT 2013
    [self.addedCustomerContacts setArray:_parentCustomer.contacts.allObjects];

    
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"createdDate" ascending:YES];
    [self.addedCustomerContacts sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [sortDescriptor release];
    
    [self setupDataSource];
    //[self setupFooterView:optionalFields.count>0];
    [self.tableView reloadData];
}


-(id)initWithCustomer:(Customer *)aCustomer
{
    self = [self init];
    
    
    
    _isNewCustomer  = !aCustomer.name.length;
    
    self.customer = aCustomer;
    

    
    //[self setupDataSource];
    
    return self;
    
}

-(void)reloadData
{

    NSLog(@"Cusatomer categoryID=%@",self.customer.categoryID);
    
    self.categoryName = _isNewCustomer ? nil : [self.customer getCategoryName];
    
    self.categoryID =  _isNewCustomer ? nil : self.customer.categoryID;

    
     NSLog(@"Cusatomer categoryName=%@",self.customer.categoryName);
    
    self.isGenuineMember     = self.customer.getGenuineMember;
    self.customerState       = self.customer.state;
    self.customerSuburb      = self.customer.suburb;
    self.customerPostCode    = self.customer.postCode;
    self.customerNotes       = self.customer.notes;
    //self.customerTerritory  = self.customer.territory;
    self.customerTerritoryID = self.customer.territoryID;
    self.workshopBays        = self.customer.workshopBays;
    self.repairsPerWeek      = self.customer.repairsPerWeek;
    self.customerMembership  = self.customer.membershipNumber;
    
    
    if([self.customer.territoryID intValue])
    {
      NSPredicate *territoryPredicate = [NSPredicate predicateWithFormat:@"self.territoryID=%@",self.customer.territoryID];
    
        NSArray *activeTerritories =[Territory fetchWithPredicate:territoryPredicate onAttributes:[NSArray arrayWithObject:@"territoryPosition"] sortDescriptors:nil limit:1 error:nil];
        if([activeTerritories count])
        {
            Territory *territory = [activeTerritories objectAtIndex:0];
            //self.customerTerritory  = territory.territoryPosition;
            self.customerTerritoryAbbreviation = territory.territoryAbbreviation;
        }
    }
    
    
    numberOfContacts =0;
    swipedSection = NSNotFound;
    
    [_deletedContacts removeAllObjects];
    [_addedContactSections removeAllObjects];
    [_newContactSections removeAllObjects];
    
    
    
    [self loadContacts];
    
    CGSize textSize = [self.customerNotes sizeWithFont:[UIFont grotesqueFontOfSize:13.0] constrainedToSize:CGSizeMake(320.0, CGFLOAT_MAX)];
    _textViewHeight=textSize.height+17.0;
    
    [self loadSelectedOEMs];    
    
    if(!dealingOEMList)
    {
        NSMutableArray *_oems = [NSMutableArray array];
        Rep *rep = [CoreDataHandler currentRep];
        
        NSArray *oems = rep.parentWdSite.parentOem.allObjects; 
        
        if([oems count])
        {
           // NSSortDescriptor *_descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
            
             NSSortDescriptor *_descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
            
            oems = [oems sortedArrayUsingDescriptors:[NSArray arrayWithObject:_descriptor]];
            [_descriptor release];
        }

        
        for(OEM *oem in oems)
        {
             NSDictionary *oemDict=[NSDictionary dictionaryWithObjectsAndKeys:oem.name,DATADICT_TITLE_KEY,[oem.oemID description],@"oemID",[NSIndexPath indexPathForRow:[_oems count] inSection:CONTACT_SECTION_END+2],@"location",nil];
            [_oems addObject:oemDict];
        }

        dealingOEMList = [[NSArray alloc] initWithArray:_oems];
    }
    
    /*
    if(!dealingOEMList)
    {
    NSDictionary *oemDict=[NSDictionary dictionaryWithObjectsAndKeys:@"Subaru",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:0 inSection:24],@"location",nil];
    NSDictionary *oemDict2=[NSDictionary dictionaryWithObjectsAndKeys:@"Mitsubishi",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:1 inSection:24],@"location",nil];
    NSDictionary *oemDict3=[NSDictionary dictionaryWithObjectsAndKeys:@"Honda",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:2 inSection:24],@"location",nil];
    
    dealingOEMList = [[NSArray alloc] initWithObjects:oemDict,oemDict2,oemDict3,nil];
    }
    
    
    if(!notDealingOEMList)
    {
    NSDictionary *oemDict4=[NSDictionary dictionaryWithObjectsAndKeys:@"Holden",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:0 inSection:25],@"location",nil];
    NSDictionary *oemDict5=[NSDictionary dictionaryWithObjectsAndKeys:@"Hyundai",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:1 inSection:25],@"location",nil];
    NSDictionary *oemDict6=[NSDictionary dictionaryWithObjectsAndKeys:@"Suzuki",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:2 inSection:25],@"location",nil];
    
    notDealingOEMList = [[NSArray alloc] initWithObjects:oemDict4,oemDict5,oemDict6, nil];  
    }
    */

    [self setupDataSource];
    [self setupFooterView:optionalFields.count>0];
    [self.tableView reloadData];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomerContactsDidLoad object:nil];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:CustomerDidDeletedOnDuplicate object:nil];

    
    delegate=nil;
    editingContext = nil;
   // customer = nil;
    
    [dataSource release];
     dataSource = nil;
    
    [arrOEM release];
    [dealingOEMList release];
    [notDealingOEMList release];
    [selectedOEMs release];
    [optionalFields release];
    
    
    
    [addFieldPopover release] ; addFieldPopover=nil;
    
    [suburbPopover release] ;
    suburbPopover = nil;
    
    [textfieldContainerDict release] ;
    textfieldContainerDict = nil;
    [territoryPopover release] ;
    territoryPopover = nil;
    [statePopover release] ,
    statePopover = nil;
    [categoryPopover release] ;
    categoryPopover = nil;
    [numberPickerPopover release] ;
    numberPickerPopover = nil;
    [customerNotes release] ;
    customerNotes = nil;
    //[customerTerritory release] ;
   // customerTerritory = nil;
    [customerTerritoryID release];
    [customerTerritoryAbbreviation release];
    customerTerritoryAbbreviation = nil;
    [isGenuineMember release] ;
    isGenuineMember = nil; // added by Som on 5.12.12
    //[isAccountConfimed release], isAccountConfimed = nil;
    [customerState release] ;
    customerState =nil;
    [categoryName release] ;
    categoryName =nil;
    
    [_categoryID release];
    _categoryID = nil;
    
    [selectedTextField release] ;
    selectedTextField = nil;
    [addedCustomerContacts release] ;
    addedCustomerContacts = nil;
    [_newContactSections release] ;
    _newContactSections =nil;
    [_addedContactSections release];
    _addedContactSections = nil;
    [_deletedContacts release] ,
    _deletedContacts = nil;
    [repairsPerWeek release] ;
    repairsPerWeek = nil;
    [workshopBays release]   ;
    repairsPerWeek = nil;
    
    //[state release] , state = nil;
    [customerSuburb release];
    customerSuburb = nil;
    [customerPostCode release] ;
    customerPostCode = nil;
    [customerMembership release] ;
    customerMembership = nil;
    
    [theNewContactLastIndexPath release];
    theNewContactLastIndexPath=nil;
    
    if(customerSaveCallback)   [customerSaveCallback release];
    if(customerCancelCallback) [customerCancelCallback release];
    
    customerSaveCallback = nil;
    customerCancelCallback = nil;
    
    [super dealloc];
}



-(void)keyboardDidAppear:(NSNotification *)aNotification
{
   // if(_isKeyboardAppeared) return;
    
    // _isKeyboardAppeared=YES;
    /*
  //  CGRect keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
   CGRect keyboardRect =  [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect toView:nil];

    UIEdgeInsets _tableInset = self.tableView.contentInset;
    _tableInset.bottom += CGRectGetHeight(keyboardRect);//-49.0-44.0;
    self.tableView.contentInset=_tableInset;
    
    if(self.selectedTextField.isFirstResponder)
    {
        UITableViewCell *cell = (UITableViewCell*) [[self.selectedTextField superview] superview];
        NSIndexPath *scrollingIndexPath = [self.tableView indexPathForCell:cell];
        [self.tableView scrollToRowAtIndexPath:scrollingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    */
    
    //============
    NSDictionary* info = [aNotification userInfo];
    CGRect keyboardRect =  [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect toView:nil];
    CGSize kbSize = keyboardRect.size;
    //kbSize.width-=50.0;
    
    CGFloat _bottom = kbSize.height-(isNewCustomer?0:49.0);
    UIEdgeInsets _tableInset = self.tableView.contentInset;
    //_tableInset.bottom = kbSize.height;
    _tableInset.bottom = _bottom;
    self.tableView.contentInset=_tableInset;
    self.tableView.scrollIndicatorInsets = _tableInset;
    
    if(!self.selectedTextField) return;
        
  // textFieldRect =[self.tableView convertRect:textFieldRect toView:nil];
    
    if(self.theNewContactLastIndexPath)
    {
        [self.tableView scrollToRowAtIndexPath:self.theNewContactLastIndexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
                
      //  self.newContactLastIndexPath = nil;
    }
    else
    {
        CGRect aRect = self.tableView.bounds;
        aRect = [self.view convertRect:aRect toView:nil];
        aRect.size.height -= kbSize.height;
        
        CGRect textFieldRect =[self.tableView convertRect:self.selectedTextField.frame fromView:self.selectedTextField.superview];

    if (!CGRectContainsPoint(aRect, textFieldRect.origin) ) 
    {
        //CGPoint scrollPoint = CGPointMake(0.0, textFieldRect.origin.y-kbSize.height+_tableInset.bottom);
        //[ self.tableView setContentOffset:scrollPoint animated:YES];
        [self.tableView scrollRectToVisible:textFieldRect animated:YES];
    }
    
    }
}

-(void)keyboardDidDismiss:(NSNotification *)aNotification
{
   // if(!_isKeyboardAppeared) return;
    
    if(!isNewCustomer) return;
  
    CGRect keyboardRect =  [[[aNotification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
   // keyboardRect = [self.view convertRect:keyboardRect toView:nil];
    
    UIEdgeInsets _tableInset = self.tableView.contentInset;
    _tableInset.bottom -= CGRectGetHeight(keyboardRect);//-49.0-44.0;
    _tableInset.bottom=MAX(0,_tableInset.bottom);
    self.tableView.contentInset =_tableInset;
    
    //self.tableView.scrollIndicatorInsets = _tableInset;
    
   // _isKeyboardAppeared=NO;
}

-(NSInteger)addNewContactSectionWithFields:(NSArray *)fields
{
    int _section =CONTACT_SECTION_START+numberOfContacts;
    
     if(_section>=CONTACT_SECTION_END+1)
     {
         NSString *msg = [NSString stringWithFormat:CUSTOMER_CONTACT_MAXLIMIT_MESSAGE_FORMAT,CONTACT_SECTION_END-CONTACT_SECTION_START];
         [UIAlertView showWarningAlertWithTitle:@"" message:msg];
         return NSNotFound;
     }
    
    int _row=0;
    NSMutableArray *rows=[[NSMutableArray alloc] init];
    
    for(NSString *fieldName in fields)
    {
        NSMutableDictionary *rowInfo = [[NSMutableDictionary alloc] init];
        [rowInfo setObject:fieldName forKey:DATADICT_TITLE_KEY];
        [rowInfo setObject:[NSIndexPath indexPathForRow:_row inSection:_section] forKey:@"location"];
        [rows addObject:rowInfo];
        [rowInfo release];
        
        _row++;
    }
    
        if(rows)
        {
            if(_section<=[dataSource count])
            {
                [dataSource insertObject:rows atIndex:_section];
            }
            else
            {
                [dataSource addObject:rows];
            }

            numberOfContacts++;
            _tolContacts = numberOfContacts;

            [rows release];
        
            return _section;
            
        }
    
    
    return _section;
}



#pragma mark SETUP DATA SOURCE
/*
-(void)setupDataSource
{
    if([dataSource count]) [dataSource removeAllObjects];
    if([optionalFields count]) [optionalFields removeAllObjects];
    

    NSMutableArray *rows=[NSMutableArray array];
    NSMutableArray  *_rows = [NSMutableArray array];
                              
    NSDictionary *rowInfo1 = [NSDictionary dictionaryWithObjectsAndKeys:@"Business Name *",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:0 inSection:0],@"location",nil];
    
    NSDictionary *rowInfo2 = [NSDictionary dictionaryWithObjectsAndKeys:@"Business Type *",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:1 inSection:0],@"location",nil];
    
    [rows addObject:rowInfo1];
  //  [rows addObject:rowInfoAccount];
    [rows addObject:rowInfo2];
    

    //=============================== OPTIONAL =====================================
     NSDictionary *rowInfo3 = [NSDictionary dictionaryWithObjectsAndKeys:@"GetGenuineMember",@"title",[NSIndexPath indexPathForRow:4 inSection:0],@"location", nil];
    
     //if([self.categoryName isEqualToString:@"Mechanical"])
     if(self.categoryName.length)
     {
        NSDictionary *rowInfo4 = [NSDictionary dictionaryWithObjectsAndKeys:@"Workshop Bays",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:2 inSection:0],@"location",nil];
         
        NSDictionary *rowInfo5 = [NSDictionary dictionaryWithObjectsAndKeys:@"Repairs per Week (Average)",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:3 inSection:0],@"location",nil];
         
        [rows addObject:rowInfo4];
        [rows addObject:rowInfo5];
     }

    
    
    //if(self.customer.getGenuineMember)
        [rows addObject:rowInfo3];
   // else
      //  [_rows addObject:rowInfo3];
    
    if([rows count])
    [dataSource  addObject:rows];
    
    //if([_rows count])[optionalFields addObject:_rows];

    ////////////// NED ADDITION ON 9 NOV 2012 ///////////////////
    rows=[NSMutableArray array];
    NSDictionary *rowInfoAccount = [NSDictionary dictionaryWithObjectsAndKeys:@"Account Number",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:0 inSection:1],@"location",nil];
    [rows addObject:rowInfoAccount];
    [dataSource  addObject:rows];
    //////////////////////////////////////////////////////////////////
    
    ///==================================== TERRITORY ============================================
    //OPTIONAL
    rows=[NSMutableArray array];
    NSDictionary *rowInfo10 = [NSDictionary dictionaryWithObjectsAndKeys:@"Territory *",@"title",[NSIndexPath indexPathForRow:0 inSection:2],@"location", nil];
    _rows = [NSMutableArray arrayWithObjects:rowInfo10, nil];
    
    // if([self.customer.territory integerValue])
    [dataSource  addObject:_rows];
    //else [optionalFields addObject:_rows];
    
    
    //======================================= ADDRESS ============================================
    //_rows=[NSMutableArray array];
    rows=[NSMutableArray array];
    
   
    //rows=[NSMutableArray arrayWithObjects:rowInfo7, nil];
    
    //OPTIONAL
    NSDictionary *rowInfo20 = [NSDictionary dictionaryWithObjectsAndKeys:@"Street Line 1",@"title",[NSIndexPath indexPathForRow:0 inSection:3],@"location", nil];
    NSDictionary *rowInfo21 = [NSDictionary dictionaryWithObjectsAndKeys:@"Street Line 2",@"title",[NSIndexPath indexPathForRow:1 inSection:3],@"location",nil];
    
     NSDictionary *rowInfo7 = [NSDictionary dictionaryWithObjectsAndKeys:@"City/Suburb *",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:2 inSection:3],@"location",nil];
    
    NSDictionary *rowInfo23 = [NSDictionary dictionaryWithObjectsAndKeys:@"Postcode *",@"title",[NSIndexPath indexPathForRow:3 inSection:3],@"location",nil];
    
    //if(self.customer.address)
    [rows addObject:rowInfo20];
    //else [_rows addObject:rowInfo20];
    
    //if(self.customer.address2)
    [rows addObject:rowInfo21];
    //else [_rows addObject:rowInfo21];
    
    [rows addObject:rowInfo7];
    
    //if(self.customer.postCode)
    [rows addObject:rowInfo23];
    //else [_rows addObject:rowInfo23];
    
    if([rows count])
    [dataSource  addObject:rows];
    
    //if([_rows count])[optionalFields addObject:_rows];
   
//================================ EMAIL MAIN =========================================================
    //_rows=[NSMutableArray array];
    rows=[NSMutableArray array];
    NSDictionary *rowInfo8 = [NSDictionary dictionaryWithObjectsAndKeys:@"Main",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:0 inSection:4],@"location",nil];
     NSDictionary *rowInfo9 = [NSDictionary dictionaryWithObjectsAndKeys:@"Email",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:1 inSection:4],@"location",nil];
    
   // if(self.customer.phone.length)
    [rows addObject:rowInfo8];
   // else [_rows addObject:rowInfo8];
    
    //if(self.customer.email.length)
    [rows addObject:rowInfo9];
   // else [_rows addObject:rowInfo9];
    
    if([rows count])
    [dataSource  addObject:rows];
    //if([_rows count])[optionalFields addObject:_rows];
    
    //================================ Contacts =========================================================
  
    if([addedCustomerContacts count])
    {
        rows=[NSMutableArray array];
        NSDictionary *rowInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"Contacts",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:0 inSection:5],@"location",nil];
        [rows addObject:rowInfo];
         [dataSource  addObject:rows];
    }

    NSArray *fields = [NSArray arrayWithObjects:@"Name",@"Title",@"Direct",@"Mobile",@"Email",nil];
    
    for(CustomerContact *contact in addedCustomerContacts)
    {
                
        int _section =[self addNewContactSectionWithFields:fields];
        NSLog(@"Inserted section:%d",_section);
        
        //[_addedContactSections addObject:[NSNumber numberWithInt:_section]];
    }
    
    
    rows=[NSMutableArray array];
    NSDictionary *rowInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"Add New Contact",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:0 inSection:CONTACT_SECTION_END+1],@"location",nil];
    [rows addObject:rowInfo];
    [dataSource  addObject:rows];

   //========================================OEMS  =====================================================
    rows=[NSMutableArray array];
  // NSMutableArray *rows2=[NSMutableArray array];
   // _rows=[NSMutableArray array];
    
       
    
    //if(![selectedOEMs count])
    {
       // [rows addObjectsFromArray:dealingOEMList];
       // [rows2 addObjectsFromArray:notDealingOEMList];
        [rows addObjectsFromArray:dealingOEMList];
    }
   // else 
    {
      //[_rows addObject:rowInfo11];
     // [_rows addObject:rowInfo12];
        //[_rows addObject:rowInfoOEMs];
    }
    

    if([rows count])
    [dataSource  addObject:rows];
    
    //if([rows2 count])
    //[dataSource  addObject:rows2];
    
  //  if([_rows count])
  //      [optionalFields addObject:_rows];
    
    //[optionalFields setObject:_rows forKey:@"4"];
    
    //================================= NOTES =========================================
    
    //OPTIONAL
    NSDictionary *rowInfo30 = [NSDictionary dictionaryWithObjectsAndKeys:@"Customer Notes",@"title",[NSIndexPath indexPathForRow:0 inSection:26],@"location", nil];
    
    _rows = [NSMutableArray arrayWithObjects:rowInfo30, nil];
    //if(self.customer.notes.length)
    {
       [dataSource  addObject:_rows]; 
    }
    //else
   // [optionalFields addObject:_rows];
    
   // [optionalFields setObject:_rows forKey:@"5"];

}
*/

-(void)setupDataSource
{
    if([dataSource count]) [dataSource removeAllObjects];
    if([optionalFields count]) [optionalFields removeAllObjects];
    
    
    NSMutableArray *rows=[NSMutableArray array];
    NSMutableArray  *_rows = [NSMutableArray array];
    
    NSDictionary *rowInfo1 = [NSDictionary dictionaryWithObjectsAndKeys:@"Business Name *",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:0 inSection:0],@"location",nil];
    
    NSDictionary *rowInfo2 = [NSDictionary dictionaryWithObjectsAndKeys:@"Business Type *",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:1 inSection:0],@"location",nil];
    
    [rows addObject:rowInfo1];
    //  [rows addObject:rowInfoAccount];
    [rows addObject:rowInfo2];
    
    
    //=============================== OPTIONAL =====================================
  
    
 
    if(self.categoryName.length)
    {
        //if([self.categoryName isEqualToString:@"Mechanical"] || [self.categoryName isEqualToString:@"All"])
        
        if(self.categoryID.integerValue == 1 || self.categoryID.integerValue== 0)
        {
            
          NSDictionary *rowInfo4 = [NSDictionary dictionaryWithObjectsAndKeys:@"Workshop Bays",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:2 inSection:0],@"location",nil];
        
          NSDictionary *rowInfo5 = [NSDictionary dictionaryWithObjectsAndKeys:@"Repairs per Week (Average)",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:3 inSection:0],@"location",nil];
        
          [rows addObject:rowInfo4];
          [rows addObject:rowInfo5];
            
        }
        //else if([self.categoryName isEqualToString:@"Collision"])
        else   if(self.categoryID.integerValue == 2)
        {
              NSDictionary *rowInfo5 = [NSDictionary dictionaryWithObjectsAndKeys:@"Repairs per Week (Average)",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:3 inSection:0],@"location",nil];
             [rows addObject:rowInfo5];
        }
    }
    
    /*
    //============= GET GENUINE MEMBER =================
      NSDictionary *rowInfo3 = [NSDictionary dictionaryWithObjectsAndKeys:@"GetGenuineMember",@"title",[NSIndexPath indexPathForRow:4 inSection:0],@"location", nil];
    
    //if(self.customer.getGenuineMember)
    [rows addObject:rowInfo3];
    // else
    //  [_rows addObject:rowInfo3];
    
    if(self.isGenuineMember.boolValue)
    {
       NSDictionary *rowInfoMemberShip = [NSDictionary dictionaryWithObjectsAndKeys:@"Membership Number",@"title",[NSIndexPath indexPathForRow:5 inSection:0],@"location", nil];
       [rows addObject:rowInfoMemberShip];
    }
     */
    
    
    NSDictionary *rowInfo3 = [NSDictionary dictionaryWithObjectsAndKeys:@"GetGenuine",@"title",[NSIndexPath indexPathForRow:4 inSection:0],@"location", nil];
    [rows addObject:rowInfo3];
    
    NSDictionary *rowInfoCapricorn = [NSDictionary dictionaryWithObjectsAndKeys:@"Trade Club",@"title",[NSIndexPath indexPathForRow:5 inSection:0],@"location", nil];
        [rows addObject:rowInfoCapricorn];
    
    NSDictionary *rowInfoMemberShip = [NSDictionary dictionaryWithObjectsAndKeys:@"Capricorn",@"title",[NSIndexPath indexPathForRow:6 inSection:0],@"location", nil];
    [rows addObject:rowInfoMemberShip];

    
    if([rows count])
        [dataSource  addObject:rows];
    
    //if([_rows count])[optionalFields addObject:_rows];
    
    ////////////// NED ADDITION ON 9 NOV 2012 ///////////////////
    rows=[NSMutableArray array];
    NSDictionary *rowInfoAccount = [NSDictionary dictionaryWithObjectsAndKeys:@"Account Number",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:0 inSection:1],@"location",nil];
    [rows addObject:rowInfoAccount];
    [dataSource  addObject:rows];
    //////////////////////////////////////////////////////////////////
    
    ///==================================== TERRITORY ============================================
    //OPTIONAL
    rows=[NSMutableArray array];
    NSDictionary *rowInfo10 = [NSDictionary dictionaryWithObjectsAndKeys:@"Territory *",@"title",[NSIndexPath indexPathForRow:0 inSection:2],@"location", nil];
    _rows = [NSMutableArray arrayWithObjects:rowInfo10, nil];
    
    // if([self.customer.territory integerValue])
    [dataSource  addObject:_rows];
    //else [optionalFields addObject:_rows];
    
    
    //======================================= ADDRESS ============================================
    //_rows=[NSMutableArray array];
    rows=[NSMutableArray array];
    
    
    //rows=[NSMutableArray arrayWithObjects:rowInfo7, nil];
    
    //OPTIONAL
    NSDictionary *rowInfo20 = [NSDictionary dictionaryWithObjectsAndKeys:@"Street Line 1",@"title",[NSIndexPath indexPathForRow:0 inSection:3],@"location", nil];
    NSDictionary *rowInfo21 = [NSDictionary dictionaryWithObjectsAndKeys:@"Street Line 2",@"title",[NSIndexPath indexPathForRow:1 inSection:3],@"location",nil];
    
    NSDictionary *rowInfo7 = [NSDictionary dictionaryWithObjectsAndKeys:@"Location *",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:2 inSection:3],@"location",nil];
    
    //NSDictionary *rowInfo23 = [NSDictionary dictionaryWithObjectsAndKeys:@"Postcode *",@"title",[NSIndexPath indexPathForRow:3 inSection:3],@"location",nil];
    
    //if(self.customer.address)
    [rows addObject:rowInfo20];
    //else [_rows addObject:rowInfo20];
    
    //if(self.customer.address2)
    [rows addObject:rowInfo21];
    //else [_rows addObject:rowInfo21];
    
    [rows addObject:rowInfo7];
    
    //if(self.customer.postCode)
    //[rows addObject:rowInfo23];
    //else [_rows addObject:rowInfo23];
    
    if([rows count])
        [dataSource  addObject:rows];
    
    //if([_rows count])[optionalFields addObject:_rows];
    
    //================================ EMAIL MAIN =========================================================
    //_rows=[NSMutableArray array];
    rows=[NSMutableArray array];
    NSDictionary *rowInfo8 = [NSDictionary dictionaryWithObjectsAndKeys:@"Main Phone",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:0 inSection:4],@"location",nil];
    NSDictionary *rowInfo9 = [NSDictionary dictionaryWithObjectsAndKeys:@"Email",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:1 inSection:4],@"location",nil];
    
    // if(self.customer.phone.length)
    [rows addObject:rowInfo8];
    // else [_rows addObject:rowInfo8];
    
    //if(self.customer.email.length)
    [rows addObject:rowInfo9];
    // else [_rows addObject:rowInfo9];
    
    if([rows count])
        [dataSource  addObject:rows];
    //if([_rows count])[optionalFields addObject:_rows];
    
    //================================ Contacts =========================================================
    
    if([addedCustomerContacts count])
    {
        rows=[NSMutableArray array];
        NSDictionary *rowInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"Contacts",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:0 inSection:5],@"location",nil];
        [rows addObject:rowInfo];
        [dataSource  addObject:rows];
    }
    
    NSArray *fields = [NSArray arrayWithObjects:@"Name",@"Title",@"Direct",@"Mobile",@"Email",nil];
    
    for(CustomerContact *contact in addedCustomerContacts)
    {
        int _section =[self addNewContactSectionWithFields:fields];
        NSLog(@"Inserted section:%d",_section);
        //[_addedContactSections addObject:[NSNumber numberWithInt:_section]];
    }
    
    
    
    rows=[NSMutableArray array];
    NSDictionary *rowInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"Add New Contact",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:0 inSection:CONTACT_SECTION_END+1],@"location",nil];
    [rows addObject:rowInfo];
    [dataSource  addObject:rows];
    
    
    //========================================OEMS  =====================================================
    rows=[NSMutableArray array];
    
    // NSMutableArray *rows2=[NSMutableArray array];
    // _rows=[NSMutableArray array];

    
    //if(![selectedOEMs count])
    {
        // [rows addObjectsFromArray:dealingOEMList];
        // [rows2 addObjectsFromArray:notDealingOEMList];
        [rows addObjectsFromArray:dealingOEMList];
    }
    // else 
    {
        //[_rows addObject:rowInfo11];
        // [_rows addObject:rowInfo12];
        //[_rows addObject:rowInfoOEMs];
    }
    
    /*
     if(self.customer.otherOEMs)
     [rows addObject:rowInfo12];
     else 
     [_rows addObject:rowInfo12];
     */
    
    if([rows count])
        [dataSource  addObject:rows];
    
    //if([rows2 count])
    //[dataSource  addObject:rows2];
    
    //  if([_rows count])
    //      [optionalFields addObject:_rows];
    
    //[optionalFields setObject:_rows forKey:@"4"];
    
    //================================= NOTES =========================================
    
    //OPTIONAL
    NSDictionary *rowInfo30 = [NSDictionary dictionaryWithObjectsAndKeys:@"Customer Notes",@"title",[NSIndexPath indexPathForRow:0 inSection:CONTACT_SECTION_END+3],@"location", nil];
    
    _rows = [NSMutableArray arrayWithObjects:rowInfo30, nil];
    //if(self.customer.notes.length)
    {
        [dataSource  addObject:_rows]; 
    }
    //else
    // [optionalFields addObject:_rows];
    
    // [optionalFields setObject:_rows forKey:@"5"];
    
}

#pragma mark -

-(void)_updateUIOnDuplicateCustomer
{
    NSIndexPath *nameIndexPath     = [NSIndexPath indexPathForRow:0 inSection:0];
    
    NSIndexPath *address1IndexPath = [NSIndexPath indexPathForRow:0 inSection:3];
    
    NSIndexPath *mainContactIndexPath = [NSIndexPath indexPathForRow:0 inSection:4];
    
    NSIndexPath *emailIndexPath  = [NSIndexPath indexPathForRow:1 inSection:4];
    
    
    UITextField *nameTextField = [textfieldContainerDict objectForKey:nameIndexPath];
    if(nameTextField)
    {
        nameTextField.text = self.customer.name;
    }
    
    
    UITextField *address1TextField = [textfieldContainerDict objectForKey:address1IndexPath];
    if(address1TextField)
    {
        address1TextField.text = self.customer.address;
    }
    
    UITextField *mainContactTextField = [textfieldContainerDict objectForKey:mainContactIndexPath];
    if(mainContactTextField)
    {
        mainContactTextField.text = self.customer.mainContactNo;
    }
    
    UITextField *emailTextField = [textfieldContainerDict objectForKey:emailIndexPath];
    if(emailTextField)
    {
        emailTextField.text = self.customer.email;
    }
    
}

#pragma mark - SAVE ACTION

-(void)saveAction:(UIButton *)sender
{
    sender.userInteractionEnabled = NO;
    
    [self save];
    
    sender.userInteractionEnabled = YES;
}

-(void)save
{
    if(self.selectedTextField) 
    {
        [self.selectedTextField resignFirstResponder];
    }
    
    if(!_allowSave) 
    {
        _allowSave = YES;
        return;
    }
    
    BOOL isAddressChanged=NO;
    BOOL isValid=YES;
    
    NSString *customerName = self.customer.name;
    NSString *customerEmail = self.customer.email;
    NSString *customerContactNum = self.customer.mainContactNo;
    NSString *address1 = self.customer.address;
    
    for(NSIndexPath *indexPath in textfieldContainerDict)
    {
        UITextField *textField= [textfieldContainerDict objectForKey:indexPath];
    
        NSString *text = textField.text.length? [textField.text trimmedString]:textField.text;
        
        textField.text = text;
        
    switch (indexPath.section) 
    {
        case 0:
        {
            switch (indexPath.row) 
            {
                //MANDATORY
                case 0: 
                {
                    if([text isValid])
                    {
                      self.customer.name =  text;
                    }
                    else
                    {
                        [UIAlertView showWarningAlertWithTitle:INVALID_FIELD_HEADER message:PLZ_ENTER_BNAME_MSG];
                        isValid=NO;
                    }
                }
                break;
                    
                /*
                case 2:
                    self.customer.workshopBays    =  textField.text;
                break;
                case 3:
                    self.customer.repairsPerWeek  =  textField.text;
                break;
                */
                /*
                case 5:
                 self.customer.membershipNumber  =  text;
                break;
                */
                case 4:
                {
                  
                    self.customer.membershipNumber  =  text;
                }
                break;
                case 5:
                {
                  
                    self.customer.tradeClubNumber  =  text;
                }
                break;
                case 6:
                {
                   
                    self.customer.capricornNumber  =  text;
                }
                break;
                    
            }
        }
        break;
        case 1:
        {
            switch (indexPath.row)
            {
                case 0:
                {
                    if([self.customer.isAccountConfirmed boolValue] && ![text isValid])
                    {
                        [UIAlertView showWarningAlertWithTitle:@"" message:ACCT_NO_BLANK_MSG];
                        isValid=NO;
                        
                        textField.text = self.customer.accountNo;
                    }
                    else
                    {
                      
                      self.customer.accountNo    =  text;
                    }
                }
                break;
            }
            break;
        }
        break;
        case 3:
        {
            switch (indexPath.row) 
            {
                case 0:
                {
                    if(self.customer.address!=text && ![self.customer.address isEqualToString:text])
                    {
                      self.customer.address  = text;
                      if(!isAddressChanged) isAddressChanged = YES;
                    }
                }
                break;
                    
                case 1:
                {
                   if(self.customer.address2!=text && ![self.customer.address2 isEqualToString:text])
                   {
                     self.customer.address2 = text;
                     if(!isAddressChanged) isAddressChanged = YES;
                   }
                }
                break;
                 
                /*
                //MANDATORY
                case 2:
                {
                    if([text isValid])
                    {
                       // if(self.customer.suburb!=text &&![self.customer.suburb isEqualToString:text])
                       self.customer.suburb   = text;
                    }
                    else
                    {
                        [UIAlertView showWarningAlertWithTitle:INVALID_FIELD_HEADER message:PLZ_ENTER_CITY_SUBURB_MSG];
                        isValid=NO;
                       // return;
                    }
                }
                break;
                    //MANDATORY
                case 3:
                {
                    if([text isValid])
                    {
                        //if(self.customer.postCode!=text && ![self.customer.postCode isEqualToString:text])
                        self.customer.postCode   = text;
                    }
                    else
                    {
                        [UIAlertView showWarningAlertWithTitle:INVALID_FIELD_HEADER message:PLZ_ENTER_PC_MSG];
                        isValid=NO;
                        // return;
                    }
                   // self.customer.postCode = textField.text;
                }
                break; 
             */
            }
        }
        break;
        case 4:
        {
            switch (indexPath.row) 
            {
                case 0:
                {
                   self.customer.mainContactNo = text;
                }
                break;
                case 1:
                {
                     self.customer.email   = text;
                }
                break;
            }
        }
        break;
        }
        
        if(!isValid)
        {
            CGRect targetRect=[self.tableView convertRect:textField.frame fromView:textField.superview];
            [self.tableView scrollRectToVisible:targetRect animated:YES];
            [textField becomeFirstResponder];
            
           // break;
            return;
        }
        
       }
    
    
    /// --------------- DUPLICATE CUSTOMER CHECKING -------------------
    
    NSString *_defaultVal = @"[{*-+&..%^-+}]";
    
    NSString *_emailToCheck = self.customer.email.length ? self.customer.email : _defaultVal;
    NSString *_mainContactNoToCheck = self.customer.mainContactNo.length ? self.customer.mainContactNo : _defaultVal;
    NSString *_addressToCheck = self.customer.address.length ? self.customer.address : _defaultVal;
    
    
    /// ----------------------- DUPLICATE CUSTOMER CHECKING ------------------------
    
    
    NSPredicate *customerDuplicatePredicate = [NSPredicate predicateWithFormat:DUPLICATE_CUSTOMER_PREDICATE_FORMAT,self.customer.name,_mainContactNoToCheck,_emailToCheck,_addressToCheck];

    
    NSMutableArray *fetchedCustomers = [Customer fetchOnContext:self.customer.managedObjectContext predicate:customerDuplicatePredicate onAttributes:[NSArray arrayWithObjects:@"customerID",@"oldCustomerID",nil] sortDescriptors:nil limit:2 startIndex:0 resultType:NSDictionaryResultType error:nil];
    
    for(NSDictionary *__customer in fetchedCustomers)
    {
        NSNumber *customerID     = [__customer objectForKey:@"customerID"];
        NSNumber *iPadCustomerID = [__customer objectForKey:@"oldCustomerID"];
        
        if([self.customer.customerID isEqualToNumber:customerID] || ((self.customer.oldCustomerID!=nil &&  iPadCustomerID) && [self.customer.oldCustomerID isEqualToNumber:iPadCustomerID]))
        {
            continue;
        }
        
        isValid = NO;
        
        [UIAlertView showWarningAlertWithTitle:DUP_CUS_DETAILS_HEADER message:DUP_CUS_DETAILS_MSG];
        
          if(!_isNewCustomer)
          {
            self.customer.name = customerName;
            self.customer.mainContactNo = customerContactNum;
            self.customer.email = customerEmail;
            self.customer.address = address1;
              
           // [self _updateUIOnDuplicateCustomer];
          }

        
        break;
    }

    
    /*
   NSMutableArray *fetchedCustomers = [Customer fetchWithPredicate:customerDuplicatePredicate onAttributes:[NSArray arrayWithObject:@"customerID"] sortDescriptors:nil limit:0 error:nil];
    
    if(_isNewCustomer)
    {
        if([fetchedCustomers count])
        {
            isValid = NO;
            [UIAlertView showWarningAlertWithTitle:DUP_CUS_DETAILS_HEADER message:DUP_CUS_DETAILS_MSG];
            return;
        }
        
    }
    else
    {
        for(Customer *__customer in fetchedCustomers)
        {
            if([self.customer.customerID isEqualToNumber:__customer.customerID])
            {
                continue;
            }
            
            isValid = NO;
            
            [UIAlertView showWarningAlertWithTitle:DUP_CUS_DETAILS_HEADER message:DUP_CUS_DETAILS_MSG];
            
            self.customer.name = customerName;
            self.customer.mainContactNo = customerContactNum;
            self.customer.email = customerEmail;
            self.customer.address = address1;
            
           // [self _updateUIOnDuplicateCustomer];
            
            break;
        }
    }
    */
    
    if(!isValid) return;
    
    
    //------------------- THIS LINE SHOULD BE REMOVED -----------------
    if(![self.customer.name isValid])
    {
        [UIAlertView showWarningAlertWithTitle:INVALID_FIELD_HEADER message:PLZ_ENTER_BNAME_MSG];
        return;
    }
    

    //===== MANDATORY =====
    if(self.categoryName && [self.categoryName isValid])
    {
          self.customer.categoryName = self.categoryName;
        
          // NSInteger catID =([[self.categoryName lowercaseString] isEqualToString:@"all"])? 0: ([[self.categoryName lowercaseString] isEqualToString:@"mechanical"]) ? 1 : 2;
        
          self.customer.categoryID = self.categoryID;
    }
    else
    {
        [UIAlertView showWarningAlertWithTitle:INVALID_FIELD_HEADER message:PLZ_SEL_BTYPE];
        return;
    }
    
    
    //MANDATORY

    if(self.customerTerritoryID && [self.customerTerritoryID intValue])
    {

        self.customer.territoryID = self.customerTerritoryID;
        self.customer.territoryAbbreviation = self.customerTerritoryAbbreviation;
    }
    else
    {
        [UIAlertView showWarningAlertWithTitle:INVALID_FIELD_HEADER message:PLZ_SEL_TERRITORY_MSG];
        return;
    }

    //MANDATORY    
    if(self.customerState && [self.customerState isValid])
    {
      // if(self.customer.state!= self.customerState && ![self.customer.state isEqualToString:self.customerState])
        
        BOOL _isSuburbChanged = (self.customer.suburb!= self.customerSuburb && ![self.customer.suburb isEqualToString:self.customerSuburb]);
        
        BOOL _isPostCodeChanged = (self.customer.postCode!= self.customerPostCode && ![self.customer.postCode isEqualToString:self.customerPostCode]);
        
        if(_isSuburbChanged || _isPostCodeChanged)
        {
         self.customer.state = self.customerState;
         self.customer.postCode = self.customerPostCode;
         self.customer.suburb = self.customerSuburb;
            
         if(!isAddressChanged) isAddressChanged = YES;
            
        }
    }
    else
    {
        [UIAlertView showWarningAlertWithTitle:INVALID_FIELD_HEADER message:PLEASE_SELECT_LOC_MSG];
        return;
    }
    

    /*
    if(self.isGenuineMember) self.customer.getGenuineMember = self.isGenuineMember;
    if(!self.isGenuineMember.boolValue) self.customer.membershipNumber       = nil;
     */
    

    self.customer.getGenuineMember = [NSNumber numberWithBool:self.customer.membershipNumber.length>0];
    
    self.customer.capricorn = [NSNumber numberWithBool:self.customer.capricornNumber.length>0];
   
    self.customer.tradeClub = [NSNumber numberWithBool:self.customer.tradeClubNumber.length>0];
    
    self.customer.workshopBays     =  self.workshopBays;
    
    self.customer.repairsPerWeek   = self.repairsPerWeek;
    
    self.customer.notes = [self.customerNotes trimmedString];
    
    
    
    if(selectedOEMs)
    {
        NSLog(@"DealingOEMS=%@",self.customer.dealingOEMs);
        NSString *oemString = [selectedOEMs componentsJoinedByString:@","];
        self.customer.dealingOEMs = oemString;
    }
    

    
    for(CustomerContact *contact in addedCustomerContacts)
    {
        if(contact.name.length || contact.title.length || contact.direct.length || contact.mobile.length || contact.email.length)
            {
                if([self.customer.contacts containsObject:contact])
                {
                    //Edit
                    //if([contact isUpdated])
                    
                    if([contact.contactID integerValue])
                    {
                        contact.isEditModeOn = YES;
                    }
                    else
                    {
                        contact.isEditModeOn = NO;
                    }
                }
                else //Insert
                {
                    contact.isEditModeOn = NO;
                    [self.customer addContactsObject:contact];
                }
            }
            else //DELETE THE CONTACT OBJECT
            {
                //contact.isDeleted=YES;
                
                [_deletedContacts addObject:contact];
                //[self.customer removeContactsObject:contact];
            }
    }
    
    
    NSMutableSet *_deletdContactIds = [NSMutableSet set];
    for(CustomerContact *contact  in _deletedContacts)
    {
         if([contact.contactID intValue]!=0)
         [_deletdContactIds addObject:contact.contactID];
        
         [self.customer removeContactsObject:contact];
        
        if(self.editingContext!=nil)
        {
            [self.editingContext deleteObject:contact];
        }
        else
        {
           [SSCoreDataManager deleteObject:contact];
        }
    }
    

 
    
    customer.deletedContactIds = _deletdContactIds;
    _currentContactSection     = NSNotFound;
    
    customer.isAddressChanged = isAddressChanged;
    
    if(self.customerSaveCallback) self.customerSaveCallback(self.customer);
    
    self.customerSaveCallback = nil;
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(profileInfoDidEditViewController:didSaveCustomer:)])
    {
        [self.delegate profileInfoDidEditViewController:self didSaveCustomer:self.customer];
        
    }
    
    [_deletedContacts removeAllObjects];
}

#pragma mark -

-(void)cancel
{
     //======= CLEAR textfieldContainerDict ========
     [textfieldContainerDict removeAllObjects];
    _currentContactSection=NSNotFound;
    
    [self customerDidCancelCallback];
}

-(void)customerDidCancelCallback
{
    if(self.customerCancelCallback) self.customerCancelCallback();
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(profileInfoDidEditViewControllerDidCancel:)])
    {
        [self.delegate profileInfoDidEditViewControllerDidCancel:self];
    }

}
-(void)cancel:(UIButton *)sender
{
    [self cancel];
}

-(void)addCustomerSaveCallback:(CustomerSaveCallback)callback
{
    if(callback)
    self.customerSaveCallback =callback;
    
}

-(void)addCustomerCancelCallback:(CustomerCancelCallback)callback
{
    if(callback)
    self.customerCancelCallback = callback;
    
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)setupFooterView:(BOOL)isNewFieldsAvailable
{
    BOOL _newFieldsAvailable = isNewFieldsAvailable;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds),_newFieldsAvailable? 125.0 : 60.0)]; //125.0
    footerView.backgroundColor = [UIColor clearColor];
    
    UIImage *addFieldImage = [UIImage imageNamed:@"add_field_button.png"];
    addFieldButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //[addFieldButton setTitle:@"Add new field" forState:UIControlStateNormal];
    addFieldButton.frame = CGRectMake(10.0, 10.0, addFieldImage.size.width,addFieldImage.size.height);
    [addFieldButton setBackgroundImage:addFieldImage forState:UIControlStateNormal];
    [addFieldButton setBackgroundImage:[UIImage imageNamed:@"add_field_button_selected.png"] forState:UIControlStateHighlighted];
    [footerView addSubview:addFieldButton];
   // addFieldButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [addFieldButton addTarget:self action:@selector(addFieldAction:) forControlEvents:UIControlEventTouchUpInside];
    
    addFieldButton.hidden = !_newFieldsAvailable;
    
    CGFloat locY=_newFieldsAvailable?CGRectGetMaxY(addFieldButton.frame)+18.0:0.0;
    
    /*
    UIImage *cancelButtonImage = [UIImage imageNamed:@"cancel_popover_button.png"];
    UIButton *cancelButton=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(20.0,locY , cancelButtonImage.size.width, cancelButtonImage.size.height) bgImage:@"cancel_popover_button.png" titleColor:nil target:self action:@selector(cancel:)];
    cancelButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    cancelButton.tag=1;
    [cancelButton setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [footerView addSubview:cancelButton];
     */
    
    UIButton *cancelButton=[UIButton repToolCancelButton];
    CGRect frm=cancelButton.frame;
    frm.origin = CGPointMake(10.0, locY);
    cancelButton.frame = frm;
    cancelButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    cancelButton.tag = 1;
    [footerView addSubview:cancelButton];
    [cancelButton addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *saveButton=[UIButton repToolSaveButton];
     frm=saveButton.frame;
    frm.origin = CGPointMake(CGRectGetMaxX(cancelButton.frame)+5.0, CGRectGetMinY(cancelButton.frame));
    saveButton.frame = frm;
    saveButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    saveButton.tag=2;
    [footerView addSubview:saveButton];
    [saveButton addTarget:self action:@selector(saveAction:) forControlEvents:UIControlEventTouchUpInside];

    /*
    UIImage *saveButtonImage=[UIImage imageNamed:@"save_popover_button.png"];
    UIButton  *saveButton=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(CGRectGetMaxX(cancelButton.frame)+13.0, CGRectGetMinY(cancelButton.frame),saveButtonImage.size.width, saveButtonImage.size.height) bgImage:@"save_popover_button.png" titleColor:nil target:self action:nil];
    [saveButton setBackgroundImage:saveButtonImage forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveAction:) forControlEvents:UIControlEventTouchUpInside];
    saveButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    saveButton.tag=2;
    [footerView addSubview:saveButton];
    */

    
    self.tableView.tableFooterView = footerView;
    [footerView release];
}
     
#pragma mark ADD FIELD
-(void)addFieldAction:(UIButton *)sender
{
    [self.selectedTextField resignFirstResponder];
    
    UIPopoverController *popover = self.addFieldPopover;
    
    if(!popover)
    {
        AddFieldsViewContoller *addFieldsViewContoller = [[AddFieldsViewContoller alloc] initWithOptionalFields:optionalFields];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:addFieldsViewContoller];
        popover = [[UIPopoverController alloc] initWithContentViewController:navController];
        popover.popoverContentSize = CGSizeMake(320.0, 500.0);
        self.addFieldPopover = popover;
        addFieldsViewContoller.popoverController=popover;
        [popover release];
        
        [addFieldsViewContoller addWillSelectFieldCallback:^(NSDictionary *fieldInfo,NSIndexPath *tableIndexPath) 
        {
            [popover dismissPopoverAnimated:YES];
            
            NSIndexPath *indexPath = [fieldInfo valueForKey:@"location"];
              NSLog(@"TargetIndexPath:Row=%d section=%d",indexPath.row,indexPath.section);
            NSIndexPath *actualIndexPath =nil;
            NSMutableArray *rows=nil;
            
            int index=0;
            for(NSMutableArray *arr in dataSource)
            {
               // if(![arr count]) continue;
                
                NSDictionary *_rowInfo = [arr objectAtIndex:0];
                
                if([_rowInfo isKindOfClass:[NSDictionary class]])
                {
                    
                NSIndexPath *_parentIndexPath = [_rowInfo valueForKey:@"location"];
     
                //===================== CHECK FOR ROW =============================
                if(indexPath.section==_parentIndexPath.section)
                {
                    rows=arr;
                    
                    int _row=0;
                    for(NSDictionary *dict in arr)
                    {
                        _parentIndexPath = [dict valueForKey:@"location"];
                        
                        if(indexPath.row <_parentIndexPath.row)
                        {
                            actualIndexPath=[NSIndexPath indexPathForRow:_row inSection:index];
                            break;
                        }
                        
                        _row++;
                    }
                    
                    //actualIndexPath=[NSIndexPath indexPathForRow:MIN(rows.count,indexPath.row) inSection:index];
                    break;
                }
                //===================== CHECK FOR SECTION =======================
                else if(indexPath.section<_parentIndexPath.section)
                {
                    actualIndexPath=[NSIndexPath indexPathForRow:indexPath.row inSection:index];
                    break;
                }

                }
                index++;
            }
            
            NSLog(@"MatchedIndexPath:Row=%d section=%d",actualIndexPath.row,actualIndexPath.section);


            if(rows) //FOR NEW ROW TO ADD INTO A AVAILABLE SECTION
            {
                
                if(actualIndexPath)
                {
                    [rows insertObject:fieldInfo atIndex:actualIndexPath.row];
                }
                else
                {
                    actualIndexPath=[NSIndexPath indexPathForRow:MIN(rows.count,indexPath.row) inSection:index];
                    [rows addObject:fieldInfo];
                }
                
                /*
                 if(indexPath.row>[rows count])
                 {
                     [rows addObject:fieldInfo];
                 }
                else 
                 {
                     [rows insertObject:fieldInfo atIndex:indexPath.row];
                 }
                */
                
                NSLog(@"IndexPath:Row=%d section=%d",actualIndexPath.row,actualIndexPath.section);
                
              
                [self.tableView beginUpdates];
                [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:actualIndexPath] withRowAnimation:UITableViewRowAnimationFade];
                [self.tableView endUpdates];
            
            }
            else //FOR NEW SECTION TO ADD
            {
                //In that case insert only section
                
    
                //if(indexPath.section==24 || indexPath.section==25)
               // if(indexPath.section==24)
                 if(indexPath.section==CONTACT_SECTION_END+1)
                {
                   // rows =[NSMutableArray arrayWithArray:(indexPath.section==24) ? dealingOEMList : notDealingOEMList];
                    rows =[NSMutableArray arrayWithArray: dealingOEMList];
                }
                else
                {
                   rows = [NSMutableArray arrayWithObject:fieldInfo];
                }
                
                /*
                actualIndexPath=nil;
                int _section=0;
                for(NSMutableArray *arr in dataSource)
                {
                    NSDictionary *_rowInfo = [arr objectAtIndex:0];
                    NSIndexPath *_parentIndexPath = [_rowInfo valueForKey:@"location"];
                    
                    if(indexPath.section<_parentIndexPath.section)
                    {
                        actualIndexPath=[NSIndexPath indexPathForRow:indexPath.row inSection:_section];
                        break;
                    }

                    _section++;
                }
                 */
                
                if(actualIndexPath)
                {
                    
                    [dataSource insertObject:rows atIndex:actualIndexPath.section];
                }
                else
                {
                    [dataSource addObject:rows];
                    actualIndexPath=[NSIndexPath indexPathForRow:indexPath.row inSection:MIN(dataSource.count-1, indexPath.section)];
                }
                
               
                NSLog(@"IndexPath:Row=%d section=%d",actualIndexPath.row,actualIndexPath.section);
                
                [self.tableView beginUpdates];
                [self.tableView insertSections:[NSIndexSet indexSetWithIndex:actualIndexPath.section] withRowAnimation:UITableViewRowAnimationFade];
                [self.tableView endUpdates];
            }

        }];
        
        [addFieldsViewContoller addDidSelectFieldCallback:^(NSIndexPath *indexPath) 
        {
            
            if(!addFieldsViewContoller.dataSource.count)
            {
                addFieldButton.hidden = YES;
                [self setupFooterView:NO];
                // addFieldButton.enabled = optionalFields.count>0;
            }
        }];
        
        
        [navController release];
        [addFieldsViewContoller release];
    }
    else
    {
        UINavigationController *contentNavContoller=(UINavigationController *)popover.contentViewController;
        AddFieldsViewContoller *addFieldsViewContoller =(AddFieldsViewContoller *)[contentNavContoller topViewController];
        [addFieldsViewContoller setOptionalFields:optionalFields];
    }
    
    CGRect targetRect = [self.view convertRect:sender.frame fromView:[sender superview]];
    [popover presentPopoverFromRect:targetRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
}

#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif

    arrOEM=[[NSMutableArray alloc]init];
     self.clearsSelectionOnViewWillAppear = NO;
    
    UIView *bgView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds))];
    bgView.backgroundColor = [UIColor whiteColor];
    self.tableView.backgroundView = bgView;
    [bgView release];
    
    self.tableView.bounces = NO;
    
    [self reloadData];
    
    //[self setupDataSource];
    //[self setupFooterView:optionalFields.count>0];
    //[self.tableView reloadData];
    
    self.tableView.editing = YES;
    self.tableView.allowsSelectionDuringEditing=YES;
    
    NSLog(@"isNewCustomer:%@",isNewCustomer?@"YES":@"NO");
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.sectionFooterHeight = 1.0;
        self.tableView.sectionHeaderHeight = 10.0;
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 10.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        self.tableView.tableHeaderView = _headerView;
        [_headerView release];
        
        
    }

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self.tableView reloadData];
    
    if(_isKeyboardNotificationRegistered) return;
    
    _isKeyboardNotificationRegistered = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidAppear:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidDismiss:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];

    _isKeyboardNotificationRegistered = NO;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{

    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

-(NSString *)formatPhoneNumber:(NSString *)phoneNumber
{
    if(!phoneNumber.length) return nil;
    
        NSString *numberString = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if(numberString.length<4 || numberString.length>10)
    {
       //NSString *numberString = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        return numberString;
    }

    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    //[formatter setNumberStyle:NSNumberFormatterNoStyle];
    //[formatter setPositiveFormat:@"###-###-####"];
    [formatter setGroupingSeparator:@" "];
    [formatter setMaximumSignificantDigits:11];
    [formatter setGroupingSize:4];
    [formatter setUsesGroupingSeparator:YES];
    [formatter setSecondaryGroupingSize:4];
    //[formatter setLenient:YES];
  

    NSNumber *number = [formatter numberFromString:numberString];
    NSString *resString= [formatter stringFromNumber:number];
    [formatter release];

    return resString;
}

-(void)setContactValue:(NSString *)val forIndexPath:(NSIndexPath *)indexPath
{
    CustomerContact *_contact =(indexPath.section-CONTACT_SECTION_START < [addedCustomerContacts count]) ? [addedCustomerContacts objectAtIndex:indexPath.section-CONTACT_SECTION_START] : nil;
    
    if(!_contact) return;
    
    if(val.length)
      val = [val trimmedString];
    
    switch (indexPath.row) 
    {
        case 0:
            _contact.name   = val;
        break;
            
        case 1:
            _contact.title  = val;
        break;
            
        case 2:
            _contact.direct = val;
        break;
            
        case 3:
        {
            _contact.mobile = val;
        }
        break;
            
        case 4:
            _contact.email  = val;
        break;
            
        default:break;
    }
    
}

-(NSString *)textFieldValueForIndexPath:(NSIndexPath *)indexPath
{
    NSString *value=nil;
    
    switch (indexPath.section) 
    {
        case 0:
        {
            switch (indexPath.row) 
            {
                //TEXT FIELD
                case 0:
                    value = self.customer.name;
                break;
                case 2:
                    value = self.customer.workshopBays;
                break;
                case 3:
                    value = self.customer.repairsPerWeek;
                break;
                    
                /*
                case 5:
                    value = self.customer.membershipNumber;
                break;
                */
                    
                case 4:
                    value = self.customer.membershipNumber;
                break;
                case 5:
                    value = self.customer.tradeClubNumber;
                break;
                case 6:
                    value = self.customer.capricornNumber;
                break;
            }
        }
        break; 
        case 1:
        {
            switch (indexPath.row) 
            {
                    //TEXT FIELD
                case 0:
                    value = self.customer.accountNo;
                break;
            }
        }
        break; 
        case 3:
        {
            switch (indexPath.row) 
            {
                //TEXT FIELD
                case 0:
                    value = self.customer.address;
                    break;
                case 1:
                    value = self.customer.address2;
                    break;
                case 2:
                    value = self.customer.suburb;
                    break;
                case 3:
                    value = self.customer.postCode;
                    break;
                
            }
        }
        break;
        case 4:
        {
            switch (indexPath.row) 
            {
                //TEXT FIELD
                case 0:
                {
                    value = self.customer.mainContactNo;
                   // if(value.length)value = [self formatPhoneNumber:value];
                }
                break;
                case 1:
                    value = self.customer.email;
                break;
            }
        }
        break;
        default:
        {
            int _section =indexPath.section-CONTACT_SECTION_START;
            CustomerContact *contact =(_section<[addedCustomerContacts count]) ? [addedCustomerContacts objectAtIndex:_section] : nil;
            
            if(!contact) break;
            
           // if([_deletedContacts containsObject:contact]) break;
            
            switch (indexPath.row) 
            {
                //TEXT FIELD
                case 0:
                value = contact.name;
                break;
                case 1:
                value = contact.title;
                break;
                case 2:
                {
                    value = contact.direct;
                    //if(value.length)value = [self formatPhoneNumber:value];
                }
                break;
                case 3:
                {
                    value = contact.mobile;
                    //if(value.length)value = [self formatPhoneNumber:value];
                  //value = contact.mobile;
                }
                break;
                case 4:
                value = contact.email;
                break;
            }
        }
        break;
    }

    return value;
}



-(UIKeyboardType)keyboardTypeForIndexPath:(NSIndexPath *)indexPath
{
    UIKeyboardType _keyboardType=UIKeyboardTypeDefault;
    
    switch (indexPath.section) 
    {
        case 0:
        {
            switch (indexPath.row) 
            {
                //TEXT FIELD
                case 0:
                    _keyboardType = UIKeyboardTypeDefault;
                break;
                case 2:
                case 3:
                /*
                case 5:
                    _keyboardType = UIKeyboardTypeNumberPad;
                break;
                */
                case 4:
                case 5:
                case 6:
                    _keyboardType = UIKeyboardTypeNumberPad;
                break;
            }
        }
            break;  
        case 3:
        {
            switch (indexPath.row) 
            {
                    //TEXT FIELD
                case 0:
                case 1:
                case 2:
                    _keyboardType = UIKeyboardTypeDefault;
                break;
                case 3:
                    _keyboardType = UIKeyboardTypeNumberPad;
                break;
            }
        }
            break;
        case 4:
        {
            switch (indexPath.row) 
            {
                //TEXT FIELD
                case 0:
                    _keyboardType = UIKeyboardTypePhonePad;
                    break;
                case 1:
                    _keyboardType = UIKeyboardTypeEmailAddress;
                    break;
            }
        }
        break;
        default:
        {
            switch (indexPath.row) 
            {
                case 0:
                case 1:
                _keyboardType  =  UIKeyboardTypeDefault; 
                 break;
                case 2:
                case 3:
                _keyboardType  =  UIKeyboardTypePhonePad;
                 break;
                 case 4:
                 _keyboardType = UIKeyboardTypeEmailAddress;
                 break;
            }
            
        }
        break;
    
    }
    
    return _keyboardType;
}

-(UIKeyboardType)validateInputForIndexPath:(NSIndexPath *)indexPath
{
    BOOL isNumberOnly = NO;
    
    switch (indexPath.section) 
    {
        case 0:
        {
            switch (indexPath.row) 
            {
                    //TEXT FIELD
                case 0:
                    isNumberOnly = NO;
                    break;
                case 2:
                case 3:
                    /*
                     case 5:
                     isNumberOnly = YES;
                     break;
                     */
                case 4:
                case 5:
                    isNumberOnly = YES;
                    break;
            }
        }
            break;  
        case 1:
        {
            switch (indexPath.row) 
            {
                    //TEXT FIELD
                case 0:
                    isNumberOnly = NO;
                    break;
                    
            }
        }
            break;
        case 3:
        {
            switch (indexPath.row) 
            {
                    //TEXT FIELD
                case 0:
                case 1:
                case 2:
                    isNumberOnly = NO;
                    break;
                case 3:
                    isNumberOnly = YES;
                    break;
                    
            }
        }
            break;
        case 4:
        {
            switch (indexPath.row) 
            {
                    //TEXT FIELD
                case 0:
                    isNumberOnly = YES;
                    break;
                case 1:
                    isNumberOnly = NO;
                    break;
            }
        }
            break;   
        default:
        {
            switch (indexPath.row) 
            {
                    //TEXT FIELD
                case 0:
                case 1:
                case 4:
                    isNumberOnly = NO;  
                    break;
                    
                case 2:
                case 3:
                    isNumberOnly = YES;
                    break;
            }
            
        }
            break;
            
    }
    
    return isNumberOnly;
}

-(int)validateInputTextLengthForIndexPath:(NSIndexPath *)indexPath
{

    NSMutableArray *rows  = (indexPath.section < [dataSource count]) ?  [dataSource objectAtIndex:indexPath.section] : nil;
    
    
    
    NSDictionary *rowInfo = (indexPath.row < [rows count]) ? [rows objectAtIndex:indexPath.row] : nil;
    
    NSIndexPath *actualIndexPath = [rowInfo valueForKey:@"location"];
    
    int _row  = actualIndexPath.row;
    
    int textLength = MAX_TEXT_LENGTH;
    
    switch (actualIndexPath.section) 
    {
        case 0:
        {
            switch (actualIndexPath.row) 
            {
                    //TEXT FIELD
                case 0:
                    textLength = 50;
                break;
               
                case 4:
                case 5:
                case 6:
                    textLength = 30;
                break;
            }
        }
        break;  
        case 1:
        {
            switch (actualIndexPath.row) 
            {
                    //TEXT FIELD
                case 0:
                    textLength = 250;
                break;
               
            }
        }
            break;
        case 3:
        {
            switch (actualIndexPath.row) 
            {
                    //TEXT FIELD
                case 0:
                case 1:
                textLength = 200;
                break;
                    
            }
        }
            break;
        case 4:
        {
            switch (actualIndexPath.row) 
            {
                    //TEXT FIELD
                case 0:
                   textLength = 20;
                    break;
                case 1:
                  textLength = 100;
                    break;
            }
        }
        break;   
        default:
        {
            switch (actualIndexPath.row) 
            {
                    //TEXT FIELD
                case 0:
                textLength = 50;
                break;
                case 1:
                    textLength = 100;
                break;
                case 2:
                textLength = 20;
                break;
                    
                case 3:
                textLength = 20;
                break;
                    
                case 4:
                textLength = 100;  
                break;
            }
            
        }
        break;

    }
    
    return textLength;
}

-(NSString *)membershipLogoImageForIndexPath:(NSIndexPath *)indexPath
{
    NSString *logoImageName = nil;
    
    switch (indexPath.row)
    {
        case 4:
            logoImageName=@"genuine_logo.png";
            break;
            
        case 5:
            logoImageName=@"tradeclub_logo.png";
            break;
            
        case 6:
            logoImageName=@"capricorn_logo.png";
            break;
            
        default:
            break;
    }
    return logoImageName;
}

#pragma mark - DEPRECATED
-(UITableViewCell *)configureCellForIndexPath:(NSIndexPath *)indexPath withRowInfo:(NSDictionary *)rowInfo
{
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    NSIndexPath *actualIndexPath = [rowInfo objectForKey:@"location"];
    
    NSString *text=[rowInfo objectForKey:DATADICT_TITLE_KEY];
    
    UITableViewCell *cell =nil;
    
    NSString *CellIdentifier = @"Cell";
    
    switch (actualIndexPath.section) 
    {
        case 0:
        {
            switch (actualIndexPath.row) 
            {
                //TEXT FIELD
                case 0:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
                    
                    NSIndexPath *key=actualIndexPath;
                    //[NSNumber numberWithInteger:actualIndexPath.section*10+indexPath.row];
                    UITextField *textField = nil;
                    
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                        
                        textField=[UITextField textFieldWithFrame:CGRectMake(10.0, 1.0, CGRectGetWidth(cell.contentView.bounds)-14.0, CGRectGetHeight(cell.contentView.bounds)-2.0) font:13 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
                        textField.delegate=self;
                        textField.placeholder=text;
                        //[textField setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
                        textField.tag = 1;
                        textField.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        textField.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
                        [textfieldContainerDict setObject:textField forKey:key];
                         textField.text = [self textFieldValueForIndexPath:actualIndexPath];
                    }
                    else
                    {
                        textField=(UITextField *)[cell.contentView viewWithTag:1];
                        [textField removeFromSuperview];
                    }

                    textField=[textfieldContainerDict objectForKey:key];
                    [cell.contentView addSubview:textField];
                    
                    textField.keyboardType = [self keyboardTypeForIndexPath:actualIndexPath];
                    
                   // textField.text = [self textFieldValueForIndexPath:actualIndexPath];
                    
                }
                break;
                    
                case 2:
                case 3:
                { 
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,2];
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                        cell.detailTextLabel.textColor = [UIColor blackColor];
                        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:13.0];
                    }
                    
                    cell.textLabel.text = text;
                    
                    if(actualIndexPath.row==2)
                    {
                    
                      cell.detailTextLabel.text = self.workshopBays.length ? self.workshopBays : @"N/A";
                    }
                    else if(actualIndexPath.row==3)
                    {
                      cell.detailTextLabel.text = self.repairsPerWeek.length ? self.repairsPerWeek : @"N/A";
                    }
                }
                break;
                //POPOVER    
                case 1:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,1];
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    
                    UIImageView *catLogoView=nil;
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                        
                        catLogoView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(cell.contentView.bounds)-100.0, (CGRectGetHeight(cell.contentView.bounds)-20.0)/2.0, 20.0, 20.0)];
                        catLogoView.tag = 3;
                        [cell.contentView addSubview:catLogoView];
                        [catLogoView release];
                        
                        
                        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:13.0];
                        cell.detailTextLabel.textColor = [UIColor blackColor];
                        cell.textLabel.textAlignment = NSTextAlignmentLeft;
                    }
                    else 
                    {
                        catLogoView = (UIImageView *)[cell.contentView viewWithTag:3];
                    }
                    
                    cell.textLabel.text = text;
                    cell.detailTextLabel.text = self.categoryName?self.categoryName:@"Select";
                    
                    catLogoView.image = self.categoryName ? [UIImage imageNamed:[NSString stringWithFormat:@"%@_logo.png",[self.categoryName lowercaseString]]] : nil;
                }
                break;
                
                /*
                //CHECKMARK
                case 4:
                {
                     CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,4];
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                        
                        cell.imageView.image = [UIImage imageNamed:@"genuine_logo.png"];
                        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                    }
                    
                    
                     cell.textLabel.text = text;
                     cell.accessoryType = [self.isGenuineMember boolValue]? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
                }
                break;
                
                case 5:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,5];
                    
                    NSIndexPath *key=actualIndexPath;
                    //[NSNumber numberWithInteger:actualIndexPath.section*10+indexPath.row];
                    UITextField *textField = nil;
                    
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                        
                        textField=[UITextField textFieldWithFrame:CGRectMake(10.0, 1.0, CGRectGetWidth(cell.contentView.bounds)-14.0, CGRectGetHeight(cell.contentView.bounds)-2.0) font:13 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
                        textField.delegate=self;
                        textField.placeholder=text;
                        textField.tag = 1;
                        textField.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        textField.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
                        [textfieldContainerDict setObject:textField forKey:key];
                        textField.text = [self textFieldValueForIndexPath:actualIndexPath];
                    }
                    else
                    {
                        textField=(UITextField *)[cell.contentView viewWithTag:1];
                        [textField removeFromSuperview];
                    }
                    
                    textField=[textfieldContainerDict objectForKey:key];
                    [cell.contentView addSubview:textField];
                    
                    textField.keyboardType = [self keyboardTypeForIndexPath:actualIndexPath];
                    
                    // textField.text = [self textFieldValueForIndexPath:actualIndexPath];
                    
                }
                break;
                */
                    
                case 4:
                case 5:
                case 6:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,actualIndexPath.row];
                    
                    NSIndexPath *key=actualIndexPath;
                    //[NSNumber numberWithInteger:actualIndexPath.section*10+indexPath.row];
                    UITextField *textField = nil;
                    
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
                        
                        textField=[UITextField textFieldWithFrame:CGRectMake(10.0, 1.0, CGRectGetWidth(cell.contentView.bounds)-150.0, CGRectGetHeight(cell.contentView.bounds)-2.0) font:13 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
                        
                        textField.delegate=self;
                        textField.placeholder = @"Membership number";
                        textField.tag = 1;
                        textField.font = [UIFont grotesqueBoldFontOfSize:13.0];
                       // textField.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin;
                        textField.textAlignment = NSTextAlignmentLeft;
                        [textfieldContainerDict setObject:textField forKey:key];
                        NSString *_text = [self textFieldValueForIndexPath:actualIndexPath];
                        textField.text = _text.length?_text:nil;
                        //textField.hidden=!_text.length;
                        
                        cell.textLabel.font=[UIFont grotesqueBoldFontOfSize:13.0];
                    }
                    else
                    {
                        //textField=(UITextField *)[cell.contentView viewWithTag:1];
                        textField = (UITextField *)cell.accessoryView;
                        [textField removeFromSuperview];
                        
                        //======================
                        cell.accessoryView = nil;
                    }
                    
                    textField=[textfieldContainerDict objectForKey:key];
                    cell.accessoryView = textField;
                    
                    // [cell.contentView addSubview:textField];
                    
                    NSString *logoImageName = [self membershipLogoImageForIndexPath:actualIndexPath];
                                        
                    cell.imageView.image = [UIImage imageNamed:logoImageName];
                    cell.textLabel.text = text;
                   
                    
                    textField.keyboardType = [self keyboardTypeForIndexPath:actualIndexPath];
                    
                    // textField.text = [self textFieldValueForIndexPath:actualIndexPath];
                    
                }
                break;
                default: break;
            }
            
        }
        break;
            
        case 1:
        {
            CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
            NSIndexPath *key=actualIndexPath;
            //[NSNumber numberWithInteger:actualIndexPath.section*10+indexPath.row];
            UITextField *textField = nil;
            
            cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                
                textField = [textfieldContainerDict objectForKey:key];
                if(!textField)
                {
                    textField=[UITextField textFieldWithFrame:CGRectMake(10.0, 1.0, CGRectGetWidth(cell.contentView.bounds)-14.0, CGRectGetHeight(cell.contentView.bounds)-2.0) font:13 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
                    textField.delegate=self;
                    textField.placeholder=text;
                    textField.tag = 1;
                    textField.font = [UIFont grotesqueBoldFontOfSize:13.0];
                    textField.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;;
                    [textfieldContainerDict setObject:textField forKey:key];
                    textField.text = [self textFieldValueForIndexPath:actualIndexPath];
                }
            }
            else
            {
                textField=(UITextField *)[cell.contentView viewWithTag:1];
                [textField removeFromSuperview];
            }
            
            textField=[textfieldContainerDict objectForKey:key];
            textField.keyboardType = [self keyboardTypeForIndexPath:actualIndexPath];
            [cell.contentView addSubview:textField];
            
            // textField.text = [self textFieldValueForIndexPath:actualIndexPath];
            
        }
        break;
        //======================== TERRITORY POPOVER =============================
        case 2:
        {
            CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
            cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil)
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                cell.detailTextLabel.textColor = [UIColor blackColor];
                cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:13.0];
            }
            
            cell.textLabel.text = text;
        
           // cell.detailTextLabel.text = self.customerTerritory.integerValue ? [self.customerTerritory description] : @"Select";
            
            cell.detailTextLabel.text = self.customerTerritoryAbbreviation.length ? self.customerTerritoryAbbreviation : @"Select";
        }
        break;
        
        //ADDRESS
        case 3:
        {
            switch (actualIndexPath.row) 
            {
                //TEXT FIELD
                case 0:
                case 1:
               // case 3:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
                    
                    NSIndexPath *key=actualIndexPath;
                    //[NSNumber numberWithInteger:actualIndexPath.section*10+indexPath.row];
                    UITextField *textField = nil;

                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                        
                        textField=[UITextField textFieldWithFrame:CGRectMake(10.0, 1.0, CGRectGetWidth(cell.contentView.bounds)-14.0, CGRectGetHeight(cell.contentView.bounds)-2.0) font:13 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
                        textField.delegate=self;
                        textField.placeholder=text;
                        textField.tag = 1;
                        textField.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        textField.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
                        [textfieldContainerDict setObject:textField forKey:key];
                        textField.text = [self textFieldValueForIndexPath:actualIndexPath];
                    }
                    else
                    {
                        textField=(UITextField *)[cell.contentView viewWithTag:1];
                        [textField removeFromSuperview];
                    }
                    
                    textField=[textfieldContainerDict objectForKey:key];
                    
                    if(!textField)
                    {
                        textField=[UITextField textFieldWithFrame:CGRectMake(5.0, 1.0, CGRectGetWidth(cell.bounds)-15, CGRectGetHeight(cell.bounds)-2.0) font:13 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
                        textField.delegate=self;
                        textField.placeholder=text;
                        textField.tag = 1;
                        textField.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        [textfieldContainerDict setObject:textField forKey:key];
                    }
                    
                    [cell.contentView addSubview:textField];
                    
                     textField.keyboardType = [self keyboardTypeForIndexPath:actualIndexPath];
                    
                   // textField.text = [self textFieldValueForIndexPath:actualIndexPath];
                }
                break;
                 
      
                    
                    
                //LOCATION POPOVER    
                case 2:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,2];
          
                    UILabel *suburbLabel = nil;
                    UILabel *stateLabel  = nil;
                    UILabel *postCodeLabel  = nil;
                     
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                     if (cell == nil)
                     {
                       cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                         
                         cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                         cell.textLabel.textColor = [UIColor lightGrayColor];
                     
                         suburbLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 1.0f, 200.0f, CGRectGetHeight(cell.contentView.bounds)-2.0)];
                         suburbLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
                         suburbLabel.backgroundColor = [UIColor clearColor];
                         suburbLabel.textColor = [UIColor blackColor];
                         suburbLabel.font = [UIFont grotesqueBoldFontOfSize:13.0f];
                         suburbLabel.numberOfLines = 2;
                         suburbLabel.tag = 1;
                         [cell.contentView addSubview:suburbLabel];
                         [suburbLabel release];
                         
                         postCodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(cell.contentView.bounds)-50.0f, 1.0f, 40.0f, CGRectGetHeight(cell.contentView.bounds)-2.0f)];
                         postCodeLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
                         postCodeLabel.backgroundColor = [UIColor clearColor];
                         postCodeLabel.textColor = [UIColor blackColor];
                         postCodeLabel.font = [UIFont grotesqueBoldFontOfSize:13.0f];
                         postCodeLabel.textAlignment = UITextAlignmentRight;
                         postCodeLabel.tag = 2;
                        // postCodeLabel.hidden = YES;
                         [cell.contentView addSubview:postCodeLabel];
                         [postCodeLabel release];
                         
                         stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(postCodeLabel.frame)-60.0f, 1.0f, 50.0f, CGRectGetHeight(cell.contentView.bounds)-2.0f)];
                         stateLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
                         stateLabel.backgroundColor = [UIColor clearColor];
                         stateLabel.textColor = [UIColor darkGrayColor];
                         stateLabel.textAlignment = UITextAlignmentRight;
                         stateLabel.font = [UIFont grotesqueBoldFontOfSize:13.0f];
                         stateLabel.tag = 3;
                        // stateLabel.hidden = YES;
                         [cell.contentView addSubview:stateLabel];
                         [stateLabel release];
                         
                        
                     }
                     else
                     {
                         suburbLabel   = (UILabel *)[cell.contentView viewWithTag:1];
                         postCodeLabel = (UILabel *)[cell.contentView viewWithTag:2];
                         stateLabel    = (UILabel *)[cell.contentView viewWithTag:3];
                     }
                    
                    if(!self.customerSuburb.length || !self.customerState.length || !self.customerPostCode.length)
                    {
                        
                        suburbLabel.text = @"Select location *";
                        //suburbLabel.textColor = [UIColor blackColor];
                    }
                    else 
                    {
                        suburbLabel.textColor = [UIColor blackColor];
                       suburbLabel.text   = self.customerSuburb;
                       stateLabel.text    = self.customerState;
                       postCodeLabel.text = self.customerPostCode;
                    }
                    
                }
                break;
                    
                    
                default: break;
            }

            
        }
        break;
        
        //TEXT
        case 4: //MAIN CONTACT & EMAIL
        {
            switch (actualIndexPath.row) 
            {
                //TEXT FIELD
                case 0:
                case 1:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
                    NSIndexPath *key=actualIndexPath;
                    UITextField *textField = nil;
                    
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                        
                        
                        textField=[textfieldContainerDict objectForKey:key];
                        if(!textField)
                        {
                            textField=[UITextField textFieldWithFrame:CGRectMake(10.0, 1.0, CGRectGetWidth(cell.contentView.bounds)-14.0, CGRectGetHeight(cell.contentView.bounds)-2.0) font:13 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
                            textField.delegate=self;
                            textField.placeholder=text;
                            textField.tag = 1;
                            textField.font = [UIFont grotesqueBoldFontOfSize:13.0];
                            textField.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
                            [textfieldContainerDict setObject:textField forKey:key];
                            textField.text = [self textFieldValueForIndexPath:actualIndexPath];
                        }
                        
                    }
                    else
                    {
                        textField=(UITextField *)[cell.contentView viewWithTag:1];
                        [textField removeFromSuperview];
                    }
                    
                     textField=[textfieldContainerDict objectForKey:key];
                     textField.keyboardType = [self keyboardTypeForIndexPath:actualIndexPath];
                     [cell.contentView addSubview:textField];
                    
                   // textField.text = [self textFieldValueForIndexPath:actualIndexPath];

                }
                break;
            }
            
        }
        break;

       // case 25:
        case CONTACT_SECTION_END+2:
        {
            // NSIndexPath *key=actualIndexPath;
            CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
            cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
                cell.textLabel.font  = [UIFont grotesqueBoldFontOfSize:13.0];
            }
            
            //NSArray *arr= (actualIndexPath.section == 24) ? dealingOEMList : notDealingOEMList;
            
            NSArray *arr=  dealingOEMList ;
            NSDictionary *infoDict = (actualIndexPath.row<[arr count]) ? [arr objectAtIndex:actualIndexPath.row] : nil;;
            NSString *title  = [infoDict objectForKey:@"title"];
            cell.textLabel.text =title;
            
            NSLog(@"Title:%@",title);
            
            NSString *oemID=[infoDict objectForKey:@"oemID"];
            
            
            //BOOL isSelected = ([selectedOEMs containsObject:title]);
            BOOL isSelected = ([selectedOEMs containsObject:oemID]);
            
            
            //cell.accessoryType = isSelected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
            if(isSelected)
            {
                cell.accessoryType =UITableViewCellAccessoryCheckmark;
                NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                [dict setValue:oemID forKey:@"id"];
                [dict setValue:title forKey:@"title"];
                [arrOEM addObject:dict];
                [dict release];
                
                NSLog(@" arrOEM %@",arrOEM);
            }
            else
            {
                  cell.accessoryType =UITableViewCellAccessoryNone;
                
            }
            
        }
        break; 
            
        //============================== NOTES ================================
       // case 26:
        case CONTACT_SECTION_END+3:
        {
            //if(_isNotesExpanded)
            {
                KTTextView *textView=nil;
                CellIdentifier = [NSString stringWithFormat:@"%d_%d_expanded",actualIndexPath.section,0];
                cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                if (cell == nil)
                {
                    cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                    
                    textView=[[KTTextView alloc] initWithFrame:CGRectMake(5.0, 5.0, CGRectGetWidth(cell.contentView.bounds)-10.0, CGRectGetHeight(cell.contentView.bounds)-10.0)];
                    textView.delegate = self;
                    textView.tag = 1;
                    textView.autocorrectionType = UITextAutocorrectionTypeNo;
                    textView.font = [UIFont grotesqueFontOfSize:13.0];
                    textView.placeholderText = @"Enter notes";
                    textView.textColor = [UIColor blackColor];
                    textView.contentInset = UIEdgeInsetsMake(2.0, 0, 0, 0);
                    textView.backgroundColor  = [UIColor clearColor];
                    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleHeight;
                    [cell.contentView addSubview:textView];
                    
                  //  if(!textView.text.length) [textView becomeFirstResponder];
                    [textView release];

                }
                else 
                {
                    textView = (KTTextView *)[cell.contentView viewWithTag:1];
                }
                
                textView.text = self.customerNotes ? self.customerNotes : nil;
            }
            
        }
        break;  
            
        case 5:
        {
            CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
            cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil)
            {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:15.0];
                cell.textLabel.backgroundColor = [UIColor clearColor];
                
                cell.textLabel.shadowColor= [UIColor lightGrayColor];
                cell.textLabel.shadowOffset = CGSizeMake(1.0, 1.0);
                
                UIView *bgView=[[UIView alloc] initWithFrame:cell.contentView.bounds];
                bgView.backgroundColor = [UIColor whiteColor];
                cell.backgroundView = bgView;
                [bgView release];
            }
            
           
            cell.textLabel.text = text;
        }
        break;
        //case 24:
        case CONTACT_SECTION_END+1:
        {
            CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
            cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil)
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
           
             //   cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                cell.textLabel.backgroundColor = [UIColor clearColor];
              
                
                UIImage *addNew = [UIImage imageNamed:@"add_new_contact.png"];
                UIButton *addNewButton = [UIButton buttonWithType:UIButtonTypeCustom];
                addNewButton.frame = CGRectMake(0, 0, addNew.size.width, addNew.size.height);
                [addNewButton setImage:addNew forState:UIControlStateNormal];
                [addNewButton setImage:[UIImage imageNamed:@"add_new_contact.png"] forState:UIControlStateHighlighted];
                [cell.contentView addSubview:addNewButton];
                [addNewButton addTarget:self action:@selector(addNewContactAction:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        break;

        default:
        {
            CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,actualIndexPath.row];
            
            NSIndexPath *key=actualIndexPath;
     
            NSLog(@"Key section:%d row:%d",key.section,key.row);
            UITextField *textField = nil;
            
            cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil || _currentContactSection==indexPath.section)
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                
                if(!textField)
                {
                    textField=[UITextField textFieldWithFrame:CGRectMake(10.0, 1.0, CGRectGetWidth(cell.contentView.bounds)-14.0, CGRectGetHeight(cell.contentView.bounds)-2.0) font:13 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
                    textField.delegate=self;
                    textField.placeholder=text;
                    textField.tag = 1;
                    textField.font = [UIFont grotesqueBoldFontOfSize:13.0];
                    textField.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
                   [cell.contentView addSubview:textField];
                
                }
                
               if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
               {
                UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeAction:)];
                rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
                rightSwipe.numberOfTouchesRequired = 1;
                [cell addGestureRecognizer:rightSwipe];
                [rightSwipe release];
                
                UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeAction:)];
                 leftSwipe.numberOfTouchesRequired = 1;
                leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
                [cell addGestureRecognizer:leftSwipe];
                [leftSwipe release];
               }

            }
            else
            {
                textField=(UITextField *)[cell.contentView viewWithTag:1];
                //[textField removeFromSuperview];
            }
            
            textField.text = [self textFieldValueForIndexPath:indexPath]; //actualIndexPath
            textField.keyboardType = [self keyboardTypeForIndexPath:actualIndexPath];
            
            BOOL __isSwiped = (swipedSection == indexPath.section);
            textField.userInteractionEnabled = !__isSwiped;

            
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                if(__isSwiped)
                {
                 UIImage *deleteImage = [UIImage imageNamed:@"minus_button.png"];
                 UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
                 deleteButton.frame = CGRectMake(0.0, 0.0, deleteImage.size.width, deleteImage.size.height);
                 [deleteButton setBackgroundImage:deleteImage forState:UIControlStateNormal];
                 [deleteButton addTarget:self action:@selector(deleteContactAction:) forControlEvents:UIControlEventTouchUpInside];
                 deleteButton.hidden = (indexPath.row!=2);
                 cell.editingAccessoryView = deleteButton;
                }
                else if(cell.editingAccessoryView)
                {
                    cell.editingAccessoryView = nil;
                }
               
            }
        

        }
        break;
    }
   
   
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
    
    return cell;
}

#pragma mark - CONFIGURE CELL
-(UITableViewCell *)_configureCellForIndexPath:(NSIndexPath *)indexPath withRowInfo:(NSDictionary *)rowInfo
{
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    NSIndexPath *actualIndexPath = [rowInfo objectForKey:@"location"];
    
    NSString *text=[rowInfo objectForKey:DATADICT_TITLE_KEY];
    
    UITableViewCell *cell =nil;
    
    NSString *CellIdentifier = @"Cell";
    
    switch (actualIndexPath.section)
    {
        case 0:
        {
            switch (actualIndexPath.row)
            {
                    //TEXT FIELD
                case 0:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
                    
                    NSIndexPath *key=actualIndexPath;
                    //[NSNumber numberWithInteger:actualIndexPath.section*10+indexPath.row];
                    UITextField *textField = nil;
                    
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                    }
                    else
                    {
                        textField=(UITextField *)[cell.contentView viewWithTag:1];
                        [textField removeFromSuperview];
                    }
                    
                    textField=[textfieldContainerDict objectForKey:key];
                    if(!textField)
                    {
                        textField=[UITextField textFieldWithFrame:CGRectMake(10.0, 1.0, CGRectGetWidth(cell.contentView.bounds)-14.0, CGRectGetHeight(cell.contentView.bounds)-2.0) font:13 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
                        textField.delegate=self;
                        textField.placeholder=text;
                        //[textField setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
                        textField.tag = 1;
                        textField.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        textField.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
                        [textfieldContainerDict setObject:textField forKey:key];
                        textField.text = [self textFieldValueForIndexPath:actualIndexPath];
                    }
                    

                    [cell.contentView addSubview:textField];
                    textField.keyboardType = [self keyboardTypeForIndexPath:actualIndexPath];
                    
                    
                }
                break;
                    
                case 2:
                case 3:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,2];
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                        cell.detailTextLabel.textColor = [UIColor blackColor];
                        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:13.0];
                    }
                    
                    cell.textLabel.text = text;
                    
                    if(actualIndexPath.row==2)
                    {
                        
                        cell.detailTextLabel.text = self.workshopBays.length ? self.workshopBays : @"N/A";
                    }
                    else if(actualIndexPath.row==3)
                    {
                        cell.detailTextLabel.text = self.repairsPerWeek.length ? self.repairsPerWeek : @"N/A";
                    }
                }
                break;
                    //POPOVER
                case 1:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,1];
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    
                    UIImageView *catLogoView=nil;
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                        
                        catLogoView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(cell.contentView.bounds)-100.0, (CGRectGetHeight(cell.contentView.bounds)-20.0)/2.0, 20.0, 20.0)];
                        catLogoView.tag = 3;
                        [cell.contentView addSubview:catLogoView];
                        [catLogoView release];
                        
                        
                        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:13.0];
                        cell.detailTextLabel.textColor = [UIColor blackColor];
                        cell.textLabel.textAlignment = NSTextAlignmentLeft;
                    }
                    else
                    {
                        catLogoView = (UIImageView *)[cell.contentView viewWithTag:3];
                    }
                    
                    cell.textLabel.text = text;
                    cell.detailTextLabel.text = self.categoryName?self.categoryName:@"Select";
                    
                    catLogoView.image = self.categoryName ? [UIImage imageNamed:[NSString stringWithFormat:@"%@_logo.png",[self.categoryName lowercaseString]]] : nil;
                }
                break;
                    
                case 4:
                case 5:
                case 6:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,actualIndexPath.row];
                    
                    NSIndexPath *key=actualIndexPath;
                    //[NSNumber numberWithInteger:actualIndexPath.section*10+indexPath.row];
                    UITextField *textField = nil;
                    
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
                        
                        
                        cell.textLabel.font=[UIFont grotesqueBoldFontOfSize:13.0];
                    }
                    else
                    {
                        textField = (UITextField *)cell.accessoryView;
                        [textField removeFromSuperview];
                        cell.accessoryView = nil;
                    }
                    
                    textField=[textfieldContainerDict objectForKey:key];
                    
                   
                    
                    if(!textField)
                    {
                         //CGRect _textFieldRect = CGRectMake(10.0, 1.0, CGRectGetWidth(cell.contentView.bounds)-150.0, CGRectGetHeight(cell.contentView.bounds)-2.0);
                        
                        CGRect _textFieldRect = CGRectMake(0.0, 1.0, 167.0, CGRectGetHeight(cell.contentView.bounds)-2.0);
                        
                        textField=[UITextField textFieldWithFrame:_textFieldRect font:13 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
                        
                        textField.delegate=self;
                        textField.placeholder = @"Membership number";
                        textField.tag = 1;
                        textField.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        // textField.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin;
                        textField.textAlignment = NSTextAlignmentLeft;
                        [textfieldContainerDict setObject:textField forKey:key];
                        NSString *_text = [self textFieldValueForIndexPath:actualIndexPath];
                        textField.text = _text.length?_text:nil;
                        //textField.hidden=!_text.length;
                    }
                    
                     cell.accessoryView = textField;
                     textField.keyboardType = [self keyboardTypeForIndexPath:actualIndexPath];
                   
                    // [cell.contentView addSubview:textField];
                    
                    NSString *logoImageName = [self membershipLogoImageForIndexPath:actualIndexPath];
                    cell.imageView.image = [UIImage imageNamed:logoImageName];
                    cell.textLabel.text = text;
                   
                    
                }
                break;
                default: break;
            }
            
        }
            break;
            
        case 1:
        {
            CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
            NSIndexPath *key=actualIndexPath;
            //[NSNumber numberWithInteger:actualIndexPath.section*10+indexPath.row];
            UITextField *textField = nil;
            
            cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            }
            else
            {
                textField=(UITextField *)[cell.contentView viewWithTag:1];
                [textField removeFromSuperview];
            }
            
            
            textField = [textfieldContainerDict objectForKey:key];
            if(!textField)
            {
                textField=[UITextField textFieldWithFrame:CGRectMake(10.0, 1.0, CGRectGetWidth(cell.contentView.bounds)-14.0, CGRectGetHeight(cell.contentView.bounds)-2.0) font:13 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
                textField.delegate=self;
                textField.placeholder=text;
                textField.tag = 1;
                textField.font = [UIFont grotesqueBoldFontOfSize:13.0];
                textField.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;;
                [textfieldContainerDict setObject:textField forKey:key];
                textField.text = [self textFieldValueForIndexPath:actualIndexPath];
            }

            textField.keyboardType = [self keyboardTypeForIndexPath:actualIndexPath];
            [cell.contentView addSubview:textField];
        }
        break;
            //=================== TERRITORY POPOVER ===================================
        case 2:
        {
            CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
            cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil)
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                cell.detailTextLabel.textColor = [UIColor blackColor];
                cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:13.0];
            }
            
            cell.textLabel.text = text;
            
            // cell.detailTextLabel.text = self.customerTerritory.integerValue ? [self.customerTerritory description] : @"Select";
            
            cell.detailTextLabel.text = self.customerTerritoryAbbreviation.length ? self.customerTerritoryAbbreviation : @"Select";
        }
            break;
            
        //ADDRESS
        case 3:
        {
            switch (actualIndexPath.row)
            {
                    //TEXT FIELD
                case 0:
                case 1:
                    // case 3:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
                    
                    NSIndexPath *key=actualIndexPath;
                    //[NSNumber numberWithInteger:actualIndexPath.section*10+indexPath.row];
                    UITextField *textField = nil;
                    
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                    }
                    else
                    {
                        textField=(UITextField *)[cell.contentView viewWithTag:1];
                        [textField removeFromSuperview];
                    }
                    
                    
                    textField=[textfieldContainerDict objectForKey:key];
                    if(!textField)
                    {
                        textField=[UITextField textFieldWithFrame:CGRectMake(10.0, 1.0, CGRectGetWidth(cell.contentView.bounds)-14.0, CGRectGetHeight(cell.contentView.bounds)-2.0) font:13 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
                        textField.delegate=self;
                        textField.placeholder=text;
                        textField.tag = 1;
                        textField.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        textField.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
                        [textfieldContainerDict setObject:textField forKey:key];
                        textField.text = [self textFieldValueForIndexPath:actualIndexPath];
                    }
                    
                    textField.keyboardType = [self keyboardTypeForIndexPath:actualIndexPath];
                    [cell.contentView addSubview:textField];
              
                }
                break;
                    
                    
                    
                    
                    //LOCATION POPOVER
                case 2:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,2];
                    
                    UILabel *suburbLabel = nil;
                    UILabel *stateLabel  = nil;
                    UILabel *postCodeLabel  = nil;
                    
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                        
                        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        cell.textLabel.textColor = [UIColor lightGrayColor];
                        
                        suburbLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 1.0f, 200.0f, CGRectGetHeight(cell.contentView.bounds)-2.0)];
                        suburbLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
                        suburbLabel.backgroundColor = [UIColor clearColor];
                        suburbLabel.textColor = [UIColor blackColor];
                        suburbLabel.font = [UIFont grotesqueBoldFontOfSize:13.0f];
                        suburbLabel.numberOfLines = 2;
                        suburbLabel.tag = 1;
                        [cell.contentView addSubview:suburbLabel];
                        [suburbLabel release];
                        
                        postCodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(cell.contentView.bounds)-50.0f, 1.0f, 40.0f, CGRectGetHeight(cell.contentView.bounds)-2.0f)];
                        postCodeLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
                        postCodeLabel.backgroundColor = [UIColor clearColor];
                        postCodeLabel.textColor = [UIColor blackColor];
                        postCodeLabel.font = [UIFont grotesqueBoldFontOfSize:13.0f];
                        postCodeLabel.textAlignment = UITextAlignmentRight;
                        postCodeLabel.tag = 2;
                        // postCodeLabel.hidden = YES;
                        [cell.contentView addSubview:postCodeLabel];
                        [postCodeLabel release];
                        
                        stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(postCodeLabel.frame)-60.0f, 1.0f, 50.0f, CGRectGetHeight(cell.contentView.bounds)-2.0f)];
                        stateLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
                        stateLabel.backgroundColor = [UIColor clearColor];
                        stateLabel.textColor = [UIColor darkGrayColor];
                        stateLabel.textAlignment = UITextAlignmentRight;
                        stateLabel.font = [UIFont grotesqueBoldFontOfSize:13.0f];
                        stateLabel.tag = 3;
                        // stateLabel.hidden = YES;
                        [cell.contentView addSubview:stateLabel];
                        [stateLabel release];
                        
                        
                    }
                    else
                    {
                        suburbLabel   = (UILabel *)[cell.contentView viewWithTag:1];
                        postCodeLabel = (UILabel *)[cell.contentView viewWithTag:2];
                        stateLabel    = (UILabel *)[cell.contentView viewWithTag:3];
                    }
                    
                    if(!self.customerSuburb.length || !self.customerState.length || !self.customerPostCode.length)
                    {
                        
                        suburbLabel.text = @"Select location *";
                        //suburbLabel.textColor = [UIColor blackColor];
                    }
                    else
                    {
                        suburbLabel.textColor = [UIColor blackColor];
                        suburbLabel.text   = self.customerSuburb;
                        stateLabel.text    = self.customerState;
                        postCodeLabel.text = self.customerPostCode;
                    }
                    
                }
                    break;
                    
                    
                default: break;
            }
            
            
        }
            break;
            
            //TEXT
        case 4: //MAIN CONTACT & EMAIL
        {
            switch (actualIndexPath.row)
            {
                    //TEXT FIELD
                case 0:
                case 1:
                {
                    CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
                    NSIndexPath *key=actualIndexPath;
                    UITextField *textField = nil;
                    
                    cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                    if (cell == nil)
                    {
                        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                    }
                    else
                    {
                        textField=(UITextField *)[cell.contentView viewWithTag:1];
                        [textField removeFromSuperview];
                    }
                    
                    
                    textField=[textfieldContainerDict objectForKey:key];
                    if(!textField)
                    {
                        textField=[UITextField textFieldWithFrame:CGRectMake(10.0, 1.0, CGRectGetWidth(cell.contentView.bounds)-14.0, CGRectGetHeight(cell.contentView.bounds)-2.0) font:13 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
                        textField.delegate=self;
                        textField.placeholder=text;
                        textField.tag = 1;
                        textField.font = [UIFont grotesqueBoldFontOfSize:13.0];
                        textField.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
                        [textfieldContainerDict setObject:textField forKey:key];
                        textField.text = [self textFieldValueForIndexPath:actualIndexPath];
                    }

                    textField.keyboardType = [self keyboardTypeForIndexPath:actualIndexPath];
                    [cell.contentView addSubview:textField];
                    
                    
                }
                break;
            }
            
        }
            break;
            
            // case 25:
        case CONTACT_SECTION_END+2:
        {
            // NSIndexPath *key=actualIndexPath;
            CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
            cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
                cell.textLabel.font  = [UIFont grotesqueBoldFontOfSize:13.0];
            }
            
            //NSArray *arr= (actualIndexPath.section == 24) ? dealingOEMList : notDealingOEMList;
            
            NSArray *arr=  dealingOEMList ;
            NSDictionary *infoDict = (actualIndexPath.row<[arr count]) ? [arr objectAtIndex:actualIndexPath.row] : nil;;
            NSString *title  = [infoDict objectForKey:@"title"];
            cell.textLabel.text =title;
            
            NSLog(@"Title:%@",title);
            
            NSString *oemID=[infoDict objectForKey:@"oemID"];
            
            
            //BOOL isSelected = ([selectedOEMs containsObject:title]);
            BOOL isSelected = ([selectedOEMs containsObject:oemID]);
            
            
            //cell.accessoryType = isSelected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
            if(isSelected)
            {
                cell.accessoryType =UITableViewCellAccessoryCheckmark;
                NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                [dict setValue:oemID forKey:@"id"];
                [dict setValue:title forKey:@"title"];
                [arrOEM addObject:dict];
                [dict release];
                
                NSLog(@" arrOEM %@",arrOEM);
            }
            else
            {
                cell.accessoryType =UITableViewCellAccessoryNone;
                
            }
            
        }
            break;
            
            //============================== NOTES ================================
            // case 26:
        case CONTACT_SECTION_END+3:
        {
            //if(_isNotesExpanded)
            {
                KTTextView *textView=nil;
                CellIdentifier = [NSString stringWithFormat:@"%d_%d_expanded",actualIndexPath.section,0];
                cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                if (cell == nil)
                {
                    cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                    
                    textView=[[KTTextView alloc] initWithFrame:CGRectMake(5.0, 5.0, CGRectGetWidth(cell.contentView.bounds)-10.0, CGRectGetHeight(cell.contentView.bounds)-10.0)];
                    textView.delegate = self;
                    textView.tag = 1;
                    textView.autocorrectionType = UITextAutocorrectionTypeNo;
                    textView.font = [UIFont grotesqueFontOfSize:13.0];
                    textView.placeholderText = @"Enter notes";
                    textView.textColor = [UIColor blackColor];
                    textView.contentInset = UIEdgeInsetsMake(2.0, 0, 0, 0);
                    textView.backgroundColor  = [UIColor clearColor];
                    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleHeight;
                    [cell.contentView addSubview:textView];
                    
                    //  if(!textView.text.length) [textView becomeFirstResponder];
                    [textView release];
                    
                }
                else
                {
                    textView = (KTTextView *)[cell.contentView viewWithTag:1];
                }
                
                textView.text = self.customerNotes ? self.customerNotes : nil;
            }
            
        }
            break;
            
        case 5:
        {
            CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
            cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil)
            {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:15.0];
                cell.textLabel.backgroundColor = [UIColor clearColor];
                
                cell.textLabel.shadowColor= [UIColor lightGrayColor];
                cell.textLabel.shadowOffset = CGSizeMake(1.0, 1.0);
                
                UIView *bgView=[[UIView alloc] initWithFrame:cell.contentView.bounds];
                bgView.backgroundColor = [UIColor whiteColor];
                cell.backgroundView = bgView;
                [bgView release];
            }
            
            
            cell.textLabel.text = text;
        }
            break;
            //case 24:
        case CONTACT_SECTION_END+1:
        {
            CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,0];
            cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil)
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                
                //   cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
                cell.textLabel.backgroundColor = [UIColor clearColor];
                
                
                UIImage *addNew = [UIImage imageNamed:@"add_new_contact.png"];
                UIButton *addNewButton = [UIButton buttonWithType:UIButtonTypeCustom];
                addNewButton.frame = CGRectMake(0, 0, addNew.size.width, addNew.size.height);
                [addNewButton setImage:addNew forState:UIControlStateNormal];
                [addNewButton setImage:[UIImage imageNamed:@"add_new_contact.png"] forState:UIControlStateHighlighted];
                [cell.contentView addSubview:addNewButton];
                [addNewButton addTarget:self action:@selector(addNewContactAction:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
            break;
            
        default:
        {
            CellIdentifier = [NSString stringWithFormat:@"%d_%d",actualIndexPath.section,actualIndexPath.row];
            
            NSIndexPath *key=actualIndexPath;
            
            NSLog(@"Key section:%d row:%d",key.section,key.row);
            UITextField *textField = nil;
            
            cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil || _currentContactSection==indexPath.section)
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                
                if(!textField)
                {
                    textField=[UITextField textFieldWithFrame:CGRectMake(10.0, 1.0, CGRectGetWidth(cell.contentView.bounds)-14.0, CGRectGetHeight(cell.contentView.bounds)-2.0) font:13 fontColor:[UIColor blackColor] bgColor:[UIColor clearColor] text:nil borderStyle:UITextBorderStyleNone];
                    textField.delegate=self;
                    textField.placeholder=text;
                    textField.tag = 1;
                    textField.font = [UIFont grotesqueBoldFontOfSize:13.0];
                    textField.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
                    [cell.contentView addSubview:textField];
                    
                }
                
                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeAction:)];
                    rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
                    rightSwipe.numberOfTouchesRequired = 1;
                    [cell addGestureRecognizer:rightSwipe];
                    [rightSwipe release];
                    
                    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeAction:)];
                    leftSwipe.numberOfTouchesRequired = 1;
                    leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
                    [cell addGestureRecognizer:leftSwipe];
                    [leftSwipe release];
                }
                
            }
            else
            {
                textField=(UITextField *)[cell.contentView viewWithTag:1];
                //[textField removeFromSuperview];
            }
            
            textField.text = [self textFieldValueForIndexPath:indexPath];
            textField.keyboardType = [self keyboardTypeForIndexPath:actualIndexPath];
            
            BOOL __isSwiped = (swipedSection == indexPath.section);
            textField.userInteractionEnabled = !__isSwiped;
            
            
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                if(__isSwiped)
                {
                    UIImage *deleteImage = [UIImage imageNamed:@"minus_button.png"];
                    UIButton * deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
                    deleteButton.frame = CGRectMake(0.0, 0.0, deleteImage.size.width, deleteImage.size.height);
                    [deleteButton setBackgroundImage:deleteImage forState:UIControlStateNormal];
                    [deleteButton addTarget:self action:@selector(deleteContactAction:) forControlEvents:UIControlEventTouchUpInside];
                    deleteButton.hidden = (indexPath.row!=2);
                    cell.editingAccessoryView = deleteButton;
                }
                else if(cell.editingAccessoryView)
                {
                    cell.editingAccessoryView = nil;
                }
                
            }
            
            
        }
            break;
    }
    
    
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
    
    return cell;
}

-(void)swipeAction:(UISwipeGestureRecognizer *)recognizer
{
    UITableViewCell *cell  = (UITableViewCell *) [recognizer view];
    NSIndexPath *indexPath =  [self.tableView indexPathForCell:cell];
    
    NSMutableIndexSet *indexSetToReload = [NSMutableIndexSet indexSet];
    
    if(swipedSection!=NSNotFound)
    {
        [indexSetToReload addIndex:swipedSection];
    }
    
    if(swipedSection!=indexPath.section)
    {
        swipedSection = indexPath.section;
        [indexSetToReload addIndex:swipedSection];
    }
    else
    {
        swipedSection = NSNotFound;
    }
    
    [self.tableView reloadSections:indexSetToReload withRowAnimation:UITableViewRowAnimationNone];
    
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int count = [dataSource count];

    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableArray *rows = (section<[dataSource count]) ?  [dataSource objectAtIndex:section] : nil;

    
    return MAX(1,[rows count]);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray *rows = (indexPath.section < [dataSource count]) ?  [dataSource objectAtIndex:indexPath.section] : nil;
    NSDictionary *rowInfo = (indexPath.row < [rows count]) ? [rows objectAtIndex:indexPath.row] : nil;
   // if(!rowInfo || ![rowInfo isKindOfClass:[NSDictionary class]]) return tableView.rowHeight;
    NSIndexPath *actualIndexPath = [rowInfo valueForKey:@"location"];
    
   // return (actualIndexPath.section == 26) ? MAX(50.0,_textViewHeight) : tableView.rowHeight;
    
    CGFloat _height = tableView.rowHeight;
    //if(actualIndexPath.section == 26)
    if(actualIndexPath.section == CONTACT_SECTION_END+3)
    {
        _height = MAX(50.0,_textViewHeight);
        _height = MIN(_height,150.0);                      
    }
    else if(actualIndexPath.section == 5)
    {
        _height=18.0;
    }

                      
    return _height;
    
    //return (actualIndexPath.section == 6 && _isNotesExpanded) ? MAX(50.0,_textViewHeight) : tableView.rowHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSMutableArray *rows = (section < [dataSource count]) ?  [dataSource objectAtIndex:section] : nil;
    NSDictionary *rowInfo = ([rows count]) ? [rows objectAtIndex:0] : nil;
    NSIndexPath *actualIndexPath = [rowInfo valueForKey:@"location"];
    
    NSLog(@"Class:%@",NSStringFromClass([actualIndexPath class]));
    //if(actualIndexPath.section == 24 || actualIndexPath.section ==25 || actualIndexPath.section ==4) return 30.0;
   // if(actualIndexPath.section == 25) return 30.0;
    
     if(actualIndexPath.section == CONTACT_SECTION_END+2) return 30.0;
    
   // else if(numberOfContacts==1 && actualIndexPath.section >= CONTACT_SECTION_START && actualIndexPath.section<=CONTACT_SECTION_END )   return 30.0;
    
   // else if(actualIndexPath.section == CONTACT_SECTION_START && numberOfContacts>=1 )  return 30.0;
    
    return 0.0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *title=nil;
    NSMutableArray *rows = (section < [dataSource count]) ?  [dataSource objectAtIndex:section] : nil;
    NSDictionary *rowInfo = ([rows count]) ? [rows objectAtIndex:0] : nil;
    NSIndexPath *actualIndexPath = [rowInfo valueForKey:@"location"];
    
    //if(actualIndexPath.section != 4 || actualIndexPath.section !=5) return nil;
    
    //if(actualIndexPath.section == 25 )      title      = @"    Purchases";
    
     if(actualIndexPath.section == CONTACT_SECTION_END+2 )      title      = @"    Purchases";
    
    //@"    OEMs Dealt With Us";
    //else if(actualIndexPath.section == 25 ) title      = @"    OEMs Dealt With Others";
    // else if(numberOfContacts==1 && section == CONTACT_SECTION_START )  title      = @"    Contacts";
   // else if(numberOfContacts==1 && actualIndexPath.section >= CONTACT_SECTION_START && actualIndexPath.section<=CONTACT_SECTION_END )  title      = @"    Contacts";
   // else if(actualIndexPath.section == CONTACT_SECTION_START && numberOfContacts>=1 )  title      = @"    Contacts";
    
    UILabel *label=nil;
    if(title)
    {
    label=[[UILabel alloc] initWithFrame:CGRectMake(0.0, 0, CGRectGetWidth(tableView.bounds), 30.0)];
    label.font = [UIFont grotesqueBoldFontOfSize:15.0];
    label.shadowColor= [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(1.0, 1.0);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.text = title;
    }
    
    return [label autorelease];
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    //return (section == 4+numberOfContacts)? 60.0 : 0.0 ;
    return (section == CONTACT_SECTION_START-1)? 0.1:5.0 ;
}
/*
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(section!=4+numberOfContacts) return nil;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 60.0)];
    footerView.backgroundColor = [UIColor clearColor];
    
    UIButton *addNewContactButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addNewContactButton.frame = CGRectMake(10.0, 11.0, CGRectGetWidth(footerView.bounds)-20.0, 44.0);
   // [addNewContactButton setTitle:@"Add new contact" forState:UIControlStateNormal];
    [addNewContactButton setBackgroundImage:[UIImage imageNamed:@"add_new_contact.png"] forState:UIControlStateNormal];
    [addNewContactButton setBackgroundImage:[UIImage imageNamed:@"add_new_contact_selected.png"] forState:UIControlStateHighlighted];
    [footerView addSubview:addNewContactButton];
    [addNewContactButton addTarget:self action:@selector(addNewContactAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return [footerView autorelease];
}
*/



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *rows = (indexPath.section < [dataSource count]) ?  [dataSource objectAtIndex:indexPath.section] : nil;
    
    NSDictionary *rowInfo = (indexPath.row < [rows count]) ? [rows objectAtIndex:indexPath.row] : nil;
    
   // UITableViewCell *cell = [self configureCellForIndexPath:indexPath withRowInfo:rowInfo];
    
     UITableViewCell *cell = [self _configureCellForIndexPath:indexPath withRowInfo:rowInfo];
        
  
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    NSMutableArray *rows = (indexPath.section < [dataSource count]) ?  [dataSource objectAtIndex:indexPath.section] : nil;
    NSDictionary *rowInfo = (indexPath.row < [rows count]) ? [rows objectAtIndex:indexPath.row] : nil;
    NSIndexPath *actualIndexPath = [rowInfo objectForKey:@"location"];
    
    BOOL _isContactCell =  ( actualIndexPath.section>=CONTACT_SECTION_START && actualIndexPath.section<=CONTACT_SECTION_END);
    
    cell.backgroundColor= [UIColor whiteColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    
    if(indexPath.section==5) return;
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    //_bgView.opaque = YES;
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset =  CGPointMake(10.0, 0.0);
    
    _bgView.contentInset = _isContactCell ? CGPointMake(10.0, 0.0) : CGPointMake(10.0, 0.0);
    
    BOOL __isSwiped = (swipedSection == indexPath.section);
    if(_isContactCell)
    {
        _bgView.contentEdgeInset = UIEdgeInsetsMake(0, 0.0, 0,__isSwiped? 40.0 : 0.0);
    }

    
    
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];;
    _bgView.tag = 1;
    
    
    int numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
}


#pragma mark ADD NEW CONTACT
-(void)addNewContactAction:(UIButton *)sender
{
    NSMutableIndexSet *indexSetToInsert = [NSMutableIndexSet indexSet];
    if(numberOfContacts==0)
    {
        NSMutableArray *rows=[NSMutableArray array];
        NSDictionary *rowInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"Contacts",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:0 inSection:CONTACT_SECTION_START-1],@"location",nil];
        [rows addObject:rowInfo];
        [dataSource  insertObject:rows atIndex:CONTACT_SECTION_START-1];
        
        [indexSetToInsert addIndex:CONTACT_SECTION_START-1];
        
     // [self.tableView insertSections:[NSIndexSet indexSetWithIndex:5] withRowAnimation:UITableViewRowAnimationNone];
    }
    
    NSArray *fields = [NSArray arrayWithObjects:@"Name",@"Title",@"Direct",@"Mobile",@"Email",nil];
    int _section    = [self addNewContactSectionWithFields:fields];

    if(_section==NSNotFound) return;
    
    
    [_newContactSections addObject:[NSNumber numberWithInt:_section]];
    
    _currentContactSection=_section;
    
    
    /*
    int _section =4+numberOfContacts;
    int _row=0;

    NSMutableArray *rows=[NSMutableArray array];
    
    for(NSString *fieldName in fields)
    {
        NSDictionary *rowInfo = [NSDictionary dictionaryWithObjectsAndKeys:fieldName,DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:_row inSection:_section],@"location",nil];
        
        [rows addObject:rowInfo];
        _row++;
    }
    
    [dataSource insertObject:rows atIndex:_section];
    numberOfContacts++;
    */
    
    CustomerContact *newContact = self.editingContext ? (CustomerContact *)[CustomerContact insertNewObjectInContext:self.editingContext] : (CustomerContact *)[CustomerContact insertNewObject];
    //
    long _localContactID = [[NSDate date] hash];
    newContact.oldContactID = [NSNumber numberWithLong:_localContactID] ;
    //newContact.createdDate  = [NSNumber numberWithDouble:[NSDate timeIntervalSinceReferenceDate]];
    
    NSTimeInterval _dateInterval =[[NSDate date] GMTTimeIntervalSince1970]*1000;
    newContact.createdDate  = [NSNumber numberWithDouble:_dateInterval];
    
  ;
    
    if(!addedCustomerContacts) addedCustomerContacts = [[NSMutableArray alloc] init];
    [addedCustomerContacts addObject:newContact];
    
    [indexSetToInsert addIndex:_section];
    [self.tableView beginUpdates];
   // [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:_section-1] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView insertSections:indexSetToInsert withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];

    
   /*
    NSIndexPath *newIndexPath=[NSIndexPath indexPathForRow:4 inSection:_section];
    //[self.tableView scrollToRowAtIndexPath:newIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    self.theNewContactLastIndexPath= newIndexPath;
   */
   
    
  //========================== makeFirstResponder to textField at first row ======================
    _shouldScrollToSelectedField = YES;
     NSIndexPath *firstResponderIndexPath=[NSIndexPath indexPathForRow:0 inSection:_section];
	UITableViewCell *cell  = [self.tableView cellForRowAtIndexPath:firstResponderIndexPath];
    UITextField *textField = (UITextField *)[cell.contentView viewWithTag:1];
    self.selectedTextField = textField;
	[textField becomeFirstResponder];
    
    //======================== SCROLLS TO CREATED CONTACT SECTION ================================
    NSIndexPath *newIndexPath=[NSIndexPath indexPathForRow:4 inSection:_section];
    CGRect _targetRect = [self.tableView rectForSection:_section];
    [self.tableView scrollRectToVisible:_targetRect animated:YES];
    self.theNewContactLastIndexPath= newIndexPath;
    
    

}
#pragma mark -

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *rows = (indexPath.section < [dataSource count]) ?  [dataSource objectAtIndex:indexPath.section] : nil;
    NSDictionary *rowInfo = (indexPath.row < [rows count]) ? [rows objectAtIndex:indexPath.row] : nil;
    NSIndexPath *actualIndexPath = [rowInfo objectForKey:@"location"];
    
    NSLog(@"Row info:%@",rowInfo);

    switch (actualIndexPath.section) 
    {
        case 0:
        {
            switch (actualIndexPath.row) 
            {
                case 1:
                    [self selectCategoyForIndexPath:indexPath];
                break;
                    
                case 2:
                case 3:
                {
                    [self selectNumberPickerForIndexPath:actualIndexPath];
                }
                break;
                    /*
                case 4:
                    [self selectGenuineMemberForIndexPath:indexPath];
                break;
                    */
                case 4:
                case 5:
                case 6:
                  //  [self selectMembershipForIndexPath:indexPath];
                break;

                default:break;
            }
        }
        break;
        case 2:
        {
            [self selectTerritoryForIndexPath:indexPath];
        }
        break;
        case 3:
        {
            if(actualIndexPath.row==2)
            [self selectSuburbForIndexPath:indexPath];
        }
        break;
        
        //case 24:
        case CONTACT_SECTION_END+1:
        {
            [self addNewContactAction:nil];
        }
        break;
            
        //case 25:
        case CONTACT_SECTION_END+2:
        {
            NSString *oemName=[rowInfo valueForKey:@"title"];
            NSString *oemID=[rowInfo valueForKey:@"oemID"];
            
            NSLog(@"Selected Title:%@",oemName);
            
            if([selectedOEMs containsObject:oemID])
            {
                [selectedOEMs removeObject:oemID];
                
              //========ADD OEMS TO DESELECTED ARRAY HERE==========
                [notDealingOEMList  addObject:oemID];
              //===================================================
            }
            else
            {
                [selectedOEMs addObject:oemID];
                
                [notDealingOEMList  removeObject:oemID];
            }
            
            if(indexPath)
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        break;
        
       // case 26:
        case CONTACT_SECTION_END+3:
        {
            _isNotesExpanded=YES;
            //[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
        }
        break;
            
        default: 
        break;
    }
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *rows = (indexPath.section < [dataSource count]) ?  [dataSource objectAtIndex:indexPath.section] : nil;
    NSDictionary *rowInfo = (indexPath.row < [rows count]) ? [rows objectAtIndex:indexPath.row] : nil;
    NSIndexPath *actualIndexPath = [rowInfo objectForKey:@"location"];

    
    return  ( actualIndexPath.section>=CONTACT_SECTION_START && actualIndexPath.section<=CONTACT_SECTION_END);
    
   // return _currentContactSection!=indexPath.section && ( actualIndexPath.section>=CONTACT_SECTION_START && actualIndexPath.section<=CONTACT_SECTION_END);
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
    return UITableViewCellEditingStyleNone ;
    }
    else
    {
    return (indexPath.row==2)?UITableViewCellEditingStyleDelete : UITableViewCellEditingStyleNone;
    }
    
     return UITableViewCellEditingStyleNone ;
}

/*
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Sure to delete?";
}
*/


- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? NO : YES;
}


#pragma mark Delete Contact
-(void)deleteContactAction:(UIButton *)sender
{
    /*
    UIAlertView *alert = [UIAlertView alertViewWithTitle:@"" message: CUSTOMER_CONTACT_DELETE_MESSAGE cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObjects:@"Yes", nil] onDismiss:^(int buttonIndex)
    {
        UITableViewCell * cell =( UITableViewCell *) [sender superViewOfType:[UITableViewCell class]];
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        [self tableView:self.tableView commitEditingStyle:UITableViewCellEditingStyleDelete forRowAtIndexPath:indexPath];
        
    } onCancel:^{
        
    }];
    
    [alert release];
     */
    
    swipedSection = NSNotFound;
    UITableViewCell * cell =( UITableViewCell *) [sender superViewOfType:[UITableViewCell class]];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    [self tableView:self.tableView commitEditingStyle:UITableViewCellEditingStyleDelete forRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) 
    {
        NSNumber *numSection = [NSNumber numberWithInt:indexPath.section];
        
        BOOL contactDeleted=NO;
        
        if([_newContactSections containsObject:numSection])
        {
            [_newContactSections removeObject:numSection];
            if(!contactDeleted)
                contactDeleted=YES;
        }
        
       // if([_addedContactSections containsObject:numSection])
        {
           // [_addedContactSections removeObject:numSection];
            
            int _section = indexPath.section-CONTACT_SECTION_START;
            CustomerContact *_contact =(_section < [addedCustomerContacts count]) ? [addedCustomerContacts objectAtIndex:_section] : nil;
            
           // CustomerContact *_contact = indexPath.section < [addedCustomerContacts count] ?  [addedCustomerContacts objectAtIndex:indexPath.section] : nil;
            
            if(_contact)
            {
                [_deletedContacts addObject:_contact];
                [addedCustomerContacts removeObjectAtIndex:_section];
                
               // [addedCustomerContacts removeObject:_contact];
            
            }
            
            if(!contactDeleted)
                contactDeleted=YES;

        }
        
        
        
        if(contactDeleted)
        {
        // Delete the row from the data source
        numberOfContacts--;
            
        /*
        //===============================
        NSMutableIndexSet *indexSetOfObjects = [NSMutableIndexSet indexSetWithIndex:indexPath.section];
        for(int i=indexPath.section+1;i<[addedCustomerContacts count];i++)
        {
            [indexSetOfObjects addIndex:i];
        }
            
           NSArray *objects= [dataSource objectsAtIndexes:indexSetOfObjects];
            
            for(NSMutableArray *rows in objects)
            {
                 NSMutableDictionary *rowInfo = (indexPath.row < [rows count]) ? [rows objectAtIndex:indexPath.row] : nil;
                 NSIndexPath *_indexPath = [rowInfo valueForKey:@"location"];
                [rowInfo setValue:[NSIndexPath indexPathForRow:0 inSection:_indexPath.section-1] forKey:@"location"];
            }
        //========================    
        */
            
        [dataSource removeObjectAtIndex:indexPath.section]; 
            
        NSMutableIndexSet *indexSet=[NSMutableIndexSet indexSetWithIndex:indexPath.section];
        if(numberOfContacts<=0)
        {
            [dataSource removeObjectAtIndex:CONTACT_SECTION_START-1];
            [indexSet addIndex:CONTACT_SECTION_START-1];
        }
            
            NSLog(@"indexpath deleted=%d",indexPath.section);
            [self.tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationTop];
            
        }
    }   
    
}
#pragma mark SELECT TERRITORY
-(void)selectTerritoryForIndexPath:(NSIndexPath *)indexPath
{
    [self.selectedTextField resignFirstResponder];
    
    UIPopoverController *popover=self.territoryPopover;
    if(!popover)
    {
        TerritoryViewController *territoryViewController = [[TerritoryViewController alloc] init];
        territoryViewController.delegate = self;
        
        /*
        [territoryViewController addTerritorySelectedCallback:^(Territory *territory)
         {
             NSIndexPath *targetIndexPath = [NSIndexPath  indexPathForRow:0 inSection:2];
             UITableViewCell *cell        = [self.tableView cellForRowAtIndexPath:targetIndexPath];
            // cell.detailTextLabel.text    = territory.territoryPosition.intValue ? [territory.territoryPosition description] : @"N/A";
         
             cell.detailTextLabel.text    = territory.territoryAbbreviation ? territory.territoryAbbreviation : @"N/A";
             
             self.customer.previousTerritory = self.customerTerritoryID;
             //self.customerTerritory = territory.territoryPosition.intValue ? territory.territoryPosition : [NSNumber numberWithInt:0];
             self.customerTerritoryID = territory.territoryID;
             
             
             NSLog(@"territoryPosition=%@",self.customer.previousTerritory);
              //NSLog(@"territoryPosition=%@",self.customerTerritory);
             
             [self.territoryPopover dismissPopoverAnimated:YES];
        }];
        */
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:territoryViewController];
        popover=[[UIPopoverController alloc] initWithContentViewController:navController];
        popover.popoverContentSize = CGSizeMake(320.0, 500.0);
        self.territoryPopover = popover;

        [popover release];
        [navController release];
        [territoryViewController release];
    }
    
    CGRect targetRect = [self.tableView rectForRowAtIndexPath:indexPath];
    [popover presentPopoverFromRect:targetRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
}

-(void)territoryViewController:(TerritoryViewController *)territoryViewController didSelectTerritory:(Territory  *)territory
{
    NSIndexPath *targetIndexPath = [NSIndexPath  indexPathForRow:0 inSection:2];
    UITableViewCell *cell        = [self.tableView cellForRowAtIndexPath:targetIndexPath];
   // cell.detailTextLabel.text    = territory.territoryPosition.intValue ? [territory.territoryPosition description] : @"N/A";
    
    cell.detailTextLabel.text    = territory.territoryAbbreviation ? territory.territoryAbbreviation : @"N/A";
    
    self.customer.previousTerritory = self.customerTerritoryID;
    //self.customerTerritory = territory.territoryPosition.intValue ? territory.territoryPosition : [NSNumber numberWithInt:0];
    self.customerTerritoryID = territory.territoryID;
    self.customerTerritoryAbbreviation = territory.territoryAbbreviation;
    
    
    NSLog(@"territoryPosition=%@",self.customer.previousTerritory);
    //NSLog(@"territoryPosition=%@",self.customerTerritory);
    
    [self.territoryPopover dismissPopoverAnimated:YES];
}

#pragma mark SELECT SUBURB FROM POPOVER
-(void)selectSuburbForIndexPath:(NSIndexPath *)indexPath
{
    UIPopoverController *popover=self.suburbPopover;
    if(!popover)
    {
        AddressViewController *suburbViewController = [[AddressViewController alloc] init];
        suburbViewController.delegate = self;
        
        /*
        //**** TO AVOID RETAIN CIRCLE NEED TO TAKE WEAK REFERENCE ****
       __block ProfileInfoEditViewController *weakSelf = self;
        
        [suburbViewController addAddressSelectedCallback:^(NSString *suburbName, NSString *stateName, NSString *pinCode) 
        {
            ProfileInfoEditViewController *strongSelf = weakSelf;
            
            strongSelf.customerSuburb   = suburbName;
            strongSelf.customerState    = stateName;
            strongSelf.customerPostCode = pinCode;
            
             NSIndexPath *targetIndexPath = [NSIndexPath  indexPathForRow:2 inSection:3];
            if(targetIndexPath)
            [strongSelf.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:targetIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            
            [strongSelf.suburbPopover dismissPopoverAnimated:NO];
        }];
        
        */
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:suburbViewController];
        popover=[[UIPopoverController alloc] initWithContentViewController:navController];
        popover.popoverContentSize = CGSizeMake(320.0, 800.0);
        self.suburbPopover = popover;
        
        [popover release];
        [navController release];
        [suburbViewController release];
    }
    
    CGRect targetRect = [self.tableView rectForRowAtIndexPath:indexPath];
    [popover presentPopoverFromRect:targetRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
}

-(void)addressViewController:(AddressViewController *)addressVC didSelectAddress:(NSDictionary *)addressDict
{
    NSString *suburbName = [addressDict valueForKey:TAG_NAME];
    NSString *stateName  = [addressDict valueForKey:TAG_STATE];
    NSString *pinCode    = [addressDict valueForKey:TAG_SUBURB_PINCODE];
    
    self.customerSuburb   = suburbName;
    self.customerState    = stateName;
    self.customerPostCode = pinCode;
    
    NSIndexPath *targetIndexPath = [NSIndexPath  indexPathForRow:2 inSection:3];
    if(targetIndexPath)
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:targetIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    
    [self.suburbPopover dismissPopoverAnimated:NO];
}
#pragma mark -

/*
-(void)selectCategoyForIndexPath:(NSIndexPath *)indexPath
{
    [self.selectedTextField resignFirstResponder];
    
    UIPopoverController *popover=self.categoryPopover;
    if(!popover)
    {
        CategoriesViewController *catViewController = [[CategoriesViewController alloc] init];
        
        [catViewController addCategoryDidSelectCallback:^(NSDictionary *category)
         {
             [self.categoryPopover dismissPopoverAnimated:YES];
             
             // UITableViewCell *cell      = [self.tableView cellForRowAtIndexPath:targetIndexPath];
             
             NSString *catNameBeforeSelection = self.categoryName;
             NSString *cat = [category valueForKey:@"categoryName"];
             if([cat isEqualToString:self.categoryName]) return ;
             self.categoryName = cat;
             
             //cell.detailTextLabel.text    = self.categoryName;
             
             NSIndexPath *targetIndexPath = [NSIndexPath  indexPathForRow:1 inSection:0];
             
             [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:targetIndexPath] withRowAnimation:UITableViewRowAnimationNone];
             
             if(!catNameBeforeSelection.length)
             {
                 NSMutableArray *rows   = [dataSource objectAtIndex:indexPath.section];
                 
                 NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:2 inSection:0];
                 NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:3 inSection:0];
                 
                 //if([self.categoryName isEqualToString:@"Mechanical"])
                 {
                     NSDictionary *rowInfo4 = [NSDictionary dictionaryWithObjectsAndKeys:@"Workshop Bays",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:2 inSection:0],@"location",nil];
                     NSDictionary *rowInfo5 = [NSDictionary dictionaryWithObjectsAndKeys:@"Repairs per Week(Average)",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:3 inSection:0],@"location",nil];
                     
                     [rows insertObject:rowInfo4 atIndex:2];
                     [rows insertObject:rowInfo5 atIndex:3];
                     
                     [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath1,indexPath2, nil] withRowAnimation:UITableViewRowAnimationBottom];
                 }
                 
                  else 
                  {
                  ///=== REMOVE @"Workshop Bays"  @"Workshop Bays" FROM ROWS ===               
                  NSMutableArray *arrToRemove = [NSMutableArray array];
                  
                  for(NSDictionary *dict in rows)
                  {
                  NSIndexPath *indexPath=[dict valueForKey:@"location"];
                  if(indexPath.row==2 || indexPath.row==3)
                  {
                  [arrToRemove addObject:dict];
                  }
                  }
                  
                  if([arrToRemove count])
                  {
                  [rows removeObjectsInArray:arrToRemove];
                  [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath1,indexPath2,nil] withRowAnimation:UITableViewRowAnimationTop];
                  }
                  
                  // [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
                  }
                  
//             }
//         } ];
        
    
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:catViewController];
        popover=[[UIPopoverController alloc] initWithContentViewController:navController];
        popover.popoverContentSize = CGSizeMake(320.0, 190.0);
        self.categoryPopover = popover;
        
        [popover release];
        [navController release];
        [catViewController release];
    }
    
    CGRect targetRect = [self.tableView rectForRowAtIndexPath:indexPath];
    [popover presentPopoverFromRect:targetRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
}
*/
#pragma mark SELECT CATEGORY
-(void)selectCategoyForIndexPath:(NSIndexPath *)indexPath
{
    
    [self.selectedTextField resignFirstResponder];
    
    UIPopoverController *popover=self.categoryPopover;
    if(!popover)
    {
        CategoriesViewController *catViewController = [[CategoriesViewController alloc] init];
        catViewController.delegate = self;
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:catViewController];
        popover=[[UIPopoverController alloc] initWithContentViewController:navController];
        popover.popoverContentSize = CGSizeMake(320.0, 190.0);
        self.categoryPopover = popover;

        [self setAssociateObject:indexPath forKey:@"currentCatIndexPath"];
        
 
        //[catNameBeforeSelection release];
        
               
        [popover release];
        [navController release];
        [catViewController release];
    }
    
    CGRect targetRect = [self.tableView rectForRowAtIndexPath:indexPath];
    [popover presentPopoverFromRect:targetRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
}

-(void)_categoriesViewController:(CategoriesViewController *)categories didSelectCategory:(NSDictionary *)category
{
    [self.categoryPopover dismissPopoverAnimated:YES];
    
    // UITableViewCell *cell      = [self.tableView cellForRowAtIndexPath:targetIndexPath];
    
   NSIndexPath *indexPath = [self associateObjectForKey:@"currentCatIndexPath"];
    
    NSString *catNameBeforeSelection = [self.categoryName copy];
    NSString *cat = [category valueForKey:@"categoryName"];
    if([cat isEqualToString:self.categoryName])
    {
        [catNameBeforeSelection release];
          [self setAssociateObject:nil forKey:@"currentCatIndexPath"];
        return ;
    }
    
    self.categoryName = cat;
    
    //cell.detailTextLabel.text    = self.categoryName;
    
    //======== RELOAD CATEGORY ROW TO REFELECT THE CHANGE =======================
    NSIndexPath *targetIndexPath = [NSIndexPath  indexPathForRow:1 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:targetIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    
    
    NSMutableArray *rows   = [dataSource objectAtIndex:indexPath.section];
    
    if([self.categoryName isEqualToString:@"Mechanical"] || [self.categoryName isEqualToString:@"All"])
    {
        NSMutableArray *rowsToInsert = [NSMutableArray array];
        
        if([catNameBeforeSelection isEqualToString:@"Collision"])
        {
            NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:2 inSection:0];
            
            NSDictionary *rowInfo4 = [NSDictionary dictionaryWithObjectsAndKeys:@"Workshop Bays",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:2 inSection:0],@"location",nil];
            
            [rows insertObject:rowInfo4 atIndex:2];
            
            [rowsToInsert addObject:indexPath1];
        }
        else if(!catNameBeforeSelection.length)
        {
            NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:2 inSection:0];
            NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:3 inSection:0];
            
            
            NSDictionary *rowInfo4 = [NSDictionary dictionaryWithObjectsAndKeys:@"Workshop Bays",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:2 inSection:0],@"location",nil];
            NSDictionary *rowInfo5 = [NSDictionary dictionaryWithObjectsAndKeys:@"Repairs per Week(Average)",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:3 inSection:0],@"location",nil];
            
            [rows insertObject:rowInfo4 atIndex:2];
            [rows insertObject:rowInfo5 atIndex:3];
            
            [rowsToInsert addObject:indexPath1];
            [rowsToInsert addObject:indexPath2];
        }
        
        if([rowsToInsert count])
            [self.tableView insertRowsAtIndexPaths:rowsToInsert withRowAnimation:UITableViewRowAnimationBottom];
        
    }
    else  if([self.categoryName isEqualToString:@"Collision"])
    {
        
        NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:2 inSection:0];
        if([catNameBeforeSelection isEqualToString:@"Mechanical"] || [catNameBeforeSelection isEqualToString:@"All"])
        {
            NSMutableArray *arrToRemove = [NSMutableArray array];
            
            for(NSDictionary *dict in rows)
            {
                NSIndexPath *indexPath=[dict valueForKey:@"location"];
                if(indexPath.row==2)
                {
                    [arrToRemove addObject:dict];
                }
            }
            
            if([arrToRemove count])
            {
                [rows removeObjectsInArray:arrToRemove];
                [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath1,nil] withRowAnimation:UITableViewRowAnimationTop];
            }
        }
        else
        {
            
            NSDictionary *rowInfo5 = [NSDictionary dictionaryWithObjectsAndKeys:@"Repairs per Week(Average)",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:3 inSection:0],@"location",nil];
            
            [rows insertObject:rowInfo5 atIndex:2];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath1 ,nil] withRowAnimation:UITableViewRowAnimationBottom];
        }
    }
    
    [catNameBeforeSelection release];
    
    [self setAssociateObject:nil forKey:@"currentCatIndexPath"];
}

-(void)categoriesViewController:(CategoriesViewController *)categories didSelectCategory:(NSDictionary *)category
{
    [self.categoryPopover dismissPopoverAnimated:YES];
    
    // UITableViewCell *cell      = [self.tableView cellForRowAtIndexPath:targetIndexPath];
    
 
    
   NSNumber *catID =  [category valueForKey:@"categoryID"];
   NSString *cat   = [category valueForKey:@"categoryName"];
    
     if(self.categoryID && [catID isEqualToNumber:self.categoryID])
     {
          [self setAssociateObject:nil forKey:@"currentCatIndexPath"];
         
         return ;
     }
    
    
    NSIndexPath *indexPath = [self associateObjectForKey:@"currentCatIndexPath"];
    
    self.categoryID = catID;
    self.categoryName = cat;
    
    //cell.detailTextLabel.text    = self.categoryName;
    
    //======== RELOAD CATEGORY ROW TO REFELECT THE CHANGE =======================
    NSIndexPath *targetIndexPath = [NSIndexPath  indexPathForRow:1 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:targetIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    
    NSMutableArray *rows    = [dataSource objectAtIndex:indexPath.section];
    
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:2 inSection:0];
    NSIndexPath *indexPath2 = [NSIndexPath indexPathForRow:3 inSection:0];
    
    NSArray *_indexPaths    =  [NSArray arrayWithObjects:indexPath1,indexPath2,nil];
    
    NSMutableArray *rowsToInsert = [NSMutableArray array];
    NSMutableArray *rowsToRemove = [NSMutableArray array];
    
    NSArray *_filteredArr =  [rows filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.location IN %@",_indexPaths ]];
    
    NSDictionary *rowInfo4 = nil;
    NSDictionary *rowInfo5 = nil;
    
     
    for(NSDictionary *rowDict in _filteredArr)
    {
        NSIndexPath *__indexPath = [rowDict valueForKey:@"location"];
        
        if([indexPath1 isEqual:__indexPath])
        {
            rowInfo4 = rowDict;
        }
        else if([indexPath2 isEqual:__indexPath])
        {
            rowInfo5 = rowDict;
        }
    }
    
    if([self.categoryID integerValue] == 0 || [self.categoryID integerValue] == 1)
     {
         
        if(!rowInfo4)
        {
            NSDictionary *rowInfo4 = [NSDictionary dictionaryWithObjectsAndKeys:@"Workshop Bays",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:2 inSection:0],@"location",nil];
            
            [rows insertObject:rowInfo4 atIndex:2];
            [rowsToInsert addObject:indexPath1];
        }
         
         
         if(!rowInfo5)
         {
             NSDictionary *rowInfo5 = [NSDictionary dictionaryWithObjectsAndKeys:@"Repairs per Week(Average)",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:3 inSection:0],@"location",nil];
             
             [rows insertObject:rowInfo5 atIndex:3];
             [rowsToInsert addObject:indexPath2];
         }
         
      }
     else if([self.categoryID integerValue] == 2)      {
         
         if(rowInfo4)
         {
            // [rows removeObjectsInArray:_filteredArr];
             [rows removeObject:rowInfo4];
             [rowsToRemove addObject:indexPath1];
         }
         
         
         if(!rowInfo5)
         {
             NSDictionary *rowInfo5 = [NSDictionary dictionaryWithObjectsAndKeys:@"Repairs per Week(Average)",DATADICT_TITLE_KEY,[NSIndexPath indexPathForRow:3 inSection:0],@"location",nil];
             
             indexPath2 = [NSIndexPath indexPathForRow:2 inSection:0];
             
             [rows insertObject:rowInfo5 atIndex:2];
             [rowsToInsert addObject:indexPath2];
         }


     }
     else  if([self.categoryID integerValue] == 3)
     {
         if(rowInfo4)
         {
             //[rows removeObjectsInArray:_filteredArr];
             [rows removeObject:rowInfo4];
             [rowsToRemove addObject:indexPath1];
         }
         
         if(rowInfo5)
         {
             //[rows removeObjectsInArray:_filteredArr];
             [rows removeObject:rowInfo5];
             [rowsToRemove addObject:indexPath2];
         }
         
     }

    [self.tableView beginUpdates];
    
    NSUInteger _removedCount = [rowsToRemove count];
    NSUInteger _addedCount   = [rowsToInsert count];
    
    if(_removedCount)
    {
      [self.tableView deleteRowsAtIndexPaths:rowsToRemove withRowAnimation:UITableViewRowAnimationTop];
    }
    
    if(_addedCount)
    {
        [self.tableView insertRowsAtIndexPaths:rowsToInsert withRowAnimation:UITableViewRowAnimationBottom];
    }
    
    [self.tableView endUpdates];
    
    if( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && (_addedCount || _removedCount))
    {
      [self.tableView reloadRowsAtIndexPaths:_indexPaths withRowAnimation:UITableViewRowAnimationFade];
    }
    
 
    
   // [catNameBeforeSelection release];
    
    [self setAssociateObject:nil forKey:@"currentCatIndexPath"];
}

#pragma mark -


#pragma mark SELECT STATE
-(void)selectState:(UIButton *)sender
{
    [self.selectedTextField resignFirstResponder];
    
    UIPopoverController *popover=self.statePopover;
    if(!popover)
    {
        //UITableViewCell *cell =(UITableViewCell *)[[sender superview] superview];
        //NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        StatesViewController *stateViewController = [[StatesViewController alloc] init];
        
        [stateViewController addStateSelectedCallback:^(NSString *stateName) {
            
            self.customerState =stateName;
            [sender setTitle:stateName forState:UIControlStateNormal];
            
            [self.statePopover dismissPopoverAnimated:YES];
        }];
        
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:stateViewController];
        popover=[[UIPopoverController alloc] initWithContentViewController:navController];
        popover.popoverContentSize = CGSizeMake(320.0, 190.0);
        self.statePopover = popover;
        
        [popover release];
        [navController release];
        [stateViewController release];
    }
    
    CGRect targetRect =[self.view convertRect:sender.frame fromView:[sender superview]];  ; // [self.tableView rectForRowAtIndexPath:indexPath];
    [popover presentPopoverFromRect:targetRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];

}
#pragma mark -

#pragma mark SELECT GET GENUINE MEMBER

-(void)selectMembershipForIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell =[self.tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryView.hidden=NO;
    
    UITextField *_textField =(UITextField *)cell.accessoryView;
    
    [_textField becomeFirstResponder];
    
   // NSMutableArray *rows = indexPath.section<[dataSource count] ? [dataSource objectAtIndex:indexPath.section] : nil;
    
}
-(void)selectGenuineMemberForIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell =[self.tableView cellForRowAtIndexPath:indexPath];
    
    self.isGenuineMember = [NSNumber numberWithInteger:!isGenuineMember.boolValue];
    
    BOOL _isGenuine = self.isGenuineMember.boolValue;
    cell.accessoryType = _isGenuine ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    
    NSMutableArray *rows = indexPath.section<[dataSource count] ? [dataSource objectAtIndex:indexPath.section] : nil;
    if(rows)
    {
        if(_isGenuine)
        {
          NSDictionary *rowInfoMemberShip = [NSDictionary dictionaryWithObjectsAndKeys:@"Membership Number",@"title",[NSIndexPath indexPathForRow:5 inSection:0],@"location", nil];
          [rows addObject:rowInfoMemberShip];
            
            NSIndexPath *_indexPathToInsert = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:0];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObjects:_indexPathToInsert, nil] withRowAnimation:UITableViewRowAnimationBottom];
        }
        else 
        {
            /*
           __block int _indexToRemove = NSNotFound;
            [rows enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                
                NSDictionary *dict=(NSDictionary *)obj;
                NSIndexPath *indexPath = [dict valueForKey:@"location"];
                if(indexPath.row==5)
                {
                    *stop = YES;
                    _indexToRemove = idx;
                }
            }];
             */
            
            NSUInteger _indexToRemove =  [rows indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) 
            {
                NSDictionary *dict     = (NSDictionary *)obj;
                NSIndexPath *indexPath = [dict valueForKey:@"location"];
                
                if(indexPath.row==5)
                {
                    *stop = YES;
                    return YES;
                }
                return NO;
            }];

            if(_indexToRemove!=NSNotFound)
            {
                [rows removeObjectAtIndex:_indexToRemove];
                
                NSIndexPath *_indexPathToDelete = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:0];
                [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:_indexPathToDelete, nil] withRowAnimation:UITableViewRowAnimationTop];
            }
        }
    }

}

#pragma mark -
#pragma mark SELECT WORKSHOP BAYS
#pragma mark SELECT REPAIRS PER WEEK
-(void)selectNumberPickerForIndexPath:(NSIndexPath *)indexPath
{
   // NSMutableArray *rows = mainIndexPath.section<[dataSource count] ? [dataSource objectAtIndex:mainIndexPath.section] : nil;
   // NSDictionary *rowDict = [rows count] ?  [rows objectAtIndex:0] : nil;
    //NSIndexPath *indexPath = [rowDict valueForKey:@"location"];
    
    int count = (indexPath.row==2)?30:100;
    NSMutableArray *numbers=[NSMutableArray array];
    [numbers addObject:@"N/A"];
    for(int i=0 ; i<count ; i++)
    {
        [numbers addObject:[NSString stringWithFormat:@"%d",i+1]];
    }
    
   
    NumberPickerViewController *numberPickerViewController=nil;
    if(numberPickerPopover)
    {
        numberPickerViewController =[(NumberPickerViewController *)[numberPickerPopover contentViewController] retain];
        numberPickerViewController.numberArr = numbers;
    }
    else
    {
        
    numberPickerViewController=[[NumberPickerViewController alloc]initWithDataSource:numbers];
    numberPickerPopover = [[UIPopoverController alloc] initWithContentViewController:numberPickerViewController];
        numberPickerPopover.popoverContentSize = CGSizeMake(320.0, 100.0);
    }
    
  numberPickerViewController.selectedNumber = (indexPath.row==2)?self.workshopBays:self.repairsPerWeek;
    
   __block ProfileInfoEditViewController *weakSelf = self;
    
    [numberPickerViewController addNumberCallback:^(NSString *strNumber) 
     {
         if(indexPath.row==2)
         {
           weakSelf.workshopBays  =  strNumber;
         }
         else
         {
            weakSelf.repairsPerWeek  =  strNumber;
         }
         
        
         NSIndexPath *targetIndexPath =[weakSelf.categoryName isEqualToString:@"Collision"] ? [NSIndexPath indexPathForRow:2 inSection:0] : indexPath ;
        [weakSelf.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:targetIndexPath] withRowAnimation:UITableViewRowAnimationNone]; 
        
         
         //DISMISS POPOVER ON EACH SELECTION
         [weakSelf->numberPickerPopover dismissPopoverAnimated:YES];
         
     }];
    
    
    CGRect targetRect = [self.tableView rectForRowAtIndexPath:indexPath];
    [numberPickerPopover dismissPopoverAnimated:NO];
    
    [numberPickerPopover presentPopoverFromRect:targetRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    
    [numberPickerViewController release];
}



#pragma mark -

#pragma mark UITextViewDelegate

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (void)textViewDidChange:(UITextView *)textView
{
   // textView.text = [textView.text trimmedString];
    self.customerNotes = textView.text;
    
    CGFloat fontHeight = (textView.font.ascender - textView.font.descender) + 1;
    
    CGRect newTextFrame = textView.frame;
    newTextFrame.size = textView.contentSize;
    //newTextFrame.size.height = newTextFrame.size.height + fontHeight;
    newTextFrame.size.height=MIN(140.0, newTextFrame.size.height);

    _textViewHeight = newTextFrame.size.height+10.0;
    [self.tableView beginUpdates];
    textView.frame = newTextFrame;
    [self.tableView endUpdates];
    
   
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
    UITableViewCell *cell =(UITableViewCell *)[textView superViewOfType:[UITableViewCell class]];
    CellBackgroundView *_bgView =(CellBackgroundView *) cell.backgroundView;
    [_bgView setNeedsDisplay];
    //[self.tableView scrollRectToVisible:cell.frame animated:NO];
    }
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.selectedTextField = textField;
    
    
    //==================================================================================//
    UITableViewCell *cell  = [self cellForTextField:textField];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    NSMutableArray *rows  = (indexPath.section < [dataSource count]) ?  [dataSource objectAtIndex:indexPath.section] : nil;
    
    NSDictionary *rowInfo = (indexPath.row < [rows count]) ? [rows objectAtIndex:indexPath.row] : nil;
    
    NSIndexPath *actualIndexPath = [rowInfo valueForKey:@"location"];
    
     if(actualIndexPath.section>=CONTACT_SECTION_START && actualIndexPath.section<=CONTACT_SECTION_END)
    {
            
        self.theNewContactLastIndexPath = [NSIndexPath indexPathForRow:4 inSection:indexPath.section];
    }
     else 
     {
         self.theNewContactLastIndexPath =nil;
     }
    
    
    //CGRect targetRect=[self.tableView convertRect:self.selectedTextField.frame fromView:self.selectedTextField.superview];
   // [self.tableView scrollRectToVisible:targetRect animated:YES];
    
//    UITableViewCell *cell = (UITableViewCell*) [[textField superview] superview];
//    NSIndexPath *scrollingIndexPath = [self.tableView indexPathForCell:cell];
//    [self.tableView scrollToRowAtIndexPath:scrollingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];

}

#pragma mark-
#pragma mark TEXTFIELD DELEGATE
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if([textField.placeholder rangeOfString:@"Email"].location!=NSNotFound)
    {
        NSString *text = textField.text;
        if(text.length && ![text isValidEmail])
        {
            [UIAlertView showWarningAlertWithTitle:INVALID_EMAIL message:VALID_EMAIL_ID_MSG];
            _allowSave=NO;
           // textField.textColor = [UIColor redColor];
            return NO;
        }
        else 
        {
           // textField.textColor = [UIColor blackColor];
            _allowSave=YES;
        }
    }

    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if([self.selectedTextField isEqual:textField])
        self.selectedTextField=nil;
    
    //if(self.newContactLastIndexPath)self.newContactLastIndexPath = nil;
}

-(BOOL)validateInputText:(NSString *)inputText fromTextField:(UITextField *)textField
{
    if(!inputText.length) return NO;
    BOOL isValid = NO;
    
    UITableViewCell *cell  = (UITableViewCell *)[self cellForTextField:textField];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    NSMutableArray *rows  = (indexPath.section < [dataSource count]) ?  [dataSource objectAtIndex:indexPath.section] : nil;
    
    NSDictionary *rowInfo = (indexPath.row < [rows count]) ? [rows objectAtIndex:indexPath.row] : nil;
    
    NSIndexPath *actualIndexPath = [rowInfo valueForKey:@"location"];
    
    /*
    //======= ENABLE FOR DELETE ONCE TEXT IS ENTERED ===============
    if(actualIndexPath.section==_currentContactSection)
    {
        _currentContactSection = NSNotFound;
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
        [textField becomeFirstResponder];
    }
    */
    //============ INGNORE SPACE IN EMAIL FIELD===================
    if((actualIndexPath.section>=CONTACT_SECTION_START && actualIndexPath.section<=CONTACT_SECTION_END && actualIndexPath.row==4) || (actualIndexPath.section==4 && actualIndexPath.row==1))
    {
        return [[inputText trimmedString] length]>0;
    }
    
    BOOL isNumberOnly = [self validateInputForIndexPath:actualIndexPath];
    
    if(isNumberOnly)
    {
       // BOOL isPhoneField =((actualIndexPath.section==4 && actualIndexPath.row==0) || (actualIndexPath.section==CONTACT_SECTION_START && actualIndexPath.row==2) || (actualIndexPath.section==CONTACT_SECTION_START && actualIndexPath.row==3));
        
       BOOL isPhoneField = textField.keyboardType == UIKeyboardTypePhonePad;
        
      NSCharacterSet * const numberSet = [NSCharacterSet characterSetWithCharactersInString: isPhoneField? @" 0123456789+-": @"0123456789+-"];
        
      isValid =  ([inputText rangeOfCharacterFromSet:numberSet].location != NSNotFound);
    }
    else
    {
        //================== FOR ADDRESS FIELD ALLOW '/' BUT OTHERS NOT FOR OTHER FIELDS =================
        NSString *ignoreCharString =  (actualIndexPath.section==3 || (actualIndexPath.section==0 && actualIndexPath.row==0)) ? @"<>;|{}~`\\" : @"<>/;|{}~`\\";
        NSCharacterSet * const numberSet = [NSCharacterSet characterSetWithCharactersInString:ignoreCharString];
        isValid =  ([inputText rangeOfCharacterFromSet:numberSet].location == NSNotFound);
    }
    
    return isValid;

}

-(NSIndexPath *)indexPathForTextField:(UITextField *)textField
{
    NSIndexPath *indexPath = nil;
    for(id key  in textfieldContainerDict)
    {
        UITextField *_textField = [textfieldContainerDict objectForKey:key];
        if([_textField isEqual:textField])
        {
            indexPath = (NSIndexPath *)key;
            break;
        }
    }
    
    return indexPath;
}


-(UITableViewCell *)cellForTextField:(UITextField *)textField
{
    UIView *superView = [textField superview];
    
    while( superView && ![superView isKindOfClass:[UITableViewCell class]])
    {
        superView = [superView superview];
        
        if([superView isKindOfClass:[UITableViewCell class]])
        {
            break;
        }
    }
    
    return (UITableViewCell *)superView;
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    UITableViewCell *cell = (UITableViewCell *)[self cellForTextField:textField];
    
    NSLog(@"text=%@",cell.textLabel.text);
   // NSIndexPath *indexPath= [self.tableView indexPathForRowAtPoint:cell.center]
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    /*
    if(!indexPath || indexPath.section==1)
    {
        indexPath = [self indexPathForTextField:textField];
    }
    */
    
    NSLog(@"row = %d",indexPath.row);
    
    NSString *text=[textField.text stringByReplacingCharactersInRange:range withString:string];
    if(textField.text.length>text.length)
    {
        //FOR DELETE TEXT ACTION
        [self setContactValue:text forIndexPath:indexPath];
        
        /*
        BOOL _isPhoneField = ([textField.placeholder rangeOfString:@"Phone"].location!=NSNotFound || ([textField.placeholder rangeOfString:@"Mobile"].location!=NSNotFound || [textField.placeholder rangeOfString:@"Direct"].location!=NSNotFound));
        
        if(_isPhoneField)
        {
            NSString *str = [self formatPhoneNumber:text];
            textField.text = str;
            return NO;
        }
       */

        return YES;
    }
    else if(text.length)
    {
        BOOL isValid =[self validateInputText:string fromTextField:textField];
        if(!isValid) return NO;

        NSString *trimmedStr=[text trimmedString];
        
        if(!trimmedStr.length) return NO;
        
            }
    
    int textLen =[self  validateInputTextLengthForIndexPath:indexPath];
    
   // BOOL b =  text.length && text.length<=MAX_TEXT_LENGTH;
    BOOL b =  text.length && text.length<=textLen;
    
    if(b)
    {
        [self setContactValue:text forIndexPath:indexPath];
    }
    
    //=============== FOR PROCESS PHONE NUMBER INPUT ===============================
    if(b)
    {
        /*
       BOOL _isPhoneField = ([textField.placeholder rangeOfString:@"Phone"].location!=NSNotFound || ([textField.placeholder rangeOfString:@"Mobile"].location!=NSNotFound || [textField.placeholder rangeOfString:@"Direct"].location!=NSNotFound));

        if(_isPhoneField)
        {
            NSString *str = [self formatPhoneNumber:text];
            textField.text = str;
            return NO;
        }
        */
    }
    //=============== FOR PROCESS PHONE NUMBER INPUT ===============================
    
    return b;
    
   


    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    UITableViewCell *cell = (UITableViewCell *)[textField superViewOfType:[UITableViewCell class]];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    [self setContactValue:nil forIndexPath:indexPath];
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField 
{
    //MOVE TO NEXT TEXTFIELD ON HITTING DONE
   NSArray *arrayCells = [self.tableView visibleCells];
    
   //UITableViewCell * currentCell  = (UITableViewCell *) textField.superview.superview;
   UITableViewCell * currentCell  = (UITableViewCell *)[textField superViewOfType:[UITableViewCell class]];
    
   NSIndexPath * currentIndexPath = [self.tableView indexPathForCell:currentCell];

    for(UITableViewCell * nextCell  in arrayCells)
    {
        if([nextCell isEqual:currentCell]) continue;
        
        NSIndexPath *nextIndexPath = [self.tableView indexPathForCell:nextCell];
        
        if((nextIndexPath.section==currentIndexPath.section && nextIndexPath.row<currentIndexPath.row) || nextIndexPath.section<currentIndexPath.section) continue;
        
         UIView *nextField =  [nextCell.contentView viewWithTag:1];
        
        if([nextField canBecomeFirstResponder])
        {
            //if([nextField text].length) continue;
           
            [textField resignFirstResponder];
            
            if(_allowSave)
            {
              [nextField becomeFirstResponder];
                
                if( self.theNewContactLastIndexPath==nil)
                
                  [self.tableView scrollToRowAtIndexPath:nextIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
               
                   self.theNewContactLastIndexPath = nil;
            }
            else
            {
                return NO;
            }
            break;
        }
    }
    
     [textField resignFirstResponder];
     return YES;
    
/*
if ((currentIndexPath.row != [arrayCells count] - 1) && (currentIndexPath.row < [arrayCells count]-1)) 
{
    
    NSIndexPath * nextIndexPath = [NSIndexPath indexPathForRow:currentIndexPath.row + 1 inSection:0];
    
    UITableViewCell * nextCell = (UITableViewCell *) [self.tableView cellForRowAtIndexPath:nextIndexPath];
    
    [self.tableView scrollToRowAtIndexPath:nextIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    
    UITextField *nextField = (UITextField *) [nextCell.contentView viewWithTag:1];
    [nextField becomeFirstResponder];
    
    
    for (id object in nextCell.contentView.subviews) 
    {
        if ([object isKindOfClass:[UITextField class]]) 
        {
            UITextField *tf = object;
            [tf becomeFirstResponder];
        }
    }
    
  }
 */   

}

@end
