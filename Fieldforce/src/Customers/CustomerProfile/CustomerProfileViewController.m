//
//  CustomerProfileViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 23/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomerProfileViewController.h"
#import "MenuViewController.h"
#import "Customer.h"
@interface CustomerProfileViewController ()
{
    BOOL _isEdit;
    
    BOOL _shouldSave;
}

@end

@implementation CustomerProfileViewController
@synthesize summaryViewController,tasksViewController,vistsViewController;



-(NSArray *)viewControllers
{
    if(!summaryViewController)
    summaryViewController = [[CustomerSummaryViewController alloc] initWithCustomer:self.customer];
    summaryViewController.customerProfileController=self;

    if(!tasksViewController)
    tasksViewController = [[TasksViewController alloc] initWithCustomer:self.customer];
    
    if(!vistsViewController)
    vistsViewController = [[CustomerVisitsViewControlller alloc] initWithCustomer:self.customer];
    
    UIButton *tabbarItem=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(0, 0, 85,0) bgImage:@"summary_tab.png" titleColor:[UIColor blueColor] target:nil action:nil];
    [tabbarItem setBackgroundImage:[UIImage imageNamed:@"summary_tab_selected.png"] forState:UIControlStateSelected];
    
    summaryViewController.SSTabBarItem=tabbarItem;
    
    tabbarItem=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(0, 0, 85, 0) bgImage:@"tasks_tab.png" titleColor:[UIColor blueColor] target:nil action:nil];
    [tabbarItem setBackgroundImage:[UIImage imageNamed:@"tasks_tab_selected.png"] forState:UIControlStateSelected];
    tasksViewController.SSTabBarItem=tabbarItem;
    
    tabbarItem=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(0, 0, 85, 0) bgImage:@"visits_tab.png" titleColor:[UIColor blueColor] target:nil action:nil];
    [tabbarItem setBackgroundImage:[UIImage imageNamed:@"visits_tab_selected.png"] forState:UIControlStateSelected];
    vistsViewController.SSTabBarItem=tabbarItem;
    
    NSArray *vcs=[NSArray arrayWithObjects:summaryViewController,tasksViewController,vistsViewController, nil];
    
    return vcs;
}

-(void)didSelectViewController:(UIViewController *)viewController
{
    NSString *customerName = self.customer.name;
    
    NSString *titleStr = customerName.length ? [customerName uppercaseString] : @"Create New Customer";
    [self addTitle:titleStr withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    
    if([viewController isKindOfClass:[summaryViewController class]])
    {
        //[self addTitleImage:@"customer_profile.png"];
        //if(!_isEdit)
        self.navigationItem.rightBarButtonItem.customView.hidden=NO;
    }
    else if([viewController isKindOfClass:[tasksViewController class]])
    {
        //[self addTitleImage:@"customer_tasks.png"];
        
              //NSLog(@"%d",app.fromCustomerTask);
        self.navigationItem.rightBarButtonItem.customView.hidden=YES;
    }
    else if([viewController isKindOfClass:[vistsViewController class]])
    {
        //[self addTitleImage:@"customer_visits.png"];
        self.navigationItem.rightBarButtonItem.customView.hidden=YES;
    }
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //MenuViewController *_menuVC=  [app.splitViewController.viewControllers objectAtIndex:0];
    //[_menuVC showVisitingCustomerName:nil];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)dealloc
{
    [summaryViewController release];
    [tasksViewController release];
    [vistsViewController release];
    
    [super dealloc];
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
#if LOCAL
    
    [UIAlertView showWarningAlertWithTitle:@"Memory Warning" message:@"At CustomerProfileViewController"];
#endif
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    //[self addDefaultBorder];
    
    [self addBackButtonWithSelector:nil];
   // [self addBackButtonWithSelector:@selector(backAction:)];
    [self addEditButtonWithSelector:@selector(editAction:)];
    
   // self.parentTabbarControlller.selectedIndex=0;
    
    //opens in edit mode
    //[self willRefresh];
}


-(void)willRefresh
{
   
}

-(void)editAction
{
    _isEdit=!_isEdit;
    [summaryViewController enableEditMode:_isEdit];
    
   UIButton *editButtton = (UIButton *) self.navigationItem.rightBarButtonItem.customView;
    editButtton.selected = _isEdit;
   // self.navigationItem.rightBarButtonItem.customView.hidden = _isEdit;
    self.navigationItem.leftBarButtonItem.customView.hidden = _isEdit;
    
}

-(void)editAction:(id)sender
{
    if(_isEdit)
    {
        [summaryViewController.profileInfoEditViewController save];
    }
    else
    {
        [self editAction];
    }
}


-(void)backAction:(UIButton *)sender
{
    if(summaryViewController)
    {
        [summaryViewController backAction:sender];
    }
 
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
