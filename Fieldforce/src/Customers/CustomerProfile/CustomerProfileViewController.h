//
//  CustomerProfileViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 23/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractTabBarController.h"
#import "CustomerSummaryViewController.h"
#import "TasksViewController.h"
#import "CustomerVisitsViewControlller.h"
//#import "AppDelegate.h"

@interface CustomerProfileViewController : AbstractTabBarController

@property(nonatomic,readonly)  CustomerSummaryViewController *summaryViewController;
@property(nonatomic,readonly)  TasksViewController *tasksViewController;
@property(nonatomic,readonly)  CustomerVisitsViewControlller *vistsViewController;




-(void)editAction;
@end
