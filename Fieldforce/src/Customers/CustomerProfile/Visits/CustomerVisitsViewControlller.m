//
//  VisitsViewControlller.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 23/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomerVisitsViewControlller.h"
#import "Customer.h"
#import "CustomerVisitSessionsViewController.h"
#import "CustomerVisitDetailsController.h"

@implementation CustomerVisitsViewControlller
@synthesize customerVisitSessionsViewController,customerVisitDetailsController;


- (id)initWithCustomer:(Customer *)cust
{
    self = [super init];
    if (self)
    {
        
        customerVisitDetailsController      = [[CustomerVisitDetailsController alloc] initWithCustomer:cust];
        
        customerVisitSessionsViewController = [[CustomerVisitSessionsViewController alloc] initWithCustomer:cust];
        
        customerVisitSessionsViewController.customerVisitDetailsController=customerVisitDetailsController;
        
    }
    return self;
}

-(void)dealloc
{

    [customerVisitDetailsController release];
    customerVisitDetailsController = nil;
    
    [customerVisitSessionsViewController release];
    customerVisitSessionsViewController = nil;
    
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle




- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.backgroundColor =  [UIColor clearColor];
    
    CGRect bounds=self.view.bounds;
    CGRect _containerLeftHalf=CGRectMake(0, 0, CGRectGetWidth(bounds)/2.0, CGRectGetHeight(bounds));
    CGRect _containerRightHalf=CGRectMake(CGRectGetWidth(_containerLeftHalf), 0, CGRectGetWidth(bounds)/2.0, CGRectGetHeight(bounds));
    
    [self addChildViewController:customerVisitSessionsViewController];
    [self addChildViewController:customerVisitDetailsController];
    
    customerVisitSessionsViewController.view.frame =_containerLeftHalf;
    customerVisitDetailsController.view.frame =_containerRightHalf;
    
    [customerVisitSessionsViewController.view  setAutoresizingMasks];
    [customerVisitDetailsController.view  setAutoresizingMasks];
    
    //[customerVisitSessionsViewController addDefaultBorder];
    //[customerVisitDetailsController addDefaultBorder];
    
    [customerVisitSessionsViewController.view addLeftBoderWithColor:[UIColor colorWithWhite:0.2 alpha:0.4] borderWidth:1.0];
    [customerVisitDetailsController.view addLeftBoderWithColor:[UIColor colorWithWhite:0.2 alpha:0.3] borderWidth:1.0];
    
    [self.view addSubview:customerVisitSessionsViewController.view];
    [self.view addSubview:customerVisitDetailsController.view];
    
    [customerVisitSessionsViewController didMoveToParentViewController:self];
    [customerVisitDetailsController didMoveToParentViewController:self];
    

}


- (void)viewDidUnload
{
    [super viewDidUnload];
   
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
   // [customerVisitSessionsViewController viewWillAppear:animated];
   // [customerVisitDetailsController viewWillAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
