//
//  VisitsViewControlller.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 23/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomerVisitSessionsViewController,CustomerVisitDetailsController,Customer;
@interface CustomerVisitsViewControlller : UIViewController

@property(nonatomic,readonly)CustomerVisitSessionsViewController *customerVisitSessionsViewController;
@property(nonatomic,readonly)CustomerVisitDetailsController *customerVisitDetailsController;


- (id)initWithCustomer:(Customer *)customer;

@end
