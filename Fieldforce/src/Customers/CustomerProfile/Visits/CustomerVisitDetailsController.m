//
//  CustomerVisitDetailsController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomerVisitDetailsController.h"
#import "Customer.h"
#import "Visit.h"
#import "UIViewController+RepToolHeader.h"
//#import "VisitReviewSessionViewController.h"
#import "VisitTypesViewcontroller.h"
#import "VisitSessionViewController.h"
#import "InsetTableViewCell.h"

@interface CustomerVisitDetailsController()<UITableViewDelegate,UITableViewDataSource,InsetTableViewCellDelegate>
{
    UITableView *_tableView;
    NSArray *natureOfVisits;
    
    BOOL _isReloading;
}
@property(nonatomic,retain)NSMutableArray *reviewedVisits;
@property(nonatomic,retain)NSMutableArray *unreviewedVisits;
@end

@implementation CustomerVisitDetailsController
@synthesize customer;

@synthesize reviewedVisits;
@synthesize unreviewedVisits;
@synthesize tableView = _tableView;

/*
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        // Custom initialization
        
    }
    return self;
}
 */

-(id)initWithCustomer:(Customer *)aCustomer
{
   // if(self=[self initWithStyle:UITableViewStyleGrouped])
    if(self=[super init])
    {
        self.customer=aCustomer;
    }
    
    return self;
}

-(void)reloadData
{
    if(_isReloading) return;
    
    _isReloading = YES;
    
    //NSMutableArray *allVisits = [CoreDataHandler allVisitsForCustomer:self.customer];
    //[self.tableView reloadData];
    
    [CoreDataHandler fetchVisitsForCustomer:self.customer OnCompletion:^(NSArray *result, NSError *error)
    {
        _isReloading = NO;
        
        if(result)
        {
            NSMutableArray *allVisits=[NSMutableArray arrayWithArray:result];
            self.reviewedVisits   = [CoreDataHandler reviewedVisitsFromVisits:allVisits];
            self.unreviewedVisits = [CoreDataHandler unreviewedVisitsFromVisits:allVisits];
            [self.tableView reloadData];
        }
    }]; 
}

-(void)dealloc
{
    _isReloading = NO;
    [customer release];customer=nil;
    [reviewedVisits release];reviewedVisits=nil;
    [unreviewedVisits release];unreviewedVisits=nil;
    [_tableView release] ; _tableView = nil;
    [natureOfVisits release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle



- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.clipsToBounds = YES;
    
    [self addTitleImage:@"customer_visits.png"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:VisitReviewDidUpdate object:nil];

    self.view.backgroundColor=CELL_BACKGROUND_COLOR;
    
   // natureOfVisits=[[VisitTypesViewcontroller internalVisitTypes] retain];
    //nameForNatureOfVisitID

    CGFloat _locY= 0.0;
    
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, _locY-0.0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-_locY) style:UITableViewStyleGrouped];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    _tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.backgroundColor   =   [UIColor clearColor];
    //_tableView.separatorColor = [UIColor lightGrayColor];
    _tableView.bounces = NO;
    _tableView.rowHeight = 44.0;
    [self.view addSubview:_tableView];
    
    self.tableView.bounces=NO;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    [self reloadData];
  
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   // [self reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //self.contentSizeForViewInPopover=CGSizeMake(320.0, self.tableView.contentSize.height);
    self.popoverController.popoverContentSize=CGSizeMake(320.0, MAX(320.0,self.tableView.contentSize.height+128.0+44.0));
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    _isReloading = NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //return (section==0) ? 1 : 2;
    
    NSUInteger count= (section==0) ? [self.unreviewedVisits count] :[self.reviewedVisits count];
    
    count=count?count:1;
    
    return count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView=[[[UIView alloc] init] autorelease];
   // headerView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    headerView.autoresizesSubviews=YES;
    headerView.backgroundColor=[UIColor clearColor];
    
    UILabel *headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, (45-22)/2.0+1.0, headerView.width,22)];
    headerLabel.autoresizingMask=UIViewAutoresizingFlexibleWidth;
    headerLabel.font = [UIFont subaruBoldFontOfSize:24.0];
    headerLabel.textColor=(section==0)?[UIColor redColor]:[UIColor blackColor];
    headerLabel.backgroundColor=[UIColor clearColor];
    headerLabel.shadowColor = [UIColor lightGrayColor];
    headerLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    //(section==0)?[UIColor redColor]:[UIColor darkGrayColor];
    headerLabel.text=(section==0)?@"   UNREVIEWED VISITS":@"   COMPLETED REPORTS";
    [headerView addSubview:headerLabel];
    
    
    UILabel *noOfVisitsLabel=[[UILabel alloc] initWithFrame:CGRectMake(200.0, (45-22)/2.0+1.0, 140.0,22)];
    //noOfVisitsLabel.autoresizingMask=UIViewAutoresizingFlexibleWidth;
    noOfVisitsLabel.font = [UIFont subaruBoldFontOfSize:16.0];
    noOfVisitsLabel.textColor=[UIColor redColor];
    noOfVisitsLabel.textAlignment = UITextAlignmentRight;
    noOfVisitsLabel.backgroundColor=[UIColor clearColor];
    noOfVisitsLabel.backgroundColor=[UIColor clearColor];
    noOfVisitsLabel.shadowColor = [UIColor lightGrayColor];
    headerLabel.shadowOffset = CGSizeMake(0.5, 1.0);
    [headerView addSubview:noOfVisitsLabel];
    
    NSUInteger visitCount =[self.unreviewedVisits count];
    if(section ==0 && visitCount)
    noOfVisitsLabel.text = [NSString stringWithFormat:@"%d %@",visitCount,(visitCount==1)?@"VISIT":@"VISITS"];
    
    
    noOfVisitsLabel.hidden = (section!=0) || !visitCount;
    [noOfVisitsLabel release];
    [headerLabel release];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return (section == 0) ? 40.0 : 0.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(section==1) return nil;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 40.0)];
    footerView.backgroundColor = [UIColor clearColor];
    
    UIView *lineview=[[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(footerView.bounds)-1.0, CGRectGetWidth(footerView.bounds), 1)];
    lineview.backgroundColor = [UIColor lightGrayColor];
    [footerView addSubview:lineview];
    lineview.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [lineview release];
    
    return [footerView autorelease];
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    static NSString *CellIdentifier2 = @"Cell_NoItems";
    
    UITableViewCell *cell=nil;
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    NSUInteger count= (indexPath.section==0) ? [self.unreviewedVisits count] :[self.reviewedVisits count];
    
    if(count==0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.accessoryType=UITableViewCellAccessoryNone;
            cell.textLabel.font=[UIFont systemFontOfSize:14];
        }
        
        cell.textLabel.text = (indexPath.section==0)? @"No unreviewed report(s)" : @"No completed report(s)";
    }
    else
    {
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        
        cell.selectionStyle=UITableViewCellSelectionStyleBlue;
        //cell.textLabel.font=[UIFont systemFontOfSize:14];
        cell.textLabel.font=[UIFont grotesqueBoldFontOfSize:14.0];
        cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:12.0];
        cell.textLabel.textColor=[UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
        
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
     {
        InsetTableViewCell *_cell = (InsetTableViewCell *)cell;
        _cell.delegate = self;
      }
    
    NSMutableArray *visits = (indexPath.section==0)?self.unreviewedVisits:self.reviewedVisits;
    
    Visit *_visit=(indexPath.row<[visits count]) ? [visits objectAtIndex:indexPath.row] : nil;
    
   
    
    NSInteger _natureOfVisitID = [_visit.natureOfVisitID integerValue];
   
     cell.textLabel.text        = _natureOfVisitID<[natureOfVisits count] ? [natureOfVisits objectAtIndex:_natureOfVisitID] : @"";
    // cell.detailTextLabel.text = [_visit.startDate stringFromDateWithFormat:VISIT_DATE_FORMAT];
        
    NSString *dateString = [_visit.startDate stringFromDateWithFormat:VISIT_DATE_FORMAT];
        
    //-------------------
    NSString  *startEndTimeString = [_visit startEndTimeString];
        
    NSString *dateTimeString = [dateString stringByAppendingFormat:@"    %@",startEndTimeString];
    cell.detailTextLabel.text = dateTimeString;
    
    }
    
    return cell;
}

*/


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    static NSString *CellIdentifier2 = @"Cell_NoItems";
    
    UITableViewCell *cell=nil;
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    NSUInteger count= (indexPath.section==0) ? [self.unreviewedVisits count] :[self.reviewedVisits count];
    
    UILabel *startEndDateLabel = nil;
    if(count==0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.accessoryType=UITableViewCellAccessoryNone;
            cell.textLabel.font=[UIFont systemFontOfSize:14];
            
            
        }
      
        
        cell.textLabel.text = (indexPath.section==0)? @"No unreviewed report(s)" : @"No completed report(s)";
    }
    else
    {
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
            
            cell.selectionStyle=UITableViewCellSelectionStyleBlue;
            //cell.textLabel.font=[UIFont systemFontOfSize:14];
            cell.textLabel.font=[UIFont grotesqueBoldFontOfSize:14.0];
            cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:11.0]; //12
            cell.textLabel.textColor=[UIColor blackColor];
            cell.detailTextLabel.textColor = [UIColor blackColor];
            
            
            startEndDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(cell.contentView.bounds)-180.0+5.0, 20.5, 180, 20.0)];
            startEndDateLabel.textColor = [UIColor blackColor];
            startEndDateLabel.font = [UIFont grotesqueFontOfSize:11.0]; //12
            //startEndDateLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
            startEndDateLabel.backgroundColor = [UIColor clearColor];
            startEndDateLabel.textAlignment = NSTextAlignmentRight;
            [cell.contentView addSubview:startEndDateLabel];
            startEndDateLabel.tag = 1;
            [startEndDateLabel release];

        }
        else
        {
            startEndDateLabel = (UILabel *)[cell.contentView viewWithTag:1];
        }
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            InsetTableViewCell *_cell = (InsetTableViewCell *)cell;
            _cell.delegate = self;
        }
        
        NSMutableArray *visits = (indexPath.section==0)?self.unreviewedVisits:self.reviewedVisits;
        
        Visit *_visit = (indexPath.row<[visits count]) ? [visits objectAtIndex:indexPath.row] : nil;
        
        NSInteger _natureOfVisitID = [_visit.natureOfVisitID integerValue];
        
        //NSString *natureOfVisitStr = _natureOfVisitID < [natureOfVisits count] ? [natureOfVisits objectAtIndex:_natureOfVisitID] : @"";
        
        NSString *natureOfVisitStr = [VisitTypesViewcontroller nameForNatureOfVisitID:_natureOfVisitID];
        cell.textLabel.text        = natureOfVisitStr ? natureOfVisitStr : @"";
       
        
        NSString *dateString = [_visit.startDate stringFromDateWithFormat:VISIT_DATE_FORMAT];
       
        cell.detailTextLabel.text = dateString;
        
        NSString  *startEndTimeString = [_visit startEndTimeString];
        startEndDateLabel.text    = startEndTimeString;
        
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];


    CellBackgroundView *_selectedBgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _selectedBgView.backgroundColor = [UIColor clearColor];
    _selectedBgView.contentInset = CGPointMake(10.0, 0.0);
    _selectedBgView.cornerRadius = 8.0;
    _selectedBgView.borderColor = self.tableView.separatorColor;
    _selectedBgView.fillColor = [UIColor cellSelectedColorBlue];
    
    
    NSInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    _selectedBgView.position = _bgView.position;
    
    cell.backgroundView = _bgView;
    cell.selectedBackgroundView = _selectedBgView;
    
    [_bgView release];
    [_selectedBgView release];
    
    
    //cell.detailTextLabel.text = [NSString stringWithFormat:@"   %@",cell.detailTextLabel.text];
   // cell.textLabel.text = [NSString stringWithFormat:@"  %@",cell.textLabel.text];
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.popoverController) return;
    
    NSMutableArray *visits = (indexPath.section==0)?self.unreviewedVisits:self.reviewedVisits;
    Visit *_visit=indexPath.row<[visits count] ? [visits objectAtIndex:indexPath.row] : nil;
    
    if(!_visit) return;
    
    VisitSessionViewController *visitReviewSessionVC=[[VisitSessionViewController alloc] initWithVisit:_visit forCustomer:self.customer];
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:visitReviewSessionVC];
   // [app.splitViewController presentModalViewController:navController animated:YES];
    [app.splitViewController presentViewController:navController animated:YES completion:^{
        
    }];
    
    [navController release];
    [visitReviewSessionVC release];
    

}

-(void)insetTableViewCell:(InsetTableViewCell *)cell didSelect:(BOOL)selected
{
    cell.textLabel.textColor= selected ? [UIColor whiteColor] : [UIColor blackColor];
    cell.detailTextLabel.textColor = selected ? [UIColor whiteColor] : [UIColor blackColor];

    UILabel *startEndDateLabel = (UILabel *)[cell.contentView viewWithTag:1];
    if(startEndDateLabel)
    startEndDateLabel.textColor = selected ? [UIColor whiteColor] : [UIColor blackColor];
    
}

@end
