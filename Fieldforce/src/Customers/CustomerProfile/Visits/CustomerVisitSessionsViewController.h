//
//  VisitSessionsViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomerVisitDetailsController,Customer;
@interface CustomerVisitSessionsViewController : UIViewController
{
    UIButton *startSessionButton;
}

@property(nonatomic,retain)Customer *customer;
@property(nonatomic,assign)CustomerVisitDetailsController *customerVisitDetailsController;

-(id)initWithCustomer:(Customer *)cust;

-(NSMutableArray *)loadUpcommingUnreviewedScheduledVisits;

@end
