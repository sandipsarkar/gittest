//
//  CustomerVisitDetailsController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Customer;
@interface CustomerVisitDetailsController : UIViewController

@property(nonatomic,retain)Customer *customer;
@property (nonatomic,retain) UITableView *tableView;

-(id)initWithCustomer:(Customer *)aCustomer;

-(void)reloadData;

@end
