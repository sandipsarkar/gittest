//
//  ScheduleVisitListViewController.m
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "ScheduleVisitListViewController.h"
#import "ScheduledVisit.h"
#import "Customer.h"

@interface ScheduleVisitListViewController()
@property(nonatomic,copy) DidSelectScheduleVisit didSelectScheduleVisit;
@end
@implementation ScheduleVisitListViewController
@synthesize visitsDataSource=_visitsDataSource;
@synthesize didSelectScheduleVisit;
@synthesize customer;

- (id)init
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self)
    {
        // Custom initialization
        
       
    }
    return self;
}

-(void)dealloc
{
    customer = nil;
    
    [_visitsDataSource release];
    
    [didSelectScheduleVisit release] ;
     didSelectScheduleVisit = nil;
    
    [super dealloc];
}

-(void)addDidSelectScheduleVisit:(DidSelectScheduleVisit )callback
{
    self.didSelectScheduleVisit = callback;
}

-(void)loadUpcommingVisits
{
    NSDate *_currentDate = [NSDate date];
    
     NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(self.customer.customerID=%@ AND self.reviewedVisitID=%@) AND (self.visitEndDate>=%@)",self.customer.customerID,[NSNumber numberWithLong:0] ,_currentDate,_currentDate];
    
    
    [ScheduledVisit fetchAsynWithPredicate:predicate onAttributes:nil sortDescriptors:nil limit:0 startIndex:0 completion:^(NSArray *result, NSError *error) 
     {
         [_visitsDataSource setArray:result];
         
         [self sortScheduledVisits];
    
         [self.tableView reloadData];
     }];
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    [self addTitle:@"SCHEDULED VISITS" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.view.backgroundColor  = [UIColor whiteColor];
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 10.0)];
        headerView.backgroundColor = [UIColor clearColor];
        self.tableView.tableHeaderView = headerView;
        [headerView release];
        
        //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        self.tableView.separatorInset = UIEdgeInsetsZero;
        
    }
    
    
    if(!_visitsDataSource)
    {
        _visitsDataSource = [[NSMutableArray alloc] init];
        [self loadUpcommingVisits];
    }
    else 
    {
        [self sortScheduledVisits];
    }
}

-(void)sortScheduledVisits
{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"visitStartDate" ascending:YES];
    [_visitsDataSource sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [sortDescriptor release];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    //return [_visitsDataSource count];
    
    return [_visitsDataSource count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
        cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:13.0];
    }
    
    if(indexPath.row==[_visitsDataSource count])
    {
        cell.textLabel.text = @"Other";
        cell.detailTextLabel.text = nil;
    }
    else
    {
        
    ScheduledVisit *visit = indexPath.row<[_visitsDataSource count]  ? [_visitsDataSource objectAtIndex:indexPath.row] : nil;
    
    cell.textLabel.text = visit.customer ? visit.customer.name : visit.customerName;
    cell.detailTextLabel.text = [visit.visitStartDate stringFromDateWithFormat:@"dd/MM/YYYY  hh:mm aa"];
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
     if(indexPath.row==[_visitsDataSource count])
     {
         if(self.didSelectScheduleVisit) self.didSelectScheduleVisit(nil,indexPath);
     }
     else 
     {
       ScheduledVisit *visit = indexPath.row<[_visitsDataSource count]  ? [_visitsDataSource objectAtIndex:indexPath.row] : nil;
         
        if(self.didSelectScheduleVisit) self.didSelectScheduleVisit(visit,indexPath);
     }
}

@end
