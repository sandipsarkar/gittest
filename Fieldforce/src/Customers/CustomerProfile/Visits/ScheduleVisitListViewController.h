//
//  ScheduleVisitListViewController.h
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ScheduledVisit;
typedef void(^DidSelectScheduleVisit) (ScheduledVisit *visit,NSIndexPath *indexPath);
@interface ScheduleVisitListViewController : UITableViewController

@property(nonatomic,retain) NSMutableArray *visitsDataSource;
@property (nonatomic,assign) Customer *customer;

-(void)addDidSelectScheduleVisit:(DidSelectScheduleVisit )callback;

@end
