//
//  VisitSessionsViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomerVisitSessionsViewController.h"
#import "Customer.h"
#import "Visit.h"
#import "ScheduledVisit.h"
#import "UIViewController+RepToolHeader.h"
//#import "VisitReviewSessionViewController.h"
#import "VisitSessionViewController.h"
#import "CalendarViewController.h"
#import "VisitTypesViewcontroller.h"
#import "UIViewController+PopOverController.h"
#import "CustomerVisitDetailsController.h"
#import "NSManagedObject+PrimaryKey.h"
#import "ScheduleVisitListViewController.h"
#import "OfflineManager.h"

#define SCHEDULE_VISIT_DIRECTLY 0
#define currentScheduleVisitKey @"currentScheduleVisit"

@interface CustomerVisitSessionsViewController()<ADDVisitDelegate,VisitTypesDelegate>

@property(nonatomic,retain)UIPopoverController *currentPopoverController;
@property(nonatomic,retain)UIPopoverController *reminderPopover;
@end

@implementation CustomerVisitSessionsViewController
@synthesize currentPopoverController,reminderPopover;
@synthesize customer;
@synthesize customerVisitDetailsController;

-(void)dealloc
{
    [reminderPopover release];
    reminderPopover=nil;
    
    [currentPopoverController release];
    currentPopoverController=nil;
    
    [customer release];
    customerVisitDetailsController=nil;
    
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) 
    {
       // self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
       // self.tableView.rowHeight=100;
    }
    return self;
}

-(id)initWithCustomer:(Customer *)cust
{
    if(self=[self init])
    {
        self.customer=cust;
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif

    self.view.backgroundColor=[UIColor clearColor];
    
   // UIImageView *headerImageView=[self pageHeaderViewForImageName:@"visit_sessions_heading.png"];
   // [self.view addSubview:headerImageView];
    //NEED To Remove IT LATER
   // headerImageView.frame=CGRectMake(0, 0, self.view.width, 80);
    CGFloat _locY = 10.0;
    UILabel *headerView = [[UILabel alloc] initWithFrame:CGRectMake(0, _locY, self.view.width, 30.0)];
    headerView.textColor = [UIColor blackColor];
    headerView.backgroundColor = [UIColor clearColor];
    headerView.font = [UIFont subaruBoldFontOfSize:24.0];
    headerView.text = @"   VISIT SESSIONS";
    headerView.shadowColor = [UIColor lightGrayColor];
    headerView.shadowOffset = CGSizeMake(1.0, 1.0);
    [self.view addSubview:headerView];
    [headerView release];
    
   _locY=headerView.bottom;
    UILabel *label1=[[UILabel alloc] initWithFrame:CGRectMake(15, _locY+5, self.view.width-20, 30)];
    label1.textColor=[UIColor lightGrayColor];
    label1.backgroundColor=[UIColor clearColor];
    label1.numberOfLines=2;
    label1.autoresizingMask=UIViewAutoresizingFlexibleWidth;
    label1.font=[UIFont grotesqueFontOfSize:13.0];
    label1.textColor = [UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1.0];
    [self.view addSubview:label1];
    label1.text=@"To start a visit session,please tap on the button below.";
    [label1 release];
    
    UIImage *startSessionImage = [UIImage imageNamed:@"start_session_button.png"];
    startSessionButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [startSessionButton setBackgroundImage:startSessionImage forState:UIControlStateNormal];
    startSessionButton.frame=CGRectMake((CGRectGetWidth(self.view.bounds)-startSessionImage.size.width)/2.0, label1.bottom +5, startSessionImage.size.width, startSessionImage.size.height);
    startSessionButton.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [startSessionButton addTarget:self action:@selector(startSessionAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:startSessionButton];
    
    UILabel *label2=[[UILabel alloc] initWithFrame:CGRectMake(15, startSessionButton.bottom+15.0, self.view.width-30, 50)];
    label2.textColor=[UIColor lightGrayColor];
    label2.backgroundColor=[UIColor clearColor];
    label2.autoresizingMask=UIViewAutoresizingFlexibleWidth;
    label2.font=[UIFont grotesqueFontOfSize:13.0];
    label2.numberOfLines=2;
    label2.text=@"Alternatively, you can schedule a visit for a later date and set a reminder for yourself.";
    label2.textColor = [UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1.0];
    [self.view addSubview:label2];
    [label2 release];
    
    UIImage *scheduleImage=[UIImage imageNamed:@"schedule_visit_button.png"];
    UIButton *scheduleVisitButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [scheduleVisitButton setBackgroundImage:scheduleImage forState:UIControlStateNormal];
    scheduleVisitButton.frame=CGRectMake((CGRectGetWidth(self.view.bounds)-scheduleImage.size.width)/2.0, label2.bottom +5,scheduleImage.size.width, scheduleImage.size.height);
    scheduleVisitButton.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [scheduleVisitButton addTarget:self action:@selector(scheduleVisitAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:scheduleVisitButton];

}

/*
-(void)startSessionAction:(UIButton *)sender
{   
    ScheduleVisitListViewController *scheduleVisitListController = [[ScheduleVisitListViewController alloc] init];
    
      VisitTypesViewcontroller *visitTypesVC=[[VisitTypesViewcontroller alloc] init];
        visitTypesVC.isReadonlyMode=NO;
    
    
      visitTypesVC.contentSizeForViewInPopover=CGSizeMake(320, 285.0);
    
    
      UINavigationController *_navController=[[UINavigationController alloc] initWithRootViewController:scheduleVisitListController];
    
    
    UIPopoverController *aPopoverController=[[UIPopoverController alloc] initWithContentViewController:_navController];
      self.currentPopoverController=aPopoverController;
    
    [scheduleVisitListController addDidSelectScheduleVisit:^(ScheduledVisit *visit, NSIndexPath *indexPath) 
    {
        
    }];
        
     [visitTypesVC addNatureOfVisitDidSelectCallback:^(NSInteger visitNatureID)
     {
            
        [aPopoverController dismissPopoverAnimated:YES];
         Visit *_newVisit=[CoreDataHandler newVisit];
         _newVisit.natureOfVisitID = [NSNumber numberWithInteger:visitNatureID];
         _newVisit.parentCustomer=self.customer;
      
        // NSString *_dateString  = [[NSDate date] stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
         _newVisit.startDate=[NSDate date];
         
         [self.customer addVisitsObject:_newVisit];
         
         /// ====================== SET LAST VISIT DATE ===========================
      
         NSString * _lastVisitdateString  = [[NSDate date] stringFromDateWithFormat:@"dd/MM/yyyy"];
         self.customer.lastVisit=_lastVisitdateString;
         
       //  VisitReviewSessionViewController *visitReviewSessionVC=[[VisitReviewSessionViewController alloc] initWithVisit:_newVisit forCustomer:self.customer];
            
        VisitSessionViewController *visitReviewSessionVC=[[VisitSessionViewController alloc] initWithVisit:_newVisit forCustomer:self.customer];
         
         UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:visitReviewSessionVC];
         [app.splitViewController presentModalViewController:navController animated:YES];
         [visitReviewSessionVC release];
         [navController release];
         
         [self.customerVisitDetailsController reloadData];
         [_newVisit release];

     }];   
        

    [aPopoverController presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    [scheduleVisitListController release];
    [visitTypesVC release];
    [_navController release];
    [aPopoverController release];
    
}
*/

-(void)startVisitSessionForScheduleVisit:(ScheduledVisit *)aScheduleVisit
{
    VisitTypesViewcontroller *visitTypesVC = [[VisitTypesViewcontroller alloc] init];
    visitTypesVC.delegate = self;
    visitTypesVC.isReadonlyMode=NO;
    visitTypesVC.contentSizeForViewInPopover=CGSizeMake(320, 285.0);
    UINavigationController *_navController=[[UINavigationController alloc] initWithRootViewController:visitTypesVC];
    
    if(!self.currentPopoverController)
    {
       UIPopoverController *aPopoverController=[[UIPopoverController alloc] initWithContentViewController:_navController];


if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
       
        aPopoverController.popoverBackgroundViewClass = [MBPopoverBackgroundView class];


       self.currentPopoverController=aPopoverController;
      [aPopoverController release];
    }
    else
    {
      [self.currentPopoverController setContentViewController:_navController animated:YES];
    }
    
    if(aScheduleVisit)
    [self setAssociateObject:aScheduleVisit forKey:currentScheduleVisitKey];
    
    /*
    [visitTypesVC addNatureOfVisitDidSelectCallback:^(NSInteger visitNatureID)
     {
         if(!aScheduleVisit)
         {
             NSDate *startDate = [NSDate date];
             NSDate *endDate = [startDate dateByAddingTimeInterval:60*60*1];
             
             if(startDate)
                 startDate = [startDate currentDateHourMinute];
             
             if(endDate)
                 endDate = [endDate currentDateHourMinute];
             
                         
             [CoreDataHandler newScheduledVisitWithCustomer:self.customer startDate:startDate endDate:endDate checkDuplicate:NO completion:^(ScheduledVisit *event)
              {
                  //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                  
                    dispatch_async([app eventQueue], ^{
                      
                    NSString *eventTitle = [NSString stringWithFormat:@"Visit: %@",self.customer?self.customer.name:@"visit"];
                        
                      EKEvent *_event = [CustomEventStore addEventWithTitle:eventTitle startDate:startDate endDate:endDate location:nil notes:nil];
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          
                          event.eventIdentifier = _event.eventIdentifier;
                          
                           /// NSLog(@"Event identifier=%@",_event.eventIdentifier);
                           // [[SSCoreDataManager sharedManager] save:nil];
                      });
                  });

                   [self.currentPopoverController dismissPopoverAnimated:YES];
                   event.isAutoGenerated = [NSNumber numberWithBool:YES];
                  
                   [[SSCoreDataManager sharedManager] save:nil];
                  

                  [[OfflineManager sharedManager] addScheduledVisit:event willDelete:NO isEditMode:NO];

                 
                  Visit *_newVisit=[CoreDataHandler newVisit];
                  _newVisit.natureOfVisitID = [NSNumber numberWithInteger:visitNatureID];
                  _newVisit.parentCustomer=self.customer;
                  //_newVisit.scheduleVisitID = event.scheduleVisitID;
                  _newVisit.scheduledVisit = event;
                  _newVisit.startDate=[NSDate date];
                
                  
                  //SET  VISIT ID TO _newVisit HERE
                  [self.customer addVisitsObject:_newVisit];
                  
                    event.reviewedVisitID = _newVisit.visitID;
                  
                  /// ====================== SET LAST VISIT DATE ===========================
                  
                  NSString * _lastVisitdateString  = [[NSDate date] stringFromDateWithFormat:CUSTOMER_LAST_VISIT_FORMAT];
                  self.customer.lastVisit=_lastVisitdateString;
                  self.customer.nextVisit = nil;
                  
                  //  VisitReviewSessionViewController *visitReviewSessionVC=[[VisitReviewSessionViewController alloc] initWithVisit:_newVisit forCustomer:self.customer];
                  
                  VisitSessionViewController *visitReviewSessionVC=[[VisitSessionViewController alloc] initWithVisit:_newVisit forCustomer:self.customer];
                  
                  UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:visitReviewSessionVC];
                  [app.splitViewController presentModalViewController:navController animated:YES];
                  [visitReviewSessionVC release];
                  [navController release];
                  
                  [self.customerVisitDetailsController reloadData];
                  [_newVisit release];
                  
                

              }];
         }
         else 
         {
         [self.currentPopoverController dismissPopoverAnimated:YES];
         Visit *_newVisit=[CoreDataHandler newVisit];
         _newVisit.natureOfVisitID = [NSNumber numberWithInteger:visitNatureID];
         //_newVisit.parentCustomer=self.customer;
          _newVisit.scheduledVisit = aScheduleVisit;
          aScheduleVisit.reviewedVisitID = _newVisit.visitID;
         // NSString *_dateString  = [[NSDate date] stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
         _newVisit.startDate=[NSDate date];
          [self.customer addVisitsObject:_newVisit];
             
             //===== SHIFT THE DATE OF SCHEDULED VISIT TO CURRENT DATE=====
            NSDate *startDate = _newVisit.startDate;
            NSDate *endDate = [startDate dateByAddingTimeInterval:60*60*1];
             
             if(startDate)
                 startDate = [startDate currentDateHourMinute];
             
             if(endDate)
                 endDate = [endDate currentDateHourMinute];
             
             
             aScheduleVisit.visitStartDate = startDate;
             aScheduleVisit.visitEndDate = endDate;
             
             [[OfflineManager sharedManager] addScheduledVisit:aScheduleVisit willDelete:NO isEditMode:YES];
         
         /// ====================== SET LAST VISIT DATE ===========================
         
         NSString * _lastVisitdateString  = [[NSDate date] stringFromDateWithFormat:@"dd/MM/yyyy"];
         self.customer.lastVisit=_lastVisitdateString;
             self.customer.nextVisit = nil;
         
         //  VisitReviewSessionViewController *visitReviewSessionVC=[[VisitReviewSessionViewController alloc] initWithVisit:_newVisit forCustomer:self.customer];
         
         VisitSessionViewController *visitReviewSessionVC=[[VisitSessionViewController alloc] initWithVisit:_newVisit forCustomer:self.customer];
         
         UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:visitReviewSessionVC];
         [app.splitViewController presentModalViewController:navController animated:YES];
         [visitReviewSessionVC release];
         [navController release];
         
         [self.customerVisitDetailsController reloadData];
         [_newVisit release];
         
             
        dispatch_async([app eventQueue], ^{
                 
                 NSString *eventTitle = [NSString stringWithFormat:@"Visit: %@",self.customer?self.customer.name:@"visit"];
                 
                 EKEvent *_event = [CustomEventStore editEventForIdentifier:aScheduleVisit.eventIdentifier withTitle:eventTitle startDate:aScheduleVisit.visitStartDate endDate:aScheduleVisit.visitEndDate location:nil notes:nil];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     aScheduleVisit.eventIdentifier = _event.eventIdentifier;
                     
                     // [[SSCoreDataManager sharedManager] save:nil];
                 });
             });
         }
        
     }];
    */
    
    if(!self.currentPopoverController.isPopoverVisible)
        [self.currentPopoverController presentPopoverFromRect:startSessionButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    [visitTypesVC release];
    [_navController release];

}

-(void)visitTypesViewcontroller:(VisitTypesViewcontroller*)visitTypesVC didSelectNatureOfVisit:(NSInteger) visitNatureID
{
    ScheduledVisit *aScheduleVisit = [self associateObjectForKey:currentScheduleVisitKey];
    
    if(!aScheduleVisit)
    {
        NSDate *startDate = [NSDate date];
        NSDate *endDate = [startDate dateByAddingTimeInterval:60*60*1];
        
        if(startDate)
            startDate = [startDate currentDateHourMinute];
        
        if(endDate)
            endDate = [endDate currentDateHourMinute];
        
        /*
         if([[OfflineManager sharedManager] isNetworkAvailable])
         {
         [UIAlertView showWarningAlertWithTitle:@"Please try again later" message:@"Poor connection.\nYou need a active connection to create a visit"];
         return ;
         }
         */
        
        [CoreDataHandler newScheduledVisitWithCustomer:self.customer startDate:startDate endDate:endDate checkDuplicate:NO completion:^(ScheduledVisit *event)
         {
             //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
             
             dispatch_async([app eventQueue], ^{
                 
                 NSString *eventTitle = [NSString stringWithFormat:@"Visit: %@",self.customer?self.customer.name:@"visit"];
                 
                 EKEvent *_event = [CustomEventStore addEventWithTitle:eventTitle startDate:startDate endDate:endDate location:nil notes:nil];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     event.eventIdentifier = _event.eventIdentifier;
                     
                     /// NSLog(@"Event identifier=%@",_event.eventIdentifier);
                     // [[SSCoreDataManager sharedManager] save:nil];
                 });
             });
             
             [self.currentPopoverController dismissPopoverAnimated:YES];
             event.isAutoGenerated = [NSNumber numberWithBool:YES];
             
             [[SSCoreDataManager sharedManager] save:nil];
             

             [[OfflineManager sharedManager] addScheduledVisit:event willDelete:NO isEditMode:NO];

             
             Visit *_newVisit=[CoreDataHandler newVisit];
             _newVisit.natureOfVisitID = [NSNumber numberWithInteger:visitNatureID];
             _newVisit.parentCustomer=self.customer;
             //_newVisit.scheduleVisitID = event.scheduleVisitID;
             _newVisit.scheduledVisit = event;
             _newVisit.startDate=[NSDate date];
             
             
             //SET  VISIT ID TO _newVisit HERE
             [self.customer addVisitsObject:_newVisit];
             
             event.reviewedVisitID = _newVisit.visitID;
             
             /// ====================== SET LAST VISIT DATE ===========================
             
             NSString * _lastVisitdateString  = [[NSDate date] stringFromDateWithFormat:CUSTOMER_LAST_VISIT_FORMAT];
             self.customer.lastVisit=_lastVisitdateString;
             self.customer.nextVisit = nil;
             
             //  VisitReviewSessionViewController *visitReviewSessionVC=[[VisitReviewSessionViewController alloc] initWithVisit:_newVisit forCustomer:self.customer];
             
             VisitSessionViewController *visitReviewSessionVC=[[VisitSessionViewController alloc] initWithVisit:_newVisit forCustomer:self.customer];
             
             UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:visitReviewSessionVC];
             [app.splitViewController presentModalViewController:navController animated:YES];
             [visitReviewSessionVC release];
             [navController release];
             
             [self.customerVisitDetailsController reloadData];
             [_newVisit release];
             
             
             
         }];
    }
    else
    {
        [self.currentPopoverController dismissPopoverAnimated:YES];
        Visit *_newVisit=[CoreDataHandler newVisit];
        _newVisit.natureOfVisitID = [NSNumber numberWithInteger:visitNatureID];
        //_newVisit.parentCustomer=self.customer;
        _newVisit.scheduledVisit = aScheduleVisit;
        aScheduleVisit.reviewedVisitID = _newVisit.visitID;
        // NSString *_dateString  = [[NSDate date] stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
        _newVisit.startDate=[NSDate date];
        [self.customer addVisitsObject:_newVisit];
        
        //===== SHIFT THE DATE OF SCHEDULED VISIT TO CURRENT DATE=====
        NSDate *startDate = _newVisit.startDate;
        NSDate *endDate = [startDate dateByAddingTimeInterval:60*60*1];
        
        if(startDate)
            startDate = [startDate currentDateHourMinute];
        
        if(endDate)
            endDate = [endDate currentDateHourMinute];
        
        
        aScheduleVisit.visitStartDate = startDate;
        aScheduleVisit.visitEndDate = endDate;
        
        [[OfflineManager sharedManager] addScheduledVisit:aScheduleVisit willDelete:NO isEditMode:YES];
        
        /// ====================== SET LAST VISIT DATE ===========================
        
        NSString * _lastVisitdateString  = [[NSDate date] stringFromDateWithFormat:@"dd/MM/yyyy"];
        self.customer.lastVisit=_lastVisitdateString;
        self.customer.nextVisit = nil;
        
        //  VisitReviewSessionViewController *visitReviewSessionVC=[[VisitReviewSessionViewController alloc] initWithVisit:_newVisit forCustomer:self.customer];
        
        VisitSessionViewController *visitReviewSessionVC=[[VisitSessionViewController alloc] initWithVisit:_newVisit forCustomer:self.customer];
        
        UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:visitReviewSessionVC];
        [app.splitViewController presentModalViewController:navController animated:YES];
        [visitReviewSessionVC release];
        [navController release];
        
        [self.customerVisitDetailsController reloadData];
        [_newVisit release];
        
        
        dispatch_async([app eventQueue], ^{
            
            NSString *eventTitle = [NSString stringWithFormat:@"Visit: %@",self.customer?self.customer.name:@"visit"];
            
            EKEvent *_event = [CustomEventStore editEventForIdentifier:aScheduleVisit.eventIdentifier withTitle:eventTitle startDate:aScheduleVisit.visitStartDate endDate:aScheduleVisit.visitEndDate location:nil notes:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                aScheduleVisit.eventIdentifier = _event.eventIdentifier;
                
                [self setAssociateObject:nil forKey:currentScheduleVisitKey];
                
                // [[SSCoreDataManager sharedManager] save:nil];
            });
        });
    }
    
}

-(NSMutableArray *)loadUpcommingUnreviewedScheduledVisits
{
     NSDate *_currentDate=[NSDate date];
    
     // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(self.customer.customerID=%@ AND self.reviewedVisitID=%@) AND (self.visitStartDate<=%@ AND self.visitEndDate>=%@)",self.customer.customerID,[NSNumber numberWithLong:0] ,_currentDate,_currentDate];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"((self.customer.customerID=%@ OR customerID=%@) AND self.reviewedVisitID=%@) AND (self.visitEndDate>=%@)",self.customer.customerID,self.customer.customerID,[NSNumber numberWithLong:0] ,_currentDate,_currentDate];
    NSMutableArray *scheduleVisits = [ScheduledVisit fetchWithPredicate:predicate error:nil];
    
    return scheduleVisits;
}

-(void)startSessionAction:(UIButton *)sender
{   
    //sender.userInteractionEnabled=NO;
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.customer.customerID=%@ AND (self.visitStartDate>=%@ AND self.reviewedVisitID==nil)",self.customer.customerID,[NSDate date]];
    
    //app.fromCustomerTask=1;
    
    NSLog(@"%@",self.customer.customerID);
        //NSLog(@"%@",self.customer.customerID);
    
    if(![CoreDataHandler checkDealersiteDealsCustomer:self.customer withAlertTitle:nil message:nil])
    {
        return;
    }
    
    
    NSMutableArray *scheduleVisits = [self loadUpcommingUnreviewedScheduledVisits];
    
    NSLog(@" scheduleVisits %@",scheduleVisits);
    
    // IF THERE IS NO SCHEDULE VISIT CREATE A SCHEDULE VISIT INTERNALLY
    if(![scheduleVisits count])
    {
        //Create new schedule visit
        
        [self startVisitSessionForScheduleVisit:nil];
        
        /*
        NSDate *startDate = [NSDate date];
        NSDate *endDate = [startDate dateByAddingTimeInterval:60*60*1];
        [ADDVisit createNewVisitForCustomer:self.customer startDate:startDate endDate:endDate completion:^(ScheduledVisit *event) 
        {
            event.isAutoGenerated = [NSNumber numberWithBool:YES];
            [self startVisitSessionForScheduleVisit:event];
            
            [self.currentPopoverController presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }];
        */
        
        return;
    }
    
    ScheduleVisitListViewController *scheduleVisitListController = [[ScheduleVisitListViewController alloc] init];
    scheduleVisitListController.customer = self.customer;
    scheduleVisitListController.visitsDataSource = scheduleVisits;
    scheduleVisitListController.contentSizeForViewInPopover=CGSizeMake(320, 285.0);
    
    UINavigationController *_navController=[[UINavigationController alloc] initWithRootViewController:scheduleVisitListController];
    
    
    UIPopoverController *aPopoverController=[[UIPopoverController alloc] initWithContentViewController:_navController];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    aPopoverController.popoverBackgroundViewClass = [MBPopoverBackgroundView class];

    self.currentPopoverController=aPopoverController;
    
    __block CustomerVisitSessionsViewController *weakSelf = self;
    [scheduleVisitListController addDidSelectScheduleVisit:^(ScheduledVisit *visit, NSIndexPath *indexPath) 
     {
         [weakSelf startVisitSessionForScheduleVisit:visit];
     }];
    
    [aPopoverController presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    [scheduleVisitListController release];
    [_navController release];
    [aPopoverController release];
}

-(void)scheduleVisitAction:(id)sender
{
    //========= CHECK ANY CUSTOMER DEALING WITH ANY DEALER SITE OEMS ===========
    if(![CoreDataHandler checkDealersiteDealsCustomer:self.customer withAlertTitle:nil message:nil])
    {
        return;
    }

    
#if SCHEDULE_VISIT_DIRECTLY
    
    UIButton *btn=(UIButton *)sender;
    
    NSString *customerName = self.customer.name;
    NSString *suburbName   = self.customer.suburb;
    suburbName = suburbName.length ? suburbName : self.customer.city;
    
    Start_EndDate *_visit = [[Start_EndDate alloc] init];
    _visit.isFromCustomer=YES;
    _visit.title = @"SCHEDULE VISIT";
    
    [_visit addDoneActionCallback:^(NSDate *startDate, NSDate *endDate) 
    {
        /*
        if([ADDVisit createNewVisitForTitle:customerName suburbName:suburbName startDate:startDate endDate:endDate])
          [self.reminderPopover dismissPopoverAnimated:YES];
        else
            [UIAlertView showWarningAlertWithTitle:@"Error" message:@"Cannot schedule a visit"];
         */
        
        [ADDVisit createNewVisitForTitle:customerName suburbName:suburbName startDate:startDate endDate:endDate completion:^(EKEvent *event) 
         {
             NSLog(@"New visit:%@",event);
             
             if(event)
             {
                  [self.reminderPopover dismissPopoverAnimated:YES];
             }
             else 
             {
                  // [UIAlertView showWarningAlertWithTitle:@"Error" message:@"Cannot schedule a visit"];
                  NSLog(@"Visit creation cancelled");
             }
             
         }];
    }];
    
   
    //_visit.contentSizeForViewInPopover = CGSizeMake(335,400);
    UINavigationController *_navController= [[UINavigationController alloc] initWithRootViewController:_visit];
    
    
    UIPopoverController *_reminderPopover =self.reminderPopover;
    if(!_reminderPopover)
    {
        _reminderPopover = [[UIPopoverController alloc] initWithContentViewController:_navController];
        

        self.reminderPopover=_reminderPopover;
        [_reminderPopover release];
    }
    else
    {
        _reminderPopover.contentViewController=_navController;
    }
    
    _reminderPopover.popoverContentSize = CGSizeMake(335,415);
    
    [_reminderPopover presentPopoverFromRect:btn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    [_navController release];
    [_visit release];
    
#else
    
     UIButton *btn=(UIButton *)sender;
    btn.userInteractionEnabled = NO;
    CalendarViewController *calendarViewController   = [[CalendarViewController alloc] init];
    calendarViewController.title = @"Calendar";
    UINavigationController   *navController=[[UINavigationController alloc] initWithRootViewController:calendarViewController];
    [app.splitViewController presentModalViewController:navController animated:NO];
    
    [calendarViewController performSelector:@selector(scheduleNewVisitForCustomer:) withObject:self.customer afterDelay:0.1];
    
    /*
    [calendarViewController addCalenderDidLoadCallbak:^{
        
        [calendarViewController scheduleNewVisitForCustomer:self.customer];
    }];
    */
    
    
    [calendarViewController release];
    [navController release];
     btn.userInteractionEnabled = YES;

#endif
}



- (void)dismissPopOverFromViewVisit:(BOOL)done
{
    if(done)
    {
        [self.reminderPopover dismissPopoverAnimated:YES];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

@end
