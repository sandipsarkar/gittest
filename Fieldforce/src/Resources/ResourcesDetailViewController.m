//
//  ResourceDetailViewController.m
//  CollectionViewAccordionTest
//
//  Created by Randem IT on 19/11/13.
//  Copyright (c) 2013 Randem IT. All rights reserved.
//

#import "ResourcesDetailViewController.h"
#import "TreeViewNode.h"
#import "DocPreviewController.h"
#import "ResourceFileDetailCell.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "OfflineManager.h"
#import "CustomerListViewController.h"
#import "WholesaleDealer.h"
#import "Rep.h"

#define HEADER_HEIGHT 65.0 ////45.0

static NSString *CellIdentifier = @"Cell";

@interface ResourcesDetailViewController ()<MFMailComposeViewControllerDelegate>
{
    UILabel *_headerTitleLabel;
}


@property (nonatomic,OBJC_STRONG) UIPopoverController *customerPopover;

@end

@implementation ResourcesDetailViewController

#if !__has_feature(objc_arc)
-(void)dealloc
{
    OBJC_RELEASE(_headerTitleLabel);
    OBJC_RELEASE(_customerPopover);
    OBJC_RELEASE(_dataSource);
    
    [super dealloc];
}
#endif

- (id)init
{
    self = [super initWithStyle:UITableViewStylePlain];
    
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.view.backgroundColor = [UIColor lightGrayColor];

     [self.tableView registerClass:[ResourceFileDetailCell class] forCellReuseIdentifier:CellIdentifier];
    
   self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.tableView.separatorInset = UIEdgeInsetsMake(0, -10, 0, 0);
    }
    
    self.tableView.sectionHeaderHeight = HEADER_HEIGHT;
    
    self.tableView.bounces = NO;
    
   // [self setupHeaderView];
    
    
}

-(void)setupHeaderView
{
    UIView* _header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 65.0)] ;
     _header.backgroundColor = [UIColor colorWithRed:165.0/255.0 green:165.0/255.0 blue:165.0/255.0 alpha:1.0];
        
    _headerTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, CGRectGetWidth(_header.bounds)-20.0, CGRectGetHeight(_header.bounds))];
    _headerTitleLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    _headerTitleLabel.tag = 1;
    _headerTitleLabel.numberOfLines = 2;
    _headerTitleLabel.textColor = [UIColor whiteColor];
    _headerTitleLabel.backgroundColor = [UIColor clearColor];
    _headerTitleLabel.font = (self.currentNode.nodeLevel ==1) ? [UIFont subaruMediumFontOfSize:22.0] : [UIFont subaruBookFontOfSize:19.0];
    [_header addSubview:_headerTitleLabel];
    
    _header.backgroundColor = (self.currentNode.nodeLevel ==1) ? [UIColor colorWithRed:99.0/255.0 green:100.0/255.0 blue:102.0/255.0 alpha:1.0] :[UIColor colorWithRed:165.0/255.0 green:165.0/255.0 blue:165.0/255.0 alpha:1.0];
    
   // self.tableView.tableHeaderView  = _header;
    [self.view addSubview:_header];
    
    OBJC_RELEASE(_header);
    
    /*
    _headerTitleLabel.text = self.currentNode.title;
    
    CGRect _tableViewFrame = CGRectMake(0, CGRectGetMaxY(_header.frame), CGRectGetWidth(self.view.bounds),  CGRectGetHeight(self.view.bounds));
    
    UITableView *__tableView = [UITableView tableViewWithFrame:_tableViewFrame style:UITableViewStylePlain delegate:self datasource:self];
    __tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:__tableView];
   // self.tableView = __tableView;
    */
}

-(void)addResource:(TreeViewNode *)node
{
    if(!node) return;
    
    if(!_dataSource) _dataSource = [[NSMutableArray alloc] init];
    
    [_dataSource addObject:node];
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void)addResources:(NSArray *)nodes forResource:(TreeViewNode *)node
{
    if(!nodes || ![nodes count]) return;
    
    [_dataSource removeAllObjects];
    
    if(!_dataSource) _dataSource = [[NSMutableArray alloc] init];
    
    self.currentNode = node;
    
    //--------------------
   // self.currentNode.isExpanded = YES;
    
    //-------------
    
    //int _prevCount = [_dataSource count];
    
    [_dataSource addObjectsFromArray:nodes];
    
    [self.tableView reloadData];
    
    
    
  //  [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationRight];
    
   // [self animateShow];
    
    /*
    int _currentCount = [_dataSource count];
    
    int _changesCount = _currentCount-_prevCount;
    
    
    
    NSMutableArray *indexPathsToInsert = [NSMutableArray array];
    
    for(int i = 0;i<_changesCount;i++)
    {
        int _index =  _prevCount+i;
        
        NSIndexPath *_indexPath = [NSIndexPath indexPathForRow:_index inSection:0];
        [indexPathsToInsert addObject:_indexPath];
    }
    
    if([indexPathsToInsert count])
     {
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
     }
    */
    
}

-(void)removeResources:(NSArray *)resources
{
    if(!resources) return;
    
   //  int _prevCount = [_dataSource count];
    
    self.currentNode = nil;
    
    [_dataSource removeObjectsInArray:resources];
    
    
    
    //[self animateHide];
    
    //[self.tableView reloadData];
    
    //[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    
    
    /*
    int _currentCount = [_dataSource count];
    
    int _changesCount = _prevCount-_currentCount;
    
    
    NSMutableArray *indexPathsToRemove = [NSMutableArray array];
    
    for(int i = 0;i<_changesCount;i++)
    {
        int _index =  _currentCount+i;
        
        NSIndexPath *_indexPath = [NSIndexPath indexPathForRow:_index inSection:0];
        [indexPathsToRemove addObject:_indexPath];
    }

     if([indexPathsToRemove count])
     {
       [self.tableView beginUpdates];
       [self.tableView deleteRowsAtIndexPaths:indexPathsToRemove withRowAnimation:UITableViewRowAnimationFade];
       [self.tableView endUpdates];
     }
    */
}

-(void)removeAllResources
{
    //---------
  //  self.currentNode.isExpanded = NO;
    //----------
    
     self.currentNode = nil;
    
    [_dataSource removeAllObjects];
}

/*
-(void)animateShow
{
     self.view.hidden = NO;
   
    CGRect _visibleFrame = self.view.frame;
    _visibleFrame.origin.x = CGRectGetWidth(self.parentViewController.view.bounds)/2.0;

    CGRect _hiddenFrame = _visibleFrame;
    
    _hiddenFrame.origin.x-= CGRectGetWidth(_hiddenFrame);
    
    self.view.frame = _hiddenFrame;
    [UIView animateWithDuration:0.3 animations:^{
        
        
    self.view.frame = _visibleFrame;
        
    }
    completion:^(BOOL finished)
    {
        
       
    }];
}

-(void)animateHide
{
    CGRect _visibleFrame = self.view.frame;
    _visibleFrame.origin.x = CGRectGetWidth(self.parentViewController.view.bounds)/2.0;
    
    CGRect _hiddenFrame = _visibleFrame;
    _hiddenFrame.origin.x-= CGRectGetWidth(_hiddenFrame);
    
     self.view.frame = _visibleFrame;
    
    [UIView animateWithDuration:0.3 animations:^{
        

      self.view.frame = _hiddenFrame;
    }
    completion:^(BOOL finished)
    {
         
        self.view.hidden = YES;
     }];
}
 */

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [self.dataSource count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ResourceFileDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [cell.viewButton addTarget:self action:@selector(viewResourceAction:) forControlEvents:UIControlEventTouchUpInside];
     [cell.emailButton addTarget:self action:@selector(sendEmailAction:) forControlEvents:UIControlEventTouchUpInside];
    
    TreeViewNode *node = (indexPath.row <[self.dataSource count]) ? [self.dataSource objectAtIndex:indexPath.row] : nil;
    [cell setResourceFileNode:node];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   // return 104.0f;
    
     return 106.0f;
}

/*
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    UIFont *_expectedFont = (self.currentNode.nodeLevel ==1) ? [UIFont subaruMediumFontOfSize:22.0] : [UIFont subaruBookFontOfSize:19.0];
    CGFloat _expectedWidth = CGRectGetWidth(tableView.bounds)-20.0;
    
    NSString *_headerTitle = self.currentNode.title;
    
    CGSize _expectedSize = [_headerTitle sizeWithFont:_expectedFont constrainedToSize:CGSizeMake(_expectedWidth, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    
    
    CGFloat _height = self.currentNode.nodeLevel==0? 45.0 : MAX(_expectedSize.height+10.0*2, 45.0);
    
    return _height;
}
*/

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *HeaderIdentifier = @"Header";
    
    UIView *_header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:HeaderIdentifier];
    
    UILabel *titleLabel = (UILabel *) [_header viewWithTag:1];
    
    if(!_header)
    {
        _header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 46.0)] ;
        OBJC_AUTORELEASE(_header);
        
        _header.backgroundColor = [UIColor colorWithRed:165.0/255.0 green:165.0/255.0 blue:165.0/255.0 alpha:1.0];
        
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, CGRectGetWidth(_header.bounds)-20.0, CGRectGetHeight(_header.bounds))];
        titleLabel.tag = 1;
        titleLabel.numberOfLines = 2;
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [_header addSubview:titleLabel];
        OBJC_RELEASE(titleLabel);
    }
    
    titleLabel.font = (self.currentNode.nodeLevel ==1) ? [UIFont subaruMediumFontOfSize:22.0] : [UIFont subaruBookFontOfSize:19.0];
    
    _header.backgroundColor = (self.currentNode.nodeLevel ==1) ? [UIColor colorWithRed:99.0/255.0 green:100.0/255.0 blue:102.0/255.0 alpha:1.0] :[UIColor colorWithRed:165.0/255.0 green:165.0/255.0 blue:165.0/255.0 alpha:1.0];
    
    titleLabel.text = self.currentNode.title;
    
  //  _header.hidden =  !self.currentNode || !self.dataSource.count;
    
    return _header;
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    

}


-(void)viewResourceAction:(UIButton *)sender
{
    ResourceFileDetailCell *cell =(ResourceFileDetailCell *)[sender superViewOfType:[ResourceFileDetailCell class]];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
   // TreeViewNode *node = (indexPath.row <[self.dataSource count]) ? [self.dataSource objectAtIndex:indexPath.row] : nil;
     TreeViewNode *node = (indexPath.row <[self.dataSource count]) ? self.dataSource[indexPath.row] : nil;
    
    NSLog(@"Node title = %@",node.title);

  //  if(!node.fileName) return;
    
    DocPreviewController *_docPreviewController = [[DocPreviewController alloc] init];
    _docPreviewController.fileNode = node;
   
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:_docPreviewController];
   //  navController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self.parentViewController presentViewController:navController animated:YES completion:nil];
    
    
    OBJC_RELEASE(_docPreviewController);
    OBJC_RELEASE(navController);
    
}

#pragma mark - EMAIL
-(void)sendEmailAction:(UIButton *)sender
{
    [self showCustomersPopoverAtView:sender];
    
    UITableViewCell *cell = (UITableViewCell *)[sender superViewOfType:[UITableViewCell class]];
    
    NSIndexPath *selectedIndexPath = cell ?  [self.tableView indexPathForCell:cell] : nil;
    
     TreeViewNode *node = (selectedIndexPath.row <[self.dataSource count]) ? self.dataSource[selectedIndexPath.row] : nil;
    
    [self setAssociateObject:node forKey:@"_selectedResource"];
    
    
}

-(NSString *)setupMailBodyForFileResource:(ResourceMaster *)fileResource
{
    NSString  *originalFileName = [fileResource resourceFileName];
    NSString  *fileDownloadLink = [NSString stringWithFormat:@"%@%@",RESOURCE_FILE_DOWNLOAD_LINK,originalFileName];

    NSString  *resourceName = fileResource.resourceName ;
    
    NSMutableString *_formatStr= [NSMutableString stringWithString:@"<html><body style=\"padding:0 0 0 0; margin:0 auto;\"> <table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">"];
    
    
    NSString *_headerString =  @"<tr> <td style=\"padding:0 0 0 0; margin:0 0 0 0; width:10px;\"></td> <td><table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td style=\"padding:0 0 0 0; margin:0 0 0 0; height:10px;\"></td> </tr> <tr> <td style=\"padding:0 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\">Hi,</td> </tr>";
    
    [_formatStr appendString:_headerString];
    
    
    /*
     NSString *resourceString = [NSString stringWithFormat:@"<tr><td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\"> <span style=\"color:#175999;\"></span> Please find below resource information: <span style=\"color:#175999;\">%@</span></td> </tr>",@""] ;
     
     [_formatStr appendString:resourceString];
     */
    
    if(fileDownloadLink)
    {
        NSString *resourceURLString = fileDownloadLink;
        if([fileDownloadLink rangeOfString:@"http://"].location==NSNotFound)
        {
            resourceURLString = [@"http://" stringByAppendingString:fileDownloadLink];
        }
        
        if(resourceURLString)
        {
          //NSString *linkStr=[NSString stringWithFormat:@"<tr><td style=\"padding:10px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\">&nbsp;</td> </tr><tr><td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\">To download this resource please <a href=\"%@\" style=\"color:#175999;\">click here</a></td> </tr>",resourceURLString];
            
            NSString *linkStr=[NSString stringWithFormat:@"<tr><td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\">Please <a href=\"%@\" style=\"color:#175999;\">click here</a> to view the file %@.</td> </tr>",resourceURLString,resourceName];
            
          [_formatStr appendString:linkStr];
            
            
        }
    }
    
    NSString *endStr=@"<td style=\"padding:0 0 0 0; margin:0 0 0 0; height:10px;\"></td> </tr> </table></td> <td style=\"padding:0 0 0 0; margin:0 0 0 0;  width:10px;\"></td> </tr> </table> </body> </html>";
    [_formatStr appendString:endStr];
    
    
    //======= DEMO =======
    //_formatStr =[NSMutableString stringWithString:fileDownloadLink];
    
    return _formatStr;
}


-(void)showCustomersPopoverAtView:(UIView *)view
{
    UIPopoverController *popover=self.customerPopover;
    
    if(!popover)
    {
        CustomerListViewController *_customerListViewController=[[CustomerListViewController alloc] initWithCustomerName:@""];
        _customerListViewController.allowMultipleSelection=YES;
        
        
        __block ResourcesDetailViewController *weakSelf = self;
        [_customerListViewController addDidSelectCustomersCallback:^(NSMutableArray *selectedCustomers) {
            
            NSMutableArray *nameOfCustomers = [NSMutableArray array];
            NSMutableArray *receipents     = [NSMutableArray array];
            for(Customer *_customer in selectedCustomers)
            {
                if(_customer.email.length)
                {
                    NSLog(@"Email=%@",_customer.email);
                    [receipents addObject:_customer.email];
                }
                else
                {
                    if(_customer.name.length)
                        [nameOfCustomers addObject:_customer.name];
                }
            }
            
            if([nameOfCustomers count])
            {
                NSString *_lastCustomerName  = [nameOfCustomers lastObject];
                NSString *lastCustomerName   = [_lastCustomerName copy];
                NSMutableArray* nameOfCustomersTempArray = [nameOfCustomers copy];
                [nameOfCustomers removeObject:_lastCustomerName];
                
                NSString *customersNameStr = ([nameOfCustomers count]>=1)? [nameOfCustomers componentsJoinedByString:@", "] : nil;
                
                customersNameStr = customersNameStr.length ?  [NSString stringWithFormat:@"%@ and %@",customersNameStr,lastCustomerName] : lastCustomerName;
                
                
                NSLog(@"Customers name=%@",customersNameStr);
                
                NSString *message = [NSString stringWithFormat:([nameOfCustomersTempArray count]>1)?OFFER_EMAIL_ALERT_FORMAT_MULTIPLE:OFFER_EMAIL_ALERT_FORMAT,customersNameStr];
                
                 OBJC_RELEASE(nameOfCustomersTempArray);
                 OBJC_RELEASE(lastCustomerName);
               
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
                
                OBJC_RELEASE(alert);
            }
            
            [weakSelf performSelector:@selector(showEmailClientWithToRecipents:) withObject:receipents];
            [weakSelf.customerPopover dismissPopoverAnimated:NO];
            weakSelf.customerPopover=nil;
            
        }];
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:_customerListViewController];
        popover=[[UIPopoverController alloc] initWithContentViewController:navController];
        popover.popoverContentSize = CGSizeMake(335.0+15.0, 800.0);
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            popover.popoverBackgroundViewClass = [MBPopoverBackgroundView class];
        
        
        self.customerPopover = popover;
        
        OBJC_RELEASE(popover);
        OBJC_RELEASE(navController);
        OBJC_RELEASE(_customerListViewController);
    }
    
    
    if([view isKindOfClass:[UIBarButtonItem class]])
    {
        [popover presentPopoverFromBarButtonItem:(UIBarButtonItem *)view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        
        CGRect targetRect = view.frame;
        
        
        [popover presentPopoverFromRect:targetRect inView:view.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
}

-(void)showEmailClientWithToRecipents:(NSArray *)receipents
{
    Class mailclass = (NSClassFromString(@"MFMailComposeViewController"));
    if(mailclass!=nil && [mailclass canSendMail])
    {
        
        // NSString *imageURL     = self.selectedOffer.offerImage? makeImageURLString(self.selectedOffer.offerImage, @"offer", ImageSizeTypeLarge):nil;
        
        /*
         ProgressHUD *progress = [ProgressHUD showHUDAddedTo:self.view animated:YES];
         progress.mode = MBProgressHUDModeIndeterminate;
         progress.tag  = NSIntegerMax-1;
         progress.labelText = @"Attaching...";
         */
        
        
        TreeViewNode *selectedNode = [self associateObjectForKey:@"_selectedResource"];
        ResourceMaster *selectedResource = selectedNode.nodeObject;
        
       // NSString *uploadedByName = selectedResource.createdByName ? selectedResource.createdByName : [selectedNode parentNodeWithLevel:0].title;
        
        NSString *uploadedByName = [selectedNode parentNodeWithLevel:0].title;
        
        NSString *subjectName=[NSString stringWithFormat:@"%@: %@",uploadedByName,selectedResource.resourceName ];
        
        NSString *mailSubject  =  subjectName;
        NSString *mailBody     = [self setupMailBodyForFileResource:selectedResource];
        
        NSLog(@"mailbody = %@", mailBody);
        
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate=self;
        picker.navigationBar.tintColor=[UIColor blackColor];
        [picker setSubject:mailSubject];
        if(mailBody)
        [picker setMessageBody:mailBody isHTML:YES];
        
        ///[picker setToRecipients:receipents];
        
        if([receipents count]>1)
        {
            //Rep *_rep = [CoreDataHandler currentRep];
            NSString *dealersiteName = [CoreDataHandler currentRep].parentWdSite.name;
            
            [picker setToRecipients:[NSArray arrayWithObject:[NSString stringWithFormat:OFFER_TO_EMAILID_FORMAT,dealersiteName]]];
            [picker setBccRecipients:receipents];
        }
        else if([receipents count])
        {
            [picker setToRecipients:receipents];
        }
        
        [self presentViewController:picker animated:YES completion:nil];
        
        OBJC_RELEASE(picker);
        
        //[ProgressHUD hideHUDForView:self.view animated:YES];
    }
    else
    {
        [UIAlertView showWarningAlertWithTitle:@"" message:NO_EMAIL_CLIENT_MESSAGE];
    }
    
    //REMOVE ASSOCIATED OBJECT
    [self setAssociateObject:nil forKey:@"_selectedResource"];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
		  didFinishWithResult:(MFMailComposeResult)result
						error:(NSError*)error
{
	if (result == MFMailComposeResultSent)
    {
		BOOL _isNetworkAvailable = [[OfflineManager sharedManager] isNetworkAvailable];
        
        NSString *success = _isNetworkAvailable? @" Sent Successfully" : @"Email will be sent when you are online";		UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Email Status:"
                                                                                                                                                                      message:success
                                                                                                                                                                     delegate:self
                                                                                                                                                            cancelButtonTitle:nil
                                                                                                                                                            otherButtonTitles:@"Ok",nil];
		[alert1 show];
        OBJC_RELEASE(alert1);
        alert1=nil;
	}
    else if (result == MFMailComposeResultFailed)
    {
		NSString *msg = @"Failed";
		UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Email Status:"
                                                         message:msg
                                                        delegate:self
                                               cancelButtonTitle:nil
                                               otherButtonTitles:@"Ok",nil];
		[alert1 show];
        OBJC_RELEASE(alert1);
		
	}
    
    else if (result == MFMailComposeResultCancelled)
    {
       
	}
	
	[self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}






@end
