//
//  ResourceDetailViewController.h
//  CollectionViewAccordionTest
//
//  Created by Randem IT on 19/11/13.
//  Copyright (c) 2013 Randem IT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TreeViewNode;
@interface ResourcesDetailViewController : UITableViewController

@property (nonatomic,OBJC_STRONG) NSMutableArray *dataSource;
@property (nonatomic,OBJC_STRONG) TreeViewNode *currentNode;

-(void)addResource:(TreeViewNode *)node;
-(void)addResources:(NSArray *)nodes forResource:(TreeViewNode *)node;
-(void)removeResources:(NSArray *)resources;

-(void)removeAllResources;

@end
