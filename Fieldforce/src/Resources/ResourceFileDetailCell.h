//
//  ResourceFileDetailCell.h
//  Fieldforce
//
//  Created by Randem IT on 10/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TreeViewNode;

@interface ResourceFileDetailCell : UITableViewCell

@property (nonatomic,readonly) UIImageView *fileIconView;
@property (nonatomic,readonly) UILabel *fileSizeLabel;
@property (nonatomic,readonly) UILabel *dateLabel;
@property (nonatomic,readonly) UILabel *fileUploaderLabel;
@property (nonatomic,readonly) UILabel *fileNameLabel;
@property (nonatomic,readonly) UILabel *fileModifiedLabel;
@property (nonatomic,readonly) UIButton *emailButton;
@property (nonatomic,readonly) UIButton *viewButton;
@property (nonatomic,retain) TreeViewNode *resourceFileNode;


@end
