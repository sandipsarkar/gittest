//
//  TreeViewNode.m
//  The Projects
//
//  Created by Ahmed Karim on 1/12/13.
//  Copyright (c) 2013 Ahmed Karim. All rights reserved.
//

#import "TreeViewNode.h"

static NSMutableArray *__allNodes=nil;

@interface TreeViewNode()

+(void)addNodeObject:(TreeViewNode *)node;

@end;

@implementation TreeViewNode

#if !__has_feature(objc_arc)
-(void)dealloc
{

    OBJC_RELEASE(_nodeChildren);
    OBJC_RELEASE(_nodeObject);
    OBJC_RELEASE(_modifiedDate);

    
    _parentNode = nil;
    
    /*
    if(__allNodes && ![__allNodes count])
    {
         OBJC_RELEASE(__allNodes);
         __allNodes = nil;
    }
    */
    
    [super dealloc];
}
#endif

-(id)init
{
    if(self= [super init])
    {
        [TreeViewNode addNodeObject:self];
    }
    
    return self;
}

+(void)addNodeObject:(TreeViewNode *)node
{
    if(!__allNodes)
    {
        __allNodes = [[NSMutableArray alloc] init];
    }
    
    if(![__allNodes containsObject:node])
     [__allNodes addObject:node];
    
}

-(TreeViewNode *)topParentNode
{
    TreeViewNode * _topParentNode = self;
    
    while (_topParentNode && [_topParentNode parentNode]!=nil)
    {
        _topParentNode = [_topParentNode parentNode];
    }
    
    return _topParentNode;
}

-(TreeViewNode *)parentNodeWithLevel:(NSUInteger)level
{
    TreeViewNode * _topParentNode = self;
    
    while ((_topParentNode && _topParentNode.nodeLevel!=level) && [_topParentNode parentNode]!=nil)
    {
        _topParentNode = [_topParentNode parentNode];
        
        if(_topParentNode==nil) break;
    }
    
    return _topParentNode;
}

-(NSUInteger)allNodesCount
{
    NSInteger _count = 1+ self.nodeChildren.count;
    
    for(TreeViewNode *__node in self.nodeChildren)
    {
        
        _count +=[__node.nodeChildren count];
    }
    
    return _count;
}

/*
-(NSMutableArray *)allNodes
{
    NSMutableArray *allNodes = [NSMutableArray arrayWithObject:self];
    
   
    for(TreeViewNode *node in self.nodeChildren)
    {
        [allNodes addObjectsFromArray:[node allNodes]];
    }
    
    return allNodes;
}
*/

+(NSMutableArray *)allNodes
{
    if(!__allNodes)
    {
        __allNodes = [[NSMutableArray alloc] init];
    }
    
    return __allNodes;
}


-(void)setParentNode:(TreeViewNode *)parentNode
{
    if(_parentNode!=parentNode)
    {
        _parentNode = parentNode;
       // [parentNode addChildNode:self];
    }
}


-(void)addChildNode:(TreeViewNode *)node
{
    if(!node)
    {
        
        return;
    }
    
    if(!_nodeChildren) _nodeChildren = [[NSMutableArray alloc] init];
    
    
    
    if(![_nodeChildren containsObject:node])
    {
      [_nodeChildren addObject:node];
       node.parentNode = self;
        
      [TreeViewNode addNodeObject:node];
    }
    
}

-(void)removeFromParentNode
{
    if(self.parentNode)
    {
        
        [self.parentNode removeChildNode:self];
    }
}

-(void)removeChildNode:(TreeViewNode *)node
{
    if(!node) return;
  
    
    if([_nodeChildren containsObject:node])
    {
         node.parentNode = nil;
        [_nodeChildren removeObject:node];
    }
}


-(NSArray *)nodeChildrenWithDirectoryType:(BOOL)isDirectoryType
{
   return  [_nodeChildren filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.isDirectory=%@",[NSNumber numberWithBool:isDirectoryType]]];
    }

+(void)removeAllNodes
{
    [__allNodes removeAllObjects];
}


+(NSMutableArray *)deleteNode:(TreeViewNode *)nodeToDelete
{
    if(!nodeToDelete) return nil;
    
    NSMutableArray *nodesToDelete = [NSMutableArray array];
    
    [nodesToDelete addObject:nodeToDelete];
    
    if(nodeToDelete.parentNode)
    {
        BOOL shouldRemove = NO;
        
        TreeViewNode *parentNode = nodeToDelete.parentNode;
        
        [nodeToDelete removeFromParentNode];
        
        if(parentNode.nodeLevel==2)
        {
            if(![parentNode.nodeChildren count])
            {
                shouldRemove = YES;
            }
        }
        else if(parentNode.nodeLevel==1 || parentNode.nodeLevel==0)
        {
            if(![parentNode.nodeChildren count])
            {
                shouldRemove = YES;
            }
        }
        
        if(!shouldRemove && ( [parentNode isKindOfClass:[ResourceFolder class]] && ![[parentNode.nodeObject valueForKey:@"resources"] count] )) //&& ![parentNode.nodeChildren count]
        {
            shouldRemove = YES;
        }
        
        
        if(shouldRemove && parentNode)
        {
            //TreeViewNode *_parentNode  = parentNode.parentNode;
            
            [nodesToDelete addObjectsFromArray:[self deleteNode:parentNode]];
    
        }
    }
    
    return nodesToDelete;
}

#pragma mark - UNDER TESTING
-(NSInteger)currentIndexInDataSource:(NSArray *)dataSource
{
    NSInteger index = [dataSource indexOfObject:self];
    
    if(!self.isExpanded) return index;
    
        NSEnumerator *enumerator =[self.nodeChildren reverseObjectEnumerator];
        
        for(TreeViewNode *_childNode in enumerator)
        {
            if(_childNode.isDirectory)
            {
                index = [_childNode currentIndexInDataSource:dataSource];
                
                break;
            }
        }
    
    
    return index;
}

@end
