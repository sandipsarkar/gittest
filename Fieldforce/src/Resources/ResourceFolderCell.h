//
//  ResourceFolderCell.h
//  Fieldforce
//
//  Created by Randem IT on 09/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResourceFolderCell : UITableViewCell
{
    
}

@property (nonatomic,readonly) UILabel *folderNameLabel;

-(void)isExapnded:(BOOL)isExpanded;

@end
