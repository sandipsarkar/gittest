//
//  ResourcesFolderListViewController.h
//  Fieldforce
//
//  Created by Randem IT on 09/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>


@class TreeViewNode;
@protocol ResourcesFolderDelegate;

@interface ResourcesFolderListViewController : UITableViewController

@property (nonatomic,OBJC_STRONG) NSMutableArray *dataSource;

@property (nonatomic,OBJC_WEAK) id<ResourcesFolderDelegate> delegate;

-(void)reset;

@end

@protocol ResourcesFolderDelegate <NSObject>

@optional
-(void)resourceAccordion:(ResourcesFolderListViewController *)controller didSelectNode:(TreeViewNode *)node expandedNodes:(NSArray *)nodes;
-(void)resourceAccordion:(ResourcesFolderListViewController *)controller didDeselectNode:(TreeViewNode *)node collapsedNodes:(NSArray *)nodes;

@end

