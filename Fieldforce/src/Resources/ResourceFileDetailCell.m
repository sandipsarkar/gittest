//
//  ResourceFileDetailCell.m
//  Fieldforce
//
//  Created by Randem IT on 10/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "ResourceFileDetailCell.h"
#import "TreeViewNode.h"
#import  "ResourceMaster.h"

//11th November 2013
#define RESOURCE_DATE_FORMAT @"F MMMM YYYY"

@interface ResourceFileDetailCell()
{
   
}
@end

@implementation ResourceFileDetailCell

+(NSString *)imageNameForFileExtension:(NSString*)fileExtension
{
    if(!fileExtension) return nil;
    
    NSString *_fileExtention = fileExtension;
    if([fileExtension  rangeOfString:@"."].location!=NSNotFound)
    {
        _fileExtention = [fileExtension stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
    }
    
    NSString *imageName = nil;
    NSString *lowerCaseFileExtension = [_fileExtention lowercaseString];
    
    if([lowerCaseFileExtension isEqualToString:@"pdf"])
    {
        imageName = @"pdf_icon.png";
    }
    else if([lowerCaseFileExtension isEqualToString:@"ppt"])
    {
        imageName = @"ppt_icon.png";
    }
    else if([lowerCaseFileExtension isEqualToString:@"png"])
    {
        imageName = @"png_icon.png";
    }
    else if([lowerCaseFileExtension isEqualToString:@"jpg"] || [lowerCaseFileExtension isEqualToString:@"jpeg"])
    {
        imageName = @"jpg_icon.png";
    }
    else if([lowerCaseFileExtension isEqualToString:@"doc"] || [lowerCaseFileExtension isEqualToString:@"docx"] || [lowerCaseFileExtension isEqualToString:@"word"])
    {
        imageName = @"word_icon.png";
    }
    else if([lowerCaseFileExtension isEqualToString:@"xls"] || [lowerCaseFileExtension isEqualToString:@"xlsx"] || [lowerCaseFileExtension isEqualToString:@"excel"])
    {
        imageName = @"excel_icon.png";
    }
    
    return imageName;
}

+(NSString *)formattedDateStringForWCFTicks:(NSNumber *)dateticks
{
    NSString *str   = [NSDate dateStringFromWCFTimeInterval:dateticks.doubleValue withFormat:@"d"];
    NSString *str2  = [NSDate dateStringFromWCFTimeInterval:dateticks.doubleValue withFormat:@"MMMM YYYY"];
    
    int day = [str intValue];
    
    if(!day) return nil;
    
    NSString *dayStr = nil;
    switch (day)
    {
        case 1:
        case 21:
        case 31:
            dayStr=@"st";
            break;
            
        case 2:
        case 22:
            dayStr=@"nd";
            break;
            
        case 3:
        case 23:
            dayStr=@"rd";
            break;
            
        default:
            dayStr = @"th";
            break;
    }
    
    NSString *dateString = [NSString stringWithFormat:@"%@%@ %@",str,dayStr,str2];
    
    return dateString;
}

-(void)dealloc
{
    
    OBJC_RELEASE(_fileIconView);
    OBJC_RELEASE(_fileSizeLabel);
    OBJC_RELEASE(_dateLabel);
    OBJC_RELEASE(_fileModifiedLabel);
    OBJC_RELEASE(_fileUploaderLabel);
    OBJC_RELEASE(_fileNameLabel);
    OBJC_RELEASE(_resourceFileNode);
    
    [super dealloc];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        _fileIconView = [[UIImageView alloc] init];
        _fileIconView.contentMode = UIViewContentModeScaleAspectFit;
        _fileIconView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_fileIconView];
        
        
        _fileSizeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _fileSizeLabel.font = [UIFont grotesqueFontOfSize:10.0];
        _fileSizeLabel.textAlignment = NSTextAlignmentLeft;
        _fileSizeLabel.textColor = [UIColor colorWithRed:99.0/255.0 green:100.0/255.0 blue:102.0/255.0 alpha:1.0];
        _fileSizeLabel.numberOfLines = 1;
        _fileSizeLabel.backgroundColor = [UIColor clearColor];
        _fileSizeLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self.contentView addSubview:_fileSizeLabel];
        
        
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _dateLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
        _dateLabel.textAlignment = NSTextAlignmentLeft;
        _dateLabel.textColor = LOGIN_BG_COLOR;
        _dateLabel.numberOfLines = 1;
        _dateLabel.backgroundColor = [UIColor clearColor];
        _dateLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self.contentView addSubview:_dateLabel];
        
        
        _fileModifiedLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _fileModifiedLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
        _fileModifiedLabel.textAlignment = NSTextAlignmentCenter;
        _fileModifiedLabel.textColor = [UIColor blackColor];;
        _fileModifiedLabel.numberOfLines = 1;
        _fileModifiedLabel.backgroundColor = [UIColor clearColor];
        _fileModifiedLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self.contentView addSubview:_fileModifiedLabel];
        
        
        _fileUploaderLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _fileUploaderLabel.font = [UIFont grotesqueFontOfSize:13.0];
        _fileUploaderLabel.textAlignment = NSTextAlignmentLeft;
        _fileUploaderLabel.textColor = [UIColor blackColor];
        _fileUploaderLabel.numberOfLines = 1;
        _fileUploaderLabel.backgroundColor = [UIColor clearColor];
        _fileUploaderLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self.contentView addSubview:_fileUploaderLabel];
        
        
        
        _fileNameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _fileNameLabel.font = [UIFont grotesqueFontOfSize:13.0];
        _fileNameLabel.textAlignment = NSTextAlignmentLeft;
        _fileNameLabel.textColor = [UIColor colorWithRed:99.0/255.0 green:100.0/255.0 blue:102.0/255.0 alpha:1.0];
        _fileNameLabel.numberOfLines = 3;
        //_fileNameLabel.lineBreakMode = NSLineBreakByCharWrapping;
        _fileNameLabel.backgroundColor = [UIColor clearColor];
        _fileNameLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self.contentView addSubview:_fileNameLabel];
        
        
        UIImage *_viewIconImage = [UIImage imageNamed:@"view_button_resource.png"];
        _viewButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _viewButton.frame = CGRectMake(0, 0, _viewIconImage.size.width, _viewIconImage.size.height);
        [_viewButton setBackgroundImage:_viewIconImage forState:UIControlStateNormal];
        _viewButton.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_viewButton];
        
        
         UIImage *_emailIconImage = [UIImage imageNamed:@"email_button_resource.png"];
        _emailButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _emailButton.frame = CGRectMake(0, 0, _emailIconImage.size.width, _emailIconImage.size.height);
        [_emailButton setBackgroundImage:_emailIconImage forState:UIControlStateNormal];
        _emailButton.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_emailButton];
        
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.bounds)-1.0, CGRectGetWidth(self.bounds), 1.0)];
        lineView.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];
        lineView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
        [self.contentView addSubview:lineView];
       
        
        OBJC_RELEASE(lineView);

        
        self.selectionStyle = UITableViewCellSelectionStyleNone;

    }
    return self;
}



-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect _bounds = self.contentView.bounds;
    
    CGRect _viewIconViewFrame = CGRectMake(CGRectGetWidth(_bounds)-_viewButton.bounds.size.width-10.0, 5.0, _viewButton.bounds.size.width, _viewButton.bounds.size.height);
   if(!CGRectEqualToRect(_viewButton.frame, _viewIconViewFrame))
    _viewButton.frame =_viewIconViewFrame;
    
    CGRect _emailIconViewFrame = CGRectMake(CGRectGetWidth(_bounds)-_emailButton.bounds.size.width-10.0,CGRectGetHeight(_bounds)-_emailButton.bounds.size.height-5.0, _emailButton.bounds.size.width, _emailButton.bounds.size.height);
    
    if(!CGRectEqualToRect(_emailButton.frame, _emailIconViewFrame))
    _emailButton.frame =_emailIconViewFrame;

    
    CGRect _fileIconViewRect = CGRectMake(15.0, 20.0, 46.0, 46.0);
    
   if(!CGRectEqualToRect(_fileIconView.frame, _fileIconViewRect))
    _fileIconView.frame = _fileIconViewRect;
    
    CGRect _fileSizeLabelRect = CGRectMake(CGRectGetMinX(_fileIconViewRect), CGRectGetMaxY(_fileIconViewRect)+5.0, 70.0, 15.0);
    
     if(!CGRectEqualToRect(_fileSizeLabel.frame, _fileSizeLabelRect))
    _fileSizeLabel.frame = _fileSizeLabelRect;
    
    //CGRect _dateLabelFrame = CGRectMake(CGRectGetMaxX(_fileIconViewRect)+20.0, CGRectGetMinY(_fileIconViewRect), CGRectGetMinX(_viewIconViewFrame)-CGRectGetMaxX(_fileIconViewRect)-24.0, 16.0);
    
    CGFloat _width = CGRectGetMinX(_viewIconViewFrame)-CGRectGetMaxX(_fileIconViewRect)-22.0;
    
     CGRect _dateLabelFrame = CGRectMake(CGRectGetMaxX(_fileIconViewRect)+20.0, CGRectGetMinY(_fileIconViewRect), 145, 15.0);
    
      if(!CGRectEqualToRect(_dateLabel.frame, _dateLabelFrame))
    _dateLabel.frame =_dateLabelFrame;
    
    
    CGRect _fileModifiedLabelFrame = CGRectMake(CGRectGetMaxX(_dateLabelFrame)+5.0, CGRectGetMinY(_dateLabelFrame), 45, 15.0);
    if(!CGRectEqualToRect(_fileModifiedLabel.frame, _fileModifiedLabelFrame))
        _fileModifiedLabel.frame =_fileModifiedLabelFrame;
    
    CGRect _fileUploaderLabelFrame = CGRectMake(CGRectGetMinX(_dateLabelFrame), CGRectGetMaxY(_dateLabelFrame)+1.0, _width, CGRectGetHeight(_dateLabelFrame));
    
     if(!CGRectEqualToRect(_fileUploaderLabel.frame, _fileUploaderLabelFrame))
    _fileUploaderLabel.frame =_fileUploaderLabelFrame;
    
    
    CGSize _fileNameLabelSize = [_fileNameLabel sizeThatFits:CGSizeMake(_width, CGFLOAT_MAX)];
    
   // CGRect _fileNameLabelFrame = CGRectMake(CGRectGetMinX(_dateLabelFrame), CGRectGetMaxY(_fileUploaderLabelFrame)+1.0, _width, CGRectGetHeight(_dateLabelFrame));
    
     CGRect _fileNameLabelFrame = CGRectMake(CGRectGetMinX(_dateLabelFrame), CGRectGetMaxY(_fileUploaderLabelFrame)+1.0, _width, _fileNameLabelSize.height);
    
    if(!CGRectEqualToRect(_fileNameLabel.frame, _fileNameLabelFrame))
    _fileNameLabel.frame =_fileNameLabelFrame;
    
   
   }

-(void)setResourceFileNode:(TreeViewNode *)resourceFileNode
{
    OBJC_RELEASE(_resourceFileNode);
    
    _resourceFileNode = OBJC_RETAIN(resourceFileNode);

    /*
    NSString *_imageName = [ResourceFileDetailCell imageNameForFileExtension:@"png"];
    
    _fileIconView.image = [UIImage imageNamed:_imageName];
    
    _fileSizeLabel.text = [NSString stringWithFormat:@"Size:%@",@"1.2 mb"];
    
    _dateLabel.text = @"11th November 2013";
    
    _fileUploaderLabel.text = [NSString stringWithFormat:@"By: %@",@"Subaru"];
    
    _fileNameLabel.text = _resourceFileNode.title;// @"Reference Guide Image";
    */
    
    ResourceMaster *_resourceMaster = (ResourceMaster *)resourceFileNode.nodeObject;
    
    NSString *_fileExt = _resourceMaster.fileExtension ? _resourceMaster.fileExtension : @"png";
    NSString *_imageName = _fileExt.length ?   [ResourceFileDetailCell imageNameForFileExtension:_fileExt] : nil;
    
    _fileIconView.image = [UIImage imageNamed:_imageName];
    
    NSString *_fileSizeString = [_resourceMaster fileSizeString];
    _fileSizeLabel.text = [NSString stringWithFormat:@"Size: %@",_fileSizeString?_fileSizeString:@""];
    
    //NSString *_fileDate = _resourceMaster.createdDate
    
    
    NSString *dateString = [ResourceFileDetailCell formattedDateStringForWCFTicks:_resourceMaster.createdDate];
    
    //NSString *str   = [NSDate dateStringFromWCFTimeInterval:_resourceMaster.createdDate.doubleValue withFormat:@"dd-MM-YYY hh:mm:ss"];
    

    _dateLabel.text = dateString;
    
    NSString *name = _resourceMaster.createdByName;
    NSString *uploadedByName = name.length ? name : [_resourceFileNode parentNodeWithLevel:0].title;
    _fileUploaderLabel.text = [NSString stringWithFormat:@"By: %@",uploadedByName];
    
    _fileNameLabel.text = _resourceFileNode.title?_resourceFileNode.title:@"";
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
