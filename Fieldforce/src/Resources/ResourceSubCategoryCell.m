//
//  ResourceSubCategoryCell.m
//  Fieldforce
//
//  Created by Randem IT on 10/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "ResourceSubCategoryCell.h"

@interface ResourceSubCategoryCell()
{
    UIView *_bulletView;
    UILabel *_subCategoryNameLabel;
    UIView *bgView;
}
@end


@implementation ResourceSubCategoryCell


-(void)dealloc
{
    OBJC_RELEASE(_subCategoryNameLabel);
    OBJC_RELEASE(_bulletView);
    OBJC_RELEASE(bgView);
    
    [super dealloc];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        _bulletView = [[UIView alloc] initWithFrame:CGRectMake(30.0, 20.0, 5.0, 5.0)];
        _bulletView.backgroundColor = [UIColor lightGrayColor];
        [self.contentView addSubview:_bulletView];
        
        _subCategoryNameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _subCategoryNameLabel.font = [UIFont subaruBookFontOfSize:19.0];
        _subCategoryNameLabel.textAlignment = NSTextAlignmentLeft;
        _subCategoryNameLabel.textColor = [UIColor whiteColor];
        _subCategoryNameLabel.numberOfLines = 2;
        _subCategoryNameLabel.backgroundColor = [UIColor clearColor];
        _subCategoryNameLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
        [self.contentView addSubview:_subCategoryNameLabel];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.bounds)-1.0, CGRectGetWidth(self.bounds), 1.0)];
        lineView.backgroundColor = [UIColor colorWithRed:180.0/255.0 green:180.0/255.0 blue:180.0/255.0 alpha:1.0];
        lineView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
        [self.contentView addSubview:lineView];
        [lineView release];
        
        
        self.backgroundColor =  [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
        
        bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.backgroundColor = self.backgroundColor;
        self.backgroundView = bgView;

        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect _bounds = self.contentView.bounds;
    
   
    //--------------------
    CGFloat _expectedWidth = CGRectGetWidth(_bounds) - 45.0;
    CGSize _size = [_subCategoryNameLabel.text sizeWithFont:_subCategoryNameLabel.font constrainedToSize:CGSizeMake(_expectedWidth, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    
    CGRect _bulletViewFrame = CGRectZero;
    
    int _labelSize = ceilf(_size.height);
    if(_labelSize/19<=1)
    {
        _bulletViewFrame = CGRectMake(30.0, (CGRectGetHeight(_bounds)-5.0)/2.0, 5.0, 5.0);
    }
    else
    {
        _bulletViewFrame = CGRectMake(30.0, 20.0, 5.0, 5.0);
    }

    //------------
    
    
   // CGRect _bulletViewFrame = CGRectMake(30.0, (CGRectGetHeight(_bounds)-5.0)/2.0, 5.0, 5.0);
    

    if(!CGRectEqualToRect(_bulletView.frame, _bulletViewFrame))
    _bulletView.frame = _bulletViewFrame;
    
    CGRect _subCategoryNameLabelBounds = CGRectMake(CGRectGetMaxX(_bulletViewFrame)+5.0, 10.0 , CGRectGetWidth(_bounds)-CGRectGetMaxX(_bulletViewFrame)-10.0,CGRectGetHeight(_bounds)-2*10.0);
    
    if(!CGRectEqualToRect(_subCategoryNameLabel.frame, _subCategoryNameLabelBounds))
    _subCategoryNameLabel.frame = _subCategoryNameLabelBounds;
}

-(void)isExapnded:(BOOL)isExpanded
{
    UIColor *selectedColor = [UIColor colorWithRed:165.0/255.0 green:165.0/255.0 blue:165.0/255.0 alpha:1.0];
    UIColor *normalColor   = [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
    self.backgroundColor = isExpanded ? selectedColor : normalColor;
    
    bgView.backgroundColor = self.backgroundColor;
    
    UIColor *seletedTextColor = [UIColor colorWithRed:87.0/255.0 green:87.0/255.0 blue:87.0/255.0 alpha:1.0];

    _bulletView.backgroundColor     =  isExpanded ? [UIColor whiteColor] :  selectedColor;
    _subCategoryNameLabel.textColor =  isExpanded ? [UIColor whiteColor] :  seletedTextColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    /*
      self.backgroundColor = selected ? [[UIColor blackColor] colorWithAlphaComponent:0.20] : [[UIColor blackColor] colorWithAlphaComponent:0.05];
      _bulletView.backgroundColor = selected ? [UIColor whiteColor] : [UIColor grayColor];
      _subCategoryNameLabel.textColor = selected ? [UIColor whiteColor] :[UIColor grayColor];
    */

 
}

@end
