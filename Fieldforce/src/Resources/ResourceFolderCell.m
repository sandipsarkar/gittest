//
//  ResourceFolderCell.m
//  Fieldforce
//
//  Created by Randem IT on 09/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "ResourceFolderCell.h"

#define FOLDER_ICON_OPEN   @"open_folder_icon.png"
#define FOLDER_ICON_CLOSE  @"close_folder_icon.png"

#define BG_IMAGE_NORMAL    @"oem_bg_normal.png"
#define BG_IMAGE_SELECTED  @"oem_bg_selected.png"

@interface ResourceFolderCell ()
{
    UIImageView *_iconImageView;
    UIImageView *bgView;
}

@end

@implementation ResourceFolderCell

-(void)dealloc
{
    OBJC_RELEASE(_iconImageView);
    OBJC_RELEASE(_folderNameLabel);
    OBJC_RELEASE(bgView);
    
    [super dealloc];
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        _iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:FOLDER_ICON_CLOSE]];
        _iconImageView.contentMode = UIViewContentModeScaleAspectFit;
        _iconImageView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_iconImageView];
        

        _folderNameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _folderNameLabel.font = [UIFont subaruBoldFontOfSize:35.0]; //35.0
        _folderNameLabel.textAlignment = NSTextAlignmentLeft;
        _folderNameLabel.numberOfLines = 2;
        _folderNameLabel.backgroundColor = [UIColor clearColor];
        _folderNameLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
        _folderNameLabel.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
        _folderNameLabel.shadowOffset = CGSizeMake(1.0, 2.0);
        [self.contentView addSubview:_folderNameLabel];
        
        
        bgView = [[UIImageView alloc] initWithFrame:CGRectZero];
        bgView.image = [UIImage imageNamed:BG_IMAGE_NORMAL];
        bgView.backgroundColor = [UIColor clearColor];
        self.backgroundView = bgView;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;

    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect _bounds = self.contentView.bounds;
    
    CGRect _iconImageViewBounds = CGRectMake(15.0, (CGRectGetHeight(_bounds)-46)/2.0, 53.0, 46);
    
    if(!CGRectEqualToRect(_iconImageView.frame, _iconImageViewBounds))
    _iconImageView.frame = _iconImageViewBounds;
    
    CGRect _folderNameLabelBounds = CGRectMake(CGRectGetMaxX(_iconImageViewBounds)+20.0, 10.0 , CGRectGetWidth(_bounds)-CGRectGetMaxX(_iconImageViewBounds)-20.0*2,CGRectGetHeight(_bounds)-2*10.0);
    
   //  CGRect _folderNameLabelBounds = CGRectMake(CGRectGetMaxX(_iconImageViewBounds)+20.0, 0.0 , CGRectGetWidth(_bounds)-CGRectGetMaxX(_iconImageViewBounds)-20.0*2,CGRectGetHeight(_bounds)-2*0.0);
    
    if(!CGRectEqualToRect(_folderNameLabel.frame, _folderNameLabelBounds))
    _folderNameLabel.frame = _folderNameLabelBounds;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    /*
    _iconImageView.image = [UIImage imageNamed: selected ? FOLDER_ICON_OPEN : FOLDER_ICON_CLOSE];
    bgView.image = [UIImage imageNamed: selected ? BG_IMAGE_SELECTED : BG_IMAGE_NORMAL];
     */
}

-(void)isExapnded:(BOOL)isExpanded
{
    _iconImageView.image = [UIImage imageNamed: isExpanded ? FOLDER_ICON_OPEN : FOLDER_ICON_CLOSE];
    bgView.image = [UIImage imageNamed: isExpanded ? BG_IMAGE_SELECTED : BG_IMAGE_NORMAL];

}

@end
