//
//  DocPreviewController.h
//  DocInteraction
//
//  Created by Randem IT on 18/11/13.
//
//

#import <QuickLook/QuickLook.h>
@class TreeViewNode;

@interface DocPreviewController : UIViewController

@property (nonatomic,assign) NSInteger selectedIndex;
@property (nonatomic,OBJC_STRONG) TreeViewNode *fileNode;
@end

