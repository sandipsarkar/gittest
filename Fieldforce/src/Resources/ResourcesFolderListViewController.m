//
//  ResourcesFolderListViewController.m
//  Fieldforce
//
//  Created by Randem IT on 09/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "ResourcesFolderListViewController.h"
#import "TreeViewNode.h"
#import "ResourceFolderCell.h"
#import "ResourceCategoryCell.h"
#import "ResourceSubCategoryCell.h"

static NSString *CellIdentifier  = @"Cell";
static NSString *CellIdentifier2 = @"Cell2";
static NSString *CellIdentifier3 = @"Cell3";

@interface ResourcesFolderListViewController ()

@property (nonatomic,OBJC_STRONG) TreeViewNode *lastExpandedNode;
@end

@implementation ResourcesFolderListViewController


-(void)dealloc
{
     [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    #if !__has_feature(objc_arc)
    
    OBJC_RELEASE(_dataSource);
    OBJC_RELEASE(_lastExpandedNode);

    _delegate = nil;
    [super dealloc];
    #endif
}


- (id)init
{
    self = [super initWithStyle:UITableViewStylePlain];
    
    if (self)
    {
        // Custom initialization
    }
    return self;
}


-(void)setDataSource:(NSMutableArray *)dataSource
{
    //if(_dataSource!=dataSource || [_dataSource count] !=[dataSource count])
    {
        OBJC_RELEASE(_dataSource);
        _dataSource = OBJC_RETAIN(dataSource);
        
        [self.tableView reloadData];
    }
}



-(void)reloadDataSource
{
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

   // self.tableView.separatorColor = [UIColor whiteColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    self.tableView.separatorInset = UIEdgeInsetsMake(0, -10, 0, 0);
    
    [self.tableView registerClass:[ResourceFolderCell class] forCellReuseIdentifier:CellIdentifier];
    [self.tableView registerClass:[ResourceCategoryCell class] forCellReuseIdentifier:CellIdentifier2];
    [self.tableView registerClass:[ResourceSubCategoryCell class] forCellReuseIdentifier:CellIdentifier3];
    
    [self addDefaultBorder];
    
    self.tableView.bounces = NO;
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)_heightForTableView:(UITableView *)tableView node:(TreeViewNode *)node
{
    if(node.nodeLevel==0) return 116.0;
    
    NSString *nodeTitle = node.title;
    
    UIFont *_expectedFont = nil;
    CGFloat _expectedWidth = 0.0;
    
    switch (node.nodeLevel)
    {
        case 1:{
            
            _expectedFont = [UIFont subaruMediumFontOfSize:22.0];
            _expectedWidth =  CGRectGetWidth(tableView.bounds)-18-48;
        }
            
        break;
            
        case 2:
        {
            
            _expectedFont =  [UIFont subaruBookFontOfSize:19.0];
            _expectedWidth =  CGRectGetWidth(tableView.bounds)-45; //3.6
        }
        break;
            
        default:
            break;
    }
   
    
    CGSize _size = [nodeTitle sizeWithFont:_expectedFont constrainedToSize:CGSizeMake(_expectedWidth, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    
    CGFloat _height = MAX(_size.height+20.0, 45.0);
    
    return _height;

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [self.dataSource count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TreeViewNode *node = (indexPath.row<[self.dataSource count]) ? [self.dataSource objectAtIndex:indexPath.row] : nil;
    
    //--------------------------
   // CGFloat _height = (node.nodeLevel==0) ? 116.0 : [self _heightForTableView:tableView node:node];
    //---------------------------
    
   // CGFloat _height = (node.nodeLevel==0) ? 116.0 : 45.0;
    
     CGFloat _height = (node.nodeLevel==0) ? 116.0 : 64.0;
    
    // CGFloat _height = (node.nodeLevel==0) ? 110.0 : 64.0;
    
    return _height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;

    TreeViewNode *node = (indexPath.row<[self.dataSource count]) ? [self.dataSource objectAtIndex:indexPath.row] : nil;
    
    switch (node.nodeLevel)
    {
        case 0:{
            
            ResourceFolderCell *  _cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            //_cell.folderNameLabel.text = indexPath.row ==0 ? @"Subaru Australia Site"  :   node.title;
            _cell.folderNameLabel.text =   node.title;
            [_cell isExapnded:node.isExpanded];
            
            cell = _cell;

        }
        break;
            
        case 1:{
            
            ResourceCategoryCell  *_cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2 forIndexPath:indexPath];
            
            _cell.categoryNameLabel.text = node.title;
            [_cell isExapnded:node.isExpanded];
            
            cell = _cell;
        }
        break;
            
        case 2:
        default:{
            
            ResourceSubCategoryCell  *_cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier3 forIndexPath:indexPath];
            
            _cell.subCategoryNameLabel.text = node.title;
            [_cell isExapnded:node.isExpanded];
            
            cell = _cell;
        }
        break;
    }
    
    
    return cell;
}

-(void)expandBranchAtIndex:(int)index
{
    if(index ==NSNotFound) return;
    
    NSMutableArray *indexAPathsToInsert = [NSMutableArray array];
    
    TreeViewNode *node = index < [self.dataSource count] ? [self.dataSource objectAtIndex:index] : nil;
    int _index = index+1;
    
    //ONLY CALCULATE DIRECTORY TYPE NODES
    NSArray *childrenToExpand =[node nodeChildrenWithDirectoryType:YES]; //     node.nodeChildren;
    for(TreeViewNode *childNode in childrenToExpand)
    {
        //if(![self.dataSource containsObject:childNode])
        [self.dataSource insertObject:childNode atIndex:_index];
        
        // [self.dataSource insertObject:childNode atIndex:_index];
        
        NSIndexPath *_indexPath = [NSIndexPath indexPathForRow:_index inSection:0];
        [indexAPathsToInsert addObject:_indexPath];
        
        _index++;
    }
    
    node.isExpanded = YES;
    
    if([indexAPathsToInsert count])
    {
        [self.tableView beginUpdates];
        
        [self.tableView insertRowsAtIndexPaths:indexAPathsToInsert withRowAnimation:UITableViewRowAnimationNone];
        
        [self.tableView endUpdates];
    }
    
      NSIndexPath *_indexPathToReload = [NSIndexPath indexPathForRow:index inSection:0];
     [self reloadCellAtIndexPath:_indexPathToReload];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(resourceAccordion: didSelectNode: expandedNodes:)])
    {
        [self.delegate resourceAccordion:self didSelectNode:node expandedNodes:childrenToExpand];
    }
    
    
       //------------------ SCROLL TO VISIBLE AREA -----------------------------------------
    
    NSIndexPath *_lastIndexPath = [NSIndexPath indexPathForRow:_index-1 inSection:0];
    
    TreeViewNode *topNode = (node.nodeLevel>0) ? [node parentNodeWithLevel:0] : node;
    TreeViewNode *lastNode = [topNode.nodeChildren lastObject];
    
    if(lastNode.isExpanded)
    {
        lastNode = [lastNode.nodeChildren lastObject];
    }
    
    NSInteger _nextTopNodeIndex = [self.dataSource indexOfObject:lastNode];
    
    if(_nextTopNodeIndex!=NSNotFound)
    _lastIndexPath = [NSIndexPath indexPathForRow:_nextTopNodeIndex inSection:0];
    
    UITableViewCell *_cell = [self.tableView cellForRowAtIndexPath:_lastIndexPath];
    
    // if(![self.tableView.indexPathsForVisibleRows containsObject:_lastIndexPath])
    if(!_cell ||(_cell && !CGRectContainsRect(self.tableView.bounds, _cell.frame)))
    {
        [self.tableView scrollToRowAtIndexPath:_lastIndexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
    }
}

-(void)collapseBranchAtIndex:(int)index
{
    TreeViewNode *node = index <[self.dataSource count] ? [self.dataSource  objectAtIndex:index] : nil;
    
    if(!node || !node.isExpanded) return;
    
    //ONLY CALCULATE DIRECTORY TYPE NODES
    NSArray *branchToCollapse = [node nodeChildrenWithDirectoryType:YES]; //  node.nodeChildren;
    
    for (NSUInteger i = 0; i < [branchToCollapse count]; i++)
    {
        TreeViewNode *_node  = [branchToCollapse objectAtIndex:i];
        if (_node.isExpanded)
        {
            [self collapseBranchAtIndex:index + i + 1]; //+1 is super important!!
        }
    }
    
    NSRange _deleteRange  = NSMakeRange(index+1, [branchToCollapse count]);
    [self.dataSource removeObjectsInRange:_deleteRange];
    
    node.isExpanded = NO;
    
    NSMutableArray *indexPathsToDelete = [NSMutableArray array];
    
    /*
    for(NSUInteger i=0;i<[branchToCollapse count];i++)
    {
        NSUInteger _index = index+1+i;
        NSIndexPath *_indexPath = [NSIndexPath indexPathForRow:_index inSection:0];
        [indexPathsToDelete addObject:_indexPath];
    }
    */
    
    for(NSInteger i=[branchToCollapse count]-1; i>=0; i--)
    {
        NSUInteger _index = index+1+i;
        NSIndexPath *_indexPath = [NSIndexPath indexPathForRow:_index inSection:0];
        [indexPathsToDelete addObject:_indexPath];
    }
    
    if([indexPathsToDelete count])
    {
        [self.tableView beginUpdates];
        
        [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationFade];
        
        [self.tableView endUpdates];
    }
    
    NSIndexPath *_indexPathToReload = [NSIndexPath indexPathForRow:index inSection:0];
    [self reloadCellAtIndexPath:_indexPathToReload];
    
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(resourceAccordion: didDeselectNode: collapsedNodes:)])
    {
        [self.delegate resourceAccordion:self didDeselectNode:node collapsedNodes:branchToCollapse];
    }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TreeViewNode *node = (indexPath.row<[self.dataSource count]) ? [self.dataSource objectAtIndex:indexPath.row] : nil;
    
    if(!node.isDirectory)
    {
        
        
    }
    else
    {
        
        NSLog(@"Expand node level=%d Current node level=%d",self.lastExpandedNode.nodeLevel,node.nodeLevel);
        
        BOOL _isSameNode = self.lastExpandedNode && ([self.lastExpandedNode isEqual:node] && self.lastExpandedNode.isExpanded);
        
       //  BOOL _isSameNode = self.lastExpandedNode && ([self.lastExpandedNode isEqual:node]);
        
        if(self.lastExpandedNode && self.lastExpandedNode.isExpanded)
       // if(self.lastExpandedNode)
        {
            NSUInteger index=NSNotFound;
            if(self.lastExpandedNode.nodeLevel == node.nodeLevel)
            {
                index = [self.dataSource indexOfObject:self.lastExpandedNode];
                
                _isSameNode = [self.lastExpandedNode isEqual:node];
                
                self.lastExpandedNode = self.lastExpandedNode.parentNode ? self.lastExpandedNode.parentNode : nil;
                
            }
            else if(self.lastExpandedNode.nodeLevel > node.nodeLevel)
            {
                
                TreeViewNode *_parentNode = [self.lastExpandedNode parentNodeWithLevel:node.nodeLevel];
                if(_parentNode)
                {
                    index = [self.dataSource indexOfObject:_parentNode];
                    _isSameNode = [_parentNode isEqual:node];
                    
                    if(_isSameNode && node.parentNode)
                    {
                        _parentNode = node.parentNode;
                    }
                    
                    self.lastExpandedNode = self.lastExpandedNode.isExpanded ? _parentNode : nil;
                }
            }
            
            if(index!=NSNotFound)
            {
                [self collapseBranchAtIndex:index];
                //self.lastExpandedNode = nil;
            }
        }
        
        if(!_isSameNode)
        {
            if(node.isExpanded) //Collapse
            {
                NSUInteger _index =  [self.dataSource indexOfObject:node];
                [self collapseBranchAtIndex:_index];
            }
            else  // Expand
            {
                NSUInteger _index = [self.dataSource indexOfObject:node];
                [self expandBranchAtIndex:_index];
                self.lastExpandedNode = node;
                
                
            }
        }
               
    }
}

-(void)reset
{
    /*
    TreeViewNode *parentNode = [self.lastExpandedNode parentNodeWithLevel:0];

    if(parentNode)
    {
     int parentNodeIndex =  [self.dataSource indexOfObject:parentNode];

     if(parentNodeIndex!=NSNotFound)
     {
         [self collapseBranchAtIndex:parentNodeIndex];
     }
    }
    */
    
   self.lastExpandedNode = nil;
    
  [self.parallelStackViewController hideDetailController:self animated:NO];

}


-(void)reloadCellAtIndexPath:(NSIndexPath *)indexPath
{
    ResourceFolderCell *cell = (ResourceFolderCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    TreeViewNode *node = indexPath.row <[self.dataSource count] ? [self.dataSource  objectAtIndex:indexPath.row] : nil;
    
    [cell isExapnded:node.isExpanded];
}

@end
