//
//  TreeViewNode.h
//  The Projects
//
//  Created by Ahmed Karim on 1/12/13.
//  Copyright (c) 2013 Ahmed Karim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TreeViewNode : NSObject

@property (nonatomic) NSUInteger nodeLevel;
@property (nonatomic) BOOL isDirectory;
@property (nonatomic) BOOL isExpanded;
@property (nonatomic, readonly) BOOL isEmpty;
@property (nonatomic, OBJC_STRONG) id nodeObject;
@property (nonatomic, OBJC_STRONG) NSMutableArray *nodeChildren;
@property (nonatomic, OBJC_WEAK)   TreeViewNode *parentNode;
@property (nonatomic) NSUInteger isDealersiteNode;

@property (nonatomic, OBJC_STRONG) NSNumber * modifiedDate;

@property (nonatomic, OBJC_STRONG) NSString * title;
@property (nonatomic, OBJC_STRONG) NSString * fileName;
@property (nonatomic, OBJC_STRONG) NSString * identifier;


-(void)addChildNode:(TreeViewNode *)node;
-(void)removeChildNode:(TreeViewNode *)node;
-(void)removeFromParentNode;

-(TreeViewNode *)topParentNode;
-(TreeViewNode *)parentNodeWithLevel:(NSUInteger)level;

-(NSUInteger)allNodesCount;

+(NSMutableArray *)allNodes;
+(void)removeAllNodes;
+(void)addNodeObject:(TreeViewNode *)node;


-(NSArray *)nodeChildrenWithDirectoryType:(BOOL)isDirectoryType;

+(NSMutableArray *)deleteNode:(TreeViewNode *)nodeToDelete;

-(NSInteger)currentIndexInDataSource:(NSArray *)dataSource;

@end
