//
//  ResourcesViewController.h
//  Fieldforce
//
//  Created by Randem IT on 09/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

#define USE_DEMO_DATA 0
@class ResourcesFolderListViewController;
@class ResourcesDetailViewController;

@interface ResourcesViewController : UIViewController

@end
