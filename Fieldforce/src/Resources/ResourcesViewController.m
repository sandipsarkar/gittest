//
//  ResourcesViewController.m
//  Fieldforce
//
//  Created by Randem IT on 09/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "ResourcesViewController.h"
#import "ResourcesFolderListViewController.h"
#import "ResourcesDetailViewController.h"
#import "TreeViewNode.h"

#import "Rep.h"
#import "OEM.h"
#import "ResourceFolder.h"
#import "ResourceMaster.h"

#import "ParallelStackViewController.h"


///Ordering of new added node


@interface ResourcesViewController ()<ResourcesFolderDelegate>
{
    ParallelStackViewController       * _parallelStackViewController;
    ResourcesFolderListViewController * _resourcesFolderListViewController;
    ResourcesDetailViewController     * _resourcesDetailViewController;
    
    UIView *containerView;
    UILabel *noResourcesLabel;
    
    BOOL _isFetchingFromServer;
    
    BOOL _isFetchingLocally;
    
    BOOL _isRefreshRequested;
    
    BOOL _isAnyFileDeleted;
}


@property (nonatomic, OBJC_STRONG) ASIHTTPRequest *resourceRequest;

-(void)reloadResourceDetailForNode:(TreeViewNode *)node;

@end

@implementation ResourcesViewController

#if !__has_feature(objc_arc)
-(void)dealloc
{
    [TreeViewNode removeAllNodes];
    
    [_resourceRequest clearDelegatesAndCancel];
    OBJC_RELEASE(_resourceRequest);
    
    OBJC_RELEASE(containerView);
    OBJC_RELEASE(noResourcesLabel);
 
    OBJC_RELEASE(_parallelStackViewController);
    OBJC_RELEASE(_resourcesFolderListViewController);
    OBJC_RELEASE(_resourcesDetailViewController);
    
   
    
    [super dealloc];
}
#endif

- (id)init
{
    self = [super init];
    
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    #if TRACK_ERROR
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    #endif
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController.navigationBar addBackGroundImage:@"header_blank.png"];
    [self addTitleImage:@"resources.png"];
        [self addRefreshButtonWithSelector:@selector(refreshButtonAction:)];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [self setupView];
    
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds))];
    containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    containerView.backgroundColor = [UIColor whiteColor];
    //[UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
    containerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    containerView.layer.borderWidth = 1.0;
    [self.view addSubview:containerView];
    
    //CGRect _noResourcesFrame = CGRectMake(10.0, (CGRectGetHeight(containerView.bounds)-50.0)/2.0, CGRectGetWidth(containerView.bounds)-20.0, 50.0);
    
    CGRect _noResourcesFrame = CGRectMake(10.0, 198.0, CGRectGetWidth(containerView.bounds)-20.0, 50.0);
    
    noResourcesLabel = [[UILabel alloc] initWithFrame:_noResourcesFrame];
    noResourcesLabel.font = [UIFont subaruMediumFontOfSize:23.0];
    noResourcesLabel.textAlignment = NSTextAlignmentCenter;
    noResourcesLabel.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    noResourcesLabel.textColor= [UIColor colorWithRed:165.0/255.0 green:165.0/255.0 blue:165.0/255.0 alpha:1.0];
    noResourcesLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    noResourcesLabel.backgroundColor = [UIColor clearColor];
    noResourcesLabel.numberOfLines = 2;
    noResourcesLabel.text = @"NO RESOURCES \nAVAILABLE";
    [containerView addSubview:noResourcesLabel];
    //noResourcesLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin| UIViewAutoresizingFlexibleRightMargin  | UIViewAutoresizingFlexibleLeftMargin;
    
    noResourcesLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth |  UIViewAutoresizingFlexibleBottomMargin| UIViewAutoresizingFlexibleRightMargin  | UIViewAutoresizingFlexibleLeftMargin;
    
    noResourcesLabel.hidden =YES;
    
   // containerView.hidden = YES;
    
    [ProgressHUD showHUDAddedTo:self.view animated:YES];

    [self fetchResourcesFromServer];
    [self fetchResources:nil];
                             
}

-(void)hideProgressLoader
{
    [ProgressHUD hideHUDForView:self.view animated:YES];
}



-(void)setupView
{
    _resourcesFolderListViewController = [[ResourcesFolderListViewController alloc] init];
      _resourcesFolderListViewController.delegate = self;
    
    _resourcesDetailViewController = [[ResourcesDetailViewController alloc] init];
    
    _resourcesFolderListViewController.view.frame = CGRectMake(0.0, 0, CGRectGetWidth(self.view.bounds)/2.0,  CGRectGetHeight(self.view.bounds));
  
    
    //==============================================================================================
    /*
    [self.view addSubview:_resourcesFolderListViewController.view];
    [self addChildViewController:_resourcesFolderListViewController];
    _resourcesFolderListViewController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleRightMargin;
    [_resourcesFolderListViewController didMoveToParentViewController:self];
    
    
    _resourcesDetailViewController.view.frame = CGRectMake(CGRectGetMaxX(_resourcesFolderListViewController.view.frame),0.0, CGRectGetWidth(self.view.bounds)/2.0,  CGRectGetHeight(self.view.bounds));
    
    [self.view insertSubview:_resourcesDetailViewController.view atIndex:0];
    [self addChildViewController:_resourcesDetailViewController];
    _resourcesDetailViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleHeight |  UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin;
    [_resourcesDetailViewController didMoveToParentViewController:self];
    */
    //==============================================================================================
    
    _parallelStackViewController = [[ParallelStackViewController alloc] initWithMasterController:_resourcesFolderListViewController];
    
    [_parallelStackViewController setDetailController:_resourcesDetailViewController];
    [self addChildViewController:_parallelStackViewController];
    _parallelStackViewController.view.frame = self.view.bounds;
    [self.view addSubview:_parallelStackViewController.view];
    _parallelStackViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [_parallelStackViewController didMoveToParentViewController:self];
    
    _resourcesDetailViewController.view.hidden =YES;
}


-(NSMutableArray *)deleteNode:(TreeViewNode *)nodeToDelete
{
    if(!nodeToDelete) return nil;
    
     [nodeToDelete retain];
    
    NSMutableArray *nodesToDelete = [[NSMutableArray alloc] init];
    
    [nodesToDelete addObject:nodeToDelete];
    
    if(nodeToDelete.parentNode)
    {
        BOOL shouldRemove = NO;
        
        TreeViewNode *parentNode = nodeToDelete.parentNode;
        
        if([nodeToDelete respondsToSelector:@selector(removeFromParentNode)])
        {
           
            [nodeToDelete removeFromParentNode];
        }
        
        if(parentNode.nodeLevel==2)
        {
            if(![parentNode.nodeChildren count])
            {
                shouldRemove = YES;
            }
        }
        else if(parentNode.nodeLevel==1 || parentNode.nodeLevel==0)
        {
            if(![parentNode.nodeChildren count])
            {
                shouldRemove = YES;
            }
            
        }
        
        if(!shouldRemove && ( [parentNode isKindOfClass:[ResourceFolder class]] && ![[parentNode.nodeObject valueForKey:@"resources"] count] )) //&& ![parentNode.nodeChildren count]
        {
            shouldRemove = YES;
        }
        
        
        if(shouldRemove && parentNode)
        {
            //TreeViewNode *_parentNode  = parentNode.parentNode;
            
           [nodesToDelete addObjectsFromArray:[self deleteNode:parentNode]];
            
        }
    }
    
    [nodeToDelete release];
    
    return  OBJC_AUTORELEASE(nodesToDelete);
}


-(void)deleteNodeForIDs:(NSArray *)IDs onKey:(NSString *)key
{
    NSMutableArray *nodesToDelete = [[NSMutableArray alloc] init];
    
     NSMutableArray *visibleNodes = self->_resourcesFolderListViewController.dataSource;
    
    NSMutableArray *allNodes = [TreeViewNode allNodes];
    
     for(TreeViewNode *nodeToDelete in allNodes)
     {
         BOOL shouldDelete = NO;
         
         NSManagedObject *_objectToDelete  = nodeToDelete.nodeObject;
     
         if([_objectToDelete isKindOfClass:[ResourceFolder class]])
         {
           NSNumber *_folderID = [_objectToDelete valueForKey:@"resourceFolderID"];
         
         if(_folderID && [IDs containsObject:_folderID])
         {
             shouldDelete = YES;
         }
             
         }
         else  if([_objectToDelete isKindOfClass:[ResourceMaster class]])
         {
             NSNumber *_resourceID = [_objectToDelete valueForKey:@"resourceID"];
         
             if(_resourceID && [IDs containsObject:_resourceID])
             {
                 shouldDelete = YES;
             }
         }
         
     
         if(shouldDelete)
         {
             /*
             /// THIS PART SHOULD BE OPTIMIZED
             //-------------------------------------------------------------------
             if(nodeToDelete.parentNode)
             {
                 BOOL shouldRemove = NO;
                 
                 TreeViewNode *parentNode = nodeToDelete.parentNode;
                 
                 [nodeToDelete removeFromParentNode];
                 
                 if(parentNode.nodeLevel==2)
                 {
                     if(![parentNode.nodeChildren count])
                     {
                         shouldRemove = YES;
                     }
                 }
                 else if(parentNode.nodeLevel==1 || parentNode.nodeLevel==0)
                 {
                     if(![parentNode.nodeChildren count])
                     {
                         shouldRemove = YES;
                     }

                 }
                 
                 if(!shouldRemove && ( [parentNode isKindOfClass:[ResourceFolder class]] && ![[parentNode.nodeObject valueForKey:@"resources"] count] )) //&& ![parentNode.nodeChildren count]
                 {
                     shouldRemove = YES;
                 }

                 
                 if(shouldRemove && parentNode)
                 {
                     TreeViewNode *_parentNode  = parentNode.parentNode;
                     
                     [nodesToDelete addObject:parentNode];
                     [parentNode removeFromParentNode];
                     
                     
                     if(_parentNode)
                     {
                         if(![_parentNode.nodeChildren count])
                         {
                             TreeViewNode *__parentNode  = _parentNode.parentNode;
                             
                             [nodesToDelete addObject:_parentNode];
                             [_parentNode removeFromParentNode];
                             
                             if(__parentNode && ![__parentNode.nodeChildren count])
                             {
                                 [nodesToDelete addObject:__parentNode];
                                 [__parentNode removeFromParentNode];
                             }

                         }
                     }
                 }
             }
              
              [nodesToDelete addObject:nodeToDelete];
             //---------------------------------------------------------------------
             */
             
           [nodesToDelete addObjectsFromArray:[self deleteNode:nodeToDelete]];
             
         }
     
     }
     
     if([nodesToDelete count])
     {
         for(TreeViewNode *_nodeToDelete in nodesToDelete)
         {
             /*
             if( [_parallelStackViewController isDetailControllerVisible] && [_resourcesDetailViewController.currentNode isEqual:_nodeToDelete])
             {
                 [_parallelStackViewController hideDetailController:_resourcesDetailViewController animated:NO];
             }
            */
             
             [visibleNodes removeObject:_nodeToDelete];
             [allNodes removeObject:_nodeToDelete];
         }
         
     }

    OBJC_RELEASE(nodesToDelete);
}

-(void)fetchResourcesFromServer
{
    if(_isFetchingFromServer) return;
    
    _isFetchingFromServer = YES;
    
    ProgressHUD *hud =  [ProgressHUD progressHUDAddedToView:self.view];
    if(hud)
    {
        [hud show:YES];
    }
    else
    {
      [ProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
#if USE_DEMO_DATA
    
    if(![_resourcesFolderListViewController.dataSource count])
    {
        noResourcesLabel.hidden = YES;
    }
    
    [self performBlock:^{
        _isFetchingFromServer = NO;
        
        //[self setupDemoDB];
        
        [self fetchResources:nil];
        [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
        
        
    } afterDealy:1.0];

    
#else
    
    ///SEND FETCH REQUEST HERE
    
    __block ASIHTTPRequest *request = [ConnectionManager fetchResources];
    self.resourceRequest = request;
    
    __block ResourcesViewController *weakSelf = self;
    
    [request setCompletionBlock:^{
        
        ResourcesViewController *strongSelf = weakSelf;
        
        NSLog(@"Response:%@",[request responseString]);
        NSDictionary *jsonDict = [request responseJSON];
        
        jsonDict = NULL_NIL(jsonDict);
        
        if(jsonDict && [jsonDict isKindOfClass:[NSDictionary class]])
        {
            BOOL _isValidResponse = [ConnectionManager isValidResponse:jsonDict];
            if(_isValidResponse)
            {
                
                NSArray *_resources           =  [jsonDict valueForKey:JSON_DETAILS_KEY];
                NSArray *_deletedFolderIDs    =  [jsonDict valueForKey:@"deletedFolderIds"];
                NSArray *_fileDeletedIDs      =  [jsonDict valueForKey:@"deletedResourceIds"];
                
                NSNumber *lastRequested       = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
                
                _resources                    =  NULL_NIL(_resources);
                _deletedFolderIDs             =  NULL_NIL(_deletedFolderIDs);
                _fileDeletedIDs               =  NULL_NIL(_fileDeletedIDs);

                lastRequested                 =  NULL_NIL(lastRequested);
                
                
                BOOL _isAnyChangeAvailable    = [_resources count] || [_deletedFolderIDs count] || [_fileDeletedIDs count];
                
                
                    //NOW PROCESS _RESOURCES
                    [[SSCoreDataManager sharedManager] performAsyncOperation:^(NSManagedObjectContext *context) {
                        
                        //----------------- DELETE ---------------------
                        if([_deletedFolderIDs count])
                        {
                             strongSelf->_isAnyFileDeleted = YES;
                            
                            [strongSelf deleteNodeForIDs:_deletedFolderIDs onKey:@"resourceFolderID"];
                        
                           [ResourceFolder deleteObjectsforIDs:_deletedFolderIDs onAttribute:@"resourceFolderID" inContext:context];
                            
                        }
                        
                        if([_fileDeletedIDs count])
                        {
                            strongSelf->_isAnyFileDeleted = YES;
                            
                            [strongSelf deleteNodeForIDs:_fileDeletedIDs onKey:@"resourceID"];
                            
                            [ResourceMaster deleteObjectsforIDs:_fileDeletedIDs onAttribute:@"resourceID" inContext:context];
                        }
                       
                        //------------  UPDATE OR INSERT -------------------
                        
                       [ResourceFolder createOrUpdateObjectsFromArray:_resources inContext:context checkDuplicateOnAttributes:[NSArray arrayWithObject:@"resourceFolderID"] objectsToUpdate:nil];
                        
                        
                    }
                    completion:^(NSSet *result, NSError *error)
                    {
                        NSLog(@"isMainThread =%d",[NSThread isMainThread]);
                        
                        
                       // if(![ strongSelf->_resourcesFolderListViewController.dataSource count])
                       //   strongSelf->noResourcesLabel.hidden = NO;
                        
                        [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_RESOURCES];
                        
                        strongSelf->_isFetchingFromServer = NO;
                        
                        if(_isAnyChangeAvailable)
                        {
                            [strongSelf fetchResources:^{
                                
                                [strongSelf resourceDidLoadFromServer];
                            }];
                        }
                        
                        else if(!strongSelf->_isFetchingLocally)
                        {
                            BOOL shouldHide = strongSelf-> _resourcesFolderListViewController.dataSource.count>0;
                            
                            strongSelf->noResourcesLabel.hidden = shouldHide;
                            strongSelf->containerView.hidden    = shouldHide;
                        }

                    }];
            }
            else
            {
                strongSelf->_isFetchingFromServer = NO;
                
                if(!strongSelf->_isFetchingLocally)
                {
                    BOOL shouldHide = strongSelf-> _resourcesFolderListViewController.dataSource.count>0;
                    
                    strongSelf->noResourcesLabel.hidden = shouldHide;
                    strongSelf->containerView.hidden    = shouldHide;
                }

            }
        }
        else
        {
            strongSelf->_isFetchingFromServer = NO;
            
            if(![ strongSelf->_resourcesFolderListViewController.dataSource count])
            {
                strongSelf->noResourcesLabel.hidden = NO;
                strongSelf->containerView.hidden    = NO;
               
            }
            
            
        }
        
        [strongSelf performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];

    }];
    
    [request setFailedBlock:^{
        
         ResourcesViewController *strongSelf = weakSelf;
         strongSelf->_isFetchingFromServer = NO;
        
        
        if(!strongSelf->_isFetchingLocally)
        {
            BOOL shouldHide = strongSelf-> _resourcesFolderListViewController.dataSource.count>0;
            
            strongSelf->noResourcesLabel.hidden = shouldHide;
            strongSelf->containerView.hidden    = shouldHide;
        }
        

         [strongSelf performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.0];
    }];
    
#endif
    
    
}

-(void)fetchResources:(void(^)(void))completion
{
    
    _isFetchingLocally = YES;
    
#if USE_DEMO_DATA
    
    [self performBlock:^{
        
        [self setupDemoDB];
        
        [_resourcesFolderListViewController.tableView reloadData];
        
        if(!_isFetchingFromServer)
        {
            noResourcesLabel.hidden = NO;
            containerView.hidden = _resourcesFolderListViewController.dataSource.count>0;
        }
        
    } afterDealy:0.5];
    
#else
    
    //FETCH RESOURCES LOCALLY
    
     [ResourceFolder fetchAsynWithPredicate:nil completion:^(NSArray *result, NSError *error){
     
         
        _isFetchingLocally = NO;
         
       //  if(completion) completion();
         
    if([result count])
    {
      
      /*
      if(_isAnyFileDeleted)
      {
          [TreeViewNode removeAllNodes];
          _resourcesFolderListViewController.dataSource = nil;
      }
      */
        
      [self setupNodesForResourceFolders:result];
    }
     
     [_resourcesFolderListViewController.tableView reloadData];
     
      if(!_isFetchingFromServer)
       {
          noResourcesLabel.hidden = NO;
          containerView.hidden = _resourcesFolderListViewController.dataSource.count>0;
       }
         
         if(completion) completion();
       
     }];

#endif
    
}

-(void)setupDemoDB
{
   
    NSMutableArray *_dataSource = [NSMutableArray array];
    Rep *currentRep = [CoreDataHandler currentRep];
    
    WholesaleDealer *dealerSite = currentRep.parentWdSite;
    
    TreeViewNode *_dealerNode = [[TreeViewNode alloc] init];
    _dealerNode.title = [dealerSite.name uppercaseString];
    _dealerNode.isDirectory = YES;
    _dealerNode.nodeLevel = 0;
    [_dataSource addObject:_dealerNode];
    OBJC_RELEASE(_dealerNode);
    
    for(int i = 0; i<2;i++)
    {
        TreeViewNode *__node = [[TreeViewNode alloc] init];
        __node.title = [NSString stringWithFormat:@"%@ %d",_dealerNode.title,i+1];
        __node.isDirectory = YES;
        __node.nodeLevel = _dealerNode.nodeLevel+1;
        [_dealerNode addChildNode:__node];
         OBJC_RELEASE(__node);
        
        for(int i = 0; i<2;i++)
        {
            TreeViewNode *fileNode = [[TreeViewNode alloc] init];
            fileNode.title = [NSString stringWithFormat:@"%@ %d",__node.title,i+1];
            fileNode.isDirectory = NO;
            fileNode.nodeLevel = __node.nodeLevel+1;
            [__node addChildNode:fileNode];
            OBJC_RELEASE(fileNode);
        }
        
        /*
        for(int i = 0; i<3;i++)
        {
            TreeViewNode *___node = [[TreeViewNode alloc] init];
            ___node.title = [NSString stringWithFormat:@"%@ %d",__node.title,i+1];
            ___node.isDirectory = YES;
            ___node.nodeLevel = __node.nodeLevel+1;
            [__node addChildNode:___node];
            [___node release];
            
            
            for(int i = 0; i<4;i++)
            {
                TreeViewNode *fileNode = [[TreeViewNode alloc] init];
                fileNode.title = [NSString stringWithFormat:@"%@ %d",___node.title,i+1];
                fileNode.isDirectory = NO;
                fileNode.nodeLevel = ___node.nodeLevel+1;
                [___node addChildNode:fileNode];
                [fileNode release];
            }
        }
        */
    }

    NSArray *oems = dealerSite.parentOem.allObjects;
    
    if([oems count])
    {
        NSSortDescriptor *_descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        oems = [oems sortedArrayUsingDescriptors:[NSArray arrayWithObject:_descriptor]];
        OBJC_RELEASE(_descriptor);
    }
    
    for(OEM *oem in oems)
    {
        TreeViewNode *_node = [[TreeViewNode alloc] init];
        _node.title = [oem.name uppercaseString];
        _node.isDirectory = YES;
        _node.nodeLevel = 0;
        [_dataSource addObject:_node];
        OBJC_RELEASE(_node);
        
        
        for(int i = 0; i<3; i++)
        {
            TreeViewNode *__node = [[TreeViewNode alloc] init];
            __node.title = [NSString stringWithFormat:@"%@ %d",_node.title,i+1];
            __node.isDirectory = YES;
            __node.nodeLevel = _node.nodeLevel+1;
            [_node addChildNode:__node];
            OBJC_RELEASE(__node);
            
            for(int i = 0; i<2;i++)
            {
                TreeViewNode *___node = [[TreeViewNode alloc] init];
                ___node.title = [NSString stringWithFormat:@"%@ %d",__node.title,i+1];
                ___node.isDirectory = YES;
                ___node.nodeLevel = __node.nodeLevel+1;
                [__node addChildNode:___node];
                 OBJC_RELEASE(___node);
                
                
                for(int i = 0; i<2;i++)
                {
                    TreeViewNode *fileNode = [[TreeViewNode alloc] init];
                    fileNode.title = [NSString stringWithFormat:@"%@ %d",___node.title,i+1];
                    fileNode.isDirectory = NO;
                    fileNode.nodeLevel = ___node.nodeLevel+1;
                    [___node addChildNode:fileNode];
                    OBJC_RELEASE(fileNode);
                }
            }

        }
    }

    _resourcesFolderListViewController.dataSource = _dataSource;
}


-(void)_setupNodesForResourceFolders:(NSArray *)folders
{
    
    Rep *currentRep = [CoreDataHandler currentRep];
    WholesaleDealer *dealerSite = currentRep.parentWdSite;
    NSArray *oems = dealerSite.parentOem.allObjects;
    
    NSMutableArray *_nodesToShow = [NSMutableArray array];
    //NSMutableArray *_foldersToShow = [NSMutableArray array];
    
    //======= CALCULATE FOLDERS AND SUB FOLDERS TO BE VISIBLE ========
    
    NSArray *catFolders = [folders filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.parentFolderID.integerValue==0"]];
    
    //SORT CAT FOLDER ACCORDING TO CREATED  DATE
    
    if([catFolders count])
    {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:NO]; //@"createdDate"
        
       //NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate" ascending:YES];
       catFolders = [catFolders sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
       OBJC_RELEASE(sortDescriptor);

    }
    
    for(ResourceFolder *_folder in catFolders)
    {
        //if(_folder.parentFolderID.integerValue!=0) continue;
        
        TreeViewNode* _node = [[TreeViewNode alloc] init];
        _node.title = _folder.resourceFolderName;
        _node.isDirectory = YES;
        _node.nodeLevel = 1;
        

        NSArray *_subFolders =  [folders filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.parentFolderID=%@",_folder.resourceFolderID]];
        
        
        if([_subFolders count])
        {
             // SORT CAT SUBFOLDERS ACCORDING TO CREATED  DATE
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:NO];
            _subFolders = [_subFolders sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            OBJC_RELEASE(sortDescriptor);
            
            NSMutableArray *_subFoldersToAdd = [NSMutableArray array];
            for(ResourceFolder *_subFolder in _subFolders)
            {
               // if([[_subFolder publishedResources] count])
                if([_subFolder.resources count])
                {
                    [_subFoldersToAdd addObject:_subFolder];
                    
                    TreeViewNode* _subNode = [[TreeViewNode alloc] init];
                    _subNode.title = _subFolder.resourceFolderName;
                    _subNode.isDirectory = YES;
                    _subNode.nodeObject = _subFolder;
                    _subNode.nodeLevel = _node.nodeLevel+1;
                    [_node addChildNode:_subNode];
                    OBJC_RELEASE(_subNode);
                }
            }
            
            if([_subFoldersToAdd count])
            {
                _node.nodeObject = _folder;
                [_nodesToShow addObject:_node];
                
                //[_folder setAssociateObject:_subFoldersToAdd forKey:@"subFolders"];
            }
        }
        else if([_folder.resources count]) //if([[_folder publishedResources] count])
        {
             _node.nodeObject = _folder;
            [_nodesToShow addObject:_node];
            // [_foldersToShow addObject:_folder];
        }
        
        OBJC_RELEASE(_node);
    }
    
       
    NSSortDescriptor *sortDescriptor1   = [[NSSortDescriptor alloc] initWithKey:@"nodeObject.userTypeID" ascending:NO];
   // NSSortDescriptor *sortDescriptor2  = [[NSSortDescriptor alloc] initWithKey:@"nodeObject.createdDate" ascending:NO];
    
    NSSortDescriptor *sortDescriptor2  = [[NSSortDescriptor alloc] initWithKey:@"nodeObject.modifiedDate" ascending:NO];
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor1,sortDescriptor2, nil];
    [_nodesToShow sortUsingDescriptors:sortDescriptors];

    OBJC_RELEASE(sortDescriptor1);
    OBJC_RELEASE(sortDescriptor2);
    
    NSMutableArray *_dataSource = [[NSMutableArray alloc] init];
    
    /*
     //------------DEALERSITE NODE-------------------
     TreeViewNode* node = [[TreeViewNode alloc] init];
     node.title         = dealerSite.name;
     node.isDirectory   = YES;
     node.nodeLevel     = 0;
     node.nodeObject    = dealerSite;
     
     for(TreeViewNode *__node in _nodesToShow)
     {
     ResourceFolder *_folder = (ResourceFolder *) __node.nodeObject;
     
     if(_folder.userTypeID.intValue!=FFUserTypeOEM && [_folder.wdOrOemID isEqualToNumber:dealerSite.wdSiteID])
     {
     [node addChildNode:__node];
     }
     }
     [_dataSource addObject:node];
     [node release];
     
     
     //-----------------OEM NODE--------------------------
     for(OEM *_oem in oems)
     {
     TreeViewNode* _node = [[TreeViewNode alloc] init];
     _node.title         = _oem.name;
     _node.isDirectory   = YES;
     _node.nodeLevel     = 0;
     _node.nodeObject    = _oem;
     
     int i = 0;
     for(TreeViewNode *node in _nodesToShow)
     {
     ResourceFolder *_folder = (ResourceFolder *) node.nodeObject;
     
     if(_folder.userTypeID.intValue==FFUserTypeOEM && [_folder.wdOrOemID isEqualToNumber:_oem.oemID])
     {
     [_node addChildNode:node];
     }
     
     [_dataSource addObject:_node];
     [_node release];
     
     i++;
     }
     
     }
     */
    
    //--------------------------------------------------------------------------
    
    for(TreeViewNode *__node in _nodesToShow)
    {
        /*
        TreeViewNode* node = [[TreeViewNode alloc] init];
        node.isDirectory   = YES;
        node.nodeLevel     = 0;
        [_dataSource addObject:node];
         */
        
        ResourceFolder *_folder = (ResourceFolder *) __node.nodeObject;
        
        // DEALER SITE NODE
        if(_folder.userTypeID.intValue != FFUserTypeOEM)
        {
            NSArray *dealerNodes = [_dataSource filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.title=%@",dealerSite.name]];
            
            TreeViewNode* node = [dealerNodes count] ? [dealerNodes objectAtIndex:0] : nil;
            
            if(!node)
            {
                node = [[TreeViewNode alloc] init];
                node.isDirectory   = YES;
                node.nodeLevel     = 0;
                node.title         = dealerSite.name;
                node.nodeObject    = dealerSite;
                [_dataSource addObject:node];
                OBJC_RELEASE(node);
            }
            
            [node addChildNode:__node];
            
            
        }
        else if(_folder.userTypeID.intValue == FFUserTypeOEM) //OEM NODE
        {
            NSNumber *oemID = _folder.wdOrOemID;
            
            __block OEM *_oem = nil;
            
            //GET THE OEM
            [oems enumerateObjectsUsingBlock:^(OEM *obj, NSUInteger idx, BOOL *stop)
             {
                 if([obj.oemID isEqualToNumber:oemID])
                 {
                     _oem = obj;
                     *stop = YES;
                 }
             }];
            
            if(_oem)
            {
               NSArray *oemNodes = [_dataSource filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.title=%@",_oem.name]];
                
                TreeViewNode* node = [oemNodes count] ? [oemNodes objectAtIndex:0] : nil;
                if(!node)
                {
                    node = [[TreeViewNode alloc] init];
                    node.isDirectory   = YES;
                    node.nodeLevel     = 0;
                    node.title         = _oem.name;
                    node.nodeObject    = _oem;
                    [_dataSource addObject:node];
                    OBJC_RELEASE(node);
                }
                
                [node addChildNode:__node];
            }
            
        }
    }
    
    
    _resourcesFolderListViewController.dataSource = _dataSource;
    
    [_dataSource release];
}


-(void)__exapandNode:(TreeViewNode *)__node inDataSource:(NSMutableArray *)_dataSource
{
    if(__node.isExpanded)
    {
        NSUInteger _index = [_dataSource indexOfObject:__node];
        
        if(_index!=NSNotFound)
        {
            NSUInteger __index = _index+1;
            
            //------------ SHOULD BE REMOVED IF NOT REQUIRED -------------------------
            NSSortDescriptor *sortDescriptor1  = [[NSSortDescriptor alloc] initWithKey:@"nodeObject.createdDate" ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor1, nil];
            [__node.nodeChildren sortUsingDescriptors:sortDescriptors];
            OBJC_RELEASE(sortDescriptor1);
              //--------------------------------------------------------------------------

            
            NSArray *children = __node.nodeLevel!=3 ? [__node nodeChildrenWithDirectoryType:YES] :__node.nodeChildren;
            
            for(TreeViewNode *___node in children)
            {
                if(![_dataSource containsObject:___node])
                {
                   // if([___node.nodeChildren count] || [[___node.nodeObject valueForKey:@"resources"] count])
                    [_dataSource insertObject:___node atIndex:__index];
                }
                
                  __index++;
            }
        }
    }

}

-(void)_exapandNode:(TreeViewNode *)__node inDataSource:(NSMutableArray *)_dataSource
{
    if(__node.isExpanded)
    {
        NSUInteger _index = [_dataSource indexOfObject:__node];
        
        if(_index!=NSNotFound)
        {
            NSUInteger __index = _index+1;
            
            //------------ SHOULD BE REMOVED IF NOT REQUIRED -------------------------
            NSSortDescriptor *sortDescriptor1  = [[NSSortDescriptor alloc] initWithKey:@"nodeObject.createdDate" ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor1, nil];
            [__node.nodeChildren sortUsingDescriptors:sortDescriptors];
            OBJC_RELEASE(sortDescriptor1);
            //--------------------------------------------------------------------------
            
            
            NSArray *children = __node.nodeLevel!=3 ? [__node nodeChildrenWithDirectoryType:YES] :__node.nodeChildren;
            
            for(TreeViewNode *___node in children)
            {
                
                 NSInteger _locationIndex = [___node currentIndexInDataSource:_dataSource];
                
                if(_locationIndex==NSNotFound)
                {
                    // if([___node.nodeChildren count] || [[___node.nodeObject valueForKey:@"resources"] count])
                    
                    [_dataSource insertObject:___node atIndex: _locationIndex!=NSNotFound ? _locationIndex : __index];
                }
                else if(_locationIndex!=NSNotFound)
                {
                    __index = _locationIndex;
                }
                
                __index++;
            }
        }
    }
    
}

-(void)setupNodesForResourceFolders:(NSArray *)folders
{
    
    NSMutableArray *__allNodes  = [TreeViewNode allNodes];
    NSMutableArray *_dataSource =  [_resourcesFolderListViewController.dataSource retain];
    if(!_dataSource)
    {
        _dataSource = [[NSMutableArray alloc] init];
       // _resourcesFolderListViewController.dataSource = _dataSource;
    }
    
    Rep *currentRep = [CoreDataHandler currentRep];
    WholesaleDealer *dealerSite = currentRep.parentWdSite;
    NSArray *oems = dealerSite.parentOem.allObjects;
    
    NSMutableArray *_nodesToShow = [NSMutableArray array];
    
    //======= CALCULATE FOLDERS AND SUB FOLDERS TO BE VISIBLE ========
    
    NSArray *catFolders = [folders filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.parentFolderID.integerValue==0"]];
    
    //SORT CAT FOLDER ACCORDING TO CREATED  DATE
    
    
    if([catFolders count])
    {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate" ascending:NO]; //@"createdDate"
        
        //NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate" ascending:YES];
        catFolders = [catFolders sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        OBJC_RELEASE(sortDescriptor);
    }
    
    
    
    for(ResourceFolder *_folder in catFolders)
    {
        //if(_folder.parentFolderID.integerValue!=0) continue;
        
        TreeViewNode* _node = nil;
        
        NSString *identifierToCheck = [NSString stringWithFormat:@"folder%@",_folder.resourceFolderID];
        
        if([__allNodes count])
        {
            
            NSArray  *_nodes = [__allNodes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.identifier=%@",identifierToCheck]];
        
           _node = [_nodes count] ? [_nodes objectAtIndex:0] : nil;
        }
        
        if(!_node)
        {
          _node = [[TreeViewNode alloc] init];
          _node.isDirectory = YES;
          _node.nodeLevel = 1;
        }
        else
        {
            [_node retain];
        }
        
        
        _node.title = _folder.resourceFolderName;
        _node.identifier = identifierToCheck;
        
        NSArray *_subFolders =  [folders filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.parentFolderID=%@",_folder.resourceFolderID]];
        
        
        if([_subFolders count])
        {
            
            // SORT CAT SUBFOLDERS ACCORDING TO CREATED  DATE
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate" ascending:NO];
            _subFolders = [_subFolders sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            OBJC_RELEASE(sortDescriptor);
            
            
            NSMutableArray *_subFoldersToAdd = [NSMutableArray array];
            for(ResourceFolder *_subFolder in _subFolders)
            {
                NSString *_identifierToCheck = [NSString stringWithFormat:@"folder%@",_subFolder.resourceFolderID];
                NSArray  *subnodes = [_node.nodeChildren filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.identifier=%@",_identifierToCheck]];
                    
                TreeViewNode* _subNode = [subnodes count] ? [subnodes objectAtIndex:0] : nil;
                    
                if([_subFolder.resources count]) //if([[_subFolder publishedResources] count])
                {
                    [_subFoldersToAdd addObject:_subFolder];
                    
                    if(!_subNode)
                    {
                       _subNode = [[TreeViewNode alloc] init];
                       _subNode.isDirectory = YES;
                       //_subNode.nodeObject = _subFolder;
                       _subNode.nodeLevel = _node.nodeLevel+1;
                       [_node addChildNode:_subNode];
                       OBJC_RELEASE(_subNode);
                    }
                    
                    _subNode.nodeObject = _subFolder;
                    _subNode.title = _subFolder.resourceFolderName;
                    _subNode.identifier = _identifierToCheck;
                    
                    if(![_node.nodeChildren containsObject:_subNode])
                        [_node addChildNode:_subNode];
                    
                    [self reloadResourceDetailForNode:_subNode];
                }
                else if(_subNode)
                {
                   
                    NSArray *_nodesToDelete =  [self deleteNode:_subNode];
                    [__allNodes removeObjectsInArray:_nodesToDelete];
                    
                    [_dataSource removeObjectsInArray:_nodesToDelete];

                }
            }
            
            if([_subFoldersToAdd count])
            {
                _node.nodeObject = _folder;
                [_nodesToShow addObject:_node];
                
               // int _isDealerSiteNode  = (_folder.userTypeID.intValue != FFUserTypeOEM);
                //_node.isDealersiteNode = _isDealerSiteNode;
                
                if(_node.isExpanded)
                    [self _exapandNode:_node inDataSource:_dataSource];
            }

        }
        else if([_folder.resources count]) //if([[_folder publishedResources] count])
        {
            _node.nodeObject = _folder;
            [_nodesToShow addObject:_node];
            
            //int _isDealerSiteNode  = (_folder.userTypeID.intValue != FFUserTypeOEM);
            //_node.isDealersiteNode = _isDealerSiteNode;
            
            if(_node.isExpanded)
            [self _exapandNode:_node inDataSource:_dataSource];
            
            [self reloadResourceDetailForNode:_node];
        }
        else if(_node.nodeObject)
        {
            NSArray *_nodesToDelete =  [self deleteNode:_node];
            [__allNodes removeObjectsInArray:_nodesToDelete];
           // [_nodesToShow removeObject:_node];
            
            [_dataSource removeObjectsInArray:_nodesToDelete];
        }
        
        /*
        if([_node.nodeChildren count])
        {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"self.nodeObject.createdDate" ascending:NO]; //modifiedDate
            [_node.nodeChildren sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            OBJC_RELEASE(sortDescriptor);
        }
        */
        
        //---------------------------------------------------------
          if(!_node.nodeObject)[__allNodes removeObject:_node];
        //---------------------------------------------------------
        
        OBJC_RELEASE(_node);
    }
    

    NSSortDescriptor *sortDescriptor1   = [[NSSortDescriptor alloc] initWithKey:@"nodeObject.userTypeID" ascending:NO];
    
    // NSSortDescriptor *sortDescriptor2  = [[NSSortDescriptor alloc] initWithKey:@"nodeObject.createdDate" ascending:NO];
    
    NSSortDescriptor *sortDescriptor2  = [[NSSortDescriptor alloc] initWithKey:@"nodeObject.createdDate" ascending:NO]; //@"nodeObject.modifiedDate"
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor1,sortDescriptor2, nil];
    [_nodesToShow sortUsingDescriptors:sortDescriptors];
    
    OBJC_RELEASE(sortDescriptor1);
    OBJC_RELEASE(sortDescriptor2);
    
    
    //--------------------------------------------------------------------------
    
    for(TreeViewNode *__node in _nodesToShow)
    {
        ResourceFolder *_folder = (ResourceFolder *) __node.nodeObject;
        
        // DEALER SITE NODE
        if(_folder.userTypeID.intValue != FFUserTypeOEM)
        {
                       
            NSString *identifierToCheck = [NSString stringWithFormat:@"folderDealer%@",dealerSite.wdSiteID];
            NSArray  *dealerNodes = [__allNodes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.identifier=%@",identifierToCheck]];
            
            TreeViewNode* node = [dealerNodes count] ? [dealerNodes objectAtIndex:0] : nil;
            
            if(!node)
            {
                node = [[TreeViewNode alloc] init];
                node.isDirectory   = YES;
                node.nodeLevel     = 0;
                node.nodeObject    = dealerSite;
                [_dataSource insertObject:node atIndex:0];
                
                OBJC_RELEASE(node);
            }
            
             node.isDealersiteNode = 1;
             node.title = dealerSite.name;
             node.identifier = identifierToCheck;
            //__node.isDealersiteNode = node.isDealersiteNode;
          
           if(![node.nodeChildren containsObject:__node])
              [node addChildNode:__node];
            
            if(![_dataSource containsObject:node])
               [_dataSource insertObject:node atIndex:0];
            
       
            //----------
            
            if([node.nodeChildren count])
            {
                NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"self.nodeObject.createdDate" ascending:NO];
                [node.nodeChildren sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                OBJC_RELEASE(sortDescriptor);
            }
            
            
            if(node.isExpanded)
            {
                [self _exapandNode:node inDataSource:_dataSource];
            }
            
            if(__node.isExpanded)
            {
              [self _exapandNode:__node inDataSource:_dataSource];
            }
            
            
            
            
            //------
     
        }
        else if(_folder.userTypeID.intValue == FFUserTypeOEM) //OEM NODE
        {
            NSNumber *oemID = _folder.wdOrOemID;
            
            __block OEM *_oem = nil;
            
            //GET THE OEM
            [oems enumerateObjectsUsingBlock:^(OEM *obj, NSUInteger idx, BOOL *stop)
             {
                 if([obj.oemID isEqualToNumber:oemID])
                 {
                     _oem = obj;
                     *stop = YES;
                 }
             }];
            
            if(_oem)
            {
               // NSArray *oemNodes = [__allNodes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.title=%@",_oem.name]];
                
                NSString *identifierToCheck = [NSString stringWithFormat:@"folderOem%@",_oem.oemID];
                NSArray *oemNodes = [__allNodes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.identifier=%@",identifierToCheck]];
                
                TreeViewNode* node = [oemNodes count] ? [oemNodes objectAtIndex:0] : nil;
                if(!node)
                {
                    node = [[TreeViewNode alloc] init];
                    node.isDirectory   = YES;
                    node.nodeLevel     = 0;
                    //node.title         = _oem.name;
                  
                    [_dataSource addObject:node];

                    OBJC_RELEASE(node);
                }
                
                 node.isDealersiteNode = 0;
                 node.nodeObject       = _oem;
                 node.title            = _oem.name;
                 node.identifier       = identifierToCheck;
                
                //__node.isDealersiteNode = node.isDealersiteNode;
                
                /*
                if([_folder.modifiedDate longLongValue] > [node.modifiedDate longLongValue])
                {
                    node.modifiedDate = _folder.modifiedDate;
                }
                */
                
                 if(![node.nodeChildren containsObject:__node])
                  [node addChildNode:__node];
                
                if(![_dataSource containsObject:node])
                    [_dataSource addObject:node];
                
                if([node.nodeChildren count])
                {
                    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"self.nodeObject.createdDate" ascending:NO];
                    [node.nodeChildren sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                    OBJC_RELEASE(sortDescriptor);
                }

                
                if(node.isExpanded)
                {
                    [self _exapandNode:node inDataSource:_dataSource];
                }
                
                if(__node.isExpanded)
                {
                    [self _exapandNode:__node inDataSource:_dataSource];
                }
                
            }
            
        }
    }
    
    /*
    sortDescriptor1   = [[NSSortDescriptor alloc] initWithKey:@"isDealersiteNode" ascending:NO];
    sortDescriptor2   = [[NSSortDescriptor alloc] initWithKey:@"modifiedDate" ascending:NO];
    sortDescriptors = [NSArray arrayWithObjects:sortDescriptor1,sortDescriptor2, nil];
    [_dataSource sortUsingDescriptors:sortDescriptors];
    OBJC_RELEASE(sortDescriptor1);
    OBJC_RELEASE(sortDescriptor2);
    */
   
    _resourcesFolderListViewController.dataSource = _dataSource;
    //[_resourcesFolderListViewController.tableView reloadData];
    
    
     OBJC_RELEASE(_dataSource);
}


-(void)refreshButtonAction:(UIButton*)refreshButton
{
   // if(_isRefreshRequested) return;
    //_isRefreshRequested = YES;
    

    [self fetchResourcesFromServer];

    
    //[_resourcesFolderListViewController reset];
}

-(void)resourceDidLoadFromServer
{
    //if(!_isRefreshRequested) return;
    //_isRefreshRequested = NO;
    
    /*
    [_resourcesDetailViewController removeAllResources];
    [_resourcesFolderListViewController reset];
    
    */
  
    if(_isAnyFileDeleted)
    {
        
        TreeViewNode *_detailNode = _resourcesDetailViewController.currentNode;
        
        if([_parallelStackViewController isDetailControllerVisible] && _detailNode.isExpanded)
        {
             if(![_resourcesFolderListViewController.dataSource containsObject:_detailNode])
             {

                //-------
                 _detailNode.isExpanded = NO;
                 //------
            
                [_resourcesDetailViewController removeAllResources];
                [_resourcesFolderListViewController reset];
             }

        }
        
        _isAnyFileDeleted = NO;
    }
    
    
  
    [ProgressHUD hideHUDForView:self.view animated:YES];
}



-(void)resourceAccordion:(ResourcesFolderListViewController *)controller didSelectNode:(TreeViewNode *)node expandedNodes:(NSArray *)nodes
{
    //----------------------- ONLY ADD FILE  NODES -----------------------------------

       NSArray *_resourceNodes = nil;
           //FETCH FILES ACCORDING TO THIS NODE
        
        ResourceFolder *_folder = (ResourceFolder *) node.nodeObject;
        
        if([_folder isKindOfClass:[ResourceFolder class]])
        {
        
        for(ResourceMaster *resourceFile in _folder.resources)
        {
           // NSArray *_nodes = [node.nodeChildren filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.title=%@",resourceFile.resourceName]];
            
            NSString *identifierToCheck = [NSString stringWithFormat:@"file%@",resourceFile.resourceID];
            NSArray *_nodes = [node.nodeChildren filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.identifier=%@",identifierToCheck]];
            
            TreeViewNode* _fileNode = [_nodes count] ? [_nodes objectAtIndex:0] : nil;
            if(!_fileNode)
            {
              _fileNode = [[TreeViewNode alloc] init];
              _fileNode.title = resourceFile.resourceName;
              _fileNode.isDirectory = NO;
              _fileNode.nodeObject = resourceFile;
              _fileNode.nodeLevel = node.nodeLevel+1;
              [node addChildNode:_fileNode];
             OBJC_RELEASE(_fileNode);
            }
            
            _fileNode.title = resourceFile.resourceName;
            _fileNode.identifier = identifierToCheck;
            
        }
            
            if([node.nodeChildren count])
            {
               _resourceNodes = [node nodeChildrenWithDirectoryType:NO];
            }
        }
    
    
    if([_resourceNodes count])
    {
        NSSortDescriptor *sortDescriptor1  = [[NSSortDescriptor alloc] initWithKey:@"nodeObject.createdDate" ascending:NO];
        
         NSSortDescriptor *sortDescriptor2  = [[NSSortDescriptor alloc] initWithKey:@"nodeObject.resourceID" ascending:NO];
        /*
        NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES comparator:^NSComparisonResult(id obj1, id obj2) {
            
            NSString *p1 = (NSString *)obj1;
            NSString *p2 = (NSString *)obj2;
            return [p1 caseInsensitiveCompare:p2];
            
            
        }];
       */
        
        // NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor1, nil];
         NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor1,sortDescriptor2, nil];
        _resourceNodes =  [_resourceNodes sortedArrayUsingDescriptors:sortDescriptors];
        OBJC_RELEASE(sortDescriptor1);
        OBJC_RELEASE(sortDescriptor2);
        
        TreeViewNode *currentSelectedNode = _resourcesDetailViewController.currentNode;
        
         BOOL _isVisible =  [currentSelectedNode isEqual:node];
        
        // BOOL _isVisible  =  currentSelectedNode.isExpanded && [currentSelectedNode isEqual:node];
    
        [_resourcesDetailViewController removeAllResources];
        [_resourcesDetailViewController addResources:_resourceNodes forResource:node];
    
        if(!_isVisible)
        {
          [_parallelStackViewController showDetailController:YES];
        }
     
    }
}

/*
-(void)resourceAccordion:(ResourcesFolderListViewController *)controller didSelectNode:(TreeViewNode *)node expandedNodes:(NSArray *)nodes
{
    //----------------------- ONLY ADD FILE  NODES -----------------------------------
    
    NSArray *_resourceNodes = [node nodeChildrenWithDirectoryType:NO];
    
    

    if(![_resourceNodes count])
    {
        //FETCH FILES ACCORDING TO THIS NODE
        
        ResourceFolder *_folder = (ResourceFolder *) node.nodeObject;
        
        if([_folder isKindOfClass:[ResourceFolder class]])
        {
            
            for(ResourceMaster *resourceFile in _folder.resources)
            {
                TreeViewNode* _fileNode = [[TreeViewNode alloc] init];
                _fileNode.title = resourceFile.resourceName;
                _fileNode.isDirectory = NO;
                _fileNode.nodeObject = resourceFile;
                _fileNode.nodeLevel = node.nodeLevel+1;
                [node addChildNode:_fileNode];
                OBJC_RELEASE(_fileNode);
            }
            
            if([node.nodeChildren count])
            {
                
                _resourceNodes = [node nodeChildrenWithDirectoryType:NO];
            }
        }
        
    }
    
 
// if([_resourceNodes count])
// {
// NSPredicate *_publishedPredicate = [NSPredicate predicateWithFormat:@"self.nodeObject.isPublished=%@",[NSNumber numberWithInt:1]];
// 
// _resourceNodes = [_resourceNodes filteredArrayUsingPredicate:_publishedPredicate];
// }
 

    if([_resourceNodes count])
    {
        
        NSSortDescriptor *sortDescriptor1  = [[NSSortDescriptor alloc] initWithKey:@"nodeObject.createdDate" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor1, nil];
        _resourceNodes =  [_resourceNodes sortedArrayUsingDescriptors:sortDescriptors];
        OBJC_RELEASE(sortDescriptor1);
        
        [_resourcesDetailViewController addResources:_resourceNodes forResource:node];
        [_parallelStackViewController showDetailController:YES];
    }
}
*/

-(void)resourceAccordion:(ResourcesFolderListViewController *)controller didDeselectNode:(TreeViewNode *)node collapsedNodes:(NSArray *)nodes
{
    //--------- ONLY REMOVES FILE TYPE NODES ------------
    
    /*
    NSArray *_resourceNodes = [node nodeChildrenWithDirectoryType:NO];
    
    if([_resourceNodes count])
    {
      [_resourcesDetailViewController removeResources:_resourceNodes];
        [_parallelStackViewController hideDetailController:_resourcesDetailViewController animated:YES];
    }
    */
    
    if([_resourcesDetailViewController.dataSource count])
    {
      [_resourcesDetailViewController removeAllResources];
      [_parallelStackViewController hideDetailController:_resourcesDetailViewController animated:YES];
    }
}


-(void)reloadResourceDetailForNode:(TreeViewNode *)node
{
    if(node.nodeLevel>0 && node.isExpanded)
    {
        [self resourceAccordion:nil didSelectNode:node expandedNodes:nil];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

@end
