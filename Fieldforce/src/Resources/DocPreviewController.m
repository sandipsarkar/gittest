//
//  DocPreviewController.m
//  DocInteraction
//
//  Created by Randem IT on 18/11/13.
//
//

#import "DocPreviewController.h"
//#import "QLPreviewController+FirstResponder.h"
#import "TreeViewNode.h"
#import "ResourceMaster.h"
#import "ProgressHUD.h"



@interface DocPreviewController ()<QLPreviewControllerDataSource,QLPreviewControllerDelegate>
{
    QLPreviewController *previewController;
}
@property (nonatomic,OBJC_STRONG) NSString *filePath;
@property (nonatomic,OBJC_STRONG) ASIHTTPRequest *fileRequest;
@end

@implementation DocPreviewController


-(void)dealloc
{
    [_fileRequest clearDelegatesAndCancel];
    
     OBJC_RELEASE(_fileRequest);
    
     OBJC_RELEASE(_filePath);
     OBJC_RELEASE(_fileNode);
     OBJC_RELEASE(previewController);
    
   
    [super dealloc];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
    if([self respondsToSelector:@selector(edgesForExtendedLayout)])
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.navigationController.navigationBar addBackGroundImage:@"navigation_bar.png"];
    [self addBackButtonWithSelector:@selector(backAction)];
     [self addTitle:self.fileNode.title withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    [NSProgress progressWithTotalUnitCount:1];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupPreviewController];
    [self loadOrDownloadResourceFile];
    
    /*
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"Email" style:UIBarButtonItemStyleBordered target:self action:nil];
    self.navigationItem.rightBarButtonItem = rightButton;
    [rightButton release];
    */
    
   // NSProgress *_currentProgress = [NSProgress currentProgress];
    
    
}

-(void)setupPreviewController
{
    previewController = [[QLPreviewController alloc] init];
    previewController.dataSource = self;
    previewController.delegate = self;
    [self addChildViewController:previewController];
    previewController.view.backgroundColor = [UIColor whiteColor];
    
    previewController.view.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
    previewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:previewController.view];
    
    if(self.selectedIndex>=0)
     previewController.currentPreviewItemIndex = self.selectedIndex;
    [previewController didMoveToParentViewController:self];


}

-(void)viewDidAppear:(BOOL)animated
{
     [super viewDidAppear:animated];
}


-(void)backAction
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)loadOrDownloadResourceFile
{
    ResourceMaster *resourceFile =(ResourceMaster *)self.fileNode.nodeObject;
    
    NSString *resourceFileName = [resourceFile resourceFileName];
    
    //NSString *resourceFileName = @"ipad-mini-game-small-2.jpg";// self.fileNode.fileName;
    
    if(!resourceFileName.length)
    {
        previewController.view.hidden = YES;
       
        [self handleResourceError];
        
        return;
    }

    
    NSString *resourceFilePath =  resourceFileName ? [DirectoryManager resourceFilePathWithName:resourceFileName] : nil;
    
    // NSString *resourceFilePath = self.fileNode.fileName ? [[NSBundle mainBundle] pathForResource:self.fileNode.fileName ofType:nil] : nil;
    
    if(resourceFilePath && [[NSFileManager defaultManager] fileExistsAtPath:resourceFilePath isDirectory:NO])
    {
        previewController.view.hidden = NO;
        self.filePath = resourceFilePath;
        
#if LOCAL
        [self performSelector:@selector(action) withObject:nil afterDelay:3.0];
#endif
        
    }
    else //DOWNLOAD FROM SERVER
    {
        previewController.view.hidden = YES;
        [self downloadFileForName:resourceFileName atPath:resourceFilePath];
    }

}

-(void)action
{
   UIView *__v =  [self.view subViewOfType:[UIScrollView class]];
    
    if(__v)
    {
        NSLog(@"vv=%@",__v);
    }
}

-(void)refresh
{
    NSURL *fileURL = self.filePath ?  [NSURL fileURLWithPath:self.filePath] : nil;
    
    if(fileURL && [QLPreviewController canPreviewItem:fileURL])
    {
       previewController.view.hidden = NO;
      [previewController refreshCurrentPreviewItem];
    }
    else
    {
        [[NSFileManager defaultManager] removeItemAtPath:self.filePath error:nil];
        
        NSString *errorMsg = RESOURCE_CORRUPTED_FILE_MESSAGE;
        UIAlertView *alert =   [UIAlertView alertViewWithTitle:@"" message:errorMsg cancelButtonTitle:@"Ok" otherButtonTitles:nil onDismiss:^(int buttonIndex){
            
        } onCancel:^{
            
            [self backAction];
        }];
        
        OBJC_RELEASE(alert);

    }
}

-(void)handleResourceError
{
    NSString *errorMsg = [NSString stringWithFormat:RESOURCE_FILE_UNAVAILABLE_MESSAGE,self.fileNode.title];
    
    UIAlertView *alert =   [UIAlertView alertViewWithTitle:@"" message:errorMsg cancelButtonTitle:@"Ok" otherButtonTitles:nil onDismiss:^(int buttonIndex){
        
    } onCancel:^{
        
        [self backAction];
    }];
    
    OBJC_RELEASE(alert);

}

-(void)downloadFileForName:(NSString *)fileName atPath:(NSString *)downloadPath
{
    if(!fileName || !downloadPath) return;
    
    ProgressHUD *progress  = [ProgressHUD showHUDAddedTo:self.view animated:YES];
    progress.mode = MBProgressHUDModeDeterminate;

    __block DocPreviewController *weakSelf = self;
    
    __block ASIHTTPRequest *request =  [ConnectionManager requestToDownloadResourceFileForName:fileName toPath:downloadPath];
    self.fileRequest = request;
    [request setDownloadProgressDelegate:progress];
  
    [request setCompletionBlock:^{
        
      DocPreviewController *strongSelf = weakSelf;
    __unused  int _statusCode =   request.responseStatusCode;
        
        progress.progress = 1.0;
        
        [strongSelf performSelector:@selector(hideProgressHUD) withObject:nil];
        
        
        if(_statusCode>=300)
        {
            if(downloadPath)
            {
              [[NSFileManager defaultManager] removeItemAtPath:downloadPath error:nil];
            }
            strongSelf.filePath = nil;
            
           [strongSelf performBlock:^{
            
            [strongSelf handleResourceError];
            
           } afterDealy:1.0];
            
          // [strongSelf performSelector:@selector(hideProgressHUD) withObject:nil afterDelay:1.0];
        }
        else
        {
         // progress.mode = MBProgressHUDModeText;
         // progress.labelText = @"Loading..";
        
          strongSelf.filePath = downloadPath;
          [strongSelf performSelector:@selector(refresh) withObject:nil];
        }
        
    }];
    
    [request setFailedBlock:^{
        
      DocPreviewController *strongSelf = weakSelf;
       NSLog(@"%@",request.error);
        
    __unused int _errorCode =   [request responseStatusCode];
        
      if(_errorCode>=300&&_errorCode<=302)
      {
           [strongSelf performSelector:@selector(hideProgressHUD) withObject:nil];
          
        
         [strongSelf performBlock:^{
             
          [strongSelf handleResourceError];
             
         } afterDealy:1.0];
          
         
      }
      else
      {
          progress.mode = MBProgressHUDModeText;
          progress.labelText = NETWORK_NOT_AVAILABLE_MSG;
          
          [strongSelf performSelector:@selector(hideProgressHUD) withObject:nil afterDelay:1.0];
      }
        
      
        if(downloadPath)
        {
          [[NSFileManager defaultManager] removeItemAtPath:downloadPath error:nil];
        }
        
       // [strongSelf performSelector:@selector(hideProgressHUD) withObject:nil afterDelay:1.0];
        
        strongSelf.filePath = nil;
    }];

}

-(void)hideProgressHUD
{
    [ProgressHUD hideHUDForView:self.view animated:YES];
}


- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController
{
    NSInteger numToPreview = 0;
    
    /*
    NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
    if (selectedIndexPath.section == 0)
        numToPreview = NUM_DOCS;
    else
        numToPreview = self.documentURLs.count;
    */
    
    return numToPreview;
}


// returns the item that the preview controller should preview
- (id)previewController:(QLPreviewController *)_previewController previewItemAtIndex:(NSInteger)idx
{
    NSURL *fileURL = self.filePath ?  [NSURL fileURLWithPath:self.filePath] : nil;
    
    

    return  (fileURL && [QLPreviewController canPreviewItem:fileURL]) ? fileURL : nil;
}

/*
- (BOOL)previewController:(QLPreviewController *)controller shouldOpenURL:(NSURL *)url forPreviewItem:(id <QLPreviewItem>)item
{
    return NO;
}


- (void)previewControllerWillDismiss:(QLPreviewController *)controller
{
    
}

- (void)previewControllerDidDismiss:(QLPreviewController *)controller
{
    // if the preview dismissed (done button touched), use this method to post-process previews
}
*/




-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
   //CLEARS ALL PASTEBOARD
    NSDictionary *dict = [NSDictionary dictionaryWithObject:@"sandip" forKey:@"name"];
    [UIPasteboard generalPasteboard].items = [NSArray arrayWithObject:dict];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)performAction
{
    NSLog(@"subviews %@ ",previewController.view.subviews);
    
    
    for(UIView *v in previewController.view.subviews)
    {
        
        [v addObserver:self forKeyPath:@"isFirstResponder" options:NSKeyValueObservingOptionNew context:NULL];
        
        NSLog(@"Sub.subviews=%@",v.subviews);
        for(UIView *v2 in v.subviews)
        {
            
            NSLog(@"Gesture recognizers=%@", v2.gestureRecognizers);
            
            //            UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress)];
            //            [v2 addGestureRecognizer:longPressGesture];
            
            NSString *classStr = NSStringFromClass([v2 class]);
            
            if([classStr isEqualToString:@"_UIRemoteView"])
            {
                //v2.hidden = YES;
                
            }
            
        }
    }
    
    // NSLog(@"Gesture recognizers=%@",previewController.view.gestureRecognizers);
    
    
}



-(void)longPress
{
    
}
-(BOOL)canBecomeFirstResponder
{
    return NO;
}

-(void)swipeAction:(UISwipeGestureRecognizer*)gesture
{
    
}

/*
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSLog(@"Dict=%@",change);
    
    NSNumber *num = [change valueForKey:@"new"];
    
    progress.progress = [num floatValue];
    
    [progress setNeedsDisplay];
    
}
*/

@end
