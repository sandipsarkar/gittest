//
//  ResourceCategoryCell.m
//  Fieldforce
//
//  Created by Randem IT on 10/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "ResourceCategoryCell.h"

#define ACCESSORY_NORMAL_IMAGE   @"plus_icon.png"
#define ACCESSORY_SELECTED_IMAGE @"minus_icon.png"

@interface ResourceCategoryCell()
{
    UIImageView *_iconImageView;
    UIView *_lineView;
    UIView *bgView;
  
   
}
@end

@implementation ResourceCategoryCell


-(void)dealloc
{
     OBJC_RELEASE(_categoryNameLabel);
     OBJC_RELEASE(_iconImageView);
     OBJC_RELEASE(_lineView);
     OBJC_RELEASE(bgView);
    
    [super dealloc];
}

+(CGFloat)cellHeight
{
   

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _categoryNameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _categoryNameLabel.font = [UIFont subaruMediumFontOfSize:22.0];
        _categoryNameLabel.textAlignment = NSTextAlignmentLeft;
        _categoryNameLabel.textColor = [UIColor whiteColor];
        _categoryNameLabel.numberOfLines = 2;
        _categoryNameLabel.backgroundColor = [UIColor clearColor];
        _categoryNameLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
        [self.contentView addSubview:_categoryNameLabel];
        
        UIImage *accessoryImage = [UIImage imageNamed:ACCESSORY_NORMAL_IMAGE];
        _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, accessoryImage.size.width, accessoryImage.size.height)];
        _iconImageView.image = accessoryImage;
        _iconImageView.contentMode = UIViewContentModeScaleAspectFit;
        _iconImageView.backgroundColor = [UIColor clearColor];
        // self.accessoryView = _iconImageView;
        [self.contentView addSubview:_iconImageView];
        
        
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.bounds)-1.0, CGRectGetWidth(self.bounds), 1.0)];
        _lineView.backgroundColor = [UIColor whiteColor];
        _lineView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
        [self.contentView addSubview:_lineView];
        
        bgView = [[UIView alloc] initWithFrame:CGRectZero];
        bgView.backgroundColor = LOGIN_BG_COLOR;
        self.backgroundView = bgView;
        
        
        self.backgroundColor = LOGIN_BG_COLOR;

        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.textLabel.backgroundColor = [UIColor clearColor];

    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect _bounds = self.contentView.bounds;
    
    CGRect  _iconImageViewFrame  = _iconImageView.frame;
    _iconImageViewFrame.origin.x = CGRectGetWidth(_bounds)-CGRectGetWidth(_iconImageViewFrame) - 16.0;
    _iconImageViewFrame.origin.y = (CGRectGetHeight(_bounds)-CGRectGetHeight(_iconImageViewFrame))/2.0;
    
    if(!CGRectEqualToRect(_iconImageView.frame, _iconImageViewFrame))
    _iconImageView.frame = _iconImageViewFrame;
    
    
    CGRect _categoryNameLabelBounds = CGRectMake(15.0, 10.0 , CGRectGetMinX(_iconImageViewFrame)-19.0,CGRectGetHeight(_bounds)-2*10.0);
    
   
    if(!CGRectEqualToRect(_categoryNameLabel.frame, _categoryNameLabelBounds))
    _categoryNameLabel.frame = _categoryNameLabelBounds;
    
    //_lineView.frame =CGRectMake(0, CGRectGetHeight(_bounds)-1.0, CGRectGetWidth(_bounds), 1.0);
    
  

}

-(void)isExapnded:(BOOL)isExpanded
{
    //self.backgroundColor = isExpanded ? [UIColor colorWithRed:99.0/255.0 green:100.0/255.0 blue:102.0/255.0 alpha:1.0] : LOGIN_BG_COLOR;
    
    bgView.backgroundColor = isExpanded ? [UIColor colorWithRed:99.0/255.0 green:100.0/255.0 blue:102.0/255.0 alpha:1.0] : LOGIN_BG_COLOR;
    
    _iconImageView.image = [UIImage imageNamed:isExpanded?ACCESSORY_SELECTED_IMAGE:ACCESSORY_NORMAL_IMAGE];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    /*
    self.backgroundColor = selected ? [UIColor darkGrayColor] : LOGIN_BG_COLOR;
    _iconImageView.image = [UIImage imageNamed:selected?ACCESSORY_SELECTED_IMAGE:ACCESSORY_NORMAL_IMAGE];
    */
}

@end
