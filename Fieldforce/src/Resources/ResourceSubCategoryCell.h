//
//  ResourceSubCategoryCell.h
//  Fieldforce
//
//  Created by Randem IT on 10/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResourceSubCategoryCell : UITableViewCell

@property (nonatomic,readonly) UILabel *subCategoryNameLabel;;


-(void)isExapnded:(BOOL)isExpanded;

@end
