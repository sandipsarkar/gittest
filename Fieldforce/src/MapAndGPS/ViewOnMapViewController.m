    //
//  ViewOnMapViewController.m
//  Locator
//
//  Created by SANDIP_ RANDEM on 10/01/11.
//  Copyright 2011 RANDEM IT. All rights reserved.
//

#import "ViewOnMapViewController.h"
#import "Customer.h"
#import "MapConstants.h"
#import "AnnotationObject.h"
#import "OfflineManager.h"

@interface ViewOnMapViewController()
{
    MKMapView *mapView;
    BOOL _isValidLocation;
    
    double latitude;
    double longitude;
    BOOL _isEditMode;
}

-(void)_createMapView;
-(void)showAlertHud;

@end

@implementation ViewOnMapViewController
@synthesize coordinate;

-(id)initWithCutomerInfo:(Customer *)aCustomer
{
	if ((self=[super init]))
	 {
         _customerInfo=[aCustomer retain];
		
		latitude=[_customerInfo.latitude doubleValue];
		longitude=[_customerInfo.longitude doubleValue];
         
         NSLog(@"lat:%lf long:%lf",latitude,longitude);
         
        coordinate=CLLocationCoordinate2DMake(latitude, longitude);
        _isValidLocation=isValidLocation(coordinate);
		
		//NSString *titleStr=[_infoDict objectForKey:TAG_NAME];
		//self.title=[titleStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	 }
	
	return self;
}

-(NSString *)fullAddress
{
    NSMutableArray *arr=[NSMutableArray array];
    
    NSString *add1   = _customerInfo.address;
    NSString *add2   = _customerInfo.address2;
    NSString *suburb = _customerInfo.suburb;
 //   NSString *city   = _customerInfo.city;
    NSString *state  = _customerInfo.state;
    NSString *postCode  = _customerInfo.postCode;
    
    if(add1.length) [arr addObject:add1];
    if(add2.length) [arr addObject:add2];
    //if(city.length) [arr addObject:city];
    
    if(suburb.length) [arr addObject:suburb];
    if(state.length)  [arr addObject:state];
    if(postCode.length)  [arr addObject:postCode];
    
    NSString *addressString=[arr componentsJoinedByString:@","];

    return addressString;
}

-(void)enableEditMode:(BOOL)editMode
{
    _isEditMode=editMode;
}


-(void)_createMapView
{
    if(mapView) return;
    
	mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
    mapView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	mapView.mapType = MKMapTypeStandard;
	mapView.delegate = self;
	mapView.showsUserLocation = NO;
    //mapView.userTrackingMode = MKUserTrackingModeNone;
	[self.view insertSubview:mapView atIndex:0];
}

-(void)setMapToLocation:(CLLocationCoordinate2D)aLocation animated:(BOOL)animated
{
	MKCoordinateRegion region;
	
   @try
   {
		region.center= aLocation;
		region.span=MKCoordinateSpanMake(0.006,0.006);
        region = [mapView regionThatFits:region];
		[mapView setRegion:region animated:animated];
	}
	@catch (NSException * e) 
    {
	    [mapView regionThatFits:region];
    
	}
	@finally
    {
		
	}
}

-(void)reloadMap
{
    NSString *address = [self fullAddress];
    
    /*
    if(NSClassFromString(@"CLGeocoder")!=nil)
    {
      CLGeocoder *forwardGeocoder = [[CLGeocoder alloc] init];
        
        [forwardGeocoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) 
        {
            CLPlacemark *placemark = [placemarks count] ? [placemarks objectAtIndex:0] : nil;
            CLLocationCoordinate2D _loc = placemark.location.coordinate;
            if(placemark && !CLLocationCoordinateIsEqual(_loc, coordinate))
            {
                coordinate = _loc;
                
                NSLog(@"calculated lat:%lf long:%lf",_loc.latitude,_loc.longitude);
                
     _customerInfo.latitude  = [NSString stringWithFormat:@"%lf",_loc.latitude];
     _customerInfo.longitude = [NSString stringWithFormat:@"%lf",_loc.longitude];
                
                NSString *titleStr=_customerInfo.name;
                if(titleStr.length)
                    titleStr=[titleStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                NSArray *annotations=mapView.annotations;
                if([annotations count])
                    [mapView removeAnnotations:annotations];
                
                AnnotationObject *annotObject =[[AnnotationObject alloc] initWithCoordinate:coordinate];
                annotObject.title=titleStr.length?titleStr:@"";
                //annotObject.subtitle=@"Address";
                [mapView addAnnotation:annotObject];
                [annotObject release];
                
                [self setMapToLocation:coordinate animated:YES];
            }
            else if(!placemark.location) //IF THERE IS NO LOCATION
            {
                coordinate = _loc;
                
     _customerInfo.latitude  = [NSString stringWithFormat:@"%lf",_loc.latitude];
     _customerInfo.longitude = [NSString stringWithFormat:@"%lf",_loc.longitude];
                
                NSArray *annotations=mapView.annotations;
                  if([annotations count])
                    [mapView removeAnnotations:annotations];
                
                MKCoordinateRegion region=mapView.region;
                region.span=MKCoordinateSpanMake(0.6,0.6);
                [mapView setRegion:region animated:YES];
               
            }

        }];
     
        [forwardGeocoder release];
        
        return;
    }
   */
      
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        
         CLLocationCoordinate2D _loc = CLLocationCoordinateFromAddress(address);
        
        NSLog(@"latitude=%lf,longitude=%lf",_loc.latitude,_loc.longitude);
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(!CLLocationCoordinateIsEqual(_loc, coordinate))
            {
              coordinate = _loc;

                NSArray *annotations=mapView.annotations;
                if([annotations count])
                [mapView removeAnnotations:annotations];
                
                
                if(isValidLocation(coordinate))
                {
                    _customerInfo.latitude  = [NSString stringWithFormat:@"%lf",_loc.latitude];
                    _customerInfo.longitude = [NSString stringWithFormat:@"%lf",_loc.longitude];

                    
                    NSString *titleStr=_customerInfo.name;
                    if(titleStr.length)
                        titleStr=[titleStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];


                    AnnotationObject *annotObject =[[AnnotationObject alloc] initWithCoordinate:coordinate];
                    annotObject.title=titleStr.length?titleStr:@"";
                    //annotObject.subtitle=@"Address";
                    [mapView addAnnotation:annotObject];
                    [annotObject release];
                    
                    
                }
                else
                {
                    [self showAlertHud];
                }
            
              [self setMapToLocation:coordinate animated:YES];
            }
            //added by sandip on 14 jan
            else if(_loc.longitude==0 || _loc.latitude==0) //IF THERE IS NO LOCATION
            {
                coordinate = _loc;
                
               // _customerInfo.latitude  = [NSString stringWithFormat:@"%lf",_loc.latitude];
               // _customerInfo.longitude = [NSString stringWithFormat:@"%lf",_loc.longitude];
                
                NSArray *annotations = mapView.annotations;
                if([annotations count]) [mapView removeAnnotations:annotations];
                
                MKCoordinateRegion region=mapView.region;
                region.span=MKCoordinateSpanMake(0.6,0.6);
                [mapView setRegion:region animated:YES];
                
                [self showAlertHud];
            }
            else 
            {
                NSString *titleStr=_customerInfo.name;
                if(titleStr.length)
                    titleStr=[titleStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                NSArray *annotations=mapView.annotations;
                if([annotations count])
                    [mapView removeAnnotations:annotations];
                
                AnnotationObject *annotObject =[[AnnotationObject alloc] initWithCoordinate:coordinate];
                annotObject.title=titleStr.length?titleStr:@"";
                //annotObject.subtitle=@"Address";
                [mapView addAnnotation:annotObject];
                [annotObject release];
                
                [self setMapToLocation:coordinate animated:YES];
            }

        });
    });
}

-(void)showAlertHud
{
    if(_customerInfo.name.length && ![[OfflineManager sharedManager] isNetworkAvailable]) //FOR EDIT CUSTOMER
    {
        
        ProgressHUD *hud = [ProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = POOR_CONNECTION_MSG_UNABLE_TO_LOCATE;
        
        [self performSelector:@selector(hideHud) withObject:nil afterDelay:3.0];
        
        // [UIAlertView showWarningAlertWithTitle:@"" message:POOR_CONNECTION_MSG_UNABLE_TO_LOCATE];
    }
}
-(void)hideHud
{
    [ProgressHUD hideHUDForView:self.view animated:YES];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif

    [self addBackButton];
	
	[self _createMapView];
    
	//if(!_isValidLocation) 
	 {
         //[self reloadMap];
         //_isValidLocation = isValidLocation(coordinate);
         
           // [UIAlertView showWarningAlertWithTitle:@"Invalid location!" message:@"Map cannot load invalid location."];
           //  return;
        
	 }

	NSString *titleStr=_customerInfo.name;
    if(titleStr.length)
      titleStr=[titleStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
	AnnotationObject *annotObject=[[AnnotationObject alloc] initWithCoordinate:coordinate];
	annotObject.title=titleStr.length?titleStr:@"";
	//annotObject.subtitle=@"Address";
	[mapView addAnnotation:annotObject];
	[annotObject release];
	[self setMapToLocation:coordinate animated:YES];
    
   // if(!isValidLocation(coordinate))
    [self reloadMap];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //mapView.delegate=self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    //mapView.delegate=nil;
}

-(void)viewDidAppear:(BOOL)animated
{
 
}

#pragma mark mapView delegate
- (void)mapViewWillStartLoadingMap:(MKMapView *)mapView_
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible=YES;

}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView_
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
	
	//NSLog(@"latSpan:%lf longSpan:%lf",mapView_.region.span.latitudeDelta,mapView_.region.span.longitudeDelta);
}


- (void)mapViewDidFailLoadingMap:(MKMapView *)mapView_ withError:(NSError *)error
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
	
}


-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation 
{
	MKAnnotationView *pinView = nil; 
	if(annotation != mV.userLocation) 
	 {
		static NSString *defaultPinID = @"com.fieldforce.pin";
		pinView = (MKPinAnnotationView *)[mV dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
		if ( pinView == nil ) 
			
			//pinView = [[[SubaruAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID] autorelease];
			//pinView.pinColor = MKPinAnnotationColorPurple; 
			//pinView.animatesDrop = YES;
			
			pinView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID] autorelease];
		    pinView.canShowCallout = YES;
		
	 } 
	else 
	 {
		[mV.userLocation setTitle:@"Current Location"];
	 }
	
	
	return pinView;
}


- (void)mapView:(MKMapView *)mapView_ didAddAnnotationViews:(NSArray *)views
{
	//////// Under Development /////////////
	AnnotationObject *annObj=(AnnotationObject *)[[views lastObject] annotation];
	CGPoint point=[mapView convertCoordinate:annObj.coordinate toPointToView:mapView];
	
	
	if(!CGRectContainsPoint(mapView_.frame, point))
	 {
		_isValidLocation=NO;
       //showAlert(MSG_INVALID_LOCATION_HEADING, MSG_INVALID_LOCATION);
		
		return;
	 }
	/////////////////////////////////////////
	
	//[self performSelector:@selector(addCallOutToMap) withObject:nil afterDelay:0.0];
    
    [self addCallOutToMap];
}
#pragma mark -

-(void)addCallOutToMap
{
	if(mapView!=nil)
    {
	 [mapView selectAnnotation:[[mapView annotations] lastObject] animated:YES];
    }
}



- (void)mapView:(MKMapView *)mv regionDidChangeAnimated:(BOOL)animated
{
	//===== Auto resize the callout of the selected annotation according to current region =======
	  NSArray *annotations=[mv selectedAnnotations]; //annotations
	  AnnotationObject *annotation=[annotations count]?[annotations objectAtIndex:0]:nil;
	
	   if(annotation==nil) return;
	   
	   // BOOL isSelected=[annotation isSelected];
	   // if(!isSelected) return;
	
		 [mv deselectAnnotation:annotation animated:NO];
		 [mv selectAnnotation:annotation animated:NO];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}


- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
    
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
	
	[mapView  release];
    mapView=nil;
}


- (void)dealloc 
{
	mapView.delegate=nil;
    [mapView release];
	[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(addCallOutToMap) object:nil];
    [_customerInfo release];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
    [super dealloc];
}


@end
