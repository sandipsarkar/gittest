//
//  AnnotationObject.m
//  Locator
//
//  Created by RANDEM MAC 1 on 12/22/10.
//  Copyright 2010 Randem Group. All rights reserved.
//

#import "AnnotationObject.h"
#import <MapKit/MapKit.h>

@implementation AnnotationObject

@synthesize coordinate, title, subtitle,identifier,tag,isUserLocation;

-(id)initWithCoordinate:(CLLocationCoordinate2D)newCoordinate
{
	if(self=[super init])
	 {
		coordinate=newCoordinate;
		tag=-1;
		identifier=nil;
		
	 }
	
	return self;
}

-(BOOL)isUserLocation
{
	return ([self isKindOfClass:[MKUserLocation class]]);
	
}

-(void)dealloc
{
	[title release];
	[subtitle release];
	[identifier release];
	[super dealloc];
}

@end
