
//
//  ViewOnMapViewController.h
//  Locator
//
//  Created by SANDIP_ RANDEM on 10/01/11.
//  Copyright 2011 RANDEM IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class Customer;
@interface ViewOnMapViewController : UIViewController<MKMapViewDelegate> 
{
	
    Customer *_customerInfo;
}

@property(nonatomic,readonly) CLLocationCoordinate2D coordinate;

-(id)initWithCutomerInfo:(Customer *)aCustomer;
-(void)enableEditMode:(BOOL)editMode;

-(void)reloadMap;

@end


