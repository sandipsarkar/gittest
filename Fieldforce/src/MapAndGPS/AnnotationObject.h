//
//  AnnotationObject.h
//  Locator
//
//  Created by RANDEM MAC 1 on 12/22/10.
//  Copyright 2010 Randem Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>

@interface AnnotationObject : NSObject<MKAnnotation>
{
	int tag;
	BOOL isUserLocation;
}

@property (nonatomic,assign)  CLLocationCoordinate2D coordinate;
@property(nonatomic,copy)    NSString* title;
@property(nonatomic,copy)    NSString* subtitle;
@property(nonatomic,retain)  NSString *identifier;

@property(nonatomic,assign)   int tag;
@property(nonatomic,readonly) BOOL isUserLocation;



-(id)initWithCoordinate:(CLLocationCoordinate2D)newCoordinate;


@end
