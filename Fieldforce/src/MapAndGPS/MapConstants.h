/*
 *  MapConstants.h
 *  Locator
 *
 *  Created by SANDIP_ RANDEM on 25/02/11.
 *  Copyright 2011 RANDEM IT. All rights reserved.
 *
 */
//#import <MapKit/MKAnnotation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

#define MKCoordinateRegionNull MKCoordinateRegionMake(CLLocationCoordinateMake(0 ,0), MKCoordinateSpanMake(0, 0))

#pragma mark Creates a new  coordinate
static CLLocationCoordinate2D CLLocationCoordinateMake(double latitude ,double longitude)
{
	CLLocationCoordinate2D coordinate;
	coordinate.latitude=latitude;
	coordinate.longitude=longitude;
	
	return coordinate;
}

#pragma mark Checks a latitude is valid
static BOOL isValidLatitude(NSString *latitudeStr)
{
	if(!latitudeStr.length) return NO;
	
	double latitude=[latitudeStr doubleValue];
	return ((latitude > -90.0f && latitude < 90.0f)  && latitude!=0.0f);
}

#pragma mark Checks a longitude is valid
static BOOL isValidLongitude(NSString *longitudeStr)
{
	if(!longitudeStr.length) return NO;
	
	double longitude=[longitudeStr doubleValue];
	return ((longitude > -180.0f && longitude < 180.0f) && longitude!=0.0f);
}

#pragma mark Checks two coordinates are equal
static BOOL CLLocationCoordinateIsEqual(CLLocationCoordinate2D coordinate1,CLLocationCoordinate2D coordinate2)
{
	return (coordinate1.latitude==coordinate2.latitude && coordinate1.longitude==coordinate2.longitude);
}

#pragma mark Checks two regions are equal
static BOOL MKCoordinateRegionIsEqual(MKCoordinateRegion reg1,MKCoordinateRegion reg2)
{
	MKCoordinateSpan span1=reg1.span;
	MKCoordinateSpan span2=reg2.span;
	
	return (CLLocationCoordinateIsEqual(reg1.center, reg2.center) && (span1.latitudeDelta== span2.latitudeDelta && span1.longitudeDelta==span2.longitudeDelta));
	
}
#pragma mark -

#pragma mark Checks a lcation is valid or not
static BOOL isValidLocation(CLLocationCoordinate2D coordinate)
{
	BOOL isValidLat=isValidLatitude([NSString stringWithFormat:@"%lf",coordinate.latitude]);
	BOOL isValidLong=isValidLongitude([NSString stringWithFormat:@"%lf",coordinate.longitude]);
	
	return (isValidLat && isValidLong);
}


#pragma mark Lat,Long from Address
/*
static CLLocationCoordinate2D CLLocationCoordinateFromAddress(NSString *address)
{
    int code = -1;
    int accuracy = -1;
    double latitude = 0.0f;
    double longitude = 0.0f;
    CLLocationCoordinate2D center;
    
    // setup maps api key
   static NSString * MAPS_API_KEY = @"ABQIAAAA5RjSVrsevFq8jMAcUU5OARRWaoTxLlFNL4vPJcJseucdH4gE8RRPajZwzJZ6iYF5ddkxpxhoPg18Ww";
    
    NSString *escaped_address =  [address stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    
    NSLog(@"Address=%@",escaped_address);
    
    // Contact Google and make a geocoding request
    NSString *requestString = [NSString stringWithFormat:@"http://maps.google.com/maps/geo?q=%@&output=csv&key=%@&sensor=false", escaped_address,MAPS_API_KEY];
    
  //@"http://maps.google.com/maps/geo?q=%@&output=csv&oe=utf8&key=%@&sensor=false&gl=it"
    
    NSURL *url = [NSURL URLWithString:requestString];
    
    NSString *result = [NSString stringWithContentsOfURL: url encoding: NSUTF8StringEncoding error:NULL];
    if(result)
    {
        // we got a result from the server, now parse it
        NSScanner *scanner = [NSScanner scannerWithString:result];
        [scanner scanInt:&code];
        if(code == 200)
        {
            // everything went off smoothly
            [scanner scanString:@"," intoString:nil];
            [scanner scanInt:&accuracy];
            
            //NSLog(@"Accuracy: %d", accuracy);
            
            [scanner scanString:@"," intoString:nil];
            [scanner scanDouble:&latitude];
            [scanner scanString:@"," intoString:nil];
            [scanner scanDouble:&longitude];
            
            
            center.latitude = latitude;
            center.longitude = longitude;
            
            return center;

        }
        else
        {
            center.latitude = 0.0f;
            center.longitude = 0.0f;
            
            return center;
        }
        
    }
    else
    {
        center.latitude = 0.0f;
        center.longitude = 0.0f;
        
        return center;
    }
    

  center.latitude = 0.0f;
  center.longitude = 0.0f;
  return center;

}
*/

static CLLocationCoordinate2D CLLocationCoordinateFromAddress(NSString *address)
{
   //const int code = -1;
   //const int accuracy = -1;
    
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(0.0, 0.0);
    
    NSString *escaped_address =  [address stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    
    NSLog(@"Address=%@",escaped_address);
    
    // Contact Google and make a geocoding request
    NSString *requestString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=%@", escaped_address];
    
    NSURL *url = [NSURL URLWithString:requestString];
    
    NSString *result = [NSString stringWithContentsOfURL: url encoding: NSUTF8StringEncoding error:NULL];
    if(result)
    {
       NSDictionary *resultDict =  [result JSONValue];
        
       NSString *status = [resultDict valueForKey:@"status"];
        
      if(resultDict && [status isEqualToString:@"OK"])
      {
         NSDictionary *locationDict = [resultDict valueForKeyPath:@"results.geometry.location"];
        
          NSArray *latNumArr  = [locationDict valueForKey:@"lat"];
          NSArray *longNumArr = [locationDict valueForKey:@"lng"];
          
          for(NSNumber *latNum in latNumArr)
          {
              NSLog(@"Latitude =%@",latNum);
              center.latitude = [latNum doubleValue];
              break;
          }
          
          for(NSNumber *longNum in longNumArr)
          {
              NSLog(@"Longitude =%@",longNum);
              center.longitude = [longNum doubleValue];
              break;
          }
          
        return center;
      }
      else
      {
          return center;
      }
    }
    else
    {
  
        return center;
    }
    
    
    center.latitude = 0.0f;
    center.longitude = 0.0f;
    return center;
    
}

