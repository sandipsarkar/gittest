//
//  Start_EndDate.m
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Start_EndDate.h"
#import "InsetTableViewCell.h"


#define DATE_INTERVAL 60.0*60.0*1
#define MAX_DATE_INTERVAL 60.0*60.0*24.0

@interface Start_EndDate()<InsetTableViewCellDelegate>

@property(nonatomic,retain) NSString *startDate;
@property(nonatomic,retain) NSString *endDate;
@property(nonatomic,copy) DoneActionCallback doneActionCallback;
@property(nonatomic,copy) DateDidChangeCallback dateDidChangeCallback;
@end

@implementation Start_EndDate
@synthesize aTableview;
@synthesize startDate,endDate;
@synthesize delegate = _delegate;
@synthesize isFromCustomer;

@synthesize selectedStartDate,selectedEndDate;
@synthesize doneActionCallback;
@synthesize dateDidChangeCallback;

- (id)initWithStartDate:(NSDate *)aDate endDate:(NSDate *)aEndDate
{
    self = [super init];
    
    if (self)
    {
        //self.selectedStartDate= aDate ? aDate :[NSDate date];
        //self.selectedEndDate = aEndDate ? aEndDate : [self.selectedStartDate dateByAddingTimeInterval:60*60*1];
        
        NSDate *_startDate = aDate ? aDate : [[NSDate date] dateByAddingTimeInterval:60*1];
        NSDate *_endDate   = aEndDate ? aEndDate : [_startDate dateByAddingTimeInterval:DATE_INTERVAL];
        
        self.selectedStartDate = [_startDate currentDateHourMinute];
        self.selectedEndDate   = [_endDate currentDateHourMinute];

    }
    return self;
}

-(void)addDoneActionCallback:(DoneActionCallback )callback
{
    if(callback)
        self.doneActionCallback = callback;
}

-(void)addDateDidChangeCallback:(DateDidChangeCallback)callback
{
    if(callback)
        self.dateDidChangeCallback = callback;
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    
    [datePicker release];
    [aTableview release];
    aTableview=nil;
    [startDate release];
    startDate=nil;
    [endDate release];
    endDate=nil;
    [selectedStartDate release];
    selectedStartDate=nil;
    [selectedEndDate release];
    selectedEndDate =nil;
    
    if(doneActionCallback)
        [doneActionCallback release];
    
    doneActionCallback = nil;
    
    if(dateDidChangeCallback)
        [dateDidChangeCallback release];
    
    dateDidChangeCallback = nil;
    
    
    [super dealloc];
    
    
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.contentSizeForViewInPopover=CGSizeMake(335+15.0, 325);
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //float _height=CGRectGetMaxY(self.aTableview.frame);
    self.contentSizeForViewInPopover=CGSizeMake(335+15.0, 325);
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_selectedTableRow inSection:0];
    [self.aTableview selectRowAtIndexPath:indexPath animated:YES scrollPosition: UITableViewScrollPositionNone];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.title=@"Start & End";
    TableCell=nil;
    wrongSelection=NO;
    [self.navigationItem setHidesBackButton:YES animated:YES];
    
    NSString *dateFormatString = @"EEE dd MMM, yyyy, h:mm a";
    self.startDate =[self.selectedStartDate stringFromDateWithFormat:dateFormatString];
    self.endDate   =[self.selectedEndDate stringFromDateWithFormat:dateFormatString];
    
    CGRect _rectGrouped=CGRectMake(0,0, self.view.bounds.size.width, 400);
    UITableView *_aTableview=[[UITableView alloc] initWithFrame:_rectGrouped style:UITableViewStyleGrouped];
    _aTableview.delegate=self;
    _aTableview.dataSource=self;
    _aTableview.showsHorizontalScrollIndicator=NO;
    _aTableview.showsVerticalScrollIndicator=NO;
    _aTableview.scrollEnabled=NO;
    //_aTableview.rowHeight=40;
    _aTableview.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:_aTableview];
    self.aTableview=_aTableview;
    [_aTableview release];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.aTableview.sectionFooterHeight = 1.0;
        self.aTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.aTableview.bounds), 10.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        self.aTableview.tableHeaderView = _headerView;
        [_headerView release];
    }

    
    [self setupTableFooterView];
    
    if(!isFromCustomer)
    {
      UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
     [btnCancel setImage:[UIImage imageNamed:@"cancel_button.png"] forState:UIControlStateNormal];
     btnCancel.frame =CGRectMake(0, 0, 60, 29);
     [btnCancel addTarget:self
                  action:@selector(cancelPopOver:) 
        forControlEvents:UIControlEventTouchUpInside];
    
     UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc]initWithCustomView:btnCancel];
	 self.navigationItem.leftBarButtonItem = barButtonCancel;
	 [barButtonCancel release];
    }
    
    
    UIButton *btnDone= [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone setImage:[UIImage imageNamed:@"done_button.png"] forState:UIControlStateNormal];
    btnDone.frame =CGRectMake(0, 0, 60, 29);
    [btnDone addTarget:self 
                action:@selector(saveChanges:) 
      forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithCustomView:btnDone];
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
    
    
     _selectedTableRow=0;
    
  /*
SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")
    [self performBlock:^{
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_selectedTableRow inSection:0];
        [self.aTableview selectRowAtIndexPath:indexPath animated:NO scrollPosition: UITableViewScrollPositionNone];
        
    } afterDealy:1.0];
#else
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_selectedTableRow inSection:0];
    [self.aTableview selectRowAtIndexPath:indexPath animated:NO scrollPosition: UITableViewScrollPositionNone];
    
#endif

  */
    
}

-(void)setupTableFooterView
{
    /* Adiing table footerview */
    float _footerHeight=250.0f;
    float _posX=0.0;
    float _posY=0.0;
    
    UIView *tableFooterView=[[UIView alloc] initWithFrame:CGRectMake(_posX, _posY, CGRectGetWidth(self.aTableview.frame), _footerHeight)];
    tableFooterView.backgroundColor=[UIColor clearColor];
    //tableFooterView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    /*
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    NSLog(@"self.startDate=%@",self.startDate);
    [formatter setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
    NSDate *_date  = [formatter dateFromString:self.startDate?self.startDate: [NSDate date]];
    NSLog(@"_date=%@",_date);
    [formatter release];
    */
    
    datePicker = [[UIDatePicker alloc] initWithFrame:tableFooterView.bounds];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    datePicker.minimumDate=[[NSDate date] dateByAddingTimeInterval:60*1];
    datePicker.date = self.selectedStartDate;
    datePicker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [datePicker addTarget:self
                   action:@selector(dateChanges:)
         forControlEvents:UIControlEventValueChanged];  //LabelChange
    [tableFooterView addSubview:datePicker];
    
    
    self.aTableview.tableFooterView=tableFooterView;
    
    [tableFooterView release];
}

-(void)dateChanges:(UIDatePicker *)_datePicker
{
    NSDate *selectedDate=_datePicker.date;
    NSDate *_dateWithZeroSeconds= selectedDate ? [selectedDate currentDateHourMinute] : nil;
    
    if(_selectedTableRow==0) // START DATE SELECTED
    {
       // self.selectedStartDate= selectedDate;
       // self.selectedEndDate = [selectedDate dateByAddingTimeInterval:60*60*1];
        
        self.selectedStartDate = _dateWithZeroSeconds;
        
        self.selectedEndDate   = [_dateWithZeroSeconds dateByAddingTimeInterval:60*60*1];
        
        /*
        //==============================================================================
        if(self.selectedEndDate)
        {
              NSTimeInterval interval = [self.selectedEndDate timeIntervalSinceDate:_dateWithZeroSeconds];
            
             if(interval>0 && (interval >=DATE_INTERVAL && interval<=MAX_DATE_INTERVAL))
             {
                 //self.selectedEndDate = _dateWithZeroSeconds;
             }
             else
             {
                 self.selectedEndDate   = [_dateWithZeroSeconds dateByAddingTimeInterval:DATE_INTERVAL];

             }
        }
        else
        {
            self.selectedEndDate   = [_dateWithZeroSeconds dateByAddingTimeInterval:DATE_INTERVAL];
        }
        //==================================================================================
        */
        wrongSelection=NO;
    }
    else // END DATE SELECTED
    {
         NSTimeInterval interval= [_dateWithZeroSeconds timeIntervalSinceDate:self.selectedStartDate];
        
        // IF INTERVAL IS POSITIVE(LATER THAN START DATE) AND NOT MORE THAN 24 HOURS
        if(interval>0 && interval<=MAX_DATE_INTERVAL)
        {
           // self.selectedEndDate = selectedDate;
            self.selectedEndDate = _dateWithZeroSeconds;
            wrongSelection=NO;
        }
        else 
        {
            wrongSelection=YES;
        }
    }
    
    NSString *dateFormatString = @"EEE dd MMM, yyyy, h:mm a";
    self.startDate =[self.selectedStartDate stringFromDateWithFormat:dateFormatString];
    self.endDate   =[self.selectedEndDate stringFromDateWithFormat:dateFormatString];
   
    [self.aTableview reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    
    
    NSLog(@"Selected start date=%@  selectedEndDate=%@",self.selectedStartDate,self.selectedEndDate);
    
    if(self.dateDidChangeCallback)
        self.dateDidChangeCallback(self.selectedStartDate,self.selectedEndDate);

}

/*
- (void)LabelChange:(id)sender
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
    NSLog(@"datePicker.date=%@",datePicker.date);
    
    

    if(_selectedTableRow==0)
    {
        self.startDate = [df stringFromDate:datePicker.date];
        //NSLog(@"startDate=%@",self.startDate);
        
        
        [df setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
        NSDate *d1=[df dateFromString:self.startDate];
        NSLog(@"d1=%@",d1);
        NSDate *d2=[df dateFromString:self.endDate];
        NSLog(@"d2=%@",d2);
        
        
        [df setDateFormat:@"dd"];
        NSString *strDate1=[df stringFromDate:d1];
        NSLog(@"strDate1=%@",strDate1);
        
        
        NSString *strDate2=[df stringFromDate:d2];
        NSLog(@"strDate2=%@",strDate2);
        if([strDate1 isEqualToString:strDate2])
        {
            [df setDateFormat:@"h:mm a"];
            d2=[NSDate dateWithTimeInterval:60*60 sinceDate:d1];
            NSLog(@"d2=%@",d2);
            [df setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
            self.endDate=[df stringFromDate:d2];
            //NSLog(@"self.endDate=%@",self.endDate);
        }
        
    }
    else
    {
        self.endDate = [df stringFromDate:datePicker.date];
        //NSLog(@"self.endDate=%@",self.endDate);
    }
    
    [df setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
    NSLog(@"startDate=%@",self.startDate);
    NSLog(@"self.endDate=%@",self.endDate);
    NSDate *_date1  = [df dateFromString:self.startDate];
    NSDate *_date2  = [df dateFromString:self.endDate];
    NSComparisonResult result=[_date1 compare:_date2];
    switch (result)
    {
        case NSOrderedAscending:
        {
            wrongSelection=NO;
            NSLog(@"%@ is before %@", self.startDate, self.endDate); 
            break;
        }
        case NSOrderedDescending:
        {
            wrongSelection=YES;
            NSLog(@"%@ is after %@", self.startDate, self.endDate); 
            break;
        }
        case NSOrderedSame:
        {
            wrongSelection=NO;
            NSLog(@"%@ is same %@", self.startDate, self.endDate);
            break;
        }
        default:
        {
            NSLog(@"erorr dates %@, %@", self.startDate, self.endDate);
            break;
        }
    }
    
    [df release];
    [self.aTableview reloadData];
                      
}
 */

-(void)cancelPopOver:(id)sender
{
    if([self.delegate respondsToSelector:@selector(selectedStartDate: endDate:)])
        [self.delegate selectedStartDate:nil endDate:nil];
    
    TableCell=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)saveChanges:(id)sender
{
    if(wrongSelection)
    {
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle:CANNOT_SAVE_REMINDER_HEADER 
                              message:CANNOT_SAVE_REMINDER_MSG 
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil, 
							  nil];
		[alert show];
		[alert release];
    }
    else
    {
        if(self.doneActionCallback)
            self.doneActionCallback(self.selectedStartDate,self.selectedEndDate);
        
        if([self.delegate respondsToSelector:@selector(selectedStartDate: endDate:)])
            [self.delegate selectedStartDate:self.startDate endDate:self.endDate];
        
        TableCell=nil;
        if(!isFromCustomer)
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return 2;
}



- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) 
    {
        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType=UITableViewCellAccessoryNone;
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
        //cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }


    switch (indexPath.row)
    {
        case 0:
        {
            cell.textLabel.text=@"Date";
            
            if(self.startDate!=nil) cell.detailTextLabel.text=self.startDate;
        }
        break;
    
        case 1:
        {
            cell.textLabel.text=@"Ends";
            
            if(self.endDate!=nil)
            {
                 cell.detailTextLabel.text=self.endDate;
            }
        }
        break;
        default:break;
    }
    
    
   #ifndef __IPHONE_7_0
    if(_selectedTableRow==indexPath.row)
    {
        NSIndexPath *_indexPath = [NSIndexPath indexPathForRow:_selectedTableRow inSection:0];
        [aTableView selectRowAtIndexPath:_indexPath animated:NO scrollPosition: UITableViewScrollPositionNone];
    }
   #endif
    
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    InsetTableViewCell *_cell = (InsetTableViewCell *)cell;
    _cell.delegate = self;
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    
    
    CellBackgroundView *_selectedBgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _selectedBgView.backgroundColor = [UIColor clearColor];
    _selectedBgView.contentInset = CGPointMake(10.0, 0.0);
    _selectedBgView.cornerRadius = 8.0;
    _selectedBgView.borderColor = tableView.separatorColor;
    _selectedBgView.fillColor = [UIColor cellSelectedColorBlue];
    
    
    NSInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    _selectedBgView.position = _bgView.position;
    
    cell.backgroundView = _bgView;
    cell.selectedBackgroundView = _selectedBgView;
    
    [_bgView release];
    [_selectedBgView release];
    
    
    //cell.selected =(indexPath.row==_selectedTableRow);
    
    
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    TableCell = [tableView cellForRowAtIndexPath:indexPath];
   // [tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:_selectedTableRow inSection:0] animated:NO];
    
    _selectedTableRow=indexPath.row;
    
    
    switch (indexPath.row)
    {
        case 0:
        {
            /*
            NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
    
            NSDate *_date  = [formatter dateFromString:self.startDate];
            NSLog(@"_date=%@",_date);
            datePicker.date = _date;
            [formatter release];
             */
            
              datePicker.minimumDate = [[NSDate date] dateByAddingTimeInterval:60*1];
              datePicker.maximumDate =  nil;
              datePicker.date = self.selectedStartDate;
          
        }
        break;
            
        case 1:
        {
            /*
            NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
            
            NSDate *_date  = [formatter dateFromString:self.endDate];
            NSLog(@"_date=%@",_date);
            datePicker.date = _date;
            [formatter release];
             */
            
           // datePicker.minimumDate=self.selectedEndDate;
            
            datePicker.minimumDate=[self.selectedStartDate dateByAddingTimeInterval:60*1];

            datePicker.maximumDate=[self.selectedStartDate dateByAddingTimeInterval:MAX_DATE_INTERVAL];
            datePicker.date = self.selectedEndDate;
           
        }
        break;
        default:
            break;
    }
}

#pragma mark - InsetTableViewCellDelegate


-(void)insetTableViewCell:(InsetTableViewCell *)cell didSelect:(BOOL)selected
{
   // CellBackgroundView *_bgView = (CellBackgroundView *) cell.backgroundView;
    
   // _bgView.fillColor = selected  ? [UIColor cellSelectedColorBlue] : [UIColor whiteColor];
   // [_bgView setNeedsDisplay];
    
    cell.textLabel.textColor = selected ? [UIColor whiteColor] : [UIColor blackColor];
    cell.detailTextLabel.textColor = selected ? [UIColor whiteColor] : [UIColor blackColor];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
