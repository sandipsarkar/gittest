//
//  CustomEventStore.h
//  OEM_CALENDAR
//
//  Created by elie maalouly on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EventKit/EventKit.h>

@interface CustomEventStore : NSObject
@property (nonatomic, retain) EKEventStore *eventStore;

+ (CustomEventStore *)sharedInstance;

+ (NSArray *)getEventsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate;
+(NSArray *)getEventsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate fromCalender:(EKCalendar *)calender;

-(EKCalendar *)customCalendarWithIdentifier:(NSString *)indentifier title:(NSString *)title;
-(BOOL)deleteCustomCalendarWithIdentifier:(NSString *)indentifier;
+(BOOL)deleteEventForEventIdentifier:(NSString *)eventIdentifier;

+(EKEvent *)addEventWithTitle:(NSString *)title startDate:(NSDate *)startDate endDate:(NSDate *)endDate location:(NSString *)location notes:(NSString *)notes;

+(EKEvent *)editEventForIdentifier:(NSString *)eventIdentifier withTitle:(NSString *)eventTitle startDate:(NSDate *)startDate endDate:(NSDate *)endDate location:(NSString *)location notes:(NSString *)notes;

+(void)deleteAllEvents:(void (^)(void))callback;

+(EKReminder *)addReminder:(Reminder *)reminder;

+(EKReminder *)editReminderForIdentifier:(NSString *)eventIdentifier reminder:(Reminder *)reminder;

@end
