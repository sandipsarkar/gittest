//
//  TaskListViewController.m
//  OEM_CALENDAR
//
//  Created by elie maalouly on 6/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TasksInCalenderController.h"
#import <EventKit/EventKit.h>
#import "Task.h"
#import "Customer.h"
#import "TaskDetailViewController.h"
#import "Constant.h"

#define CELL_HEIGHT 47.0

@interface TasksInCalenderController()

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
-(void)launchWithFirstRowSelected;

@end

@implementation TasksInCalenderController
@synthesize taskList;
@synthesize delegate = _delegate;
@synthesize calenderView;
@synthesize tableView=_tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
- (void)dealloc
{
    [taskList release];
    [_tableView release];
    calenderView=nil;
    [super dealloc];
}

#pragma mark - View lifecycle

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.contentSizeForViewInPopover=CGSizeMake(335, self.tableView.contentSize.height);
    self.popoverController.popoverContentSize=CGSizeMake(335+15.0, self.tableView.contentSize.height);
    
    [self launchWithFirstRowSelected];
   
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden=YES;
    self.contentSizeForViewInPopover=CGSizeMake(335+15.0, self.tableView.contentSize.height-1.0);
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    CGRect _rect=self.view.bounds;
    
    UITableView *aTableview=[[UITableView alloc] initWithFrame:_rect style:UITableViewStylePlain];
    aTableview.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    aTableview.delegate=self;
    aTableview.dataSource=self;
    aTableview.bounces = NO;
    [self.view addSubview:aTableview];
    self.tableView=aTableview;
    [aTableview release];
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    aTableview.backgroundColor = [UIColor whiteColor];

}

-(void)setupTableHeaderView
{
    
}

-(void)launchWithFirstRowSelected
{
    if([self.taskList count] == 1)
    {
        NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    }
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count =[self.taskList count];
    return count;
}


- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    
    UITableViewCell *cell =(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType=UITableViewCellAccessoryNone;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        UIButton *accessoryButton=[UIButton buttonWithType:UIButtonTypeCustom];
        accessoryButton.frame=CGRectMake(0, 0, 60, 29);
        [accessoryButton setBackgroundImage:[UIImage imageNamed:@"edit_button.png"] forState: UIControlStateNormal];
		accessoryButton.titleLabel.font = [UIFont helveticaNeueLTStdBDOfSize:CALENDAR_VIEW_TEXT_FONT_SIZE];
		[accessoryButton setTitle:CALENDAR_VIEW_TEXT forState:UIControlStateNormal];
		accessoryButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [accessoryButton addTarget:self action:@selector(editTask:) forControlEvents:UIControlEventTouchUpInside];
        [cell setAccessoryView:accessoryButton];
    }
    
    int taskCount = [self.taskList count];
    Task *event = indexPath.row < taskCount ? [self.taskList objectAtIndex:indexPath.row] : nil;
    
    NSString *_detailText= (event.parentCustomer) ? event.parentCustomer.name : (event.customerName) ? event.customerName : [event.dueDate stringFromDateWithFormat:@"h:mm a"];

    cell.imageView.image = [UIImage imageNamed:@"red_dot.png"];
    cell.textLabel.text  = event.subject;
    cell.detailTextLabel.text = _detailText;
    cell.detailTextLabel.textColor=[UIColor blackColor];

    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return;
    
    NSArray *arr = [self taskList];
    
  __unused  BOOL isSingleRow =[arr count]==1 ;
    
    Task *_task = (indexPath.row < [arr count]) ?  [arr objectAtIndex:indexPath.row] :nil;
    
    if(_task)
    {
        /*
        TaskDetailViewController *_taskDetailViewController=[[TaskDetailViewController alloc] initWithTask:_task];
        _taskDetailViewController.shouldShowCustomerProfile = NO;
        
        [_taskDetailViewController addDeleteTaskCallback:^(Task *_task)
         {
             [SSCoreDataManager deleteObject:_task];
             
             NSError *error=nil;
             [[SSCoreDataManager sharedManager] save:error];
             if(!error)
             {
                 //if([self.taskList containsObject:_task])
                    // [self.taskList removeObject:_task];
                 
                 //NEED TOBE OPTIMIZED
                 [self.calenderView setNeedsLayout];
                 [self.popoverController dismissPopoverAnimated:YES];
             }
         }];

        [self.navigationController pushViewController:_taskDetailViewController animated:!isSingleRow];
        _taskDetailViewController.navigationController.navigationBarHidden=isSingleRow;
        [_taskDetailViewController release];
        */
        
        /*
        CreateTaskViewController *taskViewController=[[CreateTaskViewController alloc] initWithTask:_task];
        taskViewController.title = @"EDIT TASK";
        taskViewController.contentSizeForViewInPopover=CGSizeMake(320.0, 320.0);
        
        [self.navigationController pushViewController:taskViewController animated:NO];
       // UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:taskViewController];
       // navController.navigationBarHidden=YES;
        
        [taskViewController addSaveTaskCallback:^(Task *_task) 
         {
             NSError *error=nil;
             [[SSCoreDataManager sharedManager] save:error];
             
             if(!error)
             {
                 //[self loadDataFromTask:_task];
                 //[self.tableView reloadData]; 
                 [self.calenderView setNeedsLayout];
                 [self.popoverController dismissPopoverAnimated:YES];
             }
             
             //[[self.parallelStackViewController.masterController tableView] reloadData];
             
         }];
        [taskViewController release];
       // [navController release];
        */
        
        //NSLog(@"indexPath.row=%d",indexPath.row);
    if([self.delegate respondsToSelector:@selector(selectedTaskForEdit:)])
       [self.delegate selectedTaskForEdit:indexPath.row];
        
    }

}

-(void)viewTask:(id)sender
{
    ;
}

-(void)editTask:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    UITableViewCell *cell = (UITableViewCell *)[btn superViewOfType:[UITableViewCell class]];
    NSIndexPath *indexPath = [[self tableView] indexPathForCell:cell];
    
    if([self.delegate respondsToSelector:@selector(selectedTaskForEdit:)])
        [self.delegate selectedTaskForEdit:indexPath.row];
    
    [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
