//
//  ReminderListViewController.h
//  OEM_CALENDAR
//
//  Created by elie maalouly on 6/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EditReminderDelegate <NSObject>
@required
- (void)selectedReminderForEdit:(int)index;
@end
@interface ReminderListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    id <EditReminderDelegate> _delegate;
}
@property(nonatomic, assign) id <EditReminderDelegate> delegate;
@property(nonatomic,retain)NSArray *reminderList;
@property(nonatomic,retain)UITableView *tableView;
@end
