//
//  ADDReminder.m
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ADDReminder.h"
#import "DatePickerController.h"
#import "OfflineManager.h"
#import "Reminder.h"
#import "CoreDataHandler.h"
#import "InsetTableViewCell.h"


@interface ADDReminder()<InsetTableViewCellDelegate>

-(void)showDatePicker:(NSIndexPath *)indexPath;

@property(nonatomic,retain)UIPopoverController *datePickerPopover;
@property(nonatomic,copy) SaveReminderCallback saveReminderCallback;
@end

@implementation ADDReminder
@synthesize delegate = _delegate;
@synthesize selectedIndexPath=_selectedIndexPath;
@synthesize _tableView;
@synthesize _startDate,_endDate;
@synthesize _alert;
@synthesize _secondAlert;
@synthesize _repeat;
@synthesize _endRepeat;
@synthesize txtTittle;
@synthesize reminderId;
@synthesize reminderTittle;
@synthesize datePickerPopover;
@synthesize saveReminderCallback;
@synthesize reminder;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    [reminder release];
    
    [_arrRepeatList release];
    //[arrayofCalIDs release];
    
    [txtTittle release];
    txtTittle=nil;
    
    [_selectedIndexPath release];
    _selectedIndexPath=nil;
    
    [_tableView release];
    _tableView=nil;
    
    [_startDate release];
    _startDate=nil;
    
    [_endDate release];
    _endDate=nil;
    
    [_alert release];
    _alert=nil;
    
    [_secondAlert release];
    _secondAlert=nil;
    
    [_repeat release];
    _repeat=nil;
    
    [_endRepeat release];
    _endRepeat=nil;
    
    [reminderId release];
    reminderId=nil;
    
    [reminderTittle release];
    reminderTittle=nil;
    
    [datePickerPopover release];
    datePickerPopover=nil;
    
    [saveReminderCallback release] ;
    saveReminderCallback=nil;


    [super dealloc];
}

#pragma mark - View lifecycle


-(UIView *)datePickerFooterView
{
    /* Adiing table footerview */
    float _footerHeight=250.0f;
    float _posX=0.0;
    float _posY=(_footerHeight-200.0)/2.0;
    
    UIView *tableFooterView=[[UIView alloc] initWithFrame:CGRectMake(_posX, _posY, CGRectGetWidth(self._tableView.frame), _footerHeight)];
    tableFooterView.backgroundColor=[UIColor clearColor];
    tableFooterView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
   NSDate *_date  = [NSDate dateFromString:self._startDate withFormat:@"EEE dd MMM, yyyy, h:mm a"];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:tableFooterView.bounds];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    datePicker.hidden = NO;
    datePicker.date = _date;
    datePicker.autoresizingMask =UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    [datePicker addTarget:self
                   action:@selector(dateDidChange:)
         forControlEvents:UIControlEventValueChanged];
    [tableFooterView addSubview:datePicker];
    [datePicker release];
    
    
    return [tableFooterView autorelease];
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif
    
    NSLog(@"Reminder ID=%@",self.reminderId);
    
     [self addTitle:self.reminderId ? @"EDIT REMINDER" : @"NEW REMINDER" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    _arrRepeatList=[[NSMutableArray alloc] init];
    [_arrRepeatList addObject:@"Never"];
    [_arrRepeatList addObject:@"Daily"];
    [_arrRepeatList addObject:@"Weekly"];
    [_arrRepeatList addObject:@"Biweekly"];
    [_arrRepeatList addObject:@"Monthly"];
    [_arrRepeatList addObject:@"Yearly"];
    
    //arrayofCalIDs=[[NSMutableArray alloc] init];
    if(self._alert==nil)
        //self._alert=@"None";
    self._alert=@"At time of event"; // By default
    
    if(self._secondAlert==nil)
        self._secondAlert=@"None";
    if(self._repeat==nil)
        self._repeat=@"Never";
    if(self._endRepeat==nil)
        self._endRepeat=@"Never";
    
    if(self._startDate==nil && self._endDate==nil)
    {
        NSDate* date = [NSDate date];
        
        //Create the dateformatter object
        
    
       self._startDate =  [date stringFromDateWithFormat:@"EEE dd MMM,"];
    
        NSString *myYearString = [date stringFromDateWithFormat:@"yyyy"];;
        NSLog(@"myYearString=%@",myYearString);
        self._startDate=[self._startDate stringByAppendingFormat:@" %@",myYearString];
        NSLog(@"self._startDate=%@",self._startDate);
        
        float time=[[date stringFromDateWithFormat:@"h.mm"] floatValue];
        int abs=ceilf(time);
        abs=(abs!=12)?abs%12:abs;
        
        NSString *localString=[NSString stringWithFormat:@"%d:00",abs];
        self._startDate=[self._startDate stringByAppendingFormat:@", %@",localString];
        
 
        
        if(abs == 12)
        {
            NSCalendar *_calendar = [[NSCalendar currentCalendar] retain];
            NSDateComponents *hourStep = [[NSDateComponents new] autorelease];
            hourStep.hour = 1;       
            NSDate *endRepeatDate = [_calendar dateByAddingComponents: hourStep toDate: date options: 0];
            NSLog(@"endRepeatDate=%@",endRepeatDate);
            [_calendar release];
            
       
            
             self._startDate=[self._startDate stringByAppendingFormat:@" %@",[endRepeatDate stringFromDateWithFormat:@"a"]];
            
            NSLog(@"self._startDate=%@",self._startDate);
        }
        else
        {
            
            self._startDate=[self._startDate stringByAppendingFormat:@" %@",[date stringFromDateWithFormat:@"a"]];
            NSLog(@"self._startDate=%@",self._startDate);
        }
        

        NSDate *_date=[NSDate dateFromString:self._startDate withFormat:@"EEE dd MMM, yyyy, h:mm a"];
   
        NSLog(@"_date=%@",_date);
        
         _date=[NSDate dateWithTimeInterval:60*60 sinceDate:_date];
        
        self._endDate= [_date stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
        
         NSLog(@"self._endDate=%@",self._endDate);
         
        

    }
    
    self.selectedIndexPath=nil;
    self.view.backgroundColor=[UIColor whiteColor];
    
    /*
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCancel setImage:[UIImage imageNamed:@"cancel_button.png"] forState:UIControlStateNormal];
    btnCancel.frame =CGRectMake(0, 0, 60, 29);
    [btnCancel addTarget:self 
                  action:@selector(cancelPopOver:) 
        forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc]initWithCustomView:btnCancel];
	self.navigationItem.leftBarButtonItem = barButtonCancel;
	[barButtonCancel release];
    */
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
    UIButton *btnDone= [UIButton repToolButtonWithImageName:NAV_DONE_BUTTON text:@"Save"];
    btnDone.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
    [btnDone addTarget:self
                action:@selector(saveChanges:)
      forControlEvents:UIControlEventTouchUpInside];

    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithCustomView:btnDone];
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    if(!self.reminderId)
    [self addBackButtonWithImage:[UIImage imageNamed:NAV_BACK_BUTTON] title:@"Back" selector:nil];
    
    //self.view.backgroundColor = CELL_BACKGROUND_COLOR;
    
}
else
{
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveChanges:)];
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
}
	
    
    CGRect _rectGrouped=self.view.bounds;
    UITableView *aTableview=[[UITableView alloc] initWithFrame:_rectGrouped style:UITableViewStyleGrouped];
    aTableview.delegate=self;
    aTableview.dataSource=self;
    aTableview.showsHorizontalScrollIndicator=NO;
    aTableview.showsVerticalScrollIndicator=NO;
    aTableview.scrollEnabled=NO;
    aTableview.rowHeight=47;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    aTableview.backgroundColor = REMINDER_BG_COLOR;
    else
    aTableview.backgroundColor = [UIColor clearColor];
  
    aTableview.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:aTableview];
    self._tableView=aTableview;
    [aTableview release];
    
    //[self performSelector:@selector(launchWithDatePicker) withObject:nil afterDelay:0.1];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        aTableview.sectionFooterHeight = 1.0;
        aTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(aTableview.bounds), 10.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        aTableview.tableHeaderView = _headerView;
        [_headerView release];
    }
}

-(void)launchWithDatePicker
{
    [self tableView:self._tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
   // [self._tableView reloadData];
    

    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        self.preferredContentSize=CGSizeMake(335.0+14.0, self._tableView.contentSize.height-1);
    else
        self.contentSizeForViewInPopover = CGSizeMake(335.0+14.0, self._tableView.contentSize.height-1);
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    

 
    
    //self.contentSizeForViewInPopover = self._tableView.contentSize;
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    
    self.preferredContentSize=self._tableView.contentSize;
else
     self.contentSizeForViewInPopover = CGSizeMake(335.0+15.0, self._tableView.contentSize.height);

    
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return (self.reminderId.length)  ? 5 : 4;
    
    /*
    if ([self.title isEqualToString:@"NEW REMINDER"]) 
        return 4;
    else
        return 5;
    */
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 47.0;
}

/*
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return (section==1)? 250.0 : 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if(section == 1)
        return [self datePickerFooterView];
    
    else return nil;
}
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
   if(section==2)
        return([self._repeat isEqualToString:@"Never"]?1:2);
    else if(section==3)
        //return([self._alert isEqualToString:@"None"]?1:2);
        return 1;
        /// SECOND ALERT IS NOT REQUIRED IN IOS 6.0 REMINDER
    else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    static NSString *CellIdentifier = @"Cell";
    
    static NSString *CellIdentifier1 = @"Cell1";
    UITableViewCell *cell=nil;
    UITableViewCell *cell1=nil;
    
    switch (indexPath.section)
    {
        case 0:
        {
            cell =(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) 
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.detailTextLabel.textColor = [UIColor blackColor];
               
                
                UITextField *_txtTittle = [[UITextField alloc] initWithFrame:CGRectMake(cell.contentView.bounds.origin.x+10, cell.contentView.bounds.origin.y, 
                                                                                        cell.contentView.bounds.size.width-20, cell.contentView.bounds.size.height)];
                _txtTittle.borderStyle = UITextBorderStyleNone;
                _txtTittle.placeholder = @"Reminder Title";
                _txtTittle.textColor   = [UIColor blackColor];
                //[UIColor colorWithRed:0.0/255.0 green:73.0/255.0 blue:142.0/255.0 alpha:1.0];
                _txtTittle.textAlignment = NSTextAlignmentLeft;
                _txtTittle.autocorrectionType = UITextAutocorrectionTypeNo;
                _txtTittle.returnKeyType = UIReturnKeyDone;
                if(self.reminderTittle!=nil)
                {
#if SHOW_REMINDER_TITLE
                    NSString *_reminderTitle =self.reminderTittle;                        
#else
                    NSString *_reminderTitle = ([self.reminderTittle rangeOfString:@":"].location !=NSNotFound)? [self.reminderTittle substringFromIndex:[self.reminderTittle rangeOfString:@":"].location+1] : self.reminderTittle;
#endif
                    _txtTittle.text=_reminderTitle;
                }
                    //[self.reminderTittle substringFromIndex:[self.reminderTittle rangeOfString:@":"].location+1];
                _txtTittle.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                _txtTittle.backgroundColor = [UIColor clearColor];
                _txtTittle.delegate = self;
                cell.accessoryType=UITableViewCellAccessoryNone;
                [cell.contentView addSubview:_txtTittle];
                self.txtTittle=_txtTittle;
                [_txtTittle release];
            }
        
        }
            break;
            
        case 1:
        {
            cell1 =(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier1];
            if (cell1 == nil) 
            {
                cell1 = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier1] autorelease];
                
              cell1.detailTextLabel.textColor = [UIColor blackColor];
              cell1.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
            }
            
            cell1.textLabel.text=@"Date";
            cell1.detailTextLabel.text=self._startDate;
            
            cell1.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
            
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            cell1.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            else
            {
            cell1.accessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            cell1.accessoryView.userInteractionEnabled = NO;
            }
        }
        break;
            
        case 2:
        {
            cell =(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) 
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                
                cell.detailTextLabel.textColor = [UIColor blackColor];
                cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
            }
            if(indexPath.row==0)
            {
                cell.textLabel.text=@"Repeat";
                cell.detailTextLabel.text=self._repeat;
            }
            else
            {
                cell.textLabel.text=@"End Repeat";
                cell.detailTextLabel.text=self._endRepeat;
            }
            
           if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
           {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
           }
           else
          {
            cell.accessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            cell.accessoryView.userInteractionEnabled = NO;
           }
            
        }
            break;
            
        case 3:
        {
            cell =(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) 
            {
                cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                cell.detailTextLabel.textColor = [UIColor blackColor];
                cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
            }
            if(indexPath.row==0)
            {
                cell.textLabel.text=CAMERA_VIEW_LEAVE_ALERT_HEADER;
                cell.detailTextLabel.text=self._alert;
            }
            else
            {
                cell.textLabel.text=@"Second Alert";
                cell.detailTextLabel.text=self._secondAlert;
            }
            
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
              cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            else
            {
              cell.accessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
              cell.accessoryView.userInteractionEnabled = NO;
            }
            
        }
            break;
            
        case 4:
        {
            cell1 =(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier1];
            if (cell1 == nil) 
            {
                cell1 = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier1] autorelease];
                
                cell1.detailTextLabel.textColor = [UIColor blackColor];
                
              if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
              {
                UIButton *imgv=[UIButton repToolButtonWithImageName:@"red_button.png" text:@"DELETE REMINDER"];
                CGRect _deleteFrame = imgv.frame;

                _deleteFrame.origin.x = (CGRectGetWidth(cell1.bounds)-_deleteFrame.size.width)/2.0;
                imgv.frame = _deleteFrame;
                imgv.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
                [cell1 addSubview:imgv];
                imgv.userInteractionEnabled = NO;
                
              }
             else
             {
                UIButton *imgv=[UIButton repToolButtonWithImageName:@"red_button_small.png" text:@"DELETE REMINDER"];
                CGRect _deleteFrame = imgv.frame;

                imgv.frame = _deleteFrame;
                cell1.backgroundView = imgv;
               }
              
            }
            cell1.selectionStyle=UITableViewCellSelectionStyleNone;
        }
            break;
            
        default:
            break;
    }
    
    if(indexPath.section==1||indexPath.section==4)
        return cell1;
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
   // if(indexPath.section==4) return;
    
    InsetTableViewCell *_cell = (InsetTableViewCell *)cell;
    _cell.delegate = self;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.hidden = (indexPath.section == 4);
  
    
    CellBackgroundView *_selectedBgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _selectedBgView.backgroundColor = [UIColor clearColor];
    _selectedBgView.contentInset = CGPointMake(10.0, 0.0);
    _selectedBgView.cornerRadius = 8.0;
    _selectedBgView.borderColor = tableView.separatorColor;
    _selectedBgView.fillColor = (indexPath.section == 4) ? REMINDER_BG_COLOR : [UIColor cellSelectedColorBlue];
    _selectedBgView.hidden = (indexPath.section == 4);

    
    NSInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    _selectedBgView.position = _bgView.position;
    
    cell.backgroundView = _bgView;
    cell.selectedBackgroundView = _selectedBgView;
    
    [_bgView release];
    [_selectedBgView release];
}

#pragma mark - InsetTableViewCellDelegate

-(void)insetTableViewCell:(InsetTableViewCell *)cell didSelect:(BOOL)selected
{
    //CellBackgroundView *_bgView = (CellBackgroundView *) cell.backgroundView;
    
   // _bgView.fillColor = selected  ? [UIColor cellSelectedColorBlue] : [UIColor whiteColor];
   // [_bgView setNeedsDisplay];
    
    cell.textLabel.textColor = selected ? [UIColor whiteColor] : [UIColor blackColor];
    cell.detailTextLabel.textColor = selected ? [UIColor whiteColor] : [UIColor blackColor];
}




#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    if([self.txtTittle isFirstResponder])
        [self.txtTittle resignFirstResponder];
    
    switch (indexPath.section)
    {
        case 1:
        {
            self.selectedIndexPath=indexPath;
            Date *alert = [[Date alloc] init];
            alert.delegate=self;
            alert.startDate=self._startDate;
            alert.endDate=self._endDate;
            [self.navigationController pushViewController:alert animated:YES];
            [alert release];
            
            //[self showDatePicker:indexPath];
        }
            break;
        case 2:
        {
            if(indexPath.row==0)
            {
                self.selectedIndexPath=indexPath;
                RepeatList *alert = [[RepeatList alloc] init];
                alert.delegate=self;
                alert.index=[_arrRepeatList indexOfObject:self._repeat];
                [self.navigationController pushViewController:alert animated:YES];
                [alert release];
            }
            else
            {
                self.selectedIndexPath=indexPath;
                EndRepeatList *alert = [[EndRepeatList alloc] init];
                alert.delegate=self;
                
           
                NSDate *d1=[NSDate dateFromString:self._startDate withFormat:@"EEE dd MMM, yyyy, h:mm a"];
                alert.minDate=d1;
         
            
                
                NSString *localEndRepeat=nil;
                if([self._repeat isEqualToString:@"Daily"])
                {
                    NSCalendar *_calendar = [[NSCalendar currentCalendar] retain];
                    NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
                    monthStep.week = 1;
                    
                   
                    
                    NSDate *d1=[NSDate dateFromString:self._startDate withFormat:@"EEE dd MMM, yyyy, h:mm a"];
                    NSLog(@"d1=%@",d1);
                    
                    NSDate *d2 = [_calendar dateByAddingComponents: monthStep toDate: d1 options: 0];
                    NSLog(@"d2=%@",d2);
                 
                    
                    localEndRepeat=[d2 stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
                    NSLog(@"localEndRepeat=%@",localEndRepeat);
             
                    [_calendar release];
                }
                else if([self._repeat isEqualToString:@"Weekly"])
                {
                    NSCalendar *_calendar = [[NSCalendar currentCalendar] retain];
                    NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
                    monthStep.month = 1;
                    
                    
                    
                    NSDate *d1=[NSDate dateFromString:self._startDate withFormat:@"EEE dd MMM, yyyy, h:mm a"];
                     NSLog(@"d1=%@",d1);
                    
                    NSDate *d2 = [_calendar dateByAddingComponents: monthStep toDate: d1 options: 0];
                    NSLog(@"d2=%@",d2);
                    localEndRepeat=[d2 stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
         
                    NSLog(@"localEndRepeat=%@",localEndRepeat);
           
                    [_calendar release];
                }
                else if([self._repeat isEqualToString:@"Biweekly"])
                {
                    NSCalendar *_calendar = [[NSCalendar currentCalendar] retain];
                    NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
                    monthStep.month = 2;
                    
               
                    NSDate *d1=[NSDate dateFromString:self._startDate withFormat:@"EEE dd MMM, yyyy, h:mm a"];

                    NSLog(@"d1=%@",d1);
                    
                    NSDate *d2 = [_calendar dateByAddingComponents: monthStep toDate: d1 options: 0];
                    NSLog(@"d2=%@",d2);
                    localEndRepeat=[d2 stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
          
                    NSLog(@"localEndRepeat=%@",localEndRepeat);
               
                    [_calendar release];
                }
                else if([self._repeat isEqualToString:@"Monthly"])
                {
                    NSCalendar *_calendar = [[NSCalendar currentCalendar] retain];
                    NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
                    monthStep.year = 1;
                    
           
                     NSDate *d1=[NSDate dateFromString:self._startDate withFormat:@"EEE dd MMM, yyyy, h:mm a"];
                    NSLog(@"d1=%@",d1);
                    
                    NSDate *d2 = [_calendar dateByAddingComponents: monthStep toDate: d1 options: 0];
                    NSLog(@"d2=%@",d2);
                    localEndRepeat=[d2 stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
                    NSLog(@"localEndRepeat=%@",localEndRepeat);
     
                    [_calendar release];
                }
                else if([self._repeat isEqualToString:@"Yearly"])
                {
                    NSCalendar *_calendar = [[NSCalendar currentCalendar] retain];
                    NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
                    monthStep.year = 5;
                    
                
                     NSDate *d1=[NSDate dateFromString:self._startDate withFormat:@"EEE dd MMM, yyyy, h:mm a"];
                    NSLog(@"d1=%@",d1);
                    
                    NSDate *d2 = [_calendar dateByAddingComponents: monthStep toDate: d1 options: 0];
                    NSLog(@"d2=%@",d2);
                     localEndRepeat=[d2 stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
                    NSLog(@"localEndRepeat=%@",localEndRepeat);
          
                    [_calendar release];
                }
                alert.endRepeatDate=[self._endRepeat isEqualToString:@"Never"]?localEndRepeat:self._endRepeat;
                [self.navigationController pushViewController:alert animated:YES];
                [alert release];
            }
        }
            break;
        case 3:
        {
            if(indexPath.row==0)
            {
                self.selectedIndexPath=indexPath;
                AlertList *alert = [[AlertList alloc] init];
                alert.delegate=self;
                alert.strAlert=_alert;
                [self.navigationController pushViewController:alert animated:YES];
                [alert release];
            }
            else
            {
                self.selectedIndexPath=indexPath;
                SecondAlertList *alert = [[SecondAlertList alloc] init];
                alert.delegate=self;
                
                NSLog(@"Second alert text = %@",self._secondAlert);
                alert.strSecondAlert=self._secondAlert;
                [self.navigationController pushViewController:alert animated:YES];
                [alert release];
            }
        }
            break;
        case 4:
        {
            self.selectedIndexPath=indexPath;
            
               NSLog(@" reminderId%@",self.reminderId);
            if(self.reminder)
            {
                tableView.userInteractionEnabled = NO;
                
                EKReminder *event=(EKReminder *)[[CustomEventStore sharedInstance].eventStore calendarItemWithIdentifier:self.reminder.eventIdentifier];
                //if (event != nil)
                {  
                   /*
                     //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                     UIImageView *imgv=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"delete_reminder_button_selected.png"]];
                     imgv.frame=cell.backgroundView.frame;
                     cell.selectedBackgroundView = imgv;
                     [imgv release];
                    */
                    
                    NSError* error = nil;
                    //[[CustomEventStore sharedInstance].eventStore removeEvent:event span:EKSpanThisEvent error:&error];
                    // Reminder *_reminder = [CoreDataHandler //reminderWithEventIdentifier:event.eventIdentifier];

                    [[OfflineManager sharedManager] addReminder:reminder event:event willDelete:YES isEditMode:NO];


                     if(self.saveReminderCallback) self.saveReminderCallback(reminder,3,nil);

                    
                    if(self.reminder)
                    {
                        [SSCoreDataManager deleteObject:self.reminder];
                        [[SSCoreDataManager sharedManager] save:nil];
                    }
                    
                    
                    if(event)
                    [[CustomEventStore sharedInstance].eventStore removeReminder:event commit:YES error:nil];
                    
                    NSLog(@"%@",self.reminderId);
                    
                    NSLog(@"error=%@",[error localizedDescription]);
                } 
                
                self.reminderId=nil;
                [self.datePickerPopover dismissPopoverAnimated:YES];
                
                if([self.delegate respondsToSelector:@selector(dismissPopOverFromViewReminder:)])
                    [self.delegate dismissPopOverFromViewReminder:YES];
            }
        }
        break;
            
        default:break;
    }
} 



- (void)selectedEndRepeat:(NSString *)strEndRepeat
{
    if(strEndRepeat!=nil)
        self._endRepeat=strEndRepeat;
    
    //NSMutableArray *reloadIndexPaths=[NSMutableArray arrayWithObject:self.selectedIndexPath];
    
    self.selectedIndexPath=nil;
    
    //[self._tableView reloadRowsAtIndexPaths:reloadIndexPaths withRowAnimation:UITableViewRowAnimationFade];
    
    [self._tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationNone];
    //[self._tableView reloadData];
}

- (void)selectedRepeat:(int)indexRepeat
{
    if(indexRepeat!=-1)
        
    {
        self._repeat=[_arrRepeatList objectAtIndex:indexRepeat];
        NSLog(@"self._repeat=%@",self._repeat);
    }
    self.selectedIndexPath=nil;
    //[self._tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationFade];
    [self._tableView reloadData];
}

- (void)selectedAlertTime:(NSString *)strAlert
{
    if(strAlert!=nil)
        self._alert=strAlert;

    self.selectedIndexPath=nil;
    //[self._tableView reloadSections:[NSIndexSet indexSetWithIndex:3] withRowAnimation:UITableViewRowAnimationFade];
    [self._tableView reloadData];
}

- (void)selectedSecondAlertTime:(NSString *)strAlert
{
    // NSLog(@"strAlert=%@",strAlert);
    if(strAlert!=nil)
        self._secondAlert=strAlert;
    
    //NSMutableArray *reloadIndexPaths=[NSMutableArray arrayWithObject:self.selectedIndexPath];
    self.selectedIndexPath=nil;
    //[self._tableView reloadRowsAtIndexPaths:reloadIndexPaths withRowAnimation:UITableViewRowAnimationFade];
    //[self._tableView reloadSections:[NSIndexSet indexSetWithIndex:3] withRowAnimation:UITableViewRowAnimationFade];
    [self._tableView reloadData];
}

- (void)selectedStartDate:(NSString *)strStart endDate:(NSString *)strEnd
{
    if(strStart!=nil)
    {
        self._startDate=strStart;
        NSLog(@"self._startDate=%@",self._startDate);
    }
    
    
    if(strEnd!=nil)
    {
        self._endDate=strEnd;
         NSLog(@"self._endDate=%@",self._endDate);
    }
    
    
    //NSMutableArray *reloadIndexPaths=[NSMutableArray arrayWithObject:self.selectedIndexPath];
    self.selectedIndexPath=nil;
    [self._tableView reloadData];
}


-(void)cancelPopOver:(id)sender
{
    //UIButton *btn=(UIButton *)sender;
    [self.datePickerPopover dismissPopoverAnimated:NO];
    
    if([self.delegate respondsToSelector:@selector(dismissPopOverFromViewReminder:)])
        [self.delegate dismissPopOverFromViewReminder:NO];
}



-(void)saveChanges:(id)sender
{
    //UIButton *btn=(UIButton *)sender;
    
    BOOL isEditMode = NO;
    
    if(!self.txtTittle.text.length)
    {
        [self.txtTittle becomeFirstResponder];
        return;
    }
    
    Reminder *_reminder = self.reminder;
    EKReminder *event      = nil;

   // @try
    {
        
    NSLog(@" reminderId%@",self.reminderId);
    
    if(self.reminder)
    {
        event =(EKReminder *) [[CustomEventStore sharedInstance].eventStore calendarItemWithIdentifier:self.reminder.eventIdentifier];
        
        if (event != nil) 
        {  
            isEditMode = YES;
            
        
            //[[CustomEventStore sharedInstance].eventStore removeEvent:event 
            
             NSLog(@"reminder ID=%@",_reminder.reminderID);
        } 
        self.reminderId=nil;
    }
    
    
    if(!_reminder)
    {
        _reminder =[CoreDataHandler newReminder];
    }
    
    
    if(!event)
    {
        event  = [EKReminder reminderWithEventStore:[CustomEventStore sharedInstance].eventStore];
        
        [event setCalendar:[[CustomEventStore sharedInstance].eventStore defaultCalendarForNewReminders]];
    }
    
    
    NSString *eventTitle = [NSString stringWithFormat:@"%@",self.txtTittle.text.length?self.txtTittle.text:@"New Reminder"];//@"EVENT TITLE";
    _reminder.title = eventTitle;
    
    event.title =_reminder.title;
    
    NSLog(@"event.title------%@",event.title);



    
    //event.startDate = [formatter dateFromString:self._startDate];
    NSDate *startDate =[NSDate dateFromString:self._startDate withFormat:@"EEE dd MMM, yyyy, h:mm a"];
    //startDate = [startDate currentDateHourMinute];
    _reminder.startDate = startDate;
    
    //event.startDateComponents = [startDate dateComponents];
    event.dueDateComponents = [startDate dateComponents];
    
    
    if(![self._alert isEqualToString:@"None"])
    {
    
        NSTimeInterval interval1 = 0.0; //= (-min);
        NSTimeInterval interval2 = 0.0; //= (-min);
        EKAlarm *alarm1;
        EKAlarm *alarm2;
        NSMutableArray *alarmList=[[NSMutableArray alloc] init];

        
        if([self._alert isEqualToString:@"At time of event"])
        {
            float min=0.0;
            interval1 = min;
            
            alarm1 = [EKAlarm alarmWithRelativeOffset:interval1];
           // alarm1 = [EKAlarm alarmWithAbsoluteDate:[startDate dateByAddingTimeInterval:interval1]]; //Create object of alarm
            [alarmList addObject:alarm1];
        }
        else if([self._alert isEqualToString:@"5 minutes before"])
        {
            float min=5.0;
            float second=60.0;
            interval1 = -(min*second);
            
            alarm1 = [EKAlarm alarmWithRelativeOffset:interval1];
             //alarm1 = [EKAlarm alarmWithAbsoluteDate:[startDate dateByAddingTimeInterval:interval1]];//Create object of alarm
            [alarmList addObject:alarm1];
        }
        else if([self._alert isEqualToString:@"15 minutes before"])
        {
            float min=15.0;
            float second=60.0;
            interval1 = -(min*second);
            
            alarm1 = [EKAlarm alarmWithRelativeOffset:interval1]; //Create object of alarm
            [alarmList addObject:alarm1];
        }
        else if([self._alert isEqualToString:@"30 minutes before"])
        {
            float min=30.0;
            float second=60.0;
            interval1 = -(min*second);
            
            alarm1 = [EKAlarm alarmWithRelativeOffset:interval1]; //Create object of alarm
            [alarmList addObject:alarm1];
        }
        else if([self._alert isEqualToString:@"1 hour before"])
        {
            float hour=1.0;
            float min=60.0;
            float second=60.0;
            interval1 = -(hour*min*second);
            
            alarm1 = [EKAlarm alarmWithRelativeOffset:interval1]; //Create object of alarm
            [alarmList addObject:alarm1];
        }
        else if([self._alert isEqualToString:@"2 hours before"])
        {
            float hour=2.0;
            float min=60.0;
            float second=60.0;
            interval1 = -(hour*min*second);
            
            alarm1 = [EKAlarm alarmWithRelativeOffset:interval1]; //Create object of alarm
            [alarmList addObject:alarm1];
        }
        else if([self._alert isEqualToString:@"1 day before"])
        {
            float hour=24.0;
            float min=60.0;
            float second=60.0;
            interval1 = -(hour*min*second);
            
            alarm1 = [EKAlarm alarmWithRelativeOffset:interval1]; //Create object of alarm
            [alarmList addObject:alarm1];
        }
        else if([self._alert isEqualToString:@"2 days before"])
        {
            float day=2.0;
            float hour=24.0;
            float min=60.0;
            float second=60.0;
            interval1 = -(day*hour*min*second);
            
            alarm1 = [EKAlarm alarmWithRelativeOffset:interval1]; //Create object of alarm
            [alarmList addObject:alarm1];
        }
        
        if(![self._secondAlert isEqualToString:@"None"])
        {
            if([self._secondAlert isEqualToString:@"At time of event"])
            {
                float min=0.0;
                interval2 = min;
                
                alarm2 = [EKAlarm alarmWithRelativeOffset:interval2]; //Create object of alarm
                [alarmList addObject:alarm2];
            }
            else if([self._secondAlert isEqualToString:@"5 minutes before"])
            {
                float min=5.0;
                float second=60.0;
                interval2 = -(min*second);
                
                alarm2 = [EKAlarm alarmWithRelativeOffset:interval2]; //Create object of alarm
                [alarmList addObject:alarm2];
            }
            else if([self._secondAlert isEqualToString:@"15 minutes before"])
            {
                float min=15.0;
                float second=60.0;
                interval2 = -(min*second);
                
                alarm2 = [EKAlarm alarmWithRelativeOffset:interval2]; //Create object of alarm
                [alarmList addObject:alarm2];
            }
            else if([self._secondAlert isEqualToString:@"30 minutes before"])
            {
                float min=30.0;
                float second=60.0;
                interval2 = -(min*second);
                
                alarm2 = [EKAlarm alarmWithRelativeOffset:interval2]; //Create object of alarm
                [alarmList addObject:alarm2];
            }
            else if([self._secondAlert isEqualToString:@"1 hour before"])
            {
                float hour=1.0;
                float min=60.0;
                float second=60.0;
                interval2 = -(hour*min*second);
                
                alarm2 = [EKAlarm alarmWithRelativeOffset:interval2]; //Create object of alarm
                [alarmList addObject:alarm2];
            }
            else if([self._secondAlert isEqualToString:@"2 hours before"])
            {
                float hour=2.0;
                float min=60.0;
                float second=60.0;
                interval2 = -(hour*min*second);
                
                alarm2 = [EKAlarm alarmWithRelativeOffset:interval2]; //Create object of alarm
                [alarmList addObject:alarm2];
            }
            else if([self._secondAlert isEqualToString:@"1 day before"])
            {
                float hour=24.0;
                float min=60.0;
                float second=60.0;
                interval2 = -(hour*min*second);
                
                alarm2 = [EKAlarm alarmWithRelativeOffset:interval2]; //Create object of alarm
                [alarmList addObject:alarm2];
            }
            else if([self._secondAlert isEqualToString:@"2 days before"])
            {
                float day=2.0;
                float hour=24.0;
                float min=60.0;
                float second=60.0;
                interval2 = -(day*hour*min*second);
                
                alarm2 = [EKAlarm alarmWithRelativeOffset:interval2]; //Create object of alarm
                [alarmList addObject:alarm2];
            }
        }
        
        /* This is the time at which you can see the native calendar alert. Note that here i write (-min) this indicates that alert will show before event time otherwise it will appear after event start time */
        
    
        event.alarms = alarmList;   //Add alarm list to your event
        
        NSArray  *alarms = [ADDReminder dictValueForAlarms:alarmList];
        NSString *alarmJson = [alarms JSONRepresentation];
        _reminder.alarms = alarmJson;
        
       
        /*
        for(EKAlarm *alarmToAdd in alarmList)
        {
            if(![event.alarms containsObject:alarmToAdd])
            [event addAlarm:alarmToAdd];
        }
        */
        
        [alarmList release];
    }
    
    
    
    //setting the Reuccurence rule
    BOOL isRecurrenceFrequencyExists = TRUE;
    BOOL isRecurrenceFrequencyExistsBiweekly=FALSE;
    EKRecurrenceFrequency  recurrenceFrequency;
    NSLog(@"self._repeat=%@",self._repeat);
    if([self._repeat isEqualToString:@"Daily"])
    {
        recurrenceFrequency = EKRecurrenceFrequencyDaily;
    }
    else if([self._repeat isEqualToString:@"Weekly"])
    {
        recurrenceFrequency = EKRecurrenceFrequencyWeekly;
    }
    else if([self._repeat isEqualToString:@"Biweekly"])
    {
        recurrenceFrequency = EKRecurrenceFrequencyWeekly;
        isRecurrenceFrequencyExistsBiweekly=TRUE;
    }
    else if([self._repeat isEqualToString:@"Monthly"])
    {
        recurrenceFrequency = EKRecurrenceFrequencyMonthly;
    }
    else if([self._repeat isEqualToString:@"Yearly"])
    {
        recurrenceFrequency = EKRecurrenceFrequencyYearly;
    }
    else
    {
        isRecurrenceFrequencyExists = FALSE;
    }
    

    NSDate *_endRepeatDate=[NSDate dateFromString:self._endRepeat withFormat:@"EEE dd MMM, yyyy"];

    NSInteger occurrence = _endRepeatDate ? daysBetween(_reminder.startDate, _endRepeatDate) : 1;
     occurrence = MAX(1,occurrence);

    NSTimeInterval _interval = 60*60*24*occurrence;
    NSDate *endDate   = [_reminder.startDate dateByAddingTimeInterval:_interval];;
    _reminder.endDate = endDate;


    
    if(isRecurrenceFrequencyExists)
    {
        EKRecurrenceRule * recurrenceRule = [[EKRecurrenceRule alloc] 
                                             
                                             initRecurrenceWithFrequency:recurrenceFrequency
                                             interval:isRecurrenceFrequencyExistsBiweekly?2:1
                                             end:nil];
        
        EKRecurrenceEnd * end=nil;
        if ([self._endRepeat isEqualToString:@"Never"]) 
        {
           // occurrence = 1;
                                   
        }
        else
        {
            NSLog(@"self._endRepeat=%@",self._endRepeat);
           // [formatter setDateFormat:@"EEE dd MMM, yyyy"];

            
            end=[EKRecurrenceEnd recurrenceEndWithEndDate:_reminder.endDate];
        }
        
        if(end)
        recurrenceRule.recurrenceEnd = end;
        
       NSMutableDictionary *_reccurenceDict =  [ADDReminder dictValueForRecurrence:recurrenceRule];
        _reminder.recurrenceRule = [_reccurenceDict JSONRepresentation];
        
        for(EKRecurrenceRule *recRule in event.recurrenceRules)
        [event removeRecurrenceRule:recRule];
        
        [event addRecurrenceRule:recurrenceRule];
        [recurrenceRule release];
    }
    
   
    
    event.timeZone = [NSTimeZone systemTimeZone];
    
    // event.startDateComponents = [endDate dateComponents];
    // event.dueDateComponents   = [_reminder.startDate dateComponents];
    // event.completionDate      = endDate;
    
    NSString *notes = [AppDelegate eventLocationString];
    _reminder.notes = notes;
    event.notes = notes;
    
    NSError *error = nil;
    [[CustomEventStore sharedInstance].eventStore saveReminder:event commit:YES error:&error];
    
    if(!error)
    _reminder.eventIdentifier = event.calendarItemIdentifier;
    
        
    }
   // @catch (NSException *exception)
    {
        
       // NSLog(@"Exception=%@",exception.reason);
    }
    //@finally
   // {
        if(_reminder)
        {
            [[SSCoreDataManager sharedManager] save:nil];
            
            [[OfflineManager sharedManager] addReminder:_reminder event:event willDelete:NO isEditMode:isEditMode];
            
            if(self.saveReminderCallback) self.saveReminderCallback(_reminder,isEditMode?2:1,nil);
            
            
            [self.datePickerPopover dismissPopoverAnimated:NO];
            if([self.delegate respondsToSelector:@selector(dismissPopOverFromViewReminder:)])
                [self.delegate dismissPopOverFromViewReminder:YES];
        }
   // }
}


NSDate *setDateToMidnite(NSDate *aDate) 
{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    calendar.timeZone = [NSTimeZone defaultTimeZone];
    
    NSDateComponents *dateComps = [calendar components:(NSYearCalendarUnit |NSMonthCalendarUnit |NSDayCalendarUnit |NSHourCalendarUnit | NSSecondCalendarUnit |
                                                        NSMinuteCalendarUnit) fromDate:aDate];
    
    [dateComps setHour:0];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    
    NSDate *midniteDate = [calendar dateFromComponents:dateComps];
    [calendar release];
    
    return midniteDate;
}


NSUInteger daysBetween(NSDate *fromDate, NSDate *toDate) 
{

fromDate = setDateToMidnite(fromDate);
  toDate = setDateToMidnite(toDate);
  
  NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
  calendar.timeZone = [NSTimeZone defaultTimeZone];
  
   NSInteger startDay = [calendar ordinalityOfUnit:NSDayCalendarUnit
inUnit: NSEraCalendarUnit forDate:fromDate];
   NSInteger endDay = [calendar ordinalityOfUnit:NSDayCalendarUnit
inUnit: NSEraCalendarUnit forDate:toDate];
    
    [calendar release];
    
  return endDay - startDay;
}

-(void)showDatePicker:(NSIndexPath *)indexPath
{
    
    if(self.datePickerPopover && [self.datePickerPopover isPopoverVisible]) return;
    
   CGRect targetRect = [self._tableView rectForRowAtIndexPath:indexPath];
    
    DatePickerController *_datePickerController=[[DatePickerController alloc] initWithDate:[NSDate date] pickerMode:UIDatePickerModeDateAndTime];
    
    UIPopoverController *_popover=[[UIPopoverController alloc] initWithContentViewController:_datePickerController];
   // _popover.popoverContentSize=CGSizeMake(320, 210.0);
    _datePickerController.popoverController=_popover;
    _popover.passthroughViews = [NSArray arrayWithObjects:self.view,self.navigationController.navigationBar,nil];
    
    [_popover presentPopoverFromRect:targetRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    self.datePickerPopover=_popover;
    
    [_popover release];
    
    [_datePickerController addDateChangeCallback:^(NSDate *date) 
    {
        self._startDate = [date stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
        NSDate *endDate = [date dateByAddingTimeInterval:60*60];
        self._endDate   = [endDate stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
        
        [self._tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];

        
    }];
    
    [_datePickerController release];
    
    return;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" 
                                                             delegate:nil
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    UIButton *doneButton=[UIButton buttonWithType:UIButtonTypeCustom];
    doneButton.frame=CGRectMake(20,5.0,60,30);
    [doneButton setBackgroundImage:[UIImage imageNamed:@"done_button.png"] forState:UIControlStateNormal];
    [actionSheet addSubview:doneButton];
    [doneButton addTarget:self action:@selector(dissmissActionSheet:) forControlEvents:UIControlEventTouchUpInside];
    

    NSDate *_date  = [NSDate dateFromString:self._startDate withFormat:@"EEE dd MMM, yyyy, h:mm a"];
 

    
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(8.0, 45.0,CGRectGetWidth(actionSheet.bounds), 200.0)];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    datePicker.hidden = NO;
    datePicker.date = _date;
   // datePicker.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [datePicker addTarget:self
                   action:@selector(dateDidChange:)
         forControlEvents:UIControlEventValueChanged];
   
    
    [actionSheet addSubview:datePicker];
    [actionSheet showInView:self.view];
    [actionSheet release];
     [datePicker release];
}

-(void)dissmissActionSheet:(UIButton *)button
{
    UIActionSheet *actionSheet = (UIActionSheet *)[button superview];
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)dateDidChange:(UIDatePicker *)datePicker
{
    self._startDate = [datePicker.date stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
    NSDate *endDate=[datePicker.date dateByAddingTimeInterval:60*60];
    self._endDate = [endDate stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
    
    [self._tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationNone];
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{

    return textField.text.length>0;
}

+(NSMutableDictionary *)dictValueForReminder:(EKEvent *)event
{
    //=========== ALARM ==============
    /*
    NSMutableArray *_alrms=[NSMutableArray array];
    for(EKAlarm *alarm in event.alarms)
    {
        NSArray *keys=[NSArray arrayWithObjects:@"relativeOffset", nil];
        
        NSDictionary *alarmDict = [alarm dictionaryWithValuesForKeys:keys];
        [_alrms addObject:alarmDict];
    }
    */
    
    NSMutableArray *_alrms = [self dictValueForAlarms:event.alarms];
    
    //=========== RECURRENCE ==============
    
    NSMutableDictionary *_recurrenceDict =nil;// [ADDReminder dictValueForRecurrence:event.recurrenceRule];
    /*
    NSArray *keys=[NSArray arrayWithObjects:@"frequency",@"interval", nil];
    
    NSDictionary *recurrenceDict = [event.recurrenceRule dictionaryWithValuesForKeys:keys];
    
    NSMutableDictionary *_recurrenceDict = [recurrenceDict mutableCopy];
    
    
    //=========== RECURRENCE END==============
    keys=[NSArray arrayWithObjects:@"occurrenceCount",nil];
    NSDictionary *recurrenceEndDict = [event.recurrenceRule.recurrenceEnd dictionaryWithValuesForKeys:keys];
    [_recurrenceDict setObject:recurrenceEndDict forKey:@"recurrenceEnd"];
     */
    
   
    //=========== EVENT ==============
    NSArray *keys=[NSArray arrayWithObjects:@"title",@"notes",@"location",@"eventIdentifier",nil];
    NSDictionary *eventDict = [event dictionaryWithValuesForKeys:keys];
    NSMutableDictionary *_eventDict = [NSMutableDictionary dictionaryWithDictionary:eventDict];
    [_eventDict setObject:_alrms?_alrms:[NSArray array] forKey:@"alarms"];
    [_eventDict setObject:_recurrenceDict?_recurrenceDict:[NSDictionary dictionary] forKey:@"recurrenceRule"];
    
    NSTimeInterval _dueDateInterval   = [event.startDate GMTTimeIntervalSince1970];
    NSTimeInterval _endDateInterval   = [event.endDate GMTTimeIntervalSince1970];
    
     [_eventDict setObject:[NSNumber numberWithDouble:_dueDateInterval] forKey:@"startDate"];
     [_eventDict setObject:[NSNumber numberWithDouble:_endDateInterval] forKey:@"endDate"];
    
    NSLog(@"Alarm:%@",_eventDict);
    return _eventDict;
}

+(NSMutableDictionary *)dictValueForRecurrence:(EKRecurrenceRule *)recurrenceRule
{
    NSArray *keys=[NSArray arrayWithObjects:@"frequency",@"interval", nil];
    
    NSDictionary *recurrenceDict = [recurrenceRule dictionaryWithValuesForKeys:keys];
    
    NSMutableDictionary *_recurrenceDict = [[recurrenceDict mutableCopy] autorelease];

    NSDictionary *recurreneceEndDict = [ADDReminder dictValueForRecurrenceEnd:recurrenceRule.recurrenceEnd];
    
    //if(recurreneceEndDict)
    [_recurrenceDict setObject:recurreneceEndDict?recurreneceEndDict:[NSDictionary dictionary] forKey:@"recurrenceEnd"];
    
    NSLog(@"recurrenceDict=%@",recurrenceDict);
    
    return _recurrenceDict;
}

+(NSDictionary *)dictValueForRecurrenceEnd:(EKRecurrenceEnd *)recurrenceEnd
{
    if(!recurrenceEnd) return nil;
    
   // NSArray *keys=[NSArray arrayWithObjects:@"occurrenceCount",@"endDate",nil];
  //  NSDictionary *recurrenceEndDict = [recurrenceEnd dictionaryWithValuesForKeys:keys];
    
    NSDate *recEndDate = recurrenceEnd.endDate;
    NSTimeInterval _recEndDateInterval   = [recEndDate GMTTimeIntervalSince1970];
    
    NSDictionary *recurrenceEndDict =[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:_recEndDateInterval],@"endDate", nil];
    
     NSLog(@"recurrenceEndDict=%@",recurrenceEndDict);
    
    return recurrenceEndDict;

}

+(NSMutableArray *)dictValueForAlarms:(NSArray *)alarms
{
    if(!alarms) return nil;
    
    NSMutableArray *_alrms=[NSMutableArray array];
    for(EKAlarm *alarm in alarms)
    {
        NSArray *keys=[NSArray arrayWithObjects:@"relativeOffset", nil];
        
        NSDictionary *alarmDict = [alarm dictionaryWithValuesForKeys:keys];
        [_alrms addObject:alarmDict];
    }
    
    NSLog(@"Alarms=%@",_alrms);
    
    return _alrms;
}

+(NSMutableArray *)alarmFromArr:(NSArray *)_alrms
{
    NSMutableArray *allAlarms = [NSMutableArray array];
 
    if([_alrms isKindOfClass:[NSArray class]])
    {
       
        for(NSDictionary *alarm in _alrms)
        {
            NSNumber *_relativeOffset=[alarm valueForKey:@"relativeOffset"];
            NSTimeInterval relativeOffset = [_relativeOffset doubleValue];
            
            EKAlarm *_alarm = [EKAlarm alarmWithRelativeOffset:relativeOffset]; //Create object of alarm
            [allAlarms addObject:_alarm];
        }
    }

    return allAlarms;
}

+(EKEvent *)updateReminder:(EKEvent *)event fromDict:(NSDictionary *)dict
{
    NSMutableArray *_alrms=[dict valueForKey:@"alarms"];
    if([_alrms isKindOfClass:[NSString class]])
    {
        _alrms = [_alrms JSONValue];
        
        if([_alrms isKindOfClass:[NSArray class]])
        {
            NSMutableArray *allAlarms = [NSMutableArray array];
            for(NSDictionary *alarm in _alrms)
            {
                NSNumber *_relativeOffset=[alarm valueForKey:@"relativeOffset"];
                NSTimeInterval relativeOffset = [_relativeOffset doubleValue];
                
                EKAlarm *_alarm = [EKAlarm alarmWithRelativeOffset:relativeOffset]; //Create object of alarm
                [allAlarms addObject:_alarm];
                [event addAlarm:_alarm];
            }
        }
    }
    
     NSMutableDictionary *_recurrenceDict = [dict valueForKey:@"recurrenceRule"];
    if([_recurrenceDict isKindOfClass:[NSString class]])
    {
        _recurrenceDict = [_recurrenceDict JSONValue];
        
        if([_recurrenceDict isKindOfClass:[NSDictionary class]])
        {
            NSNumber  * recurrenceFrequency = [_recurrenceDict valueForKey:@"frequency"];
            NSNumber  * interval = [_recurrenceDict valueForKey:@"interval"];
            
            NSInteger _interval = [interval integerValue];
            _interval = MAX(_interval, 1);
            
            EKRecurrenceFrequency _recurrenceFrequency = [recurrenceFrequency intValue];
            
            EKRecurrenceRule * recurrenceRule = [[EKRecurrenceRule alloc] 
                                                 
                                                 initRecurrenceWithFrequency:_recurrenceFrequency
                                                 interval:_interval
                                                 end:nil];
            
            
            //=========== RECURRENCE END==============
            NSMutableDictionary *_recurrenceEnd = [_recurrenceDict valueForKey:@"recurrenceEnd"];
            if([_recurrenceEnd isKindOfClass:[NSString class]])
            {
                _recurrenceEnd = [_recurrenceEnd JSONValue];
                
                if([_recurrenceEnd isKindOfClass:[NSDictionary class]])
                {
                    NSNumber  * occurrenceCount = [_recurrenceEnd valueForKey:@"occurrenceCount"];
                    NSUInteger _occurrenceCount = [occurrenceCount unsignedIntegerValue];
                    _occurrenceCount = MAX(_occurrenceCount, 1);
                    
                    EKRecurrenceEnd * end=[EKRecurrenceEnd recurrenceEndWithOccurrenceCount:_occurrenceCount];
                    recurrenceRule.recurrenceEnd = end;
                }
            }
            
            [event addRecurrenceRule:recurrenceRule];
            [recurrenceRule release];
        }
    }
    
    NSNumber *_startDate   = [dict valueForKey:@"startDate"];
    NSNumber *_endDate     = [dict valueForKey:@"endDate"];
    NSString *_notes       = [dict valueForKey:@"notes"];
    NSString *_location    = [dict valueForKey:@"location"];
    NSString *_title       = [dict valueForKey:@"title"];
    
     NSDate *startDate  = [NSDate dateWithTimeIntervalSince1970:[_startDate doubleValue]];
    NSDate  *endDate    = [NSDate dateWithTimeIntervalSince1970:[_endDate doubleValue]];
    
    event.startDate = startDate;
    event.endDate = endDate;
    event.notes = _notes;
    event.location = _location;
    
    event.title =([_title rangeOfString:@"Reminder:"].location!=NSNotFound)? _title.length?_title:@"Reminder" : [NSString stringWithFormat:@"Reminder: %@",_title.length?_title:@"Reminder"];


    return event;
}

+(EKEvent *)reminderFromDict:(NSDictionary *)dict
{
    if(!dict) return nil;
    
    EKEvent *event  = [EKEvent eventWithEventStore:[CustomEventStore sharedInstance].eventStore];
    [event setCalendar:[[CustomEventStore sharedInstance].eventStore defaultCalendarForNewEvents]];
    
    //=========== ALARM ==============
    NSMutableArray *_alrms=[dict valueForKey:@"alarms"];
    
    if([_alrms isKindOfClass:[NSArray class]])
    {
    NSMutableArray *allAlarms = [NSMutableArray array];
    for(NSDictionary *alarm in _alrms)
    {
        NSNumber *_relativeOffset=[alarm valueForKey:@"relativeOffset"];
        NSTimeInterval relativeOffset = [_relativeOffset doubleValue];
        
        EKAlarm *_alarm = [EKAlarm alarmWithRelativeOffset:relativeOffset]; //Create object of alarm
        [allAlarms addObject:_alarm];
        [event addAlarm:_alarm];
    }
    }
    else if([_alrms isKindOfClass:[NSString class]])
    {
        _alrms = [_alrms JSONValue];
        
        if([_alrms isKindOfClass:[NSArray class]])
        {
            NSMutableArray *allAlarms = [NSMutableArray array];
            for(NSDictionary *alarm in _alrms)
            {
                NSNumber *_relativeOffset=[alarm valueForKey:@"relativeOffset"];
                NSTimeInterval relativeOffset = [_relativeOffset doubleValue];
                
                EKAlarm *_alarm = [EKAlarm alarmWithRelativeOffset:relativeOffset]; //Create object of alarm
                [allAlarms addObject:_alarm];
                [event addAlarm:_alarm];
            }
        }
    }
    
    //================================== RECURRENCE =================================================
    
    NSMutableDictionary *_recurrenceDict = [dict valueForKey:@"recurrenceRule"];
    
    if([_recurrenceDict isKindOfClass:[NSDictionary class]])
    {
    NSNumber  * recurrenceFrequency = [_recurrenceDict valueForKey:@"frequency"];
    NSNumber  * interval = [_recurrenceDict valueForKey:@"interval"];
    
    NSInteger _interval = [interval integerValue];
        _interval = MAX(_interval,1);
    EKRecurrenceFrequency _recurrenceFrequency = [recurrenceFrequency intValue];
        
    NSLog(@"_recurrenceFrequency=%d",_recurrenceFrequency);
    
    EKRecurrenceRule * recurrenceRule = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:_recurrenceFrequency interval:_interval end:nil];
    
    
    //=========== RECURRENCE END==============
    NSMutableDictionary *_recurrenceEnd = [_recurrenceDict valueForKey:@"recurrenceEnd"];
    
    if([_recurrenceEnd isKindOfClass:[NSDictionary class]])
    {
    NSNumber  * occurrenceCount = [_recurrenceEnd valueForKey:@"occurrenceCount"];
    NSUInteger _occurrenceCount = [occurrenceCount unsignedIntegerValue];
        _occurrenceCount = MAX(_occurrenceCount, 1);
    
    EKRecurrenceEnd * end=[EKRecurrenceEnd recurrenceEndWithOccurrenceCount:_occurrenceCount];
    recurrenceRule.recurrenceEnd = end;
    }
    else  if([_recurrenceEnd isKindOfClass:[NSString class]])
    {
        _recurrenceEnd = [_recurrenceEnd JSONValue];
        
        if([_recurrenceEnd isKindOfClass:[NSDictionary class]])
        {
            NSNumber  * occurrenceCount = [_recurrenceEnd valueForKey:@"occurrenceCount"];
            NSUInteger _occurrenceCount = [occurrenceCount unsignedIntegerValue];
            _occurrenceCount = MAX(_occurrenceCount, 1);
            
            EKRecurrenceEnd * end=[EKRecurrenceEnd recurrenceEndWithOccurrenceCount:_occurrenceCount];
            recurrenceRule.recurrenceEnd = end;
        }
    }
    
    [event addRecurrenceRule:recurrenceRule];
    [recurrenceRule release];
        
    }
    else if([_recurrenceDict isKindOfClass:[NSString class]])
    {
        _recurrenceDict = [_recurrenceDict JSONValue];
        
        if([_recurrenceDict isKindOfClass:[NSDictionary class]])
        {
            NSNumber  * recurrenceFrequency = [_recurrenceDict valueForKey:@"frequency"];
            NSNumber  * interval = [_recurrenceDict valueForKey:@"interval"];
            
            NSInteger _interval = [interval integerValue];
            
            _interval = MAX(_interval,1);
            
            EKRecurrenceFrequency _recurrenceFrequency = [recurrenceFrequency intValue];
            
            EKRecurrenceRule * recurrenceRule = [[EKRecurrenceRule alloc] 
                                                 
                                                 initRecurrenceWithFrequency:_recurrenceFrequency
                                                 interval:_interval
                                                 end:nil];
            
            
            //=========== RECURRENCE END==============
            NSMutableDictionary *_recurrenceEnd = [_recurrenceDict valueForKey:@"recurrenceEnd"];
            if([_recurrenceEnd isKindOfClass:[NSDictionary class]])
            {
                NSNumber  * occurrenceCount = [_recurrenceEnd valueForKey:@"occurrenceCount"];
                NSUInteger _occurrenceCount = [occurrenceCount unsignedIntegerValue];
                
                _occurrenceCount = MAX(_occurrenceCount, 1);
                
                EKRecurrenceEnd * end=[EKRecurrenceEnd recurrenceEndWithOccurrenceCount:_occurrenceCount];
                recurrenceRule.recurrenceEnd = end;
            }
            else  if([_recurrenceEnd isKindOfClass:[NSString class]])
            {
                _recurrenceEnd = [_recurrenceEnd JSONValue];
                
                if([_recurrenceEnd isKindOfClass:[NSDictionary class]])
                {
                    NSNumber  * occurrenceCount = [_recurrenceEnd valueForKey:@"occurrenceCount"];
                    NSUInteger _occurrenceCount = [occurrenceCount unsignedIntegerValue];
                    
                    _occurrenceCount = MAX(_occurrenceCount, 1);
                    
                    EKRecurrenceEnd * end=[EKRecurrenceEnd recurrenceEndWithOccurrenceCount:_occurrenceCount];
                    recurrenceRule.recurrenceEnd = end;
                }
            }
           
            [event addRecurrenceRule:recurrenceRule];
            [recurrenceRule release];
        }
    }
    
    //============================== EVENT ==============================================================
    
    NSString *_startDate   = [dict valueForKey:@"startDate"];
    NSString *_endDate     = [dict valueForKey:@"endDate"];
    NSString *_notes       = [dict valueForKey:@"notes"];
    NSString *_location    = [dict valueForKey:@"location"];
    NSString *_title       = [dict valueForKey:@"title"];
    
    //event.startDate = [NSDate dateFromWCFTimeInterval:[_startDate doubleValue]];
    //event.endDate   = [NSDate dateFromWCFTimeInterval:[_endDate doubleValue]];;
    
    event.startDate = [NSDate dateWithTimeIntervalSince1970:[_startDate doubleValue]];
    event.endDate   = [NSDate dateWithTimeIntervalSince1970:[_endDate doubleValue]];
    
    NSLog(@"event startDate=%@ event endDate=%@",event.startDate,_endDate);
    event.notes = _notes;
    event.location = _location;
    event.title =([_title rangeOfString:@"Reminder:"].location!=NSNotFound)? _title.length?_title:@"Reminder" : _title.length?_title:@"Reminder";
    
    return event;
}

+(EKRecurrenceRule *)recurrenceRuleFromDict:(NSDictionary *)_recurrenceDict
{
    EKRecurrenceRule * recurrenceRule = nil;
    if([_recurrenceDict isKindOfClass:[NSDictionary class]])
    {
        NSNumber  * recurrenceFrequency = [_recurrenceDict valueForKey:@"frequency"];
        NSNumber  * interval = [_recurrenceDict valueForKey:@"interval"];
        
        NSInteger _interval = [interval integerValue];
        _interval = MAX(_interval,1);
        EKRecurrenceFrequency _recurrenceFrequency = [recurrenceFrequency intValue];
        
        NSLog(@"_recurrenceFrequency=%d",_recurrenceFrequency);
        
        recurrenceRule = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:_recurrenceFrequency interval:_interval end:nil];
        
        
        //=========== RECURRENCE END==============
        NSMutableDictionary *_recurrenceEnd = [_recurrenceDict valueForKey:@"recurrenceEnd"];
        
        if([_recurrenceEnd isKindOfClass:[NSDictionary class]])
        {
           // NSNumber  * occurrenceCount = [_recurrenceEnd valueForKey:@"occurrenceCount"];
           // NSUInteger _occurrenceCount = [occurrenceCount unsignedIntegerValue];
           // _occurrenceCount = MAX(_occurrenceCount, 1);
            
           // EKRecurrenceEnd * end=[EKRecurrenceEnd recurrenceEndWithOccurrenceCount:_occurrenceCount];
            
            NSNumber  * endateInterval = [_recurrenceEnd valueForKey:@"endDate"];
            
            if(endateInterval)
            {
            NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:[endateInterval doubleValue]];
            EKRecurrenceEnd * end=[EKRecurrenceEnd recurrenceEndWithEndDate:endDate];
            recurrenceRule.recurrenceEnd = end;
            }
        }
       
    }

   return [recurrenceRule autorelease];
}

-(void)addSaveReminderCallback:(SaveReminderCallback)callback
{
    self.saveReminderCallback = callback;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
