//
//  Date.h
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol StartDelegate <NSObject>
@required
- (void)selectedStartDate:(NSString *)strStart endDate:(NSString *)strEnd;
@end
@interface Date : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    UITableViewCell *TableCell;
    UIDatePicker *datePicker;
    int _selectedRow;
    id <StartDelegate> _delegate;
    BOOL wrongSelection;
}
@property(nonatomic,retain) UITableView *aTableview;
@property(nonatomic,copy) NSString *startDate;
@property(nonatomic,copy) NSString *endDate;
@property(nonatomic, assign) id <StartDelegate> delegate;
-(void)setupTableFooterView;
@end
