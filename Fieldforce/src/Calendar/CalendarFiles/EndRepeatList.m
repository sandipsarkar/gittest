//
//  EndRepeatList.m
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EndRepeatList.h"
#import "InsetTableViewCell.h"

@implementation EndRepeatList
@synthesize delegate = _delegate;
@synthesize _tableView;
//@synthesize strRepeat;
@synthesize endRepeatDate;
@synthesize minDate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    [_tableView release],_tableView=nil;
    //[strRepeat release],strRepeat=nil;
    [endRepeatDate release],endRepeatDate=nil;
    [minDate release],minDate=nil;
    
    [datePicker release];
    
    [super dealloc];
}

#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    #ifndef __IPHONE_7_0
    float _height=self._tableView.contentSize.height;
    self.contentSizeForViewInPopover=CGSizeMake(335+15.0, _height-5.0);
    #endif
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //self.contentSizeForViewInPopover=CGSizeMake(335, 415);
    float _height=self._tableView.contentSize.height;
    
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    self.preferredContentSize=CGSizeMake(335+15.0, _height);
else
    self.contentSizeForViewInPopover=CGSizeMake(335+15.0, _height);

}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif

     [self addTitle:@"END REPEAT" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    //TableCell=nil;
    [self.navigationItem setHidesBackButton:YES animated:YES];
    CGRect _rectGrouped=self.view.bounds;
    UITableView *aTableview=[[UITableView alloc] initWithFrame:_rectGrouped style:UITableViewStyleGrouped];
    aTableview.delegate=self;
    aTableview.dataSource=self;
    aTableview.showsHorizontalScrollIndicator=NO;
    aTableview.showsVerticalScrollIndicator=NO;
    aTableview.scrollEnabled=NO;
    //aTableview.rowHeight=40;
    aTableview.autoresizingMask = UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:aTableview];
    self._tableView=aTableview;
    [aTableview release];
    
    [self setupTableFooterView];
    
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCancel setImage:[UIImage imageNamed:@"cancel_button.png"] forState:UIControlStateNormal];
    btnCancel.frame =CGRectMake(0, 0, 60, 29);
    [btnCancel addTarget:self 
                  action:@selector(cancelPopOver:) 
        forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc]initWithCustomView:btnCancel];
	self.navigationItem.leftBarButtonItem = barButtonCancel;
	[barButtonCancel release];
    
   if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
   {
    self._tableView.backgroundColor = REMINDER_BG_COLOR;
    UIButton *btnDone= [UIButton repToolButtonWithImageName:NAV_DONE_BUTTON text:@"Done"];
    btnDone.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
    [btnDone addTarget:self 
                action:@selector(saveChanges:) 
      forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithCustomView:btnDone];
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
   }
    else
    {
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(saveChanges:)];
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
   }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self._tableView.sectionFooterHeight = 1.0;
        self._tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self._tableView.bounds), 10.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        self._tableView.tableHeaderView = _headerView;
        [_headerView release];
    }

}

-(void)setupTableFooterView
{
    /* Adiing table footerview */
    float _footerHeight=216.0f;
    float _posX=0.0;
    float _posY=0.0;
    
    UIView *tableFooterView=[[UIView alloc] initWithFrame:CGRectMake(_posX, _posY, CGRectGetWidth(self._tableView.frame), _footerHeight)];
    tableFooterView.backgroundColor=[UIColor clearColor];
    //tableFooterView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    NSDateFormatter* formatter = [NSDate formatter];

    NSLog(@"self.endRepeatDate=%@",self.endRepeatDate);
    NSDate *repeatDate;
    
    if([[self.endRepeatDate componentsSeparatedByString:@","] count]>2)
    {
        [formatter setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
        
        NSDate *_date  = [formatter dateFromString:self.endRepeatDate];
        NSLog(@"_date=%@",_date);
        
        [formatter setDateFormat:@"EEE dd MMM, yyyy"];
        
        self.endRepeatDate=[formatter stringFromDate:_date];
        NSLog(@"self.startDate=%@",self.endRepeatDate);
        
        repeatDate  = [formatter dateFromString:self.endRepeatDate];
        NSLog(@"repeatDate=%@",repeatDate);
    }
    else
    {
        [formatter setDateFormat:@"EEE dd MMM, yyyy"];
        repeatDate  = [formatter dateFromString:self.endRepeatDate];
        NSLog(@"repeatDate=%@",repeatDate);
    }
    
        
    datePicker = [[UIDatePicker alloc] initWithFrame:tableFooterView.bounds];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.hidden = NO;
    datePicker.date = repeatDate;//[formatter dateFromString:_date];
    datePicker.minimumDate=self.minDate;
    datePicker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [datePicker addTarget:self
                   action:@selector(LabelChange:)
         forControlEvents:UIControlEventValueChanged];
    [tableFooterView addSubview:datePicker];
    
    
    self._tableView.tableFooterView=tableFooterView;
    
    [tableFooterView release];
}


- (void)LabelChange:(id)sender
{
    NSDateFormatter *df = [NSDate formatter];
    [df setDateFormat:@"EEE dd MMM, yyyy"];
    NSLog(@"datePicker.date=%@",datePicker.date);
    
    self.endRepeatDate = [df stringFromDate:datePicker.date];
    NSLog(@"startDate=%@",self.endRepeatDate);
    
  
    [self._tableView reloadData];
    
}
-(void)cancelPopOver:(id)sender
{
    //UIButton *btn=(UIButton *)sender;
    if([self.delegate respondsToSelector:@selector(selectedEndRepeat:)])
        [self.delegate selectedEndRepeat:nil];
    //TableCell=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)saveChanges:(id)sender
{
    //UIButton *btn=(UIButton *)sender;
    //NSString *str = TableCell.textLabel.text;
    if([self.delegate respondsToSelector:@selector(selectedEndRepeat:)])
        [self.delegate selectedEndRepeat:self.endRepeatDate];
    //TableCell=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];

    
    static NSString *CellIdentifier = @"Cell";
    
    static NSString *CellIdentifier1 = @"Cell1";
    UITableViewCell *cell=nil;
    UITableViewCell *cell1=nil;
    
    if(indexPath.section==0)
    {
       cell =(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
            cell.detailTextLabel.textColor = [UIColor blackColor];
            cell.accessoryType=UITableViewCellAccessoryNone;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
        }
    }
    else
    {
        cell1 =(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell1 == nil) 
        {
            cell1 = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
            cell1.accessoryType=UITableViewCellAccessoryNone;
            cell1.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
        }
    }
    
    switch (indexPath.section)
    {
        case 0:
        {
            cell.textLabel.text=@"End Repeat";
            cell.detailTextLabel.text=self.endRepeatDate;
            cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
        }
            break;
            
        case 1:
        {
            cell1.textLabel.text=@"Repeat Forever";
            cell1.textLabel.textAlignment=UITextAlignmentCenter;
        }
            break;
        default:
            break;
    }
    
    //    if([cell.textLabel.text isEqualToString:self.strRepeat])
    //    {
    //        UIImageView *viewSelected = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark.png"]];
    //        cell.accessoryView =viewSelected;
    //        TableCell=cell;
    //        [viewSelected release];
    //    }
    
    
    if(indexPath.section==0)
        return cell;
    else
        return cell1;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.tag = 1;
    
    
    NSInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if(TableCell!=nil)
//        TableCell.accessoryView =nil;
//    UIImageView *viewSelected = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark.png"]];
//    TableCell = [tableView cellForRowAtIndexPath:indexPath];
//    TableCell.accessoryView =viewSelected;
//    [viewSelected release];
    
    if(indexPath.section==1)
    {
        self.endRepeatDate=@"Never";
        if([self.delegate respondsToSelector:@selector(selectedEndRepeat:)])
            [self.delegate selectedEndRepeat:self.endRepeatDate];
        //TableCell=nil;
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
