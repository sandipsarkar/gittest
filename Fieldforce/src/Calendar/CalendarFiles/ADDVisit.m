//
//  ADDVisit.m
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ADDVisit.h"
#import "Customer.h"
#import "CustomerListViewController.h"
#import "Events.h"
#import "ScheduledVisit.h"
#import "CoreDataHandler.h"
#import "SSCoreDataManager.h"
#import "OfflineManager.h"
#import "VisitTypesViewcontroller.h"
#import "Visit.h"
#import "VisitSessionViewController.h"
#import "InsetTableViewCell.h"

#define currentScheduleVisitKey @"currentScheduleVisit"

#define CELL_HEIGHT 47.0
#define SEARCHBAR_HEIGHT 52

@interface ADDVisit()<VisitTypesDelegate>
{
    BOOL isVisitSessionAvailable;
}
@property(nonatomic,retain) UIPopoverController *startEndDatePopover;
@property(nonatomic,retain) UIPopoverController *customerListPopover;

@property(nonatomic,retain) NSArray *connectedVisitSessions;

-(void)showCustomersPopoverAtIndexPath:(NSIndexPath *)indexPath;

@property(nonatomic,copy) SaveScheduledVisitCallback saveScheduledVisitCallback;

@end

@implementation ADDVisit
@synthesize delegate = _delegate;
@synthesize _tableView;
@synthesize _tableViewGrouped;
@synthesize _startDate,_endDate;
@synthesize selectedIndexPath=_selectedIndexPath;
@synthesize dealerName;
@synthesize visitId;
@synthesize suburb;
@synthesize selectedStartDate;
@synthesize selectedEndDate;
@synthesize visit;
@synthesize visitingCustomer;
@synthesize startEndDatePopover;
@synthesize customerListPopover;
@synthesize saveScheduledVisitCallback;
@synthesize connectedVisitSessions;


/*
+(void)createNewVisitForTitle:(NSString *)aTitle suburbName:(NSString *)suburbName startDate:(NSDate *)aStartDate endDate:(NSDate *)aEndDate completion:(void(^)(EKEvent *event))callback
{
    if(!aTitle.length || (!aStartDate || !aEndDate))
    {
        
        callback(nil);
        return;
    }
    
    NSString *calenderIdentifier=[[NSUserDefaults standardUserDefaults] objectForKey:@"calender_identifier"];
    EKCalendar *calender = [[CustomEventStore sharedInstance] customCalendarWithIdentifier:calenderIdentifier title:@"com.reptool.calender"];

    
    //ADDED BY SANDIP TO SEARCH ANY EVENT WITH SAME START DATE AND END DATE
    NSPredicate *predicate = [[CustomEventStore sharedInstance].eventStore predicateForEventsWithStartDate:aStartDate endDate:aEndDate calendars:[NSArray arrayWithObject:calender]];
    
  
    NSString *locationStr==[AppDelegate eventLocationString];
    
    __block BOOL matched=NO;
    [[CustomEventStore sharedInstance].eventStore enumerateEventsMatchingPredicate:predicate usingBlock:^(EKEvent *event, BOOL *stop) 
     {
         if(([event.location isEqualToString:locationStr]) && ([event.notes isEqualToString:@"Visit"] || [event.notes rangeOfString:@"Visit"].location!=NSNotFound))
         {
             matched=YES;
             //[UIAlertView showWarningAlertWithTitle:@"" message:@"You have already scheduled a visit during this time.\nPlease select a diffrent date."];
             *stop=YES;
         }
     }];
    
    if(matched) 
    {
        //[UIAlertView showWarningAlertWithTitle:@"" message:@"You have already scheduled a visit during this time.\nPlease select a diffrent date."];
        
        UIAlertView *alert = [UIAlertView alertViewWithTitle:@"" message:SCHEDULE_VISIT_MSG cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObject:@"Yes"] onDismiss:^(int buttonIndex) 
            {
                
                EKEvent *event  = [EKEvent eventWithEventStore:[CustomEventStore sharedInstance].eventStore];
                
                //[[CustomEventStore sharedInstance].eventStore defaultCalendarForNewEvents]
                if(calender)
                {
                  [event setCalendar:calender];
                }
                
                event.title = aTitle.length?aTitle:@"New Schedule";//@"EVENT TITLE";
                event.startDate = aStartDate;
                event.endDate   = aEndDate;
                
                //event.notes= @"Visit";
                event.notes=suburbName.length  ? [NSString stringWithFormat:@"%@+%@",@"Visit",suburbName] : @"Visit";
               
                NSString *locationStr==[AppDelegate eventLocationString];
                NSLog(@"Location=%@", locationStr);
                
                event.location=locationStr;
                
                
                NSError *err;
               // [[CustomEventStore sharedInstance].eventStore saveEvent:event span:EKSpanThisEvent error:&err];
            [[CustomEventStore sharedInstance].eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        
                //TODO
                //ADD the event identifiers to local db also
                
               // return (!err)?event:nil;
                
                
                
               if(callback) callback((!err)?event:nil);
                   
            } 
            onCancel:^{
                                                        
                if(callback) callback(nil);                                    
            }];
        
        [alert show];
    }
    else
    {
        EKEvent *event  = [EKEvent eventWithEventStore:[CustomEventStore sharedInstance].eventStore];
        //NSString *calenderIdentifier=[[NSUserDefaults standardUserDefaults] objectForKey:@"calender_identifier"];
        //EKCalendar *calender = [[CustomEventStore sharedInstance] customCalendarWithIdentifier:calenderIdentifier title:@"com.reptool.calender"];
        //[[CustomEventStore sharedInstance].eventStore defaultCalendarForNewEvents]
        if(calender)
        {
            [event setCalendar:calender];
        }

        
        event.title = aTitle.length?aTitle:@"New Schedule";//@"EVENT TITLE";
        event.startDate = aStartDate;
        event.endDate   = aEndDate;
        
        //event.notes= @"Visit";
        event.notes=suburbName.length  ? [NSString stringWithFormat:@"%@+%@",@"Visit",suburbName] : @"Visit";
      
      // NSString *locationStr==[AppDelegate eventLocationString];
        event.location=locationStr;
        
        
        NSError *err;
       // [[CustomEventStore sharedInstance].eventStore saveEvent:event span:EKSpanThisEvent error:&err];
        [[CustomEventStore sharedInstance].eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        // return (!err)?event:nil;
        
       
        
        if(callback) callback((!err)?event:nil);

    }
    
       
   }
*/

+(void)createNewVisitForCustomerName:(NSString *)aCustomerName suburbName:(NSString *)suburbName startDate:(NSDate *)aStartDate endDate:(NSDate *)aEndDate completion:(void(^)(ScheduledVisit *event))callback
{
    [CoreDataHandler newScheduledVisitWithCustomerName:aCustomerName suburbName:suburbName startDate:aStartDate endDate:aEndDate completion:callback];
}
+(void)createNewVisitForCustomer:(Customer *)customer startDate:(NSDate *)aStartDate endDate:(NSDate *)aEndDate completion:(void(^)(ScheduledVisit *event))callback
{
    [CoreDataHandler newScheduledVisitWithCustomer:customer startDate:aStartDate endDate:aEndDate completion:callback];
}

/*
-(EKEvent *)visit
{
    if(!visit)
    {
      [visit release];
        
      EKEvent *event=[[CustomEventStore sharedInstance].eventStore eventWithIdentifier:self.visitId];
        
      visit=[event retain];
    }
    
    return visit;
}
 */


-(ScheduledVisit *)visit
{
    if(!visit)
    {
        NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"self.scheduleVisitID=%@",self.visitId];
        
        NSMutableArray *fetchedVisits = [ScheduledVisit fetchWithPredicate:fetchPredicate error:nil];
        ScheduledVisit *eventVisit=[fetchedVisits count] ? [fetchedVisits objectAtIndex:0] : nil;
        
        [visit release];
        visit=[eventVisit retain];
    }
    
    return visit;
}



-(id)initWithDealerName:(NSString *)_dealerName
{
    if(self=[super init])
    {
        self.dealerName = _dealerName;
    }
    
    return self;
}

-(id)initWithDealerName:(NSString *)_dealerName suburb:(NSString *)_suburb
{
    if(self=[super init])
    {
        self.dealerName = _dealerName;
        self.suburb=_suburb;
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    [visit release] ;
    visit=nil;
    
    [dateController release] ;
    dateController=nil;
    
    [selectedEndDate release];
    selectedEndDate =nil;
    
    [selectedStartDate release] ;
    selectedStartDate = nil;
    
    [suburb release];
    
    [_tableView release];
    _tableView=nil;
    
    [_tableViewGrouped release];
    _tableViewGrouped=nil;
    
    [arrSearchResult release];
    
    [_startDate release],_startDate=nil;
    
    [_endDate release],_endDate=nil;
    
    [_selectedIndexPath release];
    _selectedIndexPath=nil;
    
    [dealerName release];
    dealerName=nil;
    
    //[visitTittle release],visitTittle=nil;
    
    [visitId release];
     visitId=nil;
    
    [visitingCustomer release] ;
    visitingCustomer = nil;
    
    [startEndDatePopover release];
    startEndDatePopover=nil;
    
    [customerListPopover dismissPopoverAnimated:NO];
    [customerListPopover release];
    customerListPopover=nil;
    
    [saveScheduledVisitCallback release];
    
    [connectedVisitSessions release];
    
    [super dealloc];
}

#pragma mark - View lifecycle



-(void)loadCustomerNames
{
    if(!arrSearchResult) arrSearchResult  = [[NSMutableArray alloc] init];
    
     NSMutableArray *customers = [CoreDataHandler allCustomers];
     NSArray *customerNames    = [customers valueForKey:@"name"];
    [arrSearchResult setArray:customerNames];

}

-(void)setupStartEndDate
{
    if(self.visit)
    {
        self._startDate = [self.visit.visitStartDate stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
        
       //NSDate *endDate=[NSDate dateWithTimeInterval:60*60 sinceDate:self.visit.startDate];
       self._endDate=[self.visit.visitEndDate stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
    }
    else if(self._startDate==nil && self._endDate==nil)
    {
        NSDate* date = [NSDate date];
        
        self._startDate = [date stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
        
        NSDate *endDate=[NSDate dateWithTimeInterval:60*60 sinceDate:date];
        self._endDate=[endDate stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
        
        /*
        //Create the dateformatter object
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        //Set the required date format
        [formatter setDateFormat:@"EEE dd MMM,"];
        //Get the string date
        self._startDate = [formatter stringFromDate:date];
        
        [formatter setDateFormat:@"yyyy"];
        NSString *myYearString = [formatter stringFromDate:date];
        NSLog(@"myYearString=%@",myYearString);
        self._startDate=[self._startDate stringByAppendingFormat:@" %@",myYearString];
        NSLog(@"self._startDate=%@",self._startDate);
        
        [formatter setDateFormat:@"h.mm"];
        float time=[[formatter stringFromDate:date] floatValue];
        int abs=ceilf(time);
        abs=(abs!=12)?abs%12:abs;
        
        NSString *localString=[NSString stringWithFormat:@"%d:00",abs];
        self._startDate=[self._startDate stringByAppendingFormat:@", %@",localString];
        
        [formatter setDateFormat:@"a"];
        self._startDate=[self._startDate stringByAppendingFormat:@" %@",[formatter stringFromDate:date]];
        NSLog(@"self._startDate=%@",self._startDate);
        
        [formatter setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
        NSDate *_date=[formatter dateFromString:self._startDate];
        NSLog(@"_date=%@",_date);
        
        _date=[NSDate dateWithTimeInterval:60*60 sinceDate:_date];
        
        self._endDate=[formatter stringFromDate:_date];
        NSLog(@"self._endDate=%@",self._endDate);
        
        
        [formatter release];
         */
    }

}

-(NSArray *)loadConnectedVisitSessions
{
    if(!self.connectedVisitSessions)
    {
        
    NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"self.scheduledVisit.scheduleVisitID=%@",self.visit.scheduleVisitID];
    NSArray *_visits = [Visit fetchWithPredicate:fetchPredicate onAttributes:nil sortDescriptors:nil limit:1 error:nil];
    self.connectedVisitSessions = _visits;
        
    }
    
return self.connectedVisitSessions;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
   if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
  
    
    [self addTitle:self.visitId ? @"EDIT VISIT" : @"NEW VISIT" withFont:[UIFont subaruBoldFontOfSize:23.0]]; 
    
    TableCell=nil;
    self.view.backgroundColor = [UIColor whiteColor];
    self.selectedIndexPath = nil;
    
   // [self loadCustomerNames];
    
    if(!self.dealerName)
    self.dealerName=self.visit.customerName;
    
    //self.dealerName=self.visit.title;
    
    [self setupStartEndDate];
        
    /*
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCancel setImage:[UIImage imageNamed:@"cancel_button.png"] forState:UIControlStateNormal];
    btnCancel.frame =CGRectMake(0, 0, 60, 29);
    [btnCancel addTarget:self 
                       action:@selector(cancelPopOver:) 
             forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc]initWithCustomView:btnCancel];
	self.navigationItem.leftBarButtonItem = barButtonCancel;
	[barButtonCancel release];
     */
NSLog(@"Dealer Name:%@",self.dealerName);
    
    UIBarButtonItem *barButtonDone = nil;
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{

    UIButton *btnDone= [UIButton repToolButtonWithImageName:NAV_DONE_BUTTON text:@"Save"];
    btnDone.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
    [btnDone addTarget:self
                action:@selector(saveChanges:)
      forControlEvents:UIControlEventTouchUpInside];
    btnDone.enabled = self.dealerName.length>0 ;
    barButtonDone = [[UIBarButtonItem alloc]initWithCustomView:btnDone];
    barButtonDone.enabled = self.dealerName.length>0;
    
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
    
    if(!self.visitId && [self.navigationController.viewControllers count]>1)
    [self addBackButtonWithImage:[UIImage imageNamed:NAV_BACK_BUTTON] title:@"Back" selector:nil];

    
}
else
{
    
    barButtonDone = [[UIBarButtonItem alloc]initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveChanges:)];
    barButtonDone.enabled = self.dealerName.length>0;
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
}
    
    CGFloat locY=0;
    
    //SEARCH HAS BEEN DISABLED HERE. NEED TO ACTIVATE IT
    
    /*
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, locY, self.view.bounds.size.width, SEARCHBAR_HEIGHT)];
    searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    //searchBar.backgroundImage=[UIImage imageNamed:@"calendar_search_bar.png"];
    searchBar.delegate=self;
   // [self.view addSubview:searchBar];
    [searchBar release];
    */
    
    //SEARCH HAS BEEN DISABLED HERE. NEED TO ACTIVATE
    
    //locY=CGRectGetMaxY(searchBar.frame);
    //CGRect _rect=CGRectMake(0,locY, self.view.bounds.size.width, MIN(([arrSearchResult count]+0)*CELL_HEIGHT, 376) ); //
    
    isVisitSessionAvailable = NO;
    
    BOOL _hasVisitSession = NO;
    
    if(self.visit)
    {
        NSArray *_visits =[self loadConnectedVisitSessions];
        
        _hasVisitSession = [_visits count]>0;
        if(_hasVisitSession || [self.visit.isVisitPublished boolValue]) isVisitSessionAvailable=YES;
        
       // if(_hasVisitSession) isVisitSessionAvailable=YES;
        
        if(!self.visit.customer)
        {
            NSLog(@"Visit CustomerID=%@",self.visit.customerID);
            
            NSPredicate *customerPredicte = [NSPredicate predicateWithFormat:@"self.customerID=%@",self.visit.customerID];
            
           NSMutableArray *customers= [Customer fetchWithPredicate:customerPredicte sortDescriptors:nil limit:1 error:nil];
            
            if([customers count])
            {
                Customer *_customer = [customers objectAtIndex:0];
                //self.visit.customer = _customer;
                
                //[self.visit.customer removeScheduledVisitsObject:self.visit];
                [_customer addScheduledVisitsObject:self.visit];
                self.visit.customerID    = _customer.customerID;
                self.visit.customerName  = _customer.name;
                
                if(!self.visitingCustomer) self.visitingCustomer = _customer;
                
                
                [[SSCoreDataManager sharedManager] save:nil];
            }
        }
        
          NSLog(@"Visit CustomerID=%@",self.visit.customerID);
    }

    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        UIButton *button = (UIButton *) self.navigationItem.rightBarButtonItem.customView;
        button.enabled = self.dealerName.length>0 && !isVisitSessionAvailable;
        button.alpha = self.dealerName.length>0 && !isVisitSessionAvailable ? 1 : 0.5;
    }
    else
    {
        self.navigationItem.rightBarButtonItem.enabled = self.dealerName.length>0 && !isVisitSessionAvailable;
    }
    
    //
    CGRect _rect=self.view.bounds;
    UITableView *aTableview=[[UITableView alloc] initWithFrame:_rect style:UITableViewStyleGrouped];
    aTableview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    aTableview.delegate=self;
    aTableview.dataSource=self;
    aTableview.bounces=NO;
    aTableview.tag=10;
     aTableview.backgroundColor=[UIColor groupTableViewBackgroundColor];
    //aTableview.backgroundColor=[UIColor clearColor];
    [self.view addSubview:aTableview];
    self._tableView=aTableview;
    [aTableview release];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        aTableview.sectionFooterHeight = 1.0;
        aTableview.sectionHeaderHeight = 5.0;
        aTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(aTableview.bounds), 10.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        aTableview.tableHeaderView = _headerView;
        [_headerView release];
    }

    
    /*
    locY=CGRectGetMaxY(_rect);
    CGRect _rectGrouped=CGRectMake(0,locY, self.view.bounds.size.width, 200.0);
    
    UITableView *aTableviewGrouped=[[UITableView alloc] initWithFrame:_rectGrouped style:UITableViewStyleGrouped];
    aTableviewGrouped.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
    aTableviewGrouped.scrollEnabled=YES;
    aTableviewGrouped.delegate=self;
    aTableviewGrouped.dataSource=self;
    aTableviewGrouped.tag=11;
    [self.view addSubview:aTableviewGrouped];
    self._tableViewGrouped=aTableviewGrouped;
    [aTableviewGrouped release];
     
    [self performSelector:@selector(launchWithStartEndDateContoller) withObject:nil afterDelay:0.1];
    */
    
    CGFloat footerHeight = (self.visitId!=nil)? 440.0 : 326;
    UIView *footerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0,335, footerHeight)];
    
    dateController = [[Start_EndDate alloc] initWithStartDate:self.visit.visitStartDate endDate:self.visit.visitEndDate];
    //dateController.delegate=self;
    dateController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    dateController.view.backgroundColor  = [UIColor clearColor];
    dateController.view.frame = CGRectMake(0, 0, CGRectGetWidth(footerView.frame), 330.0);
    [footerView addSubview:dateController.view];
    
    __block ADDVisit *weakSelf = self;
    [dateController addDateDidChangeCallback:^(NSDate *startDate, NSDate *endDate)
     {
         barButtonDone.enabled = weakSelf.dealerName.length>0;
         weakSelf._startDate = [startDate stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
         weakSelf._endDate   = [endDate stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
     }];
    
    dateController.view.userInteractionEnabled= !isVisitSessionAvailable;
    dateController.view.alpha =  isVisitSessionAvailable?0.8:1.0;

    
    if(self.visitId!=nil) //ONLY IN EDIT MODE
    {
       
        locY = CGRectGetMaxY(dateController.view.frame);
        
        UIButton * visitSessionButton=[UIButton repToolButtonWithImageName:@"green_visit_button.png" text:@"START SESSION"];
        CGRect _visitSessionButtonFrame = visitSessionButton.frame;
        _visitSessionButtonFrame.origin.x = 14.0;
        _visitSessionButtonFrame.origin.y = locY;
        
        visitSessionButton.frame = _visitSessionButtonFrame;
        [visitSessionButton addTarget:self action:@selector(addVisitSessionAction:) forControlEvents:UIControlEventTouchUpInside];
        visitSessionButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [footerView addSubview:visitSessionButton];
        
        if(isVisitSessionAvailable)
        {
            //[visitSessionButton setTitle:@"VIEW SESSION" forState:UIControlStateNormal];
            
            [visitSessionButton setTitle:[self.visit.isVisitPublished boolValue] ? @"REPORT PUBLISHED" : @"VIEW SESSION" forState:UIControlStateNormal];
            
            visitSessionButton.userInteractionEnabled = _hasVisitSession;
        }
        
        
        locY = CGRectGetMaxY(_visitSessionButtonFrame)+5.0;
        
        deleteButton=[UIButton repToolButtonWithImageName:@"red_button_small.png" text:@"DELETE VISIT"];
        CGRect _deleteFrame = deleteButton.frame;
        _deleteFrame.origin.x = 14.0;
        _deleteFrame.origin.y = locY;
        
        deleteButton.frame = _deleteFrame;
        [deleteButton addTarget:self action:@selector(deleteVisitAction:) forControlEvents:UIControlEventTouchUpInside];
        deleteButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [footerView addSubview:deleteButton];
        deleteButton.enabled = !isVisitSessionAvailable;
    }
    
    self._tableView.tableFooterView=footerView;
    [footerView release];
    
    //========== LAUNCH WITH CUSTOMER POPOVER SELECTED =========== //
    
    if(self.visitId==nil)
    {
      //NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
      //[self performSelector:@selector(showCustomersPopoverAtIndexPath:) withObject:indexPath afterDelay:0.1];
      //[self showCustomersPopoverAtIndexPath:indexPath];
    }
    
    //========== LAUNCH WITH CUSTOMER POPOVER SELECTED =========== //
}

-(void)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
//[self performBlock:^{
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    
    self.preferredContentSize=CGSizeMake(335+15.0, MAX(390,self._tableView.contentSize.height));
else
    self.contentSizeForViewInPopover = CGSizeMake(335+15.0, MAX(389,self._tableView.contentSize.height-1.0));

  //} afterDealy:0.0];

}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];


if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        self.preferredContentSize=CGSizeMake(335+15.0, MAX(391,self._tableView.contentSize.height+1));
else
        self.contentSizeForViewInPopover = CGSizeMake(335+15.0, MAX(390,self._tableView.contentSize.height));




}

-(void)viewWillDisappear:(BOOL)animated
{
    //self.title = nil;
}


-(void)launchWithStartEndDateContoller
{
     [self tableView:self._tableViewGrouped didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

/*
-(void)deleteVisitAction:(id)sender
{
    EKEvent *event=self.visit;
    if (event != nil) 
    {  
        NSArray *_eventsToDelete = [Events fetchWithPredicate:[NSPredicate predicateWithFormat:@"self.eventIdentifier = %@",event.eventIdentifier] error:nil];
        for(Events *event in _eventsToDelete)
        {
            [SSCoreDataManager deleteObject:event];
        }
        
        [[SSCoreDataManager sharedManager] save:nil];
        
        
        NSError* error = nil;
        [[CustomEventStore sharedInstance].eventStore removeEvent:event span:EKSpanFutureEvents error:&error];
        
        //EKSpanFutureEvents
        NSLog(@"error=%@",[error localizedDescription]);
        
        if(!error)
        {
            // [self.popoverController dismissPopoverAnimated:YES];
            if([self.delegate respondsToSelector:@selector(dismissPopOverFromViewVisit:)])
                [self.delegate dismissPopOverFromViewVisit:YES];
        }
        
    } 
    self.visitId=nil;
    
    
}
 */

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    /*
    if(tableView.tag==10)
        return 1;
    else
    {
        if ([self.title isEqualToString:@"SCHEDULE VISIT"]) 
            return 1;
        return 2;
    }*/
    
    return 1;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    if(tableView.tag==10)         return CELL_HEIGHT;
    else
    {
        if (indexPath.section==0)  return 70.0;
        
        return CELL_HEIGHT;
    }
     */
    
    return CELL_HEIGHT;
        
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    /*
    if(tableView.tag==10)
        return [arrSearchResult count];
    else
        return 1;
     */
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }

    cell.textLabel.text = self.dealerName ? [self.dealerName uppercaseString] : @"Select Customer";
    
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    cell.accessoryType = isVisitSessionAvailable ? UITableViewCellAccessoryNone : UITableViewCellAccessoryDisclosureIndicator  ;
else
{
    cell.accessoryView = isVisitSessionAvailable ? nil : [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    cell.accessoryView.userInteractionEnabled = NO;
}
    
    
    
    cell.userInteractionEnabled = !isVisitSessionAvailable;
    cell.textLabel.alpha = isVisitSessionAvailable?0.5:1.0;
    
    [self tableView:aTableView willAddBGForCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath

-(void)tableView:(UITableView *)tableView willAddBGForCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];

    
    
    
    
    NSInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    
    cell.backgroundView = _bgView;

    
    [_bgView release];

}

#pragma Launch Start_EndDate controller on startup
-(void)beginWithStartEndDateController
{
    Start_EndDate *alert = [[Start_EndDate alloc] init];
    alert.isFromCustomer=YES;
    alert.delegate=self;
    [self.navigationController pushViewController:alert animated:NO];
    [alert release];

}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section==0 && indexPath.row==0)
    {
       [self showCustomersPopoverAtIndexPath:indexPath];
    }
} 

-(void)showCustomersPopoverAtIndexPath:(NSIndexPath *)indexPath
{
    
    Track=1;
    CustomerListViewController *_customerListViewController=[[CustomerListViewController alloc] initWithCustomerName:self.dealerName];
    
    [_customerListViewController addCustomerSelectCallback:^(Customer *customer)
     {
         self.visitingCustomer = customer;
         self.dealerName = customer.name;
         self.suburb = customer.suburb.length ? customer.suburb : customer.city;
         [self._tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation: UITableViewRowAnimationNone];
         
         //[btnDoneHold setImage:[UIImage imageNamed:@"done_button.png"] forState:UIControlStateNormal];
        // btnDoneHold.userInteractionEnabled=YES;
         self.navigationItem.rightBarButtonItem.enabled = YES;
         
         [self.navigationController popViewControllerAnimated:YES];
         
         self.navigationItem.rightBarButtonItem.enabled = customer!=nil;
         
     }];

    [self.navigationController pushViewController:_customerListViewController animated:YES];
    [_customerListViewController release];
   
    /*
    if(self.customerListPopover &&  [self.customerListPopover isPopoverVisible]) 
    {
        return;
    }
    
        
    UIPopoverController *_popOver = self.customerListPopover;
    if(!_popOver)
    {
        CustomerListViewController *_customerListViewController=[[CustomerListViewController alloc] initWithCustomerName:self.dealerName];
        [_customerListViewController addCustomerSelectCallback:^(Customer *customer)
         {
             self.dealerName = customer.name;
             self.suburb = customer.suburb.length ? customer.suburb : customer.city;
             [self._tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation: UITableViewRowAnimationNone];
             
             [btnDoneHold setImage:[UIImage imageNamed:@"done_button.png"] forState:UIControlStateNormal];
             btnDoneHold.userInteractionEnabled=YES;
             
             [self.customerListPopover dismissPopoverAnimated:YES];
         }];

    
    _popOver =[[UIPopoverController alloc] initWithContentViewController:_customerListViewController];
    _popOver.passthroughViews     = [NSArray arrayWithObjects:self.view,self.navigationController.navigationBar,nil];
    self.customerListPopover=_popOver;
    
     _customerListViewController.popoverController=_popOver;
     [_popOver release];
     [_customerListViewController release];
        
    }
    
    CGRect _targetRect = [self._tableView rectForRowAtIndexPath:indexPath];
    [_popOver presentPopoverFromRect:_targetRect inView:self._tableView permittedArrowDirections:UIPopoverArrowDirectionRight | UIPopoverArrowDirectionLeft animated:YES];
     */
}

-(void)cancelPopOver:(id)sender
{
    
    TableCell=nil;
    
    if(self.popoverController)
    [self.popoverController dismissPopoverAnimated:YES];
    
    if(self.customerListPopover)
    [self.customerListPopover dismissPopoverAnimated:YES];
    if([self.delegate respondsToSelector:@selector(dismissPopOverFromViewVisit:)])
        [self.delegate dismissPopOverFromViewVisit:NO];
}


/*
-(void)saveChanges:(id)sender
{
    Events *visitEvent = nil; 
    if(self.visitId!=nil)
    {
        EKEvent *event=self.visit;
        if (event != nil) 
        {  
            NSArray *_eventsToDelete = [Events fetchWithPredicate:[NSPredicate predicateWithFormat:@"self.eventIdentifier = %@",event.eventIdentifier] error:nil];
            for(Events *event in _eventsToDelete)
            {
                visitEvent = event;
                break;
                //[SSCoreDataManager deleteObject:event];
            }

            
            NSError* error = nil;
            //[[CustomEventStore sharedInstance].eventStore removeEvent:event span:EKSpanThisEvent error:&error];
            [[CustomEventStore sharedInstance].eventStore removeEvent:event span:EKSpanFutureEvents error:&error];
            NSLog(@"error=%@",[error localizedDescription]);
            
            
                       
            //[[SSCoreDataManager sharedManager] save:nil];
        } 
        self.visitId=nil;
    }
    
    
 
    NSDate *__startDate =[NSDate dateFromString:self._startDate withFormat:@"EEE dd MMM, yyyy, h:mm a"];
    NSDate *__endDate  = [NSDate dateFromString:self._endDate withFormat:@"EEE dd MMM, yyyy, h:mm a"];    
    

    
      
    [ADDVisit createNewVisitForTitle:self.dealerName.length?self.dealerName:@"New visit" suburbName:self.suburb.length?self.suburb:@"" startDate:__startDate endDate:__endDate completion:^(EKEvent *event) 
    {
        NSLog(@"New visit:%@",event);
        
        if(visitEvent) 
            visitEvent.identifier = event.identifier;
        else 
        {
            Events *visitEvent = (Events *)[Events insertNewObject];
            visitEvent.identifier = event.eventIdentifier;
            visitEvent.eventID =  [NSNumber numberWithUnsignedInteger:[[NSDate date] hash]];
            
        }
        
        [[SSCoreDataManager sharedManager] save:nil];
        
        
        TableCell=nil;
        if(self.customerListPopover.popoverVisible) [self.customerListPopover dismissPopoverAnimated:YES];
        if([self.delegate respondsToSelector:@selector(dismissPopOverFromViewVisit:)])
            [self.delegate dismissPopOverFromViewVisit:YES];
    }];
    
    
    //TableCell=nil;
   // if([self.delegate respondsToSelector:@selector(dismissPopOverFromViewVisit:)])
   // [self.delegate dismissPopOverFromViewVisit:YES];
}
 */

-(void)deleteVisitAction:(id)sender
{
    ScheduledVisit *event=self.visit;
    if (event != nil) 
    {  

        [[OfflineManager sharedManager] addScheduledVisit:event willDelete:YES isEditMode:NO];

        
        NSLog(@"Event identifier:%@",event.eventIdentifier);
        
       //  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         if([CustomEventStore deleteEventForEventIdentifier:event.eventIdentifier])
           {
              NSLog(@"EKEvent Deleted");
           }
             
       //  dispatch_async(dispatch_get_main_queue(), ^{
        

        if([self.delegate respondsToSelector:@selector(didChangeVisit: actionType:)])
            [self.delegate didChangeVisit:event actionType:3];

        
            if(self.saveScheduledVisitCallback)
             self.saveScheduledVisitCallback(event,3,nil);

        
            [SSCoreDataManager deleteObject:event];
            [[SSCoreDataManager sharedManager] save:nil];
             

        
            if([self.delegate respondsToSelector:@selector(dismissPopOverFromViewVisit:)])
                 [self.delegate dismissPopOverFromViewVisit:YES];
             
       //   });
      //  });
        
        
    } 
   // self.visitId=nil;
    
}

-(void)saveChanges:(id)sender
{
    [sender setEnabled:NO];
    
    NSDate *__startDate = [NSDate dateFromString:self._startDate withFormat:@"EEE dd MMM, yyyy, h:mm a"];
    NSDate *__endDate   = [NSDate dateFromString:self._endDate withFormat:@"EEE dd MMM, yyyy, h:mm a"];  
    
    if(__startDate)
        __startDate = [__startDate currentDateHourMinute];
    
    if(__endDate)
        __endDate = [__endDate currentDateHourMinute];
    
    NSString *customerName = self.dealerName.length ? self.dealerName: @"New visit";
    NSString *suburbName   = self.suburb.length ? self.suburb: @"";

    //============== EDIT VISIT HERE ==================//
    if(self.visitId!=nil)
    {
        ScheduledVisit *event  = self.visit;
        NSDate *_lastStartDate = nil;

        if(event != nil) 
        {  
            _lastStartDate = [event.visitStartDate copy];
            event.visitStartDate = __startDate;
            event.visitEndDate =   __endDate;
            event.customerName = customerName;
            event.stateName = suburbName;
            
            if(self.visitingCustomer)
            {
              event.customerID   = self.visitingCustomer.customerID;
              [event.customer removeScheduledVisitsObject:event];
              [self.visitingCustomer addScheduledVisitsObject:event];
              // event.customer     = self.visitingCustomer;
               event.customerName = self.visitingCustomer.name;
            }
            
            //========= CHECK ANY CUSTOMER DEALING WITH ANY DEALER SITE OEMS ===========
            if(self.visitingCustomer && ![CoreDataHandler checkDealersiteDealsCustomer:self.visitingCustomer withAlertTitle:nil message:nil])
            {
                [_lastStartDate release];
                return;
            }
            //========= CHECK ANY CUSTOMER DEALING WITH ANY DEALER SITE OEMS =============
            
            
            self.visitId=nil;
            
             NSString *eventTitle = [NSString stringWithFormat:@"Visit: %@",self.visitingCustomer.name?self.visitingCustomer.name:event.customerName];
            
       
          //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async([app eventQueue],^{
                
           EKEvent *calEvent=  [CustomEventStore editEventForIdentifier:event.eventIdentifier withTitle:eventTitle startDate:__startDate endDate:__endDate location:nil notes:nil];
            
#if CREATE_EVENT_ON_EDIT
            if(!calEvent)
            {
                calEvent= [CustomEventStore addEventWithTitle:eventTitle startDate:__startDate endDate:__endDate location:nil notes:nil];
                NSLog(@"Event identifier=%@",calEvent.eventIdentifier);
            }
#endif
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
            if(calEvent)
            event.eventIdentifier = calEvent.eventIdentifier;
                
            [[SSCoreDataManager sharedManager] save:nil];
               
          //  });
          //  });
            
            

            [[OfflineManager sharedManager] addScheduledVisit:event willDelete:NO isEditMode:YES];

            
            //send request here
            
            TableCell=nil;
            if(self.customerListPopover.popoverVisible) [self.customerListPopover dismissPopoverAnimated:YES];
            

            if([self.delegate respondsToSelector:@selector(didChangeVisit:actionType:)])
                [self.delegate didChangeVisit:event actionType:2];
            
            if(self.saveScheduledVisitCallback)
             self.saveScheduledVisitCallback(event,2,_lastStartDate);

            
            if([self.delegate respondsToSelector:@selector(dismissPopOverFromViewVisit:)])
                [self.delegate dismissPopOverFromViewVisit:YES];
            
            if([self.delegate respondsToSelector:@selector(didSaveWithVisit: actionType:)])
                [self.delegate didSaveWithVisit:event actionType:2];
            
           
            
            if(_lastStartDate) [_lastStartDate release];
                
             });
          });

        } 
        
    }
    else //====================== CREATE NEW VISIT HERE   =============================
    {
        
/*
        [ADDVisit createNewVisitForCustomer:self.visitingCustomer startDate:__startDate endDate:__endDate completion:^(ScheduledVisit *event)
         {
           //  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 
                 NSString *eventTitle=[NSString stringWithFormat:@"Visit:%@",self.visitingCustomer.name];
                 EKEvent *_event = [CustomEventStore addEventWithTitle:eventTitle startDate:__startDate endDate:__endDate location:nil notes:nil];
                 
               //  dispatch_async(dispatch_get_main_queue(), ^{
                     
                     event.eventIdentifier = _event.eventIdentifier;
                     NSLog(@"Event identifier=%@",_event.eventIdentifier);
                     [[SSCoreDataManager sharedManager] save:nil];
                // });
             //});
             

             [[OfflineManager sharedManager] addScheduledVisit:event willDelete:NO isEditMode:NO];

            
             TableCell=nil;
             if(self.customerListPopover.popoverVisible) [self.customerListPopover dismissPopoverAnimated:YES];
             if([self.delegate respondsToSelector:@selector(dismissPopOverFromViewVisit:)])
                 [self.delegate dismissPopOverFromViewVisit:YES];
        }];
        
*/        
        ///in thread
        
        //========= CHECK ANY CUSTOMER DEALING WITH ANY DEALER SITE OEMS ===========
        if(self.visitingCustomer && ![CoreDataHandler checkDealersiteDealsCustomer:self.visitingCustomer withAlertTitle:nil message:nil])
        {
            return;
        }
        //========= CHECK ANY CUSTOMER DEALING WITH ANY DEALER SITE OEMS =============
        
        [ADDVisit createNewVisitForCustomer:self.visitingCustomer startDate:__startDate endDate:__endDate completion:^(ScheduledVisit *event)
         {
            
             NSString *eventTitle = [NSString stringWithFormat:@"Visit: %@",self.visitingCustomer.name?self.visitingCustomer.name:@"visit"];
             
             NSLog(@"EventFetchCount=%d",app.eventFetchCount);
             
            // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
             
             dispatch_async([app eventQueue],^{
                 
            EKEvent *_event = nil;
            //if(app.eventFetchCount>=2)
             _event = [CustomEventStore addEventWithTitle:eventTitle startDate:__startDate endDate:__endDate location:nil notes:nil];
             
              dispatch_async(dispatch_get_main_queue(), ^{
             
             if(_event)
             event.eventIdentifier = _event.eventIdentifier;
             NSLog(@"Event identifier=%@",_event.eventIdentifier);
             
             [[SSCoreDataManager sharedManager] save:nil];
                  
                });
            });
             
              [[SSCoreDataManager sharedManager] save:nil];
             

            [[OfflineManager sharedManager] addScheduledVisit:event willDelete:NO isEditMode:NO];

                  
                  TableCell=nil;
                  if(self.customerListPopover.popoverVisible) [self.customerListPopover dismissPopoverAnimated:YES];
             

             if([self.delegate respondsToSelector:@selector(didChangeVisit:actionType:)])
                 [self.delegate didChangeVisit:event actionType:1];

             
                  if([self.delegate respondsToSelector:@selector(dismissPopOverFromViewVisit:)])
                      [self.delegate dismissPopOverFromViewVisit:YES];
             
             if(self.saveScheduledVisitCallback)
                 self.saveScheduledVisitCallback(event,1,nil);

           //   });
           //  });
             
         }];
    }
}

-(void)startVisitSessionForScheduleVisit:(ScheduledVisit *)aScheduleVisit
{
    //=========== CHECK CUSTOMER DEALING WITH ANY OEM ===================
    NSArray *arrDealingOEMs = [aScheduleVisit.customer.dealingOEMs componentsSeparatedByString:@","];
    
    NSArray *dealingOEMS = [CoreDataHandler intersectWithDealingOEMs:arrDealingOEMs];
    
    if(![dealingOEMS count])
    {
        [UIAlertView showWarningAlertWithTitle:SCHEDULE_VISIT_FAIL_TITLE message:SCHEDULE_VISIT_FAIL_MSG];
        return;
    }
    //========================================================================

    
    NSArray *_visits =[self loadConnectedVisitSessions];
    
    if([_visits count])
    {
        if([self.delegate respondsToSelector:@selector(dismissPopOverFromViewVisit:)])
            [self.delegate dismissPopOverFromViewVisit:YES];
        
        Visit *_visit=[_visits objectAtIndex:0];
        
        Customer *_customer = aScheduleVisit.customer?aScheduleVisit.customer:self.visitingCustomer?self.visitingCustomer:nil;
        
        VisitSessionViewController *visitReviewSessionVC=[[VisitSessionViewController alloc] initWithVisit:_visit forCustomer:_customer];
        visitReviewSessionVC.isFromCalendar=YES;
        
        UINavigationController *controller=(UINavigationController *) app.splitViewController.modalViewController;
        if(controller)
            [controller pushViewController:visitReviewSessionVC animated:YES];
        [visitReviewSessionVC release];
        
        return;
    }
    
    if(aScheduleVisit)
    {
        [self setAssociateObject:aScheduleVisit forKey:currentScheduleVisitKey];
    }
    
    VisitTypesViewcontroller *visitTypesVC=[[VisitTypesViewcontroller alloc] init];
    visitTypesVC.isReadonlyMode=NO;
   // visitTypesVC.delegate = self;
    //visitTypesVC.contentSizeForViewInPopover=CGSizeMake(320, 285.0);
    
    
    __block ADDVisit *weakSelf = self;
    [visitTypesVC addNatureOfVisitDidSelectCallback:^(NSInteger visitNatureID)
     {
         [weakSelf createVisitSessionForScheduleVisit:aScheduleVisit visitNatureID:visitNatureID];
     }];
    
    
    [self.navigationController pushViewController:visitTypesVC animated:YES];
    [visitTypesVC release];
}

/*
-(void)visitTypesViewcontroller:(VisitTypesViewcontroller*)visitTypesVC didSelectNatureOfVisit:(NSInteger) visitNatureID
{
    ScheduledVisit *aScheduleVisit = [self associateObjectForKey:currentScheduleVisitKey];
    
    [self createVisitSessionForScheduleVisit:aScheduleVisit visitNatureID:visitNatureID];
}
*/

-(void)createVisitSessionForScheduleVisit:(ScheduledVisit *)aScheduleVisit visitNatureID:(NSInteger)visitNatureID
{
    if(!aScheduleVisit)
    {
        NSDate *startDate = [NSDate date];
        NSDate *endDate = [startDate dateByAddingTimeInterval:60*60*1];
        
        if(startDate)
            startDate = [startDate currentDateHourMinute];
        
        if(endDate)
            endDate = [endDate currentDateHourMinute];
        
        [ADDVisit createNewVisitForCustomer:aScheduleVisit.customer startDate:startDate endDate:endDate completion:^(ScheduledVisit *event)
         {
             //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
             
             dispatch_async([app eventQueue], ^{
                 
                 NSString *eventTitle = [NSString stringWithFormat:@"Visit: %@",aScheduleVisit.customer?aScheduleVisit.customer.name:@"visit"];
                 
                 EKEvent *_event = [CustomEventStore addEventWithTitle:eventTitle  startDate:startDate endDate:endDate location:nil notes:nil];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     event.eventIdentifier = _event.eventIdentifier;
                     
                     /// NSLog(@"Event identifier=%@",_event.eventIdentifier);
                     // [[SSCoreDataManager sharedManager] save:nil];
                 });
             });
             
             // [self.currentPopoverController dismissPopoverAnimated:YES];
             event.isAutoGenerated = [NSNumber numberWithBool:YES];
             
             [[SSCoreDataManager sharedManager] save:nil];

             [[OfflineManager sharedManager] addScheduledVisit:event willDelete:NO isEditMode:NO];

             
             Visit *_newVisit = [CoreDataHandler newVisit];
             _newVisit.natureOfVisitID = [NSNumber numberWithInteger:visitNatureID];
             _newVisit.parentCustomer = aScheduleVisit.customer;
             //_newVisit.scheduleVisitID = event.scheduleVisitID;
             _newVisit.scheduledVisit = event;
             _newVisit.startDate      = [NSDate date];
             
             
             //SET SCHEDULE VISIT ID TO _newVisit HERE
             [aScheduleVisit.customer addVisitsObject:_newVisit];
             
             event.reviewedVisitID = _newVisit.visitID;
             
             /// ====================== SET LAST VISIT DATE ===========================
             
             NSString * _lastVisitdateString  = [[NSDate date] stringFromDateWithFormat:CUSTOMER_LAST_VISIT_FORMAT];
             aScheduleVisit.customer.lastVisit=_lastVisitdateString;
             
             //  VisitReviewSessionViewController *visitReviewSessionVC=[[VisitReviewSessionViewController alloc] initWithVisit:_newVisit forCustomer:self.customer];
             
             Customer *_customer = aScheduleVisit.customer?aScheduleVisit.customer:self.visitingCustomer?self.visitingCustomer:nil;
             
             VisitSessionViewController *visitReviewSessionVC=[[VisitSessionViewController alloc] initWithVisit:_newVisit forCustomer:_customer];
             visitReviewSessionVC.isFromCalendar=YES;
             
             UINavigationController *controller=(UINavigationController *) app.splitViewController.modalViewController;
             if(controller)
                 [controller.navigationController pushViewController:visitReviewSessionVC animated:YES];
             [_newVisit release];
             [visitReviewSessionVC release];
             
             /*
              UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:visitReviewSessionVC];
              [app.splitViewController presentModalViewController:navController animated:YES];
              [visitReviewSessionVC release];
              [navController release];
              
              [self.customerVisitDetailsController reloadData];
              [_newVisit release];
              */
             
            
             //[_navController release];
             
         }];
    }
    else
    {
        // [self.currentPopoverController dismissPopoverAnimated:YES];
        
        Visit *_newVisit=[CoreDataHandler newVisit];
        _newVisit.natureOfVisitID = [NSNumber numberWithInteger:visitNatureID];
        _newVisit.parentCustomer=aScheduleVisit.customer;
        _newVisit.scheduledVisit = aScheduleVisit;
        aScheduleVisit.reviewedVisitID = _newVisit.visitID;
        // NSString *_dateString  = [[NSDate date] stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
        _newVisit.startDate=[NSDate date];
        
        [aScheduleVisit.customer addVisitsObject:_newVisit];
        
        //===== SHIFT THE DATE OF SCHEDULED VISIT TO CURRENT DATE=====
        NSDate *startDate = _newVisit.startDate;
        NSDate *endDate = [startDate dateByAddingTimeInterval:60*60*1];
        
        if(startDate)
            startDate = [startDate currentDateHourMinute];
        
        if(endDate)
            endDate = [endDate currentDateHourMinute];
        
        
        NSDate *_lastStartDate = aScheduleVisit.visitStartDate;
        aScheduleVisit.visitStartDate = startDate;
        aScheduleVisit.visitEndDate = endDate;
        
        [[OfflineManager sharedManager] addScheduledVisit:aScheduleVisit willDelete:NO isEditMode:YES];
        
        if(self.saveScheduledVisitCallback)
            self.saveScheduledVisitCallback(aScheduleVisit,2,_lastStartDate);
        
               
        //===========================================================
        
        
        
        /// ============== SET LAST VISIT DATE ===============
        
        NSString * _lastVisitdateString  = [[NSDate date] stringFromDateWithFormat:@"dd/MM/yyyy"];
        aScheduleVisit.customer.lastVisit=_lastVisitdateString;
        
        //  VisitReviewSessionViewController *visitReviewSessionVC=[[VisitReviewSessionViewController alloc] initWithVisit:_newVisit forCustomer:self.customer];
        
        Customer *_customer = aScheduleVisit.customer?aScheduleVisit.customer:self.visitingCustomer?self.visitingCustomer:nil;
        
        VisitSessionViewController *visitReviewSessionVC=[[VisitSessionViewController alloc] initWithVisit:_newVisit forCustomer:_customer];
        visitReviewSessionVC.isFromCalendar=YES;
        
        UINavigationController *controller=(UINavigationController *) app.splitViewController.modalViewController;
        if(controller)
        [controller pushViewController:visitReviewSessionVC animated:YES];
        [visitReviewSessionVC release];
        
        [_newVisit release];
        
        // [_navController release];
        
        dispatch_async([app eventQueue], ^{
            
            NSString *eventTitle = [NSString stringWithFormat:@"Visit: %@",aScheduleVisit.customer?aScheduleVisit.customer.name:@"visit"];
            
            EKEvent *_event = [CustomEventStore editEventForIdentifier:aScheduleVisit.eventIdentifier withTitle:eventTitle startDate:aScheduleVisit.visitStartDate endDate:aScheduleVisit.visitEndDate location:nil notes:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                aScheduleVisit.eventIdentifier = _event.eventIdentifier;
                
                /// NSLog(@"Event identifier=%@",_event.eventIdentifier);
                [[SSCoreDataManager sharedManager] save:nil];
                
                [self setAssociateObject:nil forKey:currentScheduleVisitKey];
            });
        });

    }
    
    if([self.delegate respondsToSelector:@selector(dismissPopOverFromViewVisit:)])
        [self.delegate dismissPopOverFromViewVisit:YES];
}

#pragma mark add visit session
-(void)addVisitSessionAction:(id)sender
{
    //if(!self.visit.customer && !self.visitingCustomer) return;
    
    [self startVisitSessionForScheduleVisit:self.visit];
}

- (void)selectedStartDate:(NSString *)strStart endDate:(NSString *)strEnd
{
    if(strStart!=nil)
    {
        self._startDate=strStart;
        NSLog(@"self._startDate=%@",self._startDate);
    }
    
    
    if(strEnd!=nil)
    {
        self._endDate=strEnd;
        NSLog(@"self._endDate=%@",self._endDate);
    }
    
    
    if(self.selectedIndexPath)
    {
        NSMutableArray *reloadIndexPaths=[NSMutableArray arrayWithObject:self.selectedIndexPath];
        
        self.selectedIndexPath=nil;
        
        if([reloadIndexPaths count])
        [self._tableViewGrouped reloadRowsAtIndexPaths:reloadIndexPaths withRowAnimation:UITableViewRowAnimationFade];
    }
    
    //FOR DIRECTLY FROM CUSTOMER SCHEDULE VISIT SECTION
    if(self.dealerName)
    {
        Start_EndDate *alert=nil;
        NSArray *vcs=self.navigationController.viewControllers;
        if([vcs count]>=2) alert = [vcs objectAtIndex:1];
        
        if(alert && alert.isFromCustomer) [self saveChanges:nil];
    }
}
                      
-(void)addSaveScheduledVisitCallback:(SaveScheduledVisitCallback)callback
{
    self.saveScheduledVisitCallback = callback;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
