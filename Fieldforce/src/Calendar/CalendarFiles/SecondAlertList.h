//
//  SecondAlertList.h
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SecondAlertListDelegate <NSObject>
@required
- (void)selectedSecondAlertTime:(NSString *)strAlert;
@end
@interface SecondAlertList : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    id <SecondAlertListDelegate> _delegate;
    UITableViewCell *TableCell;
}
@property(nonatomic,retain)NSString *strSecondAlert;
@property(nonatomic, assign) id <SecondAlertListDelegate> delegate;
@end
