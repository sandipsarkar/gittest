//
//  CXCalendarView.h
//  Calendar
//
//  Created by Vladimir Grichina on 13.07.11.
//  Copyright 2011 Componentix. All rights reserved.
//

#import "CXCalendarCellView.h"
#import "SliderView.h"
#import "CustomEventStore.h"
#import <EventKit/EventKit.h>

@class CXCalendarView;


@protocol CXCalendarViewDelegate <NSObject>

@optional

- (void) calendarView: (CXCalendarView *) calendarView
        didSelectDate: (NSDate *) selectedDate;

@end

@protocol CalendarDelegate <NSObject>
@optional
- (NSArray *)taskListForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate;
@end


@interface CXCalendarView : UIView<SliderViewDelegate,UISearchBarDelegate> {
@protected
    NSCalendar *_calendar;

    NSDate *_selectedDate;

    NSDate *_displayedDate;

    UIView *_monthBar;
    UILabel *_monthLabel;
    UIButton *_monthBackButton;
    UIButton *_monthForwardButton;
    UIView *_weekdayBar;
    NSArray *_weekdayNameLabels;
    UIView *_gridView;
    NSArray *_dayCells;

    CGFloat _monthBarHeight;
    CGFloat _weekBarHeight;
    
    UIButton* lastButtonSelected;
    NSMutableArray *arrBtnHold;
    id <CalendarDelegate> _calDeligate;
   
}

@property(nonatomic, retain) NSCalendar *calendar;

@property(nonatomic, assign) id<CXCalendarViewDelegate> delegate;
@property(nonatomic, assign) id <CalendarDelegate> calDeligate;

@property(nonatomic, retain) NSDate *selectedDate;

@property(nonatomic, retain) NSDate *displayedDate;

@property(nonatomic, retain) NSDate *baseDate;

@property(nonatomic, readonly) NSUInteger displayedYear;
@property(nonatomic, readonly) NSUInteger displayedMonth;
@property(nonatomic,retain)SliderView *sliderView;
@property(nonatomic,retain) NSMutableArray *arrPrevMonth,*arrNextMonth;
@property(nonatomic, retain) NSArray *arrTask;
@property(nonatomic, retain) NSArray *arrayReturnedAfterPredicate;


- (void) monthForward;
- (void) monthBack;

- (void) reset;

// UI
@property(readonly) UIView *monthBar;
@property(readonly) UILabel *monthLabel;
@property(readonly) UIButton *monthBackButton;
@property(readonly) UIButton *monthForwardButton;
@property(readonly) UIView *weekdayBar;
@property(readonly) NSArray *weekdayNameLabels;
@property(readonly) UIView *gridView;
@property(readonly) NSArray *dayCells;

@property(assign) CGFloat monthBarHeight;
@property(assign) CGFloat weekBarHeight;

- (CXCalendarCellView *) cellForDate: (NSDate *) date;
- (void) touchedCellView: (CXCalendarCellView *) cellView;
- (NSMutableArray *)calculateDaysInFinalWeekOfPreviousMonth;
- (NSUInteger)numberOfDaysInPreviousPartialWeek;
- (NSMutableArray *)calculateDaysInFirstWeekOfFollowingMonth;
- (NSUInteger)numberOfDaysInFollowingPartialWeek;
- (NSArray *)searchArrayUsingPredicateForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate;
- (NSArray *)searchTaskArrayUsingPredicateForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate;

@end