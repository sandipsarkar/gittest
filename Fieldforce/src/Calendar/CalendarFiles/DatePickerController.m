//
//  DatePickerController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 20/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "DatePickerController.h"
@interface DatePickerController ()


@property(nonatomic,copy)DateDidChangeCallback dateDidChangeCallback;
-(void)dateDidChange:(UIDatePicker *)datePicker;
@end

@implementation DatePickerController
@synthesize date,dateDidChangeCallback;

- (id)initWithDate:(NSDate *)_date pickerMode:(UIDatePickerMode)pickerMode
{
    self = [super init];
    if (self) 
    {
        self.date=_date;
        _pickerMode=pickerMode;
    }
    return self;
}

-(void)addDateChangeCallback:(DateDidChangeCallback)callback
{
    if(callback)
     self.dateDidChangeCallback = callback;
}

-(void)setupDatePicker
{
    NSDate *_date  = self.date;

    datePicker = [[UIDatePicker alloc] init];
    
    CGSize size = [datePicker sizeThatFits:self.view.bounds.size];
    
    datePicker.frame = CGRectMake(0, 0, size.width, size.height);
    
    datePicker.datePickerMode = _pickerMode;
    datePicker.hidden = NO;
    datePicker.date = _date;
    datePicker.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [datePicker addTarget:self
                action:@selector(dateDidChange:)
         forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:datePicker];
}

-(void)dateDidChange:(UIDatePicker *)_datePicker
{
    self.date = _datePicker.date;
    
    if(self.dateDidChangeCallback)
        self.dateDidChangeCallback(self.date);
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
	
    [self setupDatePicker];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.contentSizeForViewInPopover = CGSizeMake(320, 210.0);
    //self.popoverController.popoverContentSize=CGSizeMake(320, 210.0);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

-(void)dealloc
{
    [date release];
    [datePicker release];
    
    if(dateDidChangeCallback)
        [dateDidChangeCallback release];
    
    [super dealloc];
}

@end
