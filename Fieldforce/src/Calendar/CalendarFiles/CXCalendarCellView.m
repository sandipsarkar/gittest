//
//  CXCalendarCellView.m
//  Calendar
//
//  Created by Vladimir Grichina on 13.07.11.
//  Copyright 2011 Componentix. All rights reserved.
//

#import "CXCalendarCellView.h"
#import "VisitListViewController.h"
#import "CXCalendarView.h"
#import "CreateTaskViewController.h"
#import "TaskDetailViewController.h"
#import "ScheduledVisit.h"
#import "Task.h"
#import "Customer.h"
#import "Reminder.h"

@interface CXCalendarCellView()
@property(nonatomic,assign) CXCalendarView *objCal;

- (void)selectedVisit:(ScheduledVisit *)event;
-(void)selectedTask:(Task *)_task;
- (void)selectedReminder:(Reminder *)event;

@end

@implementation CXCalendarCellView
@synthesize selected;
@synthesize lblDate;
@synthesize btnTask,btnReminder,btnVisit;
@synthesize reminderArrayList,visitArrayList,taskArrayList;
@synthesize objCal;
@synthesize month;


- (id) init
{
    if ((self = [super init])) 
    {
        selectedButton=nil;
        self.lblDate=[[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
        self.lblDate.backgroundColor=[UIColor clearColor];
        //self.lblDate.textColor=[UIColor colorWithRed:202.0/255.0 green:202.0/255.0 blue:202.0/255.0 alpha:1];
        self.lblDate.textColor=[UIColor colorWithRed:180.0f/255.0f green:180.0f/255.0f blue:180.0f/255.0f alpha:1.0f];
        self.lblDate.font=[UIFont grotesqueFontOfSize:12.0];
        self.lblDate.textAlignment=UITextAlignmentRight;
        self.lblDate.userInteractionEnabled=YES;
        [self addSubview:self.lblDate];
        
        
        self.btnTask=[UIButton buttonWithType:UIButtonTypeCustom];
        //btnTask.frame =CGRectMake(_x, 0, 126, _weekBarHeight);;
        [self.btnTask setBackgroundImage:[UIImage imageNamed:@"red_button_calender.png"] forState:UIControlStateNormal];
        [self.btnTask setBackgroundImage:[UIImage imageNamed:@"red_button_selected.png"] forState:UIControlStateHighlighted];
        [self.btnTask setBackgroundImage:[UIImage imageNamed:@"red_button_selected.png"] forState:UIControlStateSelected];
        //[self.btnTask setTitle:[NSString stringWithFormat:@"    %@",@"Task"] forState:UIControlStateNormal];
        self.btnTask.titleLabel.font=[UIFont systemFontOfSize:12];
        self.btnTask.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.btnTask setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.btnTask.titleLabel.textColor = [UIColor blackColor];
        self.btnTask.backgroundColor=[UIColor clearColor];
        self.btnTask.tag=10;
        [self.btnTask addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btnTask];
        
        
        
        self.btnReminder=[UIButton buttonWithType:UIButtonTypeCustom];
        //btnTask.frame =CGRectMake(_x, 0, 126, _weekBarHeight);;
        [self.btnReminder setBackgroundImage:[UIImage imageNamed:@"yellow_button.png"] forState:UIControlStateNormal];
        [self.btnReminder setBackgroundImage:[UIImage imageNamed:@"yellow_button_selected.png"] forState:UIControlStateHighlighted];
         [self.btnReminder setBackgroundImage:[UIImage imageNamed:@"yellow_button_selected.png"] forState:UIControlStateSelected];
        self.btnReminder.titleLabel.font=[UIFont systemFontOfSize:12];
        self.btnReminder.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.btnReminder setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.btnReminder.backgroundColor=[UIColor clearColor];
        self.btnReminder.tag=11;
        [self.btnReminder addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btnReminder];
        
        self.btnVisit=[UIButton buttonWithType:UIButtonTypeCustom];
        //btnTask.frame =CGRectMake(_x, 0, 126, _weekBarHeight);;
        [self.btnVisit setBackgroundImage:[UIImage imageNamed:@"green_button.png"] forState:UIControlStateNormal];
        [self.btnVisit setBackgroundImage:[UIImage imageNamed:@"green_button_selected.png"] forState:UIControlStateHighlighted];
        [self.btnVisit setBackgroundImage:[UIImage imageNamed:@"green_button_selected.png"] forState:UIControlStateSelected];
        self.btnVisit.titleLabel.font=[UIFont systemFontOfSize:12];
        self.btnVisit.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.btnVisit setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.btnVisit.backgroundColor=[UIColor clearColor];
        self.btnVisit.tag=12;
        [self.btnVisit addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.btnVisit];
        
        
        UIEdgeInsets _inset = UIEdgeInsetsMake(0, 15.0, 0, 0.0);
        self.btnTask.titleEdgeInsets = _inset;
        self.btnVisit.titleEdgeInsets = _inset;
        self.btnReminder.titleEdgeInsets = _inset;
        
    }
    return self;
}


-(void)setVisitArrayList:(NSMutableArray *)_visitArrayList
{
    if(_visitArrayList!=visitArrayList)
    {
        [visitArrayList release];
        visitArrayList = [_visitArrayList retain];
        self.btnVisit.hidden = (![_visitArrayList count]);
        [self setNeedsLayout];
    }
}


-(void)setTaskArrayList:(NSMutableArray *)_taskArrayList
{
    if(taskArrayList!=_taskArrayList)
    {
        [taskArrayList release];
        taskArrayList = [_taskArrayList retain];
        self.btnTask.hidden = (![_taskArrayList count]);
        [self setNeedsLayout];
    }
}


-(void)setReminderArrayList:(NSMutableArray *)_reminderArrayList
{
    if(reminderArrayList!=_reminderArrayList)
    {
        [reminderArrayList release];
        reminderArrayList = [_reminderArrayList retain];
        self.btnReminder.hidden = (![_reminderArrayList count]);
        [self setNeedsLayout];
    }
}

-(void)selectVisit:(EKEvent *)visit
{
    if(!visit || !self.visitArrayList) return;
    
    
    NSInteger index = [self.visitArrayList indexOfObject:visit];
    if(index!=NSNotFound)
    //[self selectedVisitForEdit:index];
    
    [self performSelector:@selector(btnClicked:) withObject:self.btnVisit];

}


-(void)selectEvent:(id)event
{
    if(!event) return;
    
    NSArray *arrToSelect = nil;
    UIButton *buttonToSelect = nil;
    
    // NSLog(@"Event:%@",event);
    
    int type=0;
    if([event isKindOfClass:[ScheduledVisit class]])
    {
        arrToSelect = self.visitArrayList;
        buttonToSelect = self.btnVisit;
        type=1;
    }
    else if([event isKindOfClass:[Task class]])
    {
        arrToSelect = self.taskArrayList;
        buttonToSelect = self.btnTask;
        type=2;
    }
    else  if([event isKindOfClass:[Reminder class]])
    {
        arrToSelect = self.reminderArrayList;
        buttonToSelect = self.btnReminder;
        type=3;
    }
    
    if(!arrToSelect)
    {
        buttonToSelect.hidden = NO;
        buttonToSelect.selected = YES;
        selectedButton=buttonToSelect;
        
        switch (type)
        {
            case 1:
            {
                [self selectedVisit:event];
            }
            break;
                
            case 2:
            {
                [self selectedTask:event];
            }
            break;
                
            case 3:
            {
                [self selectedReminder:event];
                
            }
            break;
                
            default:break;
        }
        
        return;
    }
    
    NSInteger index = [arrToSelect indexOfObject:event];
    if(index!=NSNotFound)
    {
        // [self performSelector:@selector(btnClicked:) withObject:buttonToSelect];
        
        buttonToSelect.selected = YES;
        selectedButton=buttonToSelect;
        
        switch (type)
        {
            case 1:
                [self selectedVisitForEdit:index];  
            break;
            case 2:
                [self selectedTaskForEdit:index];
            break;
            case 3:
                [self selectedReminderForEdit:index];
            break;
                
            default:break;
        }  
    }
}
-(void)sortVisitList
{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"visitStartDate" ascending:YES];
    
    [self.visitArrayList sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];

    [sortDescriptor release];
    //[self.tableView reloadData];
}
-(void)sortReminderList
{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"startDate" ascending:YES];
    
    [self.reminderArrayList sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [sortDescriptor release];
}
-(void)btnClicked:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    selectedButton=btn;
    btn.selected = YES;
    
    if(!self.objCal)
    self.objCal=(CXCalendarView *)self.nextResponder.nextResponder;
    
    UIPopoverController *popOverReminder = self.objCal.calendarCellPopoverController;
    
    switch (btn.tag) 
    {
        case 10://task
        {
           // [btn setBackgroundImage:[UIImage imageNamed:@"red_button_selected.png"] forState:UIControlStateNormal];
            
            if(self.taskArrayList.count==1)
            {
                [self selectedTaskForEdit:0];
            }
            else
            {

            TasksInCalenderController *_task=[[TasksInCalenderController alloc] init];
            _task.calenderView=self.objCal;
            _task.taskList=self.taskArrayList;
            _task.delegate=self;
            
            UINavigationController *navController=[[UINavigationController alloc ] initWithRootViewController:_task];
            navController.navigationBarHidden=YES;
            
            
            if(popOverReminder==nil)
            {
                popOverReminder=[[UIPopoverController alloc] initWithContentViewController:navController];
                
                self.objCal.calendarCellPopoverController = popOverReminder;
            }
            else
            {
                popOverReminder.contentViewController=navController;
            }
            _task.popoverController=popOverReminder;
            popOverReminder.delegate=self;
            popOverReminder.popoverContentSize=CGSizeMake(335.0+15.0,94.0);
                
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            popOverReminder.popoverBackgroundViewClass = [MBPopoverBackgroundView class];
            
                
            [popOverReminder presentPopoverFromRect:[sender frame] inView:[sender superview] permittedArrowDirections:UIPopoverArrowDirectionRight | UIPopoverArrowDirectionLeft animated:YES];
            [_task release];
            [navController release];
            }
        }
            break;
            
        case 11://reminder
        {
            
            NSLog(@"self.nextResponder.nextResponder=%@",self.nextResponder.nextResponder);
           // [btn setBackgroundImage:[UIImage imageNamed:@"yellow_button_selected.png"] forState:UIControlStateNormal];
            
            if(self.reminderArrayList.count==1)
            {
                [self selectedReminderForEdit:0];
            }
            else
            {
                [self sortReminderList];
                
                ReminderListViewController *_remind = [[ReminderListViewController alloc] init];
                _remind.delegate     = self;
                _remind.reminderList = self.reminderArrayList;
                
                
                if(popOverReminder==nil)
                {
                    popOverReminder=[[UIPopoverController alloc] initWithContentViewController:_remind];
                    
                    self.objCal.calendarCellPopoverController = popOverReminder;
                }
                else
                {
                    popOverReminder.contentViewController=_remind;
                }
                
                popOverReminder.delegate=self;
                popOverReminder.popoverContentSize=CGSizeMake(335.0+15.0,94.0);
                
              if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                popOverReminder.popoverBackgroundViewClass = [MBPopoverBackgroundView class];
             
                
                [popOverReminder presentPopoverFromRect:[sender frame] inView:[sender superview] permittedArrowDirections:UIPopoverArrowDirectionRight | UIPopoverArrowDirectionLeft animated:YES];
                [_remind release];
            }
        }
            break;
            
        case 12://visit
        {
          
             // [btn setBackgroundImage:[UIImage imageNamed:@"green_button_selected.png"] forState:UIControlStateNormal];
            
            if(self.visitArrayList.count==1)
            {
                [self selectedVisitForEdit:0];
            }
            else
            {
                [self sortVisitList];
                
                VisitListViewController *_visit=[[VisitListViewController alloc] initWithVisitList:self.visitArrayList];
                
                _visit.delegate=self;

                 
                
                if(popOverReminder==nil)
                {
                    popOverReminder=[[UIPopoverController alloc] initWithContentViewController:_visit];
                    
                   self.objCal.calendarCellPopoverController = popOverReminder;
                }
                else
                {
                    popOverReminder.contentViewController=_visit;
                }
                popOverReminder.delegate=self;
                popOverReminder.popoverContentSize=CGSizeMake(335+15.0,94);
                
                if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                popOverReminder.popoverBackgroundViewClass = [MBPopoverBackgroundView class];
               
                CGRect targetFrame = [sender frame];
                [popOverReminder presentPopoverFromRect:targetFrame inView:[sender superview] permittedArrowDirections:UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionRight animated:YES];
                [_visit release];
                
                /*
                UIView * border = [[popOverReminder.contentViewController.view.superview.superview.superview subviews] objectAtIndex:0];  
                border.hidden = YES;
                
                NSArray* subviews = ((UIView*)[popOverReminder.contentViewController.view.superview.superview.superview.subviews objectAtIndex:0]).subviews;
                for(UIView *subview in subviews)
                {
                    subview.hidden=YES;
    
                }
                */
            }
            
        }
        break;
            
        default: break;
    }
        
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    if(selectedButton!=nil)
    {
        selectedButton.selected = NO;
        selectedButton=nil;
        
        
     //   [popOverReminder release];
      //  popOverReminder = nil;
        
    }
}

- (NSUInteger) day
{
    return _day;
}

- (void) setDay: (NSUInteger) day 
{
    if (_day != day) 
    {
        _day = day;
        //self.backgroundColor=[UIColor brownColor];
        //self.lblDate.frame=self.frame;
        //[self.lblDate setText:[NSString stringWithFormat: @"%d", day]];

    }
}


- (NSDate *) dateWithBaseDate: (NSDate *) baseDate withCalendar: (NSCalendar *)calendar {
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit|NSWeekCalendarUnit
                                               fromDate:baseDate];
    components.day = self.day;
    return [calendar dateFromComponents:components];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(!self.objCal)
    self.objCal=(CXCalendarView *)self.nextResponder.nextResponder;
    [self.objCal touchedCellView:self];
}


- (void) layoutSubviews 
{
    [super layoutSubviews];
   
    float _x=0;
    float _y=4.0;
    float _Width=self.bounds.size.width-4.0;
    float _height=17;
    float _spaceHeight=5;
    
    self.lblDate.frame=CGRectMake(_x, _y, _Width, _height);
    _y=CGRectGetMaxY(self.lblDate.frame)+_spaceHeight;
    
    CGRect btnTaskFrame = CGRectMake(_x, _y, _Width, _height);
    
    if(!self.btnTask.hidden)
    {
        if(!CGRectEqualToRect(btnTaskFrame, self.btnTask.frame))
        self.btnTask.frame=btnTaskFrame;
    }
    
    
    _y=(self.btnTask.hidden)?_y:CGRectGetMaxY(self.btnTask.frame)+_spaceHeight;
    
    CGRect reminderRect = CGRectMake(_x, _y, _Width, _height);
    if(!CGRectEqualToRect(reminderRect, self.btnReminder.frame))
    self.btnReminder.frame=reminderRect;
    
    _y=(self.btnReminder.hidden)?_y:CGRectGetMaxY(self.btnReminder.frame)+_spaceHeight;
    
    CGRect visitRect = CGRectMake(_x, _y, _Width, _height);
    
    if(!CGRectEqualToRect(visitRect, self.btnVisit.frame))
    self.btnVisit.frame=visitRect;
}

- (void)selectedReminderForEdit:(int)index
{
    //if(popOverReminder.popoverVisible)
	//	[popOverReminder dismissPopoverAnimated:NO];
    
    Reminder *event = (index>=0 && index <[reminderArrayList count]) ? [reminderArrayList objectAtIndex:index] : nil;
    
    [self selectedReminder:event];
}

- (void)selectedReminder:(Reminder *)event
{
    //if(popOverReminder.popoverVisible)
	//	[popOverReminder dismissPopoverAnimated:NO];
    
    if(!event) return;
    
    ADDReminder *mindit=[[ADDReminder alloc] init];
    mindit.delegate=self;
    mindit.reminder=event;
    
    mindit.reminderTittle=event.title;
    mindit.reminderId=event.eventIdentifier;
    

    mindit._startDate=[event.startDate stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
    mindit._endDate=[event.endDate stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
    
    NSArray *_alarms = event.alarms.length ? [event.alarms JSONValue] : nil;
    NSArray *alarms =[ADDReminder alarmFromArr:_alarms];
    
    if([alarms count]==1)
    {
        EKAlarm *alarm=[alarms objectAtIndex:0];
        
        NSTimeInterval interval=alarm.relativeOffset;
        int  min=interval/60.0;
        switch (min)
        {
            case -0:
                mindit._alert=@"At time of event";
                break;
                
            case -5:
                mindit._alert=@"5 minutes before";
                break;
                
            case -15:
                mindit._alert=@"15 minutes before";
                break;
                
            case -30:
                mindit._alert=@"30 minutes before";
                break;
                
            case -60:
                mindit._alert=@"1 hour before";
                break;
                
            case -120:
                mindit._alert=@"2 hour before";
                break;
                
            case -1440:
                mindit._alert=@"1 day before";
                break;
                
            case -2880:
                mindit._alert=@"2 day before";
                break;
                
            default:
                break;
        }
    }
    else if([alarms count]==2)
    {
        EKAlarm *alarm1=[alarms objectAtIndex:0];
        NSTimeInterval interval1=alarm1.relativeOffset;
        int  min=interval1/60.0;
        switch (min)
        {
            case 0:
                mindit._alert=@"At time of event";
                break;
                
            case -5:
                mindit._alert=@"5 minutes before";
                break;
                
            case -15:
                mindit._alert=@"15 minutees before";
                break;
                
            case -30:
                mindit._alert=@"30 minutes before";
                break;
                
            case -60:
                mindit._alert=@"1 hour before";
                break;
                
            case -120:
                mindit._alert=@"2 hour before";
                break;
                
            case -1440:
                mindit._alert=@"1 day before";
                break;
                
            case -2880:
                mindit._alert=@"2 day before";
                break;
                
            default:
                break;
        }
        
        EKAlarm *alarm2=[alarms objectAtIndex:1];
        NSTimeInterval interval2=alarm2.relativeOffset;
        min=interval2/60.0;
        switch (min)
        {
            case 0:
                mindit._secondAlert=@"At time of event";
                break;
                
            case -5:
                mindit._secondAlert=@"5 minutes before";
                break;
                
            case -15:
                mindit._secondAlert=@"15 minutes before";
                break;
                
            case -30:
                mindit._secondAlert=@"30 minutes before";
                break;
                
            case -60:
                mindit._secondAlert=@"1 hour before";
                break;
                
            case -120:
                mindit._secondAlert=@"2 hour before";
                break;
                
            case -1440:
                mindit._secondAlert=@"1 day before";
                break;
                
            case -2880:
                mindit._secondAlert=@"2 day before";
                break;
                
            default:
                break;
        }
        
    }
    
    NSString *_recurrenceRule = event.recurrenceRule;
    NSDictionary *dict = [_recurrenceRule JSONValue];
    EKRecurrenceRule * recurrenceRule=[[ADDReminder recurrenceRuleFromDict:dict] retain];
    
    
    if(recurrenceRule!=nil)
    {
        switch (recurrenceRule.frequency)
        {
            case EKRecurrenceFrequencyDaily:
            {
                mindit._repeat=@"Daily";
            }
                break;
                
            case EKRecurrenceFrequencyWeekly:
            {
                if(recurrenceRule.interval==1)
                    mindit._repeat=@"Weekly";
                else
                    mindit._repeat=@"Biweekly";
            }
                break;
                
            case EKRecurrenceFrequencyMonthly:
                mindit._repeat=@"Monthly";
                break;
                
            case EKRecurrenceFrequencyYearly:
                mindit._repeat=@"Yearly";
                break;
                
            default: break;
        }
        
        EKRecurrenceEnd * end=recurrenceRule.recurrenceEnd;
        NSLog(@"End date=%@",end.endDate);
        mindit._endRepeat= end.endDate ? [end.endDate stringFromDateWithFormat:@"EEE dd MMM, yyyy"] : nil;
       
        
        /*
         int occurrence=end.occurrenceCount;
         
         if(occurrence==1)
         {
         [formatter setDateFormat:@"EEE dd MMM, yyyy"];
         mindit._endRepeat=[formatter stringFromDate:event.endDate];
         
         }
         else
         {
         
         int interval=occurrence*24*60*60;
         NSDate *_d=[event.startDate dateByAddingTimeInterval:interval];
         
         //NSString *str=event.notes;
         
         
         [formatter setDateFormat:@"EEE dd MMM, yyyy"];
         mindit._endRepeat=[formatter stringFromDate:_d];
         //[formatter release];
         
         
         }
         */
        
        // [formatter setDateFormat:@"EEE dd MMM, yyyy"];
        // mindit._endRepeat=[formatter stringFromDate:_d];
        // [formatter release];
    }
    
    [recurrenceRule release];
 
    NSLog(@"mindit._startDate=%@",mindit._startDate);
    NSLog(@"mindit._endDate=%@",mindit._endDate);
    NSLog(@"mindit._alert=%@",mindit._alert);
    // NSLog(@"mindit._secondAlert=%@",mindit._secondAlert);
    NSLog(@"mindit._repeat=%@",mindit._repeat);
    NSLog(@"mindit._endRepeat=%@",mindit._endRepeat);
    NSLog(@"mindit.reminderId=%@",mindit.reminderId);
    
    NSDate *startDate = event.startDate;
    
    if(!objCal) self.objCal = (CXCalendarView *)self.nextResponder.nextResponder;
    __block CXCalendarCellView *weakSelf = self;
    
    [mindit addSaveReminderCallback:^(Reminder *reminder, int actionType, NSDate *lastStartDate)
     {
         if(actionType==2)
         {
             [weakSelf.objCal editReminder:reminder lastStartDate:startDate];
         }
         else if(actionType==3)
         {
             [weakSelf.objCal removeReminder:reminder];
         }
         //[self.objCal checkAndUpdateToday];
     }];
    
    UINavigationController *_navController = [[UINavigationController alloc] initWithRootViewController:mindit];
    // mindit.title = @"EDIT REMINDER";
    
    UIPopoverController *popOverReminder=self.objCal.calendarCellPopoverController;
    
    if(popOverReminder == nil)
    {
        popOverReminder = [[UIPopoverController alloc] initWithContentViewController:_navController];
        self.objCal.calendarCellPopoverController = popOverReminder;
    }
    else
    {
        popOverReminder.contentViewController=_navController;
    }

	popOverReminder.popoverContentSize=CGSizeMake(335+15.0,385);
    popOverReminder.delegate=self;
    
     if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
     popOverReminder.popoverBackgroundViewClass = [MBPopoverBackgroundView class];
    

    if(!popOverReminder.popoverVisible)
    {
        [popOverReminder presentPopoverFromRect:[self.btnReminder frame] inView:[self.btnReminder superview] permittedArrowDirections:UIPopoverArrowDirectionRight | UIPopoverArrowDirectionLeft animated:YES];
    }
    
	[mindit release];
    [_navController release];
    
}

- (void)selectedVisitForEdit:(int)index
{
   // if(popOverReminder.popoverVisible)
		//[popOverReminder dismissPopoverAnimated:NO];
    
    ScheduledVisit *event  = (index>=0 && index<[visitArrayList count]) ? [visitArrayList objectAtIndex:index] : nil;
    
    [self selectedVisit:event];
    

}

- (void)selectedVisit:(ScheduledVisit *)event
{
    if(!event) return;
    
    // if(popOverReminder.popoverVisible)
    //[popOverReminder dismissPopoverAnimated:NO];
    
    ADDVisit *visit = [[ADDVisit alloc] init];
    visit.delegate=self;
    
    //visit.dealerName=event.title;
    visit.dealerName=event.customer?event.customer.name:event.customerName;
    visit.visitingCustomer=event.customer;
    //visit.visitId=event.eventIdentifier;
    visit.visitId=event.scheduleVisitID;
    

    
    visit._startDate=[event.visitStartDate stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];
    visit._endDate=[event.visitEndDate stringFromDateWithFormat:@"EEE dd MMM, yyyy, h:mm a"];

    
    NSLog(@"visit._startDate=%@",visit._startDate);
    NSLog(@"visit._endDate=%@",visit._endDate);
    NSLog(@"visit.visitId=%@",visit.visitId);
    NSLog(@"visit.dealerName=%@",visit.dealerName);
    
    if(!objCal)
        self.objCal=(CXCalendarView *)self.nextResponder.nextResponder;
    
    __block CXCalendarCellView *weakSelf = self;
    
    
    [visit addSaveScheduledVisitCallback:^(ScheduledVisit *visit, int actionType, NSDate *lastStartDate)
     {
         if(actionType==2)
         {
             [weakSelf.objCal editVisit:visit lastStartDate:lastStartDate?lastStartDate:visit.visitStartDate];
         }
         else if(actionType==3)
         {
             [weakSelf.objCal removeScheduledVisit:visit];
         }
     }];


    UINavigationController *_navController= [[UINavigationController alloc] initWithRootViewController:visit];
    //visit.title = @"EDIT VISIT";
    
    UIPopoverController *popOverReminder=self.objCal.calendarCellPopoverController;
    
    if(popOverReminder==nil)
    {
        popOverReminder=[[UIPopoverController alloc] initWithContentViewController:_navController];
        
         self.objCal.calendarCellPopoverController = popOverReminder;
    }
    
    else
        popOverReminder.contentViewController=_navController;
    
    
	
    [popOverReminder setPopoverContentSize:CGSizeMake(335+15.0,485) animated:YES];
    popOverReminder.delegate=self;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    popOverReminder.popoverBackgroundViewClass = [MBPopoverBackgroundView class];
    
    
    // [self.btnReminder setBackgroundImage:[UIImage imageNamed:@"red_button_selected.png"] forState:UIControlStateNormal];
    
    if(!popOverReminder.popoverVisible)
    {
        CGRect targetRect=[self.btnVisit frame] ;
        [popOverReminder presentPopoverFromRect:targetRect inView:self permittedArrowDirections:UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionRight animated:YES];
    }
    visit.popoverController=popOverReminder;
	[visit release];
    [_navController release];
}

-(void)selectedTaskForEdit:(int)index
{
    //if(popOverReminder.popoverVisible)
		//[popOverReminder dismissPopoverAnimated:NO];
    
    Task *_task  = (index>=0 && index<[self.taskArrayList count]) ? [self.taskArrayList objectAtIndex:index] : nil;
    
    [self selectedTask:_task];
}

-(void)selectedTask:(Task *)_task
{
    if(!_task) return;
    
    //if(popOverReminder.popoverVisible)
    //[popOverReminder dismissPopoverAnimated:NO];
    
    TaskDetailViewController *taskViewController = [[TaskDetailViewController alloc] initWithTask:_task];
    // taskViewController.title = @"EDIT TASK";
    taskViewController.shouldShowCustomerProfile = YES;//NO
    taskViewController.isEditMode = YES;
    taskViewController.isInPopover = YES;
    
    if(!self.objCal)
        self.objCal=(CXCalendarView *)self.nextResponder.nextResponder;
    
__block UIPopoverController  *popOverReminder = self.objCal.calendarCellPopoverController;
    
    __block CXCalendarCellView *weakSelf = self;
    
    [taskViewController addDeleteTaskCallback:^(Task *_task)
     {
         if(weakSelf.btnTask)
         {
             weakSelf.btnTask.selected = NO;
             weakSelf->selectedButton = nil;
         }
         
         [SSCoreDataManager deleteObject:_task];
         
         NSError *error=nil;
         [[SSCoreDataManager sharedManager] save:error];
         if(!error)
         {
             //if([self.taskList containsObject:_task])
             // [self.taskList removeObject:_task];

            [weakSelf removeTask:_task];

             [popOverReminder dismissPopoverAnimated:YES];
             
             /// ========== May be not needed here ==========
             NSError *error=nil;
             [[SSCoreDataManager sharedManager] save:error];
         }
         
         //[self.objCal checkAndUpdateToday];
     }];
    
    NSDate *_lastDueDate = [_task.dueDate copy];
    [taskViewController addEditTaskCallback:^(Task *task)
     {
         if(weakSelf.btnTask)
         {
             weakSelf.btnTask.selected = NO;
             weakSelf->selectedButton = nil;
         }
         
         [weakSelf.objCal editTask:task lastStartDate:_lastDueDate];
         [_lastDueDate release];

         
         [popOverReminder dismissPopoverAnimated:YES];
         
         //[self.objCal checkAndUpdateToday];
     }];
    
    [taskViewController addTaskCancelCallback:^{
        
        if(weakSelf.btnTask)
        {
            weakSelf.btnTask.selected = NO;
            weakSelf->selectedButton = nil;
        }
    }];
    
    UINavigationController *_navController= [[UINavigationController alloc] initWithRootViewController:taskViewController];
    
 
    
    if(popOverReminder==nil)
    {
        popOverReminder=[[UIPopoverController alloc] initWithContentViewController:_navController];
        
        self.objCal.calendarCellPopoverController = popOverReminder;
    }
    else
        popOverReminder.contentViewController=_navController;
    
	popOverReminder.popoverContentSize=CGSizeMake(350,380);
    popOverReminder.delegate=self;
    taskViewController.popoverController=popOverReminder;
    // [self.btnReminder setBackgroundImage:[UIImage imageNamed:@"red_button_selected.png"] forState:UIControlStateNormal];
    
   if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
   popOverReminder.popoverBackgroundViewClass = [MBPopoverBackgroundView class];
 
    
    if(!popOverReminder.popoverVisible)
    {
        CGRect targetRect=[self.btnTask frame] ;
        [popOverReminder presentPopoverFromRect:targetRect inView:[self.btnTask superview] permittedArrowDirections:UIPopoverArrowDirectionRight | UIPopoverArrowDirectionLeft animated:YES];
    }
    
    taskViewController.tableView.backgroundView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:1.0];
    
	[taskViewController release];
    [_navController release];
}

-(void)dismissPopOverFromViewReminder:(BOOL)done
{
    if(selectedButton!=nil)
    {
        selectedButton.selected = NO;
        selectedButton=nil;
        
    }
    
    UIPopoverController  *popOverReminder = self.objCal.calendarCellPopoverController;
    if(popOverReminder.popoverVisible)
    [popOverReminder dismissPopoverAnimated:YES];
    
    
    //if(done && self.objCal)
       // [self.objCal editReminder:nil];
}

- (void)dismissPopOverFromViewVisit:(BOOL)done
{
    if(selectedButton!=nil)
    {
        selectedButton.selected = NO;
        selectedButton=nil;
    }
    
    UIPopoverController  *popOverReminder = self.objCal.calendarCellPopoverController;
    if(popOverReminder.popoverVisible)
      [popOverReminder dismissPopoverAnimated:YES];
    
   // if(done && self.objCal)
     //   [self.objCal editVisit:nil];

    
}

- (void)dealloc
{
    [lblDate release];
    lblDate=nil;
    
    [reminderArrayList release];
    reminderArrayList=nil;
    
    [visitArrayList release];
    visitArrayList=nil;
    
    [taskArrayList release];
    taskArrayList=nil;
    
    if(objCal!=nil)
    {
       //[objCal release];
        objCal = nil;
    }
    
    btnTask=nil;
    btnReminder=nil;
    btnVisit=nil;
    
    //[popOverReminder release];
   // popOverReminder = nil;
    
    [super dealloc];
}

-(void)addVisit:(ScheduledVisit *)visit
{
    if(!visit) return;
    
    if(!visitArrayList)
        visitArrayList = [[NSMutableArray alloc] init];
    
    if(![visitArrayList containsObject:visit])
    {
        [visitArrayList addObject:visit];
    }
    
    NSUInteger count = [visitArrayList count];
    self.btnVisit.userInteractionEnabled = count>0;
    self.btnVisit.hidden=count<=0;
    
    if(count==1)
    {
        ScheduledVisit *firstObj = [visitArrayList objectAtIndex:0];
        
        NSString *visitTitle = firstObj.customer.name.length ? firstObj.customer.name : firstObj.customerName.length ? firstObj.customerName : @"Visit";
        
        [self.btnVisit setTitle:[NSString stringWithFormat:@"%@",visitTitle] forState:UIControlStateNormal];
    }
    else if(count>1)
    {
        NSString *cellVisitTitle = [NSString stringWithFormat:@"%d Visits",count];
        
        [self.btnVisit setTitle:cellVisitTitle forState:UIControlStateNormal];
    }
    
    if(count)
    [self setNeedsLayout];
    
}

-(void)addTask:(Task *)task
{
    if(!task) return;
    
    if(!taskArrayList)
        taskArrayList = [[NSMutableArray alloc] init];
    
    if(![taskArrayList containsObject:task])
        [taskArrayList addObject:task];
        
    
    NSUInteger count = [taskArrayList count];
    
    self.btnTask.userInteractionEnabled=count;
    self.btnTask.hidden=!count;
    
    
    if(count==1)
    {
         Task *firstObj = [taskArrayList objectAtIndex:0];
        
        NSString *title= [NSString stringWithFormat:@"%@",firstObj.subject?firstObj.subject:@"Task"];
        [self.btnTask setTitle:[NSString stringWithFormat:@"%@",title] forState:UIControlStateNormal];
    }
    else if(count>=1)
    {
        NSString *title = [NSString stringWithFormat:@"%d Tasks",count];
        [self.btnTask setTitle:title forState:UIControlStateNormal];
    }
    
    if(count)
        [self setNeedsLayout];
}

-(void)addReminder:(Reminder *)event
{
    if(!event) return;
    
    if(!reminderArrayList)
        reminderArrayList = [[NSMutableArray alloc] init];
    
    if(![reminderArrayList containsObject:event])
        [reminderArrayList addObject:event];
    
    NSUInteger count = [reminderArrayList count];
    
    self.btnReminder.userInteractionEnabled=count>0;
    self.btnReminder.hidden=!count;
    
    
    if(count==1)
    {
        Reminder *firstObj = [reminderArrayList objectAtIndex:0];
        NSString *title = ([firstObj.title rangeOfString:@":"].location !=NSNotFound)? [firstObj.title substringFromIndex:[firstObj.title rangeOfString:@":"].location+1] : firstObj.title;
        [self.btnReminder setTitle:[NSString stringWithFormat:@"%@",title] forState:UIControlStateNormal];
        
        NSLog(@"title=%@",self.btnReminder.titleLabel.text);
    }
    else if(count>1)
    {
        NSString *title = [NSString stringWithFormat:@"%d Reminders",count];
        [self.btnReminder setTitle:title forState:UIControlStateNormal];
    }
    
    if(count)
    [self setNeedsLayout];
}

-(void)reset
{
    
    if([reminderArrayList count])
        self.reminderArrayList=nil;
    
    if([taskArrayList count])
        self.taskArrayList=nil;
    
    if([visitArrayList count])
        self.visitArrayList=nil;
}

-(void)removeTask:(Task *)task
{
    if(!task) return;
    
    BOOL contains = ([taskArrayList containsObject:task]);
    
    if(!contains) return;
    
    
    [taskArrayList removeObject:task];
    
    
    NSUInteger count = [taskArrayList count];
    
    self.btnTask.userInteractionEnabled=count;
    self.btnTask.hidden=!count;
    
    
    if(count==1)
    {
        Task *firstObj = [taskArrayList objectAtIndex:0];
        NSString *title= [NSString stringWithFormat:@"%@",firstObj.subject?firstObj.subject:@"Task"];
        [self.btnTask setTitle:[NSString stringWithFormat:@"%@",title] forState:UIControlStateNormal];
    }
    else if(count>=1)
    {
        NSString *title = [NSString stringWithFormat:@"%d Tasks",count];
        [self.btnTask setTitle:title forState:UIControlStateNormal];
    }
    
    //if(count)
        [self setNeedsLayout];
}

-(void)removeVisit:(ScheduledVisit *)visit
{
    if(!visit) return;
    
    
    BOOL contains=[visitArrayList containsObject:visit];
    if(!contains) return;
    
    [visitArrayList removeObject:visit];
    
    
    NSUInteger count = [visitArrayList count];
    
    self.btnVisit.userInteractionEnabled=count>0;
    self.btnVisit.hidden=count<=0;
    
    
    if(count==1)
    {
        ScheduledVisit *firstObj = [visitArrayList objectAtIndex:0];
        
        NSString *visitTitle = firstObj.customer.name.length ? firstObj.customer.name : firstObj.customerName.length ? firstObj.customerName : @"Visit";
        [self.btnVisit setTitle:[NSString stringWithFormat:@"%@",visitTitle] forState:UIControlStateNormal];
    }
    else if(count>1)
    {
        NSString *cellVisitTitle = [NSString stringWithFormat:@"%d Visits",count];
        [self.btnVisit setTitle:cellVisitTitle forState:UIControlStateNormal];
    }
    
    //if(count)
        [self setNeedsLayout];
    
}

-(void)removeReminder:(Reminder *)event
{
    if(!event) return;
    BOOL contains=[reminderArrayList containsObject:event];
    if(!contains) return;
    
    [reminderArrayList removeObject:event];
    
    NSUInteger count = [reminderArrayList count];
    
    self.btnReminder.userInteractionEnabled=count>0;
    self.btnReminder.hidden=!count;
    
    
    if(count==1)
    {
        Reminder *firstObj = [reminderArrayList objectAtIndex:0];
        NSString *title = ([firstObj.title rangeOfString:@":"].location !=NSNotFound)? [firstObj.title substringFromIndex:[firstObj.title rangeOfString:@":"].location+1] : firstObj.title;
        [self.btnReminder setTitle:[NSString stringWithFormat:@"%@",title] forState:UIControlStateNormal];
        
        NSLog(@"title=%@",self.btnReminder.titleLabel.text);
    }
    else if(count>1)
    {
        NSString *title = [NSString stringWithFormat:@"%d Reminders",count];
        [self.btnReminder setTitle:title forState:UIControlStateNormal];
    }
    
    //if(count)
        [self setNeedsLayout];
}

-(void)setTodayCell:(BOOL)_todayCell
{
    todayCell = _todayCell;
    
    self.lblDate.font = todayCell ? [UIFont grotesqueBoldFontOfSize:self.lblDate.font.pointSize] :  [UIFont grotesqueFontOfSize:12.0];
    
    self.backgroundColor  = todayCell? [UIColor colorWithRed:200.0/255.0 green:215.0f/255.0f blue:230.0f/255.0f alpha:1.0] : [UIColor whiteColor];
    
    self.lblDate.textColor = todayCell?  [UIColor colorWithRed:0 green:73.0f/255.0f blue:144.0f/255.0f alpha:1.0] : [UIColor blackColor] ;

}


@end
