//
//  CXCalendarView.h
//  Calendar
//
//  Created by Vladimir Grichina on 13.07.11.
//  Copyright 2011 Componentix. All rights reserved.
//




#import "CXCalendarCellView.h"
#import "SliderView.h"
#import "CustomEventStore.h"
#import <EventKit/EventKit.h>


@class CXCalendarView;


@protocol CXCalendarViewDelegate <NSObject>

@optional

- (void) calendarView: (CXCalendarView *) calendarView
        didSelectDate: (NSDate *) selectedDate;

- (void) calendarView: (CXCalendarView *) calendarView
        didChangeMonth: (NSString *) date;


@end

@protocol CalendarDelegate <NSObject>
@optional
- (NSArray *)taskListForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate;
- (NSArray *)visitListForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate;
- (NSArray *)eventListForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate;
- (NSArray *)reminderListForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate;
-(void)calenderDidLoad;

-(void)remindersDidLoad;
-(void)scheduledVisitsDidLoad;
-(void)tasksDidLoad;



@end


@interface CXCalendarView : UIView<SliderViewDelegate,UISearchBarDelegate> {
@protected
    NSCalendar *_calendar;

    NSDate *_selectedDate;

    NSDate *_displayedDate;

    UIView *_monthBar;
    UILabel *_monthLabel;
    UIButton *_monthBackButton;
    UIButton *_monthForwardButton;
    UIButton *_todayButton;
    UIView *_weekdayBar;
    NSArray *_weekdayNameLabels;
    UIView *_gridView;
    NSArray *_dayCells;

    CGFloat _monthBarHeight;
    CGFloat _weekBarHeight;
    
    UIButton* lastButtonSelected;
    NSMutableArray *arrBtnHold;
    id <CalendarDelegate> _calDeligate;
    
    UIButton *btnShowAll;
    UIButton *btnFilter;
    UIButton *btnReminders;
    UIButton *btnVisits;
    UIButton *btnTasks;
   
}

@property(nonatomic, retain) NSCalendar *calendar;

@property(nonatomic, assign) id<CXCalendarViewDelegate> delegate;
@property(nonatomic, assign) id <CalendarDelegate> calDeligate;
@property(nonatomic,retain) NSDate *currentStartDate;
@property(nonatomic,retain) NSDate *currentEndDate;

@property(nonatomic, retain) NSDate *selectedDate;

@property(nonatomic, retain) NSDate *displayedDate;

@property(nonatomic, retain) NSDate *baseDate;

@property(nonatomic, readonly) NSUInteger displayedYear;
@property(nonatomic, readonly) NSUInteger displayedMonth;
@property(nonatomic,retain)    SliderView *sliderView;
@property(nonatomic,retain)  NSMutableArray *arrPrevMonth,*arrNextMonth;
@property(nonatomic, retain) NSMutableArray *arrTask;
@property(nonatomic, retain) NSMutableArray *arrVisit;
@property(nonatomic, retain) NSMutableArray *arrayReturnedAfterPredicate;

@property(nonatomic,assign) NSInteger selectedTabIndex;

@property (nonatomic,retain) UIPopoverController *calendarCellPopoverController;


- (void) monthForward;
- (void) monthBack;

- (void) reset;

// UI
@property(readonly) UIView *monthBar;
@property(readonly) UILabel *monthLabel;
@property(readonly) UIButton *monthBackButton;
@property(readonly) UIButton *monthForwardButton;
@property(readonly) UIButton *todayButton;

//@property(readonly) UIView *weekdayBar;
@property(readonly) NSArray *weekdayNameLabels;
@property(readonly) UIView *gridView;
@property(readonly) NSArray *dayCells;

@property(assign) CGFloat monthBarHeight;
@property(assign) CGFloat weekBarHeight;

- (id)initWithFrame:(CGRect)frame selectedTabIndex:(NSInteger)anIndex;

-(void)selectCellForEvent:(EKEvent *)event;

-(void)reload;
- (CXCalendarCellView *) _cellForDate: (NSDate *) date;

- (CXCalendarCellView *) calenderCellForDate: (NSDate *) date;

- (CXCalendarCellView *) cellForDate: (NSDate *) date;

- (void) touchedCellView: (CXCalendarCellView *) cellView;
- (NSMutableArray *)calculateDaysInFinalWeekOfPreviousMonth;
- (NSUInteger)numberOfDaysInPreviousPartialWeek;
- (NSMutableArray *)calculateDaysInFirstWeekOfFollowingMonth;
- (NSUInteger)numberOfDaysInFollowingPartialWeek;
- (NSArray *)searchArrayUsingPredicateForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate;
- (NSArray *)searchTaskArrayUsingPredicateForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate;

- (void)searchReminders:(NSMutableArray *)reminders andVisits:(NSMutableArray *)visits fromEvents:(NSArray *)events;

-(void)fetchTasksForStartDate:(NSDate *)startDate endDate:(NSDate *)endDate;
-(void)fetchRemindersForStartDate:(NSDate *)startDate endDate:(NSDate *)endDate;
-(void)fetchScheduledVisitsForStartDate:(NSDate *)startDate endDate:(NSDate *)endDate;

-(void)addNewTask:(Task *)task;
-(void)addNewScheduledVisit:(ScheduledVisit *)visit;
-(void)addNewReminder:(Reminder *)reminder;

-(void)removeTask:(Task *)task;
-(void)removeScheduledVisit:(ScheduledVisit *)visit;
-(void)removeReminder:(Reminder *)reminder;

//-(void)editTask:(Task *)task;

-(void)editVisit:(ScheduledVisit *)visit lastStartDate:(NSDate *)date;
-(void)editTask:(Task *)task lastStartDate:(NSDate *)date;
-(void)editReminder:(Reminder *)reminder lastStartDate:(NSDate *)date;


-(NSArray *)calendarCellsFromDate:(NSDate *)sDate endDate:(NSDate *)eDate;

-(void)resetVisitCells;
-(void)resetReminderCells;
-(void)resetTaskCells;

-(void)checkAndUpdateToday;

-(void)editVisit:(ScheduledVisit *)visit;
-(void)editTask:(Task *)task;
-(void)editReminder:(Reminder *)reminder;


@end