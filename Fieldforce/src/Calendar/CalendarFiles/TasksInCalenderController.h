//
//  TaskListViewController.h
//  OEM_CALENDAR
//
//  Created by elie maalouly on 6/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TasksListViewController.h"
//#import "CXCalendarView.h"

@protocol EditTaskDelegate <NSObject>
@required
- (void)selectedTaskForEdit:(int)index;
@end
@interface TasksInCalenderController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    id <EditTaskDelegate> _delegate;
    
    UITableView *_tableView;
}
@property(nonatomic, assign) id <EditTaskDelegate> delegate;
@property(nonatomic,assign) UIView *calenderView;
@property(nonatomic,retain)NSArray *taskList;
@property(nonatomic,retain)UITableView *tableView;
@end
