//
//  CustomEventStore.m
//  OEM_CALENDAR
//
//  Created by elie maalouly on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomEventStore.h"
#import "ADDReminder.h"
#import "Reminder.h"


@implementation CustomEventStore
@synthesize eventStore;
static CustomEventStore *sharedInstance = nil;

+ (CustomEventStore *)sharedInstance
{
    if (sharedInstance == nil) 
    {
        sharedInstance = [[CustomEventStore alloc] init];
        EKEventStore *_eventStore = [[EKEventStore alloc] init];
        sharedInstance.eventStore=_eventStore;
        [_eventStore release];
    }
    return sharedInstance;
}

- (void)dealloc
{
    //[eventStore release];
    [super dealloc];
}

-(EKEventStore *)reminderEventStore
{
  
}

+(NSArray *)getEventsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
//    __block NSArray *arr;
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(queue, ^{
//        
//        CustomEventStore *instance = [CustomEventStore sharedInstance];
//        NSPredicate *predicate = [instance.eventStore predicateForEventsWithStartDate:startDate endDate:endDate calendars:nil];
//        arr= [instance.eventStore eventsMatchingPredicate:predicate];
//        
//        dispatch_sync(dispatch_get_main_queue(), ^{
//            return arr;
//            });
//        });
    
    CustomEventStore *instance = [CustomEventStore sharedInstance];
    NSPredicate *predicate = [instance.eventStore predicateForEventsWithStartDate:startDate endDate:endDate calendars:nil];
    return [instance.eventStore eventsMatchingPredicate:predicate];
}

+(NSArray *)getEventsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate fromCalender:(EKCalendar *)calender
{
    //    __block NSArray *arr;
    //    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    //    dispatch_async(queue, ^{
    //        
    //        CustomEventStore *instance = [CustomEventStore sharedInstance];
    //        NSPredicate *predicate = [instance.eventStore predicateForEventsWithStartDate:startDate endDate:endDate calendars:nil];
    //        arr= [instance.eventStore eventsMatchingPredicate:predicate];
    //        
    //        dispatch_sync(dispatch_get_main_queue(), ^{
    //            return arr;
    //            });
    //        });
    
    if(!calender) return nil;
    
    NSArray *calenders = calender ? [NSArray arrayWithObject:calender] : nil;
    
    CustomEventStore *instance = [CustomEventStore sharedInstance];
    NSPredicate *predicate = [instance.eventStore predicateForEventsWithStartDate:startDate endDate:endDate calendars:calenders];
    return [instance.eventStore eventsMatchingPredicate:predicate];
}


-(EKCalendar *)customCalendarWithIdentifier:(NSString *)indentifier title:(NSString *)title
{
    EKCalendar *calendar =[self.eventStore calendarWithIdentifier:indentifier];
    
    if(!calendar)
    {
     calendar=[EKCalendar calendarWithEventStore:self.eventStore];
    calendar.title = title;
    
    EKSource *theSource = nil;
    for (EKSource *source in eventStore.sources)
    {
        if (source.sourceType == EKSourceTypeLocal) 
        {
            theSource = source;
            break;
        }
    }
    
    if (theSource) 
    {
        calendar.source = theSource;
    } 
    else
    {
        NSLog(@"Error: Local source not available");
        return nil;
    }
    
    
    NSError *error = nil;
    BOOL result = [eventStore saveCalendar:calendar commit:YES error:&error];
    if (result) 
    {
        NSLog(@"Saved calendar to event store.");
       
        return calendar;
    } 
    else
    {
        NSLog(@"Error saving calendar: %@.", error);
        
        return nil;
    }
        
    }
    
    return calendar;
}

-(BOOL)deleteCustomCalendarWithIdentifier:(NSString *)indentifier
{
    BOOL result=NO;
    EKCalendar *calendar = [eventStore calendarWithIdentifier:indentifier];
    if (calendar) 
    {
        NSError *error = nil;
         result = [self.eventStore removeCalendar:calendar commit:YES error:&error];
        if (result)
        {
            NSLog(@"Deleted calendar from event store.");
        } 
        else
        {
            NSLog(@"Deleting calendar failed: %@.", error);
        }
    }
    
    return result;
}

+(EKEvent *)addEventWithTitle:(NSString *)title startDate:(NSDate *)startDate endDate:(NSDate *)endDate location:(NSString *)location notes:(NSString *)notes
{
    
   // NSComparisonResult _result = [startDate compare:[NSDate today]];
   // if(_result==NSOrderedAscending) return nil;
    
  EKAuthorizationStatus _status=  [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    
    if(_status!=EKAuthorizationStatusAuthorized) return nil;
    
   // if(!app.canAccessCalendar) return nil;
  
    EKEvent *event = nil;
    NSError *err   = nil;
    BOOL saved = NO;
    
    @try 
    {
        
    EKEventStore *eventStore = [CustomEventStore sharedInstance].eventStore;
        
       
    
    /*
   [eventStore requestAccessToEntityType:EKEntityMaskEvent completion:^(BOOL granted, NSError *error)
   {
    
    
   }];
    */
        
    event  = [EKEvent eventWithEventStore:eventStore];
    EKCalendar *calendar = [eventStore defaultCalendarForNewEvents];
        
    if(calendar)
    {
        event.calendar= calendar;
    }
    else
    {
        return nil;
    }

        
    event.title     = title;
    
    if(notes)
    event.notes     = notes;
    
    if(location)
    event.location  = location;
    
    if(startDate)
    event.startDate = startDate;
        
    if(endDate)
    event.endDate   = endDate;
        
    event.availability = EKEventAvailabilityFree;
    
   //================== REMOVE PREV ALARMS =============================
    for(EKAlarm *prevAlarm in event.alarms)
    {
        [event removeAlarm:prevAlarm];
    }
    //===================================================================
        
    //================= ALARM: At that time of event ====================
    EKAlarm *_alarm = [EKAlarm alarmWithRelativeOffset:0.0]; 
    [event addAlarm:_alarm];
    //=============================================================
    
  
    if(event)
    {
      //[eventStore saveEvent:event span:EKSpanThisEvent error:&err];
       saved = [eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
    }
        
    }
    @catch (NSException *exception) 
    {
        [exception print];
    }

    @finally 
    {
        if(err)
        {
            NSLog(@"Error=%@",[err localizedDescription]);
        }
        return (err || !saved) ? nil : event;
    }

    return event;
}

+(EKReminder *)addReminder:(Reminder *)reminder
{
    //if(!app.canAccessReminder) return nil;
    
    EKAuthorizationStatus _status=  [EKEventStore authorizationStatusForEntityType:EKEntityTypeReminder];
    
    if(_status!=EKAuthorizationStatusAuthorized) return nil;
        
    EKReminder *event = nil;
    NSError *err = nil;
    
    NSString *title = reminder.title? reminder.title : @"Reminder";
    NSString *notes = reminder.notes? reminder.notes : nil;
    NSString *location = reminder.location?reminder.location:nil;
    
    NSDate *startDate = reminder.startDate;
    NSDate *endDate   = reminder.endDate;
    
    NSString *alarm = reminder.alarms ? reminder.alarms : nil;
    NSString *recurrence = reminder.recurrenceRule ? reminder.recurrenceRule : nil;
    
    
    @try
    {
        EKEventStore *eventStore = [CustomEventStore sharedInstance].eventStore;
        event  = [EKReminder reminderWithEventStore:eventStore];
        

        [event setCalendar:[eventStore defaultCalendarForNewReminders]];

        
        event.title = title ;
        
         if(notes)
         event.notes    = notes;
        
         if(location)
        event.location  = location;
        
        //event.startDateComponents = [startDate dateComponents];
        event.dueDateComponents     = [startDate dateComponents];
        

        
        NSArray *alarms = alarm ? [alarm JSONValue] : nil;
        
        alarms = alarms ? [ADDReminder alarmFromArr:alarms] : nil;
        
        //================= ALARM: At that time of event ====================
        //EKAlarm *_alarm = [EKAlarm alarmWithRelativeOffset:0.0];
        //[event addAlarm:_alarm];
        
        if([alarms count])
        {
            event.alarms = alarms;
        }
        else
        {
            EKAlarm *_alarm = [EKAlarm alarmWithRelativeOffset:0.0];
            [event addAlarm:_alarm];
        }
        
        NSDictionary *recurrenceDict = recurrence ? [recurrence JSONValue] : nil;
        EKRecurrenceRule *recurrenceRule = [ADDReminder recurrenceRuleFromDict:recurrenceDict];
        
        
        
        if(recurrenceRule)
        {
            for(EKRecurrenceRule *recRule in event.recurrenceRules)
                [event removeRecurrenceRule:recRule];
            
            [event addRecurrenceRule:recurrenceRule];
        }
        //=============================================================
        
        if(event)
        [eventStore saveReminder:event commit:YES error:&err];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Event Exception: Name=%@ Reason=%@",exception.name,exception.reason);
    }
    
    @finally
    {
        return err ? nil : event;
    }
    
    return event;
}



+(EKEvent *)editEventForIdentifier:(NSString *)eventIdentifier withTitle:(NSString *)eventTitle startDate:(NSDate *)startDate endDate:(NSDate *)endDate location:(NSString *)location notes:(NSString *)notes
{
   // if(!app.canAccessCalendar) return nil;
    
    EKAuthorizationStatus _status=  [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    
    if(_status!=EKAuthorizationStatusAuthorized) return nil;
    
    EKEvent *event =nil;
    
    if(!eventIdentifier) return nil;
    
     NSError *error=nil;
     BOOL saved=NO;
    
    @try {
    NSLog(@"Event identifier before:%@",eventIdentifier);
    
    EKEventStore *eventStore = [self sharedInstance].eventStore;
     event = [eventStore eventWithIdentifier:eventIdentifier];
    
       
    if (event != nil) 
    {  
        event.title     = eventTitle;
        
        if(startDate)
        event.startDate = startDate;
        
        if(endDate)
        event.endDate   = endDate;
        
        if(location)
        event.location  = location;
        
        if(notes)
        event.notes     = notes;
        
         event.availability = EKEventAvailabilityFree;
        
        //================= At that time of event ====================
        if(![event.alarms count])
        {
          EKAlarm *_alarm = [EKAlarm alarmWithRelativeOffset:0.0]; 
          [event addAlarm:_alarm];
        }
        //=============================================================
        
       // [eventStore saveEvent:event span: EKSpanThisEvent error:&error];
        
      saved =  [eventStore saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
        
        NSLog(@"Event identifier after:%@",event.eventIdentifier);
        
       // [eventStore commit:&error];
    }
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception=%@",[exception reason]);
    }
    @finally 
    {
        return (error || !saved) ? nil : event;
    }
    
}

+(BOOL)deleteEventForEventIdentifier:(NSString *)eventIdentifier
{
    if(!eventIdentifier) return NO;
    
    EKAuthorizationStatus _status=  [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    
    if(_status!=EKAuthorizationStatusAuthorized) return nil;
    
    NSError* error = nil;
    
    BOOL isDeleted=NO;
    
    @try {
        
        EKEventStore *eventStore = [self sharedInstance].eventStore;
        EKEvent *event = [eventStore eventWithIdentifier:eventIdentifier];
    
        if (event != nil) 
        {  
            
            [[CustomEventStore sharedInstance].eventStore removeEvent:event span:EKSpanFutureEvents error:&error];
            NSLog(@"error=%@",[error localizedDescription]);
            
            isDeleted=YES;
            
        } 
    }
    @catch (NSException *exception) 
    {
         NSLog(@"Exception=%@",[exception reason]);
    }
    @finally
    {
        return    isDeleted;
    }
        
    return error ? NO : isDeleted;
}

+(void)deleteAllEvents:(void (^)(void))callback
{
    
    NSString *notes = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleDisplayName"];

    
    NSDate *startDate = [NSDate dateWithTimeIntervalSinceNow:-60*60*24*30*2];    //2 months BACK
    NSDate *endDate   = [NSDate dateWithTimeIntervalSinceNow:60*60*24*30];
    
     NSPredicate *predicate = [[CustomEventStore sharedInstance].eventStore predicateForEventsWithStartDate:startDate endDate:endDate calendars:nil];
    
       
  //  NSMutableArray *results = [NSMutableArray array];
    
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
    dispatch_async([app eventQueue], ^{
    NSArray *_events = [[CustomEventStore sharedInstance].eventStore eventsMatchingPredicate:predicate];
            
   // if([_events count])
      //  [results addObjectsFromArray:_events];
            
           // currentStart = [NSDate dateWithTimeInterval:(seconds_in_year + 1) sinceDate:currentStart];
            
      //  }
        
        NSString *locationStr=[[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleDisplayName"];
        
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"self.location contains[cd] %@ OR self.title contains[cd] %@",locationStr,@":"];
       // [results filterUsingPredicate:searchPredicate];
        
     NSArray *results = [_events filteredArrayUsingPredicate:searchPredicate];
        
        for(EKEvent *event in results)
        {
            [[CustomEventStore sharedInstance].eventStore removeEvent:event span:EKSpanFutureEvents error:nil];
        }
        
        //Filter task and filter visits now
        
        dispatch_async(dispatch_get_main_queue(), ^{
            @try
            {
            
            }
            @catch (NSException *exception)
            {
                
            }
            @finally
            {
                
                NSPredicate *fetchReminderPredicate =[[CustomEventStore sharedInstance].eventStore predicateForCompletedRemindersWithCompletionDateStarting:startDate ending:endDate calendars:nil];
                
               // NSPredicate *fetchReminderPredicate =[[CustomEventStore sharedInstance].eventStore predicateForIncompleteRemindersWithDueDateStarting:startDate ending:endDate calendars:nil];
                
                //[NSPredicate predicateWithFormat:@"self.title contains[cd] %@",@":"];
                
              [[CustomEventStore sharedInstance].eventStore  fetchRemindersMatchingPredicate:fetchReminderPredicate completion:^(NSArray *reminders)
               {
                   
                   NSLog(@"Notes=%@",[reminders valueForKey:@"notes"]);
                   
                   NSPredicate *_reminderPredicate = [NSPredicate predicateWithFormat:@"self.notes contains[c] %@",notes];
                   
                   NSArray *_reminders = [reminders filteredArrayUsingPredicate:_reminderPredicate];
                   for(EKReminder *reminder in _reminders)
                   {
                       [[CustomEventStore sharedInstance].eventStore removeReminder:reminder commit:NO error:nil];
                    }
                   
                   [[CustomEventStore sharedInstance].eventStore commit:nil];
                   
                    if(callback) callback();
               }];
                
                
                //if(callback) callback();
            }
        });
    });
    
}

+(EKReminder *)editReminderForIdentifier:(NSString *)eventIdentifier reminder:(Reminder *)reminder
{
     //if(!app.canAccessReminder) return nil;
    
    EKAuthorizationStatus _status=  [EKEventStore authorizationStatusForEntityType:EKEntityTypeReminder];
    
    if(_status!=EKAuthorizationStatusAuthorized) return nil;
    
    if(!eventIdentifier) return nil;
    
    EKReminder *event =nil;
    NSError *error=nil;
    
    @try {
        
        NSLog(@"Event identifier before:%@",eventIdentifier);
        
        EKEventStore *eventStore = [self sharedInstance].eventStore;
        event =(EKReminder *) [eventStore calendarItemWithIdentifier:eventIdentifier];
        
        
        
        if (event != nil)
        {
            NSError *err = nil;
            
            NSString *title = reminder.title?reminder.title:@"Reminder";
            NSString *notes = reminder.notes?reminder.notes:nil;
            NSString *location = reminder.location?reminder.location:nil;
            
            NSDate *startDate = reminder.startDate;
            NSDate *endDate = reminder.endDate;
            
            NSString *alarm = reminder.alarms ? reminder.alarms : nil;
            NSString *recurrence = reminder.recurrenceRule ? reminder.recurrenceRule : nil;
            
            event.title =title;
         
            //event.startDateComponents = [startDate dateComponents];
            event.dueDateComponents   = [startDate dateComponents];
            
            if(location)
               event.location  = location;
            
            if(notes)
               event.notes     = notes;
            
            //================= At that time of event ====================
            
            NSArray *alarms = alarm ? [alarm JSONValue] : nil;
            
            alarms = alarms ? [ADDReminder alarmFromArr:alarms] : nil;
            
            if([alarms count])
            {
                event.alarms = alarms;
            }
            else
            {
                EKAlarm *_alarm = [EKAlarm alarmWithRelativeOffset:0.0];
                [event addAlarm:_alarm];
            }
            //=============================================================
            
            NSDictionary *recurrenceDict = recurrence ? [recurrence JSONValue] : nil;
            EKRecurrenceRule *recurrenceRule = [ADDReminder recurrenceRuleFromDict:recurrenceDict];
            
            if(recurrenceRule)
            {
                for(EKRecurrenceRule *recRule in event.recurrenceRules)
                    [event removeRecurrenceRule:recRule];
                
                [event addRecurrenceRule:recurrenceRule];
            }

            
            [eventStore saveReminder:event commit:YES error:&error];
            
            NSLog(@"Event identifier after:%@",event.calendarItemIdentifier);
            
            
        }
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception=%@",[exception reason]);
    }
    @finally 
    {
        return error ? nil : event;
    }
    
}

/*
 
 void runDelayed(double delayInSeconds, void (^block)(void)) {
 dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
 dispatch_after(popTime, dispatch_get_main_queue(), block);
 }
 
*/


@end
