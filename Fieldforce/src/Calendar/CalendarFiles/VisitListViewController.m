//
//  VisitListViewController.m
//  OEM_CALENDAR
//
//  Created by elie maalouly on 6/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VisitListViewController.h"
#import <EventKit/EventKit.h>
#import "ScheduledVisit.h"
#import "Customer.h"
#import "Constant.h"

#define CELL_HEIGHT 47.0
@implementation VisitListViewController

@synthesize visitList;
@synthesize tableView;
@synthesize delegate = _delegate;

- (id)initWithVisitList:(NSArray *)aList
{
    self = [super init];
    if (self) {
       
        self.visitList = aList;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    [visitList release];
    [tableView release];
    
    [super dealloc];
}

#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    float _height=self.tableView.contentSize.height;
    self.contentSizeForViewInPopover=CGSizeMake(335.0+15.0, _height-1.0);
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //self.contentSizeForViewInPopover=CGSizeMake(335, 415);
    float _height=self.tableView.contentSize.height;
    self.contentSizeForViewInPopover=CGSizeMake(335.0+15.0, _height);
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    NSLog(@"visitList=%@",self.visitList);
    
    CGRect _rect=self.view.bounds;
    UITableView *aTableview=[[UITableView alloc] initWithFrame:_rect style:UITableViewStylePlain];
    aTableview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    aTableview.delegate=self;
    aTableview.dataSource=self;
    aTableview.showsHorizontalScrollIndicator=NO;
    aTableview.showsVerticalScrollIndicator=YES;
    aTableview.scrollEnabled=YES;
    aTableview.tag=10;
    [self.view addSubview:aTableview];
    self.tableView=aTableview;
    [aTableview release];
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    aTableview.backgroundColor = [UIColor whiteColor];

    
   // [self sortVisitList];
    
    [self performSelector:@selector(selectFirstRowForEdit) withObject:nil afterDelay:0.01];
}

-(void)selectFirstRowForEdit
{
    if([self.visitList count]!=1) return;
    
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    
    if([self.delegate respondsToSelector:@selector(selectedVisitForEdit:)])
        [self.delegate selectedVisitForEdit:indexPath.row];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
     return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
     return CELL_HEIGHT;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.visitList count];
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell =(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType=UITableViewCellAccessoryNone;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        UIButton *accessoryButton=[UIButton buttonWithType:UIButtonTypeCustom];
        accessoryButton.frame=CGRectMake(0, 0, 60, 29);
        [accessoryButton setBackgroundImage:[UIImage imageNamed:@"edit_button.png"] forState: UIControlStateNormal];
		[accessoryButton setTitle:CALENDAR_VIEW_TEXT forState:UIControlStateNormal];
		accessoryButton.titleLabel.font = [UIFont helveticaNeueLTStdBDOfSize:CALENDAR_VIEW_TEXT_FONT_SIZE];
		accessoryButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [accessoryButton addTarget:self action:@selector(editVisit:) forControlEvents:UIControlEventTouchUpInside];
        [cell setAccessoryView:accessoryButton];
    }
    
    int visitCount = [self.visitList count];
    //EKEvent *event = indexPath.row < visitCount ? [self.visitList objectAtIndex:indexPath.row] : nil;
    ScheduledVisit *event = indexPath.row < visitCount ? [self.visitList objectAtIndex:indexPath.row] : nil;
    /*
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"h:mm a"];
    NSString *_time=[formatter stringFromDate:event.visitStartDate];
    _time=[_time stringByAppendingFormat:@"-%@",[formatter stringFromDate:event.visitEndDate]];
    [formatter release];
    */
    
    NSString * dateFormat = @"h:mm a";
    NSString *_time = [event.visitStartDate stringFromDateWithFormat:dateFormat];
    _time=[_time stringByAppendingFormat:@"-%@", [event.visitEndDate stringFromDateWithFormat:dateFormat]];
    
    
    cell.detailTextLabel.textColor=[UIColor blackColor];
    
    cell.imageView.image=[UIImage imageNamed:@"green_dot.png"];
    //cell.textLabel.text=event.title;
    cell.textLabel.text=event.customer?event.customer.name:event.customerName;
    cell.detailTextLabel.text=_time;
    
    
    return cell;
}


-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    
}


-(void)editVisit:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    //NSLog(@"------->%@",[btn superview]);
    UITableViewCell *cell = (UITableViewCell *)[btn superViewOfType:[UITableViewCell class]];
    NSIndexPath *indexPath = [[self tableView] indexPathForCell:cell];
    NSLog(@"indexPath.row=%d",indexPath.row);
    if([self.delegate respondsToSelector:@selector(selectedVisitForEdit:)])
        [self.delegate selectedVisitForEdit:indexPath.row];
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ;
} 


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
