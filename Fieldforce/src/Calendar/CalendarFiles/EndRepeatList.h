//
//  EndRepeatList.h
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol EndRepeatListDelegate <NSObject>
@required
- (void)selectedEndRepeat:(NSString *)strEndRepeat;
@end
@interface EndRepeatList : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    id <EndRepeatListDelegate> _delegate;
    //UITableViewCell *TableCell;
    UIDatePicker *datePicker;
}
@property(nonatomic,retain)NSDate *minDate;
@property(nonatomic,retain)UITableView *_tableView;
//@property(nonatomic,retain)NSString *strRepeat;
@property(nonatomic,retain) NSString *endRepeatDate;
@property(nonatomic, assign) id <EndRepeatListDelegate> delegate;
-(void)setupTableFooterView;
@end
