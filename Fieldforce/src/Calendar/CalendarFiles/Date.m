//
//  Date.m
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Date.h"
#import "InsetTableViewCell.h"

@implementation Date
@synthesize aTableview;
@synthesize startDate,endDate;
@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    
    [datePicker release];
    [aTableview release],aTableview=nil;
    [startDate release],startDate=nil;
    [endDate release],endDate=nil;
    
    [super dealloc];
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    #ifndef __IPHONE_7_0
    self.contentSizeForViewInPopover=CGSizeMake(335.0+15.0, self.aTableview.contentSize.height-5);
    #endif
 
}
-(void)viewDidAppear:(BOOL)animated
{
    //float _height=CGRectGetMaxY(self.aTableview.frame);
    [super viewDidAppear:animated];
    

    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    
    self.preferredContentSize=CGSizeMake(335.0+15.0, self.aTableview.contentSize.height);
else
        self.contentSizeForViewInPopover=CGSizeMake(335.0+15.0, self.aTableview.contentSize.height);


}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    //self.title=@"Select Date";
    
    [self addTitle:@"SELECT DATE" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    TableCell=nil;
    wrongSelection=NO;
    
    
    CGRect _rectGrouped=self.view.bounds;
    UITableView *_aTableview=[[UITableView alloc] initWithFrame:_rectGrouped style:UITableViewStyleGrouped];
    _aTableview.delegate=self;
    _aTableview.dataSource=self;
    _aTableview.showsHorizontalScrollIndicator=NO;
    _aTableview.showsVerticalScrollIndicator=NO;
    _aTableview.scrollEnabled=NO;
    //_aTableview.rowHeight=40;
    _aTableview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_aTableview];
    self.aTableview=_aTableview;
    [_aTableview release];
    
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.aTableview.backgroundColor = REMINDER_BG_COLOR;
        self.aTableview.sectionFooterHeight = 1.0;
        self.aTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.aTableview.bounds), 10.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        self.aTableview.tableHeaderView = _headerView;
        [_headerView release];
    }

    
    
    [self setupTableFooterView];
    
    /*
     //[self.navigationItem setHidesBackButton:YES animated:YES];
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCancel setImage:[UIImage imageNamed:@"cancel_button.png"] forState:UIControlStateNormal];
    btnCancel.frame =CGRectMake(0, 0, 60, 29);
    [btnCancel addTarget:self 
                  action:@selector(cancelPopOver:) 
        forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc]initWithCustomView:btnCancel];
	self.navigationItem.leftBarButtonItem = barButtonCancel;
	[barButtonCancel release];
    */
    
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
    UIButton *btnDone= [UIButton repToolButtonWithImageName:NAV_DONE_BUTTON text:@"Done"];
    btnDone.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
    [btnDone addTarget:self action:@selector(saveChanges:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithCustomView:btnDone];
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
    
    [self addBackButtonWithImage:[UIImage imageNamed:NAV_BACK_BUTTON] title:@"Back" selector:nil];
    
    }else
    {
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(saveChanges:)];
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
   }
    
    /// ====================== TO APPLY CUSTOM FONT =======================================
    //[barButtonDone setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIFont grotesqueBoldFontOfSize:12.0], UITextAttributeFont,nil] forState:UIControlStateNormal];
    
    _selectedRow=0;
    
    //NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_selectedRow inSection:0];
    //[self.aTableview selectRowAtIndexPath:indexPath animated:NO scrollPosition: UITableViewScrollPositionNone];
}

-(void)setupTableFooterView
{
    /* Adiing table footerview */
    float _footerHeight=216.0f;
    float _posX=0.0;
    float _posY=0.0;
    
    UIView *tableFooterView=[[UIView alloc] initWithFrame:CGRectMake(_posX, _posY, CGRectGetWidth(self.aTableview.frame), _footerHeight)];
    tableFooterView.backgroundColor=[UIColor clearColor];
    //tableFooterView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    NSDateFormatter* formatter = [NSDate formatter];
    NSLog(@"self.startDate=%@",self.startDate);
    
    
    [formatter setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
    
    NSDate *_date  = [formatter dateFromString:self.startDate];
    NSLog(@"_date=%@",_date);
    
    
    datePicker = [[UIDatePicker alloc] initWithFrame:tableFooterView.bounds];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    datePicker.hidden = NO;
    datePicker.minimumDate = [NSDate date];
    datePicker.date = _date;//[formatter dateFromString:_date];
    
    datePicker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [datePicker addTarget:self
                   action:@selector(LabelChange:)
         forControlEvents:UIControlEventValueChanged];
    [tableFooterView addSubview:datePicker];
    
    
    self.aTableview.tableFooterView=tableFooterView;
    
    [tableFooterView release];
}

- (void)LabelChange:(id)sender
{
    NSDateFormatter *df = [NSDate formatter];
    [df setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
    NSLog(@"datePicker.date=%@",datePicker.date);
    
    
    
    if(_selectedRow==0)
    {
        self.startDate = [df stringFromDate:datePicker.date];
        NSLog(@"startDate=%@",self.startDate);
        
        
        [df setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
        NSDate *d1=[df dateFromString:self.startDate];
        NSLog(@"d1=%@",d1);
        NSDate *d2=[df dateFromString:self.endDate];
        NSLog(@"d2=%@",d2);
        
        
        [df setDateFormat:@"dd"];
        NSString *strDate1=[df stringFromDate:d1];
        NSLog(@"strDate1=%@",strDate1);
        
        
        NSString *strDate2=[df stringFromDate:d2];
        NSLog(@"strDate2=%@",strDate2);
        if([strDate1 isEqualToString:strDate2])
        {
            [df setDateFormat:@"h:mm a"];
            d2=[NSDate dateWithTimeInterval:60*60 sinceDate:d1];
            NSLog(@"d2=%@",d2);
            [df setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
            self.endDate=[df stringFromDate:d2];
            NSLog(@"self.endDate=%@",self.endDate);
        }
        
    }
    else
    {
        self.endDate = [df stringFromDate:datePicker.date];
        NSLog(@"self.endDate=%@",self.endDate);
    }
    
    
    
    
    NSDate *_date1  = [df dateFromString:self.startDate];
    NSDate *_date2  = [df dateFromString:self.endDate];
    NSComparisonResult result=[_date1 compare:_date2];
    switch (result)
    {
        case NSOrderedAscending:
        {
            wrongSelection=NO;
            NSLog(@"%@ is before %@", self.startDate, self.endDate); 
            break;
        }
        case NSOrderedDescending:
        {
            wrongSelection=YES;
            NSLog(@"%@ is after %@", self.startDate, self.endDate); 
            break;
        }
        case NSOrderedSame:
        {
            wrongSelection=NO;
            NSLog(@"%@ is same %@", self.startDate, self.endDate);
            break;
        }
        default:
        {
            NSLog(@"erorr dates %@, %@", self.startDate, self.endDate);
            break;
        }
    }
    
    
    [self.aTableview reloadData];
    
}

-(void)cancelPopOver:(id)sender
{
    if([self.delegate respondsToSelector:@selector(selectedStartDate: endDate:)])
        [self.delegate selectedStartDate:nil endDate:nil];
    
    TableCell=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)saveChanges:(id)sender
{
    if(wrongSelection)
    {
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle: CANNOT_SAVE_REMINDER_HEADER
                              message:CANNOT_SAVE_REMINDER_MSG
                              delegate:self 
                              cancelButtonTitle:@"Ok" 
                              otherButtonTitles:nil, 
							  nil];
		[alert show];
		[alert release];
    }
    else
    {
        if([self.delegate respondsToSelector:@selector(selectedStartDate: endDate:)])
            [self.delegate selectedStartDate:self.startDate endDate:self.endDate];
        
        TableCell=nil;
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];

    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell =(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType=UITableViewCellAccessoryNone;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
    }
    
    switch (indexPath.row)
    {
        case 0:
        {
            cell.textLabel.text=@"Date";
            if(self.startDate!=nil)
                cell.detailTextLabel.text=self.startDate;
            if(wrongSelection)
                cell.detailTextLabel.textColor=[UIColor redColor];
            else
                cell.detailTextLabel.textColor=[UIColor colorWithRed:56.0/255.0 green:84.0/255.0 blue:135.0/255.0 alpha:1.0];
        }
            break;
            
            /*
             case 1:
             {
             cell.textLabel.text=@"Ends";
             if(self.endDate!=nil)
             {
             NSLog(@"self.startDate=%@",self.startDate);
             NSLog(@"self.endDate=%@",self.endDate);
             NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
             [formatter setDateFormat:@"EEE MMM dd, yyyy, h:mm a"];
             NSDate *d1=[formatter dateFromString:self.startDate];
             NSLog(@"d1=%@",d1);
             NSDate *d2=[formatter dateFromString:self.endDate];
             NSLog(@"d2=%@",d2);
             
             
             [formatter setDateFormat:@"dd"];
             NSString *strDate1=[formatter stringFromDate:d1];
             NSLog(@"strDate1=%@",strDate1);
             
             
             NSString *strDate2=[formatter stringFromDate:d2];
             NSLog(@"strDate2=%@",strDate2);
             if([strDate1 isEqualToString:strDate2])
             {
             [formatter setDateFormat:@"h:mm a"];
             //NSDate *d1=[formatter dateFromString:self._endDate];
             NSLog(@"d2=%@",d2);
             cell.detailTextLabel.text=[formatter stringFromDate:d2];
             NSLog(@"cell.detailTextLabel.text=%@",cell.detailTextLabel.text);
             }
             else
             cell.detailTextLabel.text=self.endDate;
             
             if(wrongSelection)
             cell.detailTextLabel.textColor=[UIColor redColor];
             else
             cell.detailTextLabel.textColor=[UIColor colorWithRed:56.0/255.0 green:84.0/255.0 blue:135.0/255.0 alpha:1.0];
             [formatter release];
             }
             }
             break;
             */
        default:
            break;
    }
    
    //    if(_selectedRow==indexPath.row)
    //    {
    //        NSIndexPath *_indexPath = [NSIndexPath indexPathForRow:_selectedRow inSection:0];
    //        [aTableView selectRowAtIndexPath:_indexPath animated:NO scrollPosition: UITableViewScrollPositionNone];
    //    }
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.tag = 1;
    
    
    int numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableCell = [tableView cellForRowAtIndexPath:indexPath];
    _selectedRow=indexPath.row;
    
    switch (indexPath.row)
    {
        case 0:
        {
            NSDateFormatter* formatter = [NSDate formatter];
            [formatter setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
            
            NSDate *_date  = [formatter dateFromString:self.startDate];
            NSLog(@"_date=%@",_date);
            datePicker.date = _date;
            
        }
            break;
            
        case 1:
        {
            NSDateFormatter* formatter = [NSDate formatter];
            [formatter setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
            
            NSDate *_date  = [formatter dateFromString:self.endDate];
            NSLog(@"_date=%@",_date);
            datePicker.date = _date;
    
        }
            break;
        default:
            break;
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
