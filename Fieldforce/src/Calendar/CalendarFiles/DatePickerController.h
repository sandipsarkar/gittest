//
//  DatePickerController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 20/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DateDidChangeCallback) (NSDate *date);
@interface DatePickerController : UIViewController
{
    UIDatePicker *datePicker;
    UIDatePickerMode _pickerMode;
}

@property(nonatomic,retain) NSDate *date;

- (id)initWithDate:(NSDate *)_date pickerMode:(UIDatePickerMode)pickerMode;


-(void)addDateChangeCallback:(DateDidChangeCallback)callback;

@end
