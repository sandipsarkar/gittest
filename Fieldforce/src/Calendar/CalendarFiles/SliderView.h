//
//  SliderView.h
//  OEM_CALENDAR
//
//  Created by elie maalouly on 4/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SliderView;
@protocol SliderViewDelegate <NSObject>
@required
- (void)selectedMonth:(int)month;
- (void)selectedYear:(int)year;

- (void)selectedMonth:(int)month year:(int)year;

@end

@interface SliderView : UIView
{
    UIButton *btnPrevYear,*btnNextYear,*btnSelectedMonth;
    UILabel *lblJan,*lblFeb,*lblMarch,*lblApril,*lblMay,*lblJune,*lblJuly,*lblAug,*lblSep,*lblOct,*lblNov,*lblDec;
    NSMutableDictionary *dictRect;
    NSArray *arrMonth;
    NSString *currentMonth;
    int currentMonthIndex,prevMonthIndex,nextMonthIndex;
    float offset;
    //CGPoint			startPosition;			// starting position
	//CGPoint			currentPosition;		// current position
    id <SliderViewDelegate> _delegate;
    
}
@property(nonatomic, assign) id <SliderViewDelegate> delegate;
@property(nonatomic,retain)UILabel *lblJan,*lblFeb,*lblMarch,*lblApril,*lblMay,*lblJune,*lblJuly,*lblAug,*lblSep,*lblOct,*lblNov,*lblDec;
@property(nonatomic,retain)NSString *strCurrentYear,*strPrevYear,*strNextYear,*strLastSelectedText;
@property(nonatomic,assign)int month;
@property(nonatomic,assign)BOOL  prevYear,nextYear;
- (UILabel *)newLabelWithPrimaryColor:(UIColor *)primaryColor selectedColor:(UIColor *)selectedColor fontSize:(CGFloat)fontSize bold:(BOOL)bold;

-(void)selectDate:(NSDate *)date;
-(void)selectToday;

@end
