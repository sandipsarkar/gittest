//
//  RepeatList.h
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RepeatListDelegate <NSObject>
@required
- (void)selectedRepeat:(int)indexRepeat;
@end
@interface RepeatList : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    id <RepeatListDelegate> _delegate;
    UITableViewCell *TableCell;
    NSMutableArray *arrRepeatList;
}
@property(nonatomic,retain)UITableView *_tableView;
@property(nonatomic,retain)NSString *strRepeat;
@property(nonatomic,assign)int index;
@property(nonatomic, assign) id <RepeatListDelegate> delegate;
@end
