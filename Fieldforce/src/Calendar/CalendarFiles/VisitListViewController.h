//
//  VisitListViewController.h
//  OEM_CALENDAR
//
//  Created by elie maalouly on 6/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol EditVisitDelegate <NSObject>
@required
- (void)selectedVisitForEdit:(int)index;
@end
@interface VisitListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    id <EditVisitDelegate> _delegate;
}
@property (nonatomic, assign)  id <EditVisitDelegate> delegate;
@property (nonatomic, retain)  NSArray *visitList;
@property (nonatomic, retain)  UITableView *tableView;

- (id)initWithVisitList:(NSArray *)aList;

@end
