//
//  ADDVisit.h
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Start_EndDate.h"
#import "CustomEventStore.h"

@class ADDVisit,ScheduledVisit;
@protocol ADDVisitDelegate <NSObject>
@required
- (void)dismissPopOverFromViewVisit:(BOOL)done;

@optional
//type=1 add type=2 edit type=3 delete
- (void)didChangeVisit:(ScheduledVisit *)visit actionType:(int)type;
@end

typedef void(^SaveScheduledVisitCallback) (ScheduledVisit *visit,int actionType,NSDate *lastStartDate);

@class Customer;
@interface ADDVisit : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,Start_EndDateDelegate>
{
    id <ADDVisitDelegate> _delegate;
    NSMutableArray *arrSearchResult;
    NSIndexPath *_selectedIndexPath;
    UITableViewCell *TableCell;
    UIButton *btnDoneHold;
    UIButton *deleteButton;
    
    Start_EndDate *dateController ;
    int Track;
}
@property(nonatomic, assign) id <ADDVisitDelegate> delegate;
@property(nonatomic,retain)UITableView *_tableView;
@property(nonatomic,retain) NSIndexPath *selectedIndexPath;
@property(nonatomic,retain)UITableView *_tableViewGrouped;
@property(nonatomic,retain) NSString *_startDate;
@property(nonatomic,retain) NSString *_endDate;
@property(nonatomic,retain) NSString *dealerName;
@property(nonatomic,retain) NSString *suburb;
//@property(nonatomic,retain) NSString *visitTittle;
//@property(nonatomic,retain) NSString *visitId;
@property(nonatomic,retain) NSNumber *visitId;
@property(nonatomic,retain) NSDate *selectedStartDate;
@property(nonatomic,retain) NSDate *selectedEndDate;
//@property(nonatomic,readonly) EKEvent *visit;
@property(nonatomic,readonly) ScheduledVisit *visit;
@property(nonatomic,retain) Customer *visitingCustomer;


-(id)initWithDealerName:(NSString *)_dealerName;
-(id)initWithDealerName:(NSString *)_dealerName suburb:(NSString *)_suburb;
-(void)beginWithStartEndDateController;

+(EKEvent *)createNewVisitForTitle:(NSString *)aTitle suburbName:(NSString *)suburbName startDate:(NSDate *)aStartDate endDate:(NSDate *)aEndDate;

+(void)createNewVisitForCustomerName:(NSString *)aCustomerName suburbName:(NSString *)suburbName startDate:(NSDate *)aStartDate endDate:(NSDate *)aEndDate completion:(void(^)(ScheduledVisit *event))callback;

+(void)createNewVisitForCustomer:(Customer *)customer startDate:(NSDate *)aStartDate endDate:(NSDate *)aEndDate completion:(void(^)(ScheduledVisit *event))callback;

+(void)newScheduledVisitWithCustomer:(Customer *)customer startDate:(NSDate *)aStartDate endDate:(NSDate *)aEndDate completion:(void(^)(ScheduledVisit *event))callback;

-(void)addSaveScheduledVisitCallback:(SaveScheduledVisitCallback)callback;

@end
