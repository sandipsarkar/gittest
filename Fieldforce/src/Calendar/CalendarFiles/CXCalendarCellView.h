//
//  CXCalendarCellView.h
//  Calendar
//
//  Created by Vladimir Grichina on 13.07.11.
//  Copyright 2011 Componentix. All rights reserved.
//

#import "ADDReminder.h"
#import "ADDVisit.h"
#import "ReminderListViewController.h"
#import "VisitListViewController.h"
#import "TasksInCalenderController.h"
@interface CXCalendarCellView : UIView<UIPopoverControllerDelegate,ADDReminderDelegate,EditReminderDelegate,ADDVisitDelegate,EditVisitDelegate,EditTaskDelegate>
{
    NSUInteger _day;
  //  UIPopoverController *popOverReminder;
    UIButton *selectedButton;
    BOOL todayCell;
}


@property(nonatomic,retain)NSMutableArray *reminderArrayList;
@property(nonatomic,retain)NSMutableArray *visitArrayList;
@property(nonatomic,retain)NSMutableArray *taskArrayList;


@property(nonatomic, assign) NSUInteger day;
@property(nonatomic, assign) NSUInteger month;
@property(nonatomic, assign) BOOL selected;
@property(nonatomic,retain) UILabel *lblDate;
@property(nonatomic,assign) UIButton *btnTask;
@property(nonatomic,assign) UIButton *btnReminder;
@property(nonatomic,assign) UIButton *btnVisit;

- (NSDate *) dateWithBaseDate: (NSDate *) baseDate withCalendar: (NSCalendar *)calendar;

-(void)setTodayCell:(BOOL)_todayCell;

-(void)selectVisit:(EKEvent *)visit;

//-(void)selectEvent:(EKEvent *)event;
-(void)selectEvent:(id)event;

-(void)addVisit:(ScheduledVisit *)visit;
-(void)addTask:(Task *)task;
-(void)addReminder:(Reminder *)event;
-(void)reset;

-(void)removeTask:(Task *)task;
-(void)removeReminder:(Reminder *)reminder;
-(void)removeVisit:(ScheduledVisit *)visit;

@end

@protocol CXCalendarCellDelegate <NSObject>

@optional
-(void)calendarCell:(CXCalendarCellView *)cell didSelectReminderWithControler:(UIViewController *)controller;

@end
