//
//  SecondAlertList.m
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SecondAlertList.h"
#import "InsetTableViewCell.h"

@implementation SecondAlertList
@synthesize delegate = _delegate;
@synthesize strSecondAlert;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    [strSecondAlert release],strSecondAlert=nil;
    
    [super dealloc];
}

#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    #ifndef __IPHONE_7_0
    self.contentSizeForViewInPopover=CGSizeMake(335+15.0, 415);
    #endif
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    self.preferredContentSize=CGSizeMake(335+15.0, 415);
else
     self.contentSizeForViewInPopover=CGSizeMake(335+15.0, 415);

}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    [self addTitle:@"REMINDER ALERT" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    TableCell=nil;
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    UITableView *aTableview=[[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    aTableview.delegate=self;
    aTableview.dataSource=self;
    aTableview.showsHorizontalScrollIndicator=NO;
    aTableview.showsVerticalScrollIndicator=NO;
    aTableview.scrollEnabled=NO;
    //aTableview.rowHeight=40;
    aTableview.autoresizingMask=(UIViewAutoresizingFlexibleLeftMargin |
                                 UIViewAutoresizingFlexibleRightMargin |
                                 UIViewAutoresizingFlexibleWidth |
                                 UIViewAutoresizingFlexibleTopMargin |
                                 UIViewAutoresizingFlexibleHeight |
                                 UIViewAutoresizingFlexibleBottomMargin);
    [self.view addSubview:aTableview];
    [aTableview release];
    
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCancel setImage:[UIImage imageNamed:@"cancel_button.png"] forState:UIControlStateNormal];
    btnCancel.frame =CGRectMake(0, 0, 60, 29);
    [btnCancel addTarget:self 
                  action:@selector(cancelPopOver:) 
        forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc]initWithCustomView:btnCancel];
	self.navigationItem.leftBarButtonItem = barButtonCancel;
	[barButtonCancel release];
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
    UIButton *btnDone = [UIButton repToolButtonWithImageName:NAV_DONE_BUTTON text:@"Done"];
    btnDone.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
    [btnDone addTarget:self
                action:@selector(saveChanges:)
      forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithCustomView:btnDone];
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
    
    aTableview.backgroundColor = REMINDER_BG_COLOR;
}
else
{
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(saveChanges:)];
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
}
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        aTableview.sectionFooterHeight = 1.0;
        aTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(aTableview.bounds), 10.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        aTableview.tableHeaderView = _headerView;
        [_headerView release];
    }

}

-(void)cancelPopOver:(id)sender
{
    //UIButton *btn=(UIButton *)sender;
    if([self.delegate respondsToSelector:@selector(selectedSecondAlertTime:)])
        [self.delegate selectedSecondAlertTime:nil];
    TableCell=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)saveChanges:(id)sender
{
    //UIButton *btn=(UIButton *)sender;
    NSString *str = TableCell.textLabel.text;
    if([self.delegate respondsToSelector:@selector(selectedSecondAlertTime:)])
        [self.delegate selectedSecondAlertTime:str];
    TableCell=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return 9;
}



- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell =(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType=UITableViewCellAccessoryNone;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
    }
    
    switch (indexPath.row)
    {
        case 0:
        {
            cell.textLabel.text=@"None";
        }
            break;
            
        case 1:
        {
            cell.textLabel.text=@"At time of event";
        }
            break;
            
        case 2:
        {
            cell.textLabel.text=@"5 minutes before";
        }
            break;
            
        case 3:
        {
            cell.textLabel.text=@"15 minutes before";
        }
            break;
        case 4:
        {
            cell.textLabel.text=@"30 minutes before";
        }
            break;
        case 5:
        {
            cell.textLabel.text=@"1 hour before";
        }
            break;
        case 6:
        {
            cell.textLabel.text=@"2 hours before";
        }
            break;
        case 7:
        {
            cell.textLabel.text=@"1 day before";
        }
            break;
        case 8:
        {
            cell.textLabel.text=@"2 days before";
        }
            break;
        default:
            break;
    }
    
    
    if([cell.textLabel.text isEqualToString:self.strSecondAlert])
    {
        //UIImageView *viewSelected = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark.png"]];
        //cell.accessoryView =viewSelected;
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        TableCell=cell;
        //[viewSelected release];
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(TableCell!=nil)
    {
       // TableCell.accessoryView =nil;
        TableCell.accessoryType = UITableViewCellAccessoryNone;
    }
   // UIImageView *viewSelected = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark.png"]];
    TableCell = [tableView cellForRowAtIndexPath:indexPath];
    TableCell.accessoryType = UITableViewCellAccessoryCheckmark;
   // TableCell.accessoryView =viewSelected;
   // [viewSelected release];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
