//
//  ADDReminder.h
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertList.h"
#import "SecondAlertList.h"
#import "Date.h"
#import "RepeatList.h"
#import "EndRepeatList.h"
    /////////////Event//////////////
#import <EventKit/EventKit.h>
#import "CustomEventStore.h"

@class ADDReminder,Reminder;
@protocol ADDReminderDelegate <NSObject>
@required
- (void)dismissPopOverFromViewReminder:(BOOL)done;
@end

typedef void(^SaveReminderCallback) (Reminder *reminder,int actionType,NSDate *lastStartDate);

@interface ADDReminder : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,AlertListDelegate,SecondAlertListDelegate,StartDelegate,RepeatListDelegate,EndRepeatListDelegate>
{
    id <ADDReminderDelegate> _delegate;
    NSIndexPath *_selectedIndexPath;
    UIButton *btnDoneHold;
    NSMutableArray *_arrRepeatList;
    
    /////////////Event//////////////
    //NSMutableArray *arrayofCalIDs;
}
@property(nonatomic,retain)UITextField *txtTittle;
@property(nonatomic, assign) id <ADDReminderDelegate> delegate;
@property(nonatomic,retain)UITableView *_tableView;
@property(nonatomic,retain) NSIndexPath *selectedIndexPath;
@property(nonatomic,retain) NSString *reminderTittle;
@property(nonatomic,retain) NSString *_startDate;
@property(nonatomic,retain) NSString *_endDate;
@property(nonatomic,retain) NSString *_alert;
@property(nonatomic,retain) NSString *_secondAlert;
@property(nonatomic,retain) NSString *_repeat;
@property(nonatomic,retain) NSString *_endRepeat;
@property(nonatomic,retain) NSString *reminderId;
@property(nonatomic,retain) Reminder *reminder;
NSUInteger daysBetween(NSDate *fromDate, NSDate *toDate);
NSDate *setDateToMidnite(NSDate *aDate);

+(EKEvent *)reminderFromDict:(NSDictionary *)dict;
+(NSMutableDictionary *)dictValueForReminder:(EKEvent *)event;
+(NSDictionary *)dictValueForRecurrenceEnd:(EKRecurrenceEnd *)recurrenceEnd;
+(NSMutableDictionary *)dictValueForRecurrence:(EKRecurrenceRule *)recurrenceRule;
+(NSMutableArray *)dictValueForAlarms:(NSArray *)alarms;
+(EKEvent *)updateReminder:(EKEvent *)event fromDict:(NSDictionary *)dict;
+(EKRecurrenceRule *)recurrenceRuleFromDict:(NSDictionary *)_recurrenceDict;
+(NSMutableArray *)alarmFromArr:(NSArray *)arr;

-(void)addSaveReminderCallback:(SaveReminderCallback)callback;

@end