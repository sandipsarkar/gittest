//
//  RepeatList.m
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RepeatList.h"
#import "InsetTableViewCell.h"

@implementation RepeatList
@synthesize delegate = _delegate;
@synthesize _tableView;
@synthesize strRepeat;
@synthesize index;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

- (void)dealloc
{
    [arrRepeatList release];
    [_tableView release],_tableView=nil;
    [strRepeat release],strRepeat=nil;
    
    [super dealloc];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    #ifndef __IPHONE_7_0
    float _height=self._tableView.contentSize.height;
    self.contentSizeForViewInPopover=CGSizeMake(335+15.0, _height-1.0);
    #endif
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    float _height=self._tableView.contentSize.height;
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    self.preferredContentSize=CGSizeMake(335+15.0, _height);
else
    self.contentSizeForViewInPopover=CGSizeMake(335+15.0, _height);


}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif

     [self addTitle:@"REPEAT" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    TableCell=nil;
    arrRepeatList=[[NSMutableArray alloc] init];
    [arrRepeatList addObject:@"None"];
    [arrRepeatList addObject:@"Every Day"];
    [arrRepeatList addObject:@"Every Week"];
    [arrRepeatList addObject:@"Every 2 Weeks"];
    [arrRepeatList addObject:@"Every Month"];
    [arrRepeatList addObject:@"Every Year"];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    CGRect _rectGrouped=CGRectMake(0,0, self.view.bounds.size.width, self.view.bounds.size.height);
    UITableView *aTableview=[[UITableView alloc] initWithFrame:_rectGrouped style:UITableViewStyleGrouped];
    aTableview.delegate=self;
    aTableview.dataSource=self;
    aTableview.showsHorizontalScrollIndicator=NO;
    aTableview.showsVerticalScrollIndicator=NO;
    aTableview.scrollEnabled=NO;
    //aTableview.rowHeight=40;
    aTableview.autoresizingMask = UIViewAutoresizingFlexibleWidth
    | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:aTableview];
    self._tableView=aTableview;
    [aTableview release];
    
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCancel setImage:[UIImage imageNamed:@"cancel_button.png"] forState:UIControlStateNormal];
    btnCancel.frame =CGRectMake(0, 0, 60, 29);
    [btnCancel addTarget:self 
                  action:@selector(cancelPopOver:) 
        forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc]initWithCustomView:btnCancel];
	self.navigationItem.leftBarButtonItem = barButtonCancel;
	[barButtonCancel release];
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
    
    self._tableView.backgroundColor = REMINDER_BG_COLOR;
    UIButton *btnDone= [UIButton repToolButtonWithImageName:NAV_DONE_BUTTON text:@"Done"];
    btnDone.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
    [btnDone addTarget:self
                action:@selector(saveChanges:) 
      forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithCustomView:btnDone];
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
    
}
else
{
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(saveChanges:)];
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
}
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self._tableView.sectionFooterHeight = 1.0;
        self._tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self._tableView.bounds), 10.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        self._tableView.tableHeaderView = _headerView;
        [_headerView release];
    }

}

-(void)cancelPopOver:(id)sender
{
    //UIButton *btn=(UIButton *)sender;
    int _index = -1;
    if([self.delegate respondsToSelector:@selector(selectedRepeat:)])
        [self.delegate selectedRepeat:_index];
    TableCell=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)saveChanges:(id)sender
{
    //UIButton *btn=(UIButton *)sender;
    //NSString *str = TableCell.textLabel.text;
    int _index=[arrRepeatList indexOfObject:TableCell.textLabel.text];
    if([self.delegate respondsToSelector:@selector(selectedRepeat:)])
        [self.delegate selectedRepeat:_index];
    TableCell=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return 6;
}



- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
   
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell =(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType=UITableViewCellAccessoryNone;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
    }
    
    switch (indexPath.row)
    {
        case 0:
        {
            cell.textLabel.text=[arrRepeatList objectAtIndex:indexPath.row];
        }
            break;
            
        case 1:
        {
            cell.textLabel.text=[arrRepeatList objectAtIndex:indexPath.row];
        }
            break;
            
        case 2:
        {
            cell.textLabel.text=[arrRepeatList objectAtIndex:indexPath.row];
        }
            break;
            
        case 3:
        {
            cell.textLabel.text=[arrRepeatList objectAtIndex:indexPath.row];
        }
            break;
        case 4:
        {
            cell.textLabel.text=[arrRepeatList objectAtIndex:indexPath.row];
        }
            break;
        case 5:
        {
            cell.textLabel.text=[arrRepeatList objectAtIndex:indexPath.row];
        }
            break;
        default:
            break;
    }
    
    if(self.index==indexPath.row)
    {
        //UIImageView *viewSelected = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark.png"]];
        //cell.accessoryView =viewSelected;
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        TableCell=cell;
        //[viewSelected release];
    }
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.tag = 1;
    
    
    NSUInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(TableCell!=nil)
    {
        //TableCell.accessoryView =nil;
         TableCell.accessoryType = UITableViewCellAccessoryNone;
    }
    
   // UIImageView *viewSelected = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark.png"]];
    TableCell = [tableView cellForRowAtIndexPath:indexPath];
   // TableCell.accessoryView =viewSelected;
   // [viewSelected release];
    TableCell.accessoryType = UITableViewCellAccessoryCheckmark;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
