//
//  ReminderListViewController.m
//  OEM_CALENDAR
//
//  Created by elie maalouly on 6/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ReminderListViewController.h"
#import <EventKit/EventKit.h>
#import "Constant.h"
#define CELL_HEIGHT 47.0

@implementation ReminderListViewController

@synthesize reminderList;
@synthesize tableView;
@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    [reminderList release];
    reminderList=nil;
    [tableView release];
    tableView=nil;
    
    [super dealloc];
}
#pragma mark - View lifecycle

-(void)viewDidAppear:(BOOL)animated
{
    //self.contentSizeForViewInPopover=CGSizeMake(335, 415);
    float _height=self.tableView.contentSize.height;
    self.contentSizeForViewInPopover=CGSizeMake(335+14.0, _height-1);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    float _height=self.tableView.contentSize.height;
    self.contentSizeForViewInPopover=CGSizeMake(335+15.0, _height-1.0);
}




// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
   // NSLog(@"reminderList=%@",self.reminderList);
    
    CGRect _rect=self.view.bounds;
    UITableView *aTableview=[[UITableView alloc] initWithFrame:_rect style:UITableViewStylePlain];
    aTableview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    aTableview.delegate=self;
    aTableview.dataSource=self;
    aTableview.showsHorizontalScrollIndicator=NO;
    aTableview.showsVerticalScrollIndicator=YES;
    aTableview.scrollEnabled=YES;
    aTableview.tag=10;
    [self.view addSubview:aTableview];
    self.tableView=aTableview;
    [aTableview release];
    
    //[self sortReminderList];
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    aTableview.backgroundColor = [UIColor whiteColor];


[self performSelector:@selector(selectFirstRowForEdit) withObject:nil afterDelay:0.01];
    
}

-(void)selectFirstRowForEdit
{
    if([self.reminderList count]!=1) return;
    
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    
    if([self.delegate respondsToSelector:@selector(selectedReminderForEdit:)])
        [self.delegate selectedReminderForEdit:indexPath.row];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.reminderList count];
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell =(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType=UITableViewCellAccessoryNone;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    cell.imageView.image=[UIImage imageNamed:@"yellow_dot.png"];
    EKEvent *event=[self.reminderList objectAtIndex:indexPath.row];
    cell.textLabel.text=event.title;
    
    /*
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"h:mm a"];
    NSString *_time=[formatter stringFromDate:event.startDate];
    [formatter release];
     */
    
    NSString * dateFormat = @"h:mm a";
    NSString *_time = [event.startDate stringFromDateWithFormat:dateFormat];
    
    cell.detailTextLabel.text=_time;
    cell.detailTextLabel.textColor=[UIColor blackColor];
    
    UIButton *accessoryButton=[UIButton buttonWithType:UIButtonTypeCustom];
    accessoryButton.frame=CGRectMake(0, 0, 60, 29);
    [accessoryButton setBackgroundImage:[UIImage imageNamedNoCache:@"edit_button.png"] forState: UIControlStateNormal];
	[accessoryButton setTitle:CALENDAR_VIEW_TEXT forState:UIControlStateNormal];
	accessoryButton.titleLabel.font = [UIFont helveticaNeueLTStdBDOfSize:CALENDAR_VIEW_TEXT_FONT_SIZE];
	accessoryButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [accessoryButton addTarget:self action:@selector(editReminder:) forControlEvents:UIControlEventTouchUpInside];
    [cell setAccessoryView:accessoryButton];
    return cell;	
}


-(void)editReminder:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    //NSLog(@"------->%@",[btn superview]);
    UITableViewCell *cell = (UITableViewCell *)[btn superViewOfType:[UITableViewCell class]];
    NSIndexPath *indexPath = [[self tableView] indexPathForCell:cell];
    //NSLog(@"indexPath.row=%d",indexPath.row);
    if([self.delegate respondsToSelector:@selector(selectedReminderForEdit:)])
        [self.delegate selectedReminderForEdit:indexPath.row];
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ;
} 

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
