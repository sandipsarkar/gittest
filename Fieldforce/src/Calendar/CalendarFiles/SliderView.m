//
//  SliderView.m
//  OEM_CALENDAR
//
//  Created by elie maalouly on 4/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SliderView.h"

@implementation SliderView
@synthesize lblJan,lblFeb,lblMarch,lblApril,lblMay,lblJune,lblJuly,lblAug,lblSep,lblOct,lblNov,lblDec;
@synthesize strCurrentYear,strPrevYear,strNextYear,strLastSelectedText;
@synthesize delegate = _delegate;
@synthesize month;
@synthesize prevYear,nextYear;;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor=[UIColor clearColor];
        
        offset=16.5;
        arrMonth=[[NSArray alloc] initWithObjects:@"Jan",@"Feb", @"Mar",@"Apr",@"May",@"Jun",@"Jul",@"Aug",@"Sep",@"Oct",@"Nov",@"Dec",nil];
        
        
        NSDateFormatter *dateFormatter = [NSDate formatter];
        [dateFormatter setDateFormat:@"MMM"];
        NSLocale *enLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"UTC"];//[NSLocale currentLocale];
        [dateFormatter setLocale:enLocale];
        [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
        currentMonth=[dateFormatter stringFromDate:[NSDate date]];
        //NSLog(@"currentMonth------%@",currentMonth);
        
        
        [dateFormatter setDateFormat:@"yyyy"];
        [dateFormatter setLocale:enLocale];
        [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
        self.strCurrentYear=[dateFormatter stringFromDate:[NSDate date]];
       // NSLog(@"strCurrentYear------%@",strCurrentYear);
        [enLocale release];
      
        
        currentMonthIndex=[arrMonth indexOfObject:currentMonth];
        prevMonthIndex=currentMonthIndex-1;
        nextMonthIndex=currentMonthIndex+1;
        
        float start_x=self.bounds.origin.x+35.0;
        float start_y=self.bounds.origin.y+10.0;
        float _width=60;
        float _height=25.0;
        
        
        int _prevYear=[strCurrentYear intValue]-1;
        self.strPrevYear=[NSString stringWithFormat:@"%d",_prevYear];
        btnPrevYear=[UIButton buttonWithType:UIButtonTypeCustom];
        btnPrevYear.frame =CGRectMake(start_x, start_y, _width, _height);
        btnPrevYear.titleLabel.font  = [UIFont grotesqueFontOfSize:12.0];
        [btnPrevYear setTitle:strPrevYear forState:UIControlStateNormal];
        [btnPrevYear setTitleColor:LOGIN_BG_COLOR forState:UIControlStateNormal];
        btnPrevYear.backgroundColor=[UIColor clearColor];
        [btnPrevYear addTarget:self action:@selector(btnPrevYearClicked) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btnPrevYear];

        
        start_x=start_x+btnPrevYear.bounds.size.width;
        lblJan=[self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor blackColor] fontSize:15.0 bold:NO];
        lblJan.frame=CGRectMake(start_x, start_y, _width, _height);
        lblJan.backgroundColor=[UIColor clearColor];
        lblJan.text=@"Jan";
        
        [self addSubview:lblJan];
        
        start_x=start_x+lblJan.bounds.size.width;
        lblFeb=[self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor blackColor] fontSize:15.0 bold:NO];
        lblFeb.frame=CGRectMake(start_x, start_y, _width, _height);
        lblFeb.backgroundColor=[UIColor clearColor];
        lblFeb.text=@"Feb";
        [self addSubview:lblFeb];
        
        start_x=start_x+lblFeb.bounds.size.width;
        lblMarch=[self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor blackColor] fontSize:15.0 bold:NO];
        lblMarch.frame=CGRectMake(start_x, start_y, _width, _height);
        lblMarch.backgroundColor=[UIColor clearColor];
        lblMarch.text=@"Mar";
        [self addSubview:lblMarch];
        
        start_x=start_x+lblMarch.bounds.size.width;
        lblApril=[self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor blackColor] fontSize:15.0 bold:NO];
        lblApril.frame=CGRectMake(start_x, start_y, _width, _height);
        lblApril.backgroundColor=[UIColor clearColor];
        lblApril.text=@"Apr";
        [self addSubview:lblApril];
        
        start_x=start_x+lblApril.bounds.size.width;
        lblMay=[self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor blackColor] fontSize:15.0 bold:NO];
        lblMay.frame=CGRectMake(start_x, start_y, _width, _height);
        lblMay.backgroundColor=[UIColor clearColor];
        lblMay.text=@"May";
        [self addSubview:lblMay];
        
        start_x=start_x+lblMay.bounds.size.width;
        lblJune=[self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor blackColor] fontSize:15.0 bold:NO];
        lblJune.frame=CGRectMake(start_x, start_y, _width, _height);
        lblJune.backgroundColor=[UIColor clearColor];
        lblJune.text=@"Jun";
        [self addSubview:lblJune];
        
        start_x=start_x+lblJune.bounds.size.width;
        lblJuly=[self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor blackColor] fontSize:15.0 bold:NO];
        lblJuly.frame=CGRectMake(start_x, start_y, _width, _height);
        lblJuly.backgroundColor=[UIColor clearColor];
        lblJuly.text=@"Jul";
        [self addSubview:lblJuly];
        
        start_x=start_x+lblJuly.bounds.size.width;
        lblAug=[self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor blackColor] fontSize:15.0 bold:NO];
        lblAug.frame=CGRectMake(start_x, start_y, _width, _height);
        lblAug.backgroundColor=[UIColor clearColor];
        lblAug.text=@"Aug";
        [self addSubview:lblAug];
        
        start_x=start_x+lblAug.bounds.size.width;
        lblSep=[self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor blackColor] fontSize:15.0 bold:NO];
        lblSep.frame=CGRectMake(start_x, start_y, _width, _height);
        lblSep.backgroundColor=[UIColor clearColor];
        lblSep.text=@"Sep";
        [self addSubview:lblSep];
        
        start_x=start_x+lblSep.bounds.size.width;
        lblOct=[self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor blackColor] fontSize:15.0 bold:NO];
        lblOct.frame=CGRectMake(start_x, start_y, _width, _height);
        lblOct.text=@"Oct";
        lblOct.backgroundColor=[UIColor clearColor];
        [self addSubview:lblOct];
        
        start_x=start_x+lblOct.bounds.size.width;
        lblNov=[self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor blackColor] fontSize:15.0 bold:NO];
        lblNov.frame=CGRectMake(start_x, start_y, _width, _height);
        lblNov.text=@"Nov";
        lblNov.backgroundColor=[UIColor clearColor];
        [self addSubview:lblNov];
        
        start_x=start_x+lblNov.bounds.size.width;
        lblDec=[self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor blackColor] fontSize:15.0 bold:NO];
        lblDec.frame=CGRectMake(start_x, start_y, _width, _height);
        lblDec.backgroundColor=[UIColor clearColor];
        lblDec.text=@"Dec";
        [self addSubview:lblDec];
        
        start_x=start_x+lblDec.bounds.size.width;
        
        int _nextYear=[strCurrentYear intValue]+1;
        self.strNextYear=[NSString stringWithFormat:@"%d",_nextYear];
        btnNextYear=[UIButton buttonWithType:UIButtonTypeCustom];
        btnNextYear.frame =CGRectMake(start_x, start_y, _width, _height);
        btnNextYear.titleLabel.font  = [UIFont grotesqueFontOfSize:12.0];
        [btnNextYear setTitle:strNextYear forState:UIControlStateNormal];
        [btnNextYear setTitleColor:LOGIN_BG_COLOR forState:UIControlStateNormal];
        btnNextYear.backgroundColor=[UIColor clearColor];
        [btnNextYear addTarget:self action:@selector(btnNextYearClicked) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btnNextYear];
        
        
        UIImage *bgImage = [UIImage imageNamed:@"slider_base.png"];
        UIImageView *imgv=[[UIImageView alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.bounds)-bgImage.size.width)/2.0, 0, bgImage.size.width, bgImage.size.height)];
        //CGRectMake(0, 0, self.bounds.size.width, 45);
        imgv.image=bgImage;
        [self insertSubview:imgv atIndex:0];
        [imgv release];
        
        dictRect=[[NSMutableDictionary alloc] init];
        [dictRect setValue:[NSValue valueWithCGRect:lblJan.frame] forKey:lblJan.text];
        [dictRect setValue:[NSValue valueWithCGRect:lblFeb.frame] forKey:lblFeb.text];
        [dictRect setValue:[NSValue valueWithCGRect:lblMarch.frame] forKey:lblMarch.text];
        [dictRect setValue:[NSValue valueWithCGRect:lblApril.frame] forKey:lblApril.text];
        [dictRect setValue:[NSValue valueWithCGRect:lblMay.frame] forKey:lblMay.text];
        [dictRect setValue:[NSValue valueWithCGRect:lblJune.frame] forKey:lblJune.text];
        [dictRect setValue:[NSValue valueWithCGRect:lblJuly.frame] forKey:lblJuly.text];
        [dictRect setValue:[NSValue valueWithCGRect:lblAug.frame] forKey:lblAug.text];
        [dictRect setValue:[NSValue valueWithCGRect:lblSep.frame] forKey:lblSep.text];
        [dictRect setValue:[NSValue valueWithCGRect:lblOct.frame] forKey:lblOct.text];
        [dictRect setValue:[NSValue valueWithCGRect:lblNov.frame] forKey:lblNov.text];
        [dictRect setValue:[NSValue valueWithCGRect:lblDec.frame] forKey:lblDec.text];
        
        NSString *key=[arrMonth objectAtIndex:currentMonthIndex];
        CGRect btnRect=[[dictRect valueForKey:key] CGRectValue];
        btnSelectedMonth=[UIButton buttonWithType:UIButtonTypeCustom];
        btnSelectedMonth.frame =CGRectMake(btnRect.origin.x+offset, btnRect.origin.y+2.0, btnRect.size.width-offset*2, btnRect.size.height-4.0);
        [btnSelectedMonth setTitle:[arrMonth objectAtIndex:currentMonthIndex] forState:UIControlStateNormal];
        btnSelectedMonth.titleLabel.font  = [UIFont systemFontOfSize:12.0];
        [btnSelectedMonth setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnSelectedMonth.userInteractionEnabled=NO;
        [btnSelectedMonth setBackgroundImage:[UIImage imageNamed:@"month_blue_box.png"] forState:UIControlStateNormal];
        [btnSelectedMonth addTarget:self action:@selector(highlightedButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btnSelectedMonth];
        
        //NSLog(@"%f %f %f %f ",btnRect.origin.x,btnRect.origin.y,btnRect.size.width,btnRect.size.height);
        //NSLog(@"%f %f %f %f ",btnSelectedMonth.frame.origin.x,btnSelectedMonth.frame.origin.y,btnSelectedMonth.frame.size.width,btnSelectedMonth.frame.size.height);
        //NSLog(@"dictRect=%@",dictRect);
        
        UIPanGestureRecognizer *panGesture=[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panAction:)];
       // [panGesture setMaximumNumberOfTouches:1];
        [self addGestureRecognizer:panGesture];
        [panGesture release];
        
        
        
    }
    return self;
}

-(void)slideToSelectedFrame:(CGRect)targetRect
{
    [UIView animateWithDuration:0.2 animations:^{
        
        btnSelectedMonth.frame =CGRectMake(targetRect.origin.x+offset, targetRect.origin.y+2.0, targetRect.size.width-offset*2, targetRect.size.height-4.0);
    }];
}


-(void)setMonth:(int)_month 
{
    if(month != _month)
    {
        month = _month;
        
        self.strLastSelectedText=[arrMonth objectAtIndex:--_month];
         NSLog(@"self.strLastSelectedText=%@",self.strLastSelectedText);
        CGRect btnRect=[[dictRect valueForKey:self.strLastSelectedText] CGRectValue];
        //btnSelectedMonth.frame =CGRectMake(btnRect.origin.x+offset, btnRect.origin.y, btnRect.size.width-offset*2, btnRect.size.height);
        [self slideToSelectedFrame:btnRect];
        [btnSelectedMonth setTitle:self.strLastSelectedText forState:UIControlStateNormal];
    }
}

-(void)setPrevYear:(BOOL)_year
{
    prevYear = _year;
    
    self.strNextYear=strCurrentYear;
    self.strCurrentYear=strPrevYear;
    //NSLog(@"strNextYear=%@",strNextYear);
    //NSLog(@"strCurrentYear=%@",strCurrentYear);
    
    int temp=[strPrevYear intValue];
    temp=temp-1;
    self.strPrevYear=[NSString stringWithFormat:@"%d",temp];
    //NSLog(@"strPrevYear=%@",strPrevYear);
    [btnPrevYear setTitle:strPrevYear forState:UIControlStateNormal];
    [btnNextYear setTitle:strNextYear forState:UIControlStateNormal];
}


-(void)setNextYear:(BOOL)_year
{
    nextYear = _year;
    
    self.strPrevYear=strCurrentYear;
    self.strCurrentYear=strNextYear;
    //NSLog(@"strPrevYear=%@",strPrevYear);
    //NSLog(@"strCurrentYear=%@",strCurrentYear);
    
    int temp=[strNextYear intValue];
    temp=temp+1;
    self.strNextYear=[NSString stringWithFormat:@"%d",temp];
    //NSLog(@"strNextYear=%@",strNextYear);
    [btnPrevYear setTitle:strPrevYear forState:UIControlStateNormal];
    [btnNextYear setTitle:strNextYear forState:UIControlStateNormal];
}


- (UILabel *)newLabelWithPrimaryColor:(UIColor *)primaryColor selectedColor:(UIColor *)selectedColor fontSize:(CGFloat)fontSize bold:(BOOL)bold
{
	/*
	 Create and configure a label.
	 */
	
	UIFont *font;
	if (bold)
	{
		font = [UIFont boldSystemFontOfSize:fontSize];
	} 
	else
	{
		font = [UIFont grotesqueFontOfSize:12.0];
	}
    
    /*
	 Views are drawn most efficiently when they are opaque and do not have a clear background, so set these defaults.  To show selection properly, however, the views need to be transparent (so that the selection color shows through).  This is handled in setSelected:animated:.
	 */
	UILabel *newLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	newLabel.backgroundColor = [UIColor clearColor];
	newLabel.opaque = YES;
	newLabel.textColor = primaryColor;
	newLabel.highlightedTextColor = selectedColor;
	newLabel.font = font;
    newLabel.textAlignment = NSTextAlignmentCenter;
    newLabel.autoresizingMask=(UIViewAutoresizingFlexibleLeftMargin |
                               UIViewAutoresizingFlexibleRightMargin |
                               UIViewAutoresizingFlexibleWidth |
                               UIViewAutoresizingFlexibleTopMargin |
                               UIViewAutoresizingFlexibleHeight |
                               UIViewAutoresizingFlexibleBottomMargin);
	newLabel.lineBreakMode=UILineBreakModeTailTruncation;
    newLabel.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    tapGesture.numberOfTapsRequired = 1;
    [newLabel addGestureRecognizer:tapGesture];
    [tapGesture release];
    
    
	return newLabel;
}

-(void)panAction:(UIPanGestureRecognizer *)panGesture
{
    
    CGPoint _touchPoint = [panGesture locationInView:panGesture.view];

    
    for(NSString *key in dictRect)
    {
        NSValue *rectVal = [dictRect valueForKey:key];
        
        CGRect _rect = [rectVal CGRectValue];
        
        if(CGRectContainsPoint(_rect, _touchPoint))
        {
           // btnSelectedMonth.frame =CGRectMake(_rect.origin.x+offset, _rect.origin.y, _rect.size.width-offset*2, _rect.size.height);
            
            [self slideToSelectedFrame:_rect];
            
            [btnSelectedMonth setTitle:key forState:UIControlStateNormal];
            
        
            NSLog(@"touched:%@ state=%d",key,panGesture.state);
            
            
            if(panGesture.state == UIGestureRecognizerStateEnded)
            {
                self.strLastSelectedText=key;
                int _monthIndex=[arrMonth indexOfObject:key];
                _monthIndex+=1;
                
                if([self.delegate respondsToSelector:@selector(selectedMonth:)])
                    [self.delegate selectedMonth:_monthIndex];
            }
            break;
        }
    }
    
}


-(void)tapAction:(UITapGestureRecognizer *)tapGesture
{
   // UITouch *touch = [touches anyObject];//when a touch event occurs it creates a touch object
	CGPoint touchLocal = [tapGesture locationInView:self];//it will return the touch location in view
    
    
        UILabel *touchedLabel=(UILabel *)tapGesture.view;
        
        if(CGRectContainsPoint(touchedLabel.frame, touchLocal))
        {
            
            if(![strLastSelectedText isEqualToString:touchedLabel.text])
            {
                //NSLog(@"Highlight:%@",touchedLabel.text);
                int _monthIndex=[arrMonth indexOfObject:touchedLabel.text];
                _monthIndex+=1;
                //NSLog(@"_month index:%d",_monthIndex);
                self.strLastSelectedText=touchedLabel.text;
                CGRect btnRect=[[dictRect valueForKey:touchedLabel.text] CGRectValue];
                
                //btnSelectedMonth.frame =CGRectMake(btnRect.origin.x+offset, btnRect.origin.y, btnRect.size.width-offset*2, btnRect.size.height);
                
                 [self slideToSelectedFrame:btnRect];
                [btnSelectedMonth setTitle:touchedLabel.text forState:UIControlStateNormal];
                
                if([self.delegate respondsToSelector:@selector(selectedMonth:)])
                    [self.delegate selectedMonth:_monthIndex];
            }
        }
    
    }

//=============================== UNDER CONSTRUCTION ================================
-(void)selectToday
{
    [self selectDate:[NSDate date]];
}

-(void)selectDate:(NSDate *)date
{
    if(!date) return;
    
    NSString *_month = [date stringFromDateWithFormat:@"MMM"];
    _month = [_month capitalizedString];
     NSString *_year = [date stringFromDateWithFormat:@"yyyy"];
    
    NSInteger _monthIndex = [arrMonth indexOfObject:_month];
    NSInteger _yearIndex  = [_year intValue];
   
    
    if(_monthIndex !=NSNotFound)
    {
        self.strNextYear=[NSString stringWithFormat:@"%d",_yearIndex+1];
        self.strCurrentYear=_year;
        self.strPrevYear = [NSString stringWithFormat:@"%d",_yearIndex-1];
        [btnPrevYear setTitle:strPrevYear forState:UIControlStateNormal];
        [btnNextYear setTitle:strNextYear forState:UIControlStateNormal];
        
        self.strLastSelectedText=_month;
        CGRect btnRect=[[dictRect valueForKey:_month] CGRectValue];
        
        [self slideToSelectedFrame:btnRect];
        [btnSelectedMonth setTitle:_month forState:UIControlStateNormal];
        
        if([self.delegate respondsToSelector:@selector(selectedMonth:year:)])
            [self.delegate selectedMonth:_monthIndex+1 year:_yearIndex];
    }

}
//=============================== UNDER CONSTRUCTION ==================================

-(void)_setJanFirstMonth
{
    /// ======= SET JAN AS FIRST MONTH =========
    NSString *key = @"Jan";
    CGRect btnRect=[[dictRect valueForKey:key] CGRectValue];
    [self slideToSelectedFrame:btnRect];
    [btnSelectedMonth setTitle:key forState:UIControlStateNormal];

}

-(void)btnPrevYearClicked
{
    self.strNextYear=strCurrentYear;
    self.strCurrentYear=strPrevYear;
    
    //NSLog(@"strNextYear=%@",strNextYear);
    //NSLog(@"strCurrentYear=%@",strCurrentYear);
    
    int temp=[strPrevYear intValue];
    if([self.delegate respondsToSelector:@selector(selectedYear:)])
        [self.delegate selectedYear:temp];
    
    temp=temp-1;
    self.strPrevYear=[NSString stringWithFormat:@"%d",temp];
     //NSLog(@"strPrevYear=%@",strPrevYear);
    [btnPrevYear setTitle:strPrevYear forState:UIControlStateNormal];
    [btnNextYear setTitle:strNextYear forState:UIControlStateNormal];
    
    [self _setJanFirstMonth];
}

-(void)btnNextYearClicked
{
    self.strPrevYear=strCurrentYear;
    self.strCurrentYear=strNextYear;
    
    //NSLog(@"strPrevYear=%@",strPrevYear);
    //NSLog(@"strCurrentYear=%@",strCurrentYear);
    
    int temp=[strNextYear intValue];
    if([self.delegate respondsToSelector:@selector(selectedYear:)])
        [self.delegate selectedYear:temp];
    
    temp=temp+1;
    self.strNextYear=[NSString stringWithFormat:@"%d",temp];
    //NSLog(@"strNextYear=%@",strNextYear);
    [btnPrevYear setTitle:strPrevYear forState:UIControlStateNormal];
    [btnNextYear setTitle:strNextYear forState:UIControlStateNormal];
    
    
    [self _setJanFirstMonth];
}

-(void)highlightedButton:(id)sender
{
    ;
}

/*
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
//    UITouch *touch = [touches anyObject];//when a touch event occurs it creates a touch object
//	CGPoint touchLocal = [touch locationInView:self];//it will return the touch location in view
//    
//    if([touch.view isKindOfClass:[UILabel class]])
//    {
//        UILabel *touchedLabel=(UILabel *)touch.view;
//        
//        if(CGRectContainsPoint(touchedLabel.frame, touchLocal))
//        {
//            
//            if(![strLastSelectedText isEqualToString:touchedLabel.text])
//            {
//                NSLog(@"Highlight:%@",touchedLabel.text);
//                self.strLastSelectedText=touchedLabel.text;
//                CGRect btnRect=[[dictRect valueForKey:touchedLabel.text] CGRectValue];
//                btnSelectedMonth.frame =CGRectMake(btnRect.origin.x+offset, btnRect.origin.y, btnRect.size.width-offset*2, btnRect.size.height);
//                [btnSelectedMonth setTitle:touchedLabel.text forState:UIControlStateNormal];
//            }
//        }
//    }
}


-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touch = [touches anyObject];//when a touch event occurs it creates a touch object
    CGPoint touchLocal = [touch locationInView:self];//it will return the touch location in view
    
    NSLog(@"%f %f",touchLocal.x,touchLocal.y);
    
    
    if([touch.view isKindOfClass:[UILabel class]])
    {
        UILabel *touchedLabel=(UILabel *)touch.view;

        //if(CGRectContainsPoint(touchedLabel.frame, touchLocal))
        {
            NSLog(@"Highlight:%@",touchedLabel.text);            
        }
        touchedLabel = nil;
    }
    
}
 */

/*
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];//when a touch event occurs it creates a touch object
	CGPoint touchLocal = [touch locationInView:self];//it will return the touch location in view
    
    if([touch.view isKindOfClass:[UILabel class]])
    {
        UILabel *touchedLabel=(UILabel *)touch.view;
        
        if(CGRectContainsPoint(touchedLabel.frame, touchLocal))
        {
            
            if(![strLastSelectedText isEqualToString:touchedLabel.text])
            {
                //NSLog(@"Highlight:%@",touchedLabel.text);
                int _monthIndex=[arrMonth indexOfObject:touchedLabel.text];
                _monthIndex+=1;
               //NSLog(@"_month index:%d",_monthIndex);
                self.strLastSelectedText=touchedLabel.text;
                 CGRect btnRect=[[dictRect valueForKey:touchedLabel.text] CGRectValue];
                 btnSelectedMonth.frame =CGRectMake(btnRect.origin.x+offset, btnRect.origin.y, btnRect.size.width-offset*2, btnRect.size.height);
                 [btnSelectedMonth setTitle:touchedLabel.text forState:UIControlStateNormal];
                
                if([self.delegate respondsToSelector:@selector(selectedMonth:)])
                    [self.delegate selectedMonth:_monthIndex];
            }
        }
    }
}
*/

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) dealloc 
{
     [lblJan release],lblJan=nil;
     [lblFeb release],lblFeb=nil;
     [lblMarch release],lblMarch=nil;
     [lblApril release],lblApril=nil;
     [lblMay release],lblMay=nil;
     [lblJune release],lblJune=nil;
     [lblJuly release],lblJuly=nil;
     [lblAug release],lblAug=nil;
     [lblSep release],lblSep=nil;
     [lblOct release],lblOct=nil;
     [lblNov release],lblNov=nil;
     [lblDec release],lblDec=nil;
     [strCurrentYear release],strCurrentYear=nil;
     [strPrevYear release],strPrevYear=nil;
     [strNextYear release],strNextYear=nil;
     [strLastSelectedText release],strLastSelectedText=nil;
     [dictRect release];
     [arrMonth release];
    
     [super dealloc];
}

@end
