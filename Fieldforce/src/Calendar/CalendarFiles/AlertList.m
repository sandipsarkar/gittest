//
//  AlertList.m
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AlertList.h"
#import "InsetTableViewCell.h"

@implementation AlertList
@synthesize delegate = _delegate;
@synthesize strAlert;
- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    [strAlert release],strAlert=nil;
    [super dealloc];
}

#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    #ifndef __IPHONE_7_0
    self.contentSizeForViewInPopover=CGSizeMake(335.0+15.0, 415);
    #endif
    
    //self.contentSizeForViewInPopover = self.tableView.contentSize;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //self.contentSizeForViewInPopover=CGSizeMake(335.0+15.0, 415);
    
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    
    self.preferredContentSize=self.tableView.contentSize;
else
    self.contentSizeForViewInPopover=self.tableView.contentSize;


}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    [self addTitle:@"REMINDER ALERT" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    TableCell=nil;
    [self.navigationItem setHidesBackButton:YES animated:YES];
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    self.tableView.backgroundColor = REMINDER_BG_COLOR;
else
    self.tableView.backgroundColor = [UIColor whiteColor];

    
    self.tableView.bounces = NO;
    
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCancel setImage:[UIImage imageNamed:@"cancel_button.png"] forState:UIControlStateNormal];
    btnCancel.frame =CGRectMake(0, 0, 60, 29);
    [btnCancel addTarget:self 
                  action:@selector(cancelPopOver:) 
        forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc]initWithCustomView:btnCancel];
	self.navigationItem.leftBarButtonItem = barButtonCancel;
	[barButtonCancel release];
    
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
    UIButton *btnDone = [UIButton repToolButtonWithImageName:NAV_DONE_BUTTON text:@"Done"];
    btnDone.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
    [btnDone addTarget:self
                action:@selector(saveChanges:)
      forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithCustomView:btnDone];
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
}
else
{
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(saveChanges:)];
	self.navigationItem.rightBarButtonItem = barButtonDone;
	[barButtonDone release];
}
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        //self.tableView.sectionFooterHeight = 5.0;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 10.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        self.tableView.tableHeaderView = _headerView;
        [_headerView release];
        
        UIView *_footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 10.0)];
        _footerView.backgroundColor = [UIColor clearColor];
        self.tableView.tableFooterView = _footerView;
        [_footerView release];

    }


}

-(void)cancelPopOver:(id)sender
{
    //UIButton *btn=(UIButton *)sender;
    if([self.delegate respondsToSelector:@selector(selectedAlertTime:)])
        [self.delegate selectedAlertTime:nil];
    TableCell=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)saveChanges:(id)sender
{
    //UIButton *btn=(UIButton *)sender;
    NSString *str = TableCell.textLabel.text;
    if([self.delegate respondsToSelector:@selector(selectedAlertTime:)])
        [self.delegate selectedAlertTime:str];
    TableCell=nil;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
        return 9;
}



- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];

    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell =(UITableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.accessoryType=UITableViewCellAccessoryNone;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
    }
    
    switch (indexPath.row)
    {
        case 0:
        {
            cell.textLabel.text=@"None";
        }
            break;
            
        case 1:
        {
            cell.textLabel.text=@"At time of event";
        }
            break;
            
        case 2:
        {
            cell.textLabel.text=@"5 minutes before";
        }
            break;
            
        case 3:
        {
            cell.textLabel.text=@"15 minutes before";
        }
            break;
        case 4:
        {
            cell.textLabel.text=@"30 minutes before";
        }
            break;
        case 5:
        {
            cell.textLabel.text=@"1 hour before";
        }
            break;
        case 6:
        {
            cell.textLabel.text=@"2 hours before";
        }
            break;
        case 7:
        {
            cell.textLabel.text=@"1 day before";
        }
            break;
        case 8:
        {
            cell.textLabel.text=@"2 days before";
        }
            break;
        default:
            break;
    }
    
    if([cell.textLabel.text isEqualToString:self.strAlert])
    {
        //UIImageView *viewSelected = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark.png"]];
       // cell.accessoryView =viewSelected;
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        TableCell=cell;
        //[viewSelected release];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.tag = 1;
    
    
    NSInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(TableCell!=nil)
    {
       // TableCell.accessoryView =nil;
         TableCell.accessoryType = UITableViewCellAccessoryNone;
    }
    //UIImageView *viewSelected = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark.png"]];
    UITableViewCell *currentCell = [tableView cellForRowAtIndexPath:indexPath];
   // TableCell.accessoryView =viewSelected;
    currentCell.accessoryType = UITableViewCellAccessoryCheckmark;
    TableCell=currentCell;
   // [viewSelected release];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
