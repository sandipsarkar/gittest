//
//  CXCalendarView.m
//  Calendar
//
//  Created by Vladimir Grichina on 13.07.11.
//  Copyright 2011 Componentix. All rights reserved.
//

#import "CXCalendarView.h"

#import "CXCalendarCellView.h"

#import "NSDateAdditions.h"

#import "KalDate.h"
#import "Task.h"
#import "UIViewController+RepToolHeader.h"
#import "FilterCalenderViewController.h"
#import "CalenderSearchResultsController.h"
#import "ScheduledVisit.h"
#import "Customer.h"
#import "Reminder.h"

#import <QuartzCore/QuartzCore.h>


static const CGFloat kGridMargin = 10.0;//4

@interface CXCalendarView()<UIPopoverControllerDelegate,UITextFieldDelegate>
{
    BOOL _isProcessing;
  
}


-(void)calendarDidLoad;

- (NSArray *)searchVisitArrayUsingPredicateForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate;

@property(nonatomic,retain)CXCalendarCellView *todayCell;

@end

@implementation CXCalendarView


@synthesize delegate;
@synthesize sliderView;
@synthesize baseDate;
@synthesize arrPrevMonth,arrNextMonth;
@synthesize arrTask;
@synthesize arrVisit;
@synthesize arrayReturnedAfterPredicate;
@synthesize calDeligate = _calDeligate;
@synthesize selectedTabIndex;
@synthesize currentStartDate;
@synthesize currentEndDate;
@synthesize todayCell;


@synthesize calendarCellPopoverController;


static const CGFloat kDefaultMonthBarButtonWidth = 44;

- (id) initWithFrame: (CGRect) frame selectedTabIndex:(NSInteger)anIndex
{
    if ((self = [super initWithFrame: frame])) 
    {
        
        
        selectedTabIndex=anIndex;
        
        self.selectedDate = nil;
        self.displayedDate = [NSDate date];
        
        self.baseDate = [self.displayedDate cc_dateByMovingToFirstDayOfTheMonth];
        
        self.arrPrevMonth=[self calculateDaysInFinalWeekOfPreviousMonth];
        self.arrNextMonth=[self calculateDaysInFirstWeekOfFollowingMonth];
        
        _monthBarHeight = 45;//48;
        _weekBarHeight = 45;//32;
        
        _isProcessing = NO;
    }
    return self;
}

-(void)setSelectedTabIndex:(NSInteger)aSelectedTabIndex
{
    if(aSelectedTabIndex!=selectedTabIndex)
    {
        selectedTabIndex = aSelectedTabIndex;
        
        [self setNeedsLayout];
    }
}


- (NSMutableArray *)calculateDaysInFinalWeekOfPreviousMonth
{
    NSMutableArray *days = [NSMutableArray array];
    
    //NSLog(@"self.baseDate =%@",self.baseDate );
    NSDate *beginningOfPreviousMonth = [self.baseDate cc_dateByMovingToFirstDayOfThePreviousMonth];
    int n = [beginningOfPreviousMonth cc_numberOfDaysInMonth];
    int numPartialDays = [self numberOfDaysInPreviousPartialWeek];
    NSDateComponents *c = [beginningOfPreviousMonth cc_componentsForMonthDayAndYear];
    for (int i = n - (numPartialDays - 1); i < n + 1; i++)
        [days addObject:[KalDate dateForDay:i month:c.month year:c.year]];
    
    //NSLog(@"days=%@",days);
    return days;
}

- (NSMutableArray *)calculateDaysInFirstWeekOfFollowingMonth
{
    NSMutableArray *days = [NSMutableArray array];
    
    //NSLog(@"self.baseDate =%@",self.baseDate );
    NSDateComponents *c = [[self.baseDate cc_dateByMovingToFirstDayOfTheFollowingMonth] cc_componentsForMonthDayAndYear];
    NSUInteger numPartialDays = [self numberOfDaysInFollowingPartialWeek];
    
    for (int i = 1; i < numPartialDays + 1; i++)
        [days addObject:[KalDate dateForDay:i month:c.month year:c.year]];
    
    //NSLog(@"days=%@",days);
    return days;
}

- (NSUInteger)numberOfDaysInPreviousPartialWeek
{
    return [self.baseDate cc_weekday] - 1;
}


- (NSUInteger)numberOfDaysInFollowingPartialWeek
{
    NSDateComponents *c = [self.baseDate cc_componentsForMonthDayAndYear];
    c.day = [self.baseDate cc_numberOfDaysInMonth];
    NSDate *lastDayOfTheMonth = [[NSCalendar currentCalendar] dateFromComponents:c];
    return 7 - [lastDayOfTheMonth cc_weekday];
}


#pragma mark DEALLOC
- (void) dealloc
{
    [calendarCellPopoverController  release];
     calendarCellPopoverController = nil;
    
    
    //[_weekdayBar release],
    [_dayCells release];
    [_calendar release];
    [_selectedDate release];
    [_displayedDate release];
    [arrPrevMonth release];
    [arrNextMonth release];
    [baseDate release];
    [arrBtnHold release];
    [sliderView release];
    [arrTask release];
    [arrVisit release];
    [arrayReturnedAfterPredicate release];
    [currentEndDate release];
    [currentStartDate release];
    
    [todayCell release];
    
   

    [super dealloc];
}
#pragma mark -

- (NSCalendar *) calendar 
{
    if (!_calendar) 
    {
        _calendar = [[NSCalendar currentCalendar] retain];
    }
    return _calendar;
}

- (void) setCalendar: (NSCalendar *) calendar 
{
    if (_calendar != calendar) 
    {
        [_calendar release];
        _calendar = [calendar retain];
        [self setNeedsLayout];
    }
}

- (NSDate *) selectedDate 
{
    return _selectedDate;
}

- (void) setSelectedDate: (NSDate *) selectedDate
{
    if (![selectedDate isEqual: _selectedDate]) 
    {
        [_selectedDate release];
        _selectedDate = [selectedDate retain];

        /*
        for (CXCalendarCellView *cellView in self.dayCells) 
        {
            cellView.selected = NO;
        }
         */

        //[[self cellForDate: selectedDate] setSelected: YES];

        if ([self.delegate respondsToSelector:@selector(calendarView:didSelectDate:)]) 
        {
            [self.delegate calendarView: self didSelectDate: _selectedDate];
        }
    }
}

- (NSDate *) displayedDate 
{
    return _displayedDate;
}

- (void) setDisplayedDate: (NSDate *) displayedDate 
{
    if (_displayedDate != displayedDate) 
    {
        [_displayedDate release];
        _displayedDate = [displayedDate retain];

        NSString *monthName = [[[[NSDateFormatter new] autorelease] standaloneMonthSymbols] objectAtIndex: self.displayedMonth - 1];
        self.monthLabel.text = [NSString stringWithFormat: @"%@ %d", NSLocalizedString(monthName, @""), self.displayedYear];

        [self setNeedsLayout];
    }
}

- (NSUInteger)displayedYear 
{
    NSDateComponents *components = [self.calendar components: NSYearCalendarUnit
                                                    fromDate: self.displayedDate];
    return components.year;
}

- (NSUInteger)displayedMonth 
{
    NSDateComponents *components = [self.calendar components: NSMonthCalendarUnit
                                                    fromDate: self.displayedDate];
    return components.month;
}

- (CGFloat) monthBarHeight 
{
    return _monthBarHeight;
}

- (void) setMonthBarHeight: (CGFloat) monthBarHeight 
{
    if (_monthBarHeight != monthBarHeight) 
    {
        _monthBarHeight = monthBarHeight;
        [self setNeedsLayout];
    }
}

- (CGFloat) weekBarHeight 
{
    return _weekBarHeight;
}

- (void) setWeekBarHeight: (CGFloat) weekBarHeight 
{
    if (_weekBarHeight != weekBarHeight) 
    {
        _weekBarHeight = weekBarHeight;
        [self setNeedsLayout];
    }
}

- (void) touchedCellView: (CXCalendarCellView *) cellView 
{
    self.selectedDate = [cellView dateWithBaseDate: self.displayedDate withCalendar: self.calendar];
     //NSLog(@"Selected date: %@", self.selectedDate);
}

- (void) monthForward 
{
    /*
    NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
    monthStep.month = 1;
    
    NSDateFormatter *df = [NSDate formatter];
    [df setDateFormat:@"MM"];
    
    
    NSString *prevMonthString = [NSString stringWithFormat:@"%@",
                               [df stringFromDate:self.displayedDate]];
    int displayedIndex=[prevMonthString intValue];
    if(displayedIndex==12)
        self.sliderView.nextYear=YES;
    
    
    self.displayedDate = [self.calendar dateByAddingComponents: monthStep toDate: self.displayedDate options: 0];
    
    
    self.baseDate = [self.displayedDate cc_dateByMovingToFirstDayOfTheMonth];
    
    self.arrPrevMonth=[self calculateDaysInFinalWeekOfPreviousMonth];
    self.arrNextMonth=[self calculateDaysInFirstWeekOfFollowingMonth];
    
    NSString *currentMonthString = [NSString stringWithFormat:@"%@",
                               [df stringFromDate:self.displayedDate]];
    displayedIndex=[currentMonthString intValue];
    
    self.sliderView.month=displayedIndex;
    */
    
    NSDateComponents *_dateComponent = [self.calendar components:NSMonthCalendarUnit|NSYearCalendarUnit fromDate:self.displayedDate];
    
    int displayedIndex=_dateComponent.month;
    if(displayedIndex==12)
        self.sliderView.nextYear=YES;

    
    _dateComponent.month += 1;
    self.displayedDate = [self.calendar dateFromComponents:_dateComponent];

    self.baseDate = [self.displayedDate cc_dateByMovingToFirstDayOfTheMonth];
    
    self.arrPrevMonth=[self calculateDaysInFinalWeekOfPreviousMonth];
    self.arrNextMonth=[self calculateDaysInFirstWeekOfFollowingMonth];
    
    _dateComponent = [self.calendar components:NSMonthCalendarUnit|NSYearCalendarUnit fromDate:self.displayedDate];
    self.sliderView.month=[_dateComponent month];
}

- (void) monthBack 
{
    /*
    NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
    monthStep.month = -1;
    
    NSDateFormatter *df = [NSDate formatter];
    [df setDateFormat:@"MM"];
    
    NSString *prevMonthString = [NSString stringWithFormat:@"%@",
                                    [df stringFromDate:self.displayedDate]];
    int displayedIndex=[prevMonthString intValue];
    if(displayedIndex==1)
        self.sliderView.prevYear=YES;
    
    self.displayedDate = [self.calendar dateByAddingComponents:monthStep toDate: self.displayedDate options: 0];
     
     self.baseDate = [self.displayedDate cc_dateByMovingToFirstDayOfTheMonth];
     
     self.arrPrevMonth=[self calculateDaysInFinalWeekOfPreviousMonth];
     self.arrNextMonth=[self calculateDaysInFirstWeekOfFollowingMonth];
     
     NSString *currentMonthString = [NSString stringWithFormat:@"%@",[df stringFromDate:self.displayedDate]];
     displayedIndex=[currentMonthString intValue];
     self.sliderView.month=displayedIndex;

    */
    
    NSDateComponents *_dateComponent = [self.calendar components:NSMonthCalendarUnit|NSYearCalendarUnit fromDate:self.displayedDate];
  
    int displayedIndex=_dateComponent.month;
    if(displayedIndex==1)
        self.sliderView.prevYear=YES;
    
    _dateComponent.month -= 1;
    self.displayedDate = [self.calendar dateFromComponents:_dateComponent];

    self.baseDate = [self.displayedDate cc_dateByMovingToFirstDayOfTheMonth];
    
    self.arrPrevMonth=[self calculateDaysInFinalWeekOfPreviousMonth];
    self.arrNextMonth=[self calculateDaysInFirstWeekOfFollowingMonth];
    
   _dateComponent = [self.calendar components:NSMonthCalendarUnit|NSYearCalendarUnit fromDate:self.displayedDate];
    self.sliderView.month=[_dateComponent month];
}

- (void)selectedMonth:(int)month
{
    /*
    NSDateFormatter *df = [NSDate formatter];
    
    [df setDateFormat:@"MM"];
    
    NSString *myMonthString = [NSString stringWithFormat:@"%@",
                     [df stringFromDate:self.displayedDate]];
   // NSLog(@"myMonthString=%@",myMonthString);
    int displayedIndex=[myMonthString intValue];
    
    NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
    monthStep.month = month-displayedIndex;
    self.displayedDate = [self.calendar dateByAddingComponents: monthStep toDate: self.displayedDate options: 0];
    
    self.baseDate = [self.displayedDate cc_dateByMovingToFirstDayOfTheMonth];
    
    self.arrPrevMonth=[self calculateDaysInFinalWeekOfPreviousMonth];
    self.arrNextMonth=[self calculateDaysInFirstWeekOfFollowingMonth];
    */
    
    
    NSDateComponents *_dateComponent = [self.calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:self.displayedDate];
    _dateComponent.month = month;

     self.displayedDate = [self.calendar dateFromComponents:_dateComponent];
    
    self.baseDate = [self.displayedDate cc_dateByMovingToFirstDayOfTheMonth];
    
    self.arrPrevMonth=[self calculateDaysInFinalWeekOfPreviousMonth];
    self.arrNextMonth=[self calculateDaysInFirstWeekOfFollowingMonth];
    
    
    
}

- (void)_selectedYear:(int)year
{
    
    NSDateFormatter *df = [NSDate formatter];
    [df setDateFormat:@"YYY"];
    
    NSString *myYearString = [NSString stringWithFormat:@"%@",
                               [df stringFromDate:self.displayedDate]];
    int displayedIndex=[myYearString intValue];
    
    NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
    monthStep.month = (year-displayedIndex)*12;
    self.displayedDate = [self.calendar dateByAddingComponents: monthStep toDate: self.displayedDate options: 0];
    
    
    self.baseDate = [self.displayedDate cc_dateByMovingToFirstDayOfTheMonth];
    
    self.arrPrevMonth=[self calculateDaysInFinalWeekOfPreviousMonth];
    self.arrNextMonth=[self calculateDaysInFirstWeekOfFollowingMonth];
    
  
}

- (void)selectedYear:(int)year
{
    NSDateComponents *yearStep = [NSDateComponents new];
    yearStep.month = 1;
    yearStep.day = 1;
    yearStep.year = year;
    
    self.displayedDate = [self.calendar dateFromComponents:yearStep];
    
    self.baseDate = [self.displayedDate cc_dateByMovingToFirstDayOfTheMonth];
    
    self.arrPrevMonth=[self calculateDaysInFinalWeekOfPreviousMonth];
    self.arrNextMonth=[self calculateDaysInFirstWeekOfFollowingMonth];
    
    [yearStep release];
}


- (void)selectedMonth:(int)month year:(int)year
{
    
    NSString *myYearString = [self.displayedDate stringFromDateWithFormat:@"YYY"];
    int displayedYearIndex = [myYearString intValue];
    
    NSString *myMonthString  = [self.displayedDate stringFromDateWithFormat:@"MM"];
     int displayedMonthIndex = [myMonthString intValue];
    
    NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
    monthStep.month = (year-displayedYearIndex)*12 + month-displayedMonthIndex;
    self.displayedDate = [self.calendar dateByAddingComponents: monthStep toDate: self.displayedDate options: 0];
    
    self.baseDate = [self.displayedDate cc_dateByMovingToFirstDayOfTheMonth];
    
    //NSLog(@"self.baseDate=%@",self.baseDate);
    
    self.arrPrevMonth=[self calculateDaysInFinalWeekOfPreviousMonth];
    self.arrNextMonth=[self calculateDaysInFirstWeekOfFollowingMonth];
    
    
    /*
    NSDateComponents *_dateComponent = [self.calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[NSDate date]];
    self.displayedDate = [self.calendar dateFromComponents:_dateComponent];
    
    self.baseDate = [self.displayedDate cc_dateByMovingToFirstDayOfTheMonth];
    self.arrPrevMonth=[self calculateDaysInFinalWeekOfPreviousMonth];
    self.arrNextMonth=[self calculateDaysInFirstWeekOfFollowingMonth];
   */
    
}

- (void) reset 
{
    self.selectedDate = nil;
}

- (NSDate *) displayedMonthStartDate 
{
    NSDateComponents *components = [self.calendar components: NSMonthCalendarUnit|NSYearCalendarUnit
                                                    fromDate: self.displayedDate];
    components.day = 1;
    return [self.calendar dateFromComponents: components];
}

- (CXCalendarCellView *) cellForDate: (NSDate *) date 
{
    NSDateComponents *components = [self.calendar components: NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit
                                                        fromDate: date];
   // NSLog(@"self.displayedMonth=%d",self.displayedMonth);
    //NSLog(@"self.displayedYear=%d",self.displayedYear);
    if (components.month == self.displayedMonth &&
        components.year == self.displayedYear &&
        [self.dayCells count] >= components.day) 
    {
        NSInteger day=components.day;
        return [self.dayCells objectAtIndex: day - 1];
    }
    return nil;
}

- (CXCalendarCellView *) _cellForDate: (NSDate *) date 
{
    NSDateComponents *components = [self.calendar components: NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate: date];
    
    NSLog(@"self.displayedMonth=%d",self.displayedMonth);
    NSLog(@"self.displayedYear=%d",self.displayedYear);
    
    if (components.month == self.displayedMonth && components.year == self.displayedYear &&
    [self.dayCells count] >= components.day)
    {
        NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"self.day=%d AND self.month=%d",components.day,components.month];
        
        NSArray *cells = [self.dayCells filteredArrayUsingPredicate:filterPredicate];
        return [cells count] ? [cells objectAtIndex:0] : nil;
    }
    return nil;
}

- (CXCalendarCellView *) calenderCellForDate: (NSDate *) date 
{
    NSDateComponents *components = [self.calendar components: NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate: date];
    
    NSLog(@"self.displayedMonth=%d",self.displayedMonth);
    NSLog(@"self.displayedYear=%d",self.displayedYear);
    
    //if (components.month == self.displayedMonth && components.year == self.displayedYear &&
      if( [self.dayCells count] >= components.day)
    {
        NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"self.day=%d AND self.month=%d",components.day,components.month];
        
        NSArray *cells = [self.dayCells filteredArrayUsingPredicate:filterPredicate];
        return [cells count] ? [cells objectAtIndex:0] : nil;
    }
    return nil;
}


-(void)reload
{
    NSString *monthName = [[[[NSDateFormatter new] autorelease] standaloneMonthSymbols] objectAtIndex: self.displayedMonth - 1];
    self.monthLabel.text = [NSString stringWithFormat: @"%@ %d", NSLocalizedString(monthName, @""), self.displayedYear];
    
    if([self.delegate respondsToSelector:@selector(calendarView:didChangeMonth:)])
    {
        [self.delegate calendarView:self didChangeMonth:[NSString stringWithFormat: @"%@ %d", NSLocalizedString(monthName, @""), self.displayedYear]];
    }
    
    // Calculate shift
    NSDateComponents *components = [self.calendar components: NSWeekdayCalendarUnit
                                                    fromDate: [self displayedMonthStartDate]];
    
    NSInteger shift = components.weekday - self.calendar.firstWeekday;
    if (shift < 0) 
    {
        shift = 7 + shift;
    }
    
    // Calculate range
    NSRange range = [self.calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit
                                       forDate:self.displayedDate];
    
    
    //NSLog(@"Width=%f \n height=%f",self.gridView.frame.size.width,self.gridView.frame.size.height);
    //NSLog(@"%@",self.gridView);
    
    CGFloat cellHeight;
    if(range.length==28 && shift==0)//28 days month and week starts from sunday
    {
        //Month contains 4 row
        cellHeight = CGRectGetHeight(self.gridView.bounds) / 4.0;
        
        UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"calender_frame_four_row.png"]];
        //_gridView.backgroundColor = background;
        [background release];
        
    }
    else if(range.length==31 && (shift==5||shift==6))//31 days month and week starts from friday or saturday
    {
        //Month contains 6 row
        cellHeight = self.gridView.bounds.size.height / 6.0;
        UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"calender_frame_six_row.png"]];
        //_gridView.backgroundColor = background;
        [background release];
    }
    else if(range.length==30 && shift==6)//30 days month and week starts from Saturday
    {
        //Month contains 6 row
        cellHeight = self.gridView.bounds.size.height / 6.0;
        UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"calender_frame_six_row.png"]];
        //_gridView.backgroundColor = background;
        [background release];
    }
    else
    {
        cellHeight = (CGRectGetHeight(self.gridView.bounds)-4.0) / 5.0;
        
        // UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"calender_frame_five_row.png"]];
        //_gridView.backgroundColor = background;
        //[background release];
    }
    
    //CGFloat cellWidth = (self.bounds.size.width - kGridMargin * 2) / 7.0;
    CGFloat cellWidth = (CGRectGetWidth(self.gridView.bounds)-6.0) / 7.0;
    
    int _day=1;
    int _indexNextMonth=0;
    int _weekday=self.calendar.firstWeekday;
    NSDateFormatter *dateFromatter = [[[NSDateFormatter alloc] init] autorelease];
    dateFromatter.calendar = self.calendar;
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////     
    NSDate *predicateStartDate=nil;
    NSDate *predicateEndDate=nil;
    KalDate *_date1=nil;
    KalDate *_date2=nil;
    if([arrPrevMonth count])
        _date1=[arrPrevMonth objectAtIndex:0];
    else
        _date1=[KalDate dateForDay:1 month:self.displayedMonth year:self.displayedYear];
    predicateStartDate=[_date1 NSDate];
    
    if([arrNextMonth count])
        _date2=[arrNextMonth lastObject];
    else
    {
        NSUInteger day=range.length;
        _date2=[KalDate dateForDay:day month:self.displayedMonth year:self.displayedYear];
    }
    NSDate *_endDate=[_date2 NSDate];
    
    NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
    monthStep.hour = 23;
    monthStep.minute = 59;
    monthStep.second = 59;
    predicateEndDate = [_calendar dateByAddingComponents: monthStep toDate: _endDate options: 0];
    
    // self.arrayReturnedAfterPredicate=[CustomEventStore getEventsFromDate:predicateStartDate toDate:predicateEndDate];
    
    if([self.calDeligate respondsToSelector:@selector(reminderListForStartDate: toDate:)])
    {
        NSArray *events  = [self.calDeligate reminderListForStartDate:predicateStartDate toDate:predicateEndDate];
        self.arrayReturnedAfterPredicate = [NSMutableArray arrayWithArray:events];
    } 
    
    NSLog(@"self.arrayReturnedAfterPredicate=%@",self.arrayReturnedAfterPredicate);
    
    if([self.calDeligate respondsToSelector:@selector(taskListForStartDate: toDate:)])
    {
        NSArray *_tasks = [self.calDeligate taskListForStartDate:predicateStartDate toDate:predicateEndDate];
        self.arrTask = [NSMutableArray arrayWithArray:_tasks];
    }
    
    
    if([self.calDeligate respondsToSelector:@selector(visitListForStartDate: toDate:)])
    {
        NSArray *_visits =[self.calDeligate visitListForStartDate:predicateStartDate toDate:predicateEndDate];
        self.arrVisit = [NSMutableArray arrayWithArray:_visits];
    }
    
    
    BOOL isToday=NO;
    
    //for (NSUInteger i = 0; i < [self.dayCells count]; ++i) 
    
    NSUInteger i = 0;
    for(CXCalendarCellView *cellView in self.dayCells)
    {
        
        //CXCalendarCellView *cellView = [self.dayCells objectAtIndex:i];
        //cellView.frame = CGRectMake(cellWidth * ((shift + i) % 7), cellHeight * ((shift + i) / 7), cellWidth, cellHeight);
        cellView.frame = CGRectMake((cellWidth+1.0) * (i % 7), (cellHeight+1.0) * (i / 7),cellWidth, cellHeight);
        //NSLog(@"x=%f \n y=%f \n Width=%f \n height=%f",cellView.frame.origin.x,cellView.frame.origin.y,cellView.frame.size.width,cellView.frame.size.height);
        int count=(range.length+[arrPrevMonth count]+[arrNextMonth count]);
        cellView.hidden = (i >=count) ;
        cellView.selected = [[cellView dateWithBaseDate:self.displayedDate withCalendar:self.calendar] isEqualToDate:self.selectedDate];
        
        //cellView.layer.borderWidth = 0.52;
        //cellView.layer.borderColor = [UIColor colorWithWhite:0.2 alpha:0.3].CGColor;
        
        //////////////////////////////////////////////////////////////Previous Month Date///////////////////////////////////////////////////////////////////////////////////////
        if(i<shift)
        {
            __unused NSString *strWeekDay=nil;
            KalDate *_date=[arrPrevMonth objectAtIndex:i];
            NSLog(@"_Kaldate=%@",_date);
            NSDate *startDate=[_date NSDate];
            
            NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
            monthStep.hour = 23;
            monthStep.minute = 59;
            monthStep.second = 59;
            NSDate *endDate = [_calendar dateByAddingComponents: monthStep toDate: startDate options: 0];
            
            NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
            NSLog(@"startDate=%@",[formatter stringFromDate:startDate]);
            NSLog(@"endDate=%@",[formatter stringFromDate:endDate]);
            [formatter release];
            
            NSLog(@"_Original startDate=%@",startDate);
            NSLog(@"_Original endDate=%@",endDate);
            NSArray *arrReminder=nil;
            NSArray *arrSchedule=nil;
           
            
            NSArray *arrEvents = [self searchArrayUsingPredicateForStartDate:startDate toDate:endDate];
            NSArray *arrTasks  = [self searchTaskArrayUsingPredicateForStartDate:startDate toDate:endDate];
            
            NSArray *_arrSchedule=[self searchVisitArrayUsingPredicateForStartDate:startDate toDate:endDate];
            
            NSMutableArray *_arrReminder = [[NSMutableArray alloc] initWithArray:arrEvents];
           
            
            /*
             for(int i=0;i<[arrEvents count];i++)
             {
             EKEvent *event=[arrEvents objectAtIndex:i];
             NSLog(@"event.eventIdentifier=%@",event.eventIdentifier);
             NSLog(@"event.notes=%@",event.notes);
             NSString *strText;
             if([[event.notes componentsSeparatedByString:@"+"] count]==2)
             {
             strText=[[event.notes componentsSeparatedByString:@"+"] objectAtIndex:0];
             }
             else
             {
             strText=event.notes;
             }
             if([strText isEqualToString:@"Reminder"])
             {
             [_arrReminder addObject:event];
             }
             else//Visit
             {
             [_arrSchedule addObject:event];
             }
             }
             */
            
            arrEvents=nil;
            arrReminder=[NSArray arrayWithArray:_arrReminder];
            
            
            arrSchedule=[NSArray arrayWithArray:_arrSchedule];
            
            
            NSLog(@"arrReminder=%d",[arrReminder count]);
            NSLog(@"_date.day=%d",_date.day);
            
            if(_weekday < self.calendar.firstWeekday + 7)
            {
                NSUInteger index = (_weekday - 1) < 7 ? (_weekday - 1) : ((_weekday - 1) - 7);
                NSString *weekdayName = [[dateFromatter shortWeekdaySymbols] objectAtIndex: index];
                strWeekDay = NSLocalizedString(weekdayName, @"");
                //NSLog(@"strWeekDay=%@",strWeekDay);
                _weekday+=1;
            }
            
            NSLog(@"strWeekDay=%@",strWeekDay);
            
            strWeekDay=[strWeekDay stringByAppendingFormat:@" %d ",_date.day];
            cellView.day = _date.day;
            cellView.month = _date.month;
            cellView.lblDate.text=strWeekDay;
            cellView.lblDate.textColor = [UIColor lightGrayColor];
            
            ///////////////////////////////// REMINDER /////////////////////////////////////////
            
            if([arrReminder count]>1 && (selectedTabIndex==0||selectedTabIndex==1))
            {
                NSMutableArray *arr=[[NSMutableArray alloc] init];
                for (int i=0; i<[arrReminder count]; i++) 
                {
                    Reminder *event = [arrReminder objectAtIndex:i];
                    
                    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"EEE dd MMM, yyyy"];
                    
                    NSString *d1=[formatter stringFromDate:event.startDate];
                    NSString *d2=[formatter stringFromDate:startDate];
                    [formatter release];
                    
                    
                    if([d1 compare:d2] != NSOrderedSame)
                    {
                        [arr addObject:event];
                    }
                    
                }
                NSMutableArray *_arrReminder=[NSMutableArray arrayWithArray:arrReminder];
                [_arrReminder removeObjectsInArray:arr];
                [arr release];
                
                NSString *reminderCellTitle=nil;
                
                switch ([_arrReminder count]) 
                {
                    case 1:
                    {
                        Reminder *event    = [_arrReminder objectAtIndex:0];
                        
                        #if SHOW_REMINDER_TITLE
                        NSString *reminderTitle = event.title;                        
                        #else
                        NSString *reminderTitle = ([event.title rangeOfString:@":"].location !=NSNotFound)? [event.title substringFromIndex:[event.title rangeOfString:@":"].location+1] : event.title;
                        #endif
                        
                        reminderCellTitle = [NSString stringWithFormat:@"    %@",reminderTitle];                     
                    }
                    break;
                        
                    case 0:
                    {
                        reminderCellTitle = nil;
                    }
                    break;
                        
                    default:
                    {
                        reminderCellTitle = [NSString stringWithFormat:@"    %d Reminders",[_arrReminder count]];
                    }
                    break;
                }
                
                
                [cellView.btnReminder setTitle:reminderCellTitle forState:UIControlStateNormal];
                cellView.btnReminder.userInteractionEnabled=YES;
                cellView.btnReminder.hidden=([_arrReminder count]<=0);
                cellView.userInteractionEnabled = ([_arrReminder count]>0);
                cellView.reminderArrayList=_arrReminder;
            }
            else if([arrReminder count]==1 && (selectedTabIndex==0||selectedTabIndex==1))
            {
                Reminder *event=[arrReminder objectAtIndex:0];
                NSLog(@"event.startDate=%@",event.startDate);
                
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"EEE dd MMM, yyyy"];
                
                NSString *d1=[formatter stringFromDate:event.startDate];
                NSString *d2=[formatter stringFromDate:startDate];
                [formatter release];
                
                
                if([d1 compare:d2] == NSOrderedSame)
                {
#if SHOW_REMINDER_TITLE
                    NSString *reminderTitle = event.title;                        
#else
                    NSString *reminderTitle = ([event.title rangeOfString:@":"].location !=NSNotFound)? [event.title substringFromIndex:[event.title rangeOfString:@":"].location+1] : event.title;
#endif
                    [cellView.btnReminder setTitle:[NSString stringWithFormat:@"    %@",reminderTitle] forState:UIControlStateNormal];
                    cellView.btnReminder.userInteractionEnabled=YES;
                    cellView.btnReminder.hidden=NO;
                    
                    cellView.reminderArrayList=arrReminder;
                }
                else
                {
                    cellView.btnReminder.userInteractionEnabled=NO;
                    cellView.btnReminder.hidden=YES;
                    
                    cellView.reminderArrayList=nil;
                }
                
                
            }
            else
            {
                cellView.btnReminder.userInteractionEnabled=NO;
                cellView.btnReminder.hidden=YES;
                
                cellView.reminderArrayList=nil;
            }
            
            
            ///////////////////////////////// SCHEDULE /////////////////////////////////////////
            if([arrSchedule count]>1 && (selectedTabIndex==0||selectedTabIndex==2))
            {
                NSMutableArray *arr=[[NSMutableArray alloc] init];
                for (int i=0; i<[arrSchedule count]; i++) 
                {
                    //Reminder *event=[arrSchedule objectAtIndex:i];
                    ScheduledVisit *event=[arrSchedule objectAtIndex:i];
                    NSLog(@"event.startDate=%@",event.visitStartDate);
                    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"EEE dd MMM, yyyy"];
                    
                    NSString *d1=[formatter stringFromDate:event.visitStartDate];
                    NSString *d2=[formatter stringFromDate:startDate];
                    [formatter release];
                    
                    
                    if([d1 compare:d2] != NSOrderedSame)
                    {
                        [arr addObject:event];
                    }
                    
                }
                NSMutableArray *_arrSchedule=[NSMutableArray arrayWithArray:arrSchedule];
                [_arrSchedule removeObjectsInArray:arr];
                [arr release];
                
                NSString *cellVisitTitle = nil;
                switch ([_arrSchedule count]) 
                {
                    case 1:
                    {
                        //                        EKEvent *event = [_arrSchedule objectAtIndex:0];
                        //                        cellVisitTitle = [NSString stringWithFormat:@"    %@",event.title];
                        
                        ScheduledVisit *event = [_arrSchedule objectAtIndex:0];
                        NSString *visitTitle = event.customer.name.length ? event.customer.name : event.customerName.length ? event.customerName : @"Visit";
                        cellVisitTitle = [NSString stringWithFormat:@"    %@",visitTitle];
                    }
                        break;
                    case 0:
                    {
                        cellVisitTitle = nil;
                    }
                        break;
                        
                    default:
                    {
                        cellVisitTitle = [NSString stringWithFormat:@"    %d Visits",[_arrSchedule count]];
                    }
                        break;
                }
                
                [cellView.btnVisit setTitle:cellVisitTitle forState:UIControlStateNormal];
                cellView.btnVisit.userInteractionEnabled=[_arrSchedule count]>0;
                cellView.btnVisit.hidden=([_arrSchedule count] <=0);
                cellView.visitArrayList=_arrSchedule;
                
            }
            else if([arrSchedule count]==1 && (selectedTabIndex==0||selectedTabIndex==2))
            {
                // EKEvent *event=[arrSchedule objectAtIndex:0];
                ScheduledVisit *event=[arrSchedule objectAtIndex:0];
                NSLog(@"event.startDate=%@",event.visitStartDate);
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"EEE dd MMM, yyyy"];
                
                NSString *d1=[formatter stringFromDate:event.visitEndDate];
                NSString *d2=[formatter stringFromDate:startDate];
                [formatter release];
                
                
                if([d1 compare:d2] == NSOrderedSame)
                {
                    //[cellView.btnVisit setTitle:[NSString stringWithFormat:@"    %@",event.title] forState:UIControlStateNormal];
                    NSString *visitTitle = event.customer.name.length ? event.customer.name : event.customerName.length ? event.customerName : @"Visit";
                    [cellView.btnVisit setTitle:[NSString stringWithFormat:@"    %@",visitTitle] forState:UIControlStateNormal];
                    cellView.btnVisit.userInteractionEnabled=YES;
                    cellView.btnVisit.hidden=NO;
                    
                    cellView.visitArrayList=arrSchedule;
                }
                else
                {
                    cellView.btnVisit.userInteractionEnabled=NO;
                    cellView.btnVisit.hidden=YES;
                    
                    cellView.visitArrayList=nil;
                }
            }
            else
            {
                cellView.btnVisit.userInteractionEnabled=NO;
                cellView.btnVisit.hidden=YES;
                
                cellView.visitArrayList=nil;
            }
            
            
            ///////////////////////////////// Tasks /////////////////////////////////////////
            
            
            
            if([arrTasks count]>1 && (selectedTabIndex==0||selectedTabIndex==3))
            {
                
                switch ([arrTasks count]) 
                {
                    case 1:
                    {
                        Task *_task=[arrTasks objectAtIndex:0];
                        [cellView.btnTask setTitle:[NSString stringWithFormat:@"    %@",_task.subject] forState:UIControlStateNormal];
                        cellView.btnTask.userInteractionEnabled=YES;
                        cellView.btnTask.hidden=NO;
                        
                        cellView.taskArrayList=arrTasks;
                    }
                        break;
                    case 0:
                    {
                        cellView.btnTask.userInteractionEnabled=NO;
                        cellView.btnTask.hidden=YES;
                        
                        cellView.taskArrayList=nil;
                    }
                        break;
                        
                    default:
                    {
                        [cellView.btnTask setTitle:[NSString stringWithFormat:@"    %d Tasks",[arrTasks count]] forState:UIControlStateNormal];
                        cellView.btnTask.userInteractionEnabled=YES;
                        cellView.btnTask.hidden=NO;
                        
                        cellView.taskArrayList=arrTasks;
                    }
                        break;
                }
                
            }
            else if([arrTasks count]==1 && (selectedTabIndex==0||selectedTabIndex==3))
            {
                Task *_task=[arrTasks objectAtIndex:0];
                //TO DO
                //UnComment the following  line
                NSString *taskName=_task.subject;
                [cellView.btnTask setTitle:[NSString stringWithFormat:@"    %@",taskName] forState:UIControlStateNormal];
                cellView.btnTask.userInteractionEnabled=YES;
                cellView.btnTask.hidden=NO;
                
                cellView.taskArrayList=arrTasks;
            }
            else
            {
                cellView.btnTask.userInteractionEnabled=NO;
                cellView.btnTask.hidden=YES;
                
                cellView.taskArrayList=nil;
            }
            
            cellView.backgroundColor=[UIColor colorWithRed:250.0/255.0 green:250.0/255.0 blue:250.0/255.0 alpha:1];
            //  strWeekDay=@"";
            
            //[_arrSchedule release];
            [_arrReminder release];
        }
        
        //////////////////////////////////////////////////////////////current Month Date///////////////////////////////////////////////////////////////////////////////////////       
        else if(_day<=range.length)
        {
            
            NSString *strWeekDay;
            
            KalDate *_date=[KalDate dateForDay:_day month:self.displayedMonth year:self.displayedYear];
            NSLog(@"_Kaldate=%@",_date);
            
            BOOL _isToday=NO;
            
            if(!isToday) 
            {
                _isToday = [_date isToday];
            }
            
            
            NSDate *startDate=[_date NSDate];
            
            NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
            monthStep.hour = 23;
            monthStep.minute = 59;
            monthStep.second = 59;
            NSDate *endDate = [_calendar dateByAddingComponents: monthStep toDate: startDate options: 0];
            
            
            NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
            NSLog(@"startDate=%@",[formatter stringFromDate:startDate]);
            NSLog(@"endDate=%@",[formatter stringFromDate:endDate]);
            [formatter release];
            
            NSLog(@"_Original startDate=%@",startDate);
            NSLog(@"_Original endDate=%@",endDate);
            
            NSArray *arrReminder=nil;
            NSArray *arrSchedule=nil;
            //NSArray *arrEvents=[CustomEventStore getEventsFromDate:startDate toDate:endDate];
            NSArray *arrEvents=[self searchArrayUsingPredicateForStartDate:startDate toDate:endDate];
            NSArray *arrTasks=[self searchTaskArrayUsingPredicateForStartDate:startDate toDate:endDate];
            
            NSArray *_arrSchedule=[self searchVisitArrayUsingPredicateForStartDate:startDate toDate:endDate];
            
            NSMutableArray *_arrReminder = [[NSMutableArray alloc] initWithArray:arrEvents];
            //NSMutableArray *_arrReminder=[[NSMutableArray alloc] init];
            // NSMutableArray *_arrSchedule=[[NSMutableArray alloc] init];
            
            //[self searchReminders:_arrReminder andVisits:_arrSchedule fromEvents:arrEvents];
            //[self searchReminders:_arrReminder andVisits:nil fromEvents:arrEvents];
            
            
            
            arrEvents=nil;
            arrReminder=[NSArray arrayWithArray:_arrReminder];
            
            
            arrSchedule=[NSArray arrayWithArray:_arrSchedule];
            
            
            
            NSLog(@"arrReminder=%d",[arrReminder count]);
            NSLog(@"_date.day=%d",_date.day);
            
            
            if(_weekday < self.calendar.firstWeekday + 7)
            {
                NSUInteger index = (_weekday - 1) < 7 ? (_weekday - 1) : ((_weekday - 1) - 7);
                NSString *weekdayName = [[dateFromatter shortWeekdaySymbols] objectAtIndex: index];
                strWeekDay = NSLocalizedString(weekdayName, @"");
                //NSLog(@"strWeekDay=%@",strWeekDay);
                _weekday+=1;
            }
            strWeekDay=[strWeekDay stringByAppendingFormat:@" %d ",_day];
            
            
            cellView.day =_day;
            cellView.month = _date.month;
            cellView.lblDate.text=strWeekDay;
            cellView.lblDate.textColor = _isToday ? [UIColor colorWithRed:0 green:73.0f/255.0f blue:144.0f/255.0f alpha:1.0] : [UIColor blackColor];
            cellView.lblDate.font = _isToday ?   [UIFont grotesqueBoldFontOfSize:cellView.lblDate.font.pointSize] : [UIFont grotesqueFontOfSize:12.0];
            //cellView.backgroundColor = isToday ? LOGIN_BG_COLOR : cellView.backgroundColor;
            
            //SET COLOR FOR TODAY
            
            ///
            
            if((selectedTabIndex==0||selectedTabIndex==1))
            {
                NSMutableArray *arr=[[NSMutableArray alloc] init];
                for (Reminder *event in arrReminder) 
                {
                    NSLog(@"event.startDate=%@",event.startDate);
                    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"EEE dd MMM, yyyy"];
                    
                    NSString *d1=[formatter stringFromDate:event.startDate];
                    NSString *d2=[formatter stringFromDate:startDate];
                    [formatter release];
                    
                    
                    if([d1 compare:d2] != NSOrderedSame)
                    {
                        [arr addObject:event];
                    }
                    
                }
                
                NSMutableArray *_arrReminder=[NSMutableArray arrayWithArray:arrReminder];
                [_arrReminder removeObjectsInArray:arr];
                [arr release];
                
                
                NSString *reminderCellTitle=nil;
                
                switch ([_arrReminder count]) 
                {
                    case 1:
                    {
                        Reminder *event    = [_arrReminder objectAtIndex:0];
#if SHOW_REMINDER_TITLE
                        NSString *reminderTitle = event.title;                        
#else
                        NSString *reminderTitle = ([event.title rangeOfString:@":"].location !=NSNotFound)? [event.title substringFromIndex:[event.title rangeOfString:@":"].location+1] : event.title;
#endif

                        reminderCellTitle = [NSString stringWithFormat:@"    %@",reminderTitle];                     
                    }
                        break;
                    case 0:
                    {
                        reminderCellTitle = nil;
                    }
                        break;
                        
                    default:
                    {
                        reminderCellTitle = [NSString stringWithFormat:@"    %d Reminders",[_arrReminder count]];
                        
                    }
                        break;
                }
                
                [cellView.btnReminder setTitle:reminderCellTitle forState:UIControlStateNormal];
                //cellView.btnReminder.userInteractionEnabled=YES;
                // cellView.btnReminder.hidden=(![_arrReminder count]);
                //cellView.userInteractionEnabled = ([_arrReminder count]>0);
                cellView.reminderArrayList=_arrReminder;
                
                
            }
            
            
            ///////////////////////////////// REMINDER /////////////////////////////////////////
            if([arrReminder count]>1 && (selectedTabIndex==0||selectedTabIndex==1))
            {
                NSMutableArray *arr=[[NSMutableArray alloc] init];
                for (int i=0; i<[arrReminder count]; i++) 
                {
                    Reminder *event=[arrReminder objectAtIndex:i];
                    NSLog(@"event.startDate=%@",event.startDate);
                    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"EEE dd MMM, yyyy"];
                    
                    NSString *d1=[formatter stringFromDate:event.startDate];
                    NSString *d2=[formatter stringFromDate:startDate];
                    [formatter release];
                    
                    
                    if([d1 compare:d2] != NSOrderedSame)
                    {
                        [arr addObject:event];
                    }
                    
                }
                NSMutableArray *_arrReminder=[NSMutableArray arrayWithArray:arrReminder];
                [_arrReminder removeObjectsInArray:arr];
                [arr release];
                
                switch ([_arrReminder count]) 
                {
                    case 1:
                    {
                        Reminder *event=[_arrReminder objectAtIndex:0];
#if SHOW_REMINDER_TITLE
                        NSString *reminderTitle = event.title;                        
#else
                        NSString *reminderTitle = ([event.title rangeOfString:@":"].location !=NSNotFound)? [event.title substringFromIndex:[event.title rangeOfString:@":"].location+1] : event.title;
#endif
                        [cellView.btnReminder setTitle:[NSString stringWithFormat:@"    %@",reminderTitle] forState:UIControlStateNormal];
                        cellView.btnReminder.userInteractionEnabled=YES;
                        cellView.btnReminder.hidden=NO;
                        
                        cellView.reminderArrayList=_arrReminder;
                    }
                        break;
                    case 0:
                    {
                        cellView.btnReminder.userInteractionEnabled=NO;
                        cellView.btnReminder.hidden=YES;
                        
                        cellView.reminderArrayList=nil;
                    }
                        break;
                        
                    default:
                    {
                        [cellView.btnReminder setTitle:[NSString stringWithFormat:@"    %d Reminders",[_arrReminder count]] forState:UIControlStateNormal];
                        cellView.btnReminder.userInteractionEnabled=YES;
                        cellView.btnReminder.hidden=NO;
                        
                        cellView.reminderArrayList=_arrReminder;
                    }
                        break;
                }
                
            }
            else if([arrReminder count]==1 && (selectedTabIndex==0||selectedTabIndex==1))
            {
                Reminder *event=[arrReminder objectAtIndex:0];
                NSLog(@"event.startDate=%@",event.startDate);
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"EEE dd MMM, yyyy"];
                
                NSString *d1=[formatter stringFromDate:event.startDate];
                NSString *d2=[formatter stringFromDate:startDate];
                [formatter release];
                
                
                if([d1 compare:d2] == NSOrderedSame)
                {  
#if SHOW_REMINDER_TITLE
                    NSString *reminderTitle = event.title;                        
#else
                    NSString *reminderTitle = ([event.title rangeOfString:@":"].location !=NSNotFound)? [event.title substringFromIndex:[event.title rangeOfString:@":"].location+1] : event.title;
#endif
                    [cellView.btnReminder setTitle:[NSString stringWithFormat:@"    %@",reminderTitle] forState:UIControlStateNormal];
                    cellView.btnReminder.userInteractionEnabled=YES;
                    cellView.btnReminder.hidden=NO;
                    
                    cellView.reminderArrayList=arrReminder;
                }
                else
                {
                    cellView.btnReminder.userInteractionEnabled=NO;
                    cellView.btnReminder.hidden=YES;
                    
                    cellView.reminderArrayList=nil;
                }
            }
            else
            {
                cellView.btnReminder.userInteractionEnabled=NO;
                cellView.btnReminder.hidden=YES;
                
                cellView.reminderArrayList=nil;
            }
            
            ///////////////////////////////// SCHEDULE /////////////////////////////////////////
            
            
            
            if([arrSchedule count]>1 && (selectedTabIndex==0||selectedTabIndex==2))
            {
                
                NSMutableArray *arr=[[NSMutableArray alloc] init];
                for (int i=0; i<[arrSchedule count]; i++) 
                {
                    //Reminder *event=[arrSchedule objectAtIndex:i];
                    ScheduledVisit *event=[arrSchedule objectAtIndex:i];
                    NSLog(@"event.startDate=%@",event.visitStartDate);
                    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"EEE dd MMM, yyyy"];
                    
                    NSString *d1=[formatter stringFromDate:event.visitStartDate];
                    NSString *d2=[formatter stringFromDate:startDate];
                    [formatter release];
                    
                    
                    if([d1 compare:d2] != NSOrderedSame)
                    {
                        [arr addObject:event];
                    }
                    
                }
                NSMutableArray *_arrSchedule=[NSMutableArray arrayWithArray:arrSchedule];
                [_arrSchedule removeObjectsInArray:arr];
                [arr release];
                
                switch ([_arrSchedule count]) 
                {
                    case 1:
                    {
                        //EKEvent *event=[_arrSchedule objectAtIndex:0];
                        ScheduledVisit *event=[_arrSchedule objectAtIndex:0];
                        //[cellView.btnVisit setTitle:[NSString stringWithFormat:@"    %@",event.title] forState:UIControlStateNormal];
                        NSString *visitTitle = event.customer.name.length ? event.customer.name : event.customerName.length ? event.customerName : @"Visit";
                        [cellView.btnVisit setTitle:[NSString stringWithFormat:@"    %@",visitTitle] forState:UIControlStateNormal];
                        cellView.btnVisit.userInteractionEnabled=YES;
                        cellView.btnVisit.hidden=NO;
                        
                        cellView.visitArrayList=_arrSchedule;
                    }
                        break;
                    case 0:
                    {
                        cellView.btnVisit.userInteractionEnabled=NO;
                        cellView.btnVisit.hidden=YES;
                        
                        cellView.visitArrayList=nil;
                    }
                        break;
                        
                    default:
                    {
                        [cellView.btnVisit setTitle:[NSString stringWithFormat:@"    %d Visits",[_arrSchedule count]] forState:UIControlStateNormal];
                        cellView.btnVisit.userInteractionEnabled=YES;
                        cellView.btnVisit.hidden=NO;
                        
                        cellView.visitArrayList=_arrSchedule;
                    }
                        break;
                }
                
            }
            else if([arrSchedule count]==1 && (selectedTabIndex==0||selectedTabIndex==2))
            {
                //EKEvent *event=[arrSchedule objectAtIndex:0];
                ScheduledVisit *event=[arrSchedule objectAtIndex:0];
                NSLog(@"event.startDate=%@",event.visitStartDate);
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"EEE dd MMM, yyyy"];
                
                NSString *d1=[formatter stringFromDate:event.visitStartDate];
                NSString *d2=[formatter stringFromDate:startDate];
                [formatter release];
                
                
                if([d1 compare:d2] == NSOrderedSame)
                {
                    //[cellView.btnVisit setTitle:[NSString stringWithFormat:@"    %@",event.title] forState:UIControlStateNormal];
                    NSString *visitTitle = event.customer.name.length ? event.customer.name : event.customerName.length ? event.customerName : @"Visit";
                    [cellView.btnVisit setTitle:[NSString stringWithFormat:@"    %@",visitTitle] forState:UIControlStateNormal];
                    cellView.btnVisit.userInteractionEnabled=YES;
                    cellView.btnVisit.hidden=NO;
                    
                    cellView.visitArrayList=arrSchedule;
                }
                else
                {
                    cellView.btnVisit.userInteractionEnabled=NO;
                    cellView.btnVisit.hidden=YES;
                    
                    cellView.visitArrayList=nil;
                }
            }
            else
            {
                cellView.btnVisit.userInteractionEnabled=NO;
                cellView.btnVisit.hidden=YES;
                
                cellView.visitArrayList=nil;
            }
            
            ///////////////////////////////// Tasks /////////////////////////////////////////
            
            if([arrTasks count]>1 && (selectedTabIndex==0||selectedTabIndex==3))
            {
                
                switch ([arrTasks count]) 
                {
                    case 1:
                    {
                        Task *_task=[arrTasks objectAtIndex:0];
                        //TO DO
                        //UnComment the following  line
                        [cellView.btnTask setTitle:[NSString stringWithFormat:@"    %@",_task.subject] forState:UIControlStateNormal];
                        cellView.btnTask.userInteractionEnabled=YES;
                        cellView.btnTask.hidden=NO;
                        
                        cellView.taskArrayList=arrTasks;
                    }
                        break;
                    case 0:
                    {
                        cellView.btnTask.userInteractionEnabled=NO;
                        cellView.btnTask.hidden=YES;
                        
                        cellView.taskArrayList=nil;
                    }
                        break;
                        
                    default:
                    {
                        [cellView.btnTask setTitle:[NSString stringWithFormat:@"    %d Tasks",[arrTasks count]] forState:UIControlStateNormal];
                        cellView.btnTask.userInteractionEnabled=YES;
                        cellView.btnTask.hidden=NO;
                        
                        cellView.taskArrayList=arrTasks;
                    }
                        break;
                }
                
            }
            else if([arrTasks count]==1 && (selectedTabIndex==0||selectedTabIndex==3))
            {
                Task *_task=[arrTasks objectAtIndex:0];
                //TO DO
                //UnComment the following  line
                [cellView.btnTask setTitle:[NSString stringWithFormat:@"    %@",_task.subject] forState:UIControlStateNormal];
                cellView.btnTask.userInteractionEnabled=YES;
                cellView.btnTask.hidden=NO;
                
                cellView.taskArrayList=arrTasks;
            }
            else
            {
                cellView.btnTask.userInteractionEnabled=NO;
                cellView.btnTask.hidden=YES;
                
                cellView.taskArrayList=nil;
            }
            
            cellView.backgroundColor= _isToday ? [UIColor colorWithRed:200.0/255.0 green:215.0f/255.0f blue:230.0f/255.0f alpha:1.0] : [UIColor whiteColor];
            strWeekDay=@"";
            _day+=1;
            
            [_arrReminder release];
            //[_arrSchedule release];
        }
        
        //////////////////////////////////////////////////////////////next Month Date///////////////////////////////////////////////////////////////////////////////////////     
        else if(_day>range.length && cellView.hidden==NO)
        {
            //NSLog(@"arrNextMonth=%@",arrNextMonth);
            KalDate *_date=[arrNextMonth objectAtIndex:_indexNextMonth];
            NSLog(@"_Kaldate=%@",_date);
            NSDate *startDate=[_date NSDate];
            //NSDate *endDate=[NSDate dateWithTimeInterval:60 sinceDate:startDate];
            
            NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
            monthStep.hour = 23;
            monthStep.minute = 59;
            monthStep.second = 59;
            NSDate *endDate = [_calendar dateByAddingComponents: monthStep toDate: startDate options: 0];
            
            
            NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
            NSLog(@"startDate=%@",[formatter stringFromDate:startDate]);
            NSLog(@"endDate=%@",[formatter stringFromDate:endDate]);
            [formatter release];
            
            NSLog(@"_Original startDate=%@",startDate);
            NSLog(@"_Original endDate=%@",endDate);
            NSArray *arrReminder=nil;
            NSArray *arrSchedule=nil;
            //NSArray *arrEvents=[CustomEventStore getEventsFromDate:startDate toDate:endDate];
            NSArray *arrEvents=[self searchArrayUsingPredicateForStartDate:startDate toDate:endDate];
            NSArray *arrTasks=[self searchTaskArrayUsingPredicateForStartDate:startDate toDate:endDate];
            
            NSArray *_arrSchedule=[self searchVisitArrayUsingPredicateForStartDate:startDate toDate:endDate];
            
            NSMutableArray *_arrReminder = [[NSMutableArray alloc] initWithArray:arrEvents];
            // NSMutableArray *_arrReminder=[[NSMutableArray alloc] init];
            //NSMutableArray *_arrSchedule=[[NSMutableArray alloc] init];
            
            //[self searchReminders:_arrReminder andVisits:_arrSchedule fromEvents:arrEvents];
            //[self searchReminders:_arrReminder andVisits:nil fromEvents:arrEvents];
            
            /*TEST COMMENT BY SANDIP
            //=============== DEVIDE THE REMINDERS AND SCHEDULED VISITS FROM EVENTS ==============
            for(int i=0;i<[arrEvents count];i++)
            {
                EKEvent *event=[arrEvents objectAtIndex:i];
                NSLog(@"event.notes=%@",event.notes);
                
                NSString *strText;
                if([[event.notes componentsSeparatedByString:@"+"] count]==2)
                {
                    strText=[[event.notes componentsSeparatedByString:@"+"] objectAtIndex:0];
                }
                else
                {
                    strText=event.notes;
                }
                if([strText isEqualToString:@"Reminder"])
                {
                    [_arrReminder addObject:event];
                }
                else//Visit
                {
                    [_arrSchedule addObject:event];
                }
            }
            */
            //=============== DEVIDE THE REMINDERS AND SCHEDULED VISITS FROM EVENTS ==============
            
            
            arrEvents=nil;
            arrReminder=[NSArray arrayWithArray:_arrReminder];
            
            
            arrSchedule=[NSArray arrayWithArray:_arrSchedule];
            
            
            NSLog(@"arrReminder=%d",[arrReminder count]);
            NSLog(@"_date.day=%d",_date.day);
            
            cellView.day = _date.day;
            cellView.month = _date.month;
            cellView.lblDate.text=[NSString stringWithFormat: @"%d ",  _date.day];
            cellView.lblDate.textColor = [UIColor lightGrayColor];
            
            
            if((selectedTabIndex==0||selectedTabIndex==1))
            {
                NSMutableArray *arr=[[NSMutableArray alloc] init];
                for (Reminder *event in arrReminder) 
                {
                    NSLog(@"event.startDate=%@",event.startDate);
                    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"EEE dd MMM, yyyy"];
                    
                    NSString *d1=[formatter stringFromDate:event.startDate];
                    NSString *d2=[formatter stringFromDate:startDate];
                    [formatter release];
                    
                    
                    if([d1 compare:d2] != NSOrderedSame)
                    {
                        [arr addObject:event];
                    }
                    
                }
                
                NSMutableArray *_arrReminder=[NSMutableArray arrayWithArray:arrReminder];
                [_arrReminder removeObjectsInArray:arr];
                [arr release];
                
                
                NSString *reminderCellTitle=nil;
                
                switch ([_arrReminder count]) 
                {
                    case 1:
                    {
                        Reminder *event    = [_arrReminder objectAtIndex:0];
#if SHOW_REMINDER_TITLE
                        NSString *reminderTitle = event.title;                        
#else
                        NSString *reminderTitle = ([event.title rangeOfString:@":"].location !=NSNotFound)? [event.title substringFromIndex:[event.title rangeOfString:@":"].location+1] : event.title;
#endif
                        reminderCellTitle = [NSString stringWithFormat:@"    %@",reminderTitle];                     
                    }
                        break;
                    case 0:
                    {
                        reminderCellTitle = nil;
                    }
                        break;
                        
                    default:
                    {
                        reminderCellTitle = [NSString stringWithFormat:@"    %d Reminders",[_arrReminder count]];
                        
                    }
                        break;
                }
                
                [cellView.btnReminder setTitle:reminderCellTitle forState:UIControlStateNormal];
                //cellView.btnReminder.userInteractionEnabled=YES;
                //cellView.btnReminder.hidden=([_arrReminder count]<=0);
                // cellView.userInteractionEnabled = ([_arrReminder count]>0);
                cellView.reminderArrayList=_arrReminder;
                
                
            }
            
            
            ///////////////////////////////// REMINDER /////////////////////////////////////////
            if([arrReminder count]>1 && (selectedTabIndex==0||selectedTabIndex==1))
            {
                NSMutableArray *arr=[[NSMutableArray alloc] init];
                for (int i=0; i<[arrReminder count]; i++) 
                {
                    Reminder *event=[arrReminder objectAtIndex:i];
                    NSLog(@"event.startDate=%@",event.startDate);
                    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"EEE dd MMM, yyyy"];
                    
                    NSString *d1=[formatter stringFromDate:event.startDate];
                    NSString *d2=[formatter stringFromDate:startDate];
                    [formatter release];
                    
                    
                    if([d1 compare:d2] != NSOrderedSame)
                    {
                        [arr addObject:event];
                    }
                    
                }
                NSMutableArray *_arrReminder=[NSMutableArray arrayWithArray:arrReminder];
                [_arrReminder removeObjectsInArray:arr];
                [arr release];
                
                switch ([_arrReminder count]) 
                {
                    case 1:
                    {
                        Reminder *event=[_arrReminder objectAtIndex:0];
#if SHOW_REMINDER_TITLE
                        NSString *reminderTitle = event.title;                        
#else
                        NSString *reminderTitle = ([event.title rangeOfString:@":"].location !=NSNotFound)? [event.title substringFromIndex:[event.title rangeOfString:@":"].location+1] : event.title;
#endif
                        [cellView.btnReminder setTitle:[NSString stringWithFormat:@"    %@",reminderTitle] forState:UIControlStateNormal];
                        cellView.btnReminder.userInteractionEnabled=YES;
                        cellView.btnReminder.hidden=NO;
                        
                        cellView.reminderArrayList=_arrReminder;
                    }
                        break;
                    case 0:
                    {
                        cellView.btnReminder.userInteractionEnabled=NO;
                        cellView.btnReminder.hidden=YES;
                        
                        cellView.reminderArrayList=nil;
                    }
                        break;
                        
                    default:
                    {
                        [cellView.btnReminder setTitle:[NSString stringWithFormat:@"    %d Reminders",[_arrReminder count]] forState:UIControlStateNormal];
                        cellView.btnReminder.userInteractionEnabled=YES;
                        cellView.btnReminder.hidden=NO;
                        
                        cellView.reminderArrayList=_arrReminder;
                    }
                        break;
                }
                
                
            }
            else if([arrReminder count]==1 && (selectedTabIndex==0||selectedTabIndex==1))
            {
                Reminder *event=[arrReminder objectAtIndex:0];
                NSLog(@"event.startDate=%@",event.startDate);
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"EEE dd MMM, yyyy"];
                
                NSString *d1=[formatter stringFromDate:event.startDate];
                NSString *d2=[formatter stringFromDate:startDate];
                [formatter release];
                
                
                if([d1 compare:d2] == NSOrderedSame)
                {
#if SHOW_REMINDER_TITLE
                    NSString *reminderTitle = event.title;                        
#else
                    NSString *reminderTitle = ([event.title rangeOfString:@":"].location !=NSNotFound)? [event.title substringFromIndex:[event.title rangeOfString:@":"].location+1] : event.title;
#endif
                    [cellView.btnReminder setTitle:[NSString stringWithFormat:@"    %@",reminderTitle] forState:UIControlStateNormal];
                    cellView.btnReminder.userInteractionEnabled=YES;
                    cellView.btnReminder.hidden=NO;
                    
                    cellView.reminderArrayList=arrReminder;
                }
                else
                {
                    cellView.btnReminder.userInteractionEnabled=NO;
                    cellView.btnReminder.hidden=YES;
                    
                    cellView.reminderArrayList=nil;
                }
            }
            else
            {
                cellView.btnReminder.userInteractionEnabled=NO;
                cellView.btnReminder.hidden=YES;
                
                cellView.reminderArrayList=nil;
            }
            
            ///////////////////////////////// SCHEDULE /////////////////////////////////////////
            
            
            if([arrSchedule count]>1 && (selectedTabIndex==0||selectedTabIndex==2))
            {
                NSMutableArray *arr=[[NSMutableArray alloc] init];
                for (int i=0; i<[arrSchedule count]; i++) 
                {
                    // EKEvent *event=[arrSchedule objectAtIndex:i];
                    ScheduledVisit *event=[arrSchedule objectAtIndex:i];
                    NSLog(@"event.startDate=%@",event.visitStartDate);
                    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"EEE dd MMM, yyyy"];
                    
                    NSString *d1=[formatter stringFromDate:event.visitStartDate];
                    NSString *d2=[formatter stringFromDate:startDate];
                    [formatter release];
                    
                    
                    if([d1 compare:d2] != NSOrderedSame)
                    {
                        [arr addObject:event];
                    }
                    
                }
                NSMutableArray *_arrSchedule=[NSMutableArray arrayWithArray:arrSchedule];
                [_arrSchedule removeObjectsInArray:arr];
                [arr release];
                
                switch ([_arrSchedule count]) 
                {
                    case 1:
                    {
                        // EKEvent *event=[_arrSchedule objectAtIndex:0];
                        ScheduledVisit *event=[_arrSchedule objectAtIndex:0];
                        //[cellView.btnVisit setTitle:[NSString stringWithFormat:@"    %@",event.title] forState:UIControlStateNormal];
                        NSString *visitTitle = event.customer.name.length ? event.customer.name : event.customerName.length ? event.customerName : @"Visit";
                        [cellView.btnVisit setTitle:[NSString stringWithFormat:@"    %@",visitTitle] forState:UIControlStateNormal];
                        cellView.btnVisit.userInteractionEnabled=YES;
                        cellView.btnVisit.hidden=NO;
                        
                        cellView.visitArrayList=_arrSchedule;
                    }
                        break;
                    case 0:
                    {
                        cellView.btnVisit.userInteractionEnabled=NO;
                        cellView.btnVisit.hidden=YES;
                        
                        cellView.visitArrayList=nil;
                    }
                        break;
                        
                    default:
                    {
                        [cellView.btnVisit setTitle:[NSString stringWithFormat:@"    %d Visits",[_arrSchedule count]] forState:UIControlStateNormal];
                        cellView.btnVisit.userInteractionEnabled=YES;
                        cellView.btnVisit.hidden=NO;
                        
                        cellView.visitArrayList=_arrSchedule;
                    }
                        break;
                }
            }
            else if([arrSchedule count]==1 && (selectedTabIndex==0||selectedTabIndex==2))
            {
                //EKEvent *event=[arrSchedule objectAtIndex:0];
                ScheduledVisit *event=[arrSchedule objectAtIndex:0];
                NSLog(@"event.startDate=%@",event.visitStartDate);
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"EEE dd MMM, yyyy"];
                
                NSString *d1=[formatter stringFromDate:event.visitStartDate];
                NSString *d2=[formatter stringFromDate:startDate];
                [formatter release];
                
                
                if([d1 compare:d2] == NSOrderedSame)
                {
                    //[cellView.btnVisit setTitle:[NSString stringWithFormat:@"    %@",event.title] forState:UIControlStateNormal];
                    NSString *visitTitle = event.customer.name.length ? event.customer.name : event.customerName.length ? event.customerName : @"Visit";
                    [cellView.btnVisit setTitle:[NSString stringWithFormat:@"    %@",visitTitle] forState:UIControlStateNormal];
                    cellView.btnVisit.userInteractionEnabled=YES;
                    cellView.btnVisit.hidden=NO;
                    
                    cellView.visitArrayList=arrSchedule;
                }
                else
                {
                    cellView.btnVisit.userInteractionEnabled=NO;
                    cellView.btnVisit.hidden=YES;
                    
                    cellView.visitArrayList=nil;
                }
            }
            else
            {
                cellView.btnVisit.userInteractionEnabled=NO;
                cellView.btnVisit.hidden=YES;
                
                cellView.visitArrayList=nil;
            }
            
            ///////////////////////////////// Tasks /////////////////////////////////////////
            
            
            if([arrTasks count]>1 && (selectedTabIndex==0||selectedTabIndex==3))
            {
                
                switch ([arrTasks count]) 
                {
                    case 1:
                    {
                        Task *_task=[arrTasks objectAtIndex:0];
                        //TO DO
                        //UnComment the following  line
                        [cellView.btnTask setTitle:[NSString stringWithFormat:@"    %@",_task.subject] forState:UIControlStateNormal];
                        cellView.btnTask.userInteractionEnabled=YES;
                        cellView.btnTask.hidden=NO;
                        
                        cellView.taskArrayList=arrTasks;
                    }
                        break;
                    case 0:
                    {
                        cellView.btnTask.userInteractionEnabled=NO;
                        cellView.btnTask.hidden=YES;
                        
                        cellView.taskArrayList=nil;
                    }
                        break;
                        
                    default:
                    {
                        [cellView.btnTask setTitle:[NSString stringWithFormat:@"    %d Tasks",[arrTasks count]] forState:UIControlStateNormal];
                        cellView.btnTask.userInteractionEnabled=YES;
                        cellView.btnTask.hidden=NO;
                        
                        cellView.taskArrayList=arrTasks;
                    }
                        break;
                }
                
            }
            else if([arrTasks count]==1 && (selectedTabIndex==0||selectedTabIndex==3))
            {
                Task *_task=[arrTasks objectAtIndex:0];
                //TO DO
                //UnComment the following  line
                [cellView.btnTask setTitle:[NSString stringWithFormat:@"    %@",_task.subject] forState:UIControlStateNormal];
                cellView.btnTask.userInteractionEnabled=YES;
                cellView.btnTask.hidden=NO;
                
                cellView.taskArrayList=arrTasks;
            }
            else
            {
                cellView.btnTask.userInteractionEnabled=NO;
                cellView.btnTask.hidden=YES;
                
                cellView.taskArrayList=nil;
            }
            
            
            
            cellView.backgroundColor=[UIColor colorWithRed:250.0/255.0 green:249.0/250.0 blue:249.0/250.0 alpha:1];
            _indexNextMonth+=1;
            
            [_arrReminder release];
            //[_arrSchedule release];
        }
        
        //  [cellView setNeedsLayout];
        
        i++;
    }

    if([self.calDeligate respondsToSelector:@selector(calenderDidLoad)])
        [self.calDeligate calenderDidLoad];
}


 
 -(void)_reload
 {
 NSString *monthName = [[[[NSDateFormatter new] autorelease] standaloneMonthSymbols] objectAtIndex: self.displayedMonth - 1];
 self.monthLabel.text = [NSString stringWithFormat: @"%@ %d", NSLocalizedString(monthName, @""), self.displayedYear];
 
 if([self.delegate respondsToSelector:@selector(calendarView:didChangeMonth:)])
 {
 [self.delegate calendarView:self didChangeMonth:[NSString stringWithFormat: @"%@ %d", NSLocalizedString(monthName, @""), self.displayedYear]];
 }
 
 //===============================  Calculate shift =======================================
 NSDateComponents *components = [self.calendar components: NSWeekdayCalendarUnit
 fromDate: [self displayedMonthStartDate]];
 
 NSInteger shift = components.weekday - self.calendar.firstWeekday;
 if (shift < 0) 
 {
 shift = 7 + shift;
 }
 
 // Calculate range
 NSRange range = [self.calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit
 forDate:self.displayedDate];
 
 CGFloat cellHeight;
 if(range.length==28 && shift==0)//28 days month and week starts from sunday
 {
 //Month contains 4 row
 cellHeight = CGRectGetHeight(self.gridView.bounds) / 4.0;
 
 /*
 UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"calender_frame_four_row.png"]];
 //_gridView.backgroundColor = background;
 [background release];
 */

}
else if(range.length==31 && (shift==5||shift==6))//31 days month and week starts from friday or saturday
{
    //Month contains 6 row
    cellHeight = self.gridView.bounds.size.height / 6.0;
    /*
     UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"calender_frame_six_row.png"]];
     //_gridView.backgroundColor = background;
     [background release];
     */
}
else if(range.length==30 && shift==6)//30 days month and week starts from Saturday
{
    //Month contains 6 row
    cellHeight = self.gridView.bounds.size.height / 6.0;
    
    /*
     UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"calender_frame_six_row.png"]];
     //_gridView.backgroundColor = background;
     [background release];
     */
}
else
{
    cellHeight = (CGRectGetHeight(self.gridView.bounds)-4.0) / 5.0;
    
    // UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"calender_frame_five_row.png"]];
    //_gridView.backgroundColor = background;
    //[background release];
}

//CGFloat cellWidth = (self.bounds.size.width - kGridMargin * 2) / 7.0;
CGFloat cellWidth = (CGRectGetWidth(self.gridView.bounds)-6.0) / 7.0;

int _day=1;
int _indexNextMonth=0;
int _weekday=self.calendar.firstWeekday;
NSDateFormatter *dateFromatter = [[[NSDateFormatter alloc] init] autorelease];
dateFromatter.calendar = self.calendar;


///////////////////  CALCULATE PREDICATE START DATE AND END DATE ///////////////////////////////    
NSDate *predicateStartDate=nil;
NSDate *predicateEndDate=nil;
KalDate *_date1=nil;
KalDate *_date2=nil;
if([arrPrevMonth count])
_date1=[arrPrevMonth objectAtIndex:0];
else
_date1=[KalDate dateForDay:1 month:self.displayedMonth year:self.displayedYear];
predicateStartDate=[_date1 NSDate];

if([arrNextMonth count])
_date2=[arrNextMonth lastObject];
else
{
    NSUInteger day=range.length;
    _date2=[KalDate dateForDay:day month:self.displayedMonth year:self.displayedYear];
}
NSDate *_endDate=[_date2 NSDate];

NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
monthStep.hour = 23;
monthStep.minute = 59;
monthStep.second = 59;
predicateEndDate = [_calendar dateByAddingComponents: monthStep toDate: _endDate options: 0];


self.currentStartDate =  predicateStartDate;
self.currentEndDate   =  predicateEndDate;
//========== FETCH OBJECTS FOR PREDICATE START DATE AND END DATE =================


BOOL isToday=NO;
NSUInteger i = 0;
for(CXCalendarCellView *cellView in self.dayCells)
{
    cellView.frame = CGRectMake((cellWidth+1.0) * (i % 7), (cellHeight+1.0) * (i / 7),cellWidth, cellHeight);
    
    int count=(range.length+[arrPrevMonth count]+[arrNextMonth count]);
    cellView.hidden = (i >=count) ;
    cellView.selected = [[cellView dateWithBaseDate:self.displayedDate withCalendar:self.calendar] isEqualToDate:self.selectedDate]; //
    
    //cellView.layer.borderWidth = 0.52;
    //cellView.layer.borderColor = [UIColor colorWithWhite:0.2 alpha:0.3].CGColor;
    [cellView reset];
    
    /////////////////////////Previous Month Date////////////////////////////////////////////////
    if(i<shift)
    {
        __unused NSString *strWeekDay=nil;
        KalDate *_date=[arrPrevMonth objectAtIndex:i];
        NSLog(@"_Kaldate=%@",_date);
        NSDate *startDate=[_date NSDate];
        
        NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
        monthStep.hour = 23;
        monthStep.minute = 59;
        monthStep.second = 59;
        NSDate *endDate = [_calendar dateByAddingComponents: monthStep toDate: startDate options: 0];
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
        NSLog(@"startDate=%@",[formatter stringFromDate:startDate]);
        NSLog(@"endDate=%@",[formatter stringFromDate:endDate]);
        [formatter release];
        
        NSLog(@"_Original startDate=%@",startDate);
        NSLog(@"_Original endDate=%@",endDate);
        
        
        
        NSLog(@"_date.day=%d",_date.day);
        
        if(_weekday < self.calendar.firstWeekday + 7)
        {
            NSUInteger index = (_weekday - 1) < 7 ? (_weekday - 1) : ((_weekday - 1) - 7);
            NSString *weekdayName = [[dateFromatter shortWeekdaySymbols] objectAtIndex: index];
            strWeekDay = NSLocalizedString(weekdayName, @"");
            //NSLog(@"strWeekDay=%@",strWeekDay);
            _weekday+=1;
        }
        
        NSLog(@"strWeekDay=%@",strWeekDay);
        
        strWeekDay=[strWeekDay stringByAppendingFormat:@" %d ",_date.day];
        cellView.day = _date.day;
        cellView.month = _date.month;
        cellView.lblDate.text=strWeekDay;
        cellView.lblDate.textColor = [UIColor lightGrayColor];
        cellView.backgroundColor=[UIColor colorWithRed:250.0/255.0 green:250.0/255.0 blue:250.0/255.0 alpha:1];
        //cellView.btnReminder.hidden=NO;
    }
    /////////////////////////// current Month Date /////////////////////////////////     
    else if(_day<=range.length)
    {
        NSString *strWeekDay;
        

        KalDate *_date=[KalDate dateForDay:_day month:self.displayedMonth year:self.displayedYear];
        
        
        
        BOOL _isToday=NO;
        
        if(!isToday) 
        {
            _isToday = [_date isToday];
        }
        
        if(_isToday)
        NSLog(@"_Kaldate=%@ day=%d",_date,_day);
        
        
        NSDate *startDate=[_date NSDate];
        
        NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
        monthStep.hour = 23;
        monthStep.minute = 59;
        monthStep.second = 59;
        NSDate *endDate = [_calendar dateByAddingComponents: monthStep toDate: startDate options: 0];
        
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
        NSLog(@"startDate=%@",[formatter stringFromDate:startDate]);
        NSLog(@"endDate=%@",[formatter stringFromDate:endDate]);
        [formatter release];
        
        NSLog(@"_Original startDate=%@",startDate);
        NSLog(@"_Original endDate=%@",endDate);
        
        if(_weekday < self.calendar.firstWeekday + 7)
        {
            NSUInteger index = (_weekday - 1) < 7 ? (_weekday - 1) : ((_weekday - 1) - 7);
            NSString *weekdayName = [[dateFromatter shortWeekdaySymbols] objectAtIndex: index];
            strWeekDay = NSLocalizedString(weekdayName, @"");
            //NSLog(@"strWeekDay=%@",strWeekDay);
            _weekday+=1;
        }
        strWeekDay=[strWeekDay stringByAppendingFormat:@" %d ",_day];
        
        
        cellView.day =_day;
        cellView.month = _date.month;
        cellView.lblDate.text=strWeekDay;
        
        [cellView setTodayCell:_isToday];
        
        /*
        cellView.lblDate.textColor = _isToday ? [UIColor colorWithRed:0 green:73.0f/255.0f blue:144.0f/255.0f alpha:1.0] : [UIColor blackColor];
        cellView.lblDate.font = _isToday ?   [UIFont grotesqueBoldFontOfSize:cellView.lblDate.font.pointSize] : [UIFont grotesqueFontOfSize:12.0];
        //cellView.backgroundColor = isToday ? LOGIN_BG_COLOR : cellView.backgroundColor;
        
        cellView.backgroundColor= _isToday ? [UIColor colorWithRed:200.0/255.0 green:215.0f/255.0f blue:230.0f/255.0f alpha:1.0] : [UIColor whiteColor];
         */
        
        strWeekDay=@"";
        _day+=1;
        
        if(_isToday)
        {
            self.todayCell = cellView;
        }
    }
    
    ////////////////////////next Month Date//////////////////////////////////     
   // else if(_day>range.length && cellView.hidden==NO)
    else if(_day>range.length)
    {
        //NSLog(@"arrNextMonth=%@",arrNextMonth);
        KalDate *_date=_indexNextMonth<[arrNextMonth count] ? [arrNextMonth objectAtIndex:_indexNextMonth]:nil;
        
        if(!_date) continue;
        
        NSLog(@"_Kaldate=%@",_date);
        NSDate *startDate=[_date NSDate];
        //NSDate *endDate=[NSDate dateWithTimeInterval:60 sinceDate:startDate];
        
        NSDateComponents *monthStep = [[NSDateComponents new] autorelease];
        monthStep.hour = 23;
        monthStep.minute = 59;
        monthStep.second = 59;
        NSDate *endDate = [_calendar dateByAddingComponents: monthStep toDate: startDate options: 0];
        
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEE dd MMM, yyyy, h:mm a"];
        NSLog(@"startDate=%@",[formatter stringFromDate:startDate]);
        NSLog(@"endDate=%@",[formatter stringFromDate:endDate]);
        [formatter release];
        
        NSLog(@"_Original startDate=%@",startDate);
        NSLog(@"_Original endDate=%@",endDate);
        
        NSLog(@"_date.day=%d _date.month=%d",_date.day,_date.month);
        
        cellView.day = _date.day;
        cellView.month = _date.month;
        cellView.lblDate.text=[NSString stringWithFormat: @"%d ",  _date.day];
        cellView.lblDate.textColor = [UIColor lightGrayColor];
        
        cellView.backgroundColor=[UIColor colorWithRed:250.0/255.0 green:249.0/250.0 blue:249.0/250.0 alpha:1];
        _indexNextMonth+=1;
    }
    
     NSLog(@"day=%d month=%d",cellView.day,cellView.month);
    
    i++;
}

[self fetchItemsFromStartDate:predicateStartDate endDate:predicateEndDate];

//if([self.calDeligate respondsToSelector:@selector(calenderDidLoad)])
//[self.calDeligate calenderDidLoad];

}
 


-(void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
  
        
    self.gridView.frame = CGRectMake(kGridMargin, kGridMargin,
                                     self.bounds.size.width - kGridMargin * 2,
                                     self.bounds.size.height -kGridMargin-self.monthBarHeight);
    //================================ INNER SHADOW ==============================
    
    CGFloat top = CGRectGetMaxY(self.gridView.frame);
    
    
    if (self.monthBarHeight) 
    {
        self.monthBar.frame = CGRectMake(0, top, self.bounds.size.width, self.monthBarHeight);
        self.todayButton.frame = CGRectMake(10.0, (self.monthBarHeight-29.0)/2.0,54.0, 29.0);
        

        self.monthBackButton.frame = CGRectMake(CGRectGetMaxX(self.todayButton.frame)+22.0,  (self.monthBarHeight-30.0)/2.0, 30.0, 30.0); //16
        
        self.monthForwardButton.frame = CGRectMake(self.monthBar.bounds.size.width - (CGRectGetWidth(self.monthBackButton.frame)+18.0), (self.monthBarHeight-30.0)/2.0,30.0, 30.0);
        // top = self.monthBar.frame.origin.y + self.monthBar.frame.size.height;
    } 
    else 
    {
        self.monthBar.frame = CGRectZero;
    }

    
    [_gridView performSelector:@selector(addInnerShadowWithColor:) withObject:[UIColor colorWithWhite:0.1 alpha:0.4] afterDelay:0.46];
    //============================================================================
    
    //self.dayCells;
}
 
#pragma mark Layout
- (void) layoutSubviews 
{
    [super layoutSubviews];
    
    /// top = self.monthBar.frame.origin.y + self.monthBar.frame.size.height;

    [self _reload];
}

#pragma mark -
- (NSArray *)searchArrayUsingPredicateForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
    if(![self.arrayReturnedAfterPredicate count]) return nil;

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(self.startDate >= %@ AND self.startDate <= %@)", startDate, endDate];
    
    NSArray *arr=[self.arrayReturnedAfterPredicate filteredArrayUsingPredicate:predicate];
    NSLog(@"arr=%@",arr);
    
    //if([arr count])
    //[self.arrayReturnedAfterPredicate removeObjectsInArray:arr];
    
    return arr;
}

- (void)searchReminders:(NSMutableArray *)reminders andVisits:(NSMutableArray *)visits fromEvents:(NSArray *)events
{
    
    for(EKEvent *event in events)
    {
        NSLog(@"event.eventIdentifier=%@",event.eventIdentifier);
        NSLog(@"event.notes=%@",event.notes);
        NSString *strText;
        if([[event.notes componentsSeparatedByString:@"+"] count]==2)
        {
            strText=[[event.notes componentsSeparatedByString:@"+"] objectAtIndex:0];
        }
        else
        {
            strText=event.notes;
        }
        if([strText isEqualToString:@"Reminder"])
        {
            [reminders addObject:event];
        }
        else//Visit
        {
            [visits addObject:event];
        }
    }
    
    /*
    NSPredicate *reminderPredicate = [NSPredicate predicateWithFormat:@"self.notes CONTAINS[c] %@",@"Reminder"];
    NSArray *result= [events filteredArrayUsingPredicate:reminderPredicate];
    [reminders setArray:result];
    
    NSMutableArray *arr=[NSMutableArray arrayWithArray:events];
    [arr removeObjectsInArray:result];
    [visits setArray:arr];
     */

}

- (NSArray *)searchTaskArrayUsingPredicateForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
    if(![self.arrTask count]) return nil;
    
    NSLog(@"startDate=%@",startDate);
    NSLog(@"endDate=%@",endDate);
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(self.dueDate >= %@) AND (self.dueDate <= %@)", startDate, endDate];
    NSArray *arr=[self.arrTask filteredArrayUsingPredicate:predicate];
    [self.arrTask removeObjectsInArray:arr];
    
    return arr;
}

- (NSArray *)searchVisitArrayUsingPredicateForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
    if(![self.arrVisit count]) return nil;
    
   // NSLog(@"startDate=%@",startDate);
   // NSLog(@"endDate=%@",endDate);
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(self.visitStartDate >= %@) AND (self.visitStartDate <= %@)", startDate, endDate];
     
    NSArray *arr=[self.arrVisit filteredArrayUsingPredicate:predicate];
    [self.arrVisit removeObjectsInArray:arr];

    return arr ? arr :[NSArray array];
}


- (UIView *) monthBar 
{
    if (!_monthBar) 
    {
        _monthBar = [[[UIView alloc] init] autorelease];
        _monthBar.backgroundColor = [UIColor clearColor];
        _monthBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        
        
        SliderView *objSLide=[[SliderView alloc] initWithFrame:CGRectMake(93.5, 0, 905, 51)];
        objSLide.delegate=self;
        [_monthBar addSubview:objSLide];
        self.sliderView=objSLide;
        [objSLide release];
        
        [self addSubview: _monthBar];
    }
    return _monthBar;
}


- (UILabel *) monthLabel 
{
    return nil;
    UINavigationController *navController =(UINavigationController *) self.nextResponder.nextResponder;
    if (navController &&!_monthLabel) 
    {
        _monthLabel = [[[UILabel alloc] init] autorelease];
        _monthLabel.frame = CGRectMake(90, 0, 250.0, 30.0);
        _monthLabel.textColor = [UIColor whiteColor];
        _monthLabel.font = [UIFont subaruBoldFontOfSize:23.0];
        _monthLabel.shadowColor = [UIColor colorWithWhite:0.1 alpha:0.4];
        _monthLabel.shadowOffset = CGSizeMake(1.0, 2.0);
        
        //UIColor colorWithRed:25.0/255.0 green:91.0/255.0 blue:155.0/255.0 alpha:1.0];
        _monthLabel.textAlignment = UITextAlignmentCenter;
       // _monthLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _monthLabel.backgroundColor = [UIColor greenColor];
        //_monthLabel.hidden =YES;
       // [self.monthBar addSubview: _monthLabel];
        
        
        navController.navigationItem.titleView=_monthLabel;
        _monthLabel.hidden=YES;
    }
    
     _monthLabel.textAlignment = UITextAlignmentCenter;
    return _monthLabel;
}

- (UIButton *) monthBackButton 
{
    if (!_monthBackButton) 
    {
        UIImage *leftArowImage = [UIImage imageNamed:@"arrow_left_calendar.png"];
        _monthBackButton = [UIButton buttonWithType:UIButtonTypeCustom];//[[[UIButton alloc] init] autorelease];
        //[_monthBackButton setTitle: @"<" forState:UIControlStateNormal];
        

        [_monthBackButton setImage:leftArowImage forState:UIControlStateNormal];


        _monthBackButton.titleLabel.font = [UIFont systemFontOfSize: [UIFont buttonFontSize]];
        _monthBackButton.titleLabel.textColor = LOGIN_BG_COLOR;
        [_monthBackButton addTarget: self
                             action: @selector(monthBack)
                   forControlEvents: UIControlEventTouchUpInside];
        [self.monthBar addSubview: _monthBackButton];
    }
    return _monthBackButton;
}

- (UIButton *) monthForwardButton 
{
    if (!_monthForwardButton) 
    {
        _monthForwardButton = [UIButton buttonWithType:UIButtonTypeCustom];//[[[UIButton alloc] init] autorelease];
        //[_monthForwardButton setTitle: @">" forState:UIControlStateNormal];
        

        [_monthForwardButton setImage:[UIImage imageNamed:@"arrow_right_calendar.png"] forState:UIControlStateNormal];

        _monthForwardButton.titleLabel.font = [UIFont systemFontOfSize: [UIFont buttonFontSize]];
        _monthForwardButton.titleLabel.textColor = LOGIN_BG_COLOR;
        [_monthForwardButton addTarget: self
                                action: @selector(monthForward)
                      forControlEvents: UIControlEventTouchUpInside];
        [self.monthBar addSubview: _monthForwardButton];
    }
    return _monthForwardButton;
}

- (UIButton *) todayButton 
{
    if (!_todayButton) 
    {
        UIImage *todayImage= [UIImage imageNamed:@"edit_blank.png"];
        _todayButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _todayButton.frame = CGRectMake(10.0, 0, todayImage.size.width, todayImage.size.height);
        //[_monthForwardButton setTitle: @">" forState:UIControlStateNormal];
        [_todayButton setBackgroundImage:todayImage forState:UIControlStateNormal];
        [_todayButton setTitle:@"Today" forState:UIControlStateNormal];
        
        _todayButton.titleLabel.font = [UIFont subaruBoldFontOfSize:15.0];
        _todayButton.titleLabel.shadowColor = [UIColor colorWithWhite:0.1 alpha:0.5];
        _todayButton.titleLabel.shadowOffset = CGSizeMake(1.0, 1.0);
        
        [_todayButton addTarget: self
                         action:@selector(todayButtonAction:)
                      forControlEvents: UIControlEventTouchUpInside];
    
        [self.monthBar addSubview: _todayButton];
    }
    
    _todayButton.titleLabel.textColor = [UIColor whiteColor];
    _todayButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    return _todayButton;
}

#pragma mark -

- (NSArray *) weekdayNameLabels 
{
    if (!_weekdayNameLabels) 
    {
        NSMutableArray *labels = [NSMutableArray array];
        NSDateFormatter *dateFromatter = [[[NSDateFormatter alloc] init] autorelease];
        dateFromatter.calendar = self.calendar;
        for (NSUInteger i = self.calendar.firstWeekday; i < self.calendar.firstWeekday + 7; ++i) 
        {
            NSUInteger index = (i - 1) < 7 ? (i - 1) : ((i - 1) - 7);
            UILabel *label = [[UILabel alloc] initWithFrame: CGRectZero];
            label.backgroundColor=[UIColor clearColor];
            label.tag = i;
            label.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
            label.textAlignment = UITextAlignmentRight;
            NSString *weekdayName = [[dateFromatter shortWeekdaySymbols] objectAtIndex: index];
            label.text = NSLocalizedString(weekdayName, @"");
           // NSLog(@"label.text=%@",label.text);
            [labels addObject:label];
            //[_weekdayBar addSubview: label];
            //[self.gridView addSubview:label];
            [label release];
        }
       // [self addSubview:self.weekdayBar];
        _weekdayNameLabels = [[NSArray alloc] initWithArray:labels];
    }
    return _weekdayNameLabels;
}

- (UIView *) gridView 
{
    if (!_gridView) 
    {
        _gridView = [[[UIView alloc] init] autorelease];
        _gridView.frame = CGRectMake(kGridMargin, 0+kGridMargin,
                                         self.bounds.size.width - kGridMargin * 2,
                                         self.bounds.size.height - 0-kGridMargin-self.monthBarHeight);
        _gridView.clipsToBounds = YES;
        _gridView.layer.cornerRadius = 6.0;
        _gridView.layer.masksToBounds = YES;
        _gridView.layer.borderWidth = 0.6;
        _gridView.layer.borderColor = [UIColor colorWithWhite:0.2 alpha:0.35].CGColor;
        _gridView.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.09];
        [self addSubview: _gridView];
        
       
    }
    return _gridView;
}

- (NSArray *) dayCells 
{
    if (!_dayCells) 
    {
        CGFloat  cellHeight = (CGRectGetHeight(self.gridView.bounds)-4.0) / 5.0;
        CGFloat  cellWidth = (CGRectGetWidth(self.gridView.bounds)-6.0) / 7.0;
        
        NSMutableArray *cells = [NSMutableArray array];
        for (NSUInteger i = 1; i <= 42; ++i) 
        {
            CXCalendarCellView *cell = [[CXCalendarCellView alloc] init];
            
            cell.tag = i;
           // cell.day = i;
            cell.userInteractionEnabled=YES;
            cell.selected = NO;
            //===================
            cell.frame = CGRectMake((cellWidth+1.0) * (i % 7), (cellHeight+1.0) * (i / 7),cellWidth, cellHeight);
            cell.backgroundColor=[UIColor whiteColor];
            cell.lblDate.text=[NSString stringWithFormat:@" %d",i];
            cell.btnReminder.hidden = YES;
            cell.btnTask.hidden = YES;
            cell.btnVisit.hidden = YES;
            //=================
            [cells addObject:cell];
            
                   
            [self.gridView addSubview: cell];
            [cell release];
        }
        _dayCells = [[NSArray alloc] initWithArray:cells];
    }
    return _dayCells;
}

#pragma mark -


#pragma mark -
#pragma mark TodayButtonAction
-(void)todayButtonAction:(UIButton *)sender
{
    [self.sliderView selectToday];
}
#pragma mark


-(void)selectCellForEvent:(id)event
{
    //[self.sliderView selectDate:event.startDate];
    
    NSDate *_startDate = nil;
    if([event isKindOfClass:[Task class]])
    {
        _startDate = [event performSelector:@selector(dueDate)];
    }
    else if([event isKindOfClass:[ScheduledVisit class]])
    {
            _startDate = [event performSelector:@selector(visitStartDate)];
    }
    else if([event isKindOfClass:[Reminder class]])
    {
        _startDate = [event performSelector:@selector(startDate)];
    }
       
    //CXCalendarCellView *selectedCalCell = [self _cellForDate:_startDate];
    CXCalendarCellView *selectedCalCell = [self calenderCellForDate:_startDate];
    if(selectedCalCell)
    {
        [selectedCalCell selectEvent:event];
    }
    else 
    {
        [self.sliderView selectDate:_startDate];
        [self performSelector:@selector(selectCellForEvent:) withObject:event afterDelay:0.1];
    }
}


-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    //resign searchBar
}


-(void)fetchItemsFromStartDate:(NSDate *)startDate endDate:(NSDate *)endDate
{
    [[SSCoreDataManager sharedManager] save:nil];
    
    NSLog(@"startDate=%@ endDate=%@",startDate,endDate);
    [self fetchScheduledVisitsForStartDate:startDate endDate:endDate];
    [self fetchTasksForStartDate:startDate endDate:endDate];
    [self fetchRemindersForStartDate:startDate endDate:endDate];
}

-(void)calendarDidLoad
{
    if([self.calDeligate respondsToSelector:@selector(calenderDidLoad)])
       [self.calDeligate calenderDidLoad];
}

-(void)fetchScheduledVisitsForStartDate:(NSDate *)startDate endDate:(NSDate *)endDate
{
    if(selectedTabIndex==0||selectedTabIndex==2)
    {
        
    NSPredicate *filterPredicate=[NSPredicate predicateWithFormat:@"(self.visitStartDate>=%@ AND self.visitStartDate<=%@) && (self.visitStartDate != nil)",startDate,endDate];
    
    

     [ScheduledVisit fetchAsynWithPredicate:filterPredicate onAttributes:nil sortDescriptors:nil limit:0 startIndex:0 resultType:NSManagedObjectResultType completion:^(NSArray *result, NSError *error)
     {
     
     NSLog(@"ScheduledVisit Count:%d",[result count]);
     if([result count])
     {
        [self performSelectorOnMainThread:@selector(addEvents:) withObject:result waitUntilDone:NO];
         
        [self performSelectorOnMainThread:@selector(calendarDidLoad) withObject:nil waitUntilDone:NO];
         
     }
     
     }];
     


    }
    
    /*
    NSArray *result= [ScheduledVisit fetchWithPredicate:filterPredicate error:nil];
    
    if((selectedTabIndex==0||selectedTabIndex==2) && [result count])
    {
        for(ScheduledVisit *event in result)
        {
            CXCalendarCellView *calCell =  [self calenderCellForDate:event.visitStartDate];
            [calCell addVisit:event];
        }
    }
    */
}

-(void)fetchTasksForStartDate:(NSDate *)startDate endDate:(NSDate *)endDate
{
    if(selectedTabIndex==0||selectedTabIndex==3)
    {
       
        NSLog(@"start date=%@ EndDate=%@",startDate,endDate);
        
    NSPredicate *filterPredicate=[NSPredicate predicateWithFormat:@"(self.dueDate>=%@ AND self.dueDate<=%@) &&  (self.dueDate != nil)",startDate,endDate];
    
     [Task fetchAsynWithPredicate:filterPredicate onAttributes:nil sortDescriptors:nil limit:0 startIndex:0 resultType:NSManagedObjectResultType completion:^(NSArray *result, NSError *error)
      {
     
          NSLog(@"Task Count:%d",[result count]);
          
     if([result count])
     {
                  
         [self performSelectorOnMainThread:@selector(addEvents:) withObject:result waitUntilDone:NO];
     }
     
     }];
    }
    
    
}

-(void)fetchRemindersForStartDate:(NSDate *)startDate endDate:(NSDate *)endDate
{
 
  if((selectedTabIndex==0||selectedTabIndex==1))
   {
       
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"(self.startDate>=%@ AND self.startDate<=%@) &&  (self.startDate != nil)",startDate,endDate];
       
    [Reminder fetchAsynWithPredicate:filterPredicate onAttributes:nil sortDescriptors:nil limit:0 startIndex:0 resultType:NSManagedObjectResultType completion:^(NSArray *result, NSError *error)
     {
         if([result count])
         {
             [self performSelectorOnMainThread:@selector(addEvents:) withObject:result waitUntilDone:NO];
             
            // if([self.calDeligate respondsToSelector:@selector(remindersDidLoad)])
            //     [self.calDeligate remindersDidLoad];
         }
     }];
       
     }

}

-(void)resetVisitCells
{
    for(CXCalendarCellView *cell in self.dayCells)
    {
        cell.visitArrayList = nil;
    }
}

-(void)resetTaskCells
{
    for(CXCalendarCellView *cell in self.dayCells)
    {
        cell.taskArrayList = nil;
    }
}

-(void)resetReminderCells
{
    for(CXCalendarCellView *cell in self.dayCells)
    {
        cell.reminderArrayList = nil;
    }
}

-(void)addEvents:(NSArray *)events
{
    for(id event in events)
    {
        int type = 0;
        
        NSDate *_date=nil;
        if([event isKindOfClass:[Task class]])
        {
            Task *_event = (Task *)event;
            _date=_event.dueDate;
            type=1;
        }
        else if([event isKindOfClass:[ScheduledVisit class]])
        {
            ScheduledVisit *_event = (ScheduledVisit *)event;
            _date=_event.visitStartDate;
            type=2;
        }
        else if([event isKindOfClass:[Reminder class]])
        {
            Reminder *_event = (Reminder *)event;
            _date=_event.startDate;
            type=3;
        }

        
        CXCalendarCellView *calCell =  [self calenderCellForDate:_date];
        //  NSLog(@"date=%@ cell=%d m=%d",event.dueDate,calCell.day,calCell.month);
        
        switch (type) 
        {
            case 1:
                [calCell addTask:(Task *)event];
            break;
            case 2:
                 [calCell addVisit:(ScheduledVisit *)event];
            break;
            case 3:
                [calCell addReminder:(Reminder *)event];
            break;
                
            default:
                break;
        }
        
    }

}

-(void)addNewTask:(Task *)task
{
    if(!task) return;
    
    CXCalendarCellView *calCell =  [self calenderCellForDate:task.dueDate];
    if(calCell)
    {
        [calCell addTask:task];
    }
}




-(void)addNewScheduledVisit:(ScheduledVisit *)visit
{
    if(!visit) return;
    
    CXCalendarCellView *calCell =  [self calenderCellForDate:visit.visitStartDate];
    if(calCell)
    {
        [calCell addVisit:visit];
    }
}

-(void)addNewReminder:(Reminder *)reminder
{
    
    CXCalendarCellView *calCell = [self calenderCellForDate:reminder.startDate] ;
    
    if(calCell)
    {
        [calCell addReminder:reminder];
    }

}

-(void)removeTask:(Task *)task
{
    if(!task) return;
    
    CXCalendarCellView *calCell =  [self calenderCellForDate:task.dueDate];
    
    if(calCell)
    {
        [calCell removeTask:task];
    }
    
}

-(void)removeScheduledVisit:(ScheduledVisit *)visit
{
    if(!visit) return;
    
    CXCalendarCellView *calCell =  [self calenderCellForDate:visit.visitStartDate];
    if(calCell)
    {
        [calCell removeVisit:visit];
    }
}

-(void)removeReminder:(Reminder *)reminder
{
    if(!reminder) return;
    
   CXCalendarCellView *calCell = [self calenderCellForDate:reminder.startDate];
    
    if(calCell)
    {
        [calCell removeReminder:reminder];
    }
}



-(void)editReminder:(Reminder *)reminder lastStartDate:(NSDate *)date
{
   CXCalendarCellView *calCell = [self calenderCellForDate:reminder.startDate];
    
   CXCalendarCellView *lastCell =   [self calenderCellForDate:date];
    
    
     if(lastCell && (lastCell.day!=calCell.day || lastCell.month!=calCell.month))
    {
        [lastCell removeReminder:reminder];
    }
    
    if(calCell)
    {
        NSLog(@"cell.day = %d cell.month= %d",calCell.day,calCell.month);
        [calCell addReminder:reminder];
    }

}




#pragma mark -
#pragma mark UNDER DEVELOPMENT
-(void)editVisit:(ScheduledVisit *)visit lastStartDate:(NSDate *)date
{
    if(!visit) return;
    
    CXCalendarCellView *calCell  = [self calenderCellForDate:visit.visitStartDate];

    CXCalendarCellView *lastCell =  [self calenderCellForDate:date];
    
    if(calCell)
    {
        [calCell addVisit:visit];
    }
    
    if(lastCell && (lastCell.day!=calCell.day || lastCell.month!=calCell.month))
    {
        [lastCell removeVisit:visit];
    }
}

-(void)editTask:(Task *)task lastStartDate:(NSDate *)date
{
    if(!task) return;
    
    
    CXCalendarCellView *calCell  = [self calenderCellForDate:task.dueDate];
    //CXCalendarCellView *lastCell = [task.dueDate isEqualToDate:date]  ? nil :  [self calenderCellForDate:date];
    
     CXCalendarCellView *lastCell =  [self calenderCellForDate:date];
    
    if(calCell)
    {
        [calCell addTask:task];
    }
    
   // if(lastCell && (lastCell.day!=calCell.day || lastCell.month!=calCell.month))
    if(lastCell && (lastCell.day!=calCell.day))
    {
        [lastCell removeTask:task];
    }
}


/*
 -(void)editVisit:(ScheduledVisit *)visit
 {
 for(CXCalendarCellView *calCell in self.dayCells)
 {
 calCell.visitArrayList = nil;
 }
 
 
 [self fetchScheduledVisitsForStartDate:self.currentStartDate endDate:self.currentEndDate];
 }
 -(void)editReminder:(Reminder *)reminder
 {
 //if(!reminder) return;
 
 // NSDate *_endDate =[reminder.startDate dateByAddingTimeInterval:reminder.recurrenceRule.recurrenceEnd.occurrenceCount *60*60*24];
 //  NSArray *cells=[self calendarCellsFromDate:reminder.startDate endDate:_endDate];
 //   NSArray *cells=[self calendarCellsFromDate:self.currentStartDate endDate:self.currentEndDate];
 
 for(CXCalendarCellView *calCell in self.dayCells)
 {
 calCell.reminderArrayList = nil;
 }
 
 // [self fetchRemindersForStartDate:reminder.startDate endDate:_endDate];
 [self fetchRemindersForStartDate:self.currentStartDate endDate:self.currentEndDate];
 }
 
-(void)editTask:(Task *)task
{
    for(CXCalendarCellView *calCell in self.dayCells)
    {
        calCell.taskArrayList = nil;
    }
    
    
    [self fetchTasksForStartDate:self.currentStartDate endDate:self.currentEndDate];
}
*/


#pragma mark -

-(NSArray *)calendarCellsFromDate:(NSDate *)sDate endDate:(NSDate *)eDate
{
    unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    
    NSDateComponents* sParts = [[NSCalendar currentCalendar] components:flags fromDate:sDate];
    NSDateComponents* eParts = [[NSCalendar currentCalendar] components:flags fromDate:eDate];
    NSLog(@"day=%d month =%d  to day=%d month=%d",sParts.day,sParts.month,eParts.day,eParts.month);
    
    NSInteger day1 = MIN(sParts.day, eParts.day);
    NSInteger day2 = MAX(sParts.day, eParts.day);
    
    NSInteger month1 = MIN(sParts.month, eParts.month);
    NSInteger month2 = MAX(sParts.month, eParts.month);
    
   /// NSDateComponents* minParts = MIN(sParts, eParts);
   // NSDateComponents* maxParts = MAX(sParts, eParts);
    
    /*
     NSMutableArray *dates = [NSMutableArray array];
     NSDate *curDate = sDate;
     while([curDate timeIntervalSince1970] <= [eDate timeIntervalSince1970]) 
     {
     [dates addObject:curDate];
     curDate = [NSDate dateWithTimeInterval:60*60*24 sinceDate:curDate];
     }
     */
    
    NSArray *_cells= [self.dayCells filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(self.day>=%d AND self.month=%d) AND ( self.day<=%d AND self.month=%d)",day1,month1,day2, month2]];
    
    //  NSArray *_cells= [self.dayCells filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(self.month>=%d AND self.day>=%d) AND (self.month<=%d AND  self.day<=%d)",minParts.month,minParts.day,maxParts.month, maxParts.day]];
    
    return _cells;
}

-(void)checkAndUpdateToday
{
    KalDate *_date = [KalDate dateForDay:self.todayCell.day month:self.displayedMonth year:self.displayedYear];
    
    BOOL _isToday = [_date isToday];
    
    if(!_isToday) //self.todayCell
    {
        CXCalendarCellView *_todayCell = [self calenderCellForDate:[NSDate date]];
        
        
        [UIView animateWithDuration:0.3 animations:^{

            [_todayCell setTodayCell:YES];
            [self.todayCell setTodayCell:NO];
            
        }
        completion:^(BOOL finished)
        {
            self.todayCell = _todayCell;
            
        }];
    }
 }

@end
