//
//  Start_EndDate.h
//  OEM_CALENDAR
//
//  Created by elie maalouly on 5/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol Start_EndDateDelegate <NSObject>
@required
- (void)selectedStartDate:(NSString *)strStart endDate:(NSString *)strEnd;
@end

typedef void (^DoneActionCallback) (NSDate *startDate,NSDate *endDate);
typedef void (^DateDidChangeCallback)(NSDate *startDate,NSDate *endDate);

@interface Start_EndDate : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    UITableViewCell *TableCell;
    UIDatePicker *datePicker;
    int _selectedTableRow;
    id <Start_EndDateDelegate> _delegate;
    BOOL wrongSelection;
}
@property(nonatomic,retain) UITableView *aTableview;

@property(nonatomic,retain) NSDate *selectedStartDate;
@property(nonatomic,retain) NSDate *selectedEndDate;
@property(nonatomic,assign) BOOL isFromCustomer;
@property(nonatomic, assign) id <Start_EndDateDelegate> delegate;

- (id)initWithStartDate:(NSDate *)aDate endDate:(NSDate *)aDate;

-(void)setupTableFooterView;

-(void)addDoneActionCallback:(DoneActionCallback )callback;
-(void)addDateDidChangeCallback:(DateDidChangeCallback)callback;

@end
