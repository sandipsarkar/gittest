//
//  FilterCalenderViewController.h
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^FilterDidSelectCallback) (NSInteger index);
@interface FilterCalenderViewController : UITableViewController

- (id)initWithSelectedIndex:(NSInteger)index;
-(void)addDidSelectCallback:(FilterDidSelectCallback )callback;

@end
