               //
//  CalendarViewController.m
//  OEM_CALENDAR
//
//  Created by elie maalouly on 4/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CalendarViewController.h"
#import "Customer.h"
#import "AddNewEventController.h"
#import "UISearchBar+BackgroundImage.h"
#import "FilterCalenderViewController.h"
#import "CalenderSearchResultsController.h"
#import "Task.h"
#import "ScheduledVisit.h"
#import "CreateTaskViewController.h"
#import "Rep.h"
#import "Reminder.h"
#import "OfflineManager.h"
   
//#import "CXCalendarCellView.h"

#define CALENDER_BG_IMAGE @"calenderLaunch.png"

@interface CalendarViewController()<UISearchBarDelegate,UITextFieldDelegate>
{
    UIButton *btnADDVisit ;
    UIButton *btnADDReminder ;
    
    UIPopoverController *newEventPopover;
    
    UISearchBar *searchBar;
    BOOL _isFirstTime;
    
    UIView *popoverLayerView;
    
    BOOL isFetchingFromServer;
}
@property (nonatomic,copy)    CalenderDidLoadCallback calenderDidLoadCallback;
@property (nonatomic,retain)  UIPopoverController *filterPopover;
@property (nonatomic,retain)  UIPopoverController *searchPopover;
@property (nonatomic,retain)  EKCalendar *calender;
@property (nonatomic,retain)  id selectedSearchedResult;
@property (nonatomic,retain)  ASIHTTPRequest *currentReminderRequest;

-(void)addReminder:(id)sender;
-(void)addVisit:(id)sender;

-(DidSelectCallback)didSelectBlockForNavigationController:(UINavigationController *)navController;

@end

@implementation CalendarViewController
@synthesize calendarView;
@synthesize referance;
@synthesize selectedTabIndex;
@synthesize calenderDidLoadCallback;
@synthesize filterPopover,searchPopover;
@synthesize calender;
@synthesize selectedSearchedResult;
@synthesize currentReminderRequest;

//@synthesize popOverCalendarCell;

- (id)init
{
    self = [super init];
    if (self) 
    {
        selectedTabIndex=0;
        
        _initialInterfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
    }
    return self;
}

-(void)addCalenderDidLoadCallbak:(CalenderDidLoadCallback)callback
{
    if(callback)
    self.calenderDidLoadCallback = callback;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


-(void)setupInfoSliderView
{
    if(!infoSliderView)
    {
        UIImage *infoImage = [UIImage imageNamed:@"info_bar.png"];
       
        infoSliderView=[[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.bounds)-infoImage.size.width, CGRectGetHeight(self.view.bounds)-infoImage.size.height, infoImage.size.width, infoImage.size.height)];
        infoSliderView.image= infoImage;
        infoSliderView.userInteractionEnabled =YES;
        infoSliderView.autoresizingMask =  UIViewAutoresizingFlexibleTopMargin |UIViewAutoresizingFlexibleLeftMargin ;
        infoSliderView.contentMode = UIViewContentModeScaleAspectFit;
        infoSliderView.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.6];
        [self.view addSubview:infoSliderView];
        
        UIButton *arrowButton=[UIButton buttonWithType:UIButtonTypeCustom];
        arrowButton.frame = CGRectMake(0, 11.5, 27.0, 33.0);
        [arrowButton setBackgroundImage:[UIImage imageNamed:@"open_arrow.png"] forState:UIControlStateNormal];
        [arrowButton setBackgroundImage:[UIImage imageNamed:@"open_arrow.png"] forState:UIControlStateHighlighted];
        [arrowButton setBackgroundImage:[UIImage imageNamed:@"close_arrow.png"] forState:UIControlStateSelected];
        [arrowButton addTarget:self action:@selector(revealCloseAction:) forControlEvents:UIControlEventTouchDown];

        [infoSliderView addSubview:arrowButton];
    }
    
    CGRect initialFrame=infoSliderView.frame;
    initialFrame.origin.x=CGRectGetWidth(self.view.bounds)-30.0;
    infoSliderView.frame=initialFrame;

    /*
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(infoSliderViewTapAction:)];
    tapGesture.numberOfTapsRequired=1;
    [infoSliderView addGestureRecognizer:tapGesture];
    [tapGesture release];
    */
}

-(void)revealCloseAction:(UIButton *)sender
{
    BOOL isOpened=sender.selected;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect initialFrame   = infoSliderView.frame;
        initialFrame.origin.x = isOpened? CGRectGetWidth(self.view.bounds)-30.0 : CGRectGetWidth(self.view.bounds)-256.0;
        infoSliderView.frame  = initialFrame;
    } 
    completion:^(BOOL finished) 
    {
         if(finished)
         {
           sender.selected=!isOpened;
           [sender setBackgroundImage:[UIImage imageNamed:isOpened?@"open_arrow.png":@"close_arrow.png"] forState:UIControlStateHighlighted];
         }
    }];
}

/*
-(void)infoSliderViewTapAction:(UITapGestureRecognizer *)recognizer
{
    recognizer.enabled=NO;
    
    for(UIGestureRecognizer *_recognizer in recognizer.view.gestureRecognizers)
    {
        if([_recognizer isKindOfClass:[UISwipeGestureRecognizer class]])
        {
            _recognizer.enabled=YES;
            break;
        }
    }
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect initialFrame   = infoSliderView.frame;
        initialFrame.origin.x = 0;
        infoSliderView.frame  = initialFrame;
        
    }];
}

-(void)infoSliderViewSwipeAction:(UISwipeGestureRecognizer *)recognizer
{
    recognizer.enabled=NO;
    for(UIGestureRecognizer *_recognizer in recognizer.view.gestureRecognizers)
    {
        if([_recognizer isKindOfClass:[UITapGestureRecognizer class]])
        {
            _recognizer.enabled=YES;
            break;
        }
    }

    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect initialFrame    = infoSliderView.frame;
         initialFrame.origin.x = CGRectGetWidth(self.view.bounds)-40.0;
        infoSliderView.frame   =  initialFrame;
        
    }];
    
}
*/

- (void)setupWeekdayBar 
{
  
      CGFloat _weekBarHeight = 44.0;
       UIView  *_weekdayBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds),_weekBarHeight)];
        _weekdayBar.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        _weekdayBar.backgroundColor = [UIColor clearColor];
    
    
        UIImage *bgImage=[UIImage imageNamed:@"view_tasks_side_bar.png"];
        UIImageView *imgv=[[UIImageView alloc] initWithImage:bgImage]; //@"month_bg.png"
        imgv.frame = CGRectMake(120.0, 0, CGRectGetWidth(self.view.bounds)-120.0, CGRectGetHeight(_weekdayBar.bounds));
        imgv.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [_weekdayBar addSubview:imgv];
      
        [imgv release];
        ///======= change ===================
        float _x=0.0;
        UIImage *filterButImage=[UIImage imageNamed:@"segmented_button.png"];
        btnFilter=[UIButton buttonWithType:UIButtonTypeCustom];
        //btnFilter.frame =CGRectMake(_x, 0, filterButImage.size.width, _weekBarHeight);
         btnFilter.frame =CGRectMake(_x, 0,126.0, _weekBarHeight);
        btnFilter.titleLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
        [btnFilter setTitle:@"Filter" forState:UIControlStateNormal];
        [btnFilter setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnFilter setBackgroundImage:filterButImage forState:UIControlStateNormal];
         [btnFilter setBackgroundImage:[UIImage imageNamed:@"segmented_button_selected.png"] forState:UIControlStateHighlighted];
        [btnFilter setBackgroundImage:[UIImage imageNamed:FILTER_BUTTON_IMAGE] forState:UIControlStateSelected];
        btnFilter.backgroundColor=[UIColor clearColor];
        btnFilter.tag=10;
        [btnFilter addTarget:self action:@selector(filterAction:) forControlEvents:UIControlEventTouchUpInside];
        [_weekdayBar addSubview:btnFilter];
        
         btnFilter.selected = selectedTabIndex>0;
        ///================================= change ===================
        
    
        if(!searchBar)
        {
            searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.bounds)-312.0f, 0, 310.0, CGRectGetHeight(_weekdayBar.bounds))];
            searchBar.delegate = self;
            searchBar.backgroundColor = [UIColor clearColor];
             searchBar.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin ;
             searchBar.backgroundImage=[UIImage imageNamed:@"view_tasks_side_bar.png"]; // @"calendar_search_bar.png"
            searchBar.placeholder = @"Search...";
            
            [_weekdayBar addSubview:searchBar];
            UITextField *innerTextField = [searchBar textField];
            innerTextField.delegate = self;
            
            [searchBar hideDefaultBackground];
            
            //[searchBar s  etPositionAdjustment:UIOffsetMake(0, 0) forSearchBarIcon:UISearchBarIconSearch];
            //[searchBar setSearchTextPositionAdjustment:UIOffsetMake(-270, 0)];
        }
      
        
    [self.view addSubview:_weekdayBar];
    [_weekdayBar release];
}

-(void)setupCalendarView
{
   /*
    NSString *_calenderIdentifier = [[NSUserDefaults standardUserDefaults] objectForKey:CALENDER_IDENTIFIER_KEY];
    self.calender = [[CustomEventStore sharedInstance] customCalendarWithIdentifier:_calenderIdentifier title:@"com.reptool.calender"];
    if(!_calenderIdentifier)
    {
        _calenderIdentifier  = calender.calendarIdentifier;
        [[NSUserDefaults standardUserDefaults] setObject:_calenderIdentifier forKey:CALENDER_IDENTIFIER_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    */

    
    self.calender = [[CustomEventStore sharedInstance].eventStore defaultCalendarForNewEvents]; 

    
    CGRect calendarFrame = CGRectMake(0, 44.0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-44.0);
    calendarView = [[CXCalendarView alloc] initWithFrame: calendarFrame selectedTabIndex:self.selectedTabIndex];
    calendarView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:1.0];
    
    calendarView.alpha = 0.0;
    //calendarView.hidden=YES;
 
    calendarView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    self.referance        = self ;
    calendarView.delegate = self ;
    calendarView.calDeligate = self.referance ;
    
    self.calendarView.selectedDate = [NSDate date];
    
    [self.view addSubview: calendarView];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.calendarView.alpha = 0.0;
        self.calendarView.alpha = 1.0;
    }];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
	app.inCalender=1;
    _isFirstTime = YES;

    
    [self.navigationController.navigationBar addBackGroundImage:@"navigation_bar.png"];
    [self addBackButtonWithSelector:@selector(backButtonAction:)];
    [self addTitle:@"CALENDER" withFont:[UIFont subaruBoldFontOfSize:23.0]]; 
    
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(visitSessionDidUpdate:) name:VisitReviewDidUpdate object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onSignificantTimeChange:)
                                                 name:UIApplicationSignificantTimeChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newTaskAdded:) name:TaskDidAddEditNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(taskDidEdit:) name:TaskDidEditNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(taskDidDelete:) name:TaskDidDeleteNotification object:nil];
    
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventStoreChanged:) name:EKEventStoreChangedNotification object:[CustomEventStore sharedInstance].eventStore];
    
    selectedButton=nil;
    
    /*
    UIImage *bgImage = [UIImage imageNamed:CALENDER_BG_IMAGE];
    UIImageView *bgImageView = [[UIImageView alloc] initWithImage:bgImage];
    bgImageView.frame = self.view.bounds;
    bgImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:bgImageView];
    [bgImageView release];
    */
    
    
    UIImage *addNewImage = [UIImage imageNamed:@"add_calendar_button.png"];
    addNewButton = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    addNewButton.frame =CGRectMake(0.0, 0.0, addNewImage.size.width, addNewImage.size.height);
    [addNewButton setBackgroundImage:addNewImage forState:UIControlStateNormal];
    [addNewButton addTarget:self action:@selector(addNewEventAction:) forControlEvents:UIControlEventTouchUpInside];
   
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]initWithCustomView:addNewButton];
	self.navigationItem.rightBarButtonItem = rightBarButton;
	[rightBarButton release];

    
    self.view.backgroundColor = [UIColor whiteColor];
     
    [self setupCalendarView];
    [self setupWeekdayBar];
    
    loader =[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    loader.center = CGPointMake(self.view.center.x+loader.frame.size.width/2, self.view.center.y-loader.frame.size.height/2);
    loader.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    
    [self.view addSubview:loader];
    loader.hidesWhenStopped = YES;
    [loader startAnimating];
    
    /*
    [[CustomEventStore sharedInstance].eventStore  requestAccessToEntityType:EKEntityType
                                completion:^(BOOL granted, NSError *error) {
                                    if (!granted)
                                        NSLog(@"Access to store not granted");
                                }]; 
     */

    

    [self fetchRemindersFromServer];

    
    
   
    
}

-(void)taskDidEdit:(NSNotification *)notif
{
     Task *task = [notif object];
    
   // [self.calendarView resetTaskCells];
   // [self.calendarView fetchTasksForStartDate:self.calendarView.currentStartDate endDate:self.calendarView.currentEndDate];
    
    NSDictionary *dict = [notif userInfo];
    NSLog(@"Due date: %@", [dict valueForKey:@"lastDueDate"]);
    NSDate *lastDueDate = [dict valueForKey:@"lastDueDate"];
    
    if(lastDueDate)
    {
        [self.calendarView editTask:task lastStartDate:lastDueDate];
    }
    
}
-(void)newTaskAdded:(NSNotification *)notif
{
    Task *task = [notif object];
    
    if(!task) return;
    
    CXCalendarCellView *cell= [self.calendarView calenderCellForDate:task.dueDate];
    if(cell)
    {
        [cell addTask:task];
    }

    
    //  [self.calendarView resetTaskCells];
    //  [self.calendarView fetchTasksForStartDate:self.calendarView.currentStartDate endDate:self.calendarView.currentEndDate]
}

-(void)taskDidDelete:(NSNotification *)notif
{
    Task *task = [notif object];
    
    if(!task) return;
    
    CXCalendarCellView *cell= [self.calendarView calenderCellForDate:task.dueDate];
    
    if(cell)
    {
        [cell removeTask:task];
    }

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    /*
      //self.calendarView.alpha = 1.0;
      // self.calendarView.selectedDate = [NSDate date];
      //[self.calendarView reload];
      [loader stopAnimating];
      calendarView.hidden=NO;
  
      [UIView animateWithDuration:0.3 animations:^{
        
         self.calendarView.alpha = 0.0;
         self.calendarView.alpha = 1.0;
       
      }];
    
      // [self.calendarView calendarCellsFromDate:[NSDate date] endDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*20]];
    */
    
}

-(void)visitSessionDidUpdate:(NSNotification *)notif
{
    NSDictionary *userinfo = notif.userInfo;
    NSString *action       = [userinfo valueForKey:@"action"];
    
  //  ScheduledVisit *scheduledVisit = [userinfo valueForKey:@"scheduledVisit"];
    
    if([action isEqualToString:@"delete"])
    {
      
        {
            //========= THIS LOGIC SHOULD BE OPTIMIZED ===========//
            
            [self.calendarView resetVisitCells];
            
            
            [[SSCoreDataManager sharedManager] save:nil];
            
            [self.calendarView fetchScheduledVisitsForStartDate:self.calendarView.currentStartDate endDate:self.calendarView.currentEndDate];
        
            
          //  [self.calendarView resetTaskCells];
          //  [self.calendarView fetchTasksForStartDate:self.calendarView.currentStartDate endDate:self.calendarView.currentEndDate];
            
            /*
            CXCalendarCellView *cell= [self.calendarView calenderCellForDate:scheduledVisit.visitStartDate];
            if(cell)
            {
                [cell removeVisit:scheduledVisit];
            }
            */
        }
      
    }
}

-(void)backButtonAction:(id)sender
{
    @try 
    {
    [sender setEnabled:NO];
    if(newEventPopover!=nil && [newEventPopover isPopoverVisible]) [newEventPopover dismissPopoverAnimated:NO];
        
    }
    @catch (NSException *exception) 
    {
        
    }
    @finally 
    {
       
         [self dismissModalViewControllerAnimated:YES];
    }

    
    //[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark FILTER ACTION
-(void)filterAction:(UIButton *)sender
{
    
    __block CalendarViewController *weakSelf = self;
    __block CXCalendarView *calendarViewWeak = weakSelf.calendarView;
    
    UIPopoverController *popover = self.filterPopover;
    if(!popover)
    {
        FilterCalenderViewController *filterController = [[FilterCalenderViewController alloc ] initWithSelectedIndex:selectedTabIndex];
        // UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:filterController];
        popover = [[UIPopoverController alloc] initWithContentViewController:filterController];
        self.filterPopover = popover;
        filterController.popoverController = popover;
        [popover release];
        
        
        [filterController addDidSelectCallback:^(NSInteger index) 
         {
             sender.selected=(index!=0);
             
             [weakSelf.filterPopover dismissPopoverAnimated:YES];
             
             calendarViewWeak.selectedTabIndex = index;
             weakSelf.selectedTabIndex = index;
          
             
          if(weakSelf.searchPopover)
          {
             UINavigationController *navController=(UINavigationController *)weakSelf.searchPopover.contentViewController;
             
             CalenderSearchResultsController  *calenderSearchResultsController =(CalenderSearchResultsController *) [[navController viewControllers] objectAtIndex:0];
              calenderSearchResultsController.selectedFilterIndex = index;
          }
            
             
         }];    
        
        
        [filterController release];
        //[navController release];
    }
  
    
    self.filterPopover.popoverContentSize = CGSizeMake(320.0, 200.0);
    [self.filterPopover presentPopoverFromRect:[sender frame] inView:[sender superview] permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}
#pragma mark -

-(void)addNewEventAction:(UIButton *)sender
{
    if(searchBar)
    [searchBar resignFirstResponder];
    
    if(newEventPopover) 
    {
        if(newEventPopover.isPopoverVisible)
          [newEventPopover dismissPopoverAnimated:NO];
        
          [newEventPopover release];
          newEventPopover = nil;
    }
    
    
    AddNewEventController *addNewEventController = [[AddNewEventController alloc ] init];
   __block UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:addNewEventController];
    newEventPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
    newEventPopover.passthroughViews = [NSArray arrayWithObjects:self.navigationController.navigationBar,btnFilter,nil];
    addNewEventController.popoverController = newEventPopover;
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
  
   // [MBPopoverBackgroundView setBackgroundImageCapInsets:UIEdgeInsetsMake(12, 12, 12, 12)];
   // [MBPopoverBackgroundView setContentViewInsets:UIEdgeInsetsMake(2, 2, 2, 2)];
    
    newEventPopover.popoverBackgroundViewClass = [MBPopoverBackgroundView class];
   // CGRect rect = [sender frame];
    //rect.size.width = 1;
    //newEventPopover.popoverLayoutMargins = UIEdgeInsetsMake(rect.origin.x, 0, 0, 0);
}
    

    //navController.delegate = self;
    //newEventPopover.delegate = self;
  
   
    __block CalendarViewController *weakSelf = self;
    __block UIPopoverController *weakPopover = newEventPopover;
   
    [addNewEventController addDidSelectCallback:^(NSInteger index) 
    {
        //[newEventPopover dismissPopoverAnimated:YES];
        
        if(index == 0)
        {
            ADDReminder *mindit = [[ADDReminder alloc] init];
            mindit.delegate=self;
            
            [mindit addSaveReminderCallback:^(Reminder *reminder, int actionType, NSDate *lastStartDate)
             {
                 if(actionType==1)
                 {
                     [weakSelf.calendarView addNewReminder:reminder];
                 }
                 else if(actionType==2)
                 {
                     [weakSelf.calendarView editReminder:reminder];
                 }
                 
                 [addNewEventController addDidSelectCallback:nil];
                 
                 //[self.calendarView checkAndUpdateToday];
             }];

            [navController pushViewController:mindit animated:YES];
            [mindit release];
        }
        else if(index==1)
        {
            ADDVisit *_visit=[[ADDVisit alloc] init];
            
            _visit.delegate=self;
            [ _visit addSaveScheduledVisitCallback:^(ScheduledVisit *visit, int actionType, NSDate *lastStartDate)
            {
                

                switch (actionType) 
                {
                    case 1:
                    {
                        [weakSelf.calendarView addNewScheduledVisit:visit];
                    }
                    break;
                    case 2:
                    {
                        
                    }
                    break;
                    case 3:
                    {
                        [weakSelf.calendarView removeScheduledVisit:visit];
                    }
                    break;
                        
                    default:break;
                }

               // [self.calendarView checkAndUpdateToday];
                
                [addNewEventController addDidSelectCallback:nil];
                
            }];
            [navController pushViewController:_visit animated:YES];
            [_visit release];
        }   
        else
        {
            app.fromCustomerTask=2;
            CreateTaskViewController *_task=[[CreateTaskViewController alloc] init];
            //_task.delegate=self;
            _task.isFromCalnederTaskDatePicker=YES;
            _task.isInPopover = YES;
            //_task.title=@"Create New Task";
    
            _task.view.backgroundColor=[UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
            
            [_task addSaveTaskCallback:^(Task *task)
             {
                if(weakPopover.popoverVisible)
                    [weakPopover dismissPopoverAnimated:YES];
               

                 
        if(task) [weakSelf.calendarView addNewTask:task];         

                 //[self.calendarView checkAndUpdateToday];
                 
                 [addNewEventController addDidSelectCallback:nil];
            }];
            
            [_task addCancelTaskCallback:^{
                
                if(weakPopover.popoverVisible)
                    [weakPopover dismissPopoverAnimated:YES];
                
                [addNewEventController addDidSelectCallback:nil];

            }];
            

            [navController pushViewController:_task animated:YES];
            [_task release];
        }
                
    }];  
   
    
    //[addNewEventController addDidBackToMenuCallback:^{}];
        
    [addNewEventController release];
    [navController release];
    
    newEventPopover.popoverContentSize = CGSizeMake(335.0+15.0, 191.0);
    [newEventPopover presentPopoverFromRect:[sender frame] inView:[sender superview] permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}



- (void) calendarView: (CXCalendarView *) calendarView
        didSelectDate: (NSDate *) date 
{
    //NSLog(@"Selected date: %@", date);
    /*TTAlert([NSString stringWithFormat: @"Selected date: %@", date]);*/
}

-(void)calendarView:(CXCalendarView *)calendarView didChangeMonth:(NSString *)date
{

    [self addTitle:[date uppercaseString] withFont:[UIFont subaruBoldFontOfSize:23.0]];
}

-(void)addReminder:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    //[btn setImage:[UIImage imageNamed:@"add_new_reminder_button_selected.png"] forState:UIControlStateNormal];
    selectedButton=btn;
    
    ADDReminder *mindit = [[ADDReminder alloc] init];
    mindit.delegate = self;
    
    [mindit addSaveReminderCallback:^(Reminder *reminder, int actionType, NSDate *lastStartDate)
    {
        
        if(actionType==1)
            if(reminder) [self.calendarView addNewReminder:reminder];  
        
    }];

    
    UINavigationController *_navController= [[UINavigationController alloc] initWithRootViewController:mindit];
    
    
    if(popOverReminder==nil)
    {
        popOverReminder=[[UIPopoverController alloc] initWithContentViewController:_navController];
        popOverReminder.delegate=self;
    }
    else
    {
      popOverReminder.contentViewController=_navController;
    }
    
	popOverReminder.popoverContentSize=CGSizeMake(335,415);
	[popOverReminder presentPopoverFromRect:[sender frame] inView:[sender superview] permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	[mindit release];
    [_navController release];
}

-(void)addVisit:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    
   // [btn setImage:[UIImage imageNamed:@"schedule_a_visit_button_selected.png"] forState:UIControlStateNormal];
    selectedButton=btn;
    
    ADDVisit *_visit=[[ADDVisit alloc] init];
    _visit.delegate=self;
    [_visit addSaveScheduledVisitCallback:^(ScheduledVisit *visit, int actionType, NSDate *lastStartDate)
     {
         
         if(actionType==1)
         {

             
             if(visit) [self.calendarView addNewScheduledVisit:visit];         

         }
     }];

    
    UINavigationController *_navController= [[UINavigationController alloc] initWithRootViewController:_visit];
    
    if(popOverReminder==nil)
    {
        popOverReminder=[[UIPopoverController alloc] initWithContentViewController:_navController];
        popOverReminder.delegate=self;
    }
    else
    popOverReminder.contentViewController=_navController;
	popOverReminder.popoverContentSize=CGSizeMake(335,400);
    popOverReminder.delegate=self;
	[popOverReminder presentPopoverFromRect:[sender frame] inView:[sender superview] permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    [_visit release];
    [_navController release];
}

-(void)scheduleNewVisitForCustomer:(Customer *)customer
{
    UIButton *btn=(UIButton *)addNewButton;
    
    //[btn setImage:[UIImage imageNamed:@"schedule_a_visit_button_selected.png"] forState:UIControlStateNormal];
    selectedButton=btn;
    
    ADDVisit *_visit=[[ADDVisit alloc] initWithDealerName:customer.name suburb:customer.suburb];
    _visit.visitingCustomer = customer;
    _visit.delegate=self;
    UINavigationController *_navController= [[UINavigationController alloc] initWithRootViewController:_visit];
    
    
    [ _visit addSaveScheduledVisitCallback:^(ScheduledVisit *visit, int actionType, NSDate *lastStartDate) {
        

        switch (actionType)
        {
            case 1:
            {
                [self.calendarView addNewScheduledVisit:visit];
            }
                break;
            case 2:
            {
                
            }
                break;
            case 3:
            {
                [self.calendarView removeScheduledVisit:visit];
            }
                break;
                
            default:break;
        }

        // [self.calendarView checkAndUpdateToday];
        
    }];
 
    
    if(popOverReminder==nil)
    {
        popOverReminder=[[UIPopoverController alloc] initWithContentViewController:_navController];
        popOverReminder.delegate=self;
    }
    else
    popOverReminder.contentViewController=_navController;
    
	popOverReminder.popoverContentSize=CGSizeMake(335,430);
    popOverReminder.delegate=self;
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    popOverReminder.popoverBackgroundViewClass = [MBPopoverBackgroundView class];

    
	[popOverReminder presentPopoverFromRect:[btn frame] inView:[btn superview] permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    [_visit release];
    [_navController release];

}

-(void)selectScheduledVisit:(id)visit
{
    if(_isUpcommingVisitSelected) return;
    
    _isUpcommingVisitSelected = YES;
    
    if(!visit) return;
    
    [self performSelector:@selector(_selectVisit:) withObject:visit afterDelay:0.0];
}

-(void)_selectVisit:(id)visit
{
   // [self addCalenderDidLoadCallbak:nil];
    
    [self.calendarView selectCellForEvent:visit];
    
    /*
    return;
    CXCalendarCellView *selectedCalCell = [self.calendarView _cellForDate:visit.startDate];
    if(!selectedCalCell) return;
    [selectedCalCell selectVisit:visit];
     NSLog(@"Day:%d",selectedCalCell.day);
    */
}

/*
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    if([popoverController isEqual:newEventPopover])
    {
        
        UINavigationController *navController =(UINavigationController *) popoverController.contentViewController;
        AddNewEventController  *addNewEventController = [[navController viewControllers]  objectAtIndex:0];
        
        [addNewEventController addDidSelectCallback:nil];
        
    }

    return YES;
}
*/

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
        
    if(selectedButton!=nil)
    {
        if(selectedButton.tag==10)
        {
            //[selectedButton setImage:[UIImage imageNamed:@"add_new_reminder_button.png"] forState:UIControlStateNormal];
            
            selectedButton=nil;
        }
        else
        {
           // [selectedButton setImage:[UIImage imageNamed:@"schedule_a_visit_button.png"] forState:UIControlStateNormal];
            
            selectedButton=nil;
        }
    }
}

/*- (void)dismissPopOverFromCalenderTask:(BOOL)done
{
    
    if(newEventPopover.popoverVisible)
         [newEventPopover dismissPopoverAnimated:YES];
    if(done)
     [self.calendarView setNeedsLayout];
    
}*/

- (void)dismissPopOverFromViewVisit:(BOOL)done
{
    if(selectedButton!=nil)
    {
        [selectedButton setImage:[UIImage imageNamed:@"schedule_a_visit_button.png"] forState:UIControlStateNormal];
        selectedButton=nil;
    }
    
    if(popOverReminder.popoverVisible)
    [popOverReminder dismissPopoverAnimated:YES];
    
    if(newEventPopover) [newEventPopover dismissPopoverAnimated:YES];
    
    if(done)
    {
        

        
    }
}

-(void)dismissPopOverFromViewReminder:(BOOL)done
{
    if(selectedButton!=nil)
    {
        [selectedButton setImage:[UIImage imageNamed:@"add_new_reminder_button.png"] forState:UIControlStateNormal];
        selectedButton=nil;
    }
    if(popOverReminder.popoverVisible)
		[popOverReminder dismissPopoverAnimated:YES];
    
     if(newEventPopover) [newEventPopover dismissPopoverAnimated:YES];
    
   
}

#pragma mark FETCH THE TASK LIST FROM LOCAL-DB(CORE-DATA) AND RETURN TO CALENDAR
 - (NSArray *)taskListForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
    NSPredicate *filterPredicate=[NSPredicate predicateWithFormat:@"(self.dueDate>=%@ AND self.dueDate<=%@) &&  (self.dueDate != nil)",startDate,endDate];
    
 // NSMutableArray *tasks =  [CoreDataHandler allTasks];
//  NSArray *filteredTask =  [tasks filteredArrayUsingPredicate:filterPredicate];
    
    NSArray *filteredTask = [CoreDataHandler filteredTasksForPredicate:filterPredicate];
    
    NSLog(@"FilteredTasks:%d",[filteredTask count]);
    
    return filteredTask;
}

#pragma mark FETCH THE VISIT LIST FROM LOCAL-DB(CORE-DATA) AND RETURN TO CALENDAR
- (NSArray *)visitListForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
    NSPredicate *filterPredicate=[NSPredicate predicateWithFormat:@"(self.visitStartDate>=%@ AND self.visitStartDate<=%@) && (self.visitStartDate != nil)",startDate,endDate];
    
    //NSMutableArray *visits  =  [CoreDataHandler allScheduledVisits];
    //NSArray *filteredvisits =  [visits filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(self.visitStartDate>=%@ AND self.visitStartDate<=%@) && (self.visitStartDate != nil)",startDate,endDate]];
    
    NSArray *filteredvisits =  [CoreDataHandler filteredScheduledVisitsByPredicate:filterPredicate];
    
    return filteredvisits;
}


- (NSArray *)eventListForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
    //return [CustomEventStore getEventsFromDate:startDate toDate:endDate];
  
    NSArray *events= [CustomEventStore getEventsFromDate:startDate toDate:endDate fromCalender:self.calender];
    
    //NSLog(@"Events:%@",events);
    
    return events;
}

- (NSArray *)reminderListForStartDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
    NSPredicate *filterPredicate=[NSPredicate predicateWithFormat:@"(self.startDate>=%@ AND self.endDate<=%@) && (self.startDate != nil)",startDate,endDate];

    
    NSArray *filteredvisits =  [Reminder fetchWithPredicate:filterPredicate error:nil];
    
    return filteredvisits;
}

#pragma mark Search Bar Delegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)_searchBar
{
    if(searchPopover && _searchBar.text.length)
    {
        [searchPopover presentPopoverFromRect:[_searchBar frame] inView:[_searchBar superview] permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
    }
}
- (void)searchBar:(UISearchBar *)_searchBar textDidChange:(NSString *)searchText
{
    CalenderSearchResultsController *calenderSearchResultsController = nil;
    if(!searchPopover)
    {
        calenderSearchResultsController = [[CalenderSearchResultsController alloc] init];
        calenderSearchResultsController.calendar = self.calender;
        calenderSearchResultsController.selectedFilterIndex = selectedTabIndex;
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:calenderSearchResultsController];
        navController.navigationBar.barStyle  = UIBarStyleBlack;
        searchPopover = [[UIPopoverController alloc] initWithContentViewController:navController];
        searchPopover.delegate = self;
        searchPopover.passthroughViews = [NSArray arrayWithObject:_searchBar];
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        searchPopover.popoverBackgroundViewClass = [MBPopoverBackgroundView class];
   
        
      __block  CalendarViewController *weakSelf = self;
     __block UIPopoverController *popoverWeak = searchPopover;
        [calenderSearchResultsController addCalenderSearchResultDidSelect:^(id result, NSIndexPath *indexPath) 
         {
             [_searchBar resignFirstResponder];
             [weakSelf.view endEditing:YES];
             [popoverWeak dismissPopoverAnimated:NO];
             
             [weakSelf performSelector:@selector(selectSearchedResult:) withObject:result afterDelay:0.3];
         }];
        
        [calenderSearchResultsController release];
        [navController release];
    }
    else
    {
        UINavigationController *navController=(UINavigationController *)searchPopover.contentViewController;
        
        calenderSearchResultsController =(CalenderSearchResultsController *) [[navController viewControllers] objectAtIndex:0];
    }
    
    [calenderSearchResultsController filterEventsWithSearchText:searchText];
    
    if(!searchText.length) return;
    
    if(!searchPopover.isPopoverVisible)
        [searchPopover presentPopoverFromRect:[_searchBar frame] inView:[_searchBar superview] permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
}

-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}
#pragma mark -

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    [searchPopover dismissPopoverAnimated:NO];
    [searchBar resignFirstResponder];
    [self performSelector:@selector(dismissSearch) withObject:nil afterDelay:0.05];
    
    
    return YES;
}

#pragma mark -
#pragma mark BUG
-(void)selectSearchedResult:(id)result
{
    NSDate *_startDate = ([result isKindOfClass:[Task class]]) ? [result performSelector:@selector(dueDate)] : ([result isKindOfClass:[ScheduledVisit class]]) ? [result performSelector:@selector(visitStartDate)] :[result performSelector:@selector(startDate)];
    
    BOOL isCurrentDate = YES;
    
    if(_startDate)
    {
        isCurrentDate = [_startDate isBetweenDate:self.calendarView.currentStartDate andDate:self.calendarView.currentEndDate];
        
        if(!isCurrentDate)
            [self.calendarView.sliderView selectDate:_startDate];
    }
    
    //self.selectedSearchedResult = result;
    
    if(result)
        // [calendarView performSelector:@selector(selectCellForEvent:) withObject:result];
        [self.calendarView performSelector:@selector(selectCellForEvent:) withObject:result afterDelay:isCurrentDate?0.0:0.05];
                                               
}
#pragma mark -

-(void)dismissSearch
{
    [searchBar resignFirstResponder];
}



-(void)calenderDidLoad
{
   if(self.calenderDidLoadCallback) self.calenderDidLoadCallback();
    
    
    ///==============================
    self.calenderDidLoadCallback = nil;
    //============================
}

#pragma mark -
#pragma mark EKEventStoreChanged Notification

-(void)eventStoreChanged:(NSNotification *)notification
{
    NSLog(@"Changed Ekevent Object:%@",notification.object);
}

-(void)_fetchRemindersFromServer
{
    if(isFetchingFromServer) return;
    
    isFetchingFromServer = YES;
 
    self.currentReminderRequest =  [ConnectionManager fetchAndProcessRemindersOnCompletion:^(BOOL success, NSError *error) {
        
        isFetchingFromServer = NO;
        
        [loader stopAnimating];
        
        if(success)
        {
            if(self.calendarView)
            {
                [self.calendarView resetReminderCells];
                
                [self.calendarView fetchRemindersForStartDate:self.calendarView.currentStartDate endDate:self.calendarView.currentEndDate];
            }
        }

    }];
}

-(void)fetchRemindersFromServer
{
    if(isFetchingFromServer) return;
    
    isFetchingFromServer = YES;
    
    Rep *_rep = [CoreDataHandler currentRep];
    
    ASIHTTPRequest *request =  [ConnectionManager fetchRemindersWithRepID:_rep.repID wdSiteID:_rep.parentWdSite.wdSiteID];
    request.delegate = self;
    self.currentReminderRequest = request;
    
    /*[request setCompletionBlock:^{
        
        isFetchingFromServer = NO;
        
        NSDictionary *jsonDict = [request responseJSON];
        
        NSLog(@"json=%@",[request responseString]);
        
        if(jsonDict && [jsonDict isKindOfClass:[NSDictionary class]])
        {
            BOOL _isSiteIDChanged = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
            if(_isSiteIDChanged)
            {
                [app appDidChangeSiteID];
            }
            else
            {
                
                NSArray *_reminders  = [jsonDict valueForKey:JSON_DETAILS_KEY];
                NSArray *_updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
                NSArray *_deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
                
                _reminders  = NULL_NIL(_reminders);
                _updatedIDs = NULL_NIL(_updatedIDs);
                _deletedIDs = NULL_NIL(_deletedIDs);
                
                NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
                [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_REMINDERS];
                
                
                [Reminder populateAsyncPerformingDelete:_deletedIDs?_deletedIDs:[NSArray array] update:_updatedIDs?_updatedIDs:[NSArray array] insertOnArray:_reminders?_reminders:[NSArray array] onAttribute:@"reminderID" completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
                 {
                     
                    // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     dispatch_async([app eventQueue],^{    
                                                 
                    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] init] ;
                    [context setUndoManager:nil];
                    [context setPersistentStoreCoordinator:DEFAULT_CONTEXT.persistentStoreCoordinator];
                          
                                    
                         
                         for(Reminder *reminder in insertedResult)
                         {
                             if([reminder respondsToSelector:NSSelectorFromString(@"eventIdentifier")])
                             {
                                 Reminder *_reminder =(Reminder *)[reminder inContext:context];
                                 
                                 EKReminder * event = nil;
                                 NSString *eventIdentifier = _reminder.eventIdentifier;
                                 
                                 //NSString *title = [NSString stringWithFormat:@"Reminder: %@", _reminder.title?_reminder.title:@"Reminder"];
                                 
                                 
                                 if(eventIdentifier.length)
                                     event =(EKReminder *) [[CustomEventStore sharedInstance].eventStore calendarItemWithIdentifier:eventIdentifier];
                                 
                                 if(!event)
                                 {
                                     event = [CustomEventStore addReminder:_reminder];
                                 }
                                 else
                                 {
                                     event =[CustomEventStore editReminderForIdentifier:_reminder.eventIdentifier reminder:_reminder];
                                 }
                                 
                                 if(event && event.calendarItemIdentifier.length)
                                     _reminder.eventIdentifier = event.calendarItemIdentifier;
                             }
                         }
                         
                         if([_updatedIDs count])
                         {
                             for(Reminder *aRem in updatedResult)
                             {
                                 Reminder *_aRem =(Reminder *)[aRem inContext:context];
                                 
                                 if([_aRem respondsToSelector:NSSelectorFromString(@"eventIdentifier")] && _aRem.eventIdentifier.length)
                                 {
                                     
                                EKReminder  *event = [CustomEventStore editReminderForIdentifier:_aRem.eventIdentifier reminder:_aRem];
                                     
                                _aRem.eventIdentifier = event.calendarItemIdentifier;
                                     
                                 }
                             }
                         }
                         
                         [[SSCoreDataManager sharedManager] saveContext:context error:nil];
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                             if(self.calendarView)
                             {
                                 //[self.calendarView setNeedsLayout];
                                 
                                 [self.calendarView resetReminderCells];
                                 
                                 [self.calendarView fetchRemindersForStartDate:self.calendarView.currentStartDate endDate:self.calendarView.currentEndDate];
                             }
                         });

                         
                         [context reset];
                         [context release];
                     });
                 }];
                
            }
        }
        
}];

    [request setFailedBlock:^{
        
        isFetchingFromServer = NO;
        
    
    }]; */
    
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    isFetchingFromServer = NO;
    
    [loader stopAnimating];
    
    NSDictionary *jsonDict = [request responseJSON];
    
    jsonDict = NULL_NIL(jsonDict);
    NSLog(@"json=%@",[request responseString]);
    
    if(jsonDict && [jsonDict isKindOfClass:[NSDictionary class]])
    {
        /*
        BOOL _isSiteIDChanged = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
        BOOL _isRepDeactivated = [[jsonDict valueForKey:JSON_REP_DEACTIVATE_KEY] boolValue];
        
        NSNumber *isUnderMaintenance  = [jsonDict objectForKey:JSON_UNDER_MAINTENANCE_KEY];
        isUnderMaintenance = NULL_NIL(isUnderMaintenance);

        
        if(_isSiteIDChanged)
        {
            [app appDidChangeSiteID];
        }
        else if(_isRepDeactivated)
        {
            [app repDidDeactivate];
        }

        else if([isUnderMaintenance boolValue])
        {
            [app showUnderMaintainenceScreen:YES];
        }
        else
        */
        
        BOOL _isValidResponse = [ConnectionManager isValidResponse:jsonDict];
        if(_isValidResponse)
        {
            NSArray *_reminders  = [jsonDict valueForKey:JSON_DETAILS_KEY];
            NSArray *_updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
            NSArray *_deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
             NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
            
            _reminders  = NULL_NIL(_reminders);
            _updatedIDs = NULL_NIL(_updatedIDs);
            _deletedIDs = NULL_NIL(_deletedIDs);
            
            lastRequested = NULL_NIL(lastRequested);
           
            
            BOOL _isAnyDataAvailable = [_reminders count] || [_deletedIDs count];
            
            if(!_isAnyDataAvailable)
            {
               [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_REMINDERS];
            }
            

            
            [Reminder populateAsyncPerformingDelete:_deletedIDs?_deletedIDs:[NSArray array] update:_updatedIDs?_updatedIDs:[NSArray array] insertOnArray:_reminders?_reminders:[NSArray array] onAttribute:@"reminderID" completion:^(NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
             {
                 
                 if(_isAnyDataAvailable)
                 [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_REMINDERS];
                 
                 
                 NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType] ;
                 context.parentContext = DEFAULT_CONTEXT;
                 [context setUndoManager:nil];
                 
                 // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                 dispatch_async([app eventQueue],^{
                     
                    
                     for(Reminder *reminder in insertedResult)
                     {
                         if([reminder respondsToSelector:NSSelectorFromString(@"eventIdentifier")])
                         {
                             Reminder *_reminder =(Reminder *)[reminder inContext:context];
                             
                             EKReminder * event = nil;
                             NSString *eventIdentifier = _reminder.eventIdentifier;
                             
                             //NSString *title = [NSString stringWithFormat:@"Reminder: %@", _reminder.title?_reminder.title:@"Reminder"];
                             
                             
                             if(eventIdentifier.length)
                                 event =(EKReminder *) [[CustomEventStore sharedInstance].eventStore calendarItemWithIdentifier:eventIdentifier];
                             
                             if(!event)
                             {
                                 event = [CustomEventStore addReminder:_reminder];
                             }
                             else
                             {
                                 event =[CustomEventStore editReminderForIdentifier:_reminder.eventIdentifier reminder:_reminder];
                             }
                             
                             if(event && event.calendarItemIdentifier.length)
                                 _reminder.eventIdentifier = event.calendarItemIdentifier;
                         }
                     }
                     
                     if([_updatedIDs count])
                     {
                         for(Reminder *aRem in updatedResult)
                         {
                             Reminder *_aRem =(Reminder *)[aRem inContext:context];
                             
                             if([_aRem respondsToSelector:NSSelectorFromString(@"eventIdentifier")] && _aRem.eventIdentifier.length)
                             {
                                 
                                 EKReminder  *event = [CustomEventStore editReminderForIdentifier:_aRem.eventIdentifier reminder:_aRem];
                                 
                                 _aRem.eventIdentifier = event.calendarItemIdentifier;
                                 
                             }
                         }
                     }
                     
                     [[SSCoreDataManager sharedManager] saveContext:context error:nil];
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         if(self.calendarView)
                         {
                             //[self.calendarView setNeedsLayout];
                             
                             [self.calendarView resetReminderCells];
                             
                             [self.calendarView fetchRemindersForStartDate:self.calendarView.currentStartDate endDate:self.calendarView.currentEndDate];
                         }
                     });
                     
                     
                     [context reset];
                     [context release];
                 });
             }
            deletionBlock:^(NSMutableArray *deletedResult)
             {
                 
                 
             }];
            
        }
    }
    
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    isFetchingFromServer = NO;
    [loader stopAnimating];
}

/*
-(UIPopoverController *)popoverControllerForCalendarCell
{
    return   self.popOverCalendarCell;
}
-(void)setpopoverControllerForCalendarCell:(UIPopoverController *)popover
{
    self.popOverCalendarCell = popover;
}
*/

- (void)dealloc
{
    //[popOverCalendarCell release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
  
    [currentReminderRequest clearDelegatesAndCancel];
      currentReminderRequest.delegate = nil;
    [currentReminderRequest release];
    currentReminderRequest = nil;
    
    [addNewButton release];
    [loader release];
    [calender release];
    [popOverReminder release];
    [newEventPopover release];
    referance=nil;
    
    [calendarView release];
    calendarView = nil;
    
    selectedTabIndex=0;
    [filterPopover release];
    [searchPopover release];
    [searchBar release];
    [selectedSearchedResult release];
    
   // if(calenderDidLoadCallback)
      //  [calenderDidLoadCallback release];
    
    [calenderDidLoadCallback release];
    calenderDidLoadCallback = nil;
    
 
    
     [super dealloc];
}

/*
- (void)didChangeVisit:(ScheduledVisit *)visit actionType:(int)type
{

}
 */

- (void)onSignificantTimeChange:(NSNotification *)notification
{
    //NSLog(@"notif object=%@",notification.object);
    
    [self.calendarView checkAndUpdateToday];
}

- (void)viewDidUnload
{
      //[[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    
    return (interfaceOrientation == _initialInterfaceOrientation) ;
    
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}



@end
