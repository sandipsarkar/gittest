//
//  AddNewEventController.h
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^DidSelectCallback) (NSInteger index);
typedef void (^DidBackToMenuCallback) ();

@interface AddNewEventController : UIViewController

@property (nonatomic,retain) UITableView *tableView;
-(void)addDidSelectCallback:(DidSelectCallback )callback;
-(void)addDidBackToMenuCallback:(DidBackToMenuCallback )callback;

@end
