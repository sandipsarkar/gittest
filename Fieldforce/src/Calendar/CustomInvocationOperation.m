//
//  CustomInvocationOperation.m
//  Locator
//
//  Created by SANDIP_ RANDEM on 03/02/11.
//  Copyright 2011 RANDEM IT. All rights reserved.
//

#import "CustomInvocationOperation.h"


@implementation CustomInvocationOperation
@synthesize name,delegate;

-(id) initWithName:(NSString *)aName target:(id)target selector:(SEL)sel object:(id)arg
{	
	if((self=[super initWithTarget:target selector:sel object:arg]))
	 {
		self.name=aName;
		self.delegate=target;
		
		[self addObserver:self
					forKeyPath:@"isFinished" 
					   options:NSKeyValueObservingOptionNew
					   context:aName]; 		
	 }	
	return self;
}

-(void)_sendCallBack
{
	if([self.delegate  respondsToSelector:@selector(operationDidFinished:)])
		[self.delegate operationDidFinished:self];

}


- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
	if([keyPath isEqualToString:@"isFinished"])
	 {
		[self performSelectorOnMainThread:@selector(_sendCallBack) withObject:nil waitUntilDone:NO];	
	 }
	
	//[object removeObserver:self forKeyPath:keyPath];
	
}


- (void)dealloc 
{
	[name release];
	delegate=nil;
	
	[super dealloc];
}

@end
