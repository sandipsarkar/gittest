//
//  CustomInvocationOperation.h
//  Locator
//
//  Created by SANDIP_ RANDEM on 03/02/11.
//  Copyright 2011 RANDEM IT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CustomInvocationOperation;
@protocol InvocationOperationDelegate <NSObject>

@optional

-(void)operationDidFinished:(CustomInvocationOperation *)aOperation;

@end

@interface CustomInvocationOperation : NSInvocationOperation {

	
	NSString *name;
	
	id <InvocationOperationDelegate> delegate;
}

@property(nonatomic,retain)NSString *name;
@property(nonatomic,assign) id <InvocationOperationDelegate> delegate;

-(id) initWithName:(NSString *)aName target:(id)target selector:(SEL)sel object:(id)arg;

@end
