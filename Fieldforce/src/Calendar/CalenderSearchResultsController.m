//
//  CalenderSearchResultsController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 09/10/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CalenderSearchResultsController.h"
#import "Task.h"
#import "ScheduledVisit.h"
#import "Customer.h"
#import "CustomInvocationOperation.h"
#import "Reminder.h"

#define MAX_TICK_COUNT 3
@interface CalenderSearchResultsController ()
{
    NSTimer *_timer;
    int _timerTick;
    
    NSOperationQueue *_operationQue;
    ProgressHUD *progress;
    UILabel *bgLabel;
}

@property (nonatomic,copy)    CalenderSearchResultDidSelect calenderSearchResultDidSelect;
@property (nonatomic,copy)    NSString *currentSearchkey;
@property (nonatomic,retain)  NSArray *allEvents;


- (void) suggestiveSearchForText :(NSString *) searchText;

@end

@implementation CalenderSearchResultsController
@synthesize calenderSearchResultDidSelect;
@synthesize results;
@synthesize currentSearchkey;
@synthesize calendar;
@synthesize allEvents;
@synthesize selectedFilterIndex;

- (id)init
{
    self = [super initWithStyle:UITableViewStylePlain];
    
    if (self) 
    {
       self.title = @"Results";
       results=[[NSMutableArray alloc] init];
    }
    return self;
}

-(void)dealloc
{
    if(calenderSearchResultDidSelect)
        [calenderSearchResultDidSelect release];
    
    [results release];
    [allEvents release];
    
    [_operationQue release];
    [currentSearchkey release];
    [calendar release];
    
    [super dealloc];
}

-(void)addCalenderSearchResultDidSelect:(CalenderSearchResultDidSelect)callback
{

    self.calenderSearchResultDidSelect = callback;
}

- (NSOperationQueue *) startOperationQueue 
{
	if(_operationQue == nil)
	{
		_operationQue = [[NSOperationQueue alloc] init]; 
	}
	[_operationQue setMaxConcurrentOperationCount:1];
	return _operationQue;
}

//Cancel last operation
- (void) cancelLastOperation
{
	@try 
	{
		id currentOperation=[[[self startOperationQueue] operations] lastObject]; //Cancel last operation
		if([currentOperation isExecuting]) [currentOperation cancel];
	}
	@catch (NSException * exception) 
	{
		NSLog(@"Can not stop last operation for error::%@ with reason::%@",[exception name],[exception reason]);
	}
}

//Cancel all operations
- (void) cancelAllSearchOperations
{
	@try
	{
		[ [self startOperationQueue] cancelAllOperations];
	}
	@catch (NSException *e) 
	{
		NSLog(@"Search operation cancellation stopped for error name : %@ and reason %@",[e name],  [e reason]);
	}
}

-(void)_invalidTimer
{
	if(_timer) [_timer invalidate];
	_timer=nil;
	_timerTick=0;
}


-(void)_requestForSearch
{
	if(_timer==nil)
		_timer=[NSTimer scheduledTimerWithTimeInterval:0.18 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
}

-(void)timerAction:(NSTimer *)aTimer
{
	_timerTick++;
	if(_timerTick>MAX_TICK_COUNT)
    {
		[self _invalidTimer];
        
		[results removeAllObjects];
	
		//if( [self.currentSearchkey length] > 0 && self.currentSearchkey!=nil )
        {
			
				[self cancelLastOperation];
				
				[[self startOperationQueue] setSuspended:NO];
				CustomInvocationOperation *operation = [[CustomInvocationOperation alloc]  initWithName:self.currentSearchkey target:self selector:@selector(suggestiveSearchForText:) object:self.currentSearchkey];
				[ [self startOperationQueue] addOperation:operation]; //Add operation to thread
				[operation release];
				
        }
    }
}

-(void)filterEventsWithSearchText:(NSString *)searchText
{
    /*
    if(!progress)
    {
    progress = [ProgressHUD showHUDAddedTo:self.view animated:YES];
    progress.mode = MBProgressHUDModeIndeterminate;
    progress.tag  = NSIntegerMax-1;
    progress.labelText = @"Searching...";
    }
     */
    
    bgLabel.text=@"Searching..";
    
    self.currentSearchkey=searchText;
	_timerTick=0;
	[self _requestForSearch];

    /*
    if(!searchText.length)
    {
        [self.results removeAllObjects];
        [self.tableView reloadData];
        return;
    }
    
    NSString *specialCharString = @"$&*@+#{}[]%=";
    BOOL isSpecialChar = [specialCharString rangeOfString:searchText].location!=NSNotFound;
    
    
   // NSString *_calenderIdentifier = [[NSUserDefaults standardUserDefaults] objectForKey:CALENDER_IDENTIFIER_KEY];
   // EKCalendar *calender = [[CustomEventStore sharedInstance] customCalendarWithIdentifier:_calenderIdentifier title:@"com.reptool.calender"];
    
    NSDate *startDate = [NSDate dateWithTimeIntervalSinceNow:-60*60*24*365*1];    //FIVE YEARS BACK   
    NSDate *endDate   = [NSDate dateWithTimeIntervalSinceNow:60*60*24*365*1];     //FIVE YEARS AFTER
    [self.results removeAllObjects];
    
    ProgressHUD *progress = [ProgressHUD showHUDAddedTo:self.view animated:YES];
    progress.mode = MBProgressHUDModeIndeterminate;
    progress.tag  = NSIntegerMax-1;
    progress.labelText = @"Searching...";   

    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSDate* currentStart = [NSDate dateWithTimeInterval:0 sinceDate:startDate];
        
        int seconds_in_year = 60*60*24*365*1;
        
        // enumerate events by one year segment because iOS do not support predicate longer than 4 year !
        while ([currentStart compare:endDate] == NSOrderedAscending) 
        {
            NSDate* currentFinish = [NSDate dateWithTimeInterval:seconds_in_year sinceDate:currentStart];
            
            if ([currentFinish compare:endDate] == NSOrderedDescending) 
            {
                currentFinish = [NSDate dateWithTimeInterval:0 sinceDate:endDate];
            }
            
          //   NSPredicate *predicate = [[CustomEventStore sharedInstance].eventStore predicateForEventsWithStartDate:currentStart endDate:currentFinish calendars:[NSArray arrayWithObject:calender]];
            
            NSPredicate *predicate = [[CustomEventStore sharedInstance].eventStore predicateForEventsWithStartDate:currentStart endDate:currentFinish calendars:nil];

            
             NSArray *_events = [[CustomEventStore sharedInstance].eventStore eventsMatchingPredicate:predicate];
            
            if([_events count])
            [self.results addObjectsFromArray:_events];
             
            currentStart = [NSDate dateWithTimeInterval:(seconds_in_year + 1) sinceDate:currentStart];
            
          }
        
        NSLog(@"self.results=%d",[self.results count]);

           NSString *locationStr = [[AppDelegate eventLocationString] retain];
          [self.results filterUsingPredicate:[NSPredicate predicateWithFormat:@"(self.location == %@)",locationStr]]; 

        
       // NSPredicate *searchPredicte=[NSPredicate predicateWithFormat:@"self.title beginswith[c] %@ OR self.title contains[cd] %@ OR self.title contains[cd] %@ AND self.location = %@",searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@"",locationStr];
        
#if SHOW_REMINDER_TITLE
        
         NSPredicate *searchPredicte=[NSPredicate predicateWithFormat:@"self.location == %@ AND (self.title contains[cd] %@ OR self.title contains[cd] %@ OR self.title contains[cd] %@)",locationStr,searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@""];
#else       
        NSPredicate *searchPredicte=[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) 
        {
            NSString *_locationStr = [AppDelegate eventLocationString];
            
            NSLog(@"BINDINGS:%@",bindings);
            
            EKEvent *reminder = (EKEvent *)evaluatedObject;
            
            if(![reminder respondsToSelector:@selector(location)])
            {
                return NO;
            }
            
            if(![reminder.location isEqualToString:_locationStr])
            {
                return NO;
            }
            
              NSString *reminderTitle = ([reminder.title rangeOfString:@":"].location!=NSNotFound) ? [reminder.title substringFromIndex:[reminder.title rangeOfString:@":"].location+1] : reminder.title;
            
            if([reminderTitle rangeOfString:searchText].location!=NSNotFound)
            {
                return YES;
            }
            
            if([reminderTitle rangeOfString:searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText].location!=NSNotFound)
            {
                return YES;
            }
            
            if([reminderTitle rangeOfString:isSpecialChar?searchText:@""].location!=NSNotFound)
            {
                return YES;
            }
            
            
            return NO;
        }];
        
#endif
         if([self.results count])
         [self.results filterUsingPredicate:searchPredicte];
        
          //Filter task and filter visits now
            dispatch_async(dispatch_get_main_queue(), ^{
            
            NSMutableArray *tasks  =  [CoreDataHandler allTasks];
            NSMutableArray *visits =  [CoreDataHandler allScheduledVisits];
                
           // NSPredicate *searchPredicte=[NSPredicate predicateWithFormat:@"self.subject beginswith[c] %@ OR self.subject contains[cd] %@ OR self.subject contains[cd] %@",searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@""];
                
            NSPredicate *searchPredicte=[NSPredicate predicateWithFormat:@"self.subject contains[c] %@ OR self.subject contains[cd] %@ OR self.subject contains[cd] %@",searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@""];
              
    @try {
                    
        [tasks filterUsingPredicate:searchPredicte];
            //searchPredicte=[NSPredicate predicateWithFormat:@"self.customerName beginswith[c] %@ OR self.customerName contains[cd] %@ OR self.customerName contains[cd] %@",searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@""];
                
       // searchPredicte=[NSPredicate predicateWithFormat:@"self.customer.name beginswith[c] %@ OR self.customer.name contains[cd] %@ OR self.customer.name contains[cd] %@",searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@""];
        
           searchPredicte=[NSPredicate predicateWithFormat:@"self.customer.name contains[c] %@ OR self.customer.name contains[cd] %@ OR self.customer.name contains[cd] %@",searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@""];
                
            [visits filterUsingPredicate:searchPredicte];
                }
                @catch (NSException *exception)
                {
                    
                }
                @finally 
                {
                    [self.results addObjectsFromArray:tasks];
                    [self.results addObjectsFromArray:visits];  
                    
                    [ProgressHUD hideHUDForView:self.view animated:YES];
                    
                    NSLog(@"Results=%@",self.results);
                    [self.tableView reloadData];

                }
                            
            });
    });
     
     */
}

-(void)suggestiveSearchForText :(NSString *) searchText
{
    //if(![self.currentSearchkey isEqualToString:searchText]) return;
    
	if( [searchText length] )
    {
		[self cancelLastOperation];
    }

    
     if(!searchText.length)
     {
       [self.results removeAllObjects];
       //[self.tableView reloadData];
       return;
     }
     
     NSString *specialCharString = @"$&*@+#{}[]%=";
     BOOL isSpecialChar = [specialCharString rangeOfString:searchText].location!=NSNotFound;
     
     
     // NSString *_calenderIdentifier = [[NSUserDefaults standardUserDefaults] objectForKey:CALENDER_IDENTIFIER_KEY];
     // EKCalendar *calender = [[CustomEventStore sharedInstance] customCalendarWithIdentifier:_calenderIdentifier title:@"com.reptool.calender"];
     
   //  NSDate *startDate = [NSDate dateWithTimeIntervalSinceNow:-60*60*24*30*6];    //FIVE YEARS BACK
    // NSDate *endDate   = [NSDate dateWithTimeIntervalSinceNow:60*60*24*365*6];     //FIVE YEARS AFTER
     [self.results removeAllObjects];
    
    NSMutableArray *tasks  = nil;
    NSMutableArray *visits = nil;
     NSMutableArray *reminders = nil;
    
  @try {
            
    /*
     NSDate* currentStart = [NSDate dateWithTimeInterval:0 sinceDate:startDate];
     
     int seconds_in_year = 60*60*24*30*1;
     
     // enumerate events by one year segment because iOS do not support predicate longer than 4 year !
     while ([currentStart compare:endDate] == NSOrderedAscending) 
     {
     NSDate* currentFinish = [NSDate dateWithTimeInterval:seconds_in_year sinceDate:currentStart];
     
     if ([currentFinish compare:endDate] == NSOrderedDescending) 
     {
     currentFinish = [NSDate dateWithTimeInterval:0 sinceDate:endDate];
     }
     
     //   NSPredicate *predicate = [[CustomEventStore sharedInstance].eventStore predicateForEventsWithStartDate:currentStart endDate:currentFinish calendars:[NSArray arrayWithObject:calender]];
     
     NSPredicate *predicate = [[CustomEventStore sharedInstance].eventStore predicateForEventsWithStartDate:currentStart endDate:currentFinish calendars:nil];
     
     
     NSArray *_events = [[CustomEventStore sharedInstance].eventStore eventsMatchingPredicate:predicate];
     
     if([_events count])
     [self.results addObjectsFromArray:_events];
     
     currentStart = [NSDate dateWithTimeInterval:(seconds_in_year + 1) sinceDate:currentStart];
     
     }
     */
    
    /*
    NSPredicate *predicate = [[CustomEventStore sharedInstance].eventStore predicateForEventsWithStartDate:startDate endDate:endDate calendars:[NSArray arrayWithObject:self.calendar]];
    
    
    if(!self.allEvents)
    {
    NSArray *_events = [[CustomEventStore sharedInstance].eventStore eventsMatchingPredicate:predicate];
    self.allEvents = _events;
    }
      
    if([self.allEvents count])
    [self.results addObjectsFromArray: self.allEvents];

     
     NSLog(@"self.results=%d",[self.results count]);
     
   //  NSString *locationStr = [[AppDelegate eventLocationString] retain];
     //[self.results filterUsingPredicate:[NSPredicate predicateWithFormat:@"(self.location == %@)",locationStr]]; 
     
     
     // NSPredicate *searchPredicte=[NSPredicate predicateWithFormat:@"self.title beginswith[c] %@ OR self.title contains[cd] %@ OR self.title contains[cd] %@ AND self.location = %@",searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@"",locationStr];
     
     #if SHOW_REMINDER_TITLE
     
     NSPredicate *searchPredicte=[NSPredicate predicateWithFormat:@"self.location == %@ AND (self.title contains[c] %@ OR self.title contains[cd] %@ OR self.title contains[cd] %@)",locationStr,searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@""];
     #else       
     NSPredicate *searchPredicte=[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) 
     {
     NSString *_locationStr = [AppDelegate eventLocationString];
     
     NSLog(@"BINDINGS:%@",bindings);
     
     EKEvent *reminder = (EKEvent *)evaluatedObject;
     
     if(![reminder respondsToSelector:@selector(location)])
     {
     return NO;
     }
     
     if(![reminder.location isEqualToString:_locationStr])
     {
     return NO;
     }
     
     NSString *reminderTitle = ([reminder.title rangeOfString:@":"].location!=NSNotFound) ? [reminder.title substringFromIndex:[reminder.title rangeOfString:@":"].location+1] : reminder.title;
     
     if([reminderTitle rangeOfString:searchText].location!=NSNotFound)
     {
     return YES;
     }
     
     if([reminderTitle rangeOfString:searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText].location!=NSNotFound)
     {
     return YES;
     }
     
     if([reminderTitle rangeOfString:isSpecialChar?searchText:@""].location!=NSNotFound)
     {
     return YES;
     }
     
     
     return NO;
     }];
     
     #endif
     if([self.results count])
     [self.results filterUsingPredicate:searchPredicte];
     
     */
     //Filter task and filter visits now
  
    // NSPredicate *searchPredicte=[NSPredicate predicateWithFormat:@"self.subject beginswith[c] %@ OR self.subject contains[cd] %@ OR self.subject contains[cd] %@",searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@""];
     
    NSPredicate *  searchPredicte   = nil;
    NSManagedObjectContext *context = [SSCoreDataManager backgroundContext];
    
      if(selectedFilterIndex==0 || selectedFilterIndex==3)
      {
          //searchPredicte=[NSPredicate predicateWithFormat:@"self.subject contains[c] %@ OR self.subject contains[cd] %@ OR self.subject contains[cd] %@",searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@""];
          
          searchPredicte = [NSPredicate predicateWithFormat:@"self.subject beginswith[c] %@ OR self.subject contains[cd] %@ OR self.subject contains[cd] %@",searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@""];
          
        tasks  =  [Task fetchOnContext:context predicate:searchPredicte onAttributes:nil error:nil];
      }
    
    if(selectedFilterIndex==0 || selectedFilterIndex==2)
    {
      //searchPredicte=[NSPredicate predicateWithFormat:@"self.customer.name contains[c] %@ OR self.customer.name contains[cd] %@ OR self.customer.name contains[cd] %@",searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@""];
        
        searchPredicte=[NSPredicate predicateWithFormat:@"self.customer.name beginswith[c] %@ OR self.customer.name contains[cd] %@ OR self.customer.name contains[cd] %@",searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@""];
    
      visits =  [ScheduledVisit fetchOnContext:context predicate:searchPredicte onAttributes:nil error:nil];
    }
      
      
      if(selectedFilterIndex==0 || selectedFilterIndex==1)
      {
          
      //searchPredicte=[NSPredicate predicateWithFormat:@"self.title contains[c] %@ OR self.title contains[cd] %@ OR self.title contains[cd] %@",searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@""];
          
      searchPredicte=[NSPredicate predicateWithFormat:@"self.title beginswith[c] %@ OR self.title contains[cd] %@ OR self.title contains[cd] %@",searchText,searchText.length?[NSString stringWithFormat:@" %@",searchText]:searchText,isSpecialChar?searchText:@""];
      
      reminders =  [Reminder fetchOnContext:context predicate:searchPredicte onAttributes:nil error:nil];
      }
    
    
    [SSCoreDataManager destroyBackgroundContext:context];
     
     }
     @catch (NSException *exception)
     {
     
     }
     @finally 
     {

        NSManagedObjectContext *defaultContext=DEFAULT_CONTEXT;
                  
     //[self.results addObjectsFromArray:tasks];
    // [self.results addObjectsFromArray:visits];  
         
         
         for(ScheduledVisit *visit in visits)
         {
             ScheduledVisit *_visitInMainContext =(ScheduledVisit *)[defaultContext objectWithID:visit.objectID];
             
             if(_visitInMainContext)
                 [self.results addObject:_visitInMainContext];
         }
         
         for(Task *aTask in tasks)
         {
             Task *_taskInMainContext =(Task *)[defaultContext objectRegisteredForID:aTask.objectID];
             
             if(_taskInMainContext)
                 [self.results addObject:_taskInMainContext];
         }
         
         for(Reminder *aReminder in reminders)
         {
             Reminder *_remInMainContext =(Reminder *)[defaultContext objectRegisteredForID:aReminder.objectID];
             
             if(_remInMainContext)
                 [self.results addObject:_remInMainContext];
         }

         
    
     NSLog(@"Results=%@",self.results);
   //  [self.tableView reloadData];
     
     }
}

-(void)operationDidFinished:(CustomInvocationOperation *)aOperation
{
	if([aOperation.name isEqualToString:self.currentSearchkey])
    {
		[self performSelectorOnMainThread:@selector(reload) withObject:nil waitUntilDone:NO];
    }
    
  //  [ProgressHUD showHUDAddedTo:self.view animated:YES];
   // progress=nil;
}

-(void)reload
{
    [self.tableView reloadData];
    
    bgLabel.text = ![self.results count] ? @"NO RESULTS" : nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    self.tableView.bounces = NO;
    self.tableView.rowHeight = 55.0f;
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    self.tableView.backgroundColor = [UIColor whiteColor];

    
    UIView *bgView=[[UIView alloc] initWithFrame:self.view.bounds];
   bgLabel = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, self.tableView.rowHeight*2, CGRectGetWidth(bgView.bounds)-10.0f, 55.0f)];
    bgLabel.text = @"NO RESULTS";
    bgLabel.font = [UIFont grotesqueBoldFontOfSize:16.0];
    bgLabel.textAlignment = NSTextAlignmentCenter;
    bgLabel.textColor = [UIColor grayColor];
    bgLabel.backgroundColor = [UIColor clearColor];
    bgLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [bgView addSubview:bgLabel];
    self.tableView.backgroundView = bgView;
    [bgLabel release];
    [bgView release];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(self.currentSearchkey)
    [self filterEventsWithSearchText:self.currentSearchkey];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self cancelAllSearchOperations];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    NSUInteger count = [results count];
    
    self.tableView.backgroundView.hidden = count>0;
    // Return the number of rows in the section.
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(!cell)
    {
        cell = [[[UITableViewCell alloc ] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:16.0];
        cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:14.0];
        
        cell.accessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        cell.accessoryView.userInteractionEnabled = NO;
    }
    
    id event = (indexPath.row < [self.results count]) ?  [self.results objectAtIndex:indexPath.row] : nil;
    NSDate *_startDate=nil;
    if([event isKindOfClass:[ScheduledVisit class]])
    {
        ScheduledVisit *visit = (ScheduledVisit *)event;
        
         cell.imageView.image = [UIImage imageNamed:@"green_dot.png"];
        cell.textLabel.text = visit.customer ? visit.customer.name : visit.customerName ;
        _startDate = visit.visitStartDate;
    }
    else if([event isKindOfClass:[Task class]])
    {
        Task *task = (Task *)event;
        
        cell.imageView.image = [UIImage imageNamed:@"red_dot.png"];
        
        NSString *taskTitle = [task valueForKey:@"subject"];
        cell.textLabel.text = taskTitle;
        _startDate = task.dueDate;
    }
    else if([event isKindOfClass:[Reminder class]])
    {
        Reminder *reminder = (Reminder *)event;
        
       #if SHOW_REMINDER_TITLE
        NSString *reminderTitle = reminder.title;                        
        #else
        NSString *reminderTitle = ([reminder.title rangeOfString:@":"].location !=NSNotFound)? [reminder.title substringFromIndex:[reminder.title rangeOfString:@":"].location+1] : reminder.title;
        #endif

        
        cell.imageView.image = [UIImage imageNamed:@"yellow_dot.png"];
        cell.textLabel.text = reminderTitle ; 
        _startDate = reminder.startDate; 
    }
    

    cell.detailTextLabel.text = [_startDate stringFromDateWithFormat:@"dd/MM/YYYY  hh:mm aa"];
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    id event = (indexPath.row < [self.results count]) ?  [self.results objectAtIndex:indexPath.row] : nil;
    self.calenderSearchResultDidSelect(event,indexPath);
}

@end
