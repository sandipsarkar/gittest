//
//  CalenderSearchResultsController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 09/10/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomEventStore.h"
typedef void (^CalenderSearchResultDidSelect) (id result,NSIndexPath *indexPath);
@interface CalenderSearchResultsController : UITableViewController

@property (nonatomic,retain)  NSMutableArray *results;
@property (nonatomic,retain)  EKCalendar *calendar;
@property (nonatomic,assign)  NSInteger selectedFilterIndex;

-(void)addCalenderSearchResultDidSelect:(CalenderSearchResultDidSelect)callback;
-(void)filterEventsWithSearchText:(NSString *)searchText;
@end
