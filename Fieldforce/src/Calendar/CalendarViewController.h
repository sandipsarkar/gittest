//
//  CalendarViewController.h
//  OEM_CALENDAR
//
//  Created by elie maalouly on 4/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CXCalendarView.h"
#import "ADDReminder.h"
#import "ADDVisit.h"

typedef void(^CalenderDidLoadCallback) (void);
@interface CalendarViewController : UIViewController<CXCalendarViewDelegate,UIPopoverControllerDelegate,ADDVisitDelegate,ADDReminderDelegate,CalendarDelegate>
{
    UIButton  * selectedButton;
    UIButton  * addNewButton;
    UIButton  * btnFilter;
    UIPopoverController * popOverReminder;
    
    UIImageView * infoSliderView;
    UIActivityIndicatorView * loader;
    
    UIInterfaceOrientation _initialInterfaceOrientation;
    
    BOOL _isUpcommingVisitSelected;

}
@property(nonatomic,retain) CXCalendarView *calendarView;
@property(nonatomic,assign) id <CalendarDelegate>referance;
@property(nonatomic,assign) NSInteger selectedTabIndex;

//@property(nonatomic,retain) UIPopoverController *popOverCalendarCell;


-(void)selectScheduledVisit:(id)visit;
-(void)addCalenderDidLoadCallbak:(CalenderDidLoadCallback)callback;

-(void)scheduleNewVisitForCustomer:(Customer *)customer;



@end
