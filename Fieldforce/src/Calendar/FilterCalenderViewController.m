//
//  FilterCalenderViewController.m
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "FilterCalenderViewController.h"
@interface FilterCalenderViewController()

@property(nonatomic,copy)FilterDidSelectCallback didSelectCallback;
@property(nonatomic,retain) NSIndexPath *selectedIndexPath;
@end

@implementation FilterCalenderViewController
@synthesize didSelectCallback;
@synthesize selectedIndexPath;

- (id)initWithSelectedIndex:(NSInteger)index
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) 
    {
        selectedIndexPath = [[NSIndexPath indexPathForRow:index inSection:0] retain];
    }
    return self;
}

-(void)dealloc
{
    
    if(didSelectCallback)
        [didSelectCallback release];
    didSelectCallback=nil;
    
    [selectedIndexPath release] ;
    selectedIndexPath = nil;
    
    [super dealloc];
}

-(void)addDidSelectCallback:(FilterDidSelectCallback )callback
{
    self.didSelectCallback = callback;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.bounces = NO;
    
   if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
   {
       [self setupTableHeaderView];
       self.tableView.sectionFooterHeight = 10.0f;
       self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
   }
}

-(void)setupTableHeaderView
{
    CGRect frame = self.tableView.tableHeaderView.frame;
    frame.size.height = 10.0;
    UIView *headerView = [[UIView alloc] initWithFrame:frame];
    self.tableView.tableHeaderView = headerView;
    [headerView release];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 4;
}

-(void)configureCell:(UITableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) 
    {
        case 0:
        {
            cell.imageView.image = nil;
            cell.textLabel.text  = @"Show All";
        }
        break;
        case 1:
        {
            cell.imageView.image = [UIImage imageNamed:@"yellow_dot.png"];
            cell.textLabel.text = @"Reminders";
        }
       break;
        case 2:
        {
            cell.imageView.image = [UIImage imageNamed:@"green_dot.png"];
            cell.textLabel.text = @"Visits";
        }
        break;
        case 3:
        {
            cell.imageView.image = [UIImage imageNamed:@"red_dot.png"];
            cell.textLabel.text = @"Tasks";
        }
        break;
            
        default:
            break;
    }
    
    cell.accessoryType = [self.selectedIndexPath isEqual:indexPath] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.font = [UIFont grotesqueFontOfSize:16.0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    [self configureCell:cell forIndexPath:indexPath];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.tag = 1;
    
    
    NSInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
    
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.selectedIndexPath isEqual:indexPath]) return;
    
    NSMutableArray *indexPathsToReload = [[NSMutableArray alloc] init];
    
    if(self.selectedIndexPath) [indexPathsToReload addObject:self.selectedIndexPath];
    
    [indexPathsToReload addObject:indexPath];
    
    self.selectedIndexPath = indexPath;
    
    if([indexPathsToReload count])
    [tableView reloadRowsAtIndexPaths:indexPathsToReload withRowAnimation:UITableViewRowAnimationNone];
    
    [indexPathsToReload release];
    
    if(self.didSelectCallback) self.didSelectCallback(indexPath.row);
}

@end
