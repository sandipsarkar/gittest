//
//  AddNewEventController.m
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "AddNewEventController.h"
#import "InsetTableViewCell.h"
#import <objc/runtime.h>

//static char overviewKey;

@interface AddNewEventController()<InsetTableViewCellDelegate,UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,copy)DidSelectCallback didSelectCallback;
@property(nonatomic,copy)DidBackToMenuCallback didBackToMenuCallback;
@end

@implementation AddNewEventController
@synthesize didSelectCallback,didBackToMenuCallback;
@synthesize tableView;

- (id)init
{
    self = [super init] ; // [super initWithStyle:UITableViewStyleGrouped];
    if (self) 
    {
       
    }
    return self;
}

-(void)dealloc
{
    [tableView release];
    if(didSelectCallback) [didSelectCallback release];
    if(didBackToMenuCallback) [didBackToMenuCallback release];
    
    [super dealloc];
}

-(void)addDidSelectCallback:(DidSelectCallback )callback
{
    self.didSelectCallback = callback;
    
    // objc_setAssociatedObject (self, &overviewKey,callback,OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)addDidBackToMenuCallback:(DidBackToMenuCallback )callback
{
    self.didBackToMenuCallback = callback;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

   #if TRACK_ERROR
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
   #endif
    
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
   }
    
    [self addTitle:@"CREATE NEW" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    UITableView *_tableView =   [UITableView tableViewWithFrame:self.view.bounds style:UITableViewStyleGrouped delegate:self datasource:self];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_tableView];
    self.tableView  = _tableView;
     
    
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.bounces = NO;
    //self.tableView.backgroundColor = [UIColor clearColor];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [self setupTableHeaderView];
        self.tableView.sectionFooterHeight = 10.0;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        self.tableView.backgroundColor =CELL_BACKGROUND_COLOR;
        
        
        UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
       // backgroundView.backgroundColor = CELL_BACKGROUND_COLOR;
        self.tableView.backgroundView  = backgroundView;
        [backgroundView release];
    }
 
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {

    UIButton *_cancelButton=[UIButton repToolButtonWithImageName:@"cancel_button.png" text:nil];
    //_cancelButton.titleLabel.textColor = [UIColor whiteColor];
    //_cancelButton.titleLabel.font = [UIFont subaruBoldFontOfSize:16.0];
    [_cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *cancelBarButton = [[UIBarButtonItem alloc] initWithCustomView:_cancelButton];
    self.navigationItem.leftBarButtonItem = cancelBarButton;
    [cancelBarButton release];
    
    }
    else
    {
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction:)];
     self.navigationItem.leftBarButtonItem = cancelButton;
    [cancelButton release];
    
    }

}

-(void)setupTableHeaderView
{
    CGRect frame = self.tableView.tableHeaderView.frame;
    frame.size.height = 10.0;
    UIView *headerView = [[UIView alloc] initWithFrame:frame];
    self.tableView.tableHeaderView = headerView;
    [headerView release];
}


-(void)cancelAction:(id)sender
{
    self.didSelectCallback = nil;
    if(self.popoverController && self.popoverController.isPopoverVisible)
    [self.popoverController dismissPopoverAnimated:YES];
    

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //self.title = @"CREATE NEW";
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
    self.preferredContentSize =  CGSizeMake(335.0+15.0, 152.0);
    }
    else
    self.contentSizeForViewInPopover = CGSizeMake(335.0+15.0, 152.0);

    
    if(self.didBackToMenuCallback) self.didBackToMenuCallback();
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //self.popoverController.popoverContentSize = CGSizeMake(335.0, MAX(110.0,self.tableView.contentSize.height));
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
    self.preferredContentSize = CGSizeMake(335.0+15.0, MAX(150.0,self.tableView.contentSize.height));
    }
    else
    self.contentSizeForViewInPopover = CGSizeMake(335.0+15.0, MAX(150.0,self.tableView.contentSize.height));
    self.modalInPopover = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //self.title=nil;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return 3;
}

-(void)configureCell:(UITableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) 
    {
        case 0:
        {
            cell.imageView.image = [UIImage imageNamed:@"yellow_dot.png"];
            cell.textLabel.text  = @"New Reminder";
        }
        break;
        case 1:
        {
            cell.imageView.image = [UIImage imageNamed:@"green_dot.png"];
            cell.textLabel.text = @"New Visit";
        }
        break;
        case 2:
        {
            cell.imageView.image = [UIImage imageNamed:@"red_dot.png"];
            cell.textLabel.text = @"New Task";
        }
            break;

            
        default:
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.font = [UIFont grotesqueFontOfSize:16.0];
        
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }

    [self configureCell:cell forIndexPath:indexPath];
    
    return cell;
}

-(void)tableView:(UITableView *)_tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    InsetTableViewCell *_cell = (InsetTableViewCell *)cell;
    _cell.delegate = self;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
  
    
    CellBackgroundView *_selectedBgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _selectedBgView.backgroundColor = [UIColor clearColor];
    _selectedBgView.contentInset = CGPointMake(10.0, 0.0);
    _selectedBgView.cornerRadius = 8.0;
    _selectedBgView.borderColor = self.tableView.separatorColor;
    _selectedBgView.fillColor = [UIColor cellSelectedColorBlue];

    
    
    NSInteger numRows = [_tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    _selectedBgView.position = _bgView.position;
    
    cell.backgroundView = _bgView;
    cell.selectedBackgroundView = _selectedBgView;
    
    [_bgView release];
    [_selectedBgView release];
    
}

-(void)insetTableViewCell:(InsetTableViewCell *)cell didSelect:(BOOL)selected
{
    cell.textLabel.textColor = selected ? [UIColor whiteColor] : [UIColor blackColor];
  
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
    
  //  self.modalInPopover = YES;
    
    if(self.didSelectCallback) self.didSelectCallback(indexPath.row);
    
    
    //DidSelectCallback block=  objc_getAssociatedObject(self, &overviewKey);
    //if(block) block(indexPath.row);
    //objc_setAssociatedObject (self, &overviewKey,nil,OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
