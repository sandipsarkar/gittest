//
//  TasksViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 23/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "TasksViewController.h"
#import "Customer.h"
#import "Task.h"
#import "SSSegmentedControl.h"
//#import "TaskDetailViewController.h"
#import "UISearchBar+BackgroundImage.h"
#import "CreateTaskViewController.h"
#import "TaskDetailViewController.h"
#import "CoreDataHandler.h"
#import "Rep.h"
#import "CustomEventStore.h"

#define TOP_BAR_IMAGE @"view_tasks_side_bar.png"

@interface TasksViewController()<UISearchBarDelegate>
{
    CGRect _containerRect;
    ParallelStackViewController *_parallelStackViewController;
    SSSegmentedControl *customSegControl;
    UIButton *filterButton;

    
   
}



@property(nonatomic,retain)UIPopoverController  *createTaskPopOver;

-(void)loadTasks;

@end

@implementation TasksViewController
@synthesize taskListViewController;
@synthesize taskDetailViewController;
@synthesize createTaskPopOver;

@synthesize customer;

- (id)init
{
    self = [super init];
    if (self)
    {
        taskListViewController=[[TasksListViewController alloc] init];
        taskListViewController.parentController = self;
        _parallelStackViewController=[[ParallelStackViewController alloc]initWithMasterController:taskListViewController];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newTaskAdded:) name:TaskDidAddEditNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(taskDidEdit:) name:TaskDidEditNotification object:nil];
    }
    return self;
}

-(ParallelStackViewController *)parallelStackViewController
{
    return _parallelStackViewController;
}

-(id)initWithCustomer:(Customer *)_customer
{
    self=[self init];
    if(self)
    {
        self.customer=_customer; 
        
        //[self loadTasks];
        
    }
    
    return self;
}



-(void)dealloc
{
    [_parallelStackViewController release];
    [taskListViewController release];
    taskListViewController=nil;
    [customSegControl release];
    customSegControl=nil;
    [searchBar release];
    [createTaskPopOver release];
    [customer release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


-(void)setupSegmentedControl
{
    NSArray *_items=[NSArray arrayWithObjects:@"Current",@"Archived",nil];
    
    customSegControl=[[SSSegmentedControl alloc] initWithItems:_items setupSegment:^(SSegment *segment,NSUInteger index)
        {
            segment.textLabel.textColor=[UIColor whiteColor];
            segment.textLabel.font  = [UIFont grotesqueBoldFontOfSize:13.0];
        }];
    
    customSegControl.frame=CGRectMake(CGRectGetMaxX(filterButton.frame), 0, 117*2, 44);
    
    
    [customSegControl setBackgroundForNormalState:[UIImage imageNamed:@"segmented_button.png"] forSelectedState:[UIImage imageNamed:@"segmented_button_selected.png"]];
   
    [customSegControl addTarget:self action:@selector(segmentedControlValChanged:) forControlEvents:UIControlEventValueChanged];
    customSegControl.selectedSegmentIndex=0;
    [self.view addSubview:customSegControl];

}

-(void)selectTabAtIndex:(NSUInteger)index
{
    customSegControl.selectedSegmentIndex = index;
}

-(void)setupFilterButton
{
    filterButton =[UIButton buttonWithType:UIButtonTypeCustom];
    filterButton.frame = CGRectMake(0, 0, 117.0, 44.0); //117.0
    [filterButton setTitle:@"Filter" forState:UIControlStateNormal];
    filterButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
    [filterButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"segmented_button.png"] forState:UIControlStateNormal];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"segmented_button_selected.png"] forState:UIControlStateHighlighted];
     //[filterButton setBackgroundImage:[UIImage imageNamed:@"segmented_button_selected.png"] forState:UIControlStateSelected];
     [filterButton setBackgroundImage:[UIImage imageNamed:FILTER_BUTTON_IMAGE] forState:UIControlStateSelected];
    [self.view addSubview:filterButton];
    [filterButton addTarget:taskListViewController action:@selector(filterAction:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setupSearchBar
{
    searchBar=[[UISearchBar alloc] initWithFrame:CGRectMake(filterButton.right+235.0, 0, self.view.width-filterButton.width-235.0, 44.5)];
    searchBar.backgroundColor=[UIColor clearColor];;
    searchBar.placeholder = @"Search...";
    //searchBar.tintColor=LOGIN_BG_COLOR;
    searchBar.autoresizingMask=UIViewAutoresizingFlexibleWidth;
    searchBar.delegate= taskListViewController;
    [self.view addSubview:searchBar];
    
   // [searchBar addBackgroundImage:[[UIImage imageNamed:@"segmented_button.png"] stretchableImageWithLeftCapWidth:1 topCapHeight:0]];
    [searchBar addBackgroundImage:[UIImage imageNamed:@"search_bar_big.png"]];
}

-(void)loadTasks
{
    if(_isFetching) return;
    
    _isFetching = YES;
    
    if(self.customer)
    {
       // NSMutableArray *tasks=[CoreDataHandler allTasksForCustomer:self.customer]; 
        
        [CoreDataHandler fetchAllTasksForCustomer:self.customer OnCompletion:^(NSArray *result, NSError *error) 
        {
            if(result)
            {
                taskListViewController.tasks=[NSMutableArray arrayWithArray:result];
                
            }
            
            _isFetching = NO;
        }];
    }
}

-(void)fetchTasksFromServer
{
    Rep *_rep = [CoreDataHandler currentRep];
    NSNumber *wdSiteID = _rep.parentWdSite.wdSiteID;
    NSNumber *wdID = _rep.parentWdSite.parentWdID;
    
    
  __block ASIHTTPRequest *request = [ConnectionManager createTaskConnectionWithRepID:_rep.repID wdSiteID:wdSiteID wdID:wdID];
    
    [request setCompletionBlock:^{
        
        NSString *jsonString = [request responseString];
        NSLog(@"News json:%@",jsonString);
        
        NSDictionary *jsonDict = [request responseJSON];
        
        // NSLog(@"News jsonDict:%@",jsonDict);
        if(jsonDict && [jsonDict isKindOfClass:[NSDictionary class]])
        {
            BOOL _isSiteIDChanged = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
            
            BOOL _isRepDeactivated = [[jsonDict valueForKey:JSON_REP_DEACTIVATE_KEY] boolValue];
            
            if(_isSiteIDChanged)
            {
                [app appDidChangeSiteID];
            }
            else if(_isRepDeactivated)
            {
                [app repDidDeactivate];
            }
            else
            {
                NSArray *tasks      = [jsonDict valueForKey:JSON_DETAILS_KEY];
                NSArray *updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
                NSArray *deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
                              
                NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
                [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_TASKS];
                
                [Task populateAsyncPerformingDelete:deletedIDs?deletedIDs:[NSArray array] update:updatedIDs?updatedIDs:[NSArray array] insertOnArray:tasks onAttribute:@"taskID" completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error) 
                 {
                     
                     NSMutableArray *dataSource = taskListViewController.tasks;
                
                     if([deletedResult count])
                         [dataSource removeObjectsInArray:deletedResult];
                     
                     
                     
                     if([insertedResult count])
                         [dataSource addObjectsFromArray:insertedResult];
                     
                     
                     [taskListViewController.tableView reloadData];
                     
                     //[ProgressHUD hideHUDForView:self.view animated:YES];
                     
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         
                         NSManagedObjectContext *context=[[NSManagedObjectContext alloc] init] ;
                         [context setUndoManager:nil];
                         [context setPersistentStoreCoordinator:DEFAULT_CONTEXT.persistentStoreCoordinator];
                         
                         for(Task *task in deletedResult)
                         {
                             Task *_task =  (Task *)[task inContext:context];
                             if(_task.eventIdentifier.length)
                             {
                                 
                                 EKEvent *eventToRemove = [[CustomEventStore sharedInstance].eventStore eventWithIdentifier:_task.eventIdentifier];
                                 if(eventToRemove)
                                     [[CustomEventStore sharedInstance].eventStore removeEvent:eventToRemove span:EKSpanThisEvent error:nil];
                                 
                             }
                         }
                         
                         for(Task *task  in insertedResult)
                         {
                             Task *_task =(Task *)[task inContext:context];
                             
                             //if(task.eventIdentifier.length)
                             {
                                 EKEvent * event =nil;
                                 NSString *eventIdentifier = _task.eventIdentifier;
                                 
                                  NSString *customerName =[NSString stringWithFormat:@"Task: %@", _task.subject.length?_task.subject : @"task"];
                                 
                                 if(eventIdentifier.length)
                                     event = [[CustomEventStore sharedInstance].eventStore eventWithIdentifier:eventIdentifier];
                                 
                                 if(!event)
                                 {
                                     event = [CustomEventStore addEventWithTitle:customerName startDate:_task.dueDate endDate:[_task.dueDate dateByAddingTimeInterval:1.0] location:nil notes:nil];  
                                 }
                                 else 
                                 {
                                     event = [CustomEventStore editEventForIdentifier:_task.eventIdentifier withTitle:customerName startDate:_task.dueDate endDate:[_task.dueDate dateByAddingTimeInterval:1.0] location:nil notes:nil];
                                 }
                                 
                                 _task.eventIdentifier = event.eventIdentifier;
                             } 
                             
                             
                         }
                         
                         for(Task *task in updatedResult)
                         {
                              Task *_task =  (Task *)[task inContext:context];
                             if(_task.eventIdentifier.length)
                             {
                                 NSString *customerName =[NSString stringWithFormat:@"Task: %@", _task.subject.length?_task.subject : @"task"];
                                 
                                 [CustomEventStore editEventForIdentifier:_task.eventIdentifier withTitle:customerName startDate:_task.dueDate endDate:[_task.dueDate dateByAddingTimeInterval:1.0] location:nil notes:nil];
                             }
                         }
                         
                         [[SSCoreDataManager sharedManager] saveContext:context error:nil];
                         
                         [context reset];
                         [context release];
                     });
                 }];
            }
        }
    }];
    
    [request setFailedBlock:^{
        
        //[ConnectionManager handleError:request.error];
        //[ProgressHUD hideHUDForView:self.view animated:YES]; 
    }];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self addBackButtonWithSelector:@selector(backButtonAction)];
    //[self addDefaultBorder];
    
    UIImage *topBarImage=[UIImage imageNamed:TOP_BAR_IMAGE];
    UIImageView *topBarImageView=[[UIImageView alloc] initWithImage:topBarImage];
    topBarImageView.frame = CGRectMake(112.0, 0, topBarImage.size.width, topBarImage.size.height);
    [self.view addSubview:topBarImageView];
    [topBarImageView release];
    
     [self setupFilterButton];
     [self setupSegmentedControl];
     [self setupSearchBar];
    
    /*
    UIButton *addNewButton=[UIButton buttonWithType:UIButtonTypeCustom];
    addNewButton.frame=CGRectMake(self.view.width-126, 0, 126, 44);
    [addNewButton setBackgroundImage:[UIImage imageNamed:@"create_new_button.png"] forState:UIControlStateNormal];
    [addNewButton setBackgroundImage:[UIImage imageNamed:@"create_new_button_selected.png"] forState:UIControlStateHighlighted];
    addNewButton.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin;
    [self.view addSubview:addNewButton];
    [addNewButton addTarget:self action:@selector(createNewTaskAction:) forControlEvents:UIControlEventTouchUpInside];
     */
    
     _containerRect=CGRectMake(0, 44, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-44);
    _parallelStackViewController.view.frame=_containerRect;
    _parallelStackViewController.view.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0];
    //[_parallelStackViewController.view setAutoresizingMasks];
    [self.view addSubview:_parallelStackViewController.view];
    
    [self addChildViewController:_parallelStackViewController];
    
     [self loadTasks];
    
     //[self fetchTasksFromServer];
}

-(void)selectTask:(Task *)aTask animated:(BOOL)animated
{
    if(aTask)
    {
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSUInteger _taskCount = 0;
                
                while (!_taskCount)
                {
                    _taskCount = self.taskListViewController.tasks.count;;
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.taskListViewController selectTask:aTask animated:animated];
                });
            });
           
            /*
            [self performBlock:^{
                
               [self.taskListViewController selectTask:aTask animated:animated];
                
            } afterDealy:0.9];//0.9
            */
        }
        else
        {
          [self.taskListViewController selectTask:aTask animated:animated];
        }
    }
}

-(void)backButtonAction
{
    if(self.navigationController)
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissModalViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadTasks];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //[_parallelStackViewController hideDetailController:_parallelStackViewController.detailController];
}


-(TaskDetailViewController *)taskDetailController
{
    return (TaskDetailViewController * )_parallelStackViewController.detailController;
}


-(void)segmentedControlValChanged:(SSSegmentedControl *)segControl
{
    NSInteger index = segControl.selectedSegmentIndex;
    
    NSLog(@"selected Index:%d",index);
    
    
   // [taskListViewController filterTabDidSelectForIndex:index];
    
    //[taskListViewController segmentedTabDidSelectForIndex:index];
    
    [taskListViewController didSelectSegmentAtIndex:index];
}

/*
-(void)createNewTaskAction:(UIButton *)sender
{
    
//    Task *newTask=(Task *)[CoreDataHandler newTask];
//    
//    if(self.customer) // FOR CUSTOMER TASK ONLY
//    {
//        newTask.parentCustomer=self.customer;
//    }
    
    
    UIPopoverController  *_createTaskPopOver = self.createTaskPopOver;
    
    if(!_createTaskPopOver)
    {
    CreateTaskViewController *taskViewController=[[CreateTaskViewController alloc] initWithTask:nil];
    taskViewController.contentSizeForViewInPopover=CGSizeMake(320.0, 460.0);
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:taskViewController];
    navController.navigationBarHidden=YES;
    
    _createTaskPopOver = [[UIPopoverController alloc] initWithContentViewController:navController];

    self.createTaskPopOver=_createTaskPopOver;
    taskViewController.popoverController = _createTaskPopOver;
    
    [taskViewController addSaveTaskCallback:^(Task *_task) 
    {
        [self.customer addTasksObject:_task];
        
        NSError *error=nil;
        [[SSCoreDataManager sharedManager] save:error];
        //if(!error)[self newTaskDidAdd:_task];
        
        self.createTaskPopOver = nil;
    }];
        
    [taskViewController addCancelTaskCallback:^{
        
        self.createTaskPopOver = nil;
    }];
    
    [taskViewController release];
    [navController release];
        [_createTaskPopOver release];
   // [newTask release];
    }
    
    
    [_createTaskPopOver presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}
*/
/*
-(void)createNewTaskAction:(UIButton *)sender
{
        CreateTaskViewController *taskViewController=[[CreateTaskViewController alloc] initWithTask:nil];

        [taskListViewController.parallelStackViewController showDetailController:taskViewController animated:YES];
        
        
        [taskViewController addSaveTaskCallback:^(Task *_task) 
         {
             [self.customer addTasksObject:_task];
             
             NSError *error=nil;
             [[SSCoreDataManager sharedManager] save:error];
             //if(!error)[self newTaskDidAdd:_task];
             
             [taskListViewController.parallelStackViewController hideDetailController:taskViewController animated:YES];
             
         }];
        
        [taskViewController addCancelTaskCallback:^{
            
            [taskListViewController.parallelStackViewController hideDetailController:taskViewController animated:YES];
       
        }];
        
        [taskViewController release];

}
*/

-(void)newTaskDidAdd:(Task *)task
{
    [self loadTasks];
}

-(void)taskDidEdit:(Task *)task
{
    [self loadTasks];
}

-(void)showTaskPopover
{
   
    // [navController release];
}

-(void)newTaskAdded:(NSNotification *)notif
{
    Task *task=[notif object];
    [self newTaskDidAdd:task];
}

-(BOOL)shouldShowCustomerProfile
{
    return NO;
}

-(BOOL)allowCustomerList
{
    return NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
