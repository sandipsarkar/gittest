//
//  FilterTaskViewController.h
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum 
{
    FilterTypeAll=0,
    FilterTypeStarred,
    FilterTypeDuedate,
    FilterTypeCompleted,
    
    
}FilterType;

typedef void(^FilterTaskCallback)(FilterType filterType);
typedef void(^CancelFilterCallback)();

@protocol FilterTaskDelegate;

@interface FilterTaskViewController : UITableViewController

@property(nonatomic,assign) id <FilterTaskDelegate> delegate;

-(void)addFilterTaskCallback:(FilterTaskCallback)callback;
-(void)addCancelFilterCallback:(CancelFilterCallback )callback;

- (id)initWithFilterType:(FilterType)filterType;

@end

@protocol FilterTaskDelegate <NSObject>

@optional

-(void)filterTaskViewController:(FilterTaskViewController *)filterTaskVC didFilterForType:(FilterType)filterType;
-(void)filterTaskViewControllerDidCancel:(FilterTaskViewController *)filterTaskVC;

@end

