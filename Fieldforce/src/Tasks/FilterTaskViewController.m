//
//  FilterTaskViewController.m
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "FilterTaskViewController.h"

@interface FilterTaskViewController()
{
    NSArray *filterTaskTitles;
}

@property(nonatomic,copy)FilterTaskCallback filterTaskCallback;
@property(nonatomic,copy)CancelFilterCallback cancelFilterCallback;
@property(nonatomic,retain) NSIndexPath *selectedIndexPath;
@end

@implementation FilterTaskViewController
@synthesize filterTaskCallback,cancelFilterCallback;
@synthesize selectedIndexPath;
@synthesize delegate;

- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        // Custom initialization
        
       // filterTaskTitles = [[NSMutableArray alloc] initWithObjects:@"All",@"Show Starred",@"Sort by Due date",@"Show Completed",nil];
        
        filterTaskTitles = [[NSMutableArray alloc] initWithObjects:@"All",@"Show Starred",@"Sort by Due Date",nil];
    }
    return self;
}

- (id)initWithFilterType:(FilterType)filterType
{
    self = [self init];
    
    self.selectedIndexPath = [NSIndexPath indexPathForRow:filterType inSection:0];
    
    return self;
}

-(void)dealloc
{
    delegate = nil;
    [filterTaskTitles release];
    
    if(filterTaskCallback) [filterTaskCallback release];
    if(cancelFilterCallback) [cancelFilterCallback release];
    [selectedIndexPath release] ;
    selectedIndexPath = nil;
    
    
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)setupFooterView
{
    return;
    
    CGRect frame = self.tableView.tableHeaderView.frame;
    frame.size.height = 10.0;
    UIView *headerView = [[UIView alloc] initWithFrame:frame];
    headerView.backgroundColor = [UIColor whiteColor];
    self.tableView.tableFooterView = headerView;
    [headerView release];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif

    self.navigationController.navigationBar.barStyle=UIBarStyleBlack;
    // self.clearsSelectionOnViewWillAppear = NO;
    self.tableView.bounces = NO;
    self.tableView.showsVerticalScrollIndicator = NO;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [self setupTableHeaderView];
        self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
        self.tableView.sectionFooterHeight = 10.0f;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }

    
    [self setupFooterView];
 
    if(!self.selectedIndexPath)
        self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView reloadData];
}

-(void)setupTableHeaderView
{
    CGRect frame = self.tableView.tableHeaderView.frame;
    frame.size.height = 10.0;
    UIView *headerView = [[UIView alloc] initWithFrame:frame];
    headerView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = headerView;
    [headerView release];
}

-(void)addFilterTaskCallback:(FilterTaskCallback)callback
{
    if(callback) self.filterTaskCallback = callback;
}
-(void)addCancelFilterCallback:(CancelFilterCallback )callback
{
    if(callback) self.cancelFilterCallback=callback;
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.contentSizeForViewInPopover = CGSizeMake(320.0, self.tableView.contentSize.height);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [filterTaskTitles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.font = [UIFont grotesqueFontOfSize:16.0];
    }
    
    NSString *title = [filterTaskTitles objectAtIndex:indexPath.row];
    
    cell.textLabel.text = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [NSString stringWithFormat:@"  %@",title] : title;
    
    cell.accessoryType = [self.selectedIndexPath isEqual:indexPath]? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.selectedIndexPath isEqual:indexPath]) return;
    
    NSMutableArray *indexPathToReload=[NSMutableArray array];
    
    if(self.selectedIndexPath) [indexPathToReload addObject:self.selectedIndexPath];
    [indexPathToReload addObject:indexPath];
    self.selectedIndexPath=indexPath;
    
    if([indexPathToReload count])
    [tableView reloadRowsAtIndexPaths:indexPathToReload withRowAnimation:UITableViewRowAnimationNone];
    
    if(self.filterTaskCallback)
    self.filterTaskCallback(self.selectedIndexPath.row);
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(filterTaskViewController:didFilterForType:)])
    {
        [self.delegate filterTaskViewController:self didFilterForType:self.selectedIndexPath.row];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.tag = 1;
    
    
    NSInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
}
@end
