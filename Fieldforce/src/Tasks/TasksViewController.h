//
//  TasksViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 23/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParallelStackViewController.h"
#import "TasksListViewController.h"

@class TaskDetailViewController,Customer;
@class SSSegmentedControl;

@interface TasksViewController : UIViewController
{
        UISearchBar *searchBar;
     BOOL _isFetching;
}

@property (nonatomic,readonly) TasksListViewController *taskListViewController;
@property (nonatomic,readonly) TaskDetailViewController *taskDetailViewController;
@property (nonatomic,readonly) UISearchBar *searchBar;

@property (nonatomic,retain)   Customer *customer;


-(id)initWithCustomer:(Customer *)_customer;
-(ParallelStackViewController *)parallelStackViewController;

-(void)newTaskDidAdd:(Task *)task;

-(void)segmentedControlValChanged:(SSSegmentedControl *)segControl;

-(void)selectTask:(Task *)aTask animated:(BOOL)animated;

-(void)selectTabAtIndex:(NSUInteger)index;

#pragma Overridable
-(void)loadTasks;
-(BOOL)shouldShowCustomerProfile;
-(BOOL)allowCustomerList;

@end
