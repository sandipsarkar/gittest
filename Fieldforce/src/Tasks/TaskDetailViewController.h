//
//  TaskDetailViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 23/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreateTaskViewController.h"



@class Task;
typedef void(^TaskDeleteAction) (Task *task);
typedef void(^TaskEditCallback)(Task *task);
typedef void(^TaskCancelCallback)(void);
typedef void(^TaskStarCallback)(BOOL isStarred);

@interface TaskDetailViewController : UITableViewController

@property (nonatomic,retain) Task *task;
@property (nonatomic,assign) BOOL shouldShowCustomerProfile;
@property (nonatomic,assign) BOOL allowCustomerListing;
@property(nonatomic,assign)  BOOL isEditMode;
@property(nonatomic,assign)  BOOL isInPopover;



-(id)initWithTask:(Task *)aTask;

-(void)addDeleteTaskCallback:(TaskDeleteAction)_taskDeleteAction;
-(void)addEditTaskCallback:(TaskEditCallback)callback;
-(void)addTaskCancelCallback:(TaskCancelCallback)callBack;
-(void)addTaskStarredCallback:(TaskStarCallback )callback;

-(void)setStar:(BOOL)_isStarred;

@end
