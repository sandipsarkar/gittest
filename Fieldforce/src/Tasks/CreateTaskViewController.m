//
//  CreateTaskViewController.m
//  RepVisitationTool
//
//  Created by elie maalouly on 7/5/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.

//================================= MANDATORY FIELDS ==============================
// 1.SUBJECT 2.DUE DATE
//=================================================================================

#import "CreateTaskViewController.h"
#import "DatePickerViewController.h"
#import "NSDate+Formatter.h"
#import "Task.h"
#import "Customer.h"
#import "SSCheckBox.h"
#import "KTTextView.h"
#import "CustomerSummaryViewController.h"
#import "CustomerListViewController.h"
#import "Customer.h"
#import "CustomEventStore.h"
#import "OfflineManager.h"
#import "InsetTableViewCell.h"

#define TEXTVIEW_PLACEHOLDER @"Description"

#define DESCRIPTION_MIN_HEIGHT 100.0
#define DESCRIPTION_MAX_HEIGHT 250.0

NSString *const TaskDidAddEditNotification = @"_TaskDidAddEditNotification";



@interface CreateTaskViewController ()<UITextFieldDelegate,UITextViewDelegate>
{
    BOOL isStarred;
    CGFloat _textViewHeight;
    UIPopoverController *custPopOver;
    NSString *strCustomername;
    UITextView *descriptiontxtView;
    UITextField *insertTasktxtField;
    Customer *_customer;
   
}


@property(nonatomic,retain)Customer *_customer;
@property(nonatomic,retain) UIPopoverController *custPopOver;
@property(nonatomic,copy) SaveTaskCallback saveTaskCallback;
@property(nonatomic,copy) CancelTaskCallback cancelTaskCallback;
@property(nonatomic,copy) StarredTaskCallback starredTaskCallback;
@property(nonatomic,retain) NSDate *dueDate;
@property(nonatomic,retain) NSString *strCustomername;

-(void)addInnerShadowToCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end

@implementation CreateTaskViewController
@synthesize taskNotes, taskTitle;
@synthesize saveTaskCallback;
@synthesize taskDueDateString;

@synthesize dueDate;
@synthesize task;
@synthesize cancelTaskCallback;
@synthesize custPopOver;
@synthesize strCustomername;
@synthesize starredTaskCallback;
@synthesize _customer;
@synthesize isFromCalnederTaskDatePicker;
@synthesize isInPopover;
//@synthesize delegate;

-(void)dealloc
{
    [taskNotes release];
    [taskTitle release];
    [taskDueDateString release];
    [dueDate release];
    [custPopOver release];
    [_customer release];
    
    [task release];
    
    if(saveTaskCallback)
    [saveTaskCallback release];
    
    if(cancelTaskCallback)
    [cancelTaskCallback release];
    
    [starredTaskCallback release];
    
    [strCustomername release];
    
    [super dealloc];
}

- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) 
    {
       if( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
       
    }
    return self;
}

-(id)initWithTaskNote:(NSString *)taskNote
{
    self=[self init];
    
    if(self)
    {
        self.taskNotes=taskNote;
    }
    
    return self;
}

-(id)initWithTask:(Task *)aTask
{
    self=[self init];
    
    if(self)
    {
        self.task=aTask;
        

        [self loadDataFromTask:aTask];
    }
    
    return self;
}

-(void)loadDataFromTask:(Task *)aTask
{
    self.taskTitle = aTask.subject ;
    self.taskNotes = aTask.notes;
    self.dueDate   = aTask.dueDate;
    //self.taskDueDateString = [aTask.dueDate stringFromDateWithFormat:NEWS_DATE_FORMAT];
    self.taskDueDateString = [aTask.dueDate stringFromDateWithFormat:DATE_PICKER_FORMAT];
    
    isStarred = [aTask.isStarred boolValue];
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)setupTableFooterView
{
    CGSize sizeInPopover=self.contentSizeForViewInPopover;
    UIView *footerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 100.0)];
    footerView.backgroundColor = [UIColor clearColor];
    //[UIColor colorWithRed:242.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:1.0];
    
    //CGRect datePickerFrame=CGRectMake(0.0, 2.0, CGRectGetWidth(footerView.bounds), 200.0);
    CGRect datePickerFrame=CGRectMake(0.0, 2.0, CGRectGetWidth(footerView.bounds), 10.0);
    
    /*
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    datePicker.backgroundColor=[UIColor clearColor];
    datePicker.autoresizingMask=UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    datePicker.minimumDate=[NSDate date];
    datePicker.date=self.dueDate?self.dueDate:[NSDate date];
    [datePicker addTarget:self action:@selector(dateDidChange:) forControlEvents:UIControlEventValueChanged];
    [footerView addSubview:datePicker];
    [datePicker release];
    */
    
    UIButton *cancelButton = [UIButton repToolCancelButton];
    CGRect cancelFrame = cancelButton.frame;
    cancelFrame.origin.y = CGRectGetMaxY(datePickerFrame)+5.0;
    cancelFrame.origin.x = 10.0f;
    cancelButton.frame = cancelFrame;
    cancelButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    cancelButton.tag=1;
    [cancelButton setTitle:@"CANCEL" forState:UIControlStateNormal];
    [footerView addSubview:cancelButton];
    cancelButton.autoresizingMask =UIViewAutoresizingFlexibleRightMargin;
    [cancelButton addTarget:self action: @selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *saveButton = [UIButton repToolSaveButton];
    CGRect saveFrame = cancelButton.frame;
    saveFrame.origin.y = CGRectGetMinY(cancelButton.frame);
    saveFrame.origin.x = CGRectGetMaxX(cancelButton.frame)+5.0;
    saveButton.frame = saveFrame;
    saveButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    saveButton.tag=2;
    //[cancelButton setTitle:@"CANCEL" forState:UIControlStateNormal];
    [footerView addSubview:saveButton];
    saveButton.autoresizingMask =UIViewAutoresizingFlexibleRightMargin;
    [saveButton addTarget:self action: @selector(saveButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.tableView.tableFooterView=footerView;
    [footerView release];
    
    //======================= Should be visible only in EDit mode =========================== //
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif
    
    [self setupTableFooterView];
    self.tableView.bounces = NO;
    self.tableView.sectionFooterHeight = 0.0;
    self.tableView.sectionHeaderHeight = 10.0;
    
    [self addTitle:@"NEW TASK" withFont:[UIFont subaruBoldFontOfSize:22.0]];
    
    
    self.contentSizeForViewInPopover=CGSizeMake(320.0, self.tableView.contentSize.height);
    
    
    UIView *bgView=[[UIView alloc] initWithFrame:self.view.bounds];
   // bgView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:1.0];
    bgView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView =bgView;
    [bgView release];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [self addBackButtonWithImage:[UIImage imageNamed:NAV_BACK_BUTTON] title:@"Back" selector:nil];
        
        self.tableView.sectionFooterHeight = 1.0;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 10.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        self.tableView.tableHeaderView = _headerView;
        [_headerView release];
    }

     
}

-(void)cancelAction:(id)sender
{
    [self.popoverController dismissPopoverAnimated:YES];
    
    
   /// if([self.delegate respondsToSelector:@selector(dismissPopOverFromCalenderTask:)])
       // [self.delegate dismissPopOverFromCalenderTask:NO];
    
    if(self.cancelTaskCallback) self.cancelTaskCallback();
}


-(void)saveButtonAction:(id)sender
{
    BOOL isTitleValid = [self.taskTitle isValid];
    BOOL isDueDateValid = self.dueDate!=nil;
    
    if(!isTitleValid)
    {
         [UIAlertView showWarningAlertWithTitle:INVALID_FIELD_HEADER message:TASKNAME_VALIDATE_MSG];
        return;
    }
   else if(!isDueDateValid)
    {
        [UIAlertView showWarningAlertWithTitle:INVALID_FIELD_HEADER message:TASKDATE_VALIDATE_MSG];
        return;
    }

        
    if(!self.task)
    {
      Task *newTask=(Task *)[CoreDataHandler newTask];
      self.task = newTask;
      [newTask release];
    }
    
   // self.task.parentCustomer=self._customer;
    
    if(self._customer)
    [self._customer addTasksObject:self.task];
    
    self.task.subject   = self.taskTitle;
    self.task.notes     = self.taskNotes;
    self.task.dueDate   = self.dueDate;
    self.task.isStarred = [NSNumber numberWithBool:isStarred];
    
    if(self.strCustomername.length)
       self.task.customerName=self.strCustomername;
    

    if(self.saveTaskCallback) self.saveTaskCallback(self.task);
    
    [[SSCoreDataManager sharedManager] save:nil];
    [[OfflineManager sharedManager] addTask:self.task willDelete:NO isEditMode:NO];

    [self.popoverController dismissPopoverAnimated:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TaskDidAddEditNotification object:self.task];
    
    //================= ADD NEW EVENT FOR NEW TASK ================================
    
    NSString *taskEventTitle = [NSString stringWithFormat:@"Task:%@", self.task.subject];
    
    @try
    {
        
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     dispatch_async([app eventQueue],^{
    EKEvent *_event=nil;
        
    //if(app.eventFetchCount>=2)
    _event=[CustomEventStore addEventWithTitle:taskEventTitle startDate:self.task.dueDate endDate:[[self.task.dueDate dateByAddingTimeInterval:60.0f] currentDateHourMinute] location:nil notes:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(_event)
            self.task.eventIdentifier = _event.eventIdentifier;
            [[SSCoreDataManager sharedManager] save:nil];
            
        });
    });
   
    }
    @catch (NSException *exception)
    {
        [exception print];
    }
    @finally
    {
        
        /*
        if(self.saveTaskCallback) self.saveTaskCallback(self.task);
        
        [[SSCoreDataManager sharedManager] save:nil];
        

        [[OfflineManager sharedManager] addTask:self.task willDelete:NO isEditMode:NO];

        
        [self.popoverController dismissPopoverAnimated:YES];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:TaskDidAddEditNotification object:self.task];
         */
        [self.view endEditing:YES];
         
    }
}

-(void)addSaveTaskCallback:(SaveTaskCallback)callback
{
    if(callback)
    self.saveTaskCallback=callback;
}

-(void)addCancelTaskCallback:(CancelTaskCallback)callback
{
    if(callback)
    self.cancelTaskCallback = callback;
}

-(void)addStarredTaskCallback:(StarredTaskCallback )_starredTaskCallback
{
    if(_starredTaskCallback)
        self.starredTaskCallback = _starredTaskCallback;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    self.preferredContentSize=CGSizeMake(320.0+29, self.tableView.contentSize.height-1);
else
    self.contentSizeForViewInPopover=CGSizeMake(320.0+29.0, self.tableView.contentSize.height-1);

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    self.preferredContentSize=CGSizeMake(320.0+30.0, self.tableView.contentSize.height);
else
    self.contentSizeForViewInPopover=CGSizeMake(320.0+30.0, self.tableView.contentSize.height);

    
   // if(self.popoverController)
  //  self.popoverController.popoverContentSize = self.contentSizeForViewInPopover;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   // return 2;
    
    if(app.fromCustomerTask==1)
    {
        return 3;
    }
    else if(app.fromCustomerTask==2)
    {
      return 4;
    }
    
    else
        return 0;
    
    
     //return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   // return (section==0)?2:1;
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat height=tableView.rowHeight;
    
   if(indexPath.section==1 && indexPath.row==0) height=MAX(DESCRIPTION_MIN_HEIGHT,_textViewHeight);
   else if(indexPath.section==0 && indexPath.row==0) height=tableView.rowHeight;
       
    return height;
}


-(void)addInnerShadowToCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height=cell.contentView.frame.size.height;
    CGFloat width = cell.frame.size.width-20.0;//330.0
    //if(indexPath.section==1 && indexPath.row==0)     return;
    
    CGRect  rect = CGRectMake( 10.0f, 0.0f, width,height);
    
    CGFloat cornerRad = 8.0;
    
    UIRectCorner corners = UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight;
    
    UIBezierPath *path=[UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:corners cornerRadii:CGSizeMake( cornerRad, cornerRad)];
    
    CAShapeLayer *shapeTop = [CAShapeLayer layer]; 
    
    
    shapeTop.path = path.CGPath;
    
    cell.layer.mask = shapeTop;
    cell.layer.cornerRadius = cornerRad;
    cell.layer.borderWidth = 0.5;
    cell.layer.borderColor = [UIColor colorWithWhite:0.2 alpha:0.35].CGColor;
    cell.layer.masksToBounds= YES;
    
    [cell addInnerShadowWithColor:[UIColor colorWithWhite:0.2 alpha:0.5] rect:rect];
    
    
    UIView *bgView = [[UIView alloc] initWithFrame:cell.bounds];
    bgView.layer.cornerRadius = cornerRad;
    bgView.backgroundColor = cell.backgroundColor;
    cell.backgroundView = bgView;
    [bgView release];
    
    
}

-(void)_addInnerShadowToCell:(UITableViewCell *)cell
{
    CGRect  rect = CGRectInset(cell.backgroundView.frame, 10.0, 0.0);
    CGFloat cornerRad = cell.backgroundView.layer.cornerRadius;
    
    UIRectCorner corners = UIRectCornerAllCorners;
    
    UIBezierPath *path=[UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:corners cornerRadii:CGSizeMake( cornerRad, cornerRad)];
    
    cell.backgroundView.layer.masksToBounds= YES;
    
    CAShapeLayer *shapeTop = [CAShapeLayer layer];
    shapeTop.path = path.CGPath;
    cell.backgroundView.layer.mask = shapeTop;
    
    [cell.backgroundView addInnerShadowWithColor:[UIColor colorWithWhite:0.2 alpha:0.5] rect:rect];
}



-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [self addInnerShadowToCell:cell atIndexPath:indexPath];
        
        return;
    }
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.layer.cornerRadius=8.0;
    _bgView.tag = 1;
    
    
    NSInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
   // [self _addInnerShadowToCell:cell];
    
    [_bgView release];
}






-(UITableViewCell *)createCellForCustomerTask:(NSIndexPath *)indexPath
{
     Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    static NSString *CellIdentifier1 = @"Cell_name";
    static NSString *CellIdentifier2 = @"Cell_notes";
    static NSString *CellIdentifier3 = @"Cell_date";
    //static NSString *CellIdentifier4 = @"Cell_datePicker";
    
    
    UITableViewCell *cell=nil;
    if(indexPath.section==0 && indexPath.row==0)
    {
        SSCheckBox * _completedCheckBox=nil;
        SSCheckBox *_starView=nil;
        UITextField *textField=nil;
        
        //KTTextView *taskNameTextView = nil;
        
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
            
            /*
             _completedCheckBox=[[SSCheckBox alloc] initWithFrame:CGRectMake(10, (CGRectGetHeight(cell.contentView.bounds)-26)/2.0, 25, 26.0)];
             [_completedCheckBox setBackgroundImageForNormalState:[UIImage imageNamed:@"unchecked.png"] selectedSate:[UIImage imageNamed:@"checked.png"]];
             _completedCheckBox.tag=1;
             [cell.contentView addSubview:_completedCheckBox];
             [_completedCheckBox release];
             */
            
            CGRect _starViewFrame=CGRectMake(10, (CGRectGetHeight(cell.contentView.bounds)-26)/2.0, 28, 27.0);
            //_starViewFrame.origin.x=CGRectGetMaxX(_starViewFrame)+10;
            //_starViewFrame.origin.y=(CGRectGetHeight(cell.contentView.bounds)-27)/2.0;
            //_starViewFrame.size=CGSizeMake(28, 27);
            
            _starView=[[SSCheckBox alloc] initWithFrame:_starViewFrame];
            [_starView setBackgroundImageForNormalState:[UIImage imageNamed:@"unstarred.png"] selectedSate:[UIImage imageNamed:@"starred.png"]];
            _starView.backgroundColor=[UIColor clearColor];
            [_starView addTarget:self action:@selector(starAction:) forControlEvents:UIControlEventTouchUpInside];
            _starView.tag=2;
            [cell.contentView addSubview:_starView];
            
            [_starView release];
            
            CGRect _nameLabelFrame=CGRectMake(CGRectGetMaxX(_starViewFrame)+10, 2, CGRectGetWidth(cell.contentView.bounds)-CGRectGetMaxX(_starViewFrame)-10, CGRectGetHeight(cell.contentView.bounds)-4.0);
            
            
            textField=[[UITextField alloc] initWithFrame:_nameLabelFrame];
            textField.delegate=self;
            textField.tag=3;
            textField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
            textField.placeholder=@"Insert task name *";
            textField.textColor=[UIColor blackColor];
            textField.backgroundColor=[UIColor clearColor];
            textField.font = [UIFont grotesqueFontOfSize:14.0];
            [cell.contentView addSubview:textField];
            [textField release];
            
            
            /*
             taskNameTextView=[[KTTextView alloc] initWithFrame:_nameLabelFrame];
             taskNameTextView.delegate = self;
             taskNameTextView.tag = 3;
             taskNameTextView.placeholderText=@"Insert task name *";
             taskNameTextView.textColor=[UIColor blackColor];
             taskNameTextView.contentInset = UIEdgeInsetsMake(2.0, 0, 0, 0);
             taskNameTextView.backgroundColor=[UIColor clearColor];
             taskNameTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight| UIViewAutoresizingFlexibleBottomMargin;
             [cell.contentView addSubview:taskNameTextView];
             [taskNameTextView release];
             */
            
        }
        else
        {
            _completedCheckBox=(SSCheckBox *)[cell.contentView viewWithTag:1];
            _starView=(SSCheckBox *)[cell.contentView viewWithTag:2];
            textField=(UITextField *)[cell.contentView viewWithTag:3];
            
            //taskNameTextView=(KTTextView *)[cell.contentView viewWithTag:3];
            
        }
        textField.text=self.taskTitle?self.taskTitle:nil;
        //taskNameTextView.text = self.taskTitle?self.taskTitle:nil;
        
        _starView.checked=isStarred;
        
        _completedCheckBox.checked=NO;
        
    }
    else if(indexPath.section==1 && indexPath.row==0)
    {
        KTTextView *notesView=nil;
        
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            
            notesView=[[KTTextView alloc]initWithFrame:CGRectMake(5.0, 3.0, CGRectGetWidth(cell.contentView.bounds)-10.0, CGRectGetHeight(cell.contentView.bounds)-6.0)];
            notesView.delegate=self;
            notesView.font = [UIFont grotesqueFontOfSize:14.0];
            notesView.placeholderText=TEXTVIEW_PLACEHOLDER;
            notesView.backgroundColor=[UIColor clearColor];
            notesView.textColor=[UIColor blackColor];
            notesView.autocorrectionType = UITextAutocorrectionTypeNo;
            notesView.autoresizingMask=UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight;
            notesView.tag=1;
            [cell.contentView addSubview:notesView];
            
            
            [notesView release];
        }
        else
        {
            notesView=(KTTextView *)[cell.contentView viewWithTag:1];
        }
        
        notesView.text = self.taskNotes ? self.taskNotes : @"";
        
    }
    
    else if(indexPath.section==2 && indexPath.row==0)
    {
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier3] autorelease];
            cell.textLabel.text=@"Due Date :";
            cell.textLabel.font=[UIFont grotesqueBoldFontOfSize:14.0];
            cell.textLabel.shadowColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];
            cell.textLabel.textColor=[UIColor blackColor];
            
            cell.detailTextLabel.textColor=[UIColor blackColor];
            
            cell.detailTextLabel.font=[UIFont grotesqueFontOfSize:14.0];
            cell.detailTextLabel.textAlignment=NSTextAlignmentLeft;
            cell.textLabel.shadowOffset = CGSizeMake(1.0, 1.0);
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        cell.detailTextLabel.text=self.taskDueDateString.length ? self.taskDueDateString : @"Select Date";
    }
    
    
    return cell;

}
-(UITableViewCell *)createCellForOrganizeTask:(NSIndexPath *)indexPath
{
    
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    static NSString *CellIdentifier1 = @"Cell_name";
    static NSString *CellIdentifier2 = @"Cell_notes";
    static NSString *CellIdentifier3 = @"Cell_date";
    static NSString *CellIdentifier4 = @"Cell_customer";
    //static NSString *CellIdentifier4 = @"Cell_datePicker";
    
    
    UITableViewCell *cell=nil;
    if(indexPath.section==0 && indexPath.row==0)
    {
        SSCheckBox * _completedCheckBox=nil;
        SSCheckBox *_starView=nil;
        UITextField *textField=nil;
        
        //KTTextView *taskNameTextView = nil;
        
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
            
            /*
             _completedCheckBox=[[SSCheckBox alloc] initWithFrame:CGRectMake(10, (CGRectGetHeight(cell.contentView.bounds)-26)/2.0, 25, 26.0)];
             [_completedCheckBox setBackgroundImageForNormalState:[UIImage imageNamed:@"unchecked.png"] selectedSate:[UIImage imageNamed:@"checked.png"]];
             _completedCheckBox.tag=1;
             [cell.contentView addSubview:_completedCheckBox];
             [_completedCheckBox release];
             */
            
            CGRect _starViewFrame=CGRectMake(10, (CGRectGetHeight(cell.contentView.bounds)-26)/2.0, 28, 27.0);
            //_starViewFrame.origin.x=CGRectGetMaxX(_starViewFrame)+10;
            //_starViewFrame.origin.y=(CGRectGetHeight(cell.contentView.bounds)-27)/2.0;
            //_starViewFrame.size=CGSizeMake(28, 27);
            
            _starView=[[SSCheckBox alloc] initWithFrame:_starViewFrame];
            [_starView setBackgroundImageForNormalState:[UIImage imageNamed:@"unstarred.png"] selectedSate:[UIImage imageNamed:@"starred.png"]];
            _starView.backgroundColor=[UIColor clearColor];
            [_starView addTarget:self action:@selector(starAction:) forControlEvents:UIControlEventTouchUpInside];
            _starView.tag=2;
            [cell.contentView addSubview:_starView];
            
            [_starView release];
            
            CGRect _nameLabelFrame=CGRectMake(CGRectGetMaxX(_starViewFrame)+10, 2, CGRectGetWidth(cell.contentView.bounds)-CGRectGetMaxX(_starViewFrame)-10, CGRectGetHeight(cell.contentView.bounds)-4.0);
            
            
            textField=[[UITextField alloc] initWithFrame:_nameLabelFrame];
            textField.delegate=self;
            textField.tag=3;
            textField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
            textField.placeholder=@"Insert task name *";
            textField.textColor=[UIColor blackColor];
            textField.backgroundColor=[UIColor clearColor];
            textField.font = [UIFont grotesqueFontOfSize:14.0];
            [cell.contentView addSubview:textField];
            [textField release];
            
            
            /*
             taskNameTextView=[[KTTextView alloc] initWithFrame:_nameLabelFrame];
             taskNameTextView.delegate = self;
             taskNameTextView.tag = 3;
             taskNameTextView.placeholderText=@"Insert task name *";
             taskNameTextView.textColor=[UIColor blackColor];
             taskNameTextView.contentInset = UIEdgeInsetsMake(2.0, 0, 0, 0);
             taskNameTextView.backgroundColor=[UIColor clearColor];
             taskNameTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight| UIViewAutoresizingFlexibleBottomMargin;
             [cell.contentView addSubview:taskNameTextView];
             [taskNameTextView release];
             */
            
        }
        else
        {
            _completedCheckBox=(SSCheckBox *)[cell.contentView viewWithTag:1];
            _starView=(SSCheckBox *)[cell.contentView viewWithTag:2];
            textField=(UITextField *)[cell.contentView viewWithTag:3];
            
            //taskNameTextView=(KTTextView *)[cell.contentView viewWithTag:3];
            
        }
        textField.text=self.taskTitle?self.taskTitle:nil;
        //taskNameTextView.text = self.taskTitle?self.taskTitle:nil;
        
        _starView.checked=isStarred;
        
        _completedCheckBox.checked=NO;
        
    }
    else if(indexPath.section==1 && indexPath.row==0)
    {
        KTTextView *notesView=nil;
        
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            
            notesView=[[KTTextView alloc]initWithFrame:CGRectMake(5.0, 3.0, CGRectGetWidth(cell.contentView.bounds)-10.0, CGRectGetHeight(cell.contentView.bounds)-6.0)];
            notesView.delegate=self;
            notesView.font = [UIFont grotesqueFontOfSize:14.0];
            notesView.placeholderText=TEXTVIEW_PLACEHOLDER;
            notesView.backgroundColor=[UIColor clearColor];
            notesView.textColor=[UIColor blackColor];
            notesView.autocorrectionType = UITextAutocorrectionTypeNo;
            notesView.autoresizingMask=UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight;
            notesView.tag=1;
            [cell.contentView addSubview:notesView];
            
            
            [notesView release];
        }
        else
        {
            notesView=(KTTextView *)[cell.contentView viewWithTag:1];
        }
        
        notesView.text = self.taskNotes ? self.taskNotes : @"";
        
    }
    
    else if(indexPath.section==2 && indexPath.row==0)
    {
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier4] autorelease];
            cell.textLabel.font=[UIFont grotesqueBoldFontOfSize:14.0];
            cell.textLabel.shadowColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];
            cell.textLabel.textColor=[UIColor blackColor];
            
            
        }
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        NSString *name = self._customer.name ? self._customer.name : @"Select Customer";
        cell.textLabel.text=name;
        
    }
    else if(indexPath.section==3 && indexPath.row==0)
    {
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier3] autorelease];
            cell.textLabel.text=@"Due Date :";
            cell.textLabel.font=[UIFont grotesqueBoldFontOfSize:14.0];
            cell.textLabel.shadowColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];
            cell.textLabel.textColor=[UIColor blackColor];
            
            cell.detailTextLabel.textColor=[UIColor blackColor];
            
            cell.detailTextLabel.font=[UIFont grotesqueFontOfSize:14.0];
            cell.detailTextLabel.textAlignment=NSTextAlignmentLeft;
            cell.textLabel.shadowOffset = CGSizeMake(1.0, 1.0);
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        cell.detailTextLabel.text=self.taskDueDateString.length ? self.taskDueDateString : @"Select Date"; 
    }
    
      

    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=nil;
    if(app.fromCustomerTask==1)
    {
        cell = [self createCellForCustomerTask:indexPath];
    }
    else if(app.fromCustomerTask==2)
    {
        
        cell= [self createCellForOrganizeTask:indexPath];
    }
    
    /*
    else
    {
        cell = [self createCellForCustomerTask:indexPath];
    }
    */
    
    /*
    else if(indexPath.section==1 && indexPath.row==1)
    {
        UIDatePicker *datePicker=nil;
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
        
        if (cell == nil) 
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier4] autorelease];
            
            CGRect datePickerFrame=CGRectMake(0.0, 2.0, CGRectGetWidth(cell.contentView.bounds)-5.0, CGRectGetHeight(cell.bounds)-10.0);
            
            datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
            datePicker.tag = 1;
            datePicker.datePickerMode = UIDatePickerModeDateAndTime;
            datePicker.backgroundColor=[UIColor clearColor];
            datePicker.autoresizingMask=UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
            datePicker.hidden = NO;
            
            datePicker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            [datePicker addTarget:self action:@selector(dateDidChange:) forControlEvents:UIControlEventValueChanged];
            
            [cell.contentView addSubview:datePicker];
            [datePicker release];
        }
        else
        {
            datePicker=(UIDatePicker *)[cell.contentView viewWithTag:1];
        }
        
        datePicker.minimumDate  = [NSDate date];
        datePicker.date = [NSDate date];
    }
    */
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //cell.backgroundColor = [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
    //cell.backgroundColor = [UIColor whiteColor];
    return cell;
}

-(void)starAction:(SSCheckBox *)checkBox
{
    checkBox.checked = !checkBox.checked;
    
    isStarred=checkBox.isChecked;
    //[self.taskInfo setValue:[NSString stringWithFormat:@"%d",starred] forKey:@"isStarred"];
    
    if(self.starredTaskCallback) self.starredTaskCallback(isStarred);
}

/*
-(void)dateDidChange:(UIDatePicker *)picker
{
    self.dueDate=picker.date;
    NSString *_dateString  = [picker.date stringFromDateWithFormat:NEWS_DATE_FORMAT];
    self.taskDueDateString=_dateString;
    
    NSIndexPath *indexPathToSelect=[NSIndexPath indexPathForRow:0 inSection:2];
    
    @try 
    {
        
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPathToSelect] withRowAnimation:UITableViewRowAnimationNone];
    }
    @catch (NSException *exception) 
    {
    
    }
    @finally 
    {
        
    }
}
*/

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    insertTasktxtField=textField;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSString *text=[textField.text stringByReplacingCharactersInRange:range withString:string];
    //[app.taskdict setValue:text forKey:@"TaskTitle"];
    self.taskTitle=text;
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    self.taskTitle=textField.text;
   // [textField resignFirstResponder];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    descriptiontxtView=textView;
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    
    
   
   // [ resignFirstResponder];
}

- (void)textViewDidChange:(UITextView *)textView
{

    /*
    //CGFloat fontHeight = (textView.font.ascender - textView.font.descender);
    
    CGRect newTextFrame = textView.frame;
    newTextFrame.size = textView.contentSize;
    newTextFrame.size.height = MAX(newTextFrame.size.height,DESCRIPTION_MIN_HEIGHT);
    newTextFrame.size.height = MIN(150.0,newTextFrame.size.height);
    textView.frame = newTextFrame;
    
    _textViewHeight=newTextFrame.size.height+14.0;
    
    NSIndexPath *indexPathToReload = [NSIndexPath indexPathForRow:0 inSection:1];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPathToReload];
    
    //[self.tableView beginUpdates];
   // [self.tableView endUpdates];
    
    [self addInnerShadowToCell:cell atIndexPath:indexPathToReload];
     */
}
 

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if(text.length)
    {
       NSRange _range = [text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:ESCAPE_CHARSET]];
       BOOL valid = (_range.location ==NSNotFound);
        
       if(!valid) return NO;
    }

    NSString *_text=[textView.text stringByReplacingCharactersInRange:range withString:text];
    
    if(textView.tag==3)
    {
        self.taskTitle=_text;
    }
    else 
    {
    
    self.taskNotes=_text;
    }
    
    NSLog(@"Task Title:%@",self.taskTitle);
    return YES;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


-(void)showDatePicker
{
    DatePickerViewController *datePicker=[[DatePickerViewController alloc] initWithDate:self.dueDate];
    datePicker._isFromCalendarTaskDatePicker=isFromCalnederTaskDatePicker;
    [self.navigationController pushViewController:datePicker animated:YES];
    
    [datePicker addDoneCallback:^(NSDate *selectedDate)
    {
        if(selectedDate)
        {
            self.dueDate=selectedDate;
            
        
           //self.taskDueDateString=[selectedDate stringFromDateWithFormat:NEWS_DATE_FORMAT];
           self.taskDueDateString = [selectedDate stringFromDateWithFormat:DATE_PICKER_FORMAT];
        
            if(app.fromCustomerTask==1)
            {
                NSIndexPath *indexPathToSelect=[NSIndexPath indexPathForRow:0 inSection:2];
                
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPathToSelect] withRowAnimation:UITableViewRowAnimationFade];
            }
            else
            {
                NSIndexPath *indexPathToSelect=[NSIndexPath indexPathForRow:0 inSection:3];
                
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPathToSelect] withRowAnimation:UITableViewRowAnimationFade];

            }
            
        }
    }];
    
    [datePicker release];
}

-(void)showCustomersPopoverAtIndexPath:(NSIndexPath *)indexPath
{
    UIPopoverController *popover=self.custPopOver;
    if(!popover)
    {
         CustomerListViewController *_customerListViewController=[[CustomerListViewController alloc] initWithCustomerName:@"subho"];
        
        [_customerListViewController addCustomerSelectCallback:^(Customer *customer)
         {
             
             self.strCustomername = customer.name;
             self._customer = customer;
             //self.visitingCustomer = customer;
             //self.dealerName = customer.name;
             //self.suburb = customer.suburb.length ? customer.suburb : customer.city;
             NSIndexPath *indexPathToSelect=[NSIndexPath indexPathForRow:0 inSection:2];
             
             [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPathToSelect] withRowAnimation:UITableViewRowAnimationFade];

              [self.custPopOver dismissPopoverAnimated:NO];
             
             //[btnDoneHold setImage:[UIImage imageNamed:@"done_button.png"] forState:UIControlStateNormal];
             // btnDoneHold.userInteractionEnabled=YES;
             // self.navigationItem.rightBarButtonItem.enabled = YES;
             
            // [self.custPopOver dismissPopoverAnimated:NO];
             //[self.navigationController popToRootViewControllerAnimated:YES];
             
         }];
        
        
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:_customerListViewController];
        
       /* UIButton *btndn=[UIButton buttonWithType:UIButtonTypeCustom];
        btndn.frame=CGRectMake(105, 0, 62, 30);
        [btndn setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"done_button.png" ofType:@"png"]] forState:UIControlStateNormal];
        // [btndn setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"done_button_selected" ofType:@"png"]] forState:UIControlStateHighlighted];
        btndn.backgroundColor=[UIColor clearColor];
        [btndn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *btnDone=[[UIBarButtonItem alloc]initWithCustomView:btndn];
        _customerListViewController.navigationItem.rightBarButtonItem=btnDone;
        [btnDone release];*/
        
       /* UIBarButtonItem *btnDone=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(done)];
        _customerListViewController.navigationItem.rightBarButtonItem=btnDone;
        [btnDone release];*/
        popover=[[UIPopoverController alloc] initWithContentViewController:navController];
        
        
       
        popover.popoverContentSize = CGSizeMake(335.0+15.0, 800.0);
        self.custPopOver = popover;
        
        [popover release];
        [navController release];
        [_customerListViewController release];
    }
    
    CGRect targetRect = [self.tableView rectForRowAtIndexPath:indexPath];
    [popover presentPopoverFromRect:targetRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
  
    /*
     if(self.customerListPopover &&  [self.customerListPopover isPopoverVisible])
     {
     return;
     }
     
     
     UIPopoverController *_popOver = self.customerListPopover;
     if(!_popOver)
     {
     CustomerListViewController *_customerListViewController=[[CustomerListViewController alloc] initWithCustomerName:self.dealerName];
     [_customerListViewController addCustomerSelectCallback:^(Customer *customer)
     {
     self.dealerName = customer.name;
     self.suburb = customer.suburb.length ? customer.suburb : customer.city;
     [self._tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation: UITableViewRowAnimationNone];
     
     [btnDoneHold setImage:[UIImage imageNamed:@"done_button.png"] forState:UIControlStateNormal];
     btnDoneHold.userInteractionEnabled=YES;
     
     [self.customerListPopover dismissPopoverAnimated:YES];
     }];
     
     
     _popOver =[[UIPopoverController alloc] initWithContentViewController:_customerListViewController];
     _popOver.passthroughViews     = [NSArray arrayWithObjects:self.view,self.navigationController.navigationBar,nil];
     self.customerListPopover=_popOver;
     
     _customerListViewController.popoverController=_popOver;
     [_popOver release];
     [_customerListViewController release];
     
     }
     
     CGRect _targetRect = [self._tableView rectForRowAtIndexPath:indexPath];
     [_popOver presentPopoverFromRect:_targetRect inView:self._tableView permittedArrowDirections:UIPopoverArrowDirectionRight | UIPopoverArrowDirectionLeft animated:YES];
     */
}

-(void)done
{
      [self.custPopOver dismissPopoverAnimated:NO];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(insertTasktxtField)
        [insertTasktxtField resignFirstResponder];
    
    if(descriptiontxtView)
        [descriptiontxtView resignFirstResponder];
        
    if(app.fromCustomerTask==1)
    {
      if(indexPath.section==2 && indexPath.row==0)
      {
        [self showDatePicker];
      }
        
    }
    else if(app.fromCustomerTask==2)
    {
         if(indexPath.section==2 && indexPath.row==0)
        {
            [self showCustomersPopoverAtIndexPath:indexPath];

        }
        if(indexPath.section==3 && indexPath.row==0)
        {
            [self showDatePicker];
        }
    }
}

@end
