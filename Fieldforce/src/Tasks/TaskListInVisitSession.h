//
//  TaskListInVisitSession.h
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "TasksListViewController.h"
@class Visit;

typedef void (^CreateNewTaskCallback) (void);
@interface TaskListInVisitSession : TasksListViewController

-(void)addNewTaskCallback:(CreateNewTaskCallback)callback;

@property (nonatomic,assign) Visit *visit;

-(void)markAsComplete;



@end
