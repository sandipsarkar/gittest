//
//  TaskTableViewCell.h
//  TestIndexedTableView
//
//  Created by RANDEM MAC on 29/05/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSCheckBox.h"
#import "InsetTableViewCell.h"

@class Task;

@protocol TaskTableViewCellDelegate <NSObject>

@optional

-(void)taskDidComplete:(Task *)task;

@end

typedef void (^TaskCompleteCallback) (Task *task);
typedef void (^TaskStarredCallback) (BOOL isStarred);


@interface TaskTableViewCell : InsetTableViewCell
{
    SSCheckBox *_completedCheckBox;
    SSCheckBox *_starView;
    
    UILabel *_nameLabel;
    UILabel *_customerNameLabel;
    UILabel *_taskDateLabel;
}

@property(nonatomic,readonly)SSCheckBox *completedCheckBox;
@property(nonatomic,readonly)SSCheckBox *starView;
@property(nonatomic,readonly)UILabel *nameLabel;
@property(nonatomic,readonly)UILabel *customerNameLabel;
@property(nonatomic,readonly)UILabel *taskDateLabel;



@property(nonatomic,assign)BOOL isStarred;
@property(nonatomic,assign)BOOL isCompleted;
@property(nonatomic,assign) BOOL hideCustomer;

@property(nonatomic,retain)Task * taskInfo;
@property(nonatomic,assign)id <TaskTableViewCellDelegate> delegate;

-(void)hideCustomer:(BOOL)hideCustomer;
-(void)addTaskCompleteCallback:(TaskCompleteCallback )callback;
-(void)addTaskStarredCallback:(TaskStarredCallback)callback;
-(void)setStarred:(BOOL)starred;


@end
