//
//  CreateTaskViewController.h
//  RepVisitationTool
//
//  Created by elie maalouly on 7/5/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const TaskDidAddEditNotification;


@class Task;
typedef void(^SaveTaskCallback) (Task *task);
typedef void(^CancelTaskCallback)(void);
typedef void(^StarredTaskCallback)(BOOL isStarred);
//@protocol ADDTaskCalenderDelegate <NSObject>
//@required
//- (void)dismissPopOverFromCalenderTask:(BOOL)done;
//@end
@interface CreateTaskViewController : UITableViewController
{
    // id <ADDTaskCalenderDelegate> _delegate;
     BOOL isFromCalnederTaskDatePicker;
}
@property(nonatomic,assign) BOOL isFromCalnederTaskDatePicker;
//@property(nonatomic,retain) id <ADDTaskCalenderDelegate> delegate;
@property(nonatomic,retain)NSString *taskTitle;
@property(nonatomic,retain)NSString *taskNotes;
@property(nonatomic,retain)NSString *taskDueDateString;
@property (nonatomic,retain) Task *task;
@property (nonatomic,assign) BOOL isInPopover;


-(id)initWithTaskNote:(NSString *)taskNote;
-(id)initWithTask:(Task *)aTask;
-(void)loadDataFromTask:(Task *)aTask;

-(void)addSaveTaskCallback:(SaveTaskCallback)callback;
-(void)addCancelTaskCallback:(CancelTaskCallback)callback;
-(void)addStarredTaskCallback:(StarredTaskCallback )_starredTaskCallback;


@end
