//
//  PickerViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 09/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^DoneActionCallback) (NSDate *selectedDate);
@interface DatePickerViewController : UIViewController
{
    UIDatePicker *datePicker;
    UILabel *dateLabel;
    BOOL _isFromCalendarTaskDatePicker;
}

@property(nonatomic,assign) BOOL _isFromCalendarTaskDatePicker;
@property(nonatomic,retain)NSDate *selectedDate;
@property(nonatomic,assign) BOOL isInPopover;


-(void)addDoneCallback:(DoneActionCallback)callback;

- (id)initWithDate:(NSDate *)date;

@end
