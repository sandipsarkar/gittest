//
//  TaskListInVisitSession.m
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "TaskListInVisitSession.h"
#import "TaskDetailViewController.h"
#import "TaskTableViewCell.h"
#import "Visit.h"

@interface TaskListInVisitSession()

@property(nonatomic,copy)CreateNewTaskCallback newTaskCallback;

@end
@implementation TaskListInVisitSession
@synthesize newTaskCallback;
@synthesize visit;

-(void)dealloc
{
    visit = nil;
    if(newTaskCallback) [newTaskCallback release];
    
    [super dealloc];
}

-(void)addNewTaskCallback:(CreateNewTaskCallback)callback
{
    if(callback)
        self.newTaskCallback=callback;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif
    
    self.tableView.backgroundView.backgroundColor = [UIColor clearColor];
    
    createTaskButton.enabled = ![self.visit.isVisitCompleted boolValue];
    
}

-(void)markAsComplete
{
     createTaskButton.enabled = ![self.visit.isVisitCompleted boolValue];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    TaskTableViewCell *_cell = (TaskTableViewCell *)cell;
    _cell.hideCustomer = YES;
    
    //_cell.completedCheckBox.enabled = ![self.visit.isReportCompleted boolValue];
    
    
    _cell.contentView.userInteractionEnabled = ![self.visit.isReportCompleted boolValue];
    
    

        if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
        
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.backgroundColor = [UIColor clearColor];
        
        CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
        _bgView.backgroundColor = [UIColor clearColor];
        _bgView.contentInset = CGPointMake(10.0, 0.0);
        _bgView.cornerRadius = 8.0;
        _bgView.borderColor = tableView.separatorColor;
        _bgView.fillColor = CELL_BACKGROUND_COLOR;
    
    
    CellBackgroundView *_selectedBgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _selectedBgView.backgroundColor = [UIColor clearColor];
    _selectedBgView.contentInset = CGPointMake(10.0, 0.0);
    _selectedBgView.cornerRadius = 8.0;
    _selectedBgView.borderColor = self.tableView.separatorColor;
    _selectedBgView.fillColor = [UIColor cellSelectedColorBlue];
        
        
        int numRows = [tableView numberOfRowsInSection:indexPath.section];
        
        if(numRows==1)
        {
            _bgView.position = CellPositionSingle;
        }
        else
        {
            //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
            
            if(indexPath.row==0)
            {
                _bgView.position = CellPositionTop;
            }
            else if(indexPath.row == numRows-1)
            {
                _bgView.position = CellPositionBottom;
            }
            else
            {
                _bgView.position = CellPositionMiddle ;
            }
        }
    
        _selectedBgView.position = _bgView.position;
        
        cell.backgroundView = _bgView;
        cell.selectedBackgroundView = _selectedBgView;
    
    [_bgView release];
    [_selectedBgView release];

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSMutableArray *arr = self.tasks;
    NSMutableArray *arr = [self currentArray];
    
    Task *_task = (indexPath.row < [arr count]) ?  [arr objectAtIndex:indexPath.row] :nil;
    
    if(_task)
    {
        TaskDetailViewController *__taskDetailViewController=[[TaskDetailViewController alloc] initWithTask:_task];
        __taskDetailViewController.shouldShowCustomerProfile = NO;
       // __taskDetailViewController.isEditMode = ![self.visit.isVisitCompleted boolValue];
        __taskDetailViewController.isEditMode = ![self.visit.isReportCompleted boolValue];
     
        [self.navigationController pushViewController:__taskDetailViewController animated:YES];
        //__taskDetailViewController.view.backgroundColor = [UIColor clearColor];
        
        [__taskDetailViewController addTaskCancelCallback:^{
            
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        [__taskDetailViewController addEditTaskCallback:^(Task *task) {
            
            [self sortTasksByDueDate];
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        [__taskDetailViewController addDeleteTaskCallback:^(Task *_task)
         {
             [SSCoreDataManager deleteObject:_task];
             
             NSError *error=nil;
             [[SSCoreDataManager sharedManager] save:error];
             if(!error)
             {
                 //if([self.tasks containsObject:_task])
                    // [self.tasks removeObject:_task];
                 
                 [self deleteTask:_task];
                 
                 //[self.tableView reloadData];
                 
                 [self.navigationController popViewControllerAnimated:YES];
             }
         }];
        
                
        [__taskDetailViewController release];
    }
}

-(void)createNewTaskAction:(id)sender
{
        
    if(self.newTaskCallback) self.newTaskCallback();
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
