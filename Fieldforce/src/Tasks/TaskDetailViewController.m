//
//  TaskDetailViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 23/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "TaskDetailViewController.h"
#import "Task.h"
#import "Customer.h"
#import "SSCheckBox.h"
#import "KTTextView.h"
//#import "CustomerSummaryViewController.h"
#import "CustomerProfileViewController.h"
#import "DatePickerViewController.h"
#import "CustomerListViewController.h"
#import "Customer.h"
#import "CustomEventStore.h"
#import "OfflineManager.h"
#import "InsetTableViewCell.h"

#define TEXTVIEW_PLACEHOLDER @"Description"

#define DESCRIPTION_MIN_HEIGHT 100.0
#define DESCRIPTION_MAX_HEIGHT 250.0

#define PROFILE_INFO_EDITABLE 1




@interface TaskDetailViewController()<UITextFieldDelegate,UITextViewDelegate>
{
    BOOL isStarred;
    UIDatePicker *datePicker ;
    CGFloat _textViewHeight;
    
    BOOL isCustomerAvailable;
    NSString *strCustomername;
    UIPopoverController *custPopOver;
    Customer  *_customer;
    
}
@property(nonatomic,retain)Customer  *_customer;;
@property(nonatomic,retain)   UIPopoverController *custPopOver;
@property (nonatomic,retain) NSString *strCustomername;
@property(nonatomic,retain)NSString *taskTitle;
@property(nonatomic,retain)NSString *taskNotes;
@property(nonatomic,retain)NSString *taskDueDateString;
@property(nonatomic,retain)NSDate *dueDate;

@property (nonatomic,copy) TaskDeleteAction taskDeleteAction;
@property (nonatomic,copy) TaskEditCallback taskEditCallback;
@property(nonatomic,copy)  TaskCancelCallback taskCancelCallback;
@property(nonatomic,copy)  TaskStarCallback taskStarredCallback;

@property(nonatomic,retain)UIPopoverController *editTaskPopover;

-(void)loadDataFromTask:(Task *)aTask;
-(void)addInnerShadowToCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end

@implementation TaskDetailViewController

@synthesize taskDeleteAction;
@synthesize taskEditCallback;
@synthesize taskCancelCallback;
@synthesize taskStarredCallback;

@synthesize editTaskPopover;
@synthesize custPopOver;

@synthesize taskNotes,taskTitle;
@synthesize dueDate;
@synthesize task;
@synthesize taskDueDateString;
@synthesize strCustomername;

@synthesize shouldShowCustomerProfile;
@synthesize allowCustomerListing;
@synthesize isEditMode;
@synthesize isInPopover;
@synthesize _customer;


- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    if (self) 
    {
        shouldShowCustomerProfile=YES;
        _textViewHeight=0.0; //50.0
    }
    return self;
}
-(void)dealloc
{
    [taskNotes release];
    [taskTitle release];
    [taskDueDateString release];
    [dueDate release];
    [datePicker release];

    [task release];
    [_customer release];
    
    if(taskDeleteAction)
    [taskDeleteAction release];
    
    if(taskEditCallback)
    [taskEditCallback release];
    
    if(taskCancelCallback)
    [taskCancelCallback release];
    
    if(taskStarredCallback)
    [taskStarredCallback release];
    
    [editTaskPopover release];
    
    [super dealloc];
}


-(id)initWithTask:(Task *)aTask
{
    self=[self init];
    
    if(self)
    {
        self.task=aTask;
        
       // isCustomerAvailable = (self.task.parentCustomer!=nil);
       // shouldShowCustomerProfile = isCustomerAvailable;
        //self.customerNameForThisTask
        
        [self loadDataFromTask:aTask];
        
        NSLog(@"Task ID=%@",aTask.taskID);
        
        NSLog(@"isCompleted=%@",aTask.isCompleted);
    }
    
    return self;
}

-(void)loadDataFromTask:(Task *)aTask
{
    self.taskTitle = aTask.subject ;
    self.taskNotes = aTask.notes;
    self.dueDate=aTask.dueDate;
   // self.taskDueDateString = [aTask.dueDate stringFromDateWithFormat:NEWS_DATE_FORMAT];
    self.taskDueDateString = [aTask.dueDate stringFromDateWithFormat:DATE_PICKER_FORMAT];
    
    if(aTask.parentCustomer)
    {
        self._customer=aTask.parentCustomer;
    }
    else if(aTask.customerID)
    {
        NSPredicate *_fetchPredicate = [NSPredicate predicateWithFormat:@"self.customerID=%@",aTask.customerID];
       NSMutableArray *_customers  = [Customer fetchWithPredicate:_fetchPredicate error:nil];
        
        for(Customer *__customer in _customers)
        {
            [_customer addTasksObject:aTask];
            self._customer = __customer;
            break;
        }
    }
    
    
    isStarred=[aTask.isStarred boolValue];
}


-(void)addDeleteTaskCallback:(TaskDeleteAction)_taskDeleteAction
{
    if(_taskDeleteAction)
        self.taskDeleteAction=_taskDeleteAction;
}

-(void)addEditTaskCallback:(TaskEditCallback)callback
{
    if(callback)
    self.taskEditCallback = callback;
}

-(void)addTaskCancelCallback:(TaskCancelCallback)callBack
{
    if(callBack)
    self.taskCancelCallback = callBack;
}

-(void)addTaskStarredCallback:(TaskStarCallback )callback
{
    if(callback)
    self.taskStarredCallback = callback;
}

-(void)_setDynamicHeightOfPopover
{
    CGFloat footerHeight=CGRectGetHeight(self.tableView.tableFooterView.bounds);
    CGFloat tolHeight= [self.tableView numberOfRowsInSection:0] * (self.tableView.rowHeight)+footerHeight*2;
    
    CGSize size = self.contentSizeForViewInPopover;
    size.height = MAX(size.height, tolHeight);
    size.height = MIN(size.height, [[UIScreen mainScreen] applicationFrame].size.height-20-44);
    
    self.contentSizeForViewInPopover=size;
}



- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)setupTableHeaderView
{
    /*
    UIView *headerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 42)];
    headerView.backgroundColor=[UIColor clearColor];
    
    UIButton  *_cancelButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [_cancelButton setFrame:CGRectMake(5.0, 10.0, 70, 39)];
     //[_cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [_cancelButton setBackgroundImage:[UIImage imageNamed:@"cancel_button_blue"] forState:UIControlStateNormal];
    _cancelButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin| UIViewAutoresizingFlexibleTopMargin;
    [headerView addSubview:_cancelButton];
    [_cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton  *_editButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [_editButton setFrame:CGRectMake(CGRectGetWidth(headerView.bounds)-75.0, 10.0, 70, 39)];
   // [_editButton setTitle:isEditMode?@"Done" : @"Edit" forState:UIControlStateNormal];
    [_editButton setBackgroundImage:[UIImage imageNamed:isEditMode? @"done_button_blue.png" : @"edit_button_blue.png"] forState:UIControlStateNormal];
    _editButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
    [headerView addSubview:_editButton];
    [_editButton addTarget:self action:@selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.tableView.tableHeaderView=headerView;
    [headerView release];
     */
}


#pragma mark SAVE
-(void)editAction:(UIButton *)sender
{
    isEditMode = !isEditMode;
    //[sender setTitle:isEditMode?@"Done" : @"Edit" forState:UIControlStateNormal];
   // [sender setBackgroundImage:[UIImage imageNamed:isEditMode? @"done_button_blue.png" : @"edit_button_blue.png"] forState:UIControlStateNormal];

    datePicker.enabled=isEditMode;
    datePicker.alpha = isEditMode ? 1.0 : 0.85;
    [self.tableView reloadData];
    
    //After done is hit
    if(!isEditMode)
    {
        BOOL isTitleValid = [self.taskTitle isValid];
        BOOL isDueDateValid = self.dueDate!=nil;
        
        if(!isTitleValid)
        {
            [UIAlertView showWarningAlertWithTitle:INVALID_FIELD_HEADER message:TASKNAME_VALIDATE_MSG];
            return;
        }
        else if(!isDueDateValid)
        {
            [UIAlertView showWarningAlertWithTitle:INVALID_FIELD_HEADER message:TASKDATE_VALIDATE_MSG];
            return;
        }
        

        NSLog(@"Save");
       // self.task.parentCustomer = self._customer;
        if(self._customer)
        {
            [self._customer addTasksObject:self.task];
            self.task.customerID = self._customer.customerID;
        }
        else
        {
           // self.task.wasCustomerAvailable =  (self.task.parentCustomer=!nil);
            
            [self.task.parentCustomer removeTasksObject:self.task];
            self.task.customerID = [NSNumber numberWithLongLong:0];
        }
        
        NSDate *lastDueDate = [self.task.dueDate copy];
        
        self.task.subject   = self.taskTitle;
        self.task.notes     = self.taskNotes;
        self.task.dueDate   = self.dueDate;
        self.task.isStarred = [NSNumber numberWithBool:isStarred];
        self.task.customerName = self.strCustomername;
        
        [[SSCoreDataManager sharedManager] save:nil];
        
        [self.popoverController dismissPopoverAnimated:YES];
        
       // if(self.saveTaskCallback) self.saveTaskCallback(self.task);
        
        if(self.taskEditCallback) self.taskEditCallback(self.task);
        
        NSDictionary *userInfoDict  = [NSDictionary dictionaryWithObject:lastDueDate forKey:@"lastDueDate"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:TaskDidEditNotification object:self.task userInfo:userInfoDict];
        
        [lastDueDate release];
        [self.view endEditing:YES];
        

        [[OfflineManager sharedManager] addTask:self.task willDelete:NO isEditMode:YES];

        
        //================ EDIT EVENT WITH TASK EDIT =============================
        
       // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
        
      dispatch_async([app eventQueue],^{
          
        EKEvent *event=nil;
        @try
        {
           // if(app.eventFetchCount>=2)
            event=[CustomEventStore editEventForIdentifier:self.task.eventIdentifier withTitle:self.taskTitle startDate:self.dueDate endDate:[[self.dueDate dateByAddingTimeInterval:60.0]currentDateHourMinute] location:nil notes:nil];
            
#if CREATE_EVENT_ON_EDIT
             if(!event)
             {
             event= [CustomEventStore addEventWithTitle:self.taskTitle startDate:self.dueDate endDate:[[self.dueDate dateByAddingTimeInterval:60.0]currentDateHourMinute] location:nil notes:nil];
             NSLog(@"Event identifier=%@",event.eventIdentifier);
             
             }
#endif
            
        }
        @catch (NSException *exception)
        {
            [exception print];
        }
        @finally
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(event && event.eventIdentifier.length)
                {
                    self.task.eventIdentifier = event.eventIdentifier;
                    [[SSCoreDataManager sharedManager] save:nil];
                }
            });
        }
      });
  }
}

#pragma mark -

-(void)cancelAction:(id)sender
{
    [self.popoverController dismissPopoverAnimated:YES];
    if(self.taskCancelCallback) self.taskCancelCallback();
}

-(void)setupTableFooterView
{
    //CGSize sizeInPopover=self.contentSizeForViewInPopover;
    UIView *footerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 130.0)];
    footerView.backgroundColor = [UIColor clearColor];
    //[UIColor colorWithRed:242.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:1.0];
    
    //CGRect datePickerFrame=CGRectMake(0.0, 2.0, CGRectGetWidth(footerView.bounds), 230.0);
    CGRect datePickerFrame=CGRectMake(0.0, 2.0, CGRectGetWidth(footerView.bounds), 10.0);
    
    /*
    datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    datePicker.backgroundColor=[UIColor clearColor];
    datePicker.autoresizingMask=UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    datePicker.minimumDate=[NSDate date];
    datePicker.date=self.dueDate?self.dueDate:[NSDate date];
    [datePicker addTarget:self action:@selector(dateDidChange:) forControlEvents:UIControlEventValueChanged];
    [footerView addSubview:datePicker];
    */
    
    UIButton *deleteTaskButton=[UIButton repToolButtonWithImageName:@"red_button.png" text:@"DELETE THIS TASK"];
    CGRect deleteFrame = deleteTaskButton.frame;
    deleteFrame.origin.x = (CGRectGetWidth(self.view.bounds)-deleteFrame.size.width)/2.0;
    deleteFrame.origin.y = CGRectGetHeight(datePickerFrame)+5.0;

    deleteTaskButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;;
    [footerView addSubview:deleteTaskButton];
    deleteTaskButton.frame= deleteFrame;
    [deleteTaskButton addTarget:self action:@selector(deleteTaskButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    deleteTaskButton.enabled = isEditMode;

    
    UIButton *cancelButton = [UIButton repToolCancelButton];
       CGRect cancelFrame = cancelButton.frame;
    cancelFrame.origin.y = CGRectGetMaxY(deleteTaskButton.frame)+10.0;
    cancelFrame.origin.x = 10.0f;
    cancelButton.frame = cancelFrame;
    cancelButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    cancelButton.tag=1;
    [cancelButton setTitle:@"CANCEL" forState:UIControlStateNormal];
    [footerView addSubview:cancelButton];
    cancelButton.autoresizingMask =UIViewAutoresizingFlexibleRightMargin;
    [cancelButton addTarget:self action: @selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *saveButton = [UIButton repToolSaveButton];
    CGRect saveFrame = cancelButton.frame;
    saveFrame.origin.y = CGRectGetMinY(cancelButton.frame);
    saveFrame.origin.x = CGRectGetMaxX(cancelButton.frame)+5.0;
    saveButton.frame = saveFrame;
    saveButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    saveButton.tag=2;
    //[cancelButton setTitle:@"CANCEL" forState:UIControlStateNormal];
    [footerView addSubview:saveButton];
    saveButton.autoresizingMask =UIViewAutoresizingFlexibleRightMargin;
    [saveButton addTarget:self action: @selector(editAction:) forControlEvents:UIControlEventTouchUpInside];
    
    saveButton.enabled = isEditMode && ![self.task.isCompleted boolValue];

    self.tableView.tableFooterView = footerView;
    [footerView release];
    
    
    //datePicker.enabled=isEditMode;
    //datePicker.alpha = isEditMode ? 1.0 : 0.85;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }


     [self addTitle:@"EDIT TASK" withFont:[UIFont subaruBoldFontOfSize:23.0]];
   // shouldShowCustomerProfile=YES;

    NSLog(@"%@",self.task.parentCustomer.name);
    //isCustomerAvailable = shouldShowCustomerProfile && self.task.parentCustomer!=nil;
    
    isCustomerAvailable = shouldShowCustomerProfile && (self.task.parentCustomer!=nil || self.allowCustomerListing);
    
   
    self.tableView.bounces = NO;
    
    
       
    [self setupTableHeaderView];
    [self setupTableFooterView];
    
    self.tableView.sectionFooterHeight = 0.0;
    self.tableView.sectionHeaderHeight = 10.0;
    
    
    UIView *bgView=[[UIView alloc] initWithFrame:self.view.bounds];
    bgView.backgroundColor = [ UIColor clearColor];
    //[UIColor colorWithRed:242.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:1.0];
    self.tableView.backgroundView =bgView;
    [bgView release];
    
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.tableView.sectionFooterHeight = 1.0;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 10.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        self.tableView.tableHeaderView = _headerView;
        [_headerView release];
    }

}

#pragma mark DELETE
-(void)deleteTaskButtonAction:(UIButton *)sender
{
    sender.userInteractionEnabled = NO;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TaskDidDeleteNotification object:self.task];
    
    //================ DELETE EVENT WITH TASK DELETION =======================
     NSString *eventIdentifier = [self.task.eventIdentifier copy];
    
    [[OfflineManager sharedManager] addTask:self.task willDelete:YES isEditMode:NO];
    
    if(self.taskDeleteAction)
        self.taskDeleteAction(self.task);
    
    
    dispatch_async([app eventQueue],^{
        
    if([CustomEventStore deleteEventForEventIdentifier:eventIdentifier])
    {
        NSLog(@"EKEvent deleted");
    }
        
    dispatch_async(dispatch_get_main_queue(), ^{
        
    [eventIdentifier release];
        
    });
        
    });

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //self.navigationController.navigationBarHidden=NO;
    self.contentSizeForViewInPopover = CGSizeMake(350.0,MAX(370.0, self.tableView.contentSize.height)-1);
    //[self.popoverController setPopoverContentSize:CGSizeMake(350.0,MAX(340.0, self.tableView.contentSize.height)) animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //[self _setDynamicHeightOfPopover];
    self.contentSizeForViewInPopover = CGSizeMake(350.0,MAX(370.0, self.tableView.contentSize.height));
    //[self.popoverController setPopoverContentSize:CGSizeMake(350.0,MAX(340.0, self.tableView.contentSize.height)) animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

 

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   // return isCustomerAvailable?3:2;
    
    return isCustomerAvailable?4:3;
    
    return app.fromCustomerTask==2?4:3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return (section==0)?2:1;
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height=tableView.rowHeight;
    
    if(indexPath.section==1 && indexPath.row==0)   
    {
        if(_textViewHeight == 0)
        {
            
        UIFont *fontToCheck = [UIFont grotesqueFontOfSize:14.0];
        CGFloat spaceY=5.0;
            CGFloat fontHeight = 14.0;//(fontToCheck.ascender - fontToCheck.descender) + 1;
        CGSize textHeight = [self.taskNotes sizeWithFont:fontToCheck constrainedToSize:CGSizeMake(250, CGFLOAT_MAX)];
            
            height = MAX(textHeight.height+spaceY+fontHeight,DESCRIPTION_MIN_HEIGHT);
            height = MIN(DESCRIPTION_MAX_HEIGHT,height);
        }
        else 
        {
            height =  MAX(_textViewHeight,height);
        }
    }
    else if(indexPath.section==0 && indexPath.row == 0)
    {
        return tableView.rowHeight;
        
        /*
       CGSize _taskTitleSize =  [self.taskTitle sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(165.0, CGFLOAT_MAX)];
        return MAX(_taskTitleSize.height+2.0,tableView.rowHeight); 
       */
    }
    
    return height;
}

-(void)addInnerShadowToCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height=cell.contentView.frame.size.height;
    CGFloat width = cell.frame.size.width-20.0;//330.0
    //if(indexPath.section==1 && indexPath.row==0)     return;
    
    
    cell.contentView.userInteractionEnabled=isEditMode;
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    CGRect  rect = CGRectMake( 10.0f, 0.0f, width,height);
    
    CGFloat cornerRad = 8.0;
    
    UIRectCorner corners = UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight;
    
    UIBezierPath *path=[UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:corners cornerRadii:CGSizeMake( cornerRad, cornerRad)];
    
    CAShapeLayer *shapeTop = [CAShapeLayer layer]; 
    
    
    shapeTop.path = path.CGPath;
    
    cell.layer.mask = shapeTop;
    cell.layer.cornerRadius = cornerRad;
    cell.layer.borderWidth = 0.5;
    cell.layer.borderColor = [UIColor colorWithWhite:0.2 alpha:0.35].CGColor;
    cell.layer.masksToBounds= YES;
    
    [cell addInnerShadowWithColor:[UIColor colorWithWhite:0.2 alpha:0.6] rect:rect];
    
    
     UIView *bgView = [[UIView alloc] initWithFrame:cell.bounds];
     bgView.layer.cornerRadius = cornerRad;
     bgView.backgroundColor = cell.backgroundColor;
     cell.backgroundView = bgView;
     [bgView release];
}

-(void)_addInnerShadowToCell:(UITableViewCell *)cell
{
    CGRect  rect = CGRectInset(cell.backgroundView.frame, 10.0, 0.0);
    CGFloat cornerRad = cell.backgroundView.layer.cornerRadius;
    
    UIRectCorner corners = UIRectCornerAllCorners;
    
    UIBezierPath *path=[UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:corners cornerRadii:CGSizeMake( cornerRad, cornerRad)];
    
    cell.backgroundView.layer.masksToBounds= YES;
    
    CAShapeLayer *shapeTop = [CAShapeLayer layer];
    shapeTop.path = path.CGPath;
    cell.backgroundView.layer.mask = shapeTop;
    
    [cell.backgroundView addInnerShadowWithColor:[UIColor colorWithWhite:0.2 alpha:0.5] rect:rect];
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [self addInnerShadowToCell:cell atIndexPath:indexPath];
        return;
    }
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.tag = 1;
    
    
    int numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    //[self _addInnerShadowToCell:cell];
    
    [_bgView release];
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell=nil;
    if(isCustomerAvailable)
   // if(app.fromCustomerTask==2)
    {
        cell = [self createCellForCustomerTask:indexPath];
    }
    else //if(app.fromCustomerTask==1)
    {
        
        cell= [self createCellForOrganizeTask:indexPath];
    }
    
    
    cell.userInteractionEnabled =![self.task.isCompleted boolValue];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
   // cell.backgroundColor = [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
    
  //  cell.backgroundColor = [UIColor whiteColor];
    return cell;
}
 

-(UITableViewCell *)createCellForCustomerTask:(NSIndexPath *)indexPath
{
     Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    static NSString *CellIdentifier1 = @"Cell_name";
    static NSString *CellIdentifier2 = @"Cell_notes";
    static NSString *CellIdentifier3 = @"Cell_date";
    static NSString *CellIdentifier4 = @"Cell_customer";
    //static NSString *CellIdentifier4 = @"Cell_datePicker";
    
    
    UITableViewCell *cell=nil;
    if(indexPath.section==0 && indexPath.row==0)
    {
        SSCheckBox * _completedCheckBox=nil;
        SSCheckBox *_starView=nil;
        UITextField *textField=nil;
        
        //KTTextView *taskNameTextView = nil;
        
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
            
            /*
             _completedCheckBox=[[SSCheckBox alloc] initWithFrame:CGRectMake(10, (CGRectGetHeight(cell.contentView.bounds)-26)/2.0, 25, 26.0)];
             [_completedCheckBox setBackgroundImageForNormalState:[UIImage imageNamed:@"unchecked.png"] selectedSate:[UIImage imageNamed:@"checked.png"]];
             _completedCheckBox.tag=1;
             [cell.contentView addSubview:_completedCheckBox];
             [_completedCheckBox release];
             */
            
            CGRect _starViewFrame=CGRectMake(10, (CGRectGetHeight(cell.contentView.bounds)-26)/2.0, 28, 27.0);
            //_starViewFrame.origin.x=CGRectGetMaxX(_starViewFrame)+10;
            //_starViewFrame.origin.y=(CGRectGetHeight(cell.contentView.bounds)-27)/2.0;
            //_starViewFrame.size=CGSizeMake(28, 27);
            
            _starView=[[SSCheckBox alloc] initWithFrame:_starViewFrame];
            [_starView setBackgroundImageForNormalState:[UIImage imageNamed:@"unstarred.png"] selectedSate:[UIImage imageNamed:@"starred.png"]];
            _starView.backgroundColor=[UIColor clearColor];
            [_starView addTarget:self action:@selector(starAction:) forControlEvents:UIControlEventTouchUpInside];
            _starView.tag=2;
            [cell.contentView addSubview:_starView];
            
            [_starView release];
            
            CGRect _nameLabelFrame=CGRectMake(CGRectGetMaxX(_starViewFrame)+10, 2, CGRectGetWidth(cell.contentView.bounds)-CGRectGetMaxX(_starViewFrame)-10, CGRectGetHeight(cell.contentView.bounds)-4.0);
            
            
            textField=[[UITextField alloc] initWithFrame:_nameLabelFrame];
            textField.delegate=self;
            textField.tag=3;
            textField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
            textField.placeholder=@"Insert task name *";
            textField.textColor=[UIColor blackColor];
            textField.backgroundColor=[UIColor clearColor];
            textField.font = [UIFont grotesqueBoldFontOfSize:14.0];
            [cell.contentView addSubview:textField];
            [textField release];
            
            
            /*
             taskNameTextView=[[KTTextView alloc] initWithFrame:_nameLabelFrame];
             taskNameTextView.delegate = self;
             taskNameTextView.tag = 3;
             taskNameTextView.placeholderText=@"Insert task name *";
             taskNameTextView.textColor=[UIColor blackColor];
             taskNameTextView.contentInset = UIEdgeInsetsMake(2.0, 0, 0, 0);
             taskNameTextView.backgroundColor=[UIColor clearColor];
             taskNameTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight| UIViewAutoresizingFlexibleBottomMargin;
             [cell.contentView addSubview:taskNameTextView];
             [taskNameTextView release];
             */
            
        }
        else
        {
            _completedCheckBox=(SSCheckBox *)[cell.contentView viewWithTag:1];
            _starView=(SSCheckBox *)[cell.contentView viewWithTag:2];
            textField=(UITextField *)[cell.contentView viewWithTag:3];
            
            //taskNameTextView=(KTTextView *)[cell.contentView viewWithTag:3];
            
        }
        textField.text=self.taskTitle?self.taskTitle:nil;
        //taskNameTextView.text = self.taskTitle?self.taskTitle:nil;
        
        _starView.checked=isStarred;
        
        _completedCheckBox.checked=NO;
        
    }
    else if(indexPath.section==1 && indexPath.row==0)
    {
        KTTextView *notesView=nil;
        
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            
            notesView=[[KTTextView alloc]initWithFrame:CGRectMake(5.0, 3.0, CGRectGetWidth(cell.contentView.bounds)-10.0, CGRectGetHeight(cell.contentView.bounds)-6.0)];
            notesView.delegate=self;
            notesView.font = [UIFont grotesqueFontOfSize:14.0];
            notesView.placeholderText=TEXTVIEW_PLACEHOLDER;
            
            notesView.backgroundColor=[UIColor clearColor];
            notesView.textColor=[UIColor blackColor];
            notesView.autocorrectionType = UITextAutocorrectionTypeNo;
            notesView.autoresizingMask=UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight;
            notesView.tag=1;
            [cell.contentView addSubview:notesView];
            
            [notesView release];
        }
        else
        {
            notesView=(KTTextView *)[cell.contentView viewWithTag:1];
        }
        
        notesView.text = self.taskNotes ? self.taskNotes : @"";
        
    }
    else if(indexPath.section==2 && indexPath.row==0)
    {

        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
        if (cell == nil) 
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier3] autorelease];
            
            cell.textLabel.font=[UIFont grotesqueBoldFontOfSize:14.0];
            cell.textLabel.textColor=[UIColor blackColor];
            cell.textLabel.textAlignment = UITextAlignmentLeft;
            
            cell.detailTextLabel.textColor=[UIColor blackColor];
            cell.detailTextLabel.font=[UIFont grotesqueFontOfSize:14.0];
            cell.detailTextLabel.textAlignment=UITextAlignmentRight;
            cell.detailTextLabel.shadowOffset = CGSizeMake(1.0, 1.0);
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.accessoryType = shouldShowCustomerProfile && !self.isInPopover ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;
            
        }
        
        cell.textLabel.text=@"Customer :";
        //cell.accessoryType =  UITableViewCellAccessoryDisclosureIndicator ;
        cell.detailTextLabel.text = self._customer.name.length?[self._customer.name uppercaseString]: @"Select Customer";
        
        
    }

    
    else if(indexPath.section==3 && indexPath.row==0)
    {
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier3] autorelease];
            cell.textLabel.text=@"Due Date :";
            cell.textLabel.font=[UIFont grotesqueBoldFontOfSize:14.0];
            cell.textLabel.shadowColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];
            cell.textLabel.textColor=[UIColor blackColor];
            
            cell.detailTextLabel.textColor=[UIColor blackColor];
            
            cell.detailTextLabel.font=[UIFont grotesqueFontOfSize:14.0];
            cell.detailTextLabel.textAlignment=UITextAlignmentLeft;
            cell.textLabel.shadowOffset = CGSizeMake(1.0, 1.0);
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        cell.detailTextLabel.text=self.taskDueDateString.length ? self.taskDueDateString : @"Select Date";
    }
    
    return cell;
    
}
-(UITableViewCell *)createCellForOrganizeTask:(NSIndexPath *)indexPath
{
    Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    static NSString *CellIdentifier1 = @"Cell_name";
    static NSString *CellIdentifier2 = @"Cell_notes";
    static NSString *CellIdentifier3 = @"Cell_date";
    static NSString *CellIdentifier4 = @"Cell_customer";
    //static NSString *CellIdentifier4 = @"Cell_datePicker";
    
    
    UITableViewCell *cell=nil;
    if(indexPath.section==0 && indexPath.row==0)
    {
        SSCheckBox * _completedCheckBox=nil;
        SSCheckBox *_starView=nil;
        UITextField *textField=nil;
        
        //KTTextView *taskNameTextView = nil;
        
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
            
            /*
             _completedCheckBox=[[SSCheckBox alloc] initWithFrame:CGRectMake(10, (CGRectGetHeight(cell.contentView.bounds)-26)/2.0, 25, 26.0)];
             [_completedCheckBox setBackgroundImageForNormalState:[UIImage imageNamed:@"unchecked.png"] selectedSate:[UIImage imageNamed:@"checked.png"]];
             _completedCheckBox.tag=1;
             [cell.contentView addSubview:_completedCheckBox];
             [_completedCheckBox release];
             */
            
            CGRect _starViewFrame=CGRectMake(10, (CGRectGetHeight(cell.contentView.bounds)-26)/2.0, 28, 27.0);
            //_starViewFrame.origin.x=CGRectGetMaxX(_starViewFrame)+10;
            //_starViewFrame.origin.y=(CGRectGetHeight(cell.contentView.bounds)-27)/2.0;
            //_starViewFrame.size=CGSizeMake(28, 27);
            
            _starView=[[SSCheckBox alloc] initWithFrame:_starViewFrame];
            [_starView setBackgroundImageForNormalState:[UIImage imageNamed:@"unstarred.png"] selectedSate:[UIImage imageNamed:@"starred.png"]];
            _starView.backgroundColor=[UIColor clearColor];
            [_starView addTarget:self action:@selector(starAction:) forControlEvents:UIControlEventTouchUpInside];
            _starView.tag=2;
            [cell.contentView addSubview:_starView];
            
            [_starView release];
            
            CGRect _nameLabelFrame=CGRectMake(CGRectGetMaxX(_starViewFrame)+10, 2, CGRectGetWidth(cell.contentView.bounds)-CGRectGetMaxX(_starViewFrame)-10, CGRectGetHeight(cell.contentView.bounds)-4.0);
            
            
            textField=[[UITextField alloc] initWithFrame:_nameLabelFrame];
            textField.delegate=self;
            textField.tag=3;
            textField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
            textField.placeholder=@"Insert task name *";
            textField.textColor=[UIColor blackColor];
            textField.backgroundColor=[UIColor clearColor];
            textField.font = [UIFont grotesqueBoldFontOfSize:14.0];
            [cell.contentView addSubview:textField];
            [textField release];
            
            /*
             taskNameTextView=[[KTTextView alloc] initWithFrame:_nameLabelFrame];
             taskNameTextView.delegate = self;
             taskNameTextView.tag = 3;
             taskNameTextView.placeholderText=@"Insert task name *";
             taskNameTextView.textColor=[UIColor blackColor];
             taskNameTextView.contentInset = UIEdgeInsetsMake(2.0, 0, 0, 0);
             taskNameTextView.backgroundColor=[UIColor clearColor];
             taskNameTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight| UIViewAutoresizingFlexibleBottomMargin;
             [cell.contentView addSubview:taskNameTextView];
             [taskNameTextView release];
             */
        }
        else
        {
            _completedCheckBox=(SSCheckBox *)[cell.contentView viewWithTag:1];
            _starView=(SSCheckBox *)[cell.contentView viewWithTag:2];
            textField=(UITextField *)[cell.contentView viewWithTag:3];
            
            //taskNameTextView=(KTTextView *)[cell.contentView viewWithTag:3];
            
        }
        textField.text=self.taskTitle?self.taskTitle:nil;
        //taskNameTextView.text = self.taskTitle?self.taskTitle:nil;
        
        _starView.checked=isStarred;
        
        _completedCheckBox.checked=NO;
        
    }
    else if(indexPath.section==1 && indexPath.row==0)
    {
        KTTextView *notesView=nil;
        
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            
            notesView=[[KTTextView alloc]initWithFrame:CGRectMake(5.0, 3.0, CGRectGetWidth(cell.contentView.bounds)-10.0, CGRectGetHeight(cell.contentView.bounds)-6.0)];
            notesView.delegate=self;
            notesView.font = [UIFont grotesqueFontOfSize:14.0];
            notesView.placeholderText=TEXTVIEW_PLACEHOLDER;
            notesView.backgroundColor=[UIColor clearColor];
            notesView.textColor=[UIColor blackColor];
            notesView.autocorrectionType = UITextAutocorrectionTypeNo;
            notesView.autoresizingMask=UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight;
            notesView.tag=1;
            [cell.contentView addSubview:notesView];
            
            [notesView release];
        }
        else
        {
            notesView=(KTTextView *)[cell.contentView viewWithTag:1];
        }
        
        notesView.text = self.taskNotes ? self.taskNotes : @"";
        
    }
    
    else if(indexPath.section==3 && indexPath.row==0)
    {
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier3];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier3] autorelease];
            cell.textLabel.font=[UIFont grotesqueBoldFontOfSize:14.0];
            cell.textLabel.textColor=[UIColor blackColor];
            cell.textLabel.textAlignment = UITextAlignmentLeft;
            cell.textLabel.text=@"Customer :";
            
            cell.detailTextLabel.textColor=[UIColor blackColor];
            cell.detailTextLabel.font=[UIFont grotesqueFontOfSize:14.0];
            cell.detailTextLabel.textAlignment=UITextAlignmentRight;
            cell.detailTextLabel.shadowOffset = CGSizeMake(1.0, 1.0);
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
    
        
        cell.detailTextLabel.text=self._customer.name.length?self._customer.name: @"Select Customer";
        cell.accessoryType =  self.isInPopover ? UITableViewCellAccessoryNone :  UITableViewCellAccessoryDisclosureIndicator ;
        
    }
    else if(indexPath.section==2 && indexPath.row==0)
    {
        cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier4];
        if (cell == nil)
        {
            cell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier3] autorelease];
            cell.textLabel.text=@"Due Date :";
            cell.textLabel.font=[UIFont grotesqueBoldFontOfSize:14.0];
            cell.textLabel.shadowColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];
            cell.textLabel.textColor=[UIColor blackColor];
            
            cell.detailTextLabel.textColor=[UIColor blackColor];
            
            cell.detailTextLabel.font=[UIFont grotesqueFontOfSize:14.0];
            cell.detailTextLabel.textAlignment=UITextAlignmentLeft;
            cell.textLabel.shadowOffset = CGSizeMake(1.0, 1.0);
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        cell.detailTextLabel.text=self.taskDueDateString.length ? self.taskDueDateString : @"Select Date";
    }
    
    return cell;
}


-(void)starAction:(SSCheckBox *)checkBox
{
     checkBox.checked = !checkBox.checked;
    
    isStarred=checkBox.isChecked;
    self.task.isStarred = [NSNumber numberWithBool:isStarred];
    //[self.taskInfo setValue:[NSString stringWithFormat:@"%d",starred] forKey:@"isStarred"];
    
    if(self.taskStarredCallback) self.taskStarredCallback(isStarred);
    

    [[OfflineManager sharedManager] addTask:self.task willDelete:NO isEditMode:YES];

    
}

-(void)setStar:(BOOL)_isStarred
{
    isStarred=_isStarred;
    self.task.isStarred = [NSNumber numberWithBool:isStarred];
    
   UITableViewCell *cell  = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    SSCheckBox *_starView = (SSCheckBox *)[cell.contentView viewWithTag:2];
    _starView.checked = isStarred;
}

-(void)showCustomerProfile
{
#if PROFILE_INFO_EDITABLE   
  
    CustomerProfileViewController *_customerProfileVc=[[CustomerProfileViewController alloc] initWithCustomer:self.task.parentCustomer];
    UINavigationController *_navController = [self nextResponderNavigationController];
    if(_navController)
    {
        [_navController pushViewController:_customerProfileVc animated:YES];
    }
    [_customerProfileVc release];
        
#else
    
    CustomerSummaryViewController *summaryViewController = [[CustomerSummaryViewController alloc] initWithCustomer:self.task.parentCustomer];
    
    UINavigationController *_navController = [self nextResponderNavigationController];
    if(_navController)
    {
        [_navController pushViewController:summaryViewController animated:YES];
        [summaryViewController addTitleImage:@"customer_profile.png"];
        [summaryViewController addBackButton];
    }
    [summaryViewController release];

#endif
    
}


-(void)showDatePicker
{
    if(!isEditMode) return;
    
    DatePickerViewController *datePickerVC=[[DatePickerViewController alloc] initWithDate:self.dueDate];
    datePickerVC.isInPopover = self.isInPopover;
    [self.navigationController pushViewController:datePickerVC animated:YES];
    
    [datePickerVC addDoneCallback:^(NSDate *selectedDate)
     {
         if(selectedDate)
         {
             self.dueDate=selectedDate;
             
             
            // NSString *_dateString  =[selectedDate stringFromDateWithFormat:NEWS_DATE_FORMAT]; 
               NSString *_dateString  =[selectedDate stringFromDateWithFormat:DATE_PICKER_FORMAT];
             
             self.taskDueDateString=_dateString;
             
             NSIndexPath *indexPathToSelect=[NSIndexPath indexPathForRow:0 inSection:isCustomerAvailable?3:2];
             
              //NSIndexPath *indexPathToSelect=[NSIndexPath indexPathForRow:0 inSection:(app.fromCustomerTask==2)?3:2];
             
             
             if(indexPathToSelect)
             [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPathToSelect] withRowAnimation:UITableViewRowAnimationFade];
             
         }
     }];
    
    [datePickerVC release];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int secToCheck = isCustomerAvailable?3:2;
    int _secToCheck = isCustomerAvailable?2:3;
    
    
   // int secToCheck = app.fromCustomerTask==2?3:2;
   // int _secToCheck = app.fromCustomerTask==2?2:3;
    
//    if(shouldShowCustomerProfile /*&& (indexPath.section==2 && self.task.parentCustomer)*/)
//    {
//        [self showCustomerProfile];
//        app.fromCustomerTask = 1;
//    }
    
    if (indexPath.section ==_secToCheck && indexPath.row == 0) 
    {
        if (allowCustomerListing)
        {
            [self showCustomersPopoverAtIndexPath:indexPath];
        }
        else {
            [self showCustomerProfile];
             app.fromCustomerTask = 1;
        }
        
    }
    
   /*else if(indexPath.section ==2 && indexPath.row == 0)
    {
        
           [self showCustomersPopoverAtIndexPath:indexPath];
    }*/
    
   else  if(indexPath.section ==secToCheck && indexPath.row == 0)
    {
        [self showDatePicker];
    }
    
}
-(void)showCustomersPopoverAtIndexPath:(NSIndexPath *)indexPath
{
        CustomerListViewController *_customerListViewController=[[CustomerListViewController alloc] initWithCustomerName:self._customer.name];
        
        [_customerListViewController addCustomerSelectCallback:^(Customer *customer)
         {
             
             self.strCustomername=customer.name;
             self._customer=customer;
             //self.visitingCustomer = customer;
             //self.dealerName = customer.name;
             //self.suburb = customer.suburb.length ? customer.suburb : customer.city;
             NSIndexPath *indexPathToSelect=[NSIndexPath indexPathForRow:0 inSection:2];
             
             if(indexPathToSelect)
             [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPathToSelect] withRowAnimation:UITableViewRowAnimationNone];
                 [self.custPopOver dismissPopoverAnimated:NO];

         }];

        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:_customerListViewController];
   
        
       UIPopoverController *popover=[[UIPopoverController alloc] initWithContentViewController:navController];
        
        popover.popoverContentSize = CGSizeMake(335.0+15.0, 800.0);
        self.custPopOver = popover;
        
        [popover release];
        [navController release];
        [_customerListViewController release];
/*
    else
    {
        
        UINavigationController *navController =(UINavigationController *) popover.contentViewController;
        
        CustomerListViewController *_customerListViewController =(CustomerListViewController *) navController.topViewController;
        
        _customerListViewController.selectedCustomerName = self._customer.name;
        
        
    }
 */
    
    CGRect targetRect = [self.tableView rectForRowAtIndexPath:indexPath];
    
    [self.custPopOver presentPopoverFromRect:targetRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
    
    /*
     if(self.customerListPopover &&  [self.customerListPopover isPopoverVisible])
     {
     return;
     }
     
     
     UIPopoverController *_popOver = self.customerListPopover;
     if(!_popOver)
     {
     CustomerListViewController *_customerListViewController=[[CustomerListViewController alloc] initWithCustomerName:self.dealerName];
     [_customerListViewController addCustomerSelectCallback:^(Customer *customer)
     {
     self.dealerName = customer.name;
     self.suburb = customer.suburb.length ? customer.suburb : customer.city;
     [self._tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation: UITableViewRowAnimationNone];
     
     [btnDoneHold setImage:[UIImage imageNamed:@"done_button.png"] forState:UIControlStateNormal];
     btnDoneHold.userInteractionEnabled=YES;
     
     [self.customerListPopover dismissPopoverAnimated:YES];
     }];
     
     
     _popOver =[[UIPopoverController alloc] initWithContentViewController:_customerListViewController];
     _popOver.passthroughViews     = [NSArray arrayWithObjects:self.view,self.navigationController.navigationBar,nil];
     self.customerListPopover=_popOver;
     
     _customerListViewController.popoverController=_popOver;
     [_popOver release];
     [_customerListViewController release];
     
     }
     
     CGRect _targetRect = [self._tableView rectForRowAtIndexPath:indexPath];
     [_popOver presentPopoverFromRect:_targetRect inView:self._tableView permittedArrowDirections:UIPopoverArrowDirectionRight | UIPopoverArrowDirectionLeft animated:YES];
     */
}

-(void)done
{
    [self.custPopOver dismissPopoverAnimated:NO];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text=[textField.text stringByReplacingCharactersInRange:range withString:string];
    //[app.taskdict setValue:text forKey:@"TaskTitle"];
    self.taskTitle=text;
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.taskTitle=textField.text;
}

- (void)textViewDidChange:(UITextView *)textView
{
    /*
    //CGFloat fontHeight = (textView.font.ascender - textView.font.descender) + 1;
    
    CGRect newTextFrame = textView.frame;
    newTextFrame.size = textView.contentSize;
    newTextFrame.size.height = MAX(newTextFrame.size.height,DESCRIPTION_MIN_HEIGHT);
    newTextFrame.size.height = MIN(DESCRIPTION_MAX_HEIGHT,newTextFrame.size.height);

    textView.frame = newTextFrame;
    
    _textViewHeight=newTextFrame.size.height+14.0;
    
    NSIndexPath *indexPathToReload = [NSIndexPath indexPathForRow:0 inSection:1];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPathToReload];
    
    //[self.tableView beginUpdates];
    
    //[self.tableView endUpdates];
    
    [self addInnerShadowToCell:cell atIndexPath:indexPathToReload];
     */
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if([textView.text isEqualToString:@"Insert task notes"])
    {
        textView.text=nil;
    }

    textView.textColor=[UIColor blackColor];
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    
}


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if(text.length)
    {
        NSRange _range = [text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:ESCAPE_CHARSET]];
        BOOL valid = (_range.location ==NSNotFound);
        
        if(!valid) return NO;
    }
    
    NSString *_text=[textView.text stringByReplacingCharactersInRange:range withString:text];
    
    if(textView.tag==3)
    {
        self.taskTitle=_text;
    }
    else 
    {
        
        self.taskNotes=_text;
    }
    
    NSLog(@"Task Title:%@",self.taskTitle);
    return YES;

    
    return YES;
}

-(void)dateDidChange:(UIDatePicker *)picker
{
    self.dueDate=picker.date;
    NSString *_dateString  = [picker.date stringFromDateWithFormat:NEWS_DATE_FORMAT];
    self.taskDueDateString=_dateString;
    
    NSIndexPath *indexPathToSelect = [NSIndexPath indexPathForRow:0 inSection:isCustomerAvailable?3:2];
    
    //NSIndexPath *indexPathToSelect = [NSIndexPath indexPathForRow:0 inSection: app.fromCustomerTask==2?3:2];
    
    @try 
    {
        if(indexPathToSelect)
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPathToSelect] withRowAnimation:UITableViewRowAnimationNone];
    }
    @catch (NSException *exception) 
    {
        
    }
    @finally 
    {
        
    }
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source



#pragma mark - Table view delegate



@end
