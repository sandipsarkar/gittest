//
//  TasksListViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 23/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>



@class TaskDetailViewController;
@class TasksViewController;
@interface TasksListViewController : UIViewController
{
    UIButton *createTaskButton;
    TaskDetailViewController *_taskDetailViewController ;
    
    BOOL _isArchievedTasks;
}

@property(nonatomic,retain,readonly) TaskDetailViewController *taskDetailViewController;
@property(nonatomic,assign)TasksViewController *parentController;
@property(nonatomic,retain) NSMutableArray *tasks;
@property(nonatomic,retain) UITableView *tableView;

-(id)initWithTasks:(NSMutableArray *)taskArr;

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

-(void)filterTabDidSelectForIndex:(int)index;
-(void)filterAction:(UIButton *)sender;

-(void)segmentedTabDidSelectForIndex:(int)index;

-(void)didSelectSegmentAtIndex:(int)index;

-(void)selectTask:(Task *)aTask animated:(BOOL)animated;

-(void)setupTableHeaderView;
-(void)setupTableHeaderViewWithImage:(NSString *)imageName;

-(void)refresh;

-(void)addNewTask:(Task *)aTask;
-(void)deleteTask:(Task *)aTask;

-(void)sortTasksByDueDate;
-(NSMutableArray *)currentArray;
-(void)reload;

@end
