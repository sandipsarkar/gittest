//
//  TasksListViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 23/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "TasksListViewController.h"
#import "Task.h"
#import "TaskDetailViewController.h"
#import "ParallelStackViewController.h"
#import "FilterTaskViewController.h"
#import "TasksViewController.h"
#import "TaskTableViewCell.h"
#import "Customer.h"
#import "OfflineManager.h"

@interface TasksListViewController()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,TaskTableViewCellDelegate,FilterTaskDelegate>
{
    UILabel *noTaskLabel;
    UITableView *_tableView;
    FilterType _selectedFilterType;
}

@property(nonatomic,retain,readwrite)TaskDetailViewController *taskDetailViewController;

@property(nonatomic,retain) UIPopoverController *filterPopoverController;

@property(nonatomic,retain) NSMutableArray *searchArray;


@property (nonatomic,assign) UISearchBar *searchBar;
@property (nonatomic,retain) UIButton *filterButton;
@property (nonatomic,retain) NSPredicate *filterPredicte;
@property (nonatomic,retain) NSPredicate *searchPredicte;

-(NSMutableArray *)currentArray;

-(void)selectTaskFromTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath animated:(BOOL)animated;

@end

@implementation TasksListViewController
@synthesize taskDetailViewController=_taskDetailViewController;
@synthesize tasks;
@synthesize searchArray;
@synthesize tableView=_tableView;
@synthesize parentController;
@synthesize filterPopoverController;
@synthesize searchBar;
@synthesize filterButton;
@synthesize filterPredicte;
@synthesize searchPredicte;

-(void)dealloc
{
    parentController = nil;
    [tasks release];
    [searchArray release];
    [_tableView release];
    [filterPopoverController release];
    [_taskDetailViewController release];
    [filterButton release];
    searchBar = nil;
    
    [filterPredicte release];
    [searchPredicte release];
    
    [super dealloc];
}

- (id)init
{
    //self = [super initWithStyle:UITableViewStyleGrouped];
    self = [super init];

    
    if (self)
    {
        // Custom initialization
               
                
        searchArray=[[NSMutableArray alloc] init];

    }
    return self;
}


-(id)initWithTasks:(NSMutableArray *)taskArr
{
    if(self=[self init])
    {
        self.tasks=taskArr? taskArr : [NSMutableArray array];
    }
    
    return self;
}

-(void)sortTasksByDueDate
{
    if(![self.tasks count]) return;
    
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"dueDate" ascending:NO]; //YES
    
     // NSSortDescriptor *sortDescriptor2=[[NSSortDescriptor alloc]initWithKey:@"isCompleted" ascending:YES];
    
    NSArray *descriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
     [self.tasks sortUsingDescriptors:descriptors];
    
     [sortDescriptor release];
    //[sortDescriptor2 release];
}

-(void)setTasks:(NSMutableArray *)_tasks
{
    if(tasks !=_tasks)
    {
        [tasks release];
        tasks=[_tasks retain];
        [self sortTasksByDueDate];
        [self segmentedTabDidSelectForIndex:_isArchievedTasks?1:0];
        //[self.tableView reloadData];
    }
}

-(void)selectTask:(Task *)aTask animated:(BOOL)animated
{
    if(!aTask) return;
    
    [self.parentController selectTabAtIndex:[aTask.isCompleted boolValue]?1:0];
    
    NSMutableArray *arr = [self currentArray];
    NSInteger index = [arr indexOfObject:aTask];
    if(index!=NSNotFound)
    {
        NSIndexPath *indexPath=[NSIndexPath indexPathForRow:index inSection:0];
       // [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
        [self selectTaskFromTableView:self.tableView indexPath:indexPath animated:animated];
       
        [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    
    }
}

-(void)searchWithSearchKey:(NSString *)searchKey
{
    
}

-(void)disableSearch
{
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.clipsToBounds = YES;
    
    self.view.backgroundColor=[UIColor whiteColor];
    [self setupTableHeaderView];
    
    CGFloat locY = 55.0; //128.0
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, locY, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-locY) style:UITableViewStyleGrouped];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight |  UIViewAutoresizingFlexibleBottomMargin;
    _tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.backgroundColor   =   [UIColor clearColor];
   // _tableView.separatorColor = [UIColor lightGrayColor];
    _tableView.rowHeight=60;
    _tableView.bounces=NO;
    [self.view addSubview:_tableView];
    
   if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
   {
       UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_tableView.bounds), 10.0)];
       _headerView.backgroundColor = [UIColor clearColor];
       _tableView.tableHeaderView = _headerView;
       [_headerView release];
       
       _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
       
   }
    
     //TO ENABLE SWIPE TO DELETE
     //_tableView.allowsSelectionDuringEditing =YES;
   
    
    //self.tableView.rowHeight = 60.0;
    //self.tableView.bounces = NO;
     
    
    UIView *bgView=[[UIView alloc]init];
    bgView.backgroundColor=[UIColor clearColor];
    //bgView.layer.borderWidth=1;
    //bgView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.tableView.backgroundView=bgView;
    [bgView release];
    
    
    //=======================================
    
   // [self segmentedTabDidSelectForIndex:_isArchievedTasks?1:0];
    
    //=======================================
}


-(void)setupTableHeaderView
{
    //[self setupTableHeaderViewWithImage:@"tasks_heading.png"];
    
    UIImage *createTaskImage = [UIImage imageNamed:@"create_new_task.png"];
    createTaskButton = [UIButton buttonWithType:UIButtonTypeCustom];
    createTaskButton.frame = CGRectMake((CGRectGetWidth(self.view.bounds)-330.0)/2.0, 10.0, createTaskImage.size.width, createTaskImage.size.height);
    [createTaskButton setBackgroundImage:createTaskImage forState:UIControlStateNormal];
    [createTaskButton setBackgroundImage:[UIImage imageNamed:@"create_new_task_selected.png"] forState:UIControlStateHighlighted];
    createTaskButton.autoresizingMask  = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
    [self.view addSubview:createTaskButton];
    [createTaskButton addTarget:self action:@selector(createNewTaskAction:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)createNewTaskAction:(UIButton *)sender
{
    NSIndexPath *selectedIndexPath = self.tableView.indexPathForSelectedRow;
    if(selectedIndexPath)
    {
      [self.tableView deselectRowAtIndexPath:selectedIndexPath animated:YES];
    }
    
    if(self.parallelStackViewController.isDetailControllerVisible)
    {
     UINavigationController *_oldNavController = (UINavigationController *) self.parallelStackViewController.detailController;
     UIViewController *oldDetailControler = [_oldNavController viewControllers] [0];
     BOOL _isNewTaskType  = [oldDetailControler isKindOfClass:[CreateTaskViewController class]];
    
     if(_isNewTaskType) return;
    }
    
    CreateTaskViewController *taskViewController=[[CreateTaskViewController alloc] initWithTask:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:taskViewController];
    [self.parallelStackViewController showDetailController:navController animated:YES];
    navController.navigationBarHidden=YES;
    navController.view.layer.borderColor = [UIColor clearColor].CGColor;

    [taskViewController addSaveTaskCallback:^(Task *_task) 
     {
         [self.parentController.customer addTasksObject:_task];
         
         NSError *error=nil;
         [[SSCoreDataManager sharedManager] save:error];
         //if(!error)[self newTaskDidAdd:_task];
         [self.tableView reloadData];
        
          [self.parallelStackViewController hideDetailController:taskViewController animated:YES];
     }];
    
    
    [taskViewController addCancelTaskCallback:^{
        
        [self.parallelStackViewController hideDetailController:taskViewController animated:YES];
    }];
    
    
    /*
    [taskViewController addStarredTaskCallback:^(BOOL isStarred) 
    {
        TaskTableViewCell *cell =(TaskTableViewCell *) [self.tableView cellForRowAtIndexPath:selectedIndexPath];
        cell.starView.checked = isStarred;
    }];
    */
    
    [navController release];
    [taskViewController release];
}


-(void)setupTableHeaderViewWithImage:(NSString *)imageName
{
    UIImageView *headerView=[self pageHeaderViewForImageName:imageName];
    // self.tableView.tableHeaderView=headerView;
    [self.view addSubview:headerView];

}

-(void)deleteTask:(Task *)aTask
{
    if([self.tasks containsObject:aTask])
        [self.tasks removeObject:aTask];
    
    [self segmentedTabDidSelectForIndex:_isArchievedTasks?1:0];
}

-(void)addNewTask:(Task *)aTask
{
    [self.tasks addObject:aTask];
    [self sortTasksByDueDate];
    
    [self segmentedTabDidSelectForIndex:_isArchievedTasks?1:0];
    
    //[self.tableView reloadData];
}
-(void)reload
{
    [self segmentedTabDidSelectForIndex:_isArchievedTasks?1:0];
}

-(void)filterAction:(UIButton *)sender
{
    
    self.filterButton = sender;
    
    //sender.selected = YES;
    //[self.parallelStackViewController hideDetailController:_taskDetailViewController];
    FilterTaskViewController *filterTaskVc=[[FilterTaskViewController alloc] initWithFilterType:_selectedFilterType];
    filterTaskVc.delegate = self;
    //filterTaskVc.title = @"VIEW TASKS";
    [filterTaskVc addTitle:@"VIEW TASKS" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:filterTaskVc];
    
    UIPopoverController *popover=[[UIPopoverController alloc] initWithContentViewController:navController];
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
  
    popover.popoverBackgroundViewClass = [MBPopoverBackgroundView class];
}

    self.filterPopoverController = popover;
    popover.popoverContentSize = CGSizeMake(320.0,220.0);
    
    /*
    [filterTaskVc addFilterTaskCallback:^(FilterType filterType)
     {
         
         _selectedFilterType = filterType;
         [popover dismissPopoverAnimated:YES];
         [self filterTabDidSelectForIndex:filterType];
         
         sender.selected = (filterType!=FilterTypeAll);
         
     }];
    
    [filterTaskVc addCancelFilterCallback:^{
        
        
    }];
   */
    
    [popover presentPopoverFromRect:sender.frame inView:[sender superview] permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    
        
    [navController release];
    [popover release];
    [filterTaskVc release];
}

#pragma mark FilterTaskDelegate
-(void)filterTaskViewController:(FilterTaskViewController *)filterTaskVC didFilterForType:(FilterType)filterType
{
    _selectedFilterType = filterType;
    [self.filterPopoverController dismissPopoverAnimated:YES];
    [self filterTabDidSelectForIndex:filterType];
    
    self.filterButton.selected = (filterType!=FilterTypeAll);
    
    [self.parallelStackViewController hideDetailController:_taskDetailViewController];
}
#pragma mark -

-(void)filterTabDidSelectForIndex:(int)index
{
    switch (index) 
    {
        case 0://SHOW ALL
        {
            self.filterPredicte = nil;
            
            
            // [self.tableView reloadData];
            //[self segmentedTabDidSelectForIndex:_isArchievedTasks?1:0];
        }
        break;
            
        case 1: //STARRED
        {
           
            
            NSPredicate *starredPredicate=[NSPredicate predicateWithFormat:@"self.isStarred.intValue=1"];
            
            self.filterPredicte = starredPredicate;
            
            /*
           // NSArray *filterArr = [self.tasks filteredArrayUsingPredicate:starredPredicate];
              NSArray *filterArr = [self.searchArray filteredArrayUsingPredicate:starredPredicate];
            [searchArray setArray:filterArr];
            [self.tableView reloadData];
          */
        }
        break;
            
        case 2: //DUE DATE
        {
            
            
            NSPredicate *dueDatePredicate=[NSPredicate predicateWithFormat:@"self.dueDate>=%@",[NSDate date]];
           // NSArray *filterArr = [self.tasks filteredArrayUsingPredicate:dueDatePredicate];
            
            self.filterPredicte = dueDatePredicate;
            
            /*
             NSArray *filterArr = [self.searchArray filteredArrayUsingPredicate:dueDatePredicate];
            [searchArray setArray:filterArr];
            [self.tableView reloadData];
             */
        }
        break;
            
        case 3: //COMPLETED TASK
        {
            
            
            NSPredicate *completedPredicate=[NSPredicate predicateWithFormat:@"self.isCompleted=%@",[NSNumber numberWithBool:YES]];
            //NSArray *filterArr = [self.tasks filteredArrayUsingPredicate:completedPredicate];
            
            self.filterPredicte = completedPredicate;
            
            /*
            NSArray *filterArr = [self.searchArray filteredArrayUsingPredicate:completedPredicate];
            [searchArray setArray:filterArr];
            [self.tableView reloadData];
             */
        }
            break;
            
        default:;
            break;
    }

    [self segmentedTabDidSelectForIndex:_isArchievedTasks?1:0];
}

-(void)didSelectSegmentAtIndex:(int)index
{
    [self segmentedTabDidSelectForIndex:index];
    
    [self.parallelStackViewController hideDetailController:_taskDetailViewController];
}

-(void)segmentedTabDidSelectForIndex:(int)index
{
    
    NSMutableArray *_arr = [NSMutableArray arrayWithArray:self.tasks];
    
    NSPredicate *predicate =nil;
    
    if(self.filterPredicte && self.searchPredicte)
    {
        predicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:self.filterPredicte,self.searchPredicte, nil]];
        
       // [_arr filterUsingPredicate:predicate];
    }
    else if(self.filterPredicte)
    {
        predicate = self.filterPredicte;
       // [_arr filterUsingPredicate:self.filterPredicte];
    }
    else if(self.searchPredicte)
    {
        predicate = self.searchPredicte;
        //[_arr filterUsingPredicate:self.searchPredicte];
    }

    if(!searchArray)
    {
        searchArray = [[NSMutableArray alloc] init];
    }
    
    switch (index)
    {
        case 0://CURRENT
        {
            _isArchievedTasks = NO;
                       
            NSPredicate *completedPredicate =  [NSPredicate predicateWithFormat:@"self.isCompleted=%@",[NSNumber numberWithBool:NO]];
            
            if(predicate)
            {
                predicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate,completedPredicate, nil]];
            }
            else
            {
                predicate = completedPredicate;
            }
            
            NSArray *filterArr = [_arr filteredArrayUsingPredicate:predicate];
            [searchArray setArray:filterArr];
            [self.tableView reloadData];
        }
        break;
            
        case 1: //COMPLETED TASK
        {
            _isArchievedTasks = YES;
            
            
              NSPredicate *completedPredicate=[NSPredicate predicateWithFormat:@"self.isCompleted=%@",[NSNumber numberWithBool:YES]];
            
            if(predicate)
            {
                predicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate,completedPredicate, nil]];
            }
            else
            {
                predicate = completedPredicate;
            }

            
            NSArray *filterArr = [_arr filteredArrayUsingPredicate:predicate];
            [searchArray setArray:filterArr];
            [self.tableView reloadData];
        }
            break;
            
        default:
        {
            _isArchievedTasks=NO;
            
        }
            break;
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.parallelStackViewController hideDetailController:_taskDetailViewController];
    
    [_tableView reloadData];
    
    [super viewWillDisappear:animated];
     //app.fromCustomerTask=2;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSMutableArray *)currentArray
{
    return self.searchArray;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int taskCount=[[self currentArray] count];
    //taskCount=MAX(1, taskCount);
    
    return taskCount;
}

-(BOOL)showCustomer
{
    return YES;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier  = @"Cell";
   // static NSString *CellIdentifier2 = @"Cell_notask";
    TaskTableViewCell *cell=nil;
    
    NSMutableArray *arr=[self currentArray];
    
    int count=[arr count];
    
    /*
    if(!count)
    {
        cell =(TaskTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil) 
        {
            cell = [[[TaskTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            cell.textLabel.font=[UIFont systemFontOfSize:15];
        }
        
      
        cell.textLabel.text=@"No tasks";
    }
   else
     */
   {
    
     Task *_task=(indexPath.row < count) ?  [arr objectAtIndex:indexPath.row] :nil;
     cell =(TaskTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[TaskTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    
        //[cell.completedCheckBox addTarget:self action:@selector(completeTaskAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    if(_task)
    {
        cell.taskInfo=_task;
    }
       cell.delegate = self;
       
       cell.completedCheckBox.userInteractionEnabled = !_task.isCompleted.boolValue;
       
     //cell.hideCustomer =  _task.parentCustomer==nil;
       cell.hideCustomer = !([self.parentController shouldShowCustomerProfile] && _task.parentCustomer!=nil);
       
       NSLog(@" in cell %d",cell.hideCustomer);
       
    /*
    [cell addTaskCompleteCallback:^(Task *task)
     {
         if(task)
         {
             NSMutableArray *_tasks = [self currentArray];
            [_tasks removeObject:task];
            [SSCoreDataManager deleteObject:task];
            [self.tableView reloadData];
         }
    }];
    */
       
       [cell addTaskStarredCallback:^(BOOL isStarred) 
       {
           if(_taskDetailViewController && [_taskDetailViewController.task isEqual:cell.taskInfo])
           [_taskDetailViewController setStar:isStarred];
       }];
    
     cell.selectionStyle=UITableViewCellSelectionStyleBlue;
       
     //  cell.taskDateLabel.backgroundColor = [UIColor yellowColor];
   }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = CELL_BACKGROUND_COLOR;
    
    
    CellBackgroundView *_selectedBgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _selectedBgView.backgroundColor = [UIColor clearColor];
    _selectedBgView.contentInset = CGPointMake(10.0, 0.0);
    _selectedBgView.cornerRadius = 8.0;
    _selectedBgView.borderColor = self.tableView.separatorColor;
    _selectedBgView.fillColor = [UIColor cellSelectedColorBlue];
    
    
    
    int numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    _selectedBgView.position = _bgView.position;
    
    cell.backgroundView = _bgView;
    cell.selectedBackgroundView = _selectedBgView;
    
    [_bgView release];
    [_selectedBgView release];
}


#pragma mark - Table view delegate

-(void)selectTaskFromTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath animated:(BOOL)animated
{
    
    NSMutableArray *arr = [self currentArray];
    
    Task *_task = (indexPath.row < [arr count]) ?  [arr objectAtIndex:indexPath.row] :nil;
    
    if(!_task) return;
    
    UINavigationController *_oldNavController = (UINavigationController *) self.parallelStackViewController.detailController;
    UIViewController *oldDetailControler = [_oldNavController viewControllers] [0];
    
    BOOL _isDetailType  = [oldDetailControler isKindOfClass:[TaskDetailViewController class]];
    
    BOOL _isSameTask = (self.parallelStackViewController.isDetailControllerVisible && (_isDetailType && [_taskDetailViewController.task isEqual:_task]));
    
    if(_isSameTask)
    {
        //[tableView deselectRowAtIndexPath:indexPath animated:YES];
        //[self.parallelStackViewController hideDetailController:_taskDetailViewController animated:YES];
        
        return;
    }
    
    _taskDetailViewController = [[TaskDetailViewController alloc] initWithTask:_task];
    _taskDetailViewController.isEditMode = YES;
    _taskDetailViewController.shouldShowCustomerProfile = [self.parentController shouldShowCustomerProfile] ;
    _taskDetailViewController.allowCustomerListing = [self.parentController allowCustomerList];
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:_taskDetailViewController];
    navController.navigationBarHidden=YES;
    [self.parallelStackViewController showDetailController:navController animated:animated];
    navController.view.layer.borderColor = [UIColor clearColor].CGColor;
    _taskDetailViewController.tableView.backgroundView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:1.0];
    
    //========================= ADDING BORDER TO LEFT =================
    CALayer *leftBorder = [CALayer layer];
    leftBorder.frame = CGRectMake(0.0f, 0.0, 1.0, CGRectGetHeight(navController.view.frame));
    leftBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    [navController.view.layer addSublayer:leftBorder];
    
    
    [_taskDetailViewController addDeleteTaskCallback:^(Task *_task)
     {
          NSMutableArray *arr=[self currentArray];
         if([arr containsObject:_task])
         {
             [arr removeObject:_task];
         }
         
         
         if(![arr isEqualToArray:self.tasks] && [self.tasks containsObject:_task])
         {
             [self.tasks removeObject:_task];
         }

         [SSCoreDataManager deleteObject:_task];
         
         NSError *error=nil;
         [[SSCoreDataManager sharedManager] save:error];
         if(!error)
         {
             [self.tableView reloadData];
             [self.parallelStackViewController hideDetailController:_taskDetailViewController];
         }
     }];
    
    [_taskDetailViewController addTaskCancelCallback:^{
        
        [self.parallelStackViewController hideDetailController:_taskDetailViewController animated:YES];   
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        //===== forcefully added for task star save =====//
        [[SSCoreDataManager sharedManager] save:nil];
        
    }];
    
    [_taskDetailViewController addEditTaskCallback:^(Task *task) 
     {
          [[SSCoreDataManager sharedManager] save:nil];
         [self.parallelStackViewController hideDetailController:_taskDetailViewController animated:YES];
         [tableView deselectRowAtIndexPath:indexPath animated:NO];
         
     }];
    
    [_taskDetailViewController addTaskStarredCallback:^(BOOL isStarred)
     {
        
        TaskTableViewCell *cell =(TaskTableViewCell *) [tableView cellForRowAtIndexPath:indexPath];
        cell.starView.checked = isStarred;
    }];
    
    
    self.taskDetailViewController=_taskDetailViewController;
    
    [navController release];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    /*
    //NSIndexPath *lastSelectedIndexPath = tableView.indexPathForSelectedRow;
    //if([lastSelectedIndexPath isEqual:indexPath]) return;
    
    NSMutableArray *arr = [self currentArray];
    
    Task *_task = (indexPath.row < [arr count]) ?  [arr objectAtIndex:indexPath.row] :nil;
    
    
    if(!_task) return;
    
    _taskDetailViewController = [[TaskDetailViewController alloc] initWithTask:_task];
    _taskDetailViewController.isEditMode = YES;
    _taskDetailViewController.shouldShowCustomerProfile = [self.parentController shouldShowCustomerProfile] ;
    _taskDetailViewController.allowCustomerListing = [self.parentController allowCustomerList];
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:_taskDetailViewController];
    navController.navigationBarHidden=YES;
    [self.parallelStackViewController showDetailController:navController animated:YES];
    navController.view.layer.borderColor = [UIColor clearColor].CGColor;
    _taskDetailViewController.tableView.backgroundView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:240.0/255.0 blue:241.0/255.0 alpha:1.0];
    
    //========================= ADDING BORDER TO LEFT =================
    CALayer *leftBorder = [CALayer layer];
    leftBorder.frame = CGRectMake(0.0f, 0.0, 1.0, CGRectGetHeight(navController.view.frame));
    leftBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    [navController.view.layer addSublayer:leftBorder];

    
    [_taskDetailViewController addDeleteTaskCallback:^(Task *_task)
    {
        [SSCoreDataManager deleteObject:_task];
        
        NSError *error=nil;
        [[SSCoreDataManager sharedManager] save:error];
        if(!error)
        {
          if([self.tasks containsObject:_task])
          [self.tasks removeObject:_task];
            
          [self.tableView reloadData];
          [self.parallelStackViewController hideDetailController:_taskDetailViewController];
        }
    }];
    
    [_taskDetailViewController addTaskCancelCallback:^{
        
      [self.parallelStackViewController hideDetailController:_taskDetailViewController animated:YES];   
       
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        //===== forcefully added for task star save =====//
        [[SSCoreDataManager sharedManager] save:nil];
        
    }];
    
    [_taskDetailViewController addEditTaskCallback:^(Task *task) 
    {
        
        [self.parallelStackViewController hideDetailController:_taskDetailViewController animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }];
    
    [_taskDetailViewController addTaskStarredCallback:^(BOOL isStarred) {
        
        TaskTableViewCell *cell =(TaskTableViewCell *) [tableView cellForRowAtIndexPath:indexPath];
        cell.starView.checked = isStarred;
    }];
    
    
    self.taskDetailViewController=_taskDetailViewController;
    
    [navController release];
    */
    [self selectTaskFromTableView:tableView indexPath:indexPath animated:YES];
}

/*
- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    CellBackgroundView *_bgView = (CellBackgroundView *) cell.backgroundView;
    _bgView.fillColor = LOGIN_BG_COLOR;
    [_bgView setNeedsDisplay];
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    CellBackgroundView *_bgView = (CellBackgroundView *) cell.backgroundView;
    _bgView.fillColor = CELL_BACKGROUND_COLOR;
    [_bgView setNeedsDisplay];
}
 */

/*
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle==UITableViewCellEditingStyleDelete)
    {
        
    }
}
*/
-(void)completeTaskAction:(SSCheckBox *)checkBox
{
    UIAlertView *alert = [UIAlertView alertViewWithTitle:@"" message:TASK_COMPLETE_MESSAGE cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObjects:@"Yes",nil] onDismiss:^(int buttonIndex)
     {
         checkBox.checked = YES;
         checkBox.userInteractionEnabled = NO;
         
         BOOL _isCompleted = checkBox.checked;
         
         TaskTableViewCell *taskCell =(TaskTableViewCell *)[[checkBox superview] superview];
         Task *_task = taskCell.taskInfo;
         
         if(!_task) return;
         _task.completedDate = [NSDate date];
         _task.isCompleted   = [NSNumber numberWithBool:_isCompleted];
         
         taskCell.starView.userInteractionEnabled = NO;
         
         if(_taskDetailViewController)
             [self.parallelStackViewController hideDetailController:_taskDetailViewController animated:YES];
         
         //if(self.taskCompleteCallback) self.taskCompleteCallback(_task);
         
         [[OfflineManager sharedManager] addTask:_task willDelete:NO isEditMode:YES];
     }
    onCancel:^{
        
        // TaskTableViewCell *taskCell =(TaskTableViewCell *)[[checkBox superview] superview];
       // taskCell.completedCheckBox.checked = NO;
        ///taskCell.completedCheckBox.userInteractionEnabled = YES;
        checkBox.checked = NO;
        checkBox.userInteractionEnabled = YES;
    }];
    
    [alert release];

   
}

#pragma mark UISearchBarDelegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar
{
    
}

-(void)searchBar:(UISearchBar *)_searchBar textDidChange:(NSString *)searchText
{
   // NSPredicate *searchPredicate=[NSPredicate predicateWithFormat:@"SELF.TASK_TITLE beginswith[c] %@",searchText];
  
    
    if(searchText.length>0)
    {
       // NSPredicate *_searchPredicate=[NSPredicate predicateWithFormat:@"self.subject contains[c] %@",searchText];
        
         NSPredicate *_searchPredicate=[NSPredicate predicateWithFormat:@"self.subject beginswith[c] %@",searchText];

        /*
        if(self.filterPredicte)
        {
            NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:self.filterPredicte,searchPredicate, nil]];
            self.filterPredicte = predicate;
        }
       */
        
          self.searchPredicte = _searchPredicate;
        
    }
    else
    {
        self.searchPredicte = nil;
    }
    
    [self segmentedTabDidSelectForIndex:_isArchievedTasks?1:0];
    
    if([self.parallelStackViewController isDetailControllerVisible])
    [self.parallelStackViewController hideDetailController:_taskDetailViewController];
    
    /*
    NSArray *arr = [self.tasks filteredArrayUsingPredicate:searchPredicate];
    [self.searchArray setArray:arr];
    [self.tableView reloadData];
     */
    
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)_searchBar
{
    self.searchBar = nil;
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)_searchBar
{
    self.searchBar = _searchBar;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)_searchBar
{
    self.searchBar =nil;

    self.searchPredicte = nil;
    [self segmentedTabDidSelectForIndex:_isArchievedTasks?1:0];
    //[self.tableView reloadData];
}

-(void)refresh
{
    self.searchBar.text = nil;
    self.filterPopoverController=nil;
    self.filterButton.selected = NO;
    
    _selectedFilterType = FilterTypeAll;
    
    self.searchPredicte = nil;
    self.filterPredicte = nil;
    [self searchBar:nil textDidChange:nil];
    [self.searchBar resignFirstResponder];
    

   // [self.tableView reloadData];
}

-(void)taskDidComplete:(Task *)task
{
    if(_taskDetailViewController)
    {
        [self.parallelStackViewController hideDetailController:_taskDetailViewController animated:YES];
    }
    
    if(_isArchievedTasks)
    {
        [self.searchArray addObject:task];
    }
    else
    {
        [self.searchArray removeObject:task];
    }
    
   // [self segmentedTabDidSelectForIndex:_isArchievedTasks?1:0];
    
    [self.tableView reloadData];
}

@end
