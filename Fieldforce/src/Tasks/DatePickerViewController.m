//
//  PickerViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 09/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "DatePickerViewController.h"


@interface DatePickerViewController ()

@property(nonatomic,copy)DoneActionCallback doneActionCallback;
@property (nonatomic,retain) NSDate *prevDate;
@end

@implementation DatePickerViewController
@synthesize selectedDate;
@synthesize doneActionCallback;
@synthesize isInPopover;
@synthesize _isFromCalendarTaskDatePicker;
@synthesize prevDate;
/*
- (id)init
{
    self = [super init];
    if (self) 
    {
       // self.selectedDate = [[NSDate date] dateByAddingTimeInterval:60.0];
        self.selectedDate= [[[NSDate date] dateByAddingTimeInterval:60.0] currentDateHourMinute];
        NSLog(@"Selected date=%@",self.selectedDate);
    }
    
    return self;
}
*/
- (id)initWithDate:(NSDate *)date
{
    self = [super init];
    if (self) 
    {
        self.selectedDate = self.prevDate = (date) ? date : [[[NSDate date] dateByAddingTimeInterval:60.0] currentDateHourMinute];
    }
    
    return self;
}


-(void)dealloc
{
    [dateLabel release];
    [selectedDate release];
    [datePicker release];
    [prevDate release];
    
    if(doneActionCallback) [doneActionCallback release];
    
    [super dealloc];
}

-(void)addDoneCallback:(DoneActionCallback)callback
{
    if(callback)
        self.doneActionCallback=callback;
    
}

-(void)setupDatePicker
{
    
    if(!datePicker)
    {
        CGRect datePickerFrame;
        
        datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
        [datePicker sizeThatFits:CGSizeMake(CGRectGetWidth(self.view.bounds), 300.0)];
        datePickerFrame = datePicker.frame;
        datePickerFrame.origin.x = (CGRectGetWidth(self.view.bounds) - CGRectGetWidth(datePickerFrame))/2.0 ;
        datePickerFrame.origin.y = CGRectGetMaxY(dateLabel.frame)+20.0;
        datePicker.frame = datePickerFrame;
        datePicker.datePickerMode = UIDatePickerModeDateAndTime;
        datePicker.backgroundColor=[UIColor clearColor];
        datePicker.autoresizingMask=  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;
        datePicker.hidden = NO;
        datePicker.minimumDate  = [[NSDate date] dateByAddingTimeInterval:60.0];
        datePicker.date = (self.prevDate) ? self.prevDate : self.selectedDate;//self.selectedDate;
        [datePicker addTarget:self action:@selector(dateDidChange:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:datePicker];
    }
}

-(void)addDoneButton
{
    if(self.isInPopover)
    {
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
        UIButton *btnDone= [UIButton repToolButtonWithImageName:NAV_DONE_BUTTON text:@"Done"];
        btnDone.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
        [btnDone addTarget:self
                    action:@selector(doneAction:)
          forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithCustomView:btnDone];
        self.navigationItem.rightBarButtonItem = barButtonDone;
        [barButtonDone release];
        
        [self addBackButtonWithImage:[UIImage imageNamed:NAV_BACK_BUTTON] title:@"Back" selector:nil];
}
else
{
    
        UIBarButtonItem *retakeBarButton=[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneAction:)];
        self.navigationItem.rightBarButtonItem=retakeBarButton;
        [retakeBarButton release];
        
        //self.view.backgroundColor = CELL_BACKGROUND_COLOR;
        
}

    }
    else
    {
        if(_isFromCalendarTaskDatePicker)
        {
           
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
            UIButton *btnDone= [UIButton repToolButtonWithImageName:NAV_DONE_BUTTON text:@"Done"];
            btnDone.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
            [btnDone addTarget:self
                        action:@selector(doneAction:)
              forControlEvents:UIControlEventTouchUpInside];
            
            UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc]initWithCustomView:btnDone];
            self.navigationItem.rightBarButtonItem = barButtonDone;
            [barButtonDone release];
            
            [self addBackButtonWithImage:[UIImage imageNamed:NAV_BACK_BUTTON] title:@"Back" selector:nil];
}
else
{
    
            UIBarButtonItem *retakeBarButton=[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneAction:)];
            self.navigationItem.rightBarButtonItem=retakeBarButton;
            [retakeBarButton release];
            
            //self.view.backgroundColor = CELL_BACKGROUND_COLOR;
            
}

        }
        else
        {
            UIImage *doneButImage = [UIImage imageNamed:@"edit_blank.png"];
            UIButton *doneButton  = [UIButton buttonWithType:UIButtonTypeCustom];
            doneButton.frame = CGRectMake(0,0,doneButImage.size.width,doneButImage.size.height);
            [doneButton setBackgroundImage:doneButImage forState:UIControlStateNormal];
            doneButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
            [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [doneButton setTitle:@"Done" forState:UIControlStateNormal];
            [doneButton addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
            
            UIBarButtonItem *retakeBarButton=[[UIBarButtonItem alloc] initWithCustomView:doneButton];
            self.navigationItem.rightBarButtonItem=retakeBarButton;
            [retakeBarButton release];
        
       }
    }
}

-(void)addCancelButton
{
    
    if(self.isInPopover || _isFromCalendarTaskDatePicker)
    {
        /*
        UIBarButtonItem *cancelButton=[[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelAction:)];
        self.navigationItem.leftBarButtonItem=cancelButton;
        [cancelButton release];
         */
         
    }
    else
    {
       [self addBackButtonWithSelector:@selector(cancelAction:)];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif
    
   if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
  
    
    if(!_isFromCalendarTaskDatePicker)
    {
        if(!self.isInPopover)
            [self.navigationController.navigationBar addBackGroundImage:@"right_sub_banner.png"];
        self.navigationController.navigationBarHidden=NO;
    }
    [self addCancelButton];
       
     [self addDoneButton];
    self.view.backgroundColor=self.isInPopover ?[UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0] : [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
	//self.contentSizeForViewInPopover=CGSizeMake(320, 320);
    
    
    
    dateLabel = [[UILabel alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.view.bounds)-320.0f)/2.0,10.0,320.0,50.0)];
    dateLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    dateLabel.backgroundColor = CELL_BACKGROUND_COLOR;
    dateLabel.textAlignment   = NSTextAlignmentCenter;
    dateLabel.layer.cornerRadius = 8.0;
    dateLabel.layer.masksToBounds = YES;
    dateLabel.layer.borderWidth  = 0.5;
    dateLabel.userInteractionEnabled = YES;
    dateLabel.layer.borderColor  = [UIColor colorWithWhite:0.2 alpha:0.35].CGColor;
    
    dateLabel.textColor = [UIColor blackColor];
    dateLabel.font = [UIFont grotesqueBoldFontOfSize:15.0];
    [dateLabel addInnerShadowWithColor:[UIColor colorWithWhite:0.2 alpha:0.3]];
    [self.view addSubview:dateLabel];
    
    
    UIButton *crossButton = [UIButton buttonWithType:UIButtonTypeCustom];
    crossButton.frame = CGRectMake(CGRectGetWidth(dateLabel.bounds)-31.0, 10.0, 29.0, 30.0);
    [crossButton setImage:[UIImage imageNamed:@"close-button.png"] forState:UIControlStateNormal];
    [crossButton addTarget:self action:@selector(removeDate:) forControlEvents:UIControlEventTouchUpInside];
    
    [dateLabel addSubview:crossButton];
    
    //dateLabel.text = [self.selectedDate stringFromDateWithFormat:DATE_PICKER_FORMAT];; 
    dateLabel.text = [self.prevDate?self.prevDate:self.selectedDate stringFromDateWithFormat:DATE_PICKER_FORMAT];; 
    
    [self setupDatePicker];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   [self.navigationController setNavigationBarHidden:NO animated:YES];
    
   #ifndef __IPHONE_7_0
    self.contentSizeForViewInPopover = CGSizeMake(350.0,MAX(270.0, CGRectGetMaxY(datePicker.frame)));
   #endif
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
   if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    self.preferredContentSize = CGSizeMake(350.0,MAX(300.0, CGRectGetMaxY(datePicker.frame)));
    else
    self.contentSizeForViewInPopover = CGSizeMake(350.0,MAX(300.0, CGRectGetMaxY(datePicker.frame)));
    
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    //[super viewWillDisappear:animated];
    if(!_isFromCalendarTaskDatePicker)
    {
       if(!self.isInPopover)
       [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
  
}
-(void)dateDidChange:(UIDatePicker *)picker
{
    self.prevDate = self.selectedDate = [picker.date currentDateHourMinute];
    
    // self.selectedDate=datePicker.date;
    
    NSLog(@"selected Date=%@",self.selectedDate);
    
    dateLabel.text=[picker.date stringFromDateWithFormat:DATE_PICKER_FORMAT];;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)doneAction:(id)sender
{
    NSLog(@"selected Date=%@",self.selectedDate);
    NSLog(@"self.prevDate Date=%@",self.prevDate);
    if(self.doneActionCallback)
        self.doneActionCallback(self.selectedDate);
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)cancelAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)removeDate:(UIButton *)btn
{
    dateLabel.text = nil;
    self.selectedDate=nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
