//
//  TaskTableViewCell.m
//  TestIndexedTableView
//
//  Created by RANDEM MAC on 29/05/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import "TaskTableViewCell.h"
#import "Task.h"
#import "Customer.h"
#import "OfflineManager.h"



#define TASK_DATE_COLOR [UIColor colorWithRed:237.0/255.0 green:27.0/255.0 blue:47.0/255.0 alpha:1.0]

@interface TaskTableViewCell()<UIAlertViewDelegate>
{
    BOOL _hideCustomer;
}
@property(nonatomic,copy) TaskCompleteCallback taskCompleteCallback;
@property(nonatomic,copy) TaskStarredCallback taskStarredCallback;
@end


@implementation TaskTableViewCell
@synthesize completedCheckBox=_completedCheckBox;
@synthesize starView=_starView;
@synthesize nameLabel=_nameLabel;
@synthesize customerNameLabel=_customerNameLabel;
@synthesize taskDateLabel=_taskDateLabel;

@synthesize isStarred,isCompleted;
@synthesize taskInfo;
@synthesize taskCompleteCallback;
@synthesize taskStarredCallback;
@synthesize hideCustomer=_hideCustomer;
@synthesize delegate;

-(void)dealloc
{
    delegate = nil;
    
     [taskInfo release];
    
    [_starView release];
    _starView=nil;
    
    [_nameLabel release];
    _nameLabel=nil;
    
    [_customerNameLabel release];
    _customerNameLabel=nil;
    
    [_completedCheckBox release];
    
    [_taskDateLabel release];
    
    
    if(taskCompleteCallback)
    [taskCompleteCallback release];
    
    if(taskStarredCallback)
       [taskStarredCallback release];
    
    [super dealloc];
}

-(void)setHideCustomer:(BOOL)hideCustomer
{
    NSLog(@"%d",_hideCustomer);
    NSLog(@"%d",hideCustomer);
    if(hideCustomer!=_hideCustomer )
    {
      _hideCustomer = hideCustomer;
      _customerNameLabel.hidden = hideCustomer;
      [self setNeedsLayout];
    }
  //  else if
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) 
    {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        UIView *contentView=self.contentView;
        
        _completedCheckBox=[[SSCheckBox alloc] initWithFrame:CGRectZero];
        [_completedCheckBox setBackgroundImageForNormalState:[UIImage imageNamed:@"unchecked.png"] selectedSate:[UIImage imageNamed:@"checked.png"]];
        [contentView addSubview:_completedCheckBox];
        [_completedCheckBox addTarget:self action:@selector(taskCompleteAction:) forControlEvents:UIControlEventTouchUpInside];
        
        _starView=[[SSCheckBox alloc] initWithFrame:CGRectZero];
        [_starView setBackgroundImageForNormalState:[UIImage imageNamed:@"unstarred.png"] selectedSate:[UIImage imageNamed:@"starred.png"]];
        _starView.backgroundColor=[UIColor clearColor];
        [contentView addSubview:_starView];
        
        [_starView addTarget:self action:@selector(starAction:) forControlEvents:UIControlEventTouchUpInside];
        
        _nameLabel=[[UILabel alloc] init];
        //_nameLabel.font=[UIFont systemFontOfSize:14];
        _nameLabel.font=[UIFont grotesqueFontOfSize:14.0];
        _nameLabel.textColor=[UIColor blackColor];
        _nameLabel.backgroundColor=[UIColor clearColor];
        _nameLabel.textAlignment=NSTextAlignmentLeft;
        _nameLabel.numberOfLines = 2;
        _nameLabel.lineBreakMode  = UILineBreakModeCharacterWrap|UILineBreakModeTailTruncation;
        //_nameLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [contentView addSubview:_nameLabel];
        
        _customerNameLabel=[[UILabel alloc] init];
        _customerNameLabel.textColor=[UIColor darkGrayColor];
        //_customerNameLabel.font=[UIFont boldSystemFontOfSize:11];
         _customerNameLabel.font=[UIFont grotesqueFontOfSize:11.0];
        _customerNameLabel.backgroundColor=[UIColor clearColor];
        _customerNameLabel.textAlignment=NSTextAlignmentLeft;
        [contentView addSubview:_customerNameLabel];
        
        _taskDateLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 55.0, 33.0)]; //55.0 30.0
        _taskDateLabel.textColor=TASK_DATE_COLOR;
        _taskDateLabel.numberOfLines = 2;
        //_taskDateLabel.font=[UIFont boldSystemFontOfSize:14];
        _taskDateLabel.font=[UIFont grotesqueFontOfSize:13];
        _taskDateLabel.backgroundColor=[UIColor clearColor];
        _taskDateLabel.textAlignment=NSTextAlignmentRight;
        self.accessoryView = _taskDateLabel;

        
       
//#error  @"ADD Document Image Icon Here"
        
 
        self.backgroundColor=CELL_BACKGROUND_COLOR;
        
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect _bounds=self.contentView.bounds;
    
    CGRect _completedCheckButtonFrame=CGRectMake(10, (CGRectGetHeight(_bounds)-26)/2.0, 25, 26.0);
    
    CGRect _starViewFrame=_completedCheckButtonFrame;
    _starViewFrame.origin.x=CGRectGetMaxX(_starViewFrame)+10;
    _starViewFrame.origin.y=(CGRectGetHeight(_bounds)-27)/2.0;
    _starViewFrame.size=CGSizeMake(28, 27);
    
    NSLog(@"%d",_hideCustomer);
    CGFloat _h = _hideCustomer ? CGRectGetHeight(_bounds)-10.0f : 33.0;
    //NSLog(@"%f",_h);
   // CGFloat _h=46.0;
    CGRect _nameLabelFrame=CGRectMake(CGRectGetMaxX(_starViewFrame)+10, 5, CGRectGetWidth(_bounds)-CGRectGetMaxX(_starViewFrame)-15.0, _h);
    
    CGRect _customerNameLabelFrame=_nameLabelFrame;
    _customerNameLabelFrame.origin.y=CGRectGetHeight(_bounds)-20.0;//18.0
    _customerNameLabelFrame.size.height=16.0;
    
       
    if(!CGRectEqualToRect(_completedCheckButtonFrame, _completedCheckBox.frame))
        _completedCheckBox.frame=_completedCheckButtonFrame;
    
    if(!CGRectEqualToRect(_starViewFrame, _starView.frame))
        _starView.frame=_starViewFrame;
    
    if(!CGRectEqualToRect(_nameLabelFrame, _nameLabel.frame))
        _nameLabel.frame=_nameLabelFrame;
    
    if(!CGRectEqualToRect(_customerNameLabelFrame, _customerNameLabel.frame))
        _customerNameLabel.frame=_customerNameLabelFrame;

}

-(void)setTaskInfo:(Task *)_taskInfo
{
   // if(taskInfo!=_taskInfo)
    {
        [taskInfo release];
        taskInfo=[_taskInfo retain];
        
        _nameLabel.text=taskInfo.subject;
         NSLog(@"%@",taskInfo.subject);
       
        
       // _customerNameLabel.text= taskInfo.parentCustomer?taskInfo.parentCustomer.name :taskInfo.customername;
        //[taskInfo.dueDate stringFromDateWithFormat:TASK_DATE_FORMAT];
        
        if (taskInfo.parentCustomer)
        {
           
            _customerNameLabel.text=taskInfo.parentCustomer.name;
            NSLog(@" fff %@",_customerNameLabel.text);
        }
        /*else
         {
            _customerNameLabel.text=taskInfo.customername;
             NSLog(@" kkk %@",_customerNameLabel.text);
        }*/
        
        _taskDateLabel.text = [taskInfo.dueDate stringFromDateWithFormat:TASK_DATE_FORMAT];
        isStarred=[taskInfo.isStarred boolValue];
        _starView.checked=isStarred;
        
        isCompleted = [taskInfo.isCompleted boolValue];
        _completedCheckBox.checked = isCompleted;
        _completedCheckBox.enabled = !isCompleted;
        
        _starView.userInteractionEnabled = !isCompleted;
        //self.accessoryView = taskInfo.notes.length ? _taskDateLabel : nil;
        self.accessoryView =  _taskDateLabel ;
    }
}

-(void)addTaskCompleteCallback:(TaskCompleteCallback )callback
{
    if(callback)
        self.taskCompleteCallback = callback;
}

-(void)addTaskStarredCallback:(TaskStarredCallback)callback
{
    if(callback)
        self.taskStarredCallback = callback;
    

}


-(void)starAction:(SSCheckBox *)checkBox
{
    checkBox.checked = !checkBox.checked;
    
    BOOL starred=checkBox.isChecked;
    //[self.taskInfo setValue:[NSString stringWithFormat:@"%d",starred] forKey:@"isStarred"];
    
    @try {
        
       self.taskInfo.isStarred=[NSNumber numberWithBool:starred];

    }
    @catch (NSException *exception)
    {
        
    }
    @finally {
        
        if(self.taskStarredCallback) self.taskStarredCallback(starred);
        [[OfflineManager sharedManager] addTask:self.taskInfo willDelete:NO isEditMode:YES];
    }
   

}

-(void)setStarred:(BOOL)starred
{
    _starView.checked = starred;
}

-(void)taskCompleteAction:(SSCheckBox *)completeCheckBox
{
    completeCheckBox.checked = NO;
    completeCheckBox.userInteractionEnabled = YES;
    
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:TASK_COMPLETE_MESSAGE
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"No",@"Yes",nil];
    [alert show];
    [alert release];
     

    
    /*
    [UIAlertView alertViewWithTitle:@"" message:TASK_COMPLETE_MESSAGE cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObjects:@"Yes",nil] onDismiss:^(int buttonIndex) 
        {
             completeCheckBox.checked = YES;
            completeCheckBox.enabled=NO;
            self.taskInfo.completedDate = [NSDate date];
            self.taskInfo.isCompleted = [NSNumber numberWithBool:YES];
            
            _starView.userInteractionEnabled =  !completeCheckBox.checked;
            
            if(self.taskCompleteCallback) self.taskCompleteCallback(self.taskInfo);
            

            [[OfflineManager sharedManager] addTask:self.taskInfo willDelete:NO isEditMode:YES];

        } 
        onCancel:^{
        
            if(completeCheckBox)
            {
            completeCheckBox.checked = NO;
            completeCheckBox.enabled = YES;
            }
        }];
    
   // if(self.taskCompleteCallback) self.taskCompleteCallback(self.taskInfo);
*/
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        _completedCheckBox.checked = YES;
        _completedCheckBox.enabled=NO;
        self.taskInfo.completedDate = [NSDate date];
        self.taskInfo.isCompleted = [NSNumber numberWithBool:YES];
        
        _completedCheckBox.userInteractionEnabled =  NO;
        _starView.userInteractionEnabled = NO;
        if(self.taskCompleteCallback) self.taskCompleteCallback(self.taskInfo);
        

        [[OfflineManager sharedManager] addTask:self.taskInfo willDelete:NO isEditMode:YES];
        
        if([self.delegate respondsToSelector:@selector(taskDidComplete:)])
        {
            [self.delegate taskDidComplete:self.taskInfo];
        }

    }
    else
    {
        if(_completedCheckBox)
        {
            _completedCheckBox.checked = NO;
            _completedCheckBox.enabled = YES;
            _completedCheckBox.userInteractionEnabled=YES;
            _starView.userInteractionEnabled = YES;
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
   // self.backgroundColor= selected ?  CELL_BACKGROUND_COLOR : SELECTED_CELL_BACKGROUND_COLOR;
    
    if(selected)
    {
        //_documentImageView.image=[UIImage imageNamed:@"document_icon_white.png"];
        _nameLabel.textColor         = [UIColor whiteColor];
        _customerNameLabel.textColor = [UIColor whiteColor];
        _taskDateLabel.textColor     = [UIColor whiteColor];
    
    }
    else
    {
        //_documentImageView.image=[UIImage imageNamed:@"document_icon.png"];
        _nameLabel.textColor=self.taskInfo? [UIColor blackColor] : [UIColor lightGrayColor];
        _customerNameLabel.textColor=[UIColor darkGrayColor];
        _taskDateLabel.textColor = TASK_DATE_COLOR;
    }
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
}



- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.taskInfo = nil;
    
}


@end
