//
//  SSCoreDataManager.m
//  CoreDataBooks
//
//  Created by RANDEM MAC on 01/12/11.
//  Copyright (c) 2011 RANDEM IT. All rights reserved.
//

#import "SSCoreDataManager.h"

//static SSCoreDataManager *_coreDataManager=nil;

@implementation SSCoreDataManager
@synthesize defaultDBStorePath,dbStorePath;
@synthesize defaultManagedObjectContext,managedObjectModel,persistentStoreCoordinator;

+(SSCoreDataManager *)sharedManager
{
//    if(_coreDataManager==nil)
//        _coreDataManager=[[SSCoreDataManager alloc] init];
//    
//    return _coreDataManager;

    static SSCoreDataManager *_coreDataManager = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        _coreDataManager = [[SSCoreDataManager alloc] init]; 
    });
    return _coreDataManager;
}

+(NSManagedObjectContext *)defaultContext
{
    return [SSCoreDataManager sharedManager].defaultManagedObjectContext;
}

-(id)init
{
    if(self=[super init])
    {
        
    }
    
    return self;
}
-(void)dealloc
{
    [defaultDBStorePath release],defaultDBStorePath=nil;
    [dbStorePath release],dbStorePath=nil;
    [defaultManagedObjectContext release],defaultManagedObjectContext=nil;
    [managedObjectModel release],managedObjectModel=nil;
    [persistentStoreCoordinator release],persistentStoreCoordinator=nil;
    
#if NS_BLOCKS_AVAILABLE
    if(_willChangeContentBlock) Block_release(_willChangeContentBlock);
    if(_didChangeContentBlock)  Block_release(_didChangeContentBlock);
    if(_didChangeObjectBlock)   Block_release(_didChangeObjectBlock);
    if(_didChangeSectionBlock)  Block_release(_didChangeSectionBlock);
#endif
    
    // [_coreDataManager release],_coreDataManager=nil;
    
    [super dealloc];
}


#pragma mark REGISTER for merging changes to main context
-(void)registerContextDidSaveNotificationOnContext:(NSManagedObjectContext *)context
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mergeChangesToDefaultContext:)
                                                 name:NSManagedObjectContextDidSaveNotification
                                               object:context];
}

#pragma mark DEGISTER for merging changes to main context
-(void)removeContextDidSaveNotificationOnContext:(NSManagedObjectContext *)context
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSManagedObjectContextDidSaveNotification object:context];
}
#pragma mrk -


- (NSString *)applicationDocumentsDirectory 
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

#pragma mark GET the managedObjectModel
/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if(managedObjectModel == nil) 
    {
        managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:nil] retain];  
    }
    
    return managedObjectModel;
}
#pragma mark -

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */

#pragma mark GET the persistentStoreCoordinator
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator 
{
    if (persistentStoreCoordinator == nil) 
    {
        NSString *defaultStorePath = self.defaultDBStorePath;
        
        NSLog(@"DefaultPath:%@",defaultStorePath);
        
        NSString *defaultDBFileName =  [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
        
        NSString *dbFileName=defaultStorePath.length?[defaultStorePath lastPathComponent]:[defaultDBFileName stringByAppendingPathExtension:@"sqlite"];
        
        NSString *storePath =self.dbStorePath ? self.dbStorePath : [[self applicationDocumentsDirectory] stringByAppendingPathComponent:dbFileName];
         /*
          Set up the store.
          For the sake of illustration, provide a pre-populated default store.
        */
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        // If the expected store doesn't exist, copy the default store.
        if (![fileManager fileExistsAtPath:storePath]) 
        {
            if (defaultStorePath) 
            {
                [fileManager copyItemAtPath:defaultStorePath toPath:storePath error:NULL];
            }
        }
        
         NSLog(@"StorePath:%@",storePath);
        
        NSURL *storeUrl = [NSURL fileURLWithPath:storePath];
        
        NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];	
        persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
        
        
        NSError *error;
        if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) 
           {
             NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            // exit(-1);  // Fail
           }
        
	}
    
    return persistentStoreCoordinator;
}
#pragma mark -

#pragma mark GET the default managedObjectContext
- (NSManagedObjectContext *) defaultManagedObjectContext 
{
    if (defaultManagedObjectContext == nil) 
    {
        
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) 
    {
        defaultManagedObjectContext = [[NSManagedObjectContext alloc] init];
        [defaultManagedObjectContext setPersistentStoreCoordinator: coordinator];
    }
        
    }
    
    return defaultManagedObjectContext;
}
#pragma mark -

#pragma mark GET all the changed object related to the context
-(NSSet *)_changedObjectsInContex:(NSManagedObjectContext *)context
{
    if(![context hasChanges]) return nil;
    
    NSSet *results =[context insertedObjects];
    if(![results count])
    {
        results =[context deletedObjects];
        if(![results count])
        {
            results =[context updatedObjects];
        }
    }

    return results;
}

#pragma mark -

//#warning Under development
#pragma mark PERFORM CREATE/DELETE/UPDATE on new context in background
-(void)performAsync:(BOOL)isAsync inNewContextWithOperation:(void(^)(NSManagedObjectContext *context))operation completion:(void(^)(NSSet *result, NSError *error))completionBlock
{
    if(!isAsync)
    {
        NSManagedObjectContext *context=[[NSManagedObjectContext alloc] init];
        [context setUndoManager:nil];
        [context setPersistentStoreCoordinator:self.defaultManagedObjectContext.persistentStoreCoordinator];
    
        if(operation) operation(context);
        
        NSSet *_changedSet=[self _changedObjectsInContex:context];
        
        NSMutableSet *_results=[NSMutableSet set];
        NSError *error=nil;
        
       if(_changedSet)
        {
            [self saveContext:context error:error];
            
            ///Thransfer the objects from new context to main context through objectID 
            
            NSManagedObjectContext *_mainContext=self.defaultManagedObjectContext;
            
            [_changedSet enumerateObjectsUsingBlock:^(id obj, BOOL *stop)
            {
                
                NSManagedObject *_newObject=[obj inContext:_mainContext];
                [_results addObject:_newObject];
            }];
        }
    
        if(completionBlock) completionBlock(_results,error);
        
         [context reset];
         [context release];
        
        
        return;
    }
    
    dispatch_queue_t queue = dispatch_queue_create("com.gcd.request", 0ul);

    //dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    
    dispatch_async(queue, ^{
        
        //Create a new context with same persistentStoreCoordinator
        NSManagedObjectContext *context=[[NSManagedObjectContext alloc] init] ;
        [context setUndoManager:nil];
        [context setPersistentStoreCoordinator:self.self.defaultManagedObjectContext.persistentStoreCoordinator];
        
        //perform operation on this thread
        if(operation) operation(context);
        
        //Get the just inserted/updated/deleted  objects
        NSSet *_changedSet=[self _changedObjectsInContex:context];        
        
        if(_changedSet)
        {
            NSError *error=nil;
            //context should be saved and merged into nmain context
            [self saveContext:context error:error];
            
            if(error)
            NSLog(@"Error:%@",[error localizedDescription]);
            
            //Back to main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ///Thransfer the objects from new context to main context
                NSMutableSet *_results=[NSMutableSet set];
                NSManagedObjectContext *_mainContext=self.defaultManagedObjectContext;
            
                [_changedSet enumerateObjectsUsingBlock:^(id obj, BOOL *stop) 
                {
                    NSManagedObject *_newObject=[obj inContext:_mainContext];
                    if(_newObject)
                    [_results addObject:_newObject];
                }];


                if(completionBlock) completionBlock(_results,error);
                 
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
            if(completionBlock)  completionBlock(nil,nil);
                
            });
        }
        
        [context reset];
        [context release];
    });
    
    dispatch_release(queue);

}


-(void)performInDefaultContext:(void(^)(NSManagedObjectContext *context))operation completion:(void(^)(NSSet *result, NSError *error))completionBlock
{
        NSManagedObjectContext *context=self.defaultManagedObjectContext;
        
        if(operation) operation(context);
        
        NSSet *results =[self _changedObjectsInContex:context];        
        NSError *error=nil;
        
        [self save:error];
   
        if(completionBlock) completionBlock(results,error);
}
#pragma mark -


+(void)performAsyncFetchRequest:(NSFetchRequest *)aFetchRequest completion:(void(^)(NSFetchRequest *fetchRequest, NSArray *result, NSError *error))completionBlock
{
    NSFetchRequest *_fetchRequest=[aFetchRequest copy];
    
    NSManagedObjectContext *_mainContext=DEFAULT_CONTEXT;
    
    dispatch_queue_t queue = dispatch_queue_create("com.gcd.request", 0ul);
    dispatch_queue_t	mainQueue		= dispatch_get_main_queue();
    
    dispatch_async(queue, ^{
        
        //Create a new context with same persistentStoreCoordinator
        NSManagedObjectContext *context=[[NSManagedObjectContext alloc] init] ;
        [context setUndoManager:nil];
        [context setPersistentStoreCoordinator:_mainContext.persistentStoreCoordinator];
        
        //GET the objectIDS as result
        [_fetchRequest setResultType:NSManagedObjectIDResultType];
        [_fetchRequest setIncludesPropertyValues:NO];
        
        NSError *_error=nil;
        NSArray *_fetchedObjectIDS=[context executeFetchRequest:_fetchRequest error:&_error];
        [_fetchRequest release];
        
        if(_fetchedObjectIDS)
        {
               //Back to main thread
               dispatch_async(mainQueue, ^{
                
                ///Thransfer the objects from new context to main context
                NSFetchRequest *_modifiedFetchRequest=[aFetchRequest copy];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self IN %@", _fetchedObjectIDS];
                [_modifiedFetchRequest setPredicate:predicate];
                
                 NSError *error=nil;
                 NSArray *_results=[_mainContext executeFetchRequest:_modifiedFetchRequest error:&error];
            
                [_modifiedFetchRequest release];
                
                if(completionBlock) completionBlock(aFetchRequest,_results,error);
            });
            
        }
        else
        {
            ///Call on the main thread
               dispatch_sync(mainQueue, ^{
                if(completionBlock) completionBlock(aFetchRequest,nil,_error);
            });
            
        }
        
        [context reset];
        [context release];
        
    });
    
    dispatch_release(queue);
}


#pragma mark Save Context
-(void)saveContext:(NSManagedObjectContext *)context error:(NSError *)error
{
    if(!context || ![context hasChanges]) return;
    
    BOOL _mergeChanges=(self.defaultManagedObjectContext != context);
    
    if(_mergeChanges)
    [self registerContextDidSaveNotificationOnContext:context];
    
   // if ([context hasChanges] && ![context save:&error])
    if(![context save:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    } 
    
    if(_mergeChanges)
        [self removeContextDidSaveNotificationOnContext:context];
    
}
-(void)save:(NSError *)error
{
    [self saveContext:defaultManagedObjectContext error:error];
}

#pragma mark -

-(NSManagedObject *)objectWithID:(NSManagedObjectID *)objectId inContext:(NSManagedObjectContext *)context
{
    return  (context==nil)?[self.defaultManagedObjectContext objectWithID:objectId] : [context objectWithID:objectId]; 
    
}

-(NSSet *)_mergedObjects:(NSNotification *)aNotification
{
    NSManagedObject *object=nil;
    NSSet *_mergedSet=[aNotification.userInfo valueForKey:@"updated"];
    if([_mergedSet count])
    {
        return _mergedSet;
    }
    else
    {
        _mergedSet=[aNotification.userInfo valueForKey:@"deleted"];
        
        if([_mergedSet count])
        {
            return _mergedSet;
        }
        else
        {
            _mergedSet=[aNotification.userInfo valueForKey:@"inserted"];
        }
    }
     
        if(![_mergedSet count]) return nil;
    
    
        object=[[_mergedSet allObjects] objectAtIndex:0];
        NSDictionary *att=[[object entity] attributesByName];
        NSMutableArray *predicates=[NSMutableArray array];
    
        for(NSString *property in [att allKeys])
        {      
            NSArray *value=[_mergedSet valueForKey:property];
            NSPredicate *predicate=[NSPredicate predicateWithFormat:@"%K IN %@",property,value];
            [predicates addObject:predicate];
        }
        
        if(!object) return nil;
        
        NSPredicate *_predicate=[NSCompoundPredicate andPredicateWithSubpredicates:predicates];
     
       
    //NSPredicate *_predicate=[NSPredicate predicateWithFormat:@"self IN %@",[[_mergedSet allObjects] valueForKey:@"objectID"]];
    NSMutableArray *arr=[[object class] fetchWithPredicate:_predicate sortDescriptors:nil limit:0 error:nil];
        
    
    return [NSSet setWithArray:arr];
}
#pragma mark merge changes form another context to new context
- (void)mergeChangesToDefaultContext:(NSNotification *)notification 
{
    if(![NSThread isMainThread])
    {
        [self performSelectorOnMainThread:@selector(mergeChangesToDefaultContext:) withObject:notification waitUntilDone:YES];
    
    }
    else
    {
        NSManagedObjectContext *mainContext = [self defaultManagedObjectContext];

        // main context saved, no need to perform the merge.
        if ([notification object] == mainContext) return;
         [mainContext mergeChangesFromContextDidSaveNotification:notification];
        
       // NSSet *mergedObjects=[self _mergedObjects:notification];
        //NSLog(@"count:%d",[mergedObjects count]);
    }
}
#pragma mark -


@end


#pragma mark Insert
@implementation SSCoreDataManager(Insert)

//Under development
+(NSManagedObject *)insertNewRowForEntityForName:(NSString *)entityName inContext:(NSManagedObjectContext *)context checkDuplicateOn:(NSArray *)properties predicate:(NSPredicate *)predicate
{
    NSManagedObject *ent=(NSManagedObject *)[NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    
    NSMutableArray *fetchedResult= [[ent class] fetchOnContext:context predicate:predicate onAttributes:properties error:nil];
        
        if(fetchedResult.count == 0) 
        {
            //[context insertObject:entityObject];
            NSManagedObject *entityObject= [[ent class] insertNewObjectInContext:context];
            //[self insertNewRowForEntityForName:entityName inContext:context];
              
            return entityObject;
        }
    
    return [fetchedResult lastObject];
}

+(NSManagedObject *)insertNewRowIntoDefaultContextForEntityForName:(NSString *)entityName checkDuplicateOn:(NSArray *)properties predicate:(NSPredicate *)predicate
{
    return [SSCoreDataManager insertNewRowForEntityForName:entityName inContext:[SSCoreDataManager defaultContext] checkDuplicateOn:properties predicate:predicate];
}


//#warning UNDER DEVELOPMENT
+(NSMutableArray *)insertFromArray:(NSArray *)objects forEntityName:(NSString *)entityName inContext:(NSManagedObjectContext *)context checkDuplicateOn:(NSArray *)properties
{ 
     
    return [NSClassFromString(entityName) checkOrCreateObjectsFromArray:objects inContext:context checkDuplicateOn:properties];
}


@end
#pragma mark -

#pragma mark DELETE
@implementation SSCoreDataManager(Delete)

+(void)deleteObject:(id)object inContext:(NSManagedObjectContext *)context
{
    /*
    if(context && object)
    {
        NSManagedObjectContext *_mainContext=DEFAULT_CONTEXT;
        
        BOOL _isMainContext=(context==_mainContext);
        if(_isMainContext)
        {
            [context deleteObject:object];
        }
        else
        {
            [context deleteObject:[context objectWithID:[object objectID]]];
            
        }
    }
    */
    
    if(context && object)
    [object deleteInContext:context];
}

+(void)deleteObject:(id)object
{
    [SSCoreDataManager deleteObject:object inContext:DEFAULT_CONTEXT];
}

@end
#pragma mark -

#pragma mark Update
@implementation SSCoreDataManager(Update)

+(void)updateObject:(NSManagedObject *)object inContext:(NSManagedObjectContext *)context updateBlock:(void(^)(NSManagedObject *object))updateBlock
{
    /*
    BOOL _isMainContext=(context==DEFAULT_CONTEXT);
    
    NSManagedObject *newObject=_isMainContext? object :[context objectWithID:[object objectID]];
    [newObject retain];
    if(updateBlock) updateBlock(newObject);
    [newObject release];
     */
    
    if(object)
    [object updateInContext:context updateBlock:updateBlock];
}

+(void)updateObject:(NSManagedObject *)object updateBlock:(void(^)(NSManagedObject *object))updateBlock
{
    [SSCoreDataManager updateObject:object inContext:DEFAULT_CONTEXT updateBlock:updateBlock];
}

@end

@implementation SSCoreDataManager(NSFetchedResultsController)

- (NSFetchedResultsController *)fetchedResultsControllerWithFetchRequest:(NSFetchRequest *)aFetchRequest
{
	// Create and initialize the fetch results controller.
	NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:aFetchRequest managedObjectContext:self.defaultManagedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    
	return [aFetchedResultsController autorelease];
} 

#if NS_BLOCKS_AVAILABLE
-(void)addFetchedResultsControllerWillChangeContent:(ChangeContentblock)block
{
    if(block)
        _willChangeContentBlock=Block_copy(block);
    
}

-(void)addFetchedResultsControllerDidChangeContent:(ChangeContentblock)block
{
    if(block)
        _didChangeContentBlock=Block_copy(block);
}

-(void)addFetchedResultsControllerDidChangeObject:(DidChangeObject)block
{
    if(block)
        _didChangeObjectBlock=Block_copy(block);
}

-(void)addFetchedResultsControllerDidChangeSection:(DidChangeSectionBlock)block
{
    if(block)
        _didChangeSectionBlock=Block_copy(block);
}


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
    
    if(_willChangeContentBlock) _willChangeContentBlock(controller);
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
	
    
    if(_didChangeObjectBlock)
        _didChangeObjectBlock(controller,anObject,indexPath,type,newIndexPath);
    /*
     switch(type) {
     
     case NSFetchedResultsChangeInsert:
     break;
     
     case NSFetchedResultsChangeDelete:
     
     break;
     
     case NSFetchedResultsChangeUpdate:
     
     break;
     
     case NSFetchedResultsChangeMove:
     
     break;
     }
     */
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
	
    /*
     switch(type) 
     {
     
     case NSFetchedResultsChangeInsert:
     break;
     
     case NSFetchedResultsChangeDelete:
     
     break;
     }
     */
    
    if(_didChangeSectionBlock)
        _didChangeSectionBlock(controller,sectionInfo,sectionIndex,type);
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    
    if(_didChangeContentBlock)
        _didChangeContentBlock(controller);
}
#endif


@end

#pragma mark -

#import <objc/runtime.h>
@implementation NSManagedObject (NSManagedObject_Dictionary)
@dynamic traversed;

static char traversedKey;

-(void)setTraversed:(BOOL)_traversed
{
    if(_traversed!=[self traversed])
        objc_setAssociatedObject(self, &traversedKey, [NSNumber numberWithBool:_traversed], OBJC_ASSOCIATION_ASSIGN);
}

-(BOOL)traversed
{
    NSNumber *number=(NSNumber *)objc_getAssociatedObject(self, &traversedKey);
    return  [number boolValue];
}

#pragma mark NSManagedObject to NSDictionary
- (NSDictionary*) toDictionary
{
    self.traversed = YES;
    
    NSArray* attributes = [[[self entity] attributesByName] allKeys];
    NSArray* relationships = [[[self entity] relationshipsByName] allKeys];
    NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithCapacity:
                                 [attributes count] + [relationships count] + 1];
    
    [dict setObject:[[self class] description] forKey:@"class"];
    
    for (NSString* attr in attributes) {
        NSObject* value = [self valueForKey:attr];
        
        if (value != nil) {
            [dict setObject:value forKey:attr];
        }
    }
    
    for (NSString* relationship in relationships) {
        NSObject* value = [self valueForKey:relationship];
        
        if ([value isKindOfClass:[NSSet class]]) 
        {
            // To-many relationship
            
            // The core data set holds a collection of managed objects
            NSSet* relatedObjects = (NSSet*) value;
            
            // Our set holds a collection of dictionaries
            NSMutableSet* dictSet = [NSMutableSet setWithCapacity:[relatedObjects count]];
            
            for (NSManagedObject* relatedObject in relatedObjects) 
            {
                if (!relatedObject.traversed) 
                {
                    [dictSet addObject:[relatedObject toDictionary]];
                }
            }
            
            [dict setObject:dictSet forKey:relationship];
        }
        else if ([value isKindOfClass:[NSManagedObject class]]) 
        {
            // To-one relationship
            
            NSManagedObject* relatedObject = (NSManagedObject*) value;
            
            if (!relatedObject.traversed) 
            {
                // Call toDictionary on the referenced object and put the result back into our dictionary.
                [dict setObject:[relatedObject toDictionary] forKey:relationship];
            }
        }
    }
    
    return dict;
}

#pragma mark NSDictionary to NSManagedObject

//Populate on giving context
- (void) populateFromDictionary:(NSDictionary*)dict onContext:(NSManagedObjectContext *)newContext
{
    NSManagedObjectContext* context =newContext;
    
    for (NSString* key in dict) 
    {
        if ([key isEqualToString:@"class"]) 
        {
            continue;
        }
        
        NSObject* value = [dict objectForKey:key];
        
        if ([value isKindOfClass:[NSDictionary class]]) 
        {
            // This is a to-one relationship
            NSManagedObject* relatedObject =
            [NSManagedObject createObjectFromDictionary:(NSDictionary*)value
                                                     inContext:context];
            
            [self setValue:relatedObject forKey:key];
        }
        else if ([value isKindOfClass:[NSSet class]]) 
        {
            // This is a to-many relationship
            NSSet* relatedObjectDictionaries = (NSSet*) value;
            
            // Get a proxy set that represents the relationship, and add related objects to it.
            // (Note: this is provided by Core Data)
            NSMutableSet* relatedObjects = [self mutableSetValueForKey:key];
            
            for (NSDictionary* relatedObjectDict in relatedObjectDictionaries) 
            {
                NSManagedObject* relatedObject =[NSManagedObject createObjectFromDictionary:relatedObjectDict
                                                                                         inContext:context];
                if(relatedObject)
                [relatedObjects addObject:relatedObject];
            }
        }
        else if (value != nil) 
        {
            //This is an attribute
            
            NSLog(@"%@=%@",key,value);
            [self setValue:value forKey:key];
        }
    }
}

//Populate on mainContext
- (void) populateFromDictionary:(NSDictionary*)dict
{
    NSManagedObjectContext* context = [self managedObjectContext];
    [self populateFromDictionary:dict onContext:context];
}


+ (NSManagedObject*)createObjectFromDictionary:(NSDictionary*)dict
                                            inContext:(NSManagedObjectContext*)context
{
    NSString* class = [dict objectForKey:@"class"];
    
    //Change by sandip
    if(!class)
    {
        class=NSStringFromClass([self class]);
        [dict setValue:class forKey:@"class"];
    }
    
    NSManagedObject* newObject =(NSManagedObject*)[NSEntityDescription insertNewObjectForEntityForName:class
                                                    inManagedObjectContext:context];
    
    [newObject populateFromDictionary:dict];
    
    return newObject;
}

#pragma mark Create from a dictionary with checking the duplicates
+ (NSManagedObject*)createObjectFromDictionary:(NSDictionary*)dict
                                            inContext:(NSManagedObjectContext*)context checkDuplicates:(NSArray *)attributes predicateFormat:(NSString *)predicateFormatStr
{
    NSString* class = [dict objectForKey:@"class"];
    
    //Change by sandip
    if(!class)
    {
        class=NSStringFromClass([self class]);
        [dict setValue:class forKey:@"class"];
    }

    //START:Fetching the duplicates
    
    NSMutableArray *arrgs=[NSMutableArray arrayWithCapacity:[attributes count]];
    for(NSString *attbName in attributes)
    {
        NSString *value=[dict valueForKey:attbName];
        if(value)
        [arrgs addObject:value];    
    }
    
    //Constructing the predicate with the given attributes
    NSPredicate *predicate=[NSPredicate predicateWithFormat:predicateFormatStr argumentArray:arrgs];
    //Now fetch
    NSArray *fetchedRes=[[self class] fetchOnContext:context predicate:predicate onAttributes:attributes sortDescriptors:nil limit:0 error:nil];
    
    if([fetchedRes count]) return [fetchedRes lastObject];
    
    //END:Fetching the duplicates
    
    //CREATE: New new object if there is no existing object
    NSManagedObject* newObject=(NSManagedObject*)[NSEntityDescription insertNewObjectForEntityForName:class
                                                        inManagedObjectContext:context];
    [newObject populateFromDictionary:dict];
    
    return newObject;
}

+(NSMutableArray *)checkOrCreateObjectsFromArray:(NSArray *)objects inContext:(NSManagedObjectContext *)context checkDuplicateOn:(NSArray *)properties
{ 
    NSMutableArray *predicates=[NSMutableArray array];
    for(NSString *property in properties)
    {         
        NSArray *value=[objects valueForKey:property];
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"%K IN %@",property,value];
        [predicates addObject:predicate];
    }
    
    
    NSPredicate *fetchPredicate=[NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    NSLog(@"%@",fetchPredicate.predicateFormat);
    
    NSMutableArray *fetchedResult= [[self class] fetchOnContext:DEFAULT_CONTEXT predicate:fetchPredicate onAttributes:properties sortDescriptors:nil limit:0 startIndex:0 resultType:NSDictionaryResultType error:nil];
    
    if([fetchedResult count]==[objects count]) return nil;
    
    NSMutableArray *sourceArray=[NSMutableArray arrayWithArray:objects];
    [sourceArray removeObjectsInArray:fetchedResult];
    
    NSMutableArray *results=[[NSMutableArray array] retain];
    for(NSDictionary *dict in sourceArray)
    {
        NSManagedObject *obj=[[self class] createObjectFromDictionary:dict inContext:context];
        [results addObject:obj];
    }

    return [results autorelease];
}

#pragma mark -

@end

#pragma mark FETCH NSManagedObject
@implementation NSManagedObject (Fetch)

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit startIndex:(NSUInteger)offset resultType:(NSFetchRequestResultType)type error:(NSError *)error
{
    NSString *entityName=NSStringFromClass([self class]);
    
    NSEntityDescription *ent = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest fetchRequestWithEntity:ent predicate:predicate sortDescriptors:sortDescriptors attributes:attributes limit:limit] retain];
    
    if(offset>0)
        [fetchRequest setFetchOffset:offset];
    
      if(type!=NSManagedObjectResultType)
      [fetchRequest setResultType:type];
    
	
	NSMutableArray *mutableFetchResults = [[[context executeFetchRequest:fetchRequest error:&error] mutableCopy] autorelease];
    
    [fetchRequest release];
    
    return mutableFetchResults;
}

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit startIndex:(NSUInteger)offset error:(NSError *)error
{
    return [self fetchOnContext:context predicate:predicate onAttributes:attributes sortDescriptors:sortDescriptors limit:limit startIndex:0 resultType:NSManagedObjectResultType error:error];
}

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit error:(NSError *)error
{
	return [self fetchOnContext:context predicate:predicate onAttributes:attributes sortDescriptors:sortDescriptors limit:limit startIndex:0 error:error];
}

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes error:(NSError *)error
{
    return [[self class] fetchOnContext:context predicate:predicate onAttributes:attributes sortDescriptors:nil limit:0 error:error];
}

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors error:(NSError *)error
{
    return [[self class] fetchOnContext:context predicate:predicate onAttributes:nil sortDescriptors:sortDescriptors limit:0 error:error];
}

+(NSMutableArray *)fetchWithPredicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit error:(NSError *)error
{
    return [[self class] fetchOnContext:[SSCoreDataManager defaultContext] predicate:predicate onAttributes:attributes sortDescriptors:sortDescriptors limit:limit error:error];
}

+(NSMutableArray *)fetchWithPredicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors error:(NSError *)error
{
    return [[self class] fetchOnContext:[SSCoreDataManager defaultContext] predicate:predicate onAttributes:nil sortDescriptors:sortDescriptors limit:0 error:error];
}

+(NSMutableArray *)fetchWithPredicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit error:(NSError *)error
{
    return [[self class] fetchOnContext:[SSCoreDataManager defaultContext] predicate:predicate onAttributes:nil sortDescriptors:sortDescriptors limit:limit error:error];
}


+(NSMutableArray *)fetchWithPredicate:(NSPredicate *)predicate error:(NSError *)error
{
    return [[self class] fetchOnContext:[SSCoreDataManager defaultContext] predicate:predicate onAttributes:nil sortDescriptors:nil limit:0 error:error];
}

+(NSMutableArray *)fetchAll:(NSError *)error
{
    return [[self class] fetchOnContext:[SSCoreDataManager defaultContext] predicate:nil onAttributes:nil sortDescriptors:nil limit:0 error:error];
}

@end
#pragma mark -

#pragma mark OPERATION NSManagedObject
@implementation NSManagedObject (OPERATION)


+(NSEntityDescription *)entity
{
    NSEntityDescription *ent=[NSEntityDescription entityForName:NSStringFromClass([self class]) inManagedObjectContext:nil];
    
    return ent;
}

- (NSManagedObject *)inContext:(NSManagedObjectContext*)context
{
    return [context objectWithID:[self objectID]];
            
    NSError *error = nil;
    NSManagedObject *inContext = [[context existingObjectWithID:[self objectID] error:&error] retain];
    return [inContext autorelease];
}

+(NSManagedObject *)insertNewObjectInContext:(NSManagedObjectContext *)context
{
    NSString *entityName=NSStringFromClass([self class]);
    NSManagedObject *entityObject = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext: context];
    
    return entityObject;
}

+(NSManagedObject *)insertNewObject
{
    return [self insertNewObjectInContext:[SSCoreDataManager defaultContext]];
}



-(void)updateInContext:(NSManagedObjectContext *)context updateBlock:(void(^)(NSManagedObject *object))updateBlock
{
    BOOL _isMainContext=(context==DEFAULT_CONTEXT);
    
    NSManagedObject *newObject=_isMainContext? self :[self inContext:context];
    [newObject retain];
    if(updateBlock) updateBlock(newObject);
    [newObject release];
}

-(void)updateWithBlock:(void(^)(NSManagedObject *object))updateBlock
{
    [self updateInContext:DEFAULT_CONTEXT updateBlock:updateBlock];
}

-(void)deleteInContext:(NSManagedObjectContext *)context
{
    if(!context ) return;
    
    NSManagedObjectContext *_mainContext=DEFAULT_CONTEXT;
    
    BOOL _isMainContext= (context == _mainContext);
    
    if(_isMainContext)
    {
        [context deleteObject:self];
    }
    else
    {
        [context deleteObject:[self inContext:context]];
    }
}

-(void)delete
{
    [self deleteInContext:DEFAULT_CONTEXT];
}

@end
#pragma mark -


@implementation NSFetchRequest (CoreDataManager)

+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors attributes:(NSArray *)attributes limit:(NSUInteger)limit
{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    if(entity)
        [fetchRequest setEntity:entity];
    
    if(predicate)
        [fetchRequest setPredicate:predicate];
    
    if(sortDescriptors)
        [fetchRequest setSortDescriptors:sortDescriptors];
    
    if(attributes)
        [fetchRequest setPropertiesToFetch:attributes];
    
    if(limit>0)
        [fetchRequest setFetchLimit:limit];
    
    return [fetchRequest autorelease];
}

+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit
{
    return [NSFetchRequest fetchRequestWithEntity:entity predicate:predicate sortDescriptors:sortDescriptors attributes:nil limit:limit];
}

+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors attributes:(NSArray *)attributes
{
    return [NSFetchRequest fetchRequestWithEntity:entity predicate:predicate sortDescriptors:sortDescriptors attributes:attributes limit:0];
}


+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors
{
    
    return [self fetchRequestWithEntity:entity predicate:predicate sortDescriptors:nil limit:0];
}


+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate {
    
    return [self fetchRequestWithEntity:entity predicate:predicate sortDescriptors:nil];
}

+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity {
    
    return [self fetchRequestWithEntity:entity predicate:nil];
}



@end

