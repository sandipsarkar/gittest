//
//  CoreDataHandler.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 10/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CoreDataHandler.h"
#import "SSCoreDataManager.h"
#import "Customer.h"
#import "Task.h"
#import "Visit.h"
#import "Rep.h"
#import "News.h"
#import "Offer.h"
#import "ScheduledVisit.h"
#import "WholesaleDealer.h"
#import "OEM.h"
#import "UIAlertView+BlockExtensions.h"
#import "VisitPhoto.h"
#import "Territory.h"
#import "OfflineStore.h"
#import "Reminder.h"
#import "OfflineManager.h"
#import "CustomEventStore.h"

#define SHOW_ACTIVE_OFFERS_ONLY 1

@interface CoreDataHandler()
{
    NSManagedObjectContext *__managedObjectContext;
    NSManagedObjectModel *__managedObjectModel;
    NSPersistentStoreCoordinator *__persistentStoreCoordinator;
}

@end

@implementation CoreDataHandler

@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;
@synthesize storePath;

-(void)dealloc
{
    [storePath release];
    storePath=nil;
    
    [super dealloc];
}
/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil)
    {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        __managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        
        if([__managedObjectContext respondsToSelector:@selector(setMergePolicy:)])
        [__managedObjectContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
        
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil)
    {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"RepVisitationTool" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return __managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil)
    {
        return __persistentStoreCoordinator;
    }
    

    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    

    return __persistentStoreCoordinator;
    
#if USE_DB_MIGRATION
    NSString *storeURLPath  = self.storePath;
    NSString *storeFileName = [storeURLPath lastPathComponent];
    
    if(storeFileName.length)
        [self addPersistentStoreForStoreAtPath:storeURLPath];
    
#else
    
    NSURL *storeURL = nil;
    
    if(self.storePath.length)
    {
        storeURL = [NSURL fileURLWithPath:self.storePath];
    }
    else
    {
        storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"RepVisitationTool.sqlite"];
    }
    
    
    if(storeURL)
    {
          NSError *error = nil;

        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
           /*
            NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption:@YES,
                                      NSInferMappingModelAutomaticallyOption:@YES,
                                      NSSQLitePragmasOption: @{@"journal_mode": @"DELETE"}
                                      };
            */

    //------------------------------------------------------------------------
        
        NSDictionary *options = nil;
            
        // Check if we need a migration
   
        BOOL isModelCompatible = [self isModelCompatibleWithPersistentStoreAtURL:storeURL];
            
        if(!isModelCompatible)
        {
            options = @{NSMigratePersistentStoresAutomaticallyOption:@YES,
                                      NSInferMappingModelAutomaticallyOption:@YES,
                                      NSSQLitePragmasOption: @{@"journal_mode": @"DELETE"}
                                      };
        }
        
        NSPersistentStore *persistentStore = [__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error];
        
        if (!persistentStore)
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            
            /*
            [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
            [__persistentStoreCoordinator release];
            __persistentStoreCoordinator = nil;
            return [self persistentStoreCoordinator];
            */
            
            abort();
        }
            /* ENCRYPT COREDATA STORE
            NSDictionary *fileAttributes = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
            
            ﻿    ﻿if(![[NSFileManager defaultManager] setAttributes:fileAttributes ofItemAtPath:storePath error:&error])
                 {
                ﻿    ﻿    ﻿// Handle error
                ﻿    ﻿}
            */

      }
        else
        {
            NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
            
            NSPersistentStore *persistentStore = [__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error];
            
            if (!persistentStore)
            {
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }
            
         }

    }
    
#endif
    
    return __persistentStoreCoordinator;
}

#pragma mark-
#pragma mark Coredata migration


-(BOOL)isModelCompatibleWithPersistentStoreAtURL:(NSURL *)storeURL
{
    
    return [SSCoreDataManager isModelCompatibleWithPersistentStoreAtURL:storeURL forPersistentStoreCoordinator:__persistentStoreCoordinator];
}


- (BOOL)checkForMigrationAtTargetPath:(NSString *)targetPath sourceStorePath:(NSString *)sourcePath
{
    BOOL migrated = [SSCoreDataManager checkForMigrationAtTargetPath:targetPath sourceStorePath:sourcePath targetObjectModel:[self.persistentStoreCoordinator managedObjectModel] onMigrationCallback:^(BOOL success){
        
            if(success)
            {
              //********************** REMOVE OLD DB FILE  ****************************
              if([[NSFileManager defaultManager] removeItemAtPath:sourcePath error:nil])
              {
                 //********************** Replace to OLDDB path  ****************************
        
                [[NSFileManager defaultManager] moveItemAtPath:targetPath toPath:sourcePath error:nil];
              }
        
              //********************** Move to OLDDB path  ****************************
              //NSString *oldDBFilePath=[DirectoryManager oldDatabasePathForName:[sourcePath lastPathComponent]];
              //[[NSFileManager defaultManager] moveItemAtPath:sourcePath toPath:oldDBFilePath error:nil];
            }
            else if([[NSFileManager defaultManager] fileExistsAtPath:targetPath])
            {
                  [[NSFileManager defaultManager] removeItemAtPath:targetPath error:nil];
            }
        }];
    
        return migrated;
}//END

-(void)addPersistentStoreForStoreAtPath:(NSString *)newStorePath
{
    NSString *storeTargetPath = newStorePath;
    if(newStorePath)
    {
        //============================== CORE DATA MIGRATION ===========================
        NSInteger maxVer=NSIntegerMin;
        NSSet *versionIdentifiers = [[self managedObjectModel] versionIdentifiers];
        for(NSString *verstr in versionIdentifiers)
        {
            maxVer = MAX(maxVer, [verstr integerValue]);
        }
        
        // if ([versionIdentifiers containsObject:@"2.0"])
        if(maxVer>=2)
        {
            NSString *fileName = [[newStorePath lastPathComponent] stringByDeletingPathExtension];
            
            NSString *currentVersionSuffix = [NSString stringWithFormat:@"v%d",maxVer];
            
            //==== IF LATEST STORE FILE AVAILABLE DONT NEED TO MIGRATE =======
            if([fileName rangeOfString:currentVersionSuffix].location==NSNotFound)
            {
                
                NSString *targetStoreFileName = [fileName stringByAppendingFormat:@"%@.sqlite",currentVersionSuffix];
                
                NSString *targetDBStrorePath  = [[newStorePath stringByDeletingLastPathComponent] stringByAppendingPathComponent:targetStoreFileName];;
                
                NSString *sourceDBStorePath   = newStorePath;
                
                BOOL hasMigrated = [self checkForMigrationAtTargetPath:targetDBStrorePath sourceStorePath:sourceDBStorePath];
                
                if(hasMigrated){}
                
                storeTargetPath = targetDBStrorePath ;
                
            }
            
            //storeTargetPath = hasMigrated ?  targetDBStrorePath : sourceDBStorePath;
            
            NSLog(@"Store target path:%@",storeTargetPath);
        }
        //=========================================================
        
        NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption:@YES,
                                  NSInferMappingModelAutomaticallyOption:@NO,
                                  NSSQLitePragmasOption: @{@"journal_mode": @"DELETE"}
                                  };

        
        NSURL *storeUrl = [NSURL fileURLWithPath: storeTargetPath];
        NSError *error = nil;
        [self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error];
        if (error)
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        
    }
    
}


-(void)addPersistentStoreForStoreAtURL:(NSURL *)storePathURL
{
    
    BOOL _shouldMigrateAutomatically = NO;
    
    BOOL isModelCompatible = [self isModelCompatibleWithPersistentStoreAtURL:storePathURL];
    
     NSString *storeTargetPath =[storePathURL path];
    
    if(!isModelCompatible)
    {
         NSString *fileName = [storeTargetPath lastPathComponent];
        
         NSString *targetStoreFileName = [fileName stringByAppendingPathExtension:@"migrated"];
         //NSString *targetStoreFileName = [fileName stringByAppendingString:@".fftemp"];
        
         NSString *targetDBStrorePath  = [[storeTargetPath stringByDeletingLastPathComponent] stringByAppendingPathComponent:targetStoreFileName];
        
         NSString *sourceDBStorePath   = storeTargetPath;
        
        BOOL hasMigrated = [self checkForMigrationAtTargetPath:targetDBStrorePath sourceStorePath:sourceDBStorePath];
        
        if(hasMigrated)
        {
            NSLog(@"NSPersistentStore has migrated at path=%@",targetDBStrorePath);
        }
        
        /*
        #if LIVE_TEST
        //-------------- FOR THIS TEST VERSION ONLY ---------------
        else
        {
            NSString *loginEmail = [[NSUserDefaults standardUserDefaults] objectForKey:LOGIN_EMAIL_KEY];
            
            if([[NSFileManager defaultManager] fileExistsAtPath:sourceDBStorePath])
            {
                [[NSFileManager defaultManager] removeItemAtPath:sourceDBStorePath error:nil];
            }
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:loginEmail];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:LOGIN_EMAIL_KEY];
        }
        #endif
        */
      
        
        _shouldMigrateAutomatically = !hasMigrated;
        
       // storeTargetPath = targetDBStrorePath;
    }
    
       //NSDictionary *pragmaOptions = @{@"synchronous": @"OFF"};
   
     /*
        NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption:@YES,
                                  NSInferMappingModelAutomaticallyOption:_shouldMigrateAutomatically?@YES:@NO,
                                  NSSQLitePragmasOption: @{@"journal_mode": @"DELETE"}
                                  };
      */
    
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption:@YES,
                              NSInferMappingModelAutomaticallyOption:_shouldMigrateAutomatically?@YES:@NO,
                              NSSQLitePragmasOption: @{@"journal_mode": @"DELETE",@"synchronous": @"NORMAL"}
                              };

    
    
        NSURL *storeUrl = [NSURL fileURLWithPath: storeTargetPath];
        NSError *error = nil;
        [self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error];
        if (error)
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            
            //DELETE FILE ON ERROR
            /*
            if( !_shouldMigrateAutomatically &&  [[NSFileManager defaultManager] fileExistsAtPath:storeTargetPath])
            {
               BOOL _removed =  [[NSFileManager defaultManager] removeItemAtPath:storeTargetPath error:nil];
                
               if(_removed)
               {
                  [[NSUserDefaults standardUserDefaults] removeObjectForKey:LOGIN_EMAIL_KEY];
               }
            }
           */

            abort();
        }
    
}

#pragma mark-

-(void)setStorePath:(NSString *)newStorePath
{
    if(newStorePath != storePath) 
    {
        [storePath release];
        
        storePath = [newStorePath copy];
    
     //  if(!storePath.length) return;
        
        [self.managedObjectContext lock];
        
        NSArray *stores  = [self.persistentStoreCoordinator persistentStores];
        
        if([stores count])
        {
            if([stores count] && (self.managedObjectContext.persistentStoreCoordinator && [self.managedObjectContext hasChanges]))
            {
                [self.managedObjectContext save:nil];
            }
            
            [self.managedObjectContext reset];

        //====================== Remove Prev stores =============================================
            for (NSPersistentStore *store in stores)
            {
                    NSError *error =nil;
                    if (![self.persistentStoreCoordinator removePersistentStore:store error:&error]) 
                    {
                        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                        //abort();
                    }
            }
            
        }
    //=============================================================================================
    //=================================== Add new store to persistentStoreCoordinator =============//

#if USE_DB_MIGRATION
    //=========== Add new store to persistentStoreCoordinator ================//
       // [self addPersistentStoreForStoreAtPath:newStorePath];
        
          NSURL *storeUrl = [NSURL fileURLWithPath:newStorePath];
          [self addPersistentStoreForStoreAtURL:storeUrl];
    //========================================================================//
#else
      
    if(newStorePath)
    {
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            NSURL *storeUrl = [NSURL fileURLWithPath:newStorePath];
            
            BOOL isModelCompatible = [self isModelCompatibleWithPersistentStoreAtURL:storeUrl];
            
            //
            
            /*
            NSDictionary *options = @{
                NSMigratePersistentStoresAutomaticallyOption:isModelCompatible?@NO: @YES,
                NSInferMappingModelAutomaticallyOption:isModelCompatible?@NO: @YES,
                NSSQLitePragmasOption: @{@"journal_mode": @"DELETE"}
                }; //@"DELETE"
             */
            
            NSDictionary *options = @{
                                      NSMigratePersistentStoresAutomaticallyOption:isModelCompatible?@NO: @YES,
                                      NSInferMappingModelAutomaticallyOption:isModelCompatible?@NO: @YES,
                                      NSSQLitePragmasOption: @{@"journal_mode": @"DELETE",@"synchronous": @"NORMAL"}
                                    };
         
            NSError *error = nil;

            [self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error];

            if(error)
            {
              NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
              abort();
            }
            
        }
        else
        {
            NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
            
            NSURL *storeUrl = [NSURL fileURLWithPath:newStorePath];
            NSError *error = nil;
            
            [self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error];
            
            if (error)
            {
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }

        }
    }
#endif
        [self.managedObjectContext unlock];
    }
}

#pragma mark - Application's Documents directory


-(id)init
{
    if(self = [super init])
    {
        // Preparing SSCoreDataManager
        SSCoreDataManager *_coreDataManager = [SSCoreDataManager sharedManager];
        _coreDataManager.managedObjectModel          = self.managedObjectModel;
        _coreDataManager.persistentStoreCoordinator  = self.persistentStoreCoordinator;
        _coreDataManager.defaultManagedObjectContext = self.managedObjectContext;
    }
    
    return self;
}

-(id)initWithPersistentStorePath:(NSString *)aStorePath
{
    if(self = [super init])
    {
        storePath=[aStorePath copy];
        // Preparing SSCoreDataManager
        SSCoreDataManager *_coreDataManager = [SSCoreDataManager sharedManager];
        _coreDataManager.managedObjectModel = self.managedObjectModel;
        _coreDataManager.persistentStoreCoordinator = self.persistentStoreCoordinator;
        _coreDataManager.defaultManagedObjectContext = self.managedObjectContext;
    }
    
    return self;
}

+(NSNumber *)generateID
{
    NSString *globalString = [NSString globalIDString];
    NSNumber *localNumber = [NSNumber numberWithLongLong:[globalString longLongValue]];
    
    return localNumber;
}


+(Rep *)currentRep
{
    NSError *error = nil;
    NSMutableArray *arr = [Rep fetchAll:error];
    
    NSLog(@"%@",[error description]);
    
    return (!error  && [arr count]) ? [arr objectAtIndex:0] : nil;
}

+(Rep *)currentRepInContext:(NSManagedObjectContext *)context
{
    NSError *error = nil;
    NSMutableArray *arr = [Rep fetchOnContext:context predicate:nil onAttributes:nil error:error];
    
    NSLog(@"%@",[error description]);
    
    return (!error  && [arr count]) ? [arr objectAtIndex:0] : nil;
}

+(Territory *)territoryForTerritoryPosition:(NSNumber *)position
{
    NSPredicate *_fetchPredicate = [NSPredicate predicateWithFormat:@"self.territoryPosition=%@",position];
   NSMutableArray *territories = [Territory fetchWithPredicate:_fetchPredicate error:nil];
    return ([territories count]) ? [territories objectAtIndex:0] : nil;
    
}

+(Territory *)territoryForTerritoryID:(NSNumber *)territoryID
{
    NSPredicate *_fetchPredicate = [NSPredicate predicateWithFormat:@"self.territoryID=%@",territoryID];
    NSMutableArray *territories = [Territory fetchWithPredicate:_fetchPredicate error:nil];
    return ([territories count]) ? [territories objectAtIndex:0] : nil;
    
}

+(NSArray *)intersectWithDealingOEMs:(NSArray *)dealingOEMS
{
    NSMutableArray *commonOEMs = [NSMutableArray array];
    
    NSArray *connectedOEMs = [self connectedOEMS];
    
    for(OEM *oem in connectedOEMs)
    {
        NSString *oemID = [NSString stringWithFormat:@"%d",oem.oemID.integerValue];
        
        if([dealingOEMS containsObject:oemID])
        {
            [commonOEMs addObject:oem];
        }
    }

    
    return commonOEMs;
}


+(BOOL)checkDealersiteDealsCustomer:(Customer *)customer withAlertTitle:(NSString *)title message:(NSString *)message
{
    NSArray *arrDealingOEMs = [customer.dealingOEMs componentsSeparatedByString:@","];
    NSArray *dealingOEMS = [self intersectWithDealingOEMs:arrDealingOEMs];
    
    BOOL _isDealing = [dealingOEMS count];
    if(!_isDealing)
    {
        [UIAlertView showWarningAlertWithTitle:title ? title : SCHEDULE_VISIT_FAIL_TITLE message: message ? message : SCHEDULE_VISIT_FAIL_MSG];
    }

    return _isDealing;
}

+(NSArray *)connectedOEMS
{
    Rep *rep =[self currentRep];
    
    return rep.parentWdSite.parentOem.allObjects;
}

+(void)deleteVisit:(Visit *)aVisit
{
    if(!aVisit) return;
    
    NSString *objectId = [aVisit.visitID description];
    
    //====================== DELETE VISIT TEXT NOTES AND DRAWING ==================
    NSString *drawingPath = [DirectoryManager drawingDirectoryPathForName:objectId];
    if([[NSFileManager defaultManager] fileExistsAtPath:drawingPath])
    {
        [[NSFileManager defaultManager] removeItemAtPath:drawingPath error:nil];
    }
    
    //========================== DELETE VISIT PHOTOS ===============================
    for(VisitPhoto *photo in aVisit.photos)
    {
        NSFileManager *fileManager=[NSFileManager defaultManager];
        NSString *_imagePath = photo.imagePath;
        if(_imagePath.length && [fileManager fileExistsAtPath:_imagePath])
        {
            [fileManager removeItemAtPath:_imagePath error:nil];
        }
        
        NSString *_thumbImagePath = photo.thumbImagePath;
        if(_thumbImagePath.length && [fileManager fileExistsAtPath:_thumbImagePath])
        {
            [fileManager removeItemAtPath:_thumbImagePath error:nil];
        }
    }
    
    ScheduledVisit *connectedScheduledVisit = aVisit.scheduledVisit;
    
    if(connectedScheduledVisit)
    {
        NSString *eventIdentifier = [connectedScheduledVisit.eventIdentifier copy];
        
        [[OfflineManager sharedManager] addScheduledVisit:connectedScheduledVisit willDelete:YES isEditMode:NO];
        
        [SSCoreDataManager deleteObject:connectedScheduledVisit];
        
        NSLog(@"Event identifier:%@",eventIdentifier);
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
        if([CustomEventStore deleteEventForEventIdentifier:eventIdentifier])
        {
            NSLog(@"EKEvent Deleted");
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [eventIdentifier release];
            });
        }
        });
    }
    
    [SSCoreDataManager  deleteObject:aVisit];
}

+(NSInteger)oemIDForOEMName:(NSString *)oemName
{
    NSInteger oemId = NSNotFound;
   /* NSString *lkowerCasedName = [oemName lowercaseString];
    
    
    if([lkowerCasedName isEqualToString:@"subaru"])
    {
        oemId = 1;
    }
    else if([lkowerCasedName isEqualToString:@"suzuki"])
    {
        oemId = 2;  
    }
    else if([lkowerCasedName isEqualToString:@"honda"])
    {
        oemId = 3;  
    }
    else if([lkowerCasedName isEqualToString:@"general"] || [lkowerCasedName isEqualToString:@"none"])
    {
        oemId = 0;
    }*/
   NSMutableArray *arrOEM =(NSMutableArray *) [CoreDataHandler connectedOEMS];
    
    /*
    NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"self.name ==[c] %@",oemName];
    NSArray *fetchedArr= [arrOEM filteredArrayUsingPredicate:fetchPredicate];
    OEM *_oem=[fetchedArr lastObject];
    return [_oem.oemID intValue];
    */
    
    for(OEM *oem in arrOEM)
        {
            if([oemName isEqualToString:oem.name]){
                oemId=[oem.oemID intValue];
                break;
            }
        }
    
    return oemId;
}

+(NSString *)oemNameForOEMID:(NSInteger )_oemId
{
    
    NSString *_oemName = nil;
    
    switch (_oemId)
    {
        case 0:
            _oemName = @"General";
            break;

        case 1:
            _oemName = @"Subaru";
            break;
            
        case 2:
            _oemName = @"Suzuki";
            break;
            
        case 3:
            _oemName = @"Honda";
            break;
            
        default: break;
    }
    
    
    return _oemName;
}

+(void)setOfflineArray:(id )array forOperationKey:(NSString *)key inContext:(NSManagedObjectContext *)context
{
    OfflineStore *_store=[self offlineStoreForContext:context];
    
    NSData *data = array ? [NSKeyedArchiver archivedDataWithRootObject:array] : nil;
    
    if([key isEqualToString:CREATE_CUSTOMER_METHOD])
    {
        _store.offlineCustomers = data;
    }
    else if([key isEqualToString:CREATE_TASK_METHOD])
    {
        _store.offlineTasks = data;
    }
    else if([key isEqualToString:CREATE_REMINDER_METHOD])
    {
        _store.offlineReminders = data;
    }
    else if([key isEqualToString:EDIT_UPCOMMING_VISITS_METHOD])
    {
        _store.offlineScheduledVisits = data;
    }
    else if([key isEqualToString:NEWS_VIEWED_METHOD])
    {
        _store.offlineNews = data;
    }
    else if([key isEqualToString:OFFER_VIEWED_METHOD])
    {
        _store.offlineOffers = data;
    }
    
    
    if(context!=DEFAULT_CONTEXT)
    {
        [context performBlock:^{
            
           if([context hasChanges] && ![context save:nil])
           {
               
           }
        }];
    }
}


+(void)setOfflineArray:(id )array forOperationKey:(NSString *)key
{
    [self setOfflineArray:array forOperationKey:key inContext:DEFAULT_CONTEXT];
}

+(id)offlineArrayForOperationKey:(NSString *)key
{
    OfflineStore *_store = [self offlineStore];

    NSData *data= nil;
    if([key isEqualToString:CREATE_CUSTOMER_METHOD])
    {
       data = _store.offlineCustomers ;
    }
    else if([key isEqualToString:CREATE_TASK_METHOD])
    {
        data = _store.offlineTasks;
    }
    else if([key isEqualToString:CREATE_REMINDER_METHOD])
    {
        data = _store.offlineReminders;
    }
    else if([key isEqualToString:EDIT_UPCOMMING_VISITS_METHOD])
    {
       data =  _store.offlineScheduledVisits;
    }
    else if([key isEqualToString:NEWS_VIEWED_METHOD])
    {
        data = _store.offlineNews;
    }
    else if([key isEqualToString:OFFER_VIEWED_METHOD])
    {
        data = _store.offlineOffers;
    }

    NSMutableArray *arr = data ?  [NSKeyedUnarchiver unarchiveObjectWithData:data] : nil;
    
    return arr;
}

+(Customer *)newCustomer
{
   Customer *_newCustomer =(Customer *)[Customer insertNewObject];
    
   // NSString *dateString=[[NSDate date] stringFromDateWithFormat:@"yyMMddhhmmssSSS"];
    NSUInteger _hash = [[NSDate date] hash];
    _newCustomer.customerID=[NSNumber numberWithUnsignedInteger:_hash];
    
    NSLog(@"CUSTOMER_ID = %@",_newCustomer.customerID);
    
    return [_newCustomer retain];
}

+(Task *)newTask
{
    Task *_newTask=(Task *)[Task insertNewObject];
    _newTask.taskID = [NSNumber numberWithUnsignedInteger:[[NSDate date] hash]];
    return [_newTask retain];
}

+(NSMutableArray *)allCustomers
{
    NSError *error=nil;
    NSMutableArray *_customers= [Customer fetchAll:error];
    
    return error?nil:_customers;
}

+(void)fetchAllCustomersOnCompletion:(void(^)(NSArray *result,NSError *error))completionCallback
{
   // NSSortDescriptor *sortDesciptor=[[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES] autorelease];
    [Customer fetchAsynWithPredicate:nil onAttributes:nil sortDescriptors:nil limit:0 startIndex:0 completion:completionCallback];
}

+(NSMutableArray *)loadCustomersWithProperties:(NSArray *)properties
{
    NSSortDescriptor *sortDesciptors=[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
   
    NSError *error=nil;
   NSMutableArray *_customers = [Customer fetchWithPredicate:nil onAttributes:properties sortDescriptors:[NSArray arrayWithObject:sortDesciptors] limit:0 error:error];
     
      [sortDesciptors release];
    
    return error? nil : _customers;
  }

+(NSMutableArray *)allTasksForCustomer:(Customer *)aCustomer
{
    NSMutableArray *res = nil;
    NSSet *_customers = aCustomer.tasks;
    
    if(_customers)
    {
        res=[NSMutableArray arrayWithArray:_customers.allObjects];
        return res;
    }
    else 
    {
        NSError *error=nil;
        NSPredicate *fetchPredicate=[NSPredicate predicateWithFormat:@"self.parentCustomer.customerID.integerValue=%d",aCustomer.customerID.integerValue];
        
        NSMutableArray *res= [Task fetchWithPredicate:fetchPredicate error:error];
        
        return error?nil:res;
    }
    
    return res;
}

+(void)fetchAllTasksForCustomer:(Customer *)aCustomer OnCompletion:(void(^)(NSArray *result,NSError *error))completionBlock
{
    NSArray *res = nil;
    NSSet *_customers = aCustomer.tasks;
    
    if(_customers)
    {
        res =_customers.allObjects;
        
        if(completionBlock) completionBlock(res,nil);
    }
    else 
    {
        NSPredicate *fetchPredicate=[NSPredicate predicateWithFormat:@"self.parentCustomer.customerID.integerValue=%d",aCustomer.customerID.integerValue];
        
        [Task fetchAsynWithPredicate:fetchPredicate onAttributes:nil sortDescriptors:nil limit:0 startIndex:0 completion:completionBlock];
    }
}

+(NSMutableArray *)allTasks
{
    NSError *error=nil;
    NSMutableArray *tasks= [Task fetchAll:error];
    
    return error?nil:tasks;
}

+(NSMutableArray *)filteredTasksForPredicate:(NSPredicate *)predicate
{
    NSError *error=nil;
    NSMutableArray *tasks= [Task fetchWithPredicate:predicate error:error];
    
    return error?nil:tasks;
}

+(void)fetchAllTasksOnCompletion:(void(^)(NSArray *result,NSError *error))completionBlock
{
    [Task fetchAsynWithPredicate:nil onAttributes:nil sortDescriptors:nil limit:0 startIndex:0 completion:completionBlock];
}


+(void)fetchUpcommingTasksOnCompletion:(void(^)(NSArray *result,NSError *error))completionBlock
{
    NSDate *dateToFetch = [[NSDate date] currentDateHourMinute];
    
    NSLog(@"dateToFetch=%@",dateToFetch);
    NSLog(@"curentDate=%@",[NSDate date]);
    
    NSPredicate *upcommingTaskPredicate=[NSPredicate predicateWithFormat:@"self.dueDate>=%@",dateToFetch];
    NSSortDescriptor *sortDescriptor=[[[NSSortDescriptor alloc] initWithKey:@"dueDate" ascending:YES] autorelease];
    
    [Task fetchAsynWithPredicate:upcommingTaskPredicate onAttributes:nil sortDescriptors:[NSArray arrayWithObject:sortDescriptor] limit:0 startIndex:0 completion:completionBlock];
}

+(Visit *)newVisit
{
    Visit *_newVisit=(Visit *)[Visit insertNewObject];
    
    long _visitId = [[NSDate date] hash];
    //[[[NSDate date] stringFromDateWithFormat:@"yyyyMMddhhmmssSS"] longLongValue];
    _newVisit.visitID = [NSNumber numberWithLong:_visitId];
    
    return [_newVisit retain];
}

+(NSMutableArray *)allVisits
{
    NSError *error=nil;
    
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc] initWithKey:@"startDate" ascending:NO];
    NSMutableArray *_visits= [Visit fetchWithPredicate:nil sortDescriptors:[NSArray arrayWithObject:sortDescriptor] error:error];
    //[Visit fetchAll:error];
    [sortDescriptor release];
    
    return error?nil:_visits;
}

+(void)fetchAllVisitsOnCompletion:(void(^)(NSArray *result,NSError *error))completionBlock
{
    [Visit fetchAsynWithPredicate:nil onAttributes:nil sortDescriptors:nil limit:0 startIndex:0 completion:completionBlock];
}

+(NSMutableArray *)allVisitsForCustomer:(Customer *)aCustomer
{
    NSMutableArray *res = nil;
    NSSet *_visits = aCustomer.visits;
    
    if(_visits)
    {
        res=[NSMutableArray arrayWithArray:_visits.allObjects];
        return res;
    }
    else 
    {
        
      NSError *error = nil;
      NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"self.parentCustomer!=nil AND self.parentCustomer.objectID=%@",aCustomer.objectID];
      res = [Visit fetchWithPredicate:fetchPredicate error:error];
        
       return error?nil:res;
    }
    
    return res;
}

+(void)fetchVisitsForCustomer:(Customer *)aCustomer OnCompletion:(void(^)(NSArray *result,NSError *error))completionBlock
{
    NSArray *res = nil;
    NSSet *_visits = aCustomer.visits;
    
    if(_visits)
    {
        res=_visits.allObjects;
        if(completionBlock) completionBlock(res,nil);
    }
    else 
    {
       NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"self.parentCustomer!=nil AND self.parentCustomer.objectID=%@",aCustomer.objectID];
        
        [Visit fetchAsynWithPredicate:fetchPredicate onAttributes:nil sortDescriptors:nil limit:0 startIndex:0 completion:completionBlock];
    }
}


+(NSMutableArray *)unreviewedVisitsFromVisits:(NSMutableArray *)visits
{
    if(!visits || ![visits count]) return nil;
    
    NSPredicate *filterPredicate=[NSPredicate predicateWithFormat:@"self.isReportCompleted=%@",[NSNumber numberWithBool:NO]];
    NSArray *arr=[visits filteredArrayUsingPredicate:filterPredicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"startDate" ascending:NO];
    
    NSArray *sortedArr = [arr count] ? [arr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]] : arr;
    
    [sortDescriptor release];

    
    return [NSMutableArray arrayWithArray:sortedArr];
}

+(NSMutableArray *)reviewedVisitsFromVisits:(NSMutableArray *)visits
{
    if(!visits || ![visits count]) return nil;
    
    NSPredicate *filterPredicate=[NSPredicate predicateWithFormat:@"self.isReportCompleted=%@",[NSNumber numberWithBool:YES]];
    NSArray *arr=[visits filteredArrayUsingPredicate:filterPredicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"startDate" ascending:NO];
    
    NSArray *sortedArr = [arr count] ? [arr sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]] : arr;
    
    [sortDescriptor release];
    
    return [NSMutableArray arrayWithArray:sortedArr];
}

+(NSMutableArray *)unreviewedVisitsForCustomer:(Customer *)aCustomer
{
    NSSet *visits = aCustomer.visits;
    if(!visits || ![visits count]) return nil;
    
    NSPredicate *filterPredicate=[NSPredicate predicateWithFormat:@"self.isReportCompleted=%@",[NSNumber numberWithBool:NO]];
    NSSet *set=[visits filteredSetUsingPredicate:filterPredicate];
    
    return [NSMutableArray arrayWithArray:set.allObjects];
}

+(NSMutableArray *)reviewedVisitsForCustomer:(Customer *)aCustomer
{
    NSSet *visits = aCustomer.visits;
    
    if(!visits || ![visits count]) return nil;

    NSPredicate *filterPredicate=[NSPredicate predicateWithFormat:@"self.isReportCompleted=%@",[NSNumber numberWithBool:YES]];
    NSSet *set=[visits filteredSetUsingPredicate:filterPredicate];
    
    return [NSMutableArray arrayWithArray:set.allObjects];
}

+(void)removeOlderReviewedVisits
{
    // ======= EXPIRED DATE IS 15 DAYS FROM THE DATE VISIT COMPLETED ======== //
    
    NSTimeInterval interval = [NSDate timeIntervalSinceReferenceDate];
    interval -= 60.0*60.0*24*15; //15 days time interval.
    
     NSPredicate *filterPredicate=[NSPredicate predicateWithFormat:@"self.isReportCompleted=%@ AND self.endDateIntervalSinceRefDate>=%@",[NSNumber numberWithBool:YES],[NSNumber numberWithDouble:interval]];
    
   NSFetchRequest *fetchRequest  = [NSFetchRequest fetchRequestWithEntity:[Visit entity] predicate:filterPredicate sortDescriptors:nil attributes:[NSArray arrayWithObjects:@"endDateIntervalSinceRefDate",@"isReportCompleted",nil] limit:0];
   
  /*
    NSError *error = nil;
   NSMutableArray *result =  [Visit fetchWithPredicate:filterPredicate onAttributes:[NSArray arrayWithObjects:@"endDateIntervalSinceRefDate",@"isReportCompleted",nil] sortDescriptors:nil limit:0 error:error];
    
    BOOL isDeleted = NO;
    for(Visit *_visit in result)
    {
        [SSCoreDataManager deleteObject:_visit];
        if(!isDeleted)isDeleted = YES;
    }
    
    if(isDeleted)
    [[SSCoreDataManager sharedManager] save:error];
    
    return error!=nil;
   */
    
    [SSCoreDataManager performAsyncFetchRequest:fetchRequest completion:^(NSFetchRequest *fetchRequest, NSArray *result, NSError *error) 
    {
        if(!error)
        {
            BOOL isDeleted = NO;
            for(Visit *_visit in result)
            {
                [CoreDataHandler deleteVisit:_visit];
                //[SSCoreDataManager deleteObject:_visit];
                if(!isDeleted)isDeleted = YES;
            }
            
            if(isDeleted)
            {
                NSError *_saveError = nil;
                [[SSCoreDataManager sharedManager] save:_saveError];
            }
        }
        
    }];
}

+(Rep *)populateRepFromDict:(NSDictionary *)dict
{
   // Rep *repObj =(Rep *)[Rep createObjectFromDictionary:dict inContext:DEFAULT_CONTEXT checkDuplicates:[NSArray arrayWithObject:@"repID"] predicateFormat:@"self.repID=%@"];
    
   NSMutableArray *allReps =  [Rep fetchAll:nil];
    for(Rep *aRep in allReps)
    {
        [SSCoreDataManager  deleteObject:aRep];
    }
    
    Rep *repObj =(Rep *)[Rep createObjectFromDictionary:dict inContext:DEFAULT_CONTEXT];

    return repObj;
}

+(void)deleteNewsOlderThanMonth:(NSInteger)month inContext:(NSManagedObjectContext *)context
{
     //===================== DELETE NEWS OLDER THAN 'month' MONTHS ==============================
     NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
     NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
     [offsetComponents setMonth:-month];  // 'month' months back from today
     NSDate *_checkingDate = [gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0];
    
     //NSDate *_checkingDate   = [[NSDate date] dateByAddingTimeInterval:-(60*60*24*30*6)];
     double _checkingWCFDate = [_checkingDate GMTTimeIntervalSince1970] *1000.0 ;
     
     NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"createdDate < %lf",_checkingWCFDate];
     
      NSMutableArray *olderNews = [News fetchOnContext:context predicate:fetchPredicate error:nil];
    

     if([olderNews count])
     {
       for(News *news in olderNews)
       {
         [SSCoreDataManager deleteObject:news inContext:context];
       }
     
        [[SSCoreDataManager sharedManager] save:nil];
     }
     
     [gregorian release];
     [offsetComponents release];
     //=========================================================================================

}

+(void)asyncFetchNewsOnCompletion:(CoreDataCompletion)completionBlock
{
    
    //[self deleteNewsOlderThanMonth:6 inContext:DEFAULT_CONTEXT];
    
    
    NSSortDescriptor *sortDescriptor1 = [[[NSSortDescriptor alloc] initWithKey:@"createdDate" ascending:NO] autorelease];
    NSSortDescriptor *sortDescriptor2 = [[[NSSortDescriptor alloc] initWithKey:@"newsTitle" ascending:YES] autorelease];
    
    NSFetchRequest *fetchRequest  = [NSFetchRequest fetchRequestWithEntity:[News entity] predicate:nil sortDescriptors:[NSArray arrayWithObjects:sortDescriptor1,sortDescriptor2, nil] attributes:nil limit:50];
    
    [SSCoreDataManager performAsyncFetchRequest:fetchRequest completion:^(NSFetchRequest *fetchRequest, NSArray *result, NSError *error) 
     {
         if(completionBlock) completionBlock(result,error);
     }];
}


+(void)deleteOffersOlderThanMonth:(NSInteger)month inContext:(NSManagedObjectContext *)context
{
    //===================== DELETE NEWS OLDER THAN 'month' MONTHS ==============================
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setMonth:-month];  // 'month' months back from today
    NSDate *_checkingDate = [gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0];
    
    //NSDate *_checkingDate   = [[NSDate date] dateByAddingTimeInterval:-(60*60*24*30*6)];
    double _checkingWCFDate = [_checkingDate GMTTimeIntervalSince1970] *1000.0 ;
    
     NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"self.activeTo.doubleValue<%lf",_checkingWCFDate];
    
    NSMutableArray *olderOffers = [Offer fetchOnContext:context predicate:fetchPredicate error:nil];
    
    
    if([olderOffers count])
    {
        for(Offer *offer in olderOffers)
        {
            [SSCoreDataManager deleteObject:offer inContext:context];
        }
        
        [[SSCoreDataManager sharedManager] save:nil];
    }
    
    [gregorian release];
    [offsetComponents release];
    //=========================================================================================
    
}


+(void)populateOffersPerformingDelete:(NSArray *)deleteIds update:(NSArray *)updateIds insertOnArray:(NSArray *)offers completion:(void(^)(NSMutableArray *deletedResult,NSMutableArray *insertedResult,NSMutableArray *updatedResult, NSError *error))completionBlock
{
    
/*
#if SHOW_ACTIVE_OFFERS_ONLY
    NSArray *_offfers=nil;
    
    
    //=========== ONLY FILTER CURRENT AND FUTURE OFFERS =========
    if([offers count])
    {
        double _currentWCFDate=[[NSDate date] GMTTimeIntervalSince1970] *1000.0 ;
        //NSPredicate *fetchPredicte = [NSPredicate predicateWithFormat:@"self.activeFrom.doubleValue<=%lf AND self.activeTo.doubleValue>=%lf",_currentWCFDate,_currentWCFDate];
         NSPredicate *fetchPredicte = [NSPredicate predicateWithFormat:@"self.activeTo.doubleValue>=%lf",_currentWCFDate];
        _offfers=[offers filteredArrayUsingPredicate:fetchPredicte];
    }
    
#else
     NSArray *_offfers=offers;
#endif
*/
    
   
    
     NSArray *_offfers=offers;
    
    [Offer populateAsyncPerformingDelete:deleteIds update:updateIds insertOnArray:_offfers onAttribute:@"offerID" completion:completionBlock];
    

    
/*
     NSMutableArray *_alloffers = [NSMutableArray arrayWithArray:offers];
    
    //GET OFFERS TO UPDATE
    NSArray *_offersToUpdate = [_alloffers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.offerID IN %@",updateIds]];
    
    //GET THE OFFERS TO INSERT
    [_alloffers removeObjectsInArray:_offersToUpdate];
    
    NSMutableArray *_offersToInsert = _alloffers;
    
    //Perform Delete operations
    //========================
    
  __block  NSArray *deletedObjects  = nil;
  __block  NSMutableArray *updatedObjects  = [NSMutableArray array];
  __block  NSArray *insertedObjects = nil;
    
    
    [SSCoreDataManager asyncDeleteObjectsforIDs:deleteIds onAttribute:@"offerID" 
     completion:^(NSMutableArray *result, NSError *error) 
    {
        deletedObjects = result;    
        
         ///PERFORM INSERT OPERATIONS
         
         [SSCoreDataManager asyncInsertObjectsFromArray:_offersToInsert completion:^(NSMutableArray *result, NSError *error) 
          {
              insertedObjects = result;
              
              ///PERFORM UPDATE OPERATIONS
              NSPredicate *updatePredicte = [NSPredicate predicateWithFormat:@"%K IN %@",@"offerID" ,updateIds];
              NSMutableArray *offersToCheckUpdate =  [Offer fetchWithPredicate:updatePredicte error:nil];

               BOOL _isAnyDataUpdated = NO;
              for(NSDictionary *offerDict in _offersToUpdate)
              {
              
                  NSInteger index=[offersToCheckUpdate indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                                       if([[obj valueForKey:@"offerID"] isEqual:[offerDict valueForKey:@"offerID"]])
                                          {
                                            return YES;
                                            *stop=YES;
                                          }
                                       
                                       return NO;
                                       
                 }];
                  
                  if(index!=NSNotFound)
                  {
                       NSManagedObject *fetchedObject=[offersToCheckUpdate objectAtIndex:index];
                      
                      for(NSString *key in offerDict)
                      {
                          SEL _sel=NSSelectorFromString(key);
                          
                          if(![fetchedObject respondsToSelector:_sel]) continue;
                          
                          id newVal=[offerDict valueForKey:key];
                          id oldVal=[fetchedObject valueForKey:key];
                          
                          if(![oldVal isEqual:newVal])
                          {
                              if(!_isAnyDataUpdated)_isAnyDataUpdated=YES;
                              [fetchedObject setValue:newVal forKey:key];
                          }
                      }
                      
                      [updatedObjects addObject:fetchedObject];

                  }

              }
              
          }];
         //==========================

     }];
     */
}




+(void)asyncFetchOffersOnCompletion:(CoreDataCompletion)completionBlock
{
    
    // DELETE OLDER OFFERS
    // [self deleteOffersOlderThanMonth:6 inContext:DEFAULT_CONTEXT];
    
    NSSortDescriptor *sortDescriptor1 = [[[NSSortDescriptor alloc] initWithKey:@"activeFrom" ascending:NO] autorelease];
    NSSortDescriptor *sortDescriptor2 = [[[NSSortDescriptor alloc] initWithKey:@"offerName" ascending:NO] autorelease];
    
    NSPredicate *fetchPredicte =nil;
    
    
    #if SHOW_ACTIVE_OFFERS_ONLY
    NSDate *_today = [NSDate today];
    double _currentWCFDate = [_today GMTTimeIntervalSince1970] *1000.0 ;
   // fetchPredicte = [NSPredicate predicateWithFormat:@"self.activeFrom.doubleValue<=%lf AND self.activeTo.doubleValue>=%lf",_currentWCFDate,_currentWCFDate];
      fetchPredicte = [NSPredicate predicateWithFormat:@"self.activeTo.doubleValue>=%lf",_currentWCFDate];
    #endif

    
    NSFetchRequest *fetchRequest  = [NSFetchRequest fetchRequestWithEntity:[Offer entity] predicate:fetchPredicte sortDescriptors:[NSArray arrayWithObjects:sortDescriptor1,sortDescriptor2, nil] attributes:nil limit:0];
    
    [SSCoreDataManager performAsyncFetchRequest:fetchRequest completion:^(NSFetchRequest *fetchRequest, NSArray *result, NSError *error) 
     {
         if(completionBlock) completionBlock(result,error);
     }];
}

// CatID=> 0= All    1= Mechanical    2=Collision 

+(void)fetchMechanicalOffers:(CoreDataCompletion)completionBlock
{
   NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"self.categoryID=%@ OR self.categoryID=%@",[NSNumber numberWithInt:1],[NSNumber numberWithInt:0]];
    
   NSFetchRequest *fetchRequest  = [NSFetchRequest fetchRequestWithEntity:[Offer entity] predicate:fetchPredicate sortDescriptors:nil attributes:nil limit:0];

   [SSCoreDataManager performAsyncFetchRequest:fetchRequest completion:^(NSFetchRequest *fetchRequest, NSArray *result, NSError *error) 
 {
     if(completionBlock) completionBlock(result,error);
     
 }];
}

+(void)fetchCollisionOffers:(CoreDataCompletion)completionBlock
{
    NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"self.categoryID=%@ OR self.categoryID=%@",[NSNumber numberWithInt:2],[NSNumber numberWithInt:0]];
    
    NSFetchRequest *fetchRequest  = [NSFetchRequest fetchRequestWithEntity:[Offer entity] predicate:fetchPredicate sortDescriptors:nil attributes:nil limit:0];
    
    [SSCoreDataManager performAsyncFetchRequest:fetchRequest completion:^(NSFetchRequest *fetchRequest, NSArray *result, NSError *error) 
     {
         if(completionBlock) completionBlock(result,error);
         
     }];
}

+(NSMutableArray *)collisionOffersFromOffers:(NSArray *)offers
{
    NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"categoryID=%@ OR categoryID=%@",[NSNumber numberWithInt:2],[NSNumber numberWithInt:0]];

   NSArray *filteredArr= [offers filteredArrayUsingPredicate:fetchPredicate];
    
    return [NSMutableArray arrayWithArray:filteredArr];
}

+(NSMutableArray *)mechanicalOffersFromOffers:(NSArray *)offers
{
    NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"categoryID=%@ OR categoryID=%@",[NSNumber numberWithInt:1],[NSNumber numberWithInt:0]];
    
    NSArray *filteredArr = [offers filteredArrayUsingPredicate:fetchPredicate];
    
    return filteredArr ? [NSMutableArray arrayWithArray:filteredArr] : [NSMutableArray array];
}

+(void)populateCustomersFromArray:(NSArray *)customersArr completion:(CoreDataCompletion)completionBlock
{
    NSMutableArray *_customers = [Customer checkOrCreateObjectsFromArray:customersArr inContext:DEFAULT_CONTEXT checkDuplicateOn:[NSArray arrayWithObjects:@"customerID", nil]];
    
    if(_customers.count != customersArr.count)
    {
        NSSortDescriptor *sortDescriptor1 = [[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES] autorelease];

        
        NSFetchRequest *fetchRequest  = [NSFetchRequest fetchRequestWithEntity:[Customer entity] predicate:nil sortDescriptors:[NSArray arrayWithObjects:sortDescriptor1, nil] attributes:nil limit:50];
        
        [SSCoreDataManager performAsyncFetchRequest:fetchRequest completion:^(NSFetchRequest *fetchRequest, NSArray *result, NSError *error) 
         {
             if(completionBlock) completionBlock(result,error);
             
         }];
    }
    else 
    {
        if(completionBlock) completionBlock(_customers,nil);
    }
}

#pragma mark SCHEDULED VISIT

+(ScheduledVisit *)newScheduledVisit
{
    ScheduledVisit *_visit = (ScheduledVisit *)[ScheduledVisit insertNewObject];
    _visit.scheduleVisitID = [NSNumber numberWithLong:[[NSDate date] hash]];
    _visit.oldScheduleVisitID = _visit.scheduleVisitID;
    return  [_visit retain];
}

+(void)newScheduledVisitWithCustomerName:(NSString *)aCustomerName suburbName:(NSString *)suburbName startDate:(NSDate *)aStartDate endDate:(NSDate *)aEndDate completion:(void(^)(ScheduledVisit *event))callback
{
    if(!aCustomerName.length || (!aStartDate || !aEndDate))
    {
        callback(nil);
        return;
    }
    
    //ADDED BY SANDIP TO SEARCH ANY EVENT WITH SAME START DATE AND END DATE
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.visitStartDate>=%@ && self.visitEndDate<=%@",aStartDate,aEndDate];
    
    NSMutableArray *fetchedVisits = [ScheduledVisit fetchWithPredicate:predicate onAttributes:[NSArray arrayWithObject:@"scheduleVisitID"] sortDescriptors:nil limit:1 error:nil];
    
    BOOL matched=[fetchedVisits count]>0;
    
    if(matched) 
    {
        
        UIAlertView *alert = [UIAlertView alertViewWithTitle:@"" message:SCHEDULE_VISIT_MSG cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObject:@"Yes"] onDismiss:^(int buttonIndex) 
                              
                              {
                                  ScheduledVisit *event  = [CoreDataHandler newScheduledVisit];
                                  event.visitStartDate   =  aStartDate;
                                  event.visitEndDate     =  aEndDate;
                                  event.customerName     =  aCustomerName;
                                  event.stateName = suburbName;
                                  
                                  NSError *err=nil;
                                  
                                  if(callback) callback((!err)?event:nil);
                                  
                                  [event release];
                                  
                              } 
                              
                                onCancel:^{
                                                        
                                if(callback) callback(nil);
                                                        
                                }];
        
        [alert show];
    }
    else
    {
        ScheduledVisit *event  = [CoreDataHandler newScheduledVisit];
        event.visitStartDate = aStartDate;
        event.visitEndDate   = aEndDate;
        event.customerName = aCustomerName;
        event.stateName = suburbName;
        
        NSError *err=nil;
        if(callback) callback((!err)?event:nil);
        [event release];
    }
    
}

+(void)newScheduledVisitWithCustomer:(Customer *)customer startDate:(NSDate *)aStartDate endDate:(NSDate *)aEndDate completion:(void(^)(ScheduledVisit *event))callback
{
    [self newScheduledVisitWithCustomer:customer startDate:aStartDate endDate:aEndDate checkDuplicate:YES completion:callback];
}

+(void)newScheduledVisitWithCustomer:(Customer *)customer startDate:(NSDate *)aStartDate endDate:(NSDate *)aEndDate checkDuplicate:(BOOL)checkDuplicate completion:(void(^)(ScheduledVisit *event))callback
{
    if(!customer.name.length || (!aStartDate || !aEndDate))
    {
        callback(nil);
        return;
    }
    
    if(checkDuplicate)
    {
    //ADDED BY SANDIP TO SEARCH ANY EVENT WITH SAME START DATE AND END DATE
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.visitStartDate>=%@ && self.visitEndDate<=%@",aStartDate,aEndDate];
    
    NSMutableArray *fetchedVisits = [ScheduledVisit fetchWithPredicate:predicate onAttributes:[NSArray arrayWithObject:@"scheduleVisitID"] sortDescriptors:nil limit:1 error:nil];
    
    BOOL matched=[fetchedVisits count]>0;
    
    if(matched)
    {
        
        UIAlertView *alert = [UIAlertView alertViewWithTitle:@"" message:SCHEDULE_VISIT_MSG cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObject:@"Yes"] onDismiss:^(int buttonIndex)
                              
                              {
                                  ScheduledVisit *event  = [CoreDataHandler newScheduledVisit];
                                  event.visitStartDate   =  aStartDate;
                                  event.visitEndDate     =  aEndDate;
                                  event.customerName     =  customer.name;
                                  event.stateName        =  customer.suburb;
                                  event.customerID       =  customer.customerID;
                                  // event.customer      =  customer;
                                  if(customer)
                                  {
                                      [customer addScheduledVisitsObject:event];
                                  }
                                  
                                  //================ CALCULATE AND SET NEXT VISIT DATE FOR THE CUSTOMER HERE =========
                                  customer.nextVisit = [aStartDate stringFromDateWithFormat:CUSTOMER_LAST_VISIT_FORMAT];
                                  
                                  NSError *err=nil;
                                  
                                  if(callback) callback((!err)?event:nil);
                                  
                                  [event release];
                                  
                              }
                              
                                onCancel:^{
                                                        
                                    if(callback) callback(nil);
                                                        
                                }];
        
        [alert show];
    }
    else
    {
        ScheduledVisit *event  = [CoreDataHandler newScheduledVisit];
        event.visitStartDate     = aStartDate;
        event.visitEndDate      = aEndDate;
        event.customerName     = customer.name;
        event.stateName        = customer.suburb;
        event.customerID       = customer.customerID;
        // event.customer       = customer;
        if(customer)
        {
            [customer addScheduledVisitsObject:event];
        }
        
        //======== CALCULATE AND SET NEXT VISIT DATE FOR THE CUSTOMER HERE ================
        customer.nextVisit = [aStartDate stringFromDateWithFormat:CUSTOMER_LAST_VISIT_FORMAT];
        
        NSError *err=nil;
        if(callback) callback((!err)?event:nil);
        [event release];
    }
    }
    else
    {
        ScheduledVisit *event  = [CoreDataHandler newScheduledVisit];
        event.visitStartDate     = aStartDate;
        event.visitEndDate      = aEndDate;
        event.customerName     = customer.name;
        event.stateName        = customer.suburb;
        event.customerID       = customer.customerID;
        // event.customer       = customer;
        if(customer)
        {
            [customer addScheduledVisitsObject:event];
        }
        
        //======== CALCULATE AND SET NEXT VISIT DATE FOR THE CUSTOMER HERE ================
        customer.nextVisit = [aStartDate stringFromDateWithFormat:CUSTOMER_LAST_VISIT_FORMAT];
        
        NSError *err=nil;
        if(callback) callback((!err)?event:nil);
        [event release];
    }
}

+(NSMutableArray *)allScheduledVisits
{
    NSError *error=nil;
    
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc] initWithKey:@"visitStartDate" ascending:NO];
    NSMutableArray *_visits= [ScheduledVisit fetchWithPredicate:nil sortDescriptors:[NSArray arrayWithObject:sortDescriptor] error:error];
    [sortDescriptor release];
    
    return error?nil:_visits;
}

+(NSMutableArray *)filteredScheduledVisitsByPredicate:(NSPredicate *)predicate
{
    NSError *error=nil;
    
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc] initWithKey:@"visitStartDate" ascending:NO];
    NSMutableArray *_visits= [ScheduledVisit fetchWithPredicate:predicate sortDescriptors:[NSArray arrayWithObject:sortDescriptor] error:error];
    [sortDescriptor release];
    
    return error?nil:_visits;
}

+(OfflineStore *)offlineStore
{
    NSMutableArray *stores =  [OfflineStore fetchAll:nil];
    OfflineStore   *_store = [stores count] ? [stores objectAtIndex:0] : nil;
    
    if(!_store) _store = (OfflineStore   *) [OfflineStore insertNewObject];
    
    return _store;
}

+(OfflineStore *)offlineStoreForContext:(NSManagedObjectContext*)context
{
    NSMutableArray *stores =  [OfflineStore fetchOnContext:context predicate:nil sortDescriptors:nil error:nil];
    
    OfflineStore   *_store = [stores count] ? [stores objectAtIndex:0] : nil;
    
    if(!_store) _store = (OfflineStore   *) [OfflineStore insertNewObjectInContext:context];
    
    return _store;
}


+(Reminder *)newReminder
{
   Reminder *_insertedReminder =(Reminder *)[Reminder insertNewObject];
    
    _insertedReminder.reminderID = [NSNumber numberWithUnsignedInteger:[[NSDate date] hash]];
    
    return _insertedReminder;
}

+(Reminder *)reminderWithEventIdentifier:(NSString *)indentifier
{
   NSMutableArray *_fetched= [Reminder fetchWithPredicate:[NSPredicate predicateWithFormat:@"self.eventIdentifier=%@",indentifier] error:nil];
    
    return [_fetched count] ? [_fetched objectAtIndex:0] : nil;
}

#pragma mark -

+(void)setCustomersFetchedFromDashboard:(BOOL)isFetched
{
    OfflineStore *_store = [self offlineStore];
    
    if( [_store.isCustomerLoaded boolValue]!=isFetched)
    {
        _store.isCustomerLoaded = [NSNumber numberWithBool:isFetched];
        
        //[[SSCoreDataManager sharedManager] save:nil];
    }
}

+(BOOL)isCustomersFetchedFromDashboard
{
    OfflineStore *_store = [self offlineStore];
    return   [_store.isCustomerLoaded boolValue];
}

@end
