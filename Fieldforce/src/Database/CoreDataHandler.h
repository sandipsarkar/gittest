//
//  CoreDataHandler.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 10/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//
/*
 +(id)willSetValue:(id)value forKey:(id)key
 {
 if([key rangeOfString:@"Date"].location!=NSNotFound)
 {
 NSDate *date  = [NSDate dateFromWCFTimeInterval:[value doubleValue]];
 return date;
 }
 
 return value;
 }
*/

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SSCoreDataManager.h"
#import "NSManagedObject+Primarykey.h"


typedef enum
{
    FFUserTypeADMIN = 1,
    FFUserTypeOEM,
    FFUserTypeGM ,
    FFUserTypeGPM,
    FFUserTypePM ,
    FFUserTypeREP,
    
}FFUserType;


typedef enum
{
    CategoryTypeDefault    = -1,
    CategoryTypeAll        = 0,   /* Mechanical & Collision */
    CategoryTypeMechanical,
    CategoryTypeCollision,
    CategoryTypeOther,
}
CategoryType;


@class Customer,Task,Visit,Rep,Offer,ScheduledVisit,Territory,OfflineStore,Reminder;

typedef void(^CoreDataCompletion)(NSArray *result,NSError *error);

@interface CoreDataHandler : NSObject

@property(nonatomic,copy) NSString *storePath;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel   *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

-(id)init;
-(id)initWithPersistentStorePath:(NSString *)aStorePath;

-(void)addPersistentStoreForStoreAtURL:(NSURL *)storePathURL;

-(BOOL)isModelCompatibleWithPersistentStoreAtURL:(NSURL *)storeURL;

+(NSNumber *)generateID;
+(Rep *)currentRep;
+(Rep *)currentRepInContext:(NSManagedObjectContext *)context;

+(NSArray *)connectedOEMS;

+(NSArray *)intersectWithDealingOEMs:(NSArray *)dealingOEMS;

+(BOOL)checkDealersiteDealsCustomer:(Customer *)customer withAlertTitle:(NSString *)title message:(NSString *)message;

+(Territory *)territoryForTerritoryPosition:(NSNumber *)position;
+(Territory *)territoryForTerritoryID:(NSNumber *)territoryID;

+(void)deleteVisit:(Visit *)aVisit;

+(NSString *)oemNameForOEMID:(NSInteger )_oemId;
+(NSInteger)oemIDForOEMName:(NSString *)oemName;

+(Customer *)newCustomer;
+(Task *)newTask;
+(Visit *)newVisit;
+(ScheduledVisit *)newScheduledVisit;


+(NSMutableArray *)loadCustomersWithProperties:(NSArray *)properties;


//==================================== TASKS =================================================
+(NSMutableArray *)allTasks;
+(void)fetchAllTasksOnCompletion:(CoreDataCompletion)completionBlock;
+(NSMutableArray *)allTasksForCustomer:(Customer *)aCustomer;
+(NSMutableArray *)filteredTasksForPredicate:(NSPredicate *)predicate;

+(void)fetchAllTasksForCustomer:(Customer *)aCustomer OnCompletion:(CoreDataCompletion)completionBlock;
+(void)fetchUpcommingTasksOnCompletion:(CoreDataCompletion)completionBlock;


///================================== VISITS =================================================

+(NSMutableArray *)allVisits;
+(NSMutableArray *)filteredScheduledVisitsByPredicate:(NSPredicate *)predicate;
+(NSMutableArray *)allVisitsForCustomer:(Customer *)aCustomer;
+(void)fetchAllVisitsOnCompletion:(CoreDataCompletion)completionBlock;
+(void)fetchVisitsForCustomer:(Customer *)aCustomer OnCompletion:(CoreDataCompletion)completionBlock;

+(NSMutableArray *)unreviewedVisitsFromVisits:(NSMutableArray *)visits;
+(NSMutableArray *)reviewedVisitsFromVisits:(NSMutableArray *)visits;

+(NSMutableArray *)unreviewedVisitsForCustomer:(Customer *)aCustomer;
+(NSMutableArray *)reviewedVisitsForCustomer:(Customer *)aCustomer;

// REMOVE COMPLETED VISITS WHICH ARE OLDER THAN 15 DAYS
+(void)removeOlderReviewedVisits;



///=================================== REP =======================================================
+(Rep *)populateRepFromDict:(NSDictionary *)dict;
//+(NSMutableArray *)populateNewsFromArray:(NSArray *)newsArr;



//===================================== NEWS ======================================================
+(void)populateNewsFromArray:(NSArray *)newsArr completion:(CoreDataCompletion)completionBlock;
+(void)asyncFetchNewsOnCompletion:(CoreDataCompletion)completionBlock;
+(void)deleteNewsOlderThanMonth:(NSInteger)month inContext:(NSManagedObjectContext *)context;

+(void)populateNewsPerformingDelete:(NSArray *)deleteIds update:(NSArray *)updateIds insertOnArray:(NSArray *)offers completion:(void(^)(NSMutableArray *deletedResult,NSMutableArray *insertedResult,NSMutableArray *updatedResult, NSError *error))completionBlock;

//===================================== OFFERS ====================================================
+(void)asyncFetchOffersOnCompletion:(CoreDataCompletion)completionBlock;

+(void)fetchMechanicalOffers:(CoreDataCompletion)completionBlock;
+(void)fetchCollisionOffers:(CoreDataCompletion)completionBlock;

+(NSMutableArray *)collisionOffersFromOffers:(NSArray *)offers;
+(NSMutableArray *)mechanicalOffersFromOffers:(NSArray *)offers;

+(void)deleteOffersOlderThanMonth:(NSInteger)month inContext:(NSManagedObjectContext *)context;
+(void)populateOffersPerformingDelete:(NSArray *)deleteIds update:(NSArray *)updateIds insertOnArray:(NSArray *)offers completion:(void(^)(NSMutableArray *deletedResult,NSMutableArray *insertedResult,NSMutableArray *updatedResult, NSError *error))completionBlock;



//==================================== CUSTOMERS ======================================================
+(void)populateCustomersFromArray:(NSArray *)customersArr completion:(CoreDataCompletion)completionBlock;
+(NSMutableArray *)allCustomers;
+(void)fetchAllCustomersOnCompletion:(CoreDataCompletion)completionCallback;


//======================================= SCHEDULE VISIT ====================================

+(NSMutableArray *)allScheduledVisits;
+(void)newScheduledVisitWithCustomerName:(NSString *)aCustomerName suburbName:(NSString *)suburbName startDate:(NSDate *)aStartDate endDate:(NSDate *)aEndDate completion:(void(^)(ScheduledVisit *event))callback;
+(void)newScheduledVisitWithCustomer:(Customer *)customer startDate:(NSDate *)aStartDate endDate:(NSDate *)aEndDate completion:(void(^)(ScheduledVisit *event))callback;

+(void)newScheduledVisitWithCustomer:(Customer *)customer startDate:(NSDate *)aStartDate endDate:(NSDate *)aEndDate checkDuplicate:(BOOL)checkDuplicate completion:(void(^)(ScheduledVisit *event))callback;
//============================================================================================

+(OfflineStore *)offlineStore;
+(OfflineStore *)offlineStoreForContext:(NSManagedObjectContext*)context;

+(void)setOfflineArray:(id)array forOperationKey:(NSString *)key;
+(void)setOfflineArray:(id )array forOperationKey:(NSString *)key inContext:(NSManagedObjectContext *)context;
+(id)offlineArrayForOperationKey:(NSString *)key;

+(Reminder *)newReminder;
+(Reminder *)reminderWithEventIdentifier:(NSString *)indentifier;

+(void)setCustomersFetchedFromDashboard:(BOOL)isFetched;
+(BOOL)isCustomersFetchedFromDashboard;

@end
