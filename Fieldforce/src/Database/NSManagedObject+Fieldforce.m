//
//  NSManagedObject+Fieldforce.m
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "NSManagedObject+Fieldforce.h"
#import "CustomEventStore.h"

@implementation NSManagedObject (Fieldforce)

- (BOOL)isReallyDeleted {
    
    return [self isDeleted] || [self managedObjectContext] == nil;
}

@end


@implementation Customer (Fieldforce)

@dynamic lastVisitDate;
@dynamic nextVisitDate;

-(NSString *)fullAddress
{
    NSMutableArray *arr=[NSMutableArray array];
    
    NSString *add1   = self.address;
    NSString *add2   = self.address2;
    NSString *suburb = self.suburb;
    //NSString *city   = self.city;
    NSString *state  = self.state;
    NSString *postCode  = self.postCode;
    
    if(add1.length) [arr addObject:add1];
    if(add2.length) [arr addObject:add2];
    //if(city.length) [arr addObject:city];
    
    if(suburb.length)  [arr addObject:suburb];
    if(state.length)   [arr addObject:state];
    if(postCode.length) [arr addObject:postCode];
    
    NSString *addressString=[arr componentsJoinedByString:@","];
    
    return addressString;
}

-(NSString *)getCategoryName
{
    NSString *customerCat=self.categoryName;
    //_categoryLabel.text=[customerCat stringByAppendingString:@" "];
    
    
    //if(!customerCat.length && self.categoryID)
    if(self.categoryID)
    {
        int catID = [self.categoryID intValue];
        
        switch (catID)
        {
            case 0:
            {
                customerCat = @"All";
             // customerCat = @"Collision & Mechanical";
            }
            break;
                
            case 1: customerCat = @"Mechanical";
                break;
                
            case 2: customerCat = @"Collision";
                break;
                
            case 3: customerCat = @"Other";
                break;
                
            default:
            {
                //customerCat = @"Collision & Mechanical";
                customerCat = @"All";
            }
            break;
        }
        
        
        if(![self.categoryName isEqualToString:customerCat])
            self.categoryName = customerCat;
    }
    
    return customerCat;
}

-(BOOL)isDealingWithAnyOEM
{
    NSArray *_dealingOEMS = [self.dealingOEMs componentsSeparatedByString:@","];
    
    NSIndexSet *indexes   = [_dealingOEMS indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop)
                             {
                                 NSString *oemID = (NSString *)obj;
                                 
                                 return [oemID intValue]>0;
                             }];
    
    _dealingOEMS = [_dealingOEMS objectsAtIndexes:indexes];
    
    return [_dealingOEMS count] > 0;
}


-(NSDate *)lastVisitDate
{
    if(!self.lastVisit.length) return nil;
    
    NSDate *_date = [NSDate dateFromString:self.lastVisit withFormat:OFFER_DATE_FORMAT];
    
    return _date;
}

-(NSDate *)nextVisitDate
{
    if(!self.nextVisit.length) return nil;
    
    NSDate *_date = [NSDate dateFromString:self.nextVisit withFormat:OFFER_DATE_FORMAT];
    
    return _date;
}


@end


@implementation CustomerPhoto (Fieldforce)

-(NSMutableDictionary *)uploadDict
{
    NSMutableDictionary *uploadDict = [NSMutableDictionary dictionary];
    
    NSString *imageFileName =self.imageName.length ?  self.imageName  : @"";
    [uploadDict setValue:imageFileName forKey:@"imageName"];
    [uploadDict setValue:self.customerImageID forKey:@"customerImageID"];
    
    NSTimeInterval _dateInterval = [self.createdDate doubleValue];
    NSString *createDateString   = [NSString stringWithFormat:@"%lf",_dateInterval];
    
    [uploadDict setValue:createDateString forKey:@"createdDate"];
    
    return uploadDict;
}

-(void)prepareForDeletion
{
    NSString *imagePath = self.imageName ? [DirectoryManager customerImagePathForName:self.imageName] : nil;
    
    if(imagePath && [[NSFileManager defaultManager] fileExistsAtPath:imagePath])
    {
        [[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
    }
}

@end

@implementation ScheduledVisit (Fieldforce)


 +(id)willSetValue:(id)value forKey:(id)key
 {
 value = NULL_NIL(value);
 
 if([key rangeOfString:@"Date"].location!=NSNotFound)
 {
 // NSDate *date  = value ? [NSDate dateFromWCFTimeInterval:[value doubleValue]] : nil;
 
 NSDate *date  = value ? [NSDate dateWithTimeIntervalSince1970:[value doubleValue]] : nil;
 
 if(date && [key isEqualToString:@"visitStartDate"])
 date=[date currentDateHourMinute];
 
 return date;
 }
 
 return value;
 }
 
 -(void)willDelete
 {
 ScheduledVisit *_visit = (ScheduledVisit *)self;
 
 @try {
 
 NSString *eventIdentifier = _visit.eventIdentifier;
 if(eventIdentifier.length)
 {
 
 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
 
 EKEvent *eventToRemove = [[CustomEventStore sharedInstance].eventStore eventWithIdentifier:eventIdentifier];
 if(eventToRemove)
 [[CustomEventStore sharedInstance].eventStore removeEvent:eventToRemove span:EKSpanThisEvent error:nil];
 
 dispatch_async(dispatch_get_main_queue(), ^{
 
 // [eventIdentifier release];
 
 });
 });
 }
 
 }
 
 @catch (NSException *exception)
 {
    NSLog(@"Task Exception=%@",exception.reason);
 }
 @finally
 {
 
 }
 
 }



@end

@implementation Visit (Fieldforce)

-(NSString *)startEndTimeString
{
     BOOL _completed =   [self.isVisitCompleted boolValue];
    
    NSDate *_startDate = self.startDate;
    NSDate *_endDate   = _completed ?  self.visitCompletedDate : self.scheduledVisit.visitEndDate;
    
    //TEST
   // _endDate = [_endDate dateByAddingTimeInterval:60*60*24];
    
    NSInteger _intervalDays = 0;
    if(_endDate)
    {
        _intervalDays = abs([NSDate daysBetweenDate:_endDate toDate:_startDate]);
    }
    
    //_intervalDays = [NSDate daysBetweenDate: _visit.visitCompletedDate ? _visit.visitCompletedDate : _visit.scheduledVisit.visitEndDate toDate:_visit.startDate];
    
    NSString *startTimeStr = [_startDate stringFromDateWithFormat:@"hh.mm a"];
    NSString *endTimeStr   = _intervalDays > 0 ? [_endDate stringFromDateWithFormat:@"dd/MM hh.mm a"] : [_endDate stringFromDateWithFormat:@"hh.mm a"];
    
    
  //  BOOL _completed =   [self.isReportCompleted boolValue];
    
  //  NSString *startEndStr = _completed ? [NSString stringWithFormat:@"  Start: %@   End: %@",startTimeStr?[startTimeStr lowercaseString]:@"",[endTimeStr lowercaseString]] : [NSString stringWithFormat:@"  Start: %@",startTimeStr?[startTimeStr lowercaseString]:@""];
    
     NSString *startEndStr =  [NSString stringWithFormat:@" Start: %@   End: %@",startTimeStr?[startTimeStr lowercaseString]:@"",[endTimeStr lowercaseString]] ;
    
    return startEndStr;
}


@end


@implementation ResourceFolder (Fieldforce)

-(BOOL)isEmpty
{
    return NO;
}

-(NSSet *)publishedResources
{
    if(![self.resources count]) return self.resources;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.isPublished=%@",[NSNumber numberWithInt:1]];
    return  [self.resources filteredSetUsingPredicate:predicate];
}

@end

@implementation ResourceMaster (Fieldforce)

+(NSString *)keyToCheckDuplicate
{
    return @"resourceID";
}

-(NSString *)resourceFileName
{
    NSString *_resourceFileName = self.originalFileName;
    NSString *fileExtention = self.fileExtension;
    
    if(fileExtention.length && ![_resourceFileName pathExtension].length)
    {
        _resourceFileName = [_resourceFileName stringByAppendingString:fileExtention];
    }

    return _resourceFileName;
}

- (void)prepareForDeletion
{
    NSString *_resourceFileName = [self resourceFileName];
    NSString *_resourceFilePath =  _resourceFileName ? [DirectoryManager resourceFilePathWithName:_resourceFileName] : nil;
    
    if(_resourceFilePath && [[NSFileManager defaultManager] fileExistsAtPath:_resourceFilePath isDirectory:NO])
    {
        [[NSFileManager defaultManager] removeItemAtPath:_resourceFilePath error:nil];
    }
}


-(NSString *)fileSizeString
{
    NSString *_fileSize = self.fileSize;
    
    unsigned  long long size = [_fileSize longLongValue];

 

    NSString *_fileSizeString = [NSByteCountFormatter stringFromByteCount:size countStyle:NSByteCountFormatterCountStyleFile];

    return _fileSizeString;

}


/*
-(NSString *)fileSizeString
{
    NSString *_fileSize = self.fileSize;
    
     double convertedValue = [_fileSize doubleValue];
     int multiplyFactor = 0;
     
     
     NSArray *tokens = @[@"bytes",@"KB",@"MB",@"GB",@"TB"];
     
     while (convertedValue >= 1000) {
     convertedValue /= 1000;
     multiplyFactor++;
     }
     
     NSNumberFormatter * formatter =  [[NSNumberFormatter alloc] init];
     [formatter setUsesSignificantDigits:NO];
     [formatter setMaximumSignificantDigits:3];
     [formatter setMaximumFractionDigits:2];
 
     NSString *num = [formatter stringFromNumber:[NSNumber numberWithDouble:convertedValue]];
    
    [formatter release];
    return [NSString stringWithFormat:@"%@ %@",num, tokens[multiplyFactor]];
}

*/


@end
