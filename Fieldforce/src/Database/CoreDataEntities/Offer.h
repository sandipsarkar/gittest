//
//  Offer.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 29/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Offer : NSManagedObject

@property (nonatomic, retain) NSNumber * activeFrom;
@property (nonatomic, retain) NSNumber * activeTo;
@property (nonatomic, retain) NSNumber * categoryID;
@property (nonatomic, retain) NSString * catName;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSNumber * isMarked;
@property (nonatomic, retain) NSString * offerDescription;
@property (nonatomic, retain) NSNumber * offerID;
@property (nonatomic, retain) NSString * offerImage;
@property (nonatomic, retain) NSString * externalLink;
@property (nonatomic, retain) NSString * offerName;
@property (nonatomic, retain) NSString * thumbImageLink;
@property (nonatomic, retain) NSString * offerTerms;
@property (nonatomic, retain) NSNumber * isPublished;
@property (nonatomic, retain) NSString * createdByUserName;
@property (nonatomic, retain) NSNumber * userType;
@property (nonatomic, retain) NSNumber * userID;
@property (nonatomic,retain)  NSNumber * appViewed;
@property (nonatomic,retain)  NSString * offerLinks;
@property (nonatomic,retain)  NSString * offerPdf;


@end
