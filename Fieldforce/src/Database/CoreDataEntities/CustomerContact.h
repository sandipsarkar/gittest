//
//  CustomerContact.h
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Customer;

@interface CustomerContact : NSManagedObject

@property (nonatomic, retain) NSNumber * contactID;
@property (nonatomic, retain) NSNumber * createdDate;
@property (nonatomic, retain) NSNumber * customerID;
@property (nonatomic, retain) NSString * direct;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * mobile;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * oldContactID;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) Customer *customer;

@property (nonatomic,assign) BOOL isEditModeOn;

@end
