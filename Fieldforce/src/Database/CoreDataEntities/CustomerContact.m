//
//  CustomerContact.m
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "CustomerContact.h"
#import "Customer.h"


@implementation CustomerContact

@dynamic contactID;
@dynamic createdDate;
@dynamic customerID;
@dynamic direct;
@dynamic email;
@dynamic mobile;
@dynamic name;
@dynamic oldContactID;
@dynamic title;
@dynamic customer;

@synthesize isEditModeOn;

@end
