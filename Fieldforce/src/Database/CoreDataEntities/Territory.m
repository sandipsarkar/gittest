//
//  Territory.m
//  RepVisitationTool
//
//  Created by Sandip Sarkar on 25/04/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "Territory.h"
#import "Rep.h"


@implementation Territory

@dynamic territoryID;
@dynamic territoryName;
@dynamic territoryPosition;
@dynamic territoryAbbreviation;
@dynamic parentRep;

@end
