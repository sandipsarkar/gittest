//
//  Task.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 18/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Customer;

@interface Task : NSManagedObject

@property (nonatomic, retain) NSDate * completedDate;
@property (nonatomic, retain) NSString * completedNotes;
@property (nonatomic, retain) NSDate * dueDate;
@property (nonatomic, retain) NSNumber * isCompleted;
@property (nonatomic, retain) NSNumber * isOffline;
@property (nonatomic, retain) NSNumber * isReminder;
@property (nonatomic, retain) NSNumber * isStarred;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSNumber * taskID;
@property(nonatomic,retain)NSNumber *customerID;
@property(nonatomic,retain)NSString *customerName;
@property (nonatomic ,retain) NSString *eventIdentifier;
@property (nonatomic, retain) Customer *parentCustomer;
@property (nonatomic,assign) BOOL wasCustomerAvailable;
@property (nonatomic,retain) NSNumber *oldTaskID;



@end
