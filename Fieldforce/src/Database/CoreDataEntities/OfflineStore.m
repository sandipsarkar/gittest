//
//  OfflineStore.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 11/01/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "OfflineStore.h"


@implementation OfflineStore

@dynamic offlineCustomers;
@dynamic offlineScheduledVisits;
@dynamic offlineNews;
@dynamic offlineOffers;
@dynamic offlineTasks;
@dynamic offlineReminders;
@dynamic feedbackCategories;
@dynamic logoImageName;



@dynamic lastRequestedCustomersDate;
@dynamic lastRequestedNewsDate;
@dynamic lastRequestedOffersDate;
@dynamic lastRequestedScheduleVisitDate;
@dynamic lastRequestedTaskDate;
@dynamic lastRequestedReminderDate;
@dynamic lastRequestedSuburbDate;
@dynamic lastRequestedResourcesDate;
@dynamic isCustomerLoaded;



-(void)setFeedbackCategory:(NSMutableArray *)list
{
    self.feedbackCategories =  [NSKeyedArchiver archivedDataWithRootObject:list?list:[NSMutableArray array]];
}

-(NSMutableArray *)feedbackCategory
{
    return self.feedbackCategories ? [NSKeyedUnarchiver unarchiveObjectWithData:self.feedbackCategories] : nil;
}



@end
