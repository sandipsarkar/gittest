//
//  Territory.h
//  RepVisitationTool
//
//  Created by Sandip Sarkar on 25/04/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Rep;

@interface Territory : NSManagedObject

@property (nonatomic, retain) NSNumber * territoryID;
@property (nonatomic, retain) NSString * territoryName;
@property (nonatomic, retain) NSNumber * territoryPosition;
@property (nonatomic, retain) NSString * territoryAbbreviation;
@property (nonatomic, retain) Rep *parentRep;

@end
