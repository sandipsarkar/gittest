//
//  ScheduledVisit.h
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Customer, Visit;

@interface ScheduledVisit : NSManagedObject

@property (nonatomic, retain) NSNumber * customerID;
@property (nonatomic, retain) NSString * customerName;
@property (nonatomic, retain) NSString * eventIdentifier;
@property (nonatomic, retain) NSNumber * isAutoGenerated;
@property (nonatomic, retain) NSNumber * isScheduleVisitSentToServer;
@property (nonatomic, retain) NSNumber * isVisitPublished;
@property (nonatomic, retain) NSNumber * oldScheduleVisitID;
@property (nonatomic, retain) NSNumber * repID;
@property (nonatomic, retain) NSNumber * reviewedVisitID;
@property (nonatomic, retain) NSNumber * scheduleVisitID;
@property (nonatomic, retain) NSString * stateName;
@property (nonatomic, retain) NSDate * visitEndDate;
@property (nonatomic, retain) NSDate * visitStartDate;
@property (nonatomic, retain) Customer *customer;
@property (nonatomic, retain) Visit *visit;

@end
