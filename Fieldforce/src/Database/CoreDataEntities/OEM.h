//
//  OEM.h
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class WholesaleDealer;

@interface OEM : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * oemID;
@property (nonatomic, retain) WholesaleDealer *wholesaleDealer;

@end
