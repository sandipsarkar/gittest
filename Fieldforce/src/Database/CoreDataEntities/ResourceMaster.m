//
//  ResourceMaster.m
//  Fieldforce
//
//  Created by Randem IT on 17/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "ResourceMaster.h"
#import "ResourceFolder.h"


@implementation ResourceMaster

@dynamic createdBy;
@dynamic createdByName;
@dynamic createdDate;
@dynamic fileExtension;
@dynamic fileSize;
@dynamic modifiedBy;
@dynamic modifiedDate;
@dynamic originalFileName;
@dynamic resourceFolderID;
@dynamic resourceID;
@dynamic resourceName;
@dynamic userTypeID;
@dynamic isPublished;
@dynamic folder;

@end
