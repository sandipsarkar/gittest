//
//  Customer.m
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "Customer.h"
#import "CustomerContact.h"
#import "CustomerPhoto.h"
#import "Rep.h"
#import "ScheduledVisit.h"
#import "Task.h"
#import "Visit.h"


@implementation Customer

@dynamic accountNo;
@dynamic address;
@dynamic address2;
@dynamic capricorn;
@dynamic capricornNumber;
@dynamic categoryID;
@dynamic categoryName;
@dynamic city;
@dynamic completeVisitCount;
@dynamic createdDate;
@dynamic customerID;
@dynamic customerType;
@dynamic dealingOEMs;
@dynamic deleted;
@dynamic email;
@dynamic fax;
@dynamic getGenuineMember;
@dynamic incompleteVisitCount;
@dynamic isAccountConfirmed;
@dynamic isActive;
@dynamic isCustomerSentToServer;
@dynamic lastRequestedFromServer;
@dynamic lastVisit;
@dynamic latitude;
@dynamic longitude;
@dynamic mainContactNo;
@dynamic membershipNumber;
@dynamic modifiedDate;
@dynamic name;
@dynamic nextVisit;
@dynamic notes;
@dynamic oldCustomerID;
@dynamic otherOEMs;
@dynamic photo;
@dynamic photoLastRequestedFromServer;
@dynamic postCode;
@dynamic repairsPerWeek;
@dynamic state;
@dynamic suburb;
@dynamic territory;
@dynamic territoryAbbreviation;
@dynamic territoryID;
@dynamic tollFree;
@dynamic tradeClub;
@dynamic tradeClubNumber;
@dynamic wdSiteID;
@dynamic workshopBays;
@dynamic contacts;
@dynamic parentRep;
@dynamic photos;
@dynamic scheduledVisits;
@dynamic tasks;
@dynamic visits;

@synthesize deletedContactIds;
@synthesize previousTerritory;
@synthesize isAddressChanged;


-(void)dealloc
{
    [previousTerritory release];
    [deletedContactIds release];
    
    [super dealloc];
}

@end
