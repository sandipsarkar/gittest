//
//  Reminder.h
//  RepVisitationTool
//
//  Created by Some Sankar Ray on 01/03/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Reminder : NSManagedObject

@property (nonatomic, retain) NSString * eventIdentifier;
@property (nonatomic, retain) NSNumber * reminderID;
@property (nonatomic, retain) NSString * alarms;
@property (nonatomic, retain) NSDate * endDate;
@property (nonatomic, retain) NSNumber * isAllDay;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * recurrenceRule;

@end
