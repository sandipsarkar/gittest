//
//  Events.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 17/10/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Events : NSManagedObject

@property (nonatomic, retain) NSNumber * eventID;
@property (nonatomic, retain) NSString * eventIdentifier;

@end
