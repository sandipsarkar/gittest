//
//  Task.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 18/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "Task.h"
#import "Customer.h"
#import "CustomEventStore.h"

@implementation Task

@dynamic completedDate;
@dynamic completedNotes;
@dynamic dueDate;
@dynamic isCompleted;
@dynamic isOffline;
@dynamic isReminder;
@dynamic isStarred;
@dynamic notes;
@dynamic subject;
@dynamic taskID;
@dynamic customerName;
@dynamic customerID;
@dynamic parentCustomer;
@dynamic eventIdentifier;
@dynamic oldTaskID;
@synthesize wasCustomerAvailable;


+(id)willSetValue:(id)value forKey:(id)key
{
    NSLog(@"key=%@",key);
    NSLog(@"value=%@",value);
    
    value = NULL_NIL(value);
    
    
    if([key rangeOfString:@"Date"].location!=NSNotFound)
    {
        //[NSDate dateWithTimeIntervalSince1970:interval/1000.0];
        //NSDate *date  = value ? [NSDate dateFromWCFTimeInterval:[value doubleValue]] : nil;
        NSDate *date  = value ? [NSDate dateWithTimeIntervalSince1970:[value doubleValue]] : nil;
        
        if(date)
        date=[date currentDateHourMinute];
        
        NSLog(@"Date=%@",date);
        return date;
    }
    
    return value;
}

-(void)willDelete
{
  Task *_task = (Task *)self;
    
    NSString *eventIdentifier = [_task.eventIdentifier copy] ;
    if(eventIdentifier.length)
    {
        
         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
             
        EKEvent *eventToRemove = [[CustomEventStore sharedInstance].eventStore eventWithIdentifier:eventIdentifier];
        if(eventToRemove)
            [[CustomEventStore sharedInstance].eventStore removeEvent:eventToRemove span:EKSpanThisEvent error:nil];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 //[eventIdentifier release];
                 
             });
         });
        
    }
    [eventIdentifier release];
}

/*
-(void)prepareForDeletion
{
    
}
*/

@end
