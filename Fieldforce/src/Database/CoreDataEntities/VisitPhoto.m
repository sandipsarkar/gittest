//
//  VisitPhoto.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 22/08/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "VisitPhoto.h"
#import "Visit.h"


@implementation VisitPhoto

@dynamic caption;
@dynamic imageName;
@dynamic imagePath;
@dynamic oemID;
@dynamic oemName;
@dynamic serverImageLink;
@dynamic thumbImagePath;
@dynamic createdDate;
@dynamic parentVisit;

-(NSDictionary *)uploadDescription
{
    //=============================== Should be set dynamically ============================
    
    if(!self.oemID) self.oemID = [NSNumber numberWithInteger:0];
    
    NSTimeInterval _dateInterval = [self.createdDate GMTTimeIntervalSince1970];
    NSString *createdDateString  = [NSString stringWithFormat:@"%lf",_dateInterval];
    
    NSMutableDictionary *dict=[NSMutableDictionary dictionary];
    [dict setValue:self.oemID?self.oemID:[NSNull null] forKey:@"oemID"];
    [dict setValue:self.imageName.length?self.imageName:@"" forKey:@"imageName"];
    [dict setValue:createdDateString forKey:@"createdDateString"];
    [dict setValue:self.caption?self.caption:@"" forKey:@"imageDescription"];
    
    //NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:self.oemID,@"oemID",self.imageName,@"imageName",createdDateString, @"createdDate",self.caption,@"imageDescription", nil];
    return dict;
}

-(void)prepareForDeletion
{
    NSString *imagePath = self.imagePath ;
    NSString *thumbImagePath = self.thumbImagePath;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if(imagePath && [fileManager fileExistsAtPath:imagePath])
    {
        [fileManager removeItemAtPath:imagePath error:nil];
    }
    
    if(thumbImagePath && [fileManager fileExistsAtPath:thumbImagePath])
    {
        [fileManager removeItemAtPath:thumbImagePath error:nil];
    }
}


@end
