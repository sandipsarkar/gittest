//
//  Rep.m
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "Rep.h"
#import "Customer.h"
#import "Territory.h"
#import "WholesaleDealer.h"


@implementation Rep

@dynamic address;
@dynamic address2;
@dynamic email;
@dynamic firstName;
@dynamic lastName;
@dynamic lastRequestedCustomersDate;
@dynamic lastRequestedNewsDate;
@dynamic lastRequestedOffersDate;
@dynamic lastRequestedScheduleVisitDate;
@dynamic lastRequestedTaskDate;
@dynamic mobile;
@dynamic password;
@dynamic repID;
@dynamic userID;
@dynamic customers;
@dynamic parentWdSite;
@dynamic territories;
@dynamic phone;

@end
