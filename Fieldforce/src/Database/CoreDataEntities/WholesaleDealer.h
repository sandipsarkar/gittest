//
//  WholesaleDealer.h
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OEM, Rep;

@interface WholesaleDealer : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * parentWdID;
@property (nonatomic, retain) NSString * parentWdName;
@property (nonatomic, retain) NSNumber * wdID;
@property (nonatomic, retain) NSNumber * wdSiteID;
@property (nonatomic, retain) NSSet *parentOem;
@property (nonatomic, retain) Rep *rep;
@end

@interface WholesaleDealer (CoreDataGeneratedAccessors)

- (void)addParentOemObject:(OEM *)value;
- (void)removeParentOemObject:(OEM *)value;
- (void)addParentOem:(NSSet *)values;
- (void)removeParentOem:(NSSet *)values;

@end
