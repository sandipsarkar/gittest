//
//  OEM.m
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "OEM.h"
#import "WholesaleDealer.h"


@implementation OEM

@dynamic name;
@dynamic oemID;
@dynamic wholesaleDealer;

@end
