//
//  Reminder.m
//  RepVisitationTool
//
//  Created by Some Sankar Ray on 01/03/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "Reminder.h"
#import "CustomEventStore.h"


@implementation Reminder

@dynamic eventIdentifier;
@dynamic reminderID;
@dynamic alarms;
@dynamic endDate;
@dynamic isAllDay;
@dynamic location;
@dynamic notes;
@dynamic startDate;
@dynamic title;
@dynamic recurrenceRule;

+(id)willSetValue:(id)value forKey:(id)key
{
    value = NULL_NIL(value);
    
    if([key rangeOfString:@"Date"].location!=NSNotFound)
    {
        // NSDate *date  = value ? [NSDate dateFromWCFTimeInterval:[value doubleValue]] : nil;
        
        NSDate *date  = value ? [NSDate dateWithTimeIntervalSince1970:[value doubleValue]] : nil;
        
        // if(date && [key isEqualToString:@"visitStartDate"])
        //   date=[date currentDateHourMinute];
        
        return date;
    }
    
    return value;
}


-(void)willDelete
{
    Reminder *_reminder = (Reminder *)self;
    
    @try {
        
        NSString *eventIdentifier = _reminder.eventIdentifier;
        if(eventIdentifier.length)
        {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                EKReminder *eventToRemove = (EKReminder *) [[CustomEventStore sharedInstance].eventStore  calendarItemWithIdentifier:eventIdentifier];
                
                if(eventToRemove)
                    [[CustomEventStore sharedInstance].eventStore removeReminder:eventToRemove commit:YES error:nil];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    // [eventIdentifier release];
                    
                });
            });
        }
        
    }
    
    @catch (NSException *exception) {
        
        NSLog(@"Task Exception=%@",exception.reason);
    }
    @finally {
        
    }
    
}


@end
