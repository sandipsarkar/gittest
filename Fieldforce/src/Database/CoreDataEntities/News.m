//
//  News.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 03/08/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "News.h"


@implementation News

@dynamic newsID;
@dynamic newsTitle;
@dynamic newsImage;
@dynamic newsDescription;
@dynamic oemID;
@dynamic createdDate;
@dynamic noOfView;
@dynamic deleted;
@dynamic isRead;
@dynamic categoryID;
@dynamic creator;
@dynamic userTypeName;
@dynamic newsPdf;


@end
