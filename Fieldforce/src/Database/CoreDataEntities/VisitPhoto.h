//
//  VisitPhoto.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 22/08/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Visit;

@interface VisitPhoto : NSManagedObject

@property (nonatomic, retain) NSString * caption;
@property (nonatomic, retain) NSString * imageName;
@property (nonatomic, retain) NSString * imagePath;
@property (nonatomic, retain) NSNumber * oemID;
@property (nonatomic, retain) NSString * oemName;
@property (nonatomic, retain) NSString * serverImageLink;
@property (nonatomic, retain) NSString * thumbImagePath;
@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) Visit *parentVisit;

-(NSDictionary *)uploadDescription;

@end
