//
//  CustomerPhoto.m
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "CustomerPhoto.h"
#import "Customer.h"


@implementation CustomerPhoto

@dynamic createdDate;
@dynamic customerID;
@dynamic customerImageID;
@dynamic imageName;
@dynamic isUploaded;
@dynamic customer;

@synthesize  isDownloading;

@end
