//
//  CustomerPhoto.h
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Customer;

@interface CustomerPhoto : NSManagedObject

@property (nonatomic, retain) NSNumber * createdDate;
@property (nonatomic, retain) NSNumber * customerID;
@property (nonatomic, retain) NSNumber * customerImageID;
@property (nonatomic, retain) NSString * imageName;
@property (nonatomic, retain) NSNumber * isUploaded;
@property (nonatomic, retain) Customer *customer;

@property (nonatomic, assign) BOOL isDownloading;

@end
