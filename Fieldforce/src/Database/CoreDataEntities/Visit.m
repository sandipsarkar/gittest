//
//  Visit.m
//  RepVisitationTool
//
//  Created by Sandip Sarkar on 06/05/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "Visit.h"
#import "Customer.h"
#import "ScheduledVisit.h"
#import "VisitPhoto.h"


@implementation Visit

@dynamic actualEndDate;
@dynamic drawingPath;
@dynamic endDateIntervalSinceRefDate;
@dynamic inactiveSeconds;
@dynamic isImagesUploaded;
@dynamic isReportCompleted;
@dynamic isVisitCompleted;
@dynamic lastDraftDate;
@dynamic natureOfVisitID;
@dynamic recordFilePath;
@dynamic recordingDuration;
@dynamic reportCompletedDate;
@dynamic reportSummary;
@dynamic scheduleVisitID;
@dynamic startDate;
@dynamic visitCompletedDate;
@dynamic visitedOfferIDs;
@dynamic visitID;
@dynamic visitingNotes;
@dynamic sessionStartLat;
@dynamic sessionStartLong;
@dynamic sessionEndLong;
@dynamic sessionEndLat;
@dynamic reportPublishLong;
@dynamic reportPublishLat;
@dynamic parentCustomer;
@dynamic photos;
@dynamic scheduledVisit;

-(void)prepareForDeletion
{
  NSString *objectId = [NSString stringWithFormat:@"%u", [self.visitID hash]];
  NSString *drawingPath = [DirectoryManager drawingDirectoryPathForName:objectId];
    
  if(drawingPath && [[NSFileManager defaultManager] fileExistsAtPath:drawingPath])
  {
      [[NSFileManager defaultManager] removeItemAtPath:drawingPath error:nil];
  }
    
    NSString *recordFileName = self.recordFilePath;
    NSString *_recordFilePath =  [DirectoryManager recoredAudioPathForName:recordFileName];
    
    if(_recordFilePath && [[NSFileManager defaultManager] fileExistsAtPath:_recordFilePath])
    {
        [[NSFileManager defaultManager] removeItemAtPath:_recordFilePath error:nil];
    }
}

@end
