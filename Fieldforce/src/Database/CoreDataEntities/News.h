//
//  News.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 03/08/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface News : NSManagedObject

@property (nonatomic, retain) NSNumber * newsID;
@property (nonatomic, retain) NSString * newsTitle;
@property (nonatomic, retain) NSString * newsImage;
@property (nonatomic, retain) NSString * newsDescription;
@property (nonatomic, retain) NSString * creator;
@property (nonatomic, retain) NSNumber * oemID;
@property (nonatomic, retain) NSNumber * createdDate;
@property (nonatomic, retain) NSNumber * noOfView;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSNumber * isRead;
@property (nonatomic, retain) NSNumber * categoryID;
@property (nonatomic,retain)  NSString * userTypeName;
@property (nonatomic,retain)  NSString * newsPdf;


@end
