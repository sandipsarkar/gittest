//
//  ResourceFolder.h
//  Fieldforce
//
//  Created by Randem IT on 17/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ResourceMaster;

@interface ResourceFolder : NSManagedObject

@property (nonatomic, retain) NSNumber * createdDate;
@property (nonatomic, retain) NSNumber * parentFolderID;
@property (nonatomic, retain) NSNumber * resourceFolderID;
@property (nonatomic, retain) NSString * resourceFolderName;
@property (nonatomic, retain) NSNumber * userTypeID;
@property (nonatomic, retain) NSNumber * wdOrOemID;
@property (nonatomic, retain) NSNumber * modifiedDate;
@property (nonatomic, retain) NSSet *resources;
@end

@interface ResourceFolder (CoreDataGeneratedAccessors)

- (void)addResourcesObject:(ResourceMaster *)value;
- (void)removeResourcesObject:(ResourceMaster *)value;
- (void)addResources:(NSSet *)values;
- (void)removeResources:(NSSet *)values;

@end
