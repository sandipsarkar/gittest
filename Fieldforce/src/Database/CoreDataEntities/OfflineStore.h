//
//  OfflineStore.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 11/01/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface OfflineStore : NSManagedObject

@property (nonatomic, retain) NSData * offlineCustomers;
@property (nonatomic, retain) NSData * offlineScheduledVisits;
@property (nonatomic, retain) NSData * offlineNews;
@property (nonatomic, retain) NSData * offlineOffers;
@property (nonatomic, retain) NSData * offlineTasks;
@property (nonatomic, retain) NSData * offlineReminders;

@property (nonatomic,retain)  NSData * feedbackCategories;
@property (nonatomic,retain) NSString *logoImageName;

@property(nonatomic, retain) NSMutableArray *feedbackCategory;

@property (nonatomic, retain) NSNumber * lastRequestedCustomersDate;
@property (nonatomic, retain) NSNumber * lastRequestedNewsDate;
@property (nonatomic, retain) NSNumber * lastRequestedOffersDate;
@property (nonatomic, retain) NSNumber * lastRequestedScheduleVisitDate;
@property (nonatomic ,retain) NSNumber * lastRequestedTaskDate;
@property (nonatomic ,retain) NSNumber * lastRequestedReminderDate;
@property (nonatomic ,retain) NSNumber * lastRequestedSuburbDate;
@property (nonatomic ,retain) NSNumber * lastRequestedResourcesDate;

@property (nonatomic, retain) NSNumber *isCustomerLoaded;


@end
