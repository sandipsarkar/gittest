//
//  ResourceMaster.h
//  Fieldforce
//
//  Created by Randem IT on 17/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ResourceFolder;

@interface ResourceMaster : NSManagedObject

@property (nonatomic, retain) NSNumber * createdBy;
@property (nonatomic, retain) NSString *createdByName;
@property (nonatomic, retain) NSNumber * createdDate;
@property (nonatomic, retain) NSString * fileExtension;
@property (nonatomic, retain) NSString * fileSize;
@property (nonatomic, retain) NSNumber * modifiedBy;
@property (nonatomic, retain) NSNumber * modifiedDate;
@property (nonatomic, retain) NSString * originalFileName;
@property (nonatomic, retain) NSNumber * resourceFolderID;
@property (nonatomic, retain) NSNumber * resourceID;
@property (nonatomic, retain) NSString * resourceName;
@property (nonatomic, retain) NSNumber * userTypeID;
@property (nonatomic, retain) NSNumber *isPublished;
@property (nonatomic, retain) ResourceFolder *folder;

@end
