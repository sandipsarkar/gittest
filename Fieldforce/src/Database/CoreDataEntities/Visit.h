//
//  Visit.h
//  RepVisitationTool
//
//  Created by Sandip Sarkar on 06/05/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Customer, ScheduledVisit, VisitPhoto;

@interface Visit : NSManagedObject

@property (nonatomic, retain) NSDate * actualEndDate;
@property (nonatomic, retain) NSString * drawingPath;
@property (nonatomic, retain) NSNumber * endDateIntervalSinceRefDate;
@property (nonatomic, retain) NSNumber * inactiveSeconds;
@property (nonatomic, retain) NSNumber * isImagesUploaded;
@property (nonatomic, retain) NSNumber * isReportCompleted;
@property (nonatomic, retain) NSNumber * isVisitCompleted;
@property (nonatomic, retain) NSDate * lastDraftDate;
@property (nonatomic, retain) NSNumber * natureOfVisitID;
@property (nonatomic, retain) NSString * recordFilePath;
@property (nonatomic, retain) NSNumber * recordingDuration;
@property (nonatomic, retain) NSDate * reportCompletedDate;
@property (nonatomic, retain) NSString * reportSummary;
@property (nonatomic, retain) NSNumber * scheduleVisitID;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSDate * visitCompletedDate;
@property (nonatomic, retain) NSString * visitedOfferIDs;
@property (nonatomic, retain) NSNumber * visitID;
@property (nonatomic, retain) NSString * visitingNotes;
@property (nonatomic, retain) NSString * sessionStartLat;
@property (nonatomic, retain) NSString * sessionStartLong;
@property (nonatomic, retain) NSString * sessionEndLong;
@property (nonatomic, retain) NSString * sessionEndLat;
@property (nonatomic, retain) NSString * reportPublishLong;
@property (nonatomic, retain) NSString * reportPublishLat;
@property (nonatomic, retain) Customer * parentCustomer;
@property (nonatomic, retain) NSSet *photos;
@property (nonatomic, retain) ScheduledVisit *scheduledVisit;
@end

@interface Visit (CoreDataGeneratedAccessors)

- (void)addPhotosObject:(VisitPhoto *)value;
- (void)removePhotosObject:(VisitPhoto *)value;
- (void)addPhotos:(NSSet *)values;
- (void)removePhotos:(NSSet *)values;

@end
