//
//  Offer.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 29/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "Offer.h"


@implementation Offer

@dynamic activeFrom;
@dynamic activeTo;
@dynamic categoryID;
@dynamic catName;
@dynamic deleted;
@dynamic isMarked;
@dynamic offerDescription;
@dynamic offerID;
@dynamic offerImage;
@dynamic externalLink;
@dynamic offerName;
@dynamic thumbImageLink;
@dynamic offerTerms;
@dynamic isPublished;
@dynamic createdByUserName;
@dynamic userType;
@dynamic userID;
@dynamic appViewed;
@dynamic offerLinks;
@dynamic offerPdf;


@end
