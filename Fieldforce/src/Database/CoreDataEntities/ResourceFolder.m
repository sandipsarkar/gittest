//
//  ResourceFolder.m
//  Fieldforce
//
//  Created by Randem IT on 17/12/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "ResourceFolder.h"
#import "ResourceMaster.h"


@implementation ResourceFolder


@dynamic parentFolderID;
@dynamic resourceFolderID;
@dynamic resourceFolderName;
@dynamic userTypeID;
@dynamic wdOrOemID;
@dynamic createdDate;
@dynamic modifiedDate;
@dynamic resources;



@end
