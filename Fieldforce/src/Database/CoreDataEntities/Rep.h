//
//  Rep.h
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "WholesaleDealer.h"

@class Customer, Territory, WholesaleDealer;

@interface Rep : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * address2;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSNumber * lastRequestedCustomersDate;
@property (nonatomic, retain) NSNumber * lastRequestedNewsDate;
@property (nonatomic, retain) NSNumber * lastRequestedOffersDate;
@property (nonatomic, retain) NSNumber * lastRequestedScheduleVisitDate;
@property (nonatomic, retain) NSNumber * lastRequestedTaskDate;
@property (nonatomic, retain) NSString * mobile;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSNumber * repID;
@property (nonatomic, retain) NSNumber * userID;
@property (nonatomic, retain) NSSet *customers;
@property (nonatomic, retain) WholesaleDealer *parentWdSite;
@property (nonatomic, retain) NSSet *territories;

@property (nonatomic,retain) NSString * phone;
@end

@interface Rep (CoreDataGeneratedAccessors)

- (void)addCustomersObject:(Customer *)value;
- (void)removeCustomersObject:(Customer *)value;
- (void)addCustomers:(NSSet *)values;
- (void)removeCustomers:(NSSet *)values;

- (void)addTerritoriesObject:(Territory *)value;
- (void)removeTerritoriesObject:(Territory *)value;
- (void)addTerritories:(NSSet *)values;
- (void)removeTerritories:(NSSet *)values;

@end
