//
//  WholesaleDealer.m
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "WholesaleDealer.h"
#import "OEM.h"
#import "Rep.h"


@implementation WholesaleDealer

@dynamic name;
@dynamic parentWdID;
@dynamic parentWdName;
@dynamic wdID;
@dynamic wdSiteID;
@dynamic parentOem;
@dynamic rep;

@end
