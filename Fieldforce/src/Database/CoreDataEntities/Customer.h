//
//  Customer.h
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CustomerContact, CustomerPhoto, Rep, ScheduledVisit, Task, Visit;

@interface Customer : NSManagedObject

@property (nonatomic, retain) NSString * accountNo;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * address2;
@property (nonatomic, retain) NSNumber * capricorn;
@property (nonatomic, retain) NSString * capricornNumber;
@property (nonatomic, retain) NSNumber * categoryID;
@property (nonatomic, retain) NSString * categoryName;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSNumber * completeVisitCount;
@property (nonatomic, retain) NSNumber * createdDate;
@property (nonatomic, retain) NSNumber * customerID;
@property (nonatomic, retain) NSString * customerType;
@property (nonatomic, retain) NSString * dealingOEMs;
@property (nonatomic, retain) NSNumber * deleted;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * fax;
@property (nonatomic, retain) NSNumber * getGenuineMember;
@property (nonatomic, retain) NSNumber * incompleteVisitCount;
@property (nonatomic, retain) NSNumber * isAccountConfirmed;
@property (nonatomic, retain) NSNumber * isActive;
@property (nonatomic, retain) NSNumber * isCustomerSentToServer;
@property (nonatomic, retain) NSNumber * lastRequestedFromServer;
@property (nonatomic, retain) NSString * lastVisit;
@property (nonatomic, retain) NSString * latitude;
@property (nonatomic, retain) NSString * longitude;
@property (nonatomic, retain) NSString * mainContactNo;
@property (nonatomic, retain) NSString * membershipNumber;
@property (nonatomic, retain) NSNumber * modifiedDate;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * nextVisit;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSNumber * oldCustomerID;
@property (nonatomic, retain) NSString * otherOEMs;
@property (nonatomic, retain) NSData * photo;
@property (nonatomic, retain) NSNumber * photoLastRequestedFromServer;
@property (nonatomic, retain) NSString * postCode;
@property (nonatomic, retain) NSString * repairsPerWeek;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * suburb;
@property (nonatomic, retain) NSNumber * territory;
@property (nonatomic, retain) NSString * territoryAbbreviation;
@property (nonatomic, retain) NSNumber * territoryID;
@property (nonatomic, retain) NSString * tollFree;
@property (nonatomic, retain) NSNumber * tradeClub;
@property (nonatomic, retain) NSString * tradeClubNumber;
@property (nonatomic, retain) NSNumber * wdSiteID;
@property (nonatomic, retain) NSString * workshopBays;
@property (nonatomic, retain) NSSet *contacts;
@property (nonatomic, retain) Rep *parentRep;
@property (nonatomic, retain) NSSet *photos;
@property (nonatomic, retain) NSSet *scheduledVisits;
@property (nonatomic, retain) NSSet *tasks;
@property (nonatomic, retain) NSSet *visits;

@property (nonatomic,retain) NSMutableSet *deletedContactIds;
@property (nonatomic,retain) NSNumber *previousTerritory;
@property (nonatomic,assign) BOOL isAddressChanged;


@end

@interface Customer (CoreDataGeneratedAccessors)

- (void)addContactsObject:(CustomerContact *)value;
- (void)removeContactsObject:(CustomerContact *)value;
- (void)addContacts:(NSSet *)values;
- (void)removeContacts:(NSSet *)values;

- (void)addPhotosObject:(CustomerPhoto *)value;
- (void)removePhotosObject:(CustomerPhoto *)value;
- (void)addPhotos:(NSSet *)values;
- (void)removePhotos:(NSSet *)values;

- (void)addScheduledVisitsObject:(ScheduledVisit *)value;
- (void)removeScheduledVisitsObject:(ScheduledVisit *)value;
- (void)addScheduledVisits:(NSSet *)values;
- (void)removeScheduledVisits:(NSSet *)values;

- (void)addTasksObject:(Task *)value;
- (void)removeTasksObject:(Task *)value;
- (void)addTasks:(NSSet *)values;
- (void)removeTasks:(NSSet *)values;

- (void)addVisitsObject:(Visit *)value;
- (void)removeVisitsObject:(Visit *)value;
- (void)addVisits:(NSSet *)values;
- (void)removeVisits:(NSSet *)values;

@end
