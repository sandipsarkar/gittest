
//
//  SSCoreDataManager.m
//  CoreDataBooks
//
//  Created by RANDEM MAC on 01/12/11.
//  Copyright (c) 2011 RANDEM IT. All rights reserved.
//

#import "SSCoreDataManager.h"

//static SSCoreDataManager *_coreDataManager=nil;

@interface NSArray(NULLRemover)

-(NSArray *)validatedArray;

@end

@implementation NSArray(NULLRemover)

-(NSArray *)validatedArray
{
  NSMutableArray *nulls = [NSMutableArray array];
   for (id candidate in self)
   {
     if (candidate != [NSNull null])
      [nulls addObject:candidate];
   }
   
    return nulls;
}

@end

@implementation SSCoreDataManager
@synthesize defaultDBStorePath,dbStorePath;
@synthesize defaultManagedObjectContext,managedObjectModel,persistentStoreCoordinator;

+(SSCoreDataManager *)sharedManager
{
//    if(_coreDataManager==nil)
//        _coreDataManager=[[SSCoreDataManager alloc] init];
//    
//    return _coreDataManager;

    static SSCoreDataManager *_coreDataManager = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        _coreDataManager = [[SSCoreDataManager alloc] init]; 
    });
    return _coreDataManager;
}

+(NSManagedObjectContext *)defaultContext
{
    return [SSCoreDataManager sharedManager].defaultManagedObjectContext;
}

-(id)init
{
    if(self=[super init])
    {
        
    }
    
    return self;
}
-(void)dealloc
{
    [defaultDBStorePath release],defaultDBStorePath=nil;
    [dbStorePath release],dbStorePath=nil;
    [defaultManagedObjectContext release];
    defaultManagedObjectContext=nil;
    [managedObjectModel release];
    managedObjectModel=nil;
    [persistentStoreCoordinator release];
    persistentStoreCoordinator=nil;
    
#if NS_BLOCKS_AVAILABLE
    if(_willChangeContentBlock) [_willChangeContentBlock release];
    if(_didChangeContentBlock)  [_didChangeContentBlock release];
    if(_didChangeObjectBlock)   [_didChangeObjectBlock release];
    if(_didChangeSectionBlock)  [_didChangeSectionBlock release];
#endif
    
    // [_coreDataManager release],_coreDataManager=nil;
    
    [super dealloc];
}


#pragma mark REGISTER for merging changes to main context
-(void)registerContextDidSaveNotificationOnContext:(NSManagedObjectContext *)context
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mergeChangesToDefaultContext:)
                                                 name:NSManagedObjectContextDidSaveNotification
                                               object:context];
}

#pragma mark DEGISTER for merging changes to main context
-(void)removeContextDidSaveNotificationOnContext:(NSManagedObjectContext *)context
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSManagedObjectContextDidSaveNotification object:context];
}
#pragma mrk -


- (NSString *)applicationDocumentsDirectory 
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

#pragma mark GET the managedObjectModel
/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if(managedObjectModel == nil) 
    {
        managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:nil] retain];  
    }
    
    return managedObjectModel;
}
#pragma mark -

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */

#pragma mark GET the persistentStoreCoordinator
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator 
{
    if (persistentStoreCoordinator == nil) 
    {
        NSString *defaultStorePath = self.defaultDBStorePath;
        
        NSLog(@"DefaultPath:%@",defaultStorePath);
        
        NSString *defaultDBFileName =  [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
        
        NSString *dbFileName=defaultStorePath.length?[defaultStorePath lastPathComponent]:[defaultDBFileName stringByAppendingPathExtension:@"sqlite"];
        
        NSString *storePath =self.dbStorePath ? self.dbStorePath : [[self applicationDocumentsDirectory] stringByAppendingPathComponent:dbFileName];
         /*
          Set up the store.
          For the sake of illustration, provide a pre-populated default store.
        */
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        // If the expected store doesn't exist, copy the default store.
        if (![fileManager fileExistsAtPath:storePath]) 
        {
            if (defaultStorePath) 
            {
                [fileManager copyItemAtPath:defaultStorePath toPath:storePath error:NULL];
            }
        }
        
         NSLog(@"StorePath:%@",storePath);
        
        NSURL *storeUrl = [NSURL fileURLWithPath:storePath];
        
        
     //   NSDictionary *_pragmaDict = [NSDictionary dictionaryWithObject:@"DELETE" forKey:@"journal_mode"];
        //NSSQLitePragmasOption
        
        NSDictionary *options = nil;
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
          options = @{NSMigratePersistentStoresAutomaticallyOption:@YES,
                                  NSInferMappingModelAutomaticallyOption:@YES,
                                  NSSQLitePragmasOption: @{@"journal_mode": @"DELETE"}
                                  }; //@"DELETE"
        }
        else
        {
             options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        }

        
        persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
        
        
        NSError *error;
        if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) 
           {
             NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            // exit(-1);  // Fail
           }
        
	}
    
    return persistentStoreCoordinator;
}
#pragma mark -

#pragma mark GET the default managedObjectContext
- (NSManagedObjectContext *) defaultManagedObjectContext 
{
    if (defaultManagedObjectContext == nil) 
    {
        
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) 
    {
        defaultManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        
          if([defaultManagedObjectContext respondsToSelector:@selector(setMergePolicy:)])
        [defaultManagedObjectContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
        
        [defaultManagedObjectContext setPersistentStoreCoordinator: coordinator];
    }
        
    }
    
    return defaultManagedObjectContext;
}
#pragma mark -

+(NSManagedObjectContext *)backgroundContext
{
    NSManagedObjectContext *context=[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType] ;
   // context.parentContext = DEFAULT_CONTEXT;
    [context setPersistentStoreCoordinator:DEFAULT_CONTEXT.persistentStoreCoordinator];
    [context setUndoManager:nil];
    
    return context;
    
    ///Must destroy this bgcontext after use by following method
}

+(NSManagedObjectContext *)backgroundContextWithConcurrencyType:(NSManagedObjectContextConcurrencyType)concurrencyType
{
    NSManagedObjectContext *context=[[NSManagedObjectContext alloc] initWithConcurrencyType:concurrencyType] ;
    // context.parentContext = DEFAULT_CONTEXT;
   // [context setPersistentStoreCoordinator:DEFAULT_CONTEXT.persistentStoreCoordinator];
    [context setUndoManager:nil];
    
    return context;
    
    ///Must destroy this bgcontext after use by following method
}

+(void)destroyBackgroundContext:(NSManagedObjectContext *)context
{
    if(context)
    {
        [context reset];
        [context release];
    }
}

#pragma mark GET all the changed object related to the context
-(NSSet *)_changedObjectsInContex:(NSManagedObjectContext *)context
{
    if(![context hasChanges]) return nil;
    
    NSSet *results =[context insertedObjects];
    if(![results count])
    {
        results =[context deletedObjects];
        if(![results count])
        {
            results =[context updatedObjects];
        }
    }

    return results;
}

#pragma mark -

//#warning Under development
#pragma mark PERFORM CREATE/DELETE/UPDATE on new context in background



-(void)performInDefaultContext:(void(^)(NSManagedObjectContext *context))operation completion:(void(^)(NSSet *result, NSError *error))completionBlock
{
        NSManagedObjectContext *context=self.defaultManagedObjectContext;
        
        if(operation) operation(context);
        
        NSSet *results =[self _changedObjectsInContex:context];        
        NSError *error=nil;
        
        [self save:error];
   
        if(completionBlock) completionBlock(results,error);
}

-(void)performAsyncOperation:(void(^)(NSManagedObjectContext *context))operation completion:(void(^)(NSSet *result, NSError *error))completionBlock
{
     NSManagedObjectContext *context   = self.defaultManagedObjectContext;
    
     NSManagedObjectContext *bgContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
     bgContext.parentContext = context;
     bgContext.undoManager = nil;
    
     [bgContext performBlock:^{
        
        if(operation) operation(bgContext);
         
           NSError *error = nil;
         
           __block  NSSet *results = nil;
           if([bgContext hasChanges] && [bgContext save:&error])
           {
               [context performBlockAndWait:^{
                   
                 if([context hasChanges]) [context save:nil];
                   
                  results =[self _changedObjectsInContex:context];
                 
                   if(completionBlock) completionBlock(results,error);
               }];
           }
           else
           {
               dispatch_sync(dispatch_get_main_queue(), ^{
                   
                   if(completionBlock) completionBlock(results,error);
               });
               
           }

         
         [bgContext reset];
         [bgContext release];
         
     }];
}
#pragma mark -


+(void)performAsyncFetchRequest:(NSFetchRequest *)aFetchRequest completion:(void(^)(NSFetchRequest *fetchRequest, NSArray *result, NSError *error))completionBlock
{
    NSManagedObjectContext *_mainContext = DEFAULT_CONTEXT;
    
    NSManagedObjectContext *bgContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    bgContext.parentContext = _mainContext;
    bgContext.undoManager   = nil;
    
    [bgContext performBlock:^{
        
    NSFetchRequest *_fetchRequest=[aFetchRequest copy];
      [_fetchRequest setResultType:NSManagedObjectIDResultType];
      [_fetchRequest setIncludesPropertyValues:NO];
        
        NSError *_error=nil;
        NSArray *_fetchedObjectIDS=[bgContext executeFetchRequest:_fetchRequest error:&_error];
        
  
        
        if([_fetchedObjectIDS count] && !_error)
        {
           NSFetchRequest *_modifiedFetchRequest=[aFetchRequest copy];
           NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self IN %@", _fetchedObjectIDS];
           [_modifiedFetchRequest setPredicate:predicate];
        
            
            [_mainContext performBlockAndWait:^{
                
                NSError *error=nil;
                NSArray *_results=[_mainContext executeFetchRequest:_modifiedFetchRequest error:&error];
                
                if(completionBlock) completionBlock(aFetchRequest,_results,error);
                
                [_modifiedFetchRequest release];
                
            }];
            
        }
        else
        {
            ///Call on the main thread
            dispatch_sync(dispatch_get_main_queue(), ^{
                if(completionBlock) completionBlock(aFetchRequest,nil,_error);
            });
        }
        
        
        [_fetchRequest release];
        [bgContext reset];
        [bgContext release];
        
    }];
}


#pragma mark Save Context
-(void)saveContext:(NSManagedObjectContext *)context error:(NSError *)error
{
    if(!context || ![context hasChanges]) return;
    
    // BOOL isMainContext = (self.defaultManagedObjectContext == context);
    // BOOL _mergeChanges = !isMainContext;
    
    BOOL _mergeChanges = (self.defaultManagedObjectContext != context);
    
    @try 
    {
        if(_mergeChanges)
        {
            if(!context.parentContext)
            [self registerContextDidSaveNotificationOnContext:context];
        }
        
        // if ([context hasChanges] && ![context save:&error])
        if(![context save:&error])
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
        /*
        else if(!isMainContext)
        {
            [self save:nil];
        }
        */
    }
    @catch (NSException *exception) 
    {
        NSLog(@"Exception:%@ reason:%@",exception.name,exception.reason);
        NSLog(@"Exception end");
        
       // [self exceptionDidOccur];
    }
    @finally 
    {
        if(_mergeChanges)
        {
            if(!context.parentContext)
            [self removeContextDidSaveNotificationOnContext:context];
        }
    }

}


-(void)_saveContext:(NSManagedObjectContext *)context error:(NSError *)error
{
    if(!context || ![context hasChanges]) return;
    
    __block  NSError *_error = error;
   
    if(![context respondsToSelector:@selector(parentContext)])
    {
        BOOL _mergeChanges = (self.defaultManagedObjectContext != context);
        
        @try
        {
            if(_mergeChanges)
            {
                if(!context.parentContext)
                    [self registerContextDidSaveNotificationOnContext:context];
            }
            
            if(![context save:&error])
            {
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            }
            
        }
        @catch (NSException *exception)
        {
            NSLog(@"Exception:%@ reason:%@",exception.name,exception.reason);
            NSLog(@"Exception end");
            
            // [self exceptionDidOccur];
        }
        @finally
        {
            if(_mergeChanges)
            {
                if(!context.parentContext)
                    [self removeContextDidSaveNotificationOnContext:context];
            }
        }

        return;
    }
    
    
    id _block = ^{
        
        @try
        {
            if([context save:&_error])
            {
                NSManagedObjectContext *parentContext = context.parentContext;
                
                if(parentContext)
                {
                    [self saveContext:parentContext error:_error];
                }
            }
                
            }
            @catch (NSException *exception)
            {
                NSLog(@"Exception:%@ reason:%@",exception.name,exception.reason);
                
                if(error)
                 NSLog(@"Error:%@",error);
            }
           @finally
           {
              
           }
    };
    
    
    
    if([NSThread isMainThread])
    {
        if(context.concurrencyType == NSPrivateQueueConcurrencyType)
        {
            [context performBlock:_block];
        }
        else if(context.concurrencyType == NSMainQueueConcurrencyType)
        {
            [context performBlockAndWait:_block];
        }
    }
    else
    {
        
        [context performBlockAndWait:_block];
        
    }
    
    
    //-------------------- OLD --------------
    /*
     NSManagedObjectContext* c = context;
     __block BOOL success = YES;
     
     while (c && success) 
     {
        [c performBlockAndWait:^{
     
        success = [c save:&error];
        //handle save success/failure
     
     }];
     
       c = c.parentContext;
     }
    */
}

-(void)exceptionDidOccur
{
    
}
-(void)save:(NSError *)error
{
    [self saveContext:self.defaultManagedObjectContext?self.defaultManagedObjectContext:DEFAULT_CONTEXT error:error];
}

#pragma mark -

-(NSManagedObject *)objectWithID:(NSManagedObjectID *)objectId inContext:(NSManagedObjectContext *)context
{
    return  (context==nil)?[self.defaultManagedObjectContext objectWithID:objectId] : [context objectWithID:objectId]; 
    
}

-(NSSet *)_mergedObjects:(NSNotification *)aNotification
{
    NSManagedObject *object=nil;
    NSSet *_mergedSet=[aNotification.userInfo valueForKey:@"updated"];
    if([_mergedSet count])
    {
        return _mergedSet;
    }
    else
    {
        _mergedSet=[aNotification.userInfo valueForKey:@"deleted"];
        
        if([_mergedSet count])
        {
            return _mergedSet;
        }
        else
        {
            _mergedSet=[aNotification.userInfo valueForKey:@"inserted"];
        }
    }
     
        if(![_mergedSet count]) return nil;
    
    
        object=[[_mergedSet allObjects] objectAtIndex:0];
        NSDictionary *att=[[object entity] attributesByName];
        NSMutableArray *predicates=[NSMutableArray array];
    
        for(NSString *property in [att allKeys])
        {      
            NSArray *value=[_mergedSet valueForKey:property];
            NSPredicate *predicate=[NSPredicate predicateWithFormat:@"%K IN %@",property,value];
            [predicates addObject:predicate];
        }
        
        if(!object) return nil;
        
        NSPredicate *_predicate=[NSCompoundPredicate andPredicateWithSubpredicates:predicates];
     
       
    //NSPredicate *_predicate=[NSPredicate predicateWithFormat:@"self IN %@",[[_mergedSet allObjects] valueForKey:@"objectID"]];
    NSMutableArray *arr=[[object class] fetchWithPredicate:_predicate sortDescriptors:nil limit:0 error:nil];
        
    
    return [NSSet setWithArray:arr];
}
#pragma mark merge changes form another context to new context
- (void)mergeChangesToDefaultContext:(NSNotification *)notification 
{
    if(![NSThread isMainThread])
    {
        [self performSelectorOnMainThread:@selector(mergeChangesToDefaultContext:) withObject:notification waitUntilDone:YES];
    
    }
    else
    {
        NSManagedObjectContext *mainContext = [self defaultManagedObjectContext];

        // main context saved, no need to perform the merge.
        if ([notification object] == mainContext) return;
         [mainContext mergeChangesFromContextDidSaveNotification:notification];
        
       // NSSet *mergedObjects=[self _mergedObjects:notification];
        //NSLog(@"count:%d",[mergedObjects count]);
        
        [[SSCoreDataManager sharedManager] save:nil];
    }
}
#pragma mark -


@end

#pragma mark -
#pragma mark Migration
@implementation SSCoreDataManager(Migration)

+(BOOL)performMigrationWithSourceMetadata :(NSDictionary *)sourceMetadata toDestinationModel:(NSManagedObjectModel *)destinationModel fromSourcePath:(NSString *)sourcePath toDestinationPath:(NSString *)destinationPath
{
    BOOL migrationSuccess = NO;
    //Initialise a Migration Manager...
    NSManagedObjectModel *sourceModel = [NSManagedObjectModel mergedModelFromBundles:nil
                                                                    forStoreMetadata:sourceMetadata];
    //Perform the migration.
    if (sourceModel)
    {
        NSMigrationManager *standardMigrationManager = [[NSMigrationManager alloc] initWithSourceModel:sourceModel
                                                                                      destinationModel:destinationModel];
        //Retrieve the appropriate mapping model.
        NSArray *theBundles = [NSArray arrayWithObject:[NSBundle mainBundle]];
        NSMappingModel *mappingModel = [NSMappingModel mappingModelFromBundles:theBundles
                                                                forSourceModel:sourceModel
                                                              destinationModel:destinationModel];
        
        if (nil == mappingModel)
        {
            mappingModel = [NSMappingModel inferredMappingModelForSourceModel:sourceModel
                                                             destinationModel:destinationModel
                                                                        error:nil];
        }
        
        if (mappingModel)
        {
            NSError *error = nil;
            NSString *storeSourcePath = sourcePath;
            NSURL *storeSourceUrl = [NSURL fileURLWithPath: storeSourcePath];
            NSString *storeDestPath = destinationPath ;
            NSURL *storeDestUrl = [NSURL fileURLWithPath:storeDestPath];
            
            if([storeSourceUrl isEqual:storeDestUrl]) return NO;
            
            //Pass nil here because we don't want to use any of these options:
            //NSIgnorePersistentStoreVersioningOption, NSMigratePersistentStoresAutomaticallyOption, or NSInferMappingModelAutomaticallyOption
            
            NSDictionary *sourceStoreOptions = nil;
            NSDictionary *destinationStoreOptions = nil;
            
            migrationSuccess = [standardMigrationManager migrateStoreFromURL:storeSourceUrl
                                                                        type:NSSQLiteStoreType
                                                                     options:sourceStoreOptions
                                                            withMappingModel:mappingModel
                                                            toDestinationURL:storeDestUrl
                                                             destinationType:NSSQLiteStoreType
                                                          destinationOptions:destinationStoreOptions
                                                                       error:&error];
            NSLog(@"MIGRATION SUCCESSFUL? %@", (migrationSuccess==YES)?@"YES":@"NO");
        }
        
        [standardMigrationManager release];
    }
    else
    {
        //TODO: Error to user...
        NSLog(@"checkForMigration FAIL - No Mapping Model found!");
        //abort();
    }
    return migrationSuccess;
}



+(BOOL)checkForMigrationAtTargetPath:(NSString *)targetPath sourceStorePath:(NSString *)sourcePath targetObjectModel:(NSManagedObjectModel *)targetObjectModel onMigrationCallback:(void(^)(BOOL success))callback
{
    BOOL migrationSuccess = NO;
    
    NSString *storeTargetPath = targetPath;
    
    NSString *storeSourcePath = storeTargetPath;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
	if (![fileManager fileExistsAtPath:storeTargetPath])
    {
        //Version 2 SQL has not been created yet, so the source is still version 1...
        storeSourcePath = sourcePath;
        
    }
    else
    {
        //********** ALREADY MIGRATED WE DONT NEED MIGRATION AGAIN **************
       // return YES;
    }
    
    
    NSURL *storeSourceUrl = [NSURL fileURLWithPath: storeSourcePath];
	NSError *error = nil;
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
                                                                                              URL:storeSourceUrl
                                                                                            error:&error];
    if (sourceMetadata)
    {
        NSString *configuration = nil;
        NSManagedObjectModel *destinationModel = targetObjectModel;
        
        //Our Source 1 is going to be incompatible with the Version 2 Model, our Source 2 won't be.
        
        BOOL pscCompatible = [destinationModel isConfiguration:configuration compatibleWithStoreMetadata:sourceMetadata];
        
        NSLog(@"Is the STORE data COMPATIBLE? %@", (pscCompatible==YES) ?@"YES" :@"NO");
        
        if (pscCompatible == NO)
        {
            
            migrationSuccess = [self performMigrationWithSourceMetadata:sourceMetadata toDestinationModel:destinationModel fromSourcePath:storeSourcePath toDestinationPath:storeTargetPath];
            
           
            if(callback) callback(migrationSuccess);
        }
        else
        {
            return pscCompatible;
        }
    }
    else
    {
        NSLog(@"CheckForMigration FAIL - No Source Metadata! \nERROR: %@", [error localizedDescription]);
        
        if(callback) callback(migrationSuccess);
    }
    
    return migrationSuccess;
    
}//END





+(BOOL)isModelCompatibleWithPersistentStoreAtURL:(NSURL *)storeURL forPersistentStoreCoordinator:(NSPersistentStoreCoordinator *)persistentCoordinator
{
    NSError *error = nil;
    
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:storeURL error:&error];
    
    NSManagedObjectModel *destinationModel = [persistentCoordinator managedObjectModel];
    
    BOOL isModelCompatible = (sourceMetadata == nil) || [destinationModel isConfiguration:nil compatibleWithStoreMetadata:sourceMetadata];
    
    
    return isModelCompatible;
}

+(BOOL)_performMigrationWithSourceMetadata :(NSDictionary *)sourceMetadata toDestinationModel:(NSManagedObjectModel *)destinationModel fromSourcePath:(NSString *)sourcePath toDestinationPath:(NSString *)destinationPath
{
    BOOL migrationSuccess = NO;
    //Initialise a Migration Manager...
    NSManagedObjectModel *sourceModel = [NSManagedObjectModel mergedModelFromBundles:nil
                                                                    forStoreMetadata:sourceMetadata];
    //Perform the migration...
    if (sourceModel)
    {
        NSMigrationManager *standardMigrationManager = [[NSMigrationManager alloc] initWithSourceModel:sourceModel
                                                                                      destinationModel:destinationModel];
        //Retrieve the appropriate mapping model...
        NSArray *theBundles = [NSArray arrayWithObject:[NSBundle mainBundle]];
        NSMappingModel *mappingModel = [NSMappingModel mappingModelFromBundles:theBundles
                                                                forSourceModel:sourceModel
                                                              destinationModel:destinationModel];
        
        if (nil == mappingModel)
        {
            mappingModel = [NSMappingModel inferredMappingModelForSourceModel:sourceModel
                                                             destinationModel:destinationModel
                                                                        error:nil];
        }

        
        NSArray *mappingModels = [self segregatedModelsFromMappingModel:mappingModel];
        
        //   NSArray *mappingModels = [NSArray arrayWithObjects:@"Model1",@"Model2",@"Model3", nil];
        
        NSError *error = nil;
        NSString *storeSourcePath = sourcePath;
        NSURL *storeSourceUrl = [NSURL fileURLWithPath: storeSourcePath];
        NSString *storeDestPath = destinationPath ;
        NSURL *storeDestUrl = [NSURL fileURLWithPath:storeDestPath];
        
        if([storeSourceUrl isEqual:storeDestUrl]) return NO;
        
        for(NSMappingModel *_mappingModel in mappingModels)
        {
            if (_mappingModel)
            {
                
                NSDictionary *sourceStoreOptions = nil;
                NSDictionary *destinationStoreOptions = nil;
                
                @autoreleasepool {
                    
                    migrationSuccess = [standardMigrationManager migrateStoreFromURL:storeSourceUrl
                                                                                type:NSSQLiteStoreType
                                                                             options:sourceStoreOptions
                                                                    withMappingModel:_mappingModel
                                                                    toDestinationURL:storeDestUrl
                                                                     destinationType:NSSQLiteStoreType
                                                                  destinationOptions:destinationStoreOptions
                                                                               error:&error];
                    
                    
                    NSLog(@"MIGRATION SUCCESSFUL? %@", (migrationSuccess==YES)?@"YES":@"NO");
                    
                }
                
                if(!migrationSuccess) break;
                
                [NSThread sleepForTimeInterval:1.5];
            }
            
        }
        
        [standardMigrationManager release];
    }
    else
    {
        //TODO: Error to user...
        NSLog(@"checkForMigration FAIL - No Mapping Model found!");
        
    }
    return migrationSuccess;
}


+(NSArray *)segregatedModelsFromMappingModel:(NSMappingModel *)mappingModel
{
    NSDictionary *entityMappingsByName = [mappingModel entityMappingsByName];
     NSArray *entityMappings = [mappingModel entityMappings];
    

    //  NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    // entityMappings = [entityMappings sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    //  [descriptor release];
    
    NSEntityMapping *_RepEntityMapping = [mappingModel valueForKey:@"RepToRep"];
    
     NSArray *arr = [entityMappings filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name=%@",@"RepToRep"]];
     
     NSEntityMapping *__RepEntityMapping = [arr count] ? [arr objectAtIndex:0] : nil;
     NSMutableArray *_entityMappings = [NSMutableArray arrayWithArray:entityMappings];
    
     if(__RepEntityMapping)
     [_entityMappings exchangeObjectAtIndex:0 withObjectAtIndex:__RepEntityMapping];
    
    
    NSMutableArray *traversedEntityMappings = [NSMutableArray array];
    NSMutableArray *mappingModels = [NSMutableArray array];
    
    int i=0;
    for(NSEntityMapping *entityMapping in _entityMappings)
    {
        
        if([traversedEntityMappings containsObject:entityMapping]) continue;
        
        NSMappingModel *newMappingModel = [[NSMappingModel alloc] init];
        
        NSMutableArray *newEntityMappings = [NSMutableArray array];
        
        if(i==0)
        {
            NSLog(@"Entity mapping = %@",entityMapping);
        }
        
        [newEntityMappings addObject:entityMapping];
        
        [self relationshipEntityMappingsFor:entityMapping entityMappings:entityMappingsByName addedMappings:newEntityMappings];
        [newMappingModel setEntityMappings:newEntityMappings];
        
        //----------------
        /// NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"_sourceEntityVersionHash" ascending:YES];
        // [newEntityMappings sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];
        // [descriptor release];
        //-----------------------------------
        
        [traversedEntityMappings addObjectsFromArray:newEntityMappings];
        
        [mappingModels addObject:newMappingModel];
        
        [newMappingModel release];
        
        i++;
    }
    
    
    
    
    return mappingModels;
}

+(void)relationshipEntityMappingsFor:(NSEntityMapping *)entityMapping entityMappings:(NSDictionary *)entityMappingsByName addedMappings:(NSMutableArray *)addedMappings
{
    
    NSString *entityName = [entityMapping sourceEntityName];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:DEFAULT_CONTEXT];
    
    NSDictionary *destinationRelationships = [entity relationshipsByName];
    
    NSArray *relationshipMappings = entityMapping.relationshipMappings;
    
    //NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:NO];
    //relationshipMappings = [relationshipMappings sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    // [descriptor release];
    
    for(NSPropertyMapping *relationshipMapping in relationshipMappings)
    {
        NSString *relationshipName = [relationshipMapping name];
        NSRelationshipDescription *destinationRelationship = [destinationRelationships objectForKey:relationshipName];
        
        NSLog(@"relationship mapping = %@",relationshipMapping);
        NSExpression *_expression = relationshipMapping.valueExpression;
        NSLog(@"Expression function = %@",_expression);
        
        if([_expression.arguments count])
        {
            NSString *entityMappingName = [[[_expression arguments] objectAtIndex:0] constantValue];
            
            //GET ENTITY MAPPING FOR entityMappingName;
            
            NSEntityMapping *_entityMapping = [entityMappingsByName objectForKey:entityMappingName];
            // [relationshipEntityMappings addObject:_entityMapping];
            
            if(![addedMappings containsObject:_entityMapping])
            {
                if ([destinationRelationship isToMany])
                {
                    [addedMappings addObject:_entityMapping];
                }
                else
                {
                    [addedMappings insertObject:_entityMapping atIndex:MAX([addedMappings count]-1,0)];
                }
                
                [self relationshipEntityMappingsFor:_entityMapping entityMappings:entityMappingsByName addedMappings:addedMappings];
                
                
            }
        }
    }
    
}

@end

#pragma mark -



#pragma mark DELETE
@implementation SSCoreDataManager (Delete)

+(void)deleteObject:(NSManagedObject *)object inContext:(NSManagedObjectContext *)context
{
    if(context && object)
    {
        NSManagedObjectContext *_mainContext = DEFAULT_CONTEXT;
        
        BOOL _isMainContext = (context ==_mainContext);
        
        if(_isMainContext)
        {
            [context deleteObject:object];
        }
        else
        {
            [context deleteObject:[context objectWithID:[object objectID]]];
            
        }
    }
}

+(void)deleteObject:(NSManagedObject *)object
{
    [SSCoreDataManager deleteObject:object inContext:DEFAULT_CONTEXT];
}


+(void)_deleteObject:(NSManagedObject *)object inContext:(NSManagedObjectContext *)context
{
    if(context && object)
    {
        NSError *_error = nil;
        NSManagedObject *_objectToDelete = [context existingObjectWithID:[object objectID] error:&_error];
        
        if(_objectToDelete && !_error)
            [context deleteObject:_objectToDelete];
    }
}


@end
#pragma mark -


@implementation SSCoreDataManager(NSFetchedResultsController)

- (NSFetchedResultsController *)fetchedResultsControllerWithFetchRequest:(NSFetchRequest *)aFetchRequest
{
	// Create and initialize the fetch results controller.
	NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:aFetchRequest managedObjectContext:self.defaultManagedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    
	return [aFetchedResultsController autorelease];
} 

#if NS_BLOCKS_AVAILABLE
-(void)addFetchedResultsControllerWillChangeContent:(ChangeContentblock)block
{
    if(block)
        _willChangeContentBlock=[block copy];
    
}

-(void)addFetchedResultsControllerDidChangeContent:(ChangeContentblock)block
{
    if(block)
        _didChangeContentBlock=[block copy];
}

-(void)addFetchedResultsControllerDidChangeObject:(DidChangeObject)block
{
    if(block)
        _didChangeObjectBlock=[block copy];
}

-(void)addFetchedResultsControllerDidChangeSection:(DidChangeSectionBlock)block
{
    if(block)
        _didChangeSectionBlock=[block copy];
}


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
    
    if(_willChangeContentBlock) _willChangeContentBlock(controller);
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
	
    
    if(_didChangeObjectBlock)
        _didChangeObjectBlock(controller,anObject,indexPath,type,newIndexPath);
    /*
     switch(type) {
     
     case NSFetchedResultsChangeInsert:
     break;
     
     case NSFetchedResultsChangeDelete:
     
     break;
     
     case NSFetchedResultsChangeUpdate:
     
     break;
     
     case NSFetchedResultsChangeMove:
     
     break;
     }
     */
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
	
    /*
     switch(type) 
     {
     
     case NSFetchedResultsChangeInsert:
     break;
     
     case NSFetchedResultsChangeDelete:
     
     break;
     }
     */
    
    if(_didChangeSectionBlock)
        _didChangeSectionBlock(controller,sectionInfo,sectionIndex,type);
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	// The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    
    if(_didChangeContentBlock)
        _didChangeContentBlock(controller);
}
#endif


@end

#pragma mark -

#import <objc/runtime.h>
@implementation NSManagedObject (NSManagedObject_Dictionary)
@dynamic traversed;

static char traversedKey;

-(void)setTraversed:(BOOL)_traversed
{
    if(_traversed!=[self traversed])
        objc_setAssociatedObject(self, &traversedKey, [NSNumber numberWithBool:_traversed], OBJC_ASSOCIATION_ASSIGN);
}

-(BOOL)traversed
{
    NSNumber *number=(NSNumber *)objc_getAssociatedObject(self, &traversedKey);
    return  [number boolValue];
}

#pragma mark NSManagedObject to NSDictionary
- (NSDictionary*) toDictionary
{
    self.traversed = YES;
    
    NSArray* attributes = [[[self entity] attributesByName] allKeys];
    NSArray* relationships = [[[self entity] relationshipsByName] allKeys];
    NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithCapacity:
                                 [attributes count] + [relationships count] + 1];
    
    [dict setObject:[[self class] description] forKey:@"class"];
    
    for (NSString* attr in attributes) {
        NSObject* value = [self valueForKey:attr];
        
        if (value != nil) {
            [dict setObject:value forKey:attr];
        }
    }
    
    for (NSString* relationship in relationships) {
        NSObject* value = [self valueForKey:relationship];
        
        if ([value isKindOfClass:[NSSet class]]) 
        {
            // To-many relationship
            
            // The core data set holds a collection of managed objects
            NSSet* relatedObjects = (NSSet*) value;
            
            // Our set holds a collection of dictionaries
            NSMutableSet* dictSet = [NSMutableSet setWithCapacity:[relatedObjects count]];
            
            for (NSManagedObject* relatedObject in relatedObjects) 
            {
                if (!relatedObject.traversed) 
                {
                    [dictSet addObject:[relatedObject toDictionary]];
                }
            }
            
            [dict setObject:dictSet forKey:relationship];
        }
        else if ([value isKindOfClass:[NSManagedObject class]]) 
        {
            // To-one relationship
            
            NSManagedObject* relatedObject = (NSManagedObject*) value;
            
            if (!relatedObject.traversed) 
            {
                // Call toDictionary on the referenced object and put the result back into our dictionary.
                [dict setObject:[relatedObject toDictionary] forKey:relationship];
            }
        }
    }
    
    return dict;
}

#pragma mark NSDictionary to NSManagedObject

-(BOOL)addValue:(id)value forKey:(NSString *)key
{
    BOOL _isUpdated = NO;
    
    // TO HANDLE 'isDeleted' key as coreData does not support isDeleted attribute in entity.
    
    if([key isEqualToString:@"isDeleted"]) key = @"deleted";
    
    
    SEL _selector= NSSelectorFromString(key);
    
    if(_selector && [self respondsToSelector:_selector])
    {
        if(value ==(id)[NSNull null]) value = nil;
        
        id oldValue = [self valueForKey:key];
        
        if(![value isEqual:oldValue])
        {
           [self setValue:value forKey:key];
            
           _isUpdated = YES;
        }
    }
    
    return _isUpdated;
}


//Populate on giving context

/*
- (void) populateFromDictionary:(NSDictionary*)dict onContext:(NSManagedObjectContext *)newContext
{
   
    NSManagedObjectContext* context =newContext;
    
    for (NSString* key in dict) 
    {
        if ([key isEqualToString:@"class"]) 
        {
            continue;
        }
        
        NSObject* value = [dict objectForKey:key];
        
        value = NULL_NIL(value);
        
        if ([value isKindOfClass:[NSDictionary class]]) 
        {
             // ======================== THIS IS TO-ONE RELATIONSHIP ==========================
            NSDictionary *relationShipDict =[[self entity] relationshipsByName];
            NSRelationshipDescription *relationShip = [relationShipDict valueForKey:key];
                
           // NSManagedObject* relatedObject =
           // [NSManagedObject createObjectFromDictionary:(NSDictionary*)value
                                                    // inContext:context];
            
             Class managedObjectClass= NSClassFromString([relationShip.destinationEntity managedObjectClassName]); 
            
            NSManagedObject* relatedObject =
            [managedObjectClass  createObjectFromDictionary:(NSDictionary*)value
                                              inContext:context];
            
            [self addValue:relatedObject forKey:key];
            
        // ======================== THIS IS TO-ONE RELATIONSHIP ==========================
        }
        else if ([value isKindOfClass:[NSSet class]] || [value isKindOfClass:[NSArray class]]) 
        {
            // ======================== THIS IS TO-MANY RELATIONSHIPS ==========================
            NSSet* relatedObjectDictionaries = ([value isKindOfClass:[NSSet class]]) ? (NSSet*) value : [NSSet setWithArray:(NSArray *)value];
            
            // Get a proxy set that represents the relationship, and add related objects to it.
            // (Note: this is provided by Core Data)
            NSMutableSet* relatedObjects = [self mutableSetValueForKey:key];
            NSDictionary *relationShipDict =[[self entity] relationshipsByName];
            NSLog(@"relationship:%@",relationShipDict);
            
            for (NSDictionary* relatedObjectDict in relatedObjectDictionaries) 
            {
                NSRelationshipDescription *relationShip = [relationShipDict valueForKey:key];
                 Class managedObjectClass= NSClassFromString([relationShip.destinationEntity managedObjectClassName]); 
                
                NSManagedObject* relatedObject =[managedObjectClass createObjectFromDictionary:relatedObjectDict
                                                                                         inContext:context];
                if(relatedObject)
                [relatedObjects addObject:relatedObject];
            }
            
            // ======================== THIS IS TO-MANY RELATIONSHIPS ==========================
        }
        
        
        else if (value != nil) 
        {
            NSLog(@"%@=%@",key,value);

           
            //This is an attribute
                       
            
            Class _currentClass = [self class];
            if([_currentClass  respondsToSelector:@selector(willSetValue:forKey:)])
            value =[_currentClass willSetValue:value forKey:key];
            
            [self addValue:value forKey:key];
        }
    }
}
*/

+ (NSManagedObject*)createOrUpdateObjectFromDictionary:(NSDictionary*)dict
                                             inContext:(NSManagedObjectContext*)context checkDuplicates:(NSString *)attribute
{
    NSString* class = [dict objectForKey:@"class"];
    
    //Change by sandip
    if(!class)
    {
        class=NSStringFromClass([self class]);
        [dict setValue:class forKey:@"class"];
    }
    
    //START:Fetching the duplicates
    
    //Constructing the predicate with the given attributes
    
    id valueToCheck = [dict valueForKey:attribute];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"%K=%@",attribute,valueToCheck];
    
    //Now fetch
    NSArray *fetchedRes=[[self class] fetchOnContext:context predicate:predicate sortDescriptors:nil error:nil];
    

    
    for(NSManagedObject *objToUpdate in fetchedRes)
    {
        [objToUpdate setAssociateObject:[NSNumber numberWithInt:1] forKey:@"_operationType"];
        [objToUpdate populateFromDictionary:dict];
        
        break;
    }
    //===========================================================================================
    
    if([fetchedRes count]) return [fetchedRes objectAtIndex:0];
    
    
    //CREATE: New new object if there is no existing object
    NSManagedObject* newObject=(NSManagedObject*)[NSEntityDescription insertNewObjectForEntityForName:class
                                                                               inManagedObjectContext:context];
    
    [newObject setAssociateObject:[NSNumber numberWithInt:0] forKey:@"_operationType"];
    [newObject populateFromDictionary:dict];
    
    return newObject;
}


-(BOOL)populateFromDictionary:(NSDictionary*)dict onContext:(NSManagedObjectContext *)newContext
{
    BOOL _isAnyDataUpdated= NO;
    
    
    NSManagedObjectContext* context =newContext;
    
    for (NSString* key in dict)
    {
        if ([key isEqualToString:@"class"])
        {
            continue;
        }
        
        NSObject* value = [dict objectForKey:key];
        
        value = NULL_NIL(value);
        
        if ([value isKindOfClass:[NSDictionary class]])
        {
            // ======================== THIS IS TO-ONE RELATIONSHIP ==========================
            NSDictionary *relationShipDict =[[self entity] relationshipsByName];
            NSRelationshipDescription *relationShip = [relationShipDict valueForKey:key];
            
            
            Class managedObjectClass= NSClassFromString([relationShip.destinationEntity managedObjectClassName]);
            
            NSString *keyToCheckDuplicate=nil;
            if([managedObjectClass  respondsToSelector:@selector(keyToCheckDuplicate)])
                keyToCheckDuplicate =[managedObjectClass performSelector:@selector(keyToCheckDuplicate)];
            
            NSManagedObject* relatedObject = nil;
            
            if(keyToCheckDuplicate)
            {
                relatedObject = [managedObjectClass createOrUpdateObjectFromDictionary:(NSDictionary*)value inContext:context checkDuplicates:keyToCheckDuplicate];
                
                // relatedObject =[managedObjectClass createObjectFromDictionary:(NSDictionary*)value inContext:context checkDuplicates:keyToCheckDuplicate];
            }
            else
            {
                relatedObject =
                [managedObjectClass  createObjectFromDictionary:(NSDictionary*)value
                                                      inContext:context];
            }
            
           BOOL _isUpdated = [self addValue:relatedObject forKey:key];
            
            if(_isUpdated && !_isAnyDataUpdated)
                _isAnyDataUpdated = YES;

        }
        else if ([value isKindOfClass:[NSSet class]] || [value isKindOfClass:[NSArray class]])
        {
            // ======================== THIS IS TO-MANY RELATIONSHIPS ==========================
            NSSet* relatedObjectDictionaries = ([value isKindOfClass:[NSSet class]]) ? (NSSet*) value : [NSSet setWithArray:(NSArray *)value];
            
            // Get a proxy set that represents the relationship, and add related objects to it.
            // (Note: this is provided by Core Data)
            NSMutableSet* relatedObjects   = [self mutableSetValueForKey:key];
            NSDictionary *relationShipDict = [[self entity] relationshipsByName];
            NSLog(@"relationship:%@",relationShipDict);
            
            NSString *keyToCheckDuplicate = nil;
            
            for (NSDictionary* relatedObjectDict in relatedObjectDictionaries)
            {
                NSRelationshipDescription *relationShip = [relationShipDict valueForKey:key];
                Class managedObjectClass= NSClassFromString([relationShip.destinationEntity managedObjectClassName]);
                
                if([managedObjectClass  respondsToSelector:@selector(keyToCheckDuplicate)])
                    keyToCheckDuplicate =[managedObjectClass performSelector:@selector(keyToCheckDuplicate)];
                
                NSManagedObject* relatedObject = nil;
                if(keyToCheckDuplicate)
                {
                    relatedObject = [managedObjectClass createOrUpdateObjectFromDictionary:relatedObjectDict inContext:context checkDuplicates:keyToCheckDuplicate];
                    
                    // [managedObjectClass createObjectFromDictionary:relatedObjectDict inContext:context checkDuplicates:keyToCheckDuplicate];
                }
                else
                {
                    relatedObject =[managedObjectClass createObjectFromDictionary:relatedObjectDict
                                                                        inContext:context];
                }
                if(relatedObject)
                    [relatedObjects addObject:relatedObject];
            }
        }
        
        
        else //if (value != nil)
        {
            NSLog(@"%@=%@",key,value);
            
            //========== This is an attribute ==============
            Class _currentClass = [self class];
            
            if([_currentClass  respondsToSelector:@selector(willSetValue:forKey:)])
                value =[_currentClass willSetValue:value forKey:key];
            
            if([self respondsToSelector:NSSelectorFromString(key)])
            {
                BOOL _isUpdated =  [self addValue:value forKey:key];
                
                 if(_isUpdated && !_isAnyDataUpdated)
                     _isAnyDataUpdated = YES;
            }
        }
    }
    
    return _isAnyDataUpdated;
}

/*
+(id)willSetValue:(id)value forKey:(id)key
{
    
    return value;
}
*/


//Populate on mainContext
- (BOOL) populateFromDictionary:(NSDictionary*)dict
{
    NSManagedObjectContext* context = [self managedObjectContext];
   return  [self populateFromDictionary:dict onContext:context];
}


+ (NSManagedObject*)createObjectFromDictionary:(NSDictionary*)dict
                                            inContext:(NSManagedObjectContext*)context
{
    NSString* class = [dict objectForKey:@"class"];
    
   
    if(!class)
    {
        class=NSStringFromClass([self class]);
        [dict setValue:class forKey:@"class"];
    }
    
    NSManagedObject* newObject =(NSManagedObject*)[NSEntityDescription insertNewObjectForEntityForName:class
                                                    inManagedObjectContext:context];
    
    [newObject populateFromDictionary:dict];
    
    return newObject;
}

#pragma mark Create from a dictionary with checking the duplicates




//keep
+(NSMutableArray *)checkOrCreateObjectsFromArray:(NSArray *)objects inContext:(NSManagedObjectContext *)context checkDuplicateOnAttribulte:(NSString *)attribute IDsToUpdate:(NSMutableArray *)idsToUpdate
{
    NSArray *value = [objects valueForKey:attribute];
    
    if(value)
    value = [value validatedArray];
    
    // NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"%K IN %@",attribute,value];
    
    NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"%K!=nil AND %K IN %@",attribute,attribute,value];
    
    NSLog(@"%@",fetchPredicate.predicateFormat);
    
    NSMutableArray *fetchedResult= [[self class] fetchOnContext:context?context:DEFAULT_CONTEXT predicate:fetchPredicate onAttributes:[NSArray arrayWithObject:attribute] sortDescriptors:nil limit:0 startIndex:0 resultType:NSDictionaryResultType error:nil];
    
    //if([fetchedResult count]==[objects count]) return nil;
    
    NSArray *duplicateIDs =  [fetchedResult valueForKey:attribute];
    
    [idsToUpdate setArray:duplicateIDs];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (%K IN %@)",attribute,duplicateIDs];
    NSArray *insertArray=[objects filteredArrayUsingPredicate:predicate];
    
    //[sourceArray removeObjectsInArray:fetchedResult];
    
    NSMutableArray *results=[[NSMutableArray alloc] init];
    for(NSDictionary *dict in insertArray)
    {
        NSManagedObject *obj=[[self class] createObjectFromDictionary:dict inContext:context];
        [results addObject:obj];
    }
    
    
    return [results autorelease];
}





-(BOOL)updateWithDictionary:(NSDictionary *)dict inContext:(NSManagedObjectContext *)context
{
    NSManagedObject *fetchedObject = (self.managedObjectContext != context) ? [self inContext:context] : self;
    
    BOOL _isAnyDataUpdated = NO;
    
    for(NSString *key in dict)
    {
        SEL _sel=NSSelectorFromString(key);

        if(![fetchedObject respondsToSelector:_sel]) continue;

        id newVal=[dict valueForKey:key];
        id oldVal=[fetchedObject valueForKey:key];

        if(![oldVal isEqual:newVal])
        {
            if(!_isAnyDataUpdated) _isAnyDataUpdated = YES;
            
            newVal = NULL_NIL(newVal);
            
            Class _currentClass = [self class];
            if([_currentClass  respondsToSelector:@selector(willSetValue:forKey:)])
                newVal =[_currentClass willSetValue:newVal forKey:key];
            
            [fetchedObject setValue:newVal forKey:key];
        }
    }
    
    return _isAnyDataUpdated;
}

/*
+(NSMutableArray *)createOrUpdateObjectsFromArray:(NSArray *)objects inContext:(NSManagedObjectContext *)context checkDuplicateOnAttributes:(NSArray *)attributes objectsToUpdate:(NSMutableArray *)objectsToUpdate
{
    
    NSLog(@"Start Date=%@",[NSDate date]);
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
  //  NSMutableArray *allObjects = [[self class] fetchOnContext:context predicate:nil sortDescriptors:nil error:nil];
    
    
    for(NSDictionary *dict in objects)
    {
         NSMutableArray *predicates = [[NSMutableArray alloc] init];
        for(NSString *property in attributes)
        {
           id val = [dict valueForKey:property];
            
            val = NULL_NIL(val);
            
            if(val)
            {
              NSPredicate *predicate=[NSPredicate predicateWithFormat:@"%K = %@",property,val];
              [predicates addObject:predicate];
            }
            
         }
        
        NSPredicate *fetchPredicate = [predicates count] ? [NSCompoundPredicate orPredicateWithSubpredicates:predicates] : nil;
        
     
        if(fetchPredicate)
        {
            NSMutableArray *fetchedResult = [[self class] fetchOnContext:context predicate:fetchPredicate onAttributes:nil sortDescriptors:nil limit:2 startIndex:0 error:nil];
            
            // NSArray *fetchedResult = [allObjects filteredArrayUsingPredicate:fetchPredicate];

            if([fetchedResult count])
            {
                for(NSManagedObject *obj in fetchedResult)
                {
                    // [obj populateFromDictionary:dict onContext:context];
                    BOOL _isUpdated = [obj updateWithDictionary:dict inContext:context];
                    
                    if(_isUpdated)
                    {
                        [objectsToUpdate addObject:obj];
                    }
                }

                //[objectsToUpdate addObjectsFromArray:fetchedResult];
            }
            else
            {
                NSManagedObject *obj=[[self class] createObjectFromDictionary:dict inContext:context];
                [results addObject:obj];
            }
        }
        else
        {
            NSManagedObject *obj=[[self class] createObjectFromDictionary:dict inContext:context];
            [results addObject:obj];
            
            
            //[allObjects addObject:obj];
        }
        
        [predicates release];

    }
    
    NSLog(@"End Date=%@",[NSDate date]);
        
    return [results autorelease];
}
*/


+(NSMutableArray *)_createOrUpdateObjectsFromArray:(NSArray *)objects inContext:(NSManagedObjectContext *)context checkDuplicateOnAttributes:(NSArray *)attributes objectsToUpdate:(NSMutableArray *)objectsToUpdate
{

   NSLog(@"Start Date=%@",[NSDate date]);
    
   //NSString *firstAttribute = nil;
    
    NSMutableArray *sortDescriptors = [NSMutableArray array];

    NSMutableArray *predicates      =  [NSMutableArray array];
    
   for(NSString *property in attributes)
   {
     //  if(!firstAttribute) firstAttribute = property;
       
       if([sortDescriptors count] < [attributes count])
       {
           NSSortDescriptor *_sortDescriptor = [[NSSortDescriptor alloc] initWithKey:property ascending:YES];
           [sortDescriptors addObject:_sortDescriptor];
           [_sortDescriptor release];
       }

       
       NSMutableArray *_values = [NSMutableArray array];
    
       [objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
       {
           
           id _objVal = [obj valueForKey:property];
           
           _objVal = NULL_NIL(_objVal);
           
           if(_objVal) [_values addObject:_objVal];
          
       }];
    

     if([_values count])
      {
          NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K IN %@",property,_values];
          [predicates addObject:predicate];
      }
    
    }


  NSPredicate *fetchPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicates];

  NSLog(@"%@",fetchPredicate.predicateFormat);

  NSMutableArray *fetchedResult= [[self class] fetchOnContext:context?context:DEFAULT_CONTEXT predicate:fetchPredicate  sortDescriptors:sortDescriptors   error:nil];

 //-------------- Sort objects according to sort descriptors -----------------------------------------
  NSArray *_objects = [objects sortedArrayUsingDescriptors:sortDescriptors];

 NSMutableArray *results = [[NSMutableArray alloc] init];
 for(NSDictionary *objDict in _objects)
  {
    
    if([fetchedResult count])
    {
        NSManagedObject *obj=nil;
        
        BOOL _isMatched = NO;
        for(NSManagedObject *_obj in fetchedResult)
        {
            for(NSString *property in attributes)
            {
                id dictVal = [objDict valueForKey:property];
                dictVal = NULL_NIL(dictVal);
                
                id objVal = [_obj valueForKey:property];
                objVal = NULL_NIL(objVal);
                
                if((dictVal && objVal) && !_isMatched)
                {
                  _isMatched = [objVal isEqual:dictVal];
                }
                
                if(_isMatched) break;
            }
            
            if(_isMatched)
            {
                obj = _obj;
                break;
            }
        }
        
        
        // IF MATCHED UPDATE THE OBJECT
        if(_isMatched)
        {
           // BOOL _isUpdated = [obj updateWithDictionary:objDict inContext:context];
            
            BOOL _isUpdated = [obj populateFromDictionary:objDict onContext:context];
            
            if(_isUpdated)
            {
                [objectsToUpdate addObject:obj];
            }
        }
        else // CREATE NEW NSMANAGEDOBJECT
        {
            NSManagedObject *managedObj = [[self class] createObjectFromDictionary:objDict inContext:context];
            [results addObject:managedObj];
        }
    }
    else    //FIRST TIME CREATE MANAGED OBJECTS
    {
        NSManagedObject *managedObj = [[self class] createObjectFromDictionary:objDict inContext:context];
        [results addObject:managedObj];
    }
      
}

  NSLog(@"End Date=%@",[NSDate date]);

return [results autorelease];

}


+(NSMutableArray *)createOrUpdateObjectsFromArray:(NSArray *)objects inContext:(NSManagedObjectContext *)context checkDuplicateOnAttributes:(NSArray *)attributes objectsToUpdate:(NSMutableArray *)objectsToUpdate
{
    
    NSLog(@"Start Date=%@",[NSDate date]);
    
    //NSString *firstAttribute = nil;
    
    NSMutableArray *sortDescriptors = [NSMutableArray array];
    
    NSMutableArray *predicates      =  [NSMutableArray array];
    
    for(NSString *property in attributes)
    {
        //  if(!firstAttribute) firstAttribute = property;
        
        if([sortDescriptors count] < [attributes count])
        {
            NSSortDescriptor *_sortDescriptor = [[NSSortDescriptor alloc] initWithKey:property ascending:YES];
            [sortDescriptors addObject:_sortDescriptor];
            [_sortDescriptor release];
        }
        
        
        NSMutableArray *_values = [NSMutableArray array];
        
        [objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
         {
             
             id _objVal = [obj valueForKey:property];
             
             _objVal = NULL_NIL(_objVal);
             
             if(_objVal) [_values addObject:_objVal];
             
         }];
        
        
        if([_values count])
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K IN %@",property,_values];
            [predicates addObject:predicate];
        }
        
    }
    
    
    NSPredicate *fetchPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicates];
    
    NSLog(@"%@",fetchPredicate.predicateFormat);
    
   // NSMutableArray *fetchedResult = [[self class] fetchOnContext:context?context:DEFAULT_CONTEXT predicate:fetchPredicate  sortDescriptors:sortDescriptors   error:nil];
    
    NSMutableArray *fetchedResult = [[self class] fetchOnContext:context?context:DEFAULT_CONTEXT predicate:fetchPredicate  onAttributes:nil sortDescriptors:sortDescriptors limit:0 startIndex:0 resultType:NSManagedObjectIDResultType error:nil];
    
    //-------------- Sort objects according to sort descriptors -----------------------------------------
    NSArray *_objects = [objects sortedArrayUsingDescriptors:sortDescriptors];
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    for(NSDictionary *objDict in _objects)
    {
        if([fetchedResult count])
        {
            NSManagedObject *obj = nil;
            
            BOOL _isMatched = NO;
            for(NSManagedObjectID *_objID in fetchedResult)
            {
                NSManagedObject *_obj = [context objectWithID:_objID];
                
                for(NSString *property in attributes)
                {
                    id dictVal = [objDict valueForKey:property];
                    dictVal = NULL_NIL(dictVal);
                    
                    id objVal = [_obj valueForKey:property];
                    objVal = NULL_NIL(objVal);
                    
                    if((dictVal && objVal) && !_isMatched)
                    {
                        _isMatched = [objVal isEqual:dictVal];
                    }
                    
                    if(_isMatched) break;
                }
                
                if(_isMatched)
                {
                    obj = _obj;
                    break;
                }
            }
            
            
            // IF MATCHED UPDATE THE OBJECT
            if(_isMatched)
            {
                // BOOL _isUpdated = [obj updateWithDictionary:objDict inContext:context];
                
                BOOL _isUpdated = [obj populateFromDictionary:objDict onContext:context];
                
                if(_isUpdated)
                {
                    [objectsToUpdate addObject:obj];
                }
            }
            else // CREATE NEW NSMANAGEDOBJECT
            {
                NSManagedObject *managedObj = [[self class] createObjectFromDictionary:objDict inContext:context];
                [results addObject:managedObj];
            }
        }
        else  //FIRST TIME CREATE MANAGED OBJECTS
        {
            NSManagedObject *managedObj = [[self class] createObjectFromDictionary:objDict inContext:context];
            [results addObject:managedObj];
        }
        
    }
    
    NSLog(@"End Date=%@",[NSDate date]);
    
    return [results autorelease];
    
}
/*
 NSMutableArray *_predicates = [[NSMutableArray alloc] init];
 
 for(NSString *property in attributes)
 {
 id val = [objDict valueForKey:property];
 val = NULL_NIL(val);
 
 if(val)
 {
 NSPredicate *_predicate=[NSPredicate predicateWithFormat:@"%K = %@",property,val];
 [_predicates addObject:_predicate];
 }
 
 }
 
 NSPredicate *_fetchPredicate = [_predicates count] ? [NSCompoundPredicate orPredicateWithSubpredicates:_predicates] : nil;
 
 NSLog(@"PredicateFormat = %@",_fetchPredicate.predicateFormat) ;
 
 [_predicates release];
 
 
 //FETCH THE DICTIONARY TO UPDATE
 NSArray *_fetchedResult = _fetchPredicate ? [fetchedResult filteredArrayUsingPredicate:_fetchPredicate] : nil;
 
 if([_fetchedResult count])
 {
 for(NSManagedObject *obj in _fetchedResult)
 {
 // [obj populateFromDictionary:dict onContext:context];
 BOOL _isUpdated = [obj updateWithDictionary:objDict inContext:context];
 
 if(_isUpdated)
 {
 [objectsToUpdate addObject:obj];
 }
 }
 
 }
 
 else
 {
 NSManagedObject *obj = [[self class] createObjectFromDictionary:objDict inContext:context];
 [results addObject:obj];
 }
 */

#pragma mark -

@end

#pragma mark FETCH NSManagedObject
@implementation NSManagedObject (Fetch)

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit startIndex:(NSUInteger)offset resultType:(NSFetchRequestResultType)type error:(NSError *)error
{
    
    NSString *entityName=NSStringFromClass([self class]);
    
    NSEntityDescription *ent = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest fetchRequestWithEntity:ent predicate:predicate sortDescriptors:sortDescriptors attributes:attributes limit:limit] retain];
    
    if(offset>0)
        [fetchRequest setFetchOffset:offset];
    
      if(type!=NSManagedObjectResultType)
      [fetchRequest setResultType:type];
    
      if(type==NSManagedObjectIDResultType)
      {
          [fetchRequest setIncludesPropertyValues:NO];
      }
    

	NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *mutableFetchResults = [NSMutableArray arrayWithArray:results];
    [fetchRequest release];
    
    return mutableFetchResults;
}

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit startIndex:(NSUInteger)offset error:(NSError *)error
{
    return [self fetchOnContext:context predicate:predicate onAttributes:attributes sortDescriptors:sortDescriptors limit:limit startIndex:0 resultType:NSManagedObjectResultType error:error];
}

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit error:(NSError *)error
{
	return [self fetchOnContext:context predicate:predicate onAttributes:attributes sortDescriptors:sortDescriptors limit:limit startIndex:0 error:error];
}

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes error:(NSError *)error
{
    return [[self class] fetchOnContext:context predicate:predicate onAttributes:attributes sortDescriptors:nil limit:0 error:error];
}

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors error:(NSError *)error
{
    return [[self class] fetchOnContext:context predicate:predicate onAttributes:nil sortDescriptors:sortDescriptors limit:0 error:error];
}

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate error:(NSError *)error
{
    return [[self class] fetchOnContext:context predicate:predicate onAttributes:nil sortDescriptors:nil limit:0 error:error];
}

+(NSMutableArray *)fetchWithPredicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit error:(NSError *)error
{
    return [[self class] fetchOnContext:[SSCoreDataManager defaultContext] predicate:predicate onAttributes:attributes sortDescriptors:sortDescriptors limit:limit error:error];
}

+(NSMutableArray *)fetchWithPredicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors error:(NSError *)error
{
    return [[self class] fetchOnContext:[SSCoreDataManager defaultContext] predicate:predicate onAttributes:nil sortDescriptors:sortDescriptors limit:0 error:error];
}

+(NSMutableArray *)fetchWithPredicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit error:(NSError *)error
{
    return [[self class] fetchOnContext:[SSCoreDataManager defaultContext] predicate:predicate onAttributes:nil sortDescriptors:sortDescriptors limit:limit error:error];
}


+(NSMutableArray *)fetchWithPredicate:(NSPredicate *)predicate error:(NSError *)error
{
    return [[self class] fetchOnContext:[SSCoreDataManager defaultContext] predicate:predicate onAttributes:nil sortDescriptors:nil limit:0 error:error];
}

+(NSMutableArray *)fetchAll:(NSError *)error
{
    return [[self class] fetchOnContext:[SSCoreDataManager defaultContext] predicate:nil onAttributes:nil sortDescriptors:nil limit:0 error:error];
}

+(void)fetchAsynWithPredicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit startIndex:(NSUInteger)offset resultType:(NSFetchRequestResultType)type completion:(void(^)(NSArray *result,NSError *error))completionBlock
{
    NSFetchRequest *fetchRequest  = [NSFetchRequest fetchRequestWithEntity:[self entity] predicate:predicate sortDescriptors:sortDescriptors attributes:attributes limit:limit];
    
    if(offset>0)
    fetchRequest.fetchOffset = offset;
    
    if(type!=NSManagedObjectResultType)
        [fetchRequest setResultType:type];
    
    [SSCoreDataManager performAsyncFetchRequest:fetchRequest completion:^(NSFetchRequest *fetchRequest, NSArray *result, NSError *error) 
     {
         if(completionBlock) completionBlock(result,error);
         
     }];

}

+(void)fetchAsynWithPredicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit  completion:(void(^)(NSArray *result,NSError *error))completionBlock
{
    [self fetchAsynWithPredicate:predicate onAttributes:nil sortDescriptors:sortDescriptors limit:limit startIndex:0 resultType:NSManagedObjectResultType completion:completionBlock];
}

+(void)fetchAsynWithPredicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors  completion:(void(^)(NSArray *result,NSError *error))completionBlock
{
    [self fetchAsynWithPredicate:predicate onAttributes:nil sortDescriptors:sortDescriptors limit:0 startIndex:0 resultType:NSManagedObjectResultType completion:completionBlock];
}

+(void)fetchAsynWithPredicate:(NSPredicate *)predicate completion:(void(^)(NSArray *result,NSError *error))completionBlock
{
    [self fetchAsynWithPredicate:predicate onAttributes:nil sortDescriptors:nil limit:0 startIndex:0 resultType:NSManagedObjectResultType completion:completionBlock];
}


+(void)fetchAsynWithPredicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit startIndex:(NSUInteger)offset completion:(void(^)(NSArray *result,NSError *error))completionBlock
{
    [self fetchAsynWithPredicate:predicate onAttributes:attributes sortDescriptors:sortDescriptors limit:limit startIndex:offset resultType:NSManagedObjectResultType completion:completionBlock];
}

@end
#pragma mark -

#pragma mark OPERATION NSManagedObject
@implementation NSManagedObject (OPERATION)


+(NSEntityDescription *)entity
{
    NSEntityDescription *ent=[NSEntityDescription entityForName:NSStringFromClass([self class]) inManagedObjectContext:DEFAULT_CONTEXT];
    
    return ent;
}

- (NSManagedObject *)inContext:(NSManagedObjectContext*)context
{
    return [context objectWithID:[self objectID]];
            
    NSError *error = nil;
    NSManagedObject *inContext = [[context existingObjectWithID:[self objectID] error:&error] retain];
    return [inContext autorelease];
}

+(NSManagedObject *)insertNewObjectInContext:(NSManagedObjectContext *)context
{
    NSString *entityName=NSStringFromClass([self class]);
    NSManagedObject *entityObject = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext: context];
    
    return entityObject;
}

+(NSManagedObject *)insertNewObject
{
    return [self insertNewObjectInContext:[SSCoreDataManager defaultContext]];
}

+(NSMutableArray *)deleteObjectsforIDs:(NSArray *)deleteIds onAttribute:(NSString *)attribute inContext:(NSManagedObjectContext *)context
{
    if(![deleteIds count] || !attribute) return nil;
    
    NSMutableArray *_deleted = [NSMutableArray array];
    
    NSPredicate *deletePredicte = [NSPredicate predicateWithFormat:@"%K IN %@",attribute,deleteIds];
        
    NSError *deleteFetchError   = nil;
        
    NSMutableArray *fetchedResult = [[self class] fetchOnContext:context predicate:deletePredicte onAttributes:nil sortDescriptors:nil limit:0 startIndex:0 resultType:NSManagedObjectResultType error:deleteFetchError];
        
        
    if(deleteFetchError) NSLog(@"Error:%@",[deleteFetchError description]);
        
    for(NSManagedObject *obj in fetchedResult)
    {
        if([obj  respondsToSelector:@selector(willDelete)])
            [obj willDelete];
            
        @try
        {
            [_deleted addObject:obj];
                
            [context deleteObject:obj];
        }
        @catch (NSException *exception)
        {
            NSLog(@"Exception=%@ name=%@",exception.name,exception.reason);
        }
        @finally
        {
        }
    }
    
    return _deleted;
}

+(void)asyncDeleteObjectsforIDs:(NSArray *)objects onAttribute:(NSString *)attribute completion:(void(^)(NSMutableArray *result, NSError *error))completionBlock
{ 
    if(!objects || !objects.count) if(completionBlock) completionBlock(nil,nil);
    
    NSManagedObjectContext *__mainContext = DEFAULT_CONTEXT;
    NSManagedObjectContext *context=[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType] ;
    context.parentContext = __mainContext;
    [context setUndoManager:nil];
   
    [context performBlock:^{
        
        NSPredicate *deletePredicte = [NSPredicate predicateWithFormat:@"%K IN %@",attribute,objects];
        
        NSMutableArray *fetchedResult= [[self class] fetchOnContext:context predicate:deletePredicte onAttributes:[NSArray arrayWithObject:attribute] sortDescriptors:nil limit:0 startIndex:0 resultType:NSManagedObjectResultType error:nil];
        
        
        //================== delete new objects =========================
        NSMutableArray *_deleted=[[NSMutableArray alloc] init];
        for(NSManagedObject *obj in fetchedResult)
        {
            [_deleted addObject:[obj inContext:__mainContext]];
            [context deleteObject:obj];
        }
        
           if(completionBlock) completionBlock(_deleted,nil);
        
        if([context hasChanges] && [context save:nil])
        {
            [__mainContext performBlockAndWait:^{
                
                [__mainContext save:nil];
                
                
            }];
        }
        
        [_deleted release];
        
        [context reset];
        [context release];

    }];
}



+(void)populateAsyncPerformingDelete:(NSArray *)deleteIds update:(NSArray *)updateIds insertOnArray:(NSArray *)objects onAttribute:(NSString *)attribute  completion:(void(^)(NSMutableArray *deletedResult,NSMutableArray *insertedResult,NSMutableArray *updatedResult, NSError *error))completionBlock

{
    if(![objects count] && ![deleteIds count] && ![updateIds count]) return;

    
    dispatch_queue_t queue =dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        
        
        NSMutableSet *_inserted = [NSMutableSet set];
        NSMutableSet *_deleted  = [NSMutableSet set];
        NSMutableSet *_updated  = [NSMutableSet set];
        
        //============= Create a new context with same persistentStoreCoordinator ============
        NSManagedObjectContext *context=[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType] ;
       // context.parentContext = DEFAULT_CONTEXT;
        [context setUndoManager:nil];
        [context setPersistentStoreCoordinator:DEFAULT_CONTEXT.persistentStoreCoordinator];
        
        
        NSMutableArray *_allObjects = NULL_NIL(objects)? [NSMutableArray arrayWithArray:objects]:[NSMutableArray array];
        
       
        
        
        //====================== PERFORM DELETE =============================
        
        if([deleteIds count])
        {
        NSPredicate *deletePredicte = [NSPredicate predicateWithFormat:@"%K IN %@",attribute,deleteIds];
        
        NSLog(@"Delete IDS:%@",deleteIds);
        NSError *deleteFetchError=nil;
        NSMutableArray *fetchedResult= [[self class] fetchOnContext:context predicate:deletePredicte onAttributes:nil sortDescriptors:nil limit:0 startIndex:0 resultType:NSManagedObjectResultType error:deleteFetchError]; //[NSArray arrayWithObject:attribute]
        
        if(deleteFetchError)
        {
            NSLog(@"Error:%@",[deleteFetchError description]);
        }
       // NSMutableArray *_deleted=[[NSMutableArray alloc] init];
        
        for(NSManagedObject *obj in fetchedResult)
        {
           // Class _currentClass = [self class];
            
            if([obj  respondsToSelector:@selector(willDelete)])
              [obj willDelete];
            
            [_deleted addObject:obj];
            
            @try
            {
                 [context deleteObject:obj];
            }
            @catch (NSException *exception)
            {
                NSLog(@"Exception=%@ name=%@",exception.name,exception.reason);
            }
            @finally
            {
                
            }
        }
        }
        
     
        
        //================= PERFORM INSERT ===================
        
        NSMutableArray *idsToUpdate = [NSMutableArray array];
        
        if([_allObjects count])
        {
           NSMutableArray *_insertedObjs = [[self class] checkOrCreateObjectsFromArray:_allObjects inContext:context checkDuplicateOnAttribulte:attribute IDsToUpdate:idsToUpdate];
            if([_insertedObjs count])
            {
                [_inserted addObjectsFromArray:_insertedObjs];
            }
        }
        
        NSPredicate *updatePredicate = [idsToUpdate count] ? [NSPredicate predicateWithFormat:@"%K IN %@",attribute,idsToUpdate] : nil;
        
        NSLog(@"Predicate query:%@",updatePredicate.predicateFormat);
        
        NSArray *_objectsToUpdate = updatePredicate ?   [_allObjects filteredArrayUsingPredicate:updatePredicate] : nil;
        
        //=============== PERFORM UPDATE =================
        
       // NSMutableArray *_updated = [[NSMutableArray alloc] init];
        
        NSMutableArray *offersToCheckUpdate= updatePredicate ?   [[self class] fetchOnContext:context predicate:updatePredicate onAttributes:nil sortDescriptors:nil limit:0 startIndex:0 resultType:NSManagedObjectResultType error:nil] : nil; //[NSArray arrayWithObject:attribute]
        
        
        BOOL _isAnyDataUpdated = NO;
        for(NSDictionary *offerDict in _objectsToUpdate)
        {
            
            NSInteger index=[offersToCheckUpdate indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) 
                             {
                                 if([[obj valueForKey:attribute] isEqual:[offerDict valueForKey:attribute]])
                                 {
                                     return YES;
                                     *stop=YES;
                                 }
                                 
                                 return NO;
                                 
                             }];
            
            if(index!=NSNotFound)
            {
                NSManagedObject *fetchedObject=[offersToCheckUpdate objectAtIndex:index];
                
                for(NSString *key in offerDict)
                {
                    SEL _sel=NSSelectorFromString(key);
                    
                    if(![fetchedObject respondsToSelector:_sel]) continue;
                    
                    id newVal=[offerDict valueForKey:key];
                    id oldVal=[fetchedObject valueForKey:key];
                    
                    if(![oldVal isEqual:newVal])
                    {
                        if(!_isAnyDataUpdated) _isAnyDataUpdated = YES;
                        
                        newVal = NULL_NIL(newVal);
                        
                        Class _currentClass = [self class];
                        if([_currentClass  respondsToSelector:@selector(willSetValue:forKey:)])
                        newVal =[_currentClass willSetValue:newVal forKey:key];
                        
                        [fetchedObject setValue:newVal forKey:key];
                    }
                }
                
                if(_isAnyDataUpdated)
                [_updated addObject:fetchedObject];
            }
            
        }
                   
            NSError *error=nil;
            //context should be saved and merged into main context
            [[SSCoreDataManager sharedManager] saveContext:context error:error];
            
            if(error) NSLog(@"Error:%@",[error description]);
            
            //====================== Back to main thread ==========================
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSManagedObjectContext *mainContext = DEFAULT_CONTEXT;
                
              
                NSMutableArray *_insertedResults =  [NSMutableArray array];
                NSMutableArray *_deletedResults  =  [NSMutableArray array];
                NSMutableArray *_updatedResults  =  [NSMutableArray array];
                
                [_inserted enumerateObjectsUsingBlock:^(NSManagedObject * obj, BOOL *stop) 
                {
                    NSManagedObject *_obj = [obj inContext:mainContext];
                    if(_obj)
                    [_insertedResults addObject:_obj];
                    
                    NSLog(@"inserted object:%@",_obj);
                    
                }];
                    
                
                
                [_deleted enumerateObjectsUsingBlock:^(NSManagedObject * obj, BOOL *stop) 
                 {
                     NSManagedObject *_obj = [obj inContext:mainContext];
                     if(_obj)
                         [_deletedResults addObject:_obj];
                     
                     NSLog(@"inserted object:%@",_obj);
                     
                 }];
                
                [_updated enumerateObjectsUsingBlock:^(NSManagedObject * obj, BOOL *stop) 
                 {
                     NSManagedObject *_obj = [obj inContext:mainContext];
                     if(_obj)
                         [_updatedResults addObject:_obj];
                     
                     NSLog(@"inserted object:%@",_obj);
                     
                 }];
                
            
                if(completionBlock) completionBlock(_deletedResults,_insertedResults,_updatedResults,error);
            });
            
        
        
        //====================================================================

        
        [context reset];
        [context release];
    });
    
}

/*
+(void)populateAsyncPerformingDelete:(NSArray *)deleteIds update:(NSArray *)updateIds insertOnArray:(NSArray *)objects onAttributes:(NSArray *)attributes  completion:(void(^)(NSMutableArray *insertedResult,NSMutableArray *updatedResult, NSError *error))completionBlock deletionBlock:(void(^)(NSMutableArray *deletedResult))deletionBlock
{
    if(![objects count] && ![deleteIds count])
    {
        //if(completionBlock) completionBlock(nil,nil,nil);
        
        return;
    }
    
    NSString *attribute = nil;
    for(NSString *_attibuteString in attributes)
    {
        attribute = _attibuteString;
        break;
    }
    
    
    //============= Create a new context with  ============
    NSManagedObjectContext *context=[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType] ;
    [context setParentContext:DEFAULT_CONTEXT];
    [context setUndoManager:nil];
    
    
    NSMutableArray *_allObjects = NULL_NIL(objects)? [NSMutableArray arrayWithArray:objects]:nil;
    
    [context performBlock:^{
        
        NSMutableSet *_deleted  = [NSMutableSet set];
        NSMutableSet *_inserted = [NSMutableSet set];
        NSMutableSet *_updated  = [NSMutableSet set];
        
#pragma mark - PERFORM DELETE
    
        if([deleteIds count])
        {
           NSMutableArray *_deletedObjs = [[self class] deleteObjectsforIDs:deleteIds onAttribute:attribute inContext:context];
        
           if([_deletedObjs count])
           {
              [_deleted addObjectsFromArray:_deletedObjs];
           }
        }
        
#pragma mark - PERFORM INSERT
    
        
        if([_allObjects count])
        {
          NSMutableArray *objectsToUpdate = [[NSMutableArray alloc] init];
            
           NSMutableArray *_insertedObjs =  [[self class] createOrUpdateObjectsFromArray:_allObjects inContext:context checkDuplicateOnAttributes:attributes objectsToUpdate:objectsToUpdate];
            
            if([_insertedObjs count])
            {
               [_inserted addObjectsFromArray:_insertedObjs];
            }
            
#pragma mark - PERFORM UPDATE
            
            if([objectsToUpdate count])
            {
                [_updated addObjectsFromArray:objectsToUpdate];
            }
            
            [objectsToUpdate release];

        }
        
        
        //Save changes to main parent context
        NSError *error=nil;
        if([context hasChanges] && ![context save:&error])
        {
            if(error) NSLog(@"Error:%@",[error description]);
        }
        
    
        //====================== Back to main thread ==========================
       // dispatch_async(dispatch_get_main_queue(), ^{
        
        NSManagedObjectContext *mainContext = DEFAULT_CONTEXT;
        
        [mainContext performBlockAndWait:^{
            
            NSMutableArray *_deletedResults  =  [[NSMutableArray alloc] init];
            NSMutableArray *_insertedResults =  [[NSMutableArray alloc] init];
            NSMutableArray *_updatedResults  =  [[NSMutableArray alloc] init];
            
            [_deleted enumerateObjectsUsingBlock:^(NSManagedObject * obj, BOOL *stop)
             {
                 NSManagedObject *_obj = [obj inContext:mainContext];
                 
                 if(_obj)
                 {
                     [_deletedResults addObject:_obj];
                 }
                 
                 NSLog(@"deleted object:%@",_obj);
                 
             }];
        
            
            if(deletionBlock) deletionBlock(_deletedResults);
            
             [_deletedResults release];
            
            //========NOW SAVE MAIN CONTEXT TO REFLECT CHANGES===========
            NSError *_error=error;
            if([mainContext hasChanges] && ![mainContext save:&_error])
            {
                if(_error) NSLog(@"Error:%@",[_error description]);
            }
            
            
            [_inserted enumerateObjectsUsingBlock:^(NSManagedObject * obj, BOOL *stop)
             {
                 NSManagedObject *_obj = [obj inContext:mainContext];
                 
                 if(_obj)
                 {
                     [_insertedResults addObject:_obj];
                 }
                 
                 NSLog(@"inserted object:%@",_obj);
                 
             }];
            
            
            [_updated enumerateObjectsUsingBlock:^(NSManagedObject * obj, BOOL *stop)
             {
                 NSManagedObject *_obj = [obj inContext:mainContext];
                 
                 if(_obj)
                 {
                     [_updatedResults addObject:_obj];
                 }
                 
                 NSLog(@"updated object:%@",_obj);
                 
             }];
            
            
            if(completionBlock) completionBlock(_insertedResults,_updatedResults,_error);
            
            [_insertedResults release];
            [_updatedResults release];

       // });
        }];
        
        
        [context reset];
        [context release];
    }];
}
*/

+(void)populateAsyncPerformingDelete:(NSArray *)deleteIds update:(NSArray *)updateIds insertOnArray:(NSArray *)objects onAttributes:(NSArray *)attributes  completion:(void(^)(NSMutableArray *insertedResult,NSMutableArray *updatedResult, NSError *error))completionBlock deletionBlock:(void(^)(NSMutableArray *deletedResult))deletionBlock
{
    if(![objects count] && ![deleteIds count])
    {
        //if(completionBlock) completionBlock(nil,nil,nil);
        
        return;
    }
    
    NSString *attribute = nil;
    for(NSString *_attibuteString in attributes)
    {
        attribute = _attibuteString;
        break;
    }
    
    
    //============= Create a new context with  ============
    NSManagedObjectContext *context=[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType] ;
    [context setParentContext:DEFAULT_CONTEXT];
    [context setUndoManager:nil];
    
    
    NSMutableArray *_allObjects = NULL_NIL(objects)? [NSMutableArray arrayWithArray:objects]:nil;
    
    [context performBlock:^{
        
        NSMutableArray *_deleted  = nil;
        NSMutableArray *_inserted = nil;
        NSMutableArray *_updated  = [NSMutableArray array];
        
#pragma mark - PERFORM DELETE
        
        if([deleteIds count])
        {
            _deleted = [[self class] deleteObjectsforIDs:deleteIds onAttribute:attribute inContext:context];
        }
        
#pragma mark - PERFORM INSERT
        
        
        if([_allObjects count])
        {
            _inserted =  [[self class] createOrUpdateObjectsFromArray:_allObjects inContext:context checkDuplicateOnAttributes:attributes objectsToUpdate:_updated];
            
#pragma mark - PERFORM UPDATE
            
        }
        
        
        //Save changes to main parent context
        NSError *error=nil;
        if([context hasChanges] && ![context save:&error])
        {
            if(error) NSLog(@"Error:%@",[error description]);
        }
        
        
        //====================== Back to main thread ==========================
        // dispatch_async(dispatch_get_main_queue(), ^{
        
        NSManagedObjectContext *mainContext = DEFAULT_CONTEXT;
        
        [mainContext performBlockAndWait:^{
            
            NSMutableArray *_deletedResults  =  [[NSMutableArray alloc] init];
            NSMutableArray *_insertedResults =  [[NSMutableArray alloc] init];
            NSMutableArray *_updatedResults  =  [[NSMutableArray alloc] init];
            
            [_deleted enumerateObjectsUsingBlock:^(NSManagedObject *obj, NSUInteger idx, BOOL *stop)
             {
                 NSManagedObject *_obj = [obj inContext:mainContext];
                 
                 if(_obj)
                 {
                     [_deletedResults addObject:_obj];
                 }
                 
                 NSLog(@"deleted object:%@",_obj);
                 
             }];
            
            
            if(deletionBlock) deletionBlock(_deletedResults);
            
            [_deletedResults release];
            
            //========NOW SAVE MAIN CONTEXT TO REFLECT CHANGES===========
            NSError *_error=error;
            if([mainContext hasChanges] && ![mainContext save:&_error])
            {
                if(_error) NSLog(@"Error:%@",[_error description]);
            }
            
            
            [_inserted enumerateObjectsUsingBlock:^(NSManagedObject *obj, NSUInteger idx, BOOL *stop)
             {
                 NSManagedObject *_obj = [obj inContext:mainContext];
                 
                 if(_obj)
                 {
                     [_insertedResults addObject:_obj];
                 }
                 
                 NSLog(@"inserted object:%@",_obj);
                 
             }];
            
            
            [_updated enumerateObjectsUsingBlock:^(NSManagedObject *obj, NSUInteger idx, BOOL *stop)
             {
                 NSManagedObject *_obj = [obj inContext:mainContext];
                 
                 if(_obj)
                 {
                     [_updatedResults addObject:_obj];
                 }
                 
                 NSLog(@"updated object:%@",_obj);
                 
             }];
            
            
            if(completionBlock) completionBlock(_insertedResults,_updatedResults,_error);
            
            [_insertedResults release];
            [_updatedResults release];
            
            // });
        }];
        
        
        [context reset];
        [context release];
    }];
}

+(void)populateAsyncPerformingDelete:(NSArray *)deleteIds update:(NSArray *)updateIds insertOnArray:(NSArray *)objects onAttribute:(NSString *)attribute  completion:(void(^)(NSMutableArray *insertedResult,NSMutableArray *updatedResult, NSError *error))completionBlock deletionBlock:(void(^)(NSMutableArray *deletedResult))deletionBlock

{
    [self populateAsyncPerformingDelete:deleteIds update:updateIds insertOnArray:objects onAttributes:[NSArray arrayWithObject:attribute] completion:completionBlock deletionBlock:deletionBlock];
}

+(void)populateAsyncPerformingDelete:(NSArray *)deleteIds insertOnArray:(NSArray *)objects onAttributes:(NSArray *)attributes  completion:(void(^)(NSMutableArray *insertedResult,NSMutableArray *updatedResult, NSError *error))completionBlock deletionBlock:(void(^)(NSMutableArray *deletedResult))deletionBlock
{
     [self populateAsyncPerformingDelete:deleteIds update:nil insertOnArray:objects onAttributes:attributes completion:completionBlock deletionBlock:deletionBlock];
}

@end
#pragma mark -


@implementation NSFetchRequest (CoreDataManager)

+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors attributes:(NSArray *)attributes limit:(NSUInteger)limit
{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    if(entity)
        [fetchRequest setEntity:entity];
    
    if(predicate)
        [fetchRequest setPredicate:predicate];
    
    if(sortDescriptors)
        [fetchRequest setSortDescriptors:sortDescriptors];
    
    if(attributes)
        [fetchRequest setPropertiesToFetch:attributes];
    
    if(limit>0)
        [fetchRequest setFetchLimit:limit];
    
    return [fetchRequest autorelease];
}

+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit
{
    return [NSFetchRequest fetchRequestWithEntity:entity predicate:predicate sortDescriptors:sortDescriptors attributes:nil limit:limit];
}

+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors attributes:(NSArray *)attributes
{
    return [NSFetchRequest fetchRequestWithEntity:entity predicate:predicate sortDescriptors:sortDescriptors attributes:attributes limit:0];
}


+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors
{
    
    return [self fetchRequestWithEntity:entity predicate:predicate sortDescriptors:nil limit:0];
}


+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate {
    
    return [self fetchRequestWithEntity:entity predicate:predicate sortDescriptors:nil];
}

+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity {
    
    return [self fetchRequestWithEntity:entity predicate:nil];
}



@end

