//
//  NSManagedObject+Fieldforce.h
//  Fieldforce
//
//  Created by Randem IT on 28/10/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "Customer.h"
#import "CustomerPhoto.h"
#import "ScheduledVisit.h"
#import "ResourceFolder.h"
#import "ResourceMaster.h"
#import "Visit.h"

@interface NSManagedObject (Fieldforce)

- (BOOL)isReallyDeleted;

@end

@interface Customer (Fieldforce)

-(NSString *)fullAddress;
-(NSString *)getCategoryName;
-(BOOL)isDealingWithAnyOEM;

@property(nonatomic,readonly) NSDate *lastVisitDate;
@property(nonatomic,readonly) NSDate *nextVisitDate;

@end

@interface CustomerPhoto (Fieldforce)

-(NSMutableDictionary *)uploadDict;
-(void)prepareForDeletion;

@end

@interface ScheduledVisit (Fieldforce)

+(id)willSetValue:(id)value forKey:(id)key;
-(void)willDelete;

@end

@interface ResourceFolder (Fieldforce)

-(BOOL)isEmpty;

-(NSSet *)publishedResources;

@end

@interface ResourceMaster (Fieldforce)

+(NSString *)keyToCheckDuplicate;
-(NSString *)resourceFileName;
-(NSString *)fileSizeString;

@end

@interface  Visit (Fieldforce)

-(NSString *)startEndTimeString;

@end