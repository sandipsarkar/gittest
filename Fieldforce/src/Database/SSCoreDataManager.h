//
//  SSCoreDataManager.h
//  CoreDataBooks
//
//  Created by RANDEM MAC on 01/12/11.
//  Copyright (c) 2011 RANDEM IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define DEFAULT_CONTEXT [SSCoreDataManager defaultContext]

//typedef void (^ContextDidSave)(NSManagedObjectContext *context);

#if NS_BLOCKS_AVAILABLE
//typedef void(^CD_CompletionBlock)(NSSet *result, NSError *error);
typedef void(^ChangeContentblock)(NSFetchedResultsController *controller);

typedef void(^DidChangeObject)(NSFetchedResultsController *controller, id object ,NSIndexPath *indexPath,NSFetchedResultsChangeType type,NSIndexPath *newIndexPath);

typedef void(^DidChangeSectionBlock)(NSFetchedResultsController *controller, id <NSFetchedResultsSectionInfo>sectionInfo ,NSUInteger sectionIndex,NSFetchedResultsChangeType type);
#endif


@interface SSCoreDataManager : NSObject
{
    NSManagedObjectModel         * managedObjectModel;
    NSManagedObjectContext       * defaultManagedObjectContext;	    
    NSPersistentStoreCoordinator * persistentStoreCoordinator;
    
#if NS_BLOCKS_AVAILABLE
@private
    ChangeContentblock    _willChangeContentBlock;
    ChangeContentblock    _didChangeContentBlock;
    DidChangeObject       _didChangeObjectBlock;
    DidChangeSectionBlock _didChangeSectionBlock;
   // CD_CompletionBlock _completionBlock;
#endif
    
}

@property (nonatomic, retain) NSManagedObjectModel * managedObjectModel;
@property (nonatomic, retain) NSManagedObjectContext * defaultManagedObjectContext;
@property (nonatomic, retain) NSPersistentStoreCoordinator * persistentStoreCoordinator;

@property (nonatomic,copy) NSString *dbStorePath;
@property (nonatomic,copy) NSString *defaultDBStorePath;

-(id)init;
+(SSCoreDataManager *)sharedManager;
+(NSManagedObjectContext *)defaultContext;
+(NSManagedObjectContext *)backgroundContext;
+(NSManagedObjectContext *)backgroundContextWithConcurrencyType:(NSManagedObjectContextConcurrencyType)concurrencyType;
+(void)destroyBackgroundContext:(NSManagedObjectContext *)context;


-(void)performInDefaultContext:(void(^)(NSManagedObjectContext *context))operation completion:(void(^)(NSSet *result, NSError *error))completionBlock;

-(void)performAsyncOperation:(void(^)(NSManagedObjectContext *context))operation completion:(void(^)(NSSet *result, NSError *error))completionBlock;

/*
 Perform fetch operation asynchronously
*/
+(void)performAsyncFetchRequest:(NSFetchRequest *)aFetchRequest completion:(void(^)(NSFetchRequest *fetchRequest, NSArray *result, NSError *error))completionBlock;

-(void)registerContextDidSaveNotificationOnContext:(NSManagedObjectContext *)context;
-(void)removeContextDidSaveNotificationOnContext:(NSManagedObjectContext *)context;

-(void)save:(NSError *)error;
-(void)saveContext:(NSManagedObjectContext *)context error:(NSError *)error;

-(NSManagedObject *)objectWithID:(NSManagedObjectID *)objectId inContext:(NSManagedObjectContext *)context;

//- (void)updateDefaultContext:(NSNotification *)notification;

@end

#pragma mark - Migration
@interface SSCoreDataManager (Migration)

+(BOOL)performMigrationWithSourceMetadata :(NSDictionary *)sourceMetadata toDestinationModel:(NSManagedObjectModel *)destinationModel fromSourcePath:(NSString *)sourcePath toDestinationPath:(NSString *)destinationPath;

+(BOOL)checkForMigrationAtTargetPath:(NSString *)targetPath sourceStorePath:(NSString *)sourcePath targetObjectModel:(NSManagedObjectModel *)targetObjectModel onMigrationCallback:(void(^)(BOOL success))callback;

+(BOOL)isModelCompatibleWithPersistentStoreAtURL:(NSURL *)storeURL forPersistentStoreCoordinator:(NSPersistentStoreCoordinator *)persistentCoordinator;




+(BOOL)performMigrationWithSourceMetadata :(NSDictionary *)sourceMetadata toDestinationModel:(NSManagedObjectModel *)destinationModel fromSourcePath:(NSString *)sourcePath toDestinationPath:(NSString *)destinationPath mappingModels:(NSArray *)mapingModelNames;

@end

#pragma mark - DELETE
@interface SSCoreDataManager (Delete)

+(void)deleteObject:(NSManagedObject *)object inContext:(NSManagedObjectContext *)context;
+(void)deleteObject:(NSManagedObject *)object;

@end

#pragma mark - NSFetchedResultsController
@interface SSCoreDataManager(NSFetchedResultsController)<NSFetchedResultsControllerDelegate>

- (NSFetchedResultsController *)fetchedResultsControllerWithFetchRequest:(NSFetchRequest *)aFetchRequest;

#if NS_BLOCKS_AVAILABLE 
-(void)addFetchedResultsControllerWillChangeContent:(ChangeContentblock)block;
-(void)addFetchedResultsControllerDidChangeContent:(ChangeContentblock)block;
-(void)addFetchedResultsControllerDidChangeObject:(DidChangeObject)block;
-(void)addFetchedResultsControllerDidChangeSection:(DidChangeSectionBlock)block;
#endif

@end

@interface NSManagedObject (NSManagedObject_Dictionary)

@property (nonatomic ,assign) BOOL traversed;

+ (NSManagedObject*) createObjectFromDictionary:(NSDictionary*)dict
                                             inContext:(NSManagedObjectContext*)context;
- (BOOL) populateFromDictionary:(NSDictionary*)dict;
- (BOOL) populateFromDictionary:(NSDictionary*)dict onContext:(NSManagedObjectContext *)newContext;
- (NSDictionary*) toDictionary;

+(NSMutableArray *)createOrUpdateObjectsFromArray:(NSArray *)objects inContext:(NSManagedObjectContext *)context checkDuplicateOnAttributes:(NSArray *)attributes objectsToUpdate:(NSMutableArray *)objectsToUpdate;

+(NSMutableArray *)checkOrCreateObjectsFromArray:(NSArray *)objects inContext:(NSManagedObjectContext *)context checkDuplicateOnAttribulte:(NSString *)attribute IDsToUpdate:(NSMutableArray *)idsToUpdate;


//============== SHOULD BE IMPLEMENTED BY NSMANAGED OBJECT CLASS ===========
+(id)willSetValue:(id)value forKey:(id)key;
+(void)willDelete:(id)obj;
+(NSString *)keyToCheckDuplicate;

@end


@interface NSManagedObject (OPERATION)

+(NSEntityDescription *)entity;

- (NSManagedObject *)inContext:(NSManagedObjectContext*)context;

+(NSManagedObject *)insertNewObjectInContext:(NSManagedObjectContext *)context;
+(NSManagedObject *)insertNewObject;


//-(BOOL)updateWithDictionary:(NSDictionary *)dict inContext:(NSManagedObjectContext *)context;

+(void)populateAsyncPerformingDelete:(NSArray *)deleteIds update:(NSArray *)updateIds insertOnArray:(NSArray *)objects onAttribute:(NSString *)attribute  completion:(void(^)(NSMutableArray *deletedResult,NSMutableArray *insertedResult,NSMutableArray *updatedResult, NSError *error))completionBlock  NS_DEPRECATED_IOS(4_0, 7_0);


+(void)populateAsyncPerformingDelete:(NSArray *)deleteIds update:(NSArray *)updateIds insertOnArray:(NSArray *)objects onAttributes:(NSArray *)attributes  completion:(void(^)(NSMutableArray *insertedResult,NSMutableArray *updatedResult, NSError *error))completionBlock deletionBlock:(void(^)(NSMutableArray *deletedResult))deletionBlock;

+(void)populateAsyncPerformingDelete:(NSArray *)deleteIds update:(NSArray *)updateIds insertOnArray:(NSArray *)objects onAttribute:(NSString *)attribute  completion:(void(^)(NSMutableArray *insertedResult,NSMutableArray *updatedResult, NSError *error))completionBlock deletionBlock:(void(^)(NSMutableArray *deletedResult))deletionBlock;

+(void)populateAsyncPerformingDelete:(NSArray *)deleteIds insertOnArray:(NSArray *)objects onAttributes:(NSArray *)attributes  completion:(void(^)(NSMutableArray *insertedResult,NSMutableArray *updatedResult, NSError *error))completionBlock deletionBlock:(void(^)(NSMutableArray *deletedResult))deletionBlock;

+(NSMutableArray *)deleteObjectsforIDs:(NSArray *)deleteIds onAttribute:(NSString *)attribute inContext:(NSManagedObjectContext *)context;

+(void)asyncDeleteObjectsforIDs:(NSArray *)objects onAttribute:(NSString *)attribute completion:(void(^)(NSMutableArray *result, NSError *error))completionBlock;


-(void)willDelete;

@end

#pragma mark - Fetch
@interface NSManagedObject (Fetch)

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit startIndex:(NSUInteger)offset resultType:(NSFetchRequestResultType)type error:(NSError *)error;

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit startIndex:(NSUInteger)offset error:(NSError *)error;

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit error:(NSError *)error;

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate error:(NSError *)error;

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes error:(NSError *)error;

+(NSMutableArray *)fetchOnContext:(NSManagedObjectContext *)context predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors error:(NSError *)error;

+(NSMutableArray *)fetchWithPredicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit error:(NSError *)error;

+(NSMutableArray *)fetchWithPredicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit error:(NSError *)error;

+(NSMutableArray *)fetchWithPredicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors error:(NSError *)error;

+(NSMutableArray *)fetchWithPredicate:(NSPredicate *)predicate error:(NSError *)error;
+(NSMutableArray *)fetchAll:(NSError *)error;

#pragma mark - Async fetch
+(void)fetchAsynWithPredicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit startIndex:(NSUInteger)offset resultType:(NSFetchRequestResultType)type completion:(void(^)(NSArray *result,NSError *error))completionBlock;

+(void)fetchAsynWithPredicate:(NSPredicate *)predicate onAttributes:(NSArray *)attributes sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit startIndex:(NSUInteger)offset completion:(void(^)(NSArray *result,NSError *error))completionBlock;

+(void)fetchAsynWithPredicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors  completion:(void(^)(NSArray *result,NSError *error))completionBlock;

+(void)fetchAsynWithPredicate:(NSPredicate *)predicate completion:(void(^)(NSArray *result,NSError *error))completionBlock;

+(void)fetchAsynWithPredicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit  completion:(void(^)(NSArray *result,NSError *error))completionBlock;

@end

#pragma mark - FetchRequest
@interface NSFetchRequest (CoreDataManager)

+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors attributes:(NSArray *)attributes limit:(NSUInteger)limit;

+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors limit:(NSUInteger)limit;

+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors attributes:(NSArray *)attributes;

+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors;

+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity predicate:(NSPredicate *)predicate ;

+ (NSFetchRequest *)fetchRequestWithEntity:(NSEntityDescription *)entity;


@end
