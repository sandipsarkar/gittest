//
//  MenuViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 22/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "MenuViewController.h"
#import "DashboardViewController.h"
#import "CustomersViewController.h"
#import "CustomerOfferViewController.h"
#import "OrganizeViewController.h"
#import "AccountSettingsViewController.h"
#import "CalendarViewController.h"
#import "OfflineManager.h"
#import "SSTabBarController.h"
#import "FeedBackViewController.h"
#import "WhatsnewOverlayView.h"
#import "ResourcesViewController.h"


@interface MenuViewController()<SSTabBarControllerDelegate>
{
    UIButton *whatsNewButton;
}

@property(nonatomic,retain)NSIndexPath *currentIndexPath;
@property(nonatomic,retain)UIPopoverController *settingsPopovercontroller;
@end

@implementation MenuViewController
@synthesize currentIndexPath;
@synthesize settingsPopovercontroller;

-(void)dealloc
{
    [currentIndexPath release];
    [settingsPopovercontroller release];
    [visitingCustomerLabel release];
    settingsPopovercontroller = nil;
    
    //if(_dataDict)[_dataDict release];
    
    [super dealloc];
}
//OVERRIDING FROM PARENT
-(void)setupDataDict:(NSMutableDictionary *)dict
{
        NSArray *sec1Items=[NSArray arrayWithObjects:@"Customers",@"Offers", nil];
        [dict setObject:sec1Items forKey:@"1"];
        
        NSArray *sec2Items=[NSArray arrayWithObjects:@"Task List",@"Visit Reviews", nil];
        [dict setObject:sec2Items forKey:@"2"];
        
        [dict setObject:@"DASHBOARD"         forKey:@"0_title"];
        [dict setObject:@"CUSTOMERS"         forKey:@"1_title"];
        [dict setObject:@"ORGANISE"          forKey:@"2_title"];
        [dict setObject:@"RESOURCES"          forKey:@"3_title"];
        [dict setObject:@"CALENDAR"          forKey:@"4_title"];
        [dict setObject:@"ACCOUNT SETTINGS"  forKey:@"5_title"];
        //[_dataDict setObject:@"VERSION" forKey:@"5_title"];
}



-(void)showVisitingCustomerName:(NSString *)_customerName
{
   // if(!_customerName.length) return;
    
    //visitingCustomerLabel.text=[_customerName uppercaseString];
    //VisitingCustomerImageView.hidden=!_customerName.length;
}



#pragma mark select viewController according to indexPath. OVERRIDING FROM PARENT
-(void)selectViewControllerForIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"load vc for indexPath->row:%d ->section:%d",indexPath.row,indexPath.section);
    
    [self  showVisitingCustomerName:nil];
    
   // if(self.currentIndexPath.section!=indexPath.section && [_dataDict objectForKey:indexPath])
   // [_dataDict removeObjectForKey:indexPath];
  
    switch (indexPath.section)
    {
        case 0:
        {
            NSLog(@"Dashboard");
             
            /*
            UINavigationController *navController = [_dataDict objectForKey:indexPath];
            if(!navController) 
            {
                DashboardViewController *dashboardViewController  = [[DashboardViewController alloc] init];
                navController=[[UINavigationController alloc] initWithRootViewController:dashboardViewController];
                [_dataDict setObject:navController forKey:indexPath];
                [dashboardViewController release];
                [navController release];
            }
            
            [navController popViewControllerAnimated:NO];

            #if CUSTOM_SPLITSVC
            [app.splitViewController showDetailController:navController animated:NO];
            #else
            [app.splitViewController setDetailController:navController isNavigationController:NO];
            #endif
            */
            
            DashboardViewController *dashboardViewController  = [[DashboardViewController alloc] init];
            UINavigationController *navController =[[UINavigationController alloc] initWithRootViewController:dashboardViewController];
#if CUSTOM_SPLITSVC
            [app.splitViewController showDetailController:navController animated:NO];
#else
            [app.splitViewController setDetailController:navController isNavigationController:NO];
#endif
            [navController release];
            [dashboardViewController release];
        }
        break;
            
        case 1:
        {
            switch (indexPath.row) 
            {
                case 1:
                {
                    NSLog(@"Customers");
                    app.fromCustomerTask=1;

                        CustomersViewController *customersViewController   = [[CustomersViewController alloc] init];
                        UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:customersViewController];
                        // customersViewController.isSearching=NO;
                        #if CUSTOM_SPLITSVC
                        [app.splitViewController showDetailController:navController animated:NO];
                        #else
                        [app.splitViewController setDetailController:navController isNavigationController:NO];
                        #endif

                        [navController release];
                        [customersViewController release];
                }
                break;
                    
                case 2:
                {
                    NSLog(@"Offers");
  
                    CustomerOfferViewController *customerOfferViewController   = [[CustomerOfferViewController alloc] initWithOfferType:CategoryTypeDefault];
                    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:customerOfferViewController];
                   // [_dataDict setObject:navController forKey:indexPath];

                    #if CUSTOM_SPLITSVC
                    [app.splitViewController showDetailController:navController animated:NO];
                    #else
                    [app.splitViewController setDetailController:navController isNavigationController:NO];
                    #endif

                    [navController release];
                    [customerOfferViewController release];

                }
                break;
                    
                default: break;
            }
        }
            
            break;
        case 2://Organize
        {
             app.fromCustomerTask=2;
            NSIndexPath *_indexPath = (indexPath.row==2) ? [NSIndexPath indexPathForRow:1 inSection:indexPath.section] : indexPath;
            
            
            UINavigationController *navController= [_dataDict objectForKey:_indexPath];
            if(!navController) 
            {
               
                OrganizeViewController *organizeViewController = [[OrganizeViewController alloc]initWithCustomer:nil];
                navController=[[UINavigationController alloc] initWithRootViewController:organizeViewController];
                organizeViewController.parentTabbarControlller.delegate=self;
                [_dataDict setObject:navController forKey:_indexPath];
                [organizeViewController release];
                [navController release];
                //[app.splitViewController setDetailController:organizeViewController isNavigationController:YES]; 
            }
            
            if(self.currentIndexPath.section!=_indexPath.section)
            {
                #if CUSTOM_SPLITSVC
                [app.splitViewController showDetailController:navController animated:NO];
                #else
                [app.splitViewController setDetailController:navController isNavigationController:NO];
                #endif
            }
     
            NSArray *vcStack = [navController viewControllers];
            if([vcStack count])
            {
                OrganizeViewController *organizeViewController=(OrganizeViewController *) [vcStack objectAtIndex:0];
                
                 organizeViewController .parentTabbarControlller.selectedIndex=indexPath.row-1;
            }
    
        }
            
        break;
            
        case 3: //RESOURCE
        {
            ResourcesViewController *resourcesViewController  = [[ResourcesViewController alloc] init];
            UINavigationController *navController =[[UINavigationController alloc] initWithRootViewController:resourcesViewController];
            #if CUSTOM_SPLITSVC
            [app.splitViewController showDetailController:navController animated:NO];
            #else
            [app.splitViewController setDetailController:navController isNavigationController:NO];
             #endif
            [navController release];
            [resourcesViewController release];
        }
        break;
            
        case 4:
        {
            NSLog(@"Calendar");
            
                CalendarViewController *calendarViewController   = [[CalendarViewController alloc] init];
                calendarViewController.title = @"Calendar";
                UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:calendarViewController];

                 [app.splitViewController presentViewController:navController animated:YES completion:nil];
                [navController release];
                [calendarViewController release];
        }
        break;
            
        case 5:
        {
            NSLog(@"Settings");
            
            AccountSettingsViewController *accountSettingsViewController   = [[AccountSettingsViewController alloc] init];
            accountSettingsViewController.title = @"Settings";
            UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:accountSettingsViewController];


            #if CUSTOM_SPLITSVC
            [app.splitViewController showDetailController:navController animated:NO];
            #else
            [app.splitViewController setDetailController:navController isNavigationController:NO];
            #endif

            [accountSettingsViewController release];
            [navController release];
        }
        break;
            
        default: break;
    }
    
    if(indexPath.section!=CALENDAR_SECTION)
    self.currentIndexPath=indexPath;
    
    //[app performSelector:@selector(fixRoundedSplitViewCorner) withObject:NULL afterDelay:0];
    
}
#pragma mark -


- (BOOL)tabBarController:(SSTabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController atIndex:(int)index
{
    if(self.currentIndexPath.section==2)//ONLY ORGANIZE SECTION
    {
      //IF ALREADY SELECTED RETURN
      if(self.currentIndexPath.row-1==index) return YES;
        
      NSUInteger selectedRow = index+1;
    
      NSIndexPath *indexPath=[NSIndexPath indexPathForRow:selectedRow inSection:self.currentIndexPath.section];

     //BUG
      [self selectCellAtIndexPath:indexPath];
        
      self.currentIndexPath=indexPath;
    }
    
    
    return YES;
}

-(void)setupFooterView
{
    float footerHeight=CGRectGetHeight(self.view.bounds);

    
    UIButton *settingsButton=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(10.0, footerHeight-183.0, 300.0, 44.0) bgImage:@"account_settings_button.png" titleColor:nil target:self action:@selector(settingsAction:)];
  
    settingsButton.autoresizingMask=UIViewAutoresizingFlexibleTopMargin;
    [settingsButton setBackgroundImage:[UIImage imageNamedNoCache:@"account_settings_button.png"] forState:UIControlStateHighlighted];
    [self.view addSubview:settingsButton];
    
    float locY=CGRectGetMaxY(settingsButton.frame)+20;
    UIButton *feedBackButton=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(10.0, locY, 300.0, 44.0) bgImage:@"feedback_button.png" titleColor:nil target:self action:@selector(feedBackAction:)];
    feedBackButton.autoresizingMask=UIViewAutoresizingFlexibleTopMargin;
    [feedBackButton setBackgroundImage:[UIImage imageNamedNoCache:@"feedback_button.png"]  forState:UIControlStateHighlighted];
    [self.view addSubview:feedBackButton];
    
    float _versionLabelWidth  = 240; //330
    float _versionLabelHeight = 20;
    
NSString *envName = @"";
    
#if  LOCAL
    envName=@"Local";
#elif  LOCAL2
    envName=@"Local2";
#elif GENERIC_DEMO
    envName=@"Demo";
#elif LIVE_TEST
    envName=@"Test";
#else
    envName = @"";
#endif

    
    UIImage *whatsNewImage = [UIImage imageNamed:@"whats_new_button.png"];
    
    UILabel* versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, locY+58, _versionLabelWidth, _versionLabelHeight)];
    versionLabel.text = [NSString stringWithFormat:@"%@  version %@",envName, APP_VERSION];
    versionLabel.font = [UIFont grotesqueFontOfSize:12.0];
    versionLabel.autoresizingMask=UIViewAutoresizingFlexibleTopMargin;//|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    versionLabel.textColor = [UIColor darkGrayColor];
    versionLabel.textAlignment = NSTextAlignmentCenter;
    versionLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:versionLabel];
    [versionLabel release];

    whatsNewButton = [UIButton buttonWithType:UIButtonTypeCustom];
    whatsNewButton.backgroundColor = [UIColor clearColor];
    whatsNewButton.frame = CGRectMake(57, locY+60, whatsNewImage.size.width, whatsNewImage.size.height);
    [whatsNewButton setBackgroundImage:whatsNewImage forState:UIControlStateNormal];
    whatsNewButton.autoresizingMask=UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:whatsNewButton];
    [whatsNewButton addTarget:self action:@selector(whatsNewPageAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self manageVisibilityOfwhatsNew];
    
    //---------- AUTOMATICALLY SHOW WHAT'S NEW PAGE ONCE AT FIRST TIME AFTER LOGIN ------------------
    BOOL _isVersionUpdated =   [[NSUserDefaults standardUserDefaults] boolForKey:BUILD_VERSION_UPDATED];

    if(_isVersionUpdated && !whatsNewButton.hidden)
    {
        [self performSelector:@selector(whatsNewPageAction:) withObject:nil afterDelay:0.4];
        
         [[NSUserDefaults standardUserDefaults] setBool:NO forKey:BUILD_VERSION_UPDATED];
    }

    /*
    VisitingCustomerImageView=[[UIImageView alloc] initWithFrame:CGRectMake(10.0, footerHeight-250.0, 300.0, 100.0)];
    VisitingCustomerImageView.image=[UIImage imageNamed:@"currently_visiting_customer.png"];
    VisitingCustomerImageView.autoresizingMask=UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:VisitingCustomerImageView];
    
    visitingCustomerLabel=[[UILabel alloc ] initWithFrame:CGRectMake(10.0, 45.0, CGRectGetWidth(VisitingCustomerImageView.frame)-20.0, 45.0)];
    visitingCustomerLabel.textColor = [UIColor darkGrayColor];
    visitingCustomerLabel.font = [UIFont boldSystemFontOfSize:15.0];
    visitingCustomerLabel.numberOfLines=2;
    visitingCustomerLabel.textAlignment = UITextAlignmentCenter;
    visitingCustomerLabel.autoresizingMask=UIViewAutoresizingFlexibleTopMargin;
    visitingCustomerLabel.backgroundColor=[UIColor clearColor];
    [VisitingCustomerImageView addSubview:visitingCustomerLabel];
    VisitingCustomerImageView.hidden=YES;
    */
    
}

#if defined(__IPHONE_7_0)

#pragma mark - WHATS NEW ACTIO
-(void)whatsNewPageAction:(UIButton *)sender
{

    WhatsnewOverlayView *whatsNewAlert = [[WhatsnewOverlayView alloc] initWithParentView:app.window.rootViewController.view];
    [whatsNewAlert show];
    [whatsNewAlert release];
    
}
 
-(void)manageVisibilityOfwhatsNew
{
    const double _visibleDuration = 60*60*24*7;
    NSNumber *lastUpdatedTimeInterval  = [[NSUserDefaults standardUserDefaults] objectForKey:  LAST_VERSION_UPDATED_DATE]; 
    
    NSTimeInterval _currentTimeInterval = [[[NSDate date] dateEnd] GMTTimeIntervalSince1970];
    //NSTimeInterval _currentTimeInterval = [[NSDate date] GMTTimeIntervalSince1970];
    
    NSLog(@"Interval = %lf",(_currentTimeInterval-[lastUpdatedTimeInterval doubleValue]));
    
    BOOL shouldHide = (_currentTimeInterval-[lastUpdatedTimeInterval doubleValue]>_visibleDuration);
    whatsNewButton.hidden = shouldHide;
}
#endif

-(void)feedBackAction:(UIButton *)sender
{
    BOOL _isNetworkAvailable = [[OfflineManager sharedManager] isNetworkAvailable];
    
    if (_isNetworkAvailable)
    {
        FeedBackViewController *feedViewController = [[FeedBackViewController alloc] init];
        UINavigationController  *navController=[[UINavigationController alloc] initWithRootViewController:feedViewController];
        navController.modalPresentationStyle=UIModalPresentationPageSheet;
        navController.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
      //  [app.splitViewController presentModalViewController:navController animated:YES];
        [app.splitViewController presentViewController:navController animated:YES completion:nil];
        [navController release];
        [feedViewController release];
    }
    else
    {
        [UIAlertView showWarningAlertWithTitle:nil message:FEEDBACK_CANNOT_BE_SENT_DUE_TO_UNAVAILABILITY_OF_NETWORK];
    }
}

/*
-(void)feedBackAction:(UIButton *)sender
{
    FeedBackViewController *feedViewController = [[FeedBackViewController alloc] init];
    UINavigationController  *navController=[[UINavigationController alloc] initWithRootViewController:feedViewController];
    navController.modalPresentationStyle=UIModalPresentationPageSheet;
    navController.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
    [app.splitViewController presentModalViewController:navController animated:YES];
    [navController release];
    [feedViewController release];
}
*/

-(void)settingsAction:(UIButton *)sender
{
    UIPopoverController *popover=self.settingsPopovercontroller;
    
    if(!popover)
    {
        AccountSettingsViewController *accountSettingsViewController   = [[AccountSettingsViewController alloc] init];
            accountSettingsViewController.contentSizeForViewInPopover=CGSizeMake(320, 320);

        UINavigationController  *navController=[[UINavigationController alloc] initWithRootViewController:accountSettingsViewController];
        navController.navigationBar.barStyle = UIBarStyleBlack;

        popover=[[UIPopoverController alloc] initWithContentViewController:navController];
        //UIPopoverController *popover=[navController presentInPopOverFr om:sender inView:self.view animated:YES arrowDirection:UIPopoverArrowDirectionDown];
        self.settingsPopovercontroller=popover;
        accountSettingsViewController.popoverController = popover;
        
        /*
        SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")
        popover.popoverBackgroundViewClass = [MBPopoverBackgroundView class];
        #endif
        */
        
        [popover release];
        [accountSettingsViewController release];
        [navController release];
       
    }
  
    [popover presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif
    
    [self setupFooterView];
    
}





- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
