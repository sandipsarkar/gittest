//
//  MenuViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 22/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "MasterMenuViewController.h"

@interface MenuViewController : MasterMenuViewController
{
    UILabel *visitingCustomerLabel;
    UIImageView *VisitingCustomerImageView;
}

-(void)showVisitingCustomerName:(NSString *)_customerName;


@end
