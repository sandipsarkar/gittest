//
//  MasterMenuViewController.h
//  TestOEM
//
//  Created by RANDEM MAC on 15/05/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CALENDAR_SECTION 4

@interface MasterMenuViewController : UITableViewController
{
    NSMutableDictionary *_dataDict;
}

-(void)selectSection:(NSUInteger)aSection;
-(void)selectIndexPath:(NSIndexPath *)indexPath;
-(void)selectCellAtIndexPath:(NSIndexPath *)indexPath;
-(NSIndexPath *)selectedIndexPath;

-(void)setupDataDict:(NSMutableDictionary *)dict;
-(void)selectViewControllerForIndexPath:(NSIndexPath *)indexPath;



@end
