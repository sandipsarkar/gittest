//
//  MasterMenuViewController.m
//  TestOEM
//
//  Created by RANDEM MAC on 15/05/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import "MasterMenuViewController.h"
#include <QuartzCore/QuartzCore.h>
#import "UIImageView+LazyLoading.h"
#import "Rep.h"
#import "UIImage+Resize.h"
#import "OfflineStore.h"
#import "InsetTableViewCell.h"
/*
#import "CustomersViewController.h"
#import "CustomerOfferViewController.h"
#import "DashboardViewController.h"
#import "AccountSettingsViewController.h"
#import "CalendarViewController.h"
#import "OrganizeViewController.h"
*/

#define LOGO_IMAGE_NAME @"default_logo.png" // @"header_logo.png"

//@"autoprofiler.png"

#define DEFAULT_CELL_HEADER_COLOR LOGIN_BG_COLOR// [UIColor colorWithRed:0.1 green:0.3 blue:0.7 alpha:1]
#define SELECTED_CELL_HEADER_COLOR [UIColor darkGrayColor]

@interface MasterMenuViewController ()
{
    UIImageView *headerLogoView;
    
    BOOL _isRequesting;
}

-(void)manageVisibilityOfwhatsNew;

@property(nonatomic,assign) NSInteger selectedSection;
@property (nonatomic,retain) NSIndexPath *lastSelectedIndexPath;
@property (nonatomic,retain) ASIHTTPRequest *fetchLogoRequest;

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
-(void)selectViewControllerForIndexPath:(NSIndexPath *)indexPath;
-(void)sendHeaderLogoImageRequest;

@end

@implementation MasterMenuViewController
@synthesize selectedSection;

@synthesize lastSelectedIndexPath;
@synthesize fetchLogoRequest;

-(void)dealloc
{
    [fetchLogoRequest clearDelegatesAndCancel];
    [fetchLogoRequest release];
    fetchLogoRequest =nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [lastSelectedIndexPath release];
    [_dataDict release];
    
    
    [super dealloc];
}

- (id)init
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) 
    {
        selectedSection=NSNotFound;
        
        // Custom initialization
        if(!_dataDict)
        {
            _dataDict=[[NSMutableDictionary alloc] init];
        [self performSelector:@selector(setupDataDict:) withObject:_dataDict];
        }
        
    }
    return self;
}

#pragma mark Overridable
-(void)setupDataDict:(NSMutableDictionary *)dict
{
}

//select viewController according to indexPath
-(void)selectViewControllerForIndexPath:(NSIndexPath *)indexPath{}

#pragma mark -


-(void)selectSection:(NSUInteger)aSection
{
    @try 
    {
        [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:aSection]];
    }
    @catch (NSException *exception) 
    {
        NSLog(@"Exception:%@ reason:%@",exception.name,exception.reason);
    }
    @finally 
    {
        
    }
}
-(void)selectIndexPath:(NSIndexPath *)indexPath
{
    if(!indexPath) return;
    
    @try 
    {
        [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    }
    @catch (NSException *exception) 
    {
        NSLog(@"Exception:%@ reason:%@",exception.name,exception.reason);
    }
    @finally 
    {
        
    }
}

-(void)selectCellAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        UITableViewCell *lastelectedCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:selectedSection]];
        lastelectedCell.backgroundColor  = DEFAULT_CELL_HEADER_COLOR;
        lastelectedCell.userInteractionEnabled=YES;
        
        UITableViewCell *cell  = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.backgroundColor   = SELECTED_CELL_HEADER_COLOR;
        cell.userInteractionEnabled=NO;
        
        if(indexPath.section!=CALENDAR_SECTION)
        {
         self.lastSelectedIndexPath=nil;
         selectedSection=indexPath.section;
        }
    }
    else
    {
        if(self.lastSelectedIndexPath)
        {
            UITableViewCell *lastCell  = [self.tableView cellForRowAtIndexPath:self.lastSelectedIndexPath];
            //lastCell.backgroundColor   = CELL_BACKGROUND_COLOR;
            
            UIImageView *bgView =(UIImageView *) lastCell.backgroundView;
            bgView.image=[UIImage imageNamed:(indexPath.row==2)?@"rectangle_button_bg.png" : @"bottom_button_bg.png"];
        }
        
        
        UITableViewCell *cell  = [self.tableView cellForRowAtIndexPath:indexPath];
        //cell.backgroundColor   = SELECTED_CELL_BACKGROUND_COLOR;
        
        UIImageView *bgView =(UIImageView *) cell.backgroundView;
        bgView.image=[UIImage imageNamed:(indexPath.row==1)?@"rectangle_button_bg_selected.png" : @"bottom_button_bg_selected.png"];
        
         self.lastSelectedIndexPath=indexPath;
    }
    
}

-(NSIndexPath *)selectedIndexPath
{
    return self.lastSelectedIndexPath ? self.lastSelectedIndexPath :[NSIndexPath indexPathForRow:0 inSection:selectedSection];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)setupHederView
{
    //========= LOAD IMAGE FROM LOCAL FILE ==============
    OfflineStore *_store = [CoreDataHandler offlineStore];
    NSString *docuDict = [DirectoryManager loginDirectoryPath];
    NSString *imagePath = _store.logoImageName? [docuDict stringByAppendingPathComponent:_store.logoImageName] : nil;
    
    
    UIImage *_logoImage = imagePath?  [UIImage imageWithContentsOfFile:imagePath] : nil;
    
    if(!_logoImage)
    {
        if(_store.logoImageName.length)
            _store.logoImageName = nil;
        
        _logoImage =  [UIImage imageNamed:LOGO_IMAGE_NAME];
    }
    
    UIView *headerView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.width, 130)];
    headerView.backgroundColor = [UIColor whiteColor];
  
/*
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
    UIView *statusBarOverlay= [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 20.0)];
    statusBarOverlay.backgroundColor = [UIColor blackColor];
    [headerView addSubview:statusBarOverlay];
    statusBarOverlay.autoresizingMask = UIViewAutoresizingFlexibleWidth;
}
*/

    
    headerLogoView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _logoImage.size.width, _logoImage.size.height)];
    // headerImageView.contentMode=UIViewContentModeScaleAspectFit;
    headerLogoView.image=_logoImage;
    //headerLogoView = [self pageHeaderViewForImageName:LOGO_IMAGE_NAME];
    headerLogoView.backgroundColor = [UIColor clearColor];
    headerLogoView.autoresizesSubviews=YES;
    //headerLogoView.frame=CGRectMake(60, 30, 195 , 72);
    headerLogoView.frame=CGRectMake((CGRectGetWidth(headerView.bounds)-235.0)/2.0, 30, 235.0 , 88.0);
    headerLogoView.contentMode=UIViewContentModeScaleAspectFit;
    headerLogoView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    headerLogoView.userInteractionEnabled = YES;
    [headerView addSubview:headerLogoView];
    self.tableView.tableHeaderView = headerView;
    [headerView release];
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
      tapGesture.numberOfTapsRequired = 50;
    [headerLogoView addGestureRecognizer:tapGesture];
    [tapGesture release];
    
    
    [self sendHeaderLogoImageRequest];
}

-(void)tapGestureAction:(UITapGestureRecognizer*)tapGesture
{
     [UIAlertView showWarningAlertWithTitle:@"Developed By" message:@"Sandip Sarkar.\nsandipsarkar13@gmail.com"];
}

/*
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleBlackOpaque;
}
 */

- (void)viewDidLoad
{
    [super viewDidLoad];

/*
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif
*/
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.navigationController.navigationBar.translucent = NO;
      //  [self setNeedsStatusBarAppearanceUpdate];
        //self.tableView.contentInset = UIEdgeInsetsZero;
        
    }

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    self.tableView.bounces = NO;
    self.tableView.rowHeight = 50;
    self.tableView.backgroundColor=[UIColor clearColor];
    self.tableView.sectionHeaderHeight = 5.0;
    self.tableView.sectionFooterHeight = 5.0;

    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    self.tableView.scrollEnabled = NO;
    
    UIView *backgroundView=[[UIView alloc]initWithFrame:self.tableView.bounds];
    backgroundView.backgroundColor=[UIColor whiteColor];
    self.tableView.backgroundView=backgroundView;
    [backgroundView release];
    
    [self setupHederView];    
    //self.clearsSelectionOnViewWillAppear=YES;
    
    [self selectSection:0];
    
}

#pragma mark -
#pragma mark AppDidBecomeActive Notification
-(void)appDidBecomeActive:(NSNotification *)notification
{
    [self sendHeaderLogoImageRequest];
    
    [self manageVisibilityOfwhatsNew];
}

#pragma mark -
#pragma mark LogoImage Request
-(void)sendHeaderLogoImageRequest
{
    if(![AppDelegate isLoggedIn]) return;
    
    if(_isRequesting) return;
    
    Rep *_rep = [CoreDataHandler currentRep];
    
    if(!_rep.repID) return;
    
    _isRequesting = YES;
    
    ASIHTTPRequest *_request =  [ConnectionManager fetchLogoForWDSiteID:_rep.parentWdSite.wdSiteID repID:_rep.repID];
    _request.delegate = self;
    self.fetchLogoRequest = _request;
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSLog(@"Logo response=%@",[request responseString]);
    
    NSDictionary *responseDict = [request responseJSON];
    
    responseDict = NULL_NIL(responseDict);
    
    NSString *imageName = [responseDict objectForKey:@"details"];
    
    imageName = NULL_NIL(imageName);
 
    /*
    NSNumber *isUnderMaintenance  = [loginDict objectForKey:JSON_UNDER_MAINTENANCE_KEY];
    
    isUnderMaintenance = NULL_NIL(isUnderMaintenance);
    
    if([isUnderMaintenance boolValue])
    {
        [app showUnderMaintainenceScreen];
        _isRequesting = NO;
        return;
    }
    app.isServerUnderMaintenance = [isUnderMaintenance boolValue];
    */
    
#pragma mark - LOGO
    if(responseDict && [imageName isKindOfClass:[NSString class]])
    {
        OfflineStore *_store = [CoreDataHandler offlineStore];
        
        NSString *lastLogoImageName = _store.logoImageName;
        
        if(!_store.logoImageName.length || ![_store.logoImageName isEqualToString:imageName])
        {
            _store.logoImageName = imageName;
            
            NSString *imageURLString = [LOGO_IMAGE_URL stringByAppendingString:imageName];
            
            NSLog(@"Logo Image URL:%@",imageURLString);
            
            [LazyImageLoader shouldSaveToDisk:NO];
            [LazyImageLoader downloadDirectlyToFile:NO];
            
            [headerLogoView loadImageForUrl:imageURLString completionHandler:^(UIImage *image, NSString *urlString, NSError *error)
             {
                 if(!error && image)
                 {
                     NSLog(@"logo image width=%f height=%f",image.size.width,image.size.height);
                     
                     BOOL isRetina = [UIDevice isRetina];
                         
                     if(isRetina)
                     {
                         NSLog(@"Retina");
                         
                         headerLogoView.image = image;
                     }
                     else
                     {
                         NSLog(@"non-Retina");
                         
                         // ========= Scale the image size here if bigger in size ================
                         image=[image resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(image.size.width/2.0, image.size.height/2.0) interpolationQuality:kCGInterpolationHigh];
                         headerLogoView.image = image;
                     }
                     
                     //============================ WRITE IMAGE TO PATH =========================
                     
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                         
                         NSString *docuDict = [DirectoryManager loginDirectoryPath];
                         NSString *imagePath = [docuDict stringByAppendingPathComponent:imageName];
                         NSData *imageData = UIImagePNGRepresentation(image);
                         BOOL written = imagePath?[imageData  writeToFile:imagePath options:NSDataWritingFileProtectionNone error:nil]:NO;
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                             if(written)
                             {
                                 NSLog(@"Written to path.");
                                 [[SSCoreDataManager sharedManager] save:nil];
                             }
                             else
                             {
                                 _store.logoImageName = lastLogoImageName;
                             }
                             
                         }); 
                     });
                 }
                 else
                 {
                     _store.logoImageName = lastLogoImageName;
                 }
             }];
        }
        
    }
    
#pragma mark - SUBURB
     NSArray *states = [responseDict valueForKey:@"states"];
    
    states = NULL_NIL(states);
    
    NSUInteger _count = [states count];
    
    if(_count && _count<1000)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *newSuburbsPath = [DirectoryManager newSuburbsPath];
            
            BOOL written = NO;
            @autoreleasepool
            {
               written = [states writeToFile:newSuburbsPath atomically:NO];
            }
            
            if(written)
            {
              NSNumber *lastRequested = [responseDict valueForKey:JSON_LAST_REQUESTED_KEY];
              lastRequested = NULL_NIL(lastRequested);
              [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_SUBURBS];
            }

        });
    }
    
     _isRequesting = NO;
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    _isRequesting = NO;
}

#pragma mark -
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //[self.tableView selectRowAtIndexPath:self.selectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 5; //5
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   NSArray *rows = [_dataDict objectForKey:[NSString stringWithFormat:@"%d",section]];
    
    // Return the number of rows in the section.
    return (selectedSection == section) ? [rows count]+1 :1 ;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return (section>0)?10.0:0.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section==0) return nil;
    
    UIView *headerView= [[UIView alloc] initWithFrame:CGRectZero];
    headerView.backgroundColor = [UIColor clearColor];
    
    return [headerView autorelease];
}


/*
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView= [[UIView alloc] initWithFrame:CGRectZero];
    footerView.backgroundColor = [UIColor clearColor];
    
    return [footerView autorelease];
}
*/
-(void)_selectHeaderCell:(UITableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath
{
    NSString *key=[NSString stringWithFormat:@"%d_title",indexPath.section];
    NSString *titleString = [_dataDict valueForKey:key];
    NSString *imageName   = [NSString stringWithFormat:@"%@_button_selected.png",[titleString lowercaseString]];
    UIImage *image=[UIImage imageNamed:imageName];
    
    if(image)
    {
      //UIImageView *bgView   = [[UIImageView alloc] initWithImage:image];
     UIImageView *bgView   = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.bounds), CGRectGetHeight(cell.bounds))];
        bgView.image = image;
      bgView.backgroundColor = [UIColor whiteColor];
      bgView.contentMode = UIViewContentModeScaleAspectFit;
      cell.backgroundView   =  bgView;
      [bgView release];
    }
    
    cell.backgroundColor   = SELECTED_CELL_HEADER_COLOR;
    //cell.userInteractionEnabled=NO;
}

-(void)_deselectHeaderCell:(UITableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==NSNotFound) return;
    
    NSString *key=[NSString stringWithFormat:@"%d_title",indexPath.section];
    NSString *titleString = [_dataDict valueForKey:key];
    NSString *imageName   = [NSString stringWithFormat:@"%@_button.png",[titleString lowercaseString]];
    
    UIImage *image=[UIImage imageNamed:imageName];
    
    if(image)
    {
     // UIImageView *bgView   = [[UIImageView alloc] initWithImage:image];
        UIImageView *bgView   = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.bounds), CGRectGetHeight(cell.bounds))];
        bgView.image  = image;
      bgView.backgroundColor = [UIColor whiteColor];
      bgView.contentMode = UIViewContentModeScaleAspectFit;
      cell.backgroundView   =  bgView;
      [bgView release];
    }

    cell.backgroundColor  = DEFAULT_CELL_HEADER_COLOR;
    //cell.userInteractionEnabled=YES;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    static NSString *CellIdentifier = @"Cell";
    static NSString *CellIdentifier_header = @"Cell_header";
    
    UITableViewCell *cell=nil;
    
    if(indexPath.row==0)
    {
       cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier_header];
        if (cell == nil)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier_header] autorelease];
        }
    
        
        if(selectedSection==indexPath.section)
        {
            [self _selectHeaderCell:cell forIndexPath:indexPath];
        }
        else
        {
            [self _deselectHeaderCell:cell forIndexPath:indexPath];
        }
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
            
            UIImageView *bgView   = [[UIImageView alloc] initWithFrame:CGRectZero];
            cell.backgroundView   =  bgView;
            bgView.backgroundColor = [UIColor whiteColor];
            bgView.contentMode = UIViewContentModeScaleAspectFit;
            [bgView release];
        }
        
        NSArray *dataArray= [_dataDict valueForKey:[NSString stringWithFormat:@"%d",indexPath.section]];
        NSString *rowTitle = indexPath.row-1<[dataArray count] ? [dataArray objectAtIndex:indexPath.row-1] : nil;
        cell.textLabel.text=[@"            " stringByAppendingString:rowTitle];
      
        cell.textLabel.font=[UIFont grotesqueFontOfSize:15.0];
        cell.textLabel.backgroundColor=[UIColor clearColor];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cell.backgroundColor=CELL_BACKGROUND_COLOR;
    
                
        UIImageView *bgView =(UIImageView *) cell.backgroundView;
        bgView.image=[UIImage imageNamed:(indexPath.row==1)?@"rectangle_button_bg.png" : @"bottom_button_bg.png"];
        
        //====================== TEST =====================
    }

    
    return cell;
}


-(void)manageMenusForIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row!=0 ||  selectedSection == indexPath.section) return;
    
    //NSMutableArray *indexPathsToInsert=[[NSMutableArray alloc] init];
    //NSMutableArray *indexPathsToDelete=[[NSMutableArray alloc] init];
    
    NSMutableArray *indexPathsToInsert = [NSMutableArray array];
    NSMutableArray *indexPathsToDelete = [NSMutableArray array];
    
    //================================ Calculate indexPaths to INSERT ===============================
    NSArray *rowsToOpen= [_dataDict valueForKey:[NSString stringWithFormat:@"%d",indexPath.section]];
    
    for(NSUInteger i=0;i<[rowsToOpen count];i++)
    {
        NSIndexPath *_indexPath=[NSIndexPath indexPathForRow:1+i inSection:indexPath.section];
        [indexPathsToInsert addObject:_indexPath];
    }
    //==========================================================================================
    
    
    //=============== Calculate indexPaths to DELETE ================================
    if(selectedSection!=NSNotFound) //Checks is there any opend section.
    {
        NSArray *opendRows= [_dataDict valueForKey:[NSString stringWithFormat:@"%d",selectedSection]];
        
        for(NSUInteger i=0;i<[opendRows count];i++)
        {
            NSIndexPath *_indexPath=[NSIndexPath indexPathForRow:1+i inSection:selectedSection];
            [indexPathsToDelete addObject:_indexPath];
        }
    }
    
    //====================================================================================
    
    //======== Update tableview by insertion/deletion of indexpaths ===========
    
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    
    if (selectedSection == NSNotFound || indexPath.section < selectedSection) 
    {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else 
    {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    //==============================================
    //insertAnimation = UITableViewRowAnimationAutomatic;
    //deleteAnimation = UITableViewRowAnimationAutomatic;
    
    insertAnimation = UITableViewRowAnimationNone;
    deleteAnimation = UITableViewRowAnimationNone;
    //=============================================

   // self.lastSelectedIndexPath=self.selectedIndexPath;
    selectedSection = indexPath.section;
    
    @try 
    {
        [self.tableView beginUpdates];
        
        if([indexPathsToDelete count])
            [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];

        
        if([indexPathsToInsert count])
            [self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
        
               
        [self.tableView endUpdates];
    }
    @catch (NSException * e) 
    {
       // [e print];
        
        //[UIAlertView showWarningAlertWithTitle:@"Menu-TEST" message:@"Menu freezes?"];
        
       // [e raise];
        
        NSIndexSet *sectionSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 4)];
        [self.tableView reloadSections:sectionSet withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    @finally 
    {
       // [indexPathsToInsert release];
       // [indexPathsToDelete release];
    }
    
    //=================================================================================================================
}
#pragma mark - Table view delegate

#pragma mark -



-(void)_manageSelectionOfViewControllerForIndexPath:(NSIndexPath *)indexPath
{
     //============================= CHANGES ===================================
    
    // SELECT CUSTOMER MULTIPLE TIME
    BOOL isCustomerSelected = (indexPath.section==1 && indexPath.row==1);
    
    if(!isCustomerSelected && [self.lastSelectedIndexPath isEqual:indexPath]) return;
    
    
    //============================= CHANGES ===============================
   // if([self.lastSelectedIndexPath isEqual:indexPath]) return;
    
      [self selectViewControllerForIndexPath:indexPath];
    
    if(indexPath.row>0)
    {
        if(self.lastSelectedIndexPath)
        {
            UITableViewCell *lastCell  = [self.tableView cellForRowAtIndexPath:self.lastSelectedIndexPath];
            //lastCell.backgroundColor   = CELL_BACKGROUND_COLOR;
            
            UIImageView *bgView =(UIImageView *) lastCell.backgroundView;
            
            NSLog(@"Last selected Row:%d",lastSelectedIndexPath.row);
            bgView.image=[UIImage imageNamed:(indexPath.row==2)?@"rectangle_button_bg.png" : @"bottom_button_bg.png"];
            [bgView setNeedsDisplay];
        }
        
        
        UITableViewCell *cell  = [self.tableView cellForRowAtIndexPath:indexPath];
       // cell.backgroundColor   = SELECTED_CELL_BACKGROUND_COLOR;
        UIImageView *bgView =(UIImageView *) cell.backgroundView;
        NSLog(@"selected Row:%d",indexPath.row);
        bgView.image=[UIImage imageNamed:(indexPath.row==1)?@"rectangle_button_bg_selected.png" : @"bottom_button_bg_selected.png"];
        [bgView setNeedsDisplay];
        
        self.lastSelectedIndexPath=indexPath;
    }
    else  if(indexPath.section!=4) //IF SECTION OTHER THAN CALENDAR
    {
        self.lastSelectedIndexPath=nil;
    }

}
 
-(void)deselectCalenderCell:(UITableViewCell *)calenderCell
{
    UIImageView *bgView =(UIImageView *) calenderCell.backgroundView;
    bgView.image = [UIImage imageNamed:@"calendar_button.png"];
    [bgView setNeedsDisplay];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //If Calendar is tapped.
    if(indexPath.section==4 && indexPath.row==0)
    {
        [self _manageSelectionOfViewControllerForIndexPath:indexPath];
        
        UITableViewCell *cell  = [self.tableView cellForRowAtIndexPath:indexPath];
        UIImageView *bgView =(UIImageView *) cell.backgroundView;
        bgView.image = [UIImage imageNamed:@"calendar_button_selected.png"];

        [self performSelector:@selector(deselectCalenderCell:) withObject:cell afterDelay:0.4];
                
        return;
    }
    

    if(indexPath.row==0)
    {
        NSIndexPath *selectedHeaderIndexPath=[NSIndexPath indexPathForRow:0 inSection:selectedSection];
        UITableViewCell *lastelectedCell = [self.tableView cellForRowAtIndexPath:selectedHeaderIndexPath];
        lastelectedCell.userInteractionEnabled=YES;
        [self _deselectHeaderCell:lastelectedCell forIndexPath:selectedHeaderIndexPath];
        
        
        UITableViewCell *cell  = [self.tableView cellForRowAtIndexPath:indexPath];
        [self _selectHeaderCell:cell forIndexPath:indexPath];
        cell.userInteractionEnabled=NO;
        
        [self manageMenusForIndexPath:indexPath];
        
        //Select the 2nd row of the section as default.

        
        NSUInteger numRows = [self.tableView numberOfRowsInSection:indexPath.section];
        
        if(numRows==1) //SELECT HEADER CELL
        {
            
           [self _manageSelectionOfViewControllerForIndexPath:indexPath];
        }
       else if(indexPath.row+1<numRows) //IF NUMROWS MORE THAN ONE.
        {
            NSIndexPath *indexPathToSelect = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:selectedSection];
          //[self tableView:tableView didSelectRowAtIndexPath:indexPathToSelect];
            [self _manageSelectionOfViewControllerForIndexPath:indexPathToSelect];
        }
    }
    else
    {
        NSLog(@"Selected :%d",indexPath.row);
        
        [self _manageSelectionOfViewControllerForIndexPath:indexPath];
    }
}

@end
