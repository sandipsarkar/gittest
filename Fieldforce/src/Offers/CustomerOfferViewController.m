//
//  CustomerOfferViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 27/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomerOfferViewController.h"
#import "OfferListViewControler.h"
#import "UISearchBar+BackgroundImage.h"
#import "OfferDetailViewController.h"
#import "FilterOfferViewController.h"
#import "Rep.h"
#import "Visit.h"
#import "Offer.h"
#import "Customer.h"
#import "OEM.h"
#import "WholesaleDealer.h"
#import "LazyImageLoader.h"
#import "SSMemoryCache.h"


#define TOP_BAR_IMAGE @"view_tasks_side_bar.png"
#define CONTROLLER_WIDTH 337.0

@interface CustomerOfferViewController ()<UISearchBarDelegate,ImageLoaderDelegate,OfferFilterDelegate>
{
    UIButton  *filterButton;
    UISearchBar *searchBar;
    
    CGFloat headerBarHeight;
    
    BOOL isSearching;
    
    BOOL isFetchingFromServer;
    
    SSMemoryCache *imageCache;
    
}
@property(nonatomic,retain) UIPopoverController *filterPopoverController;
@property(nonatomic,retain) ASIHTTPRequest *currentOfferRequest;



-(void)fetchOffersFromServer;
-(void)fetchOffers;

@end

@implementation CustomerOfferViewController

@synthesize mechanicalOfferListViewControler=_mechanicalOfferListViewControler;
@synthesize collisionOfferListViewControler=_collisionOfferListViewControler;
@synthesize offerType;
@synthesize offers,mechanicalOffers,collissionOffers;
@synthesize filterPopoverController;
@synthesize visit;
@synthesize currentOfferRequest;


- (id)initWithOfferType:(CategoryType)type
{
    self = [super init];
    
    if (self)
    {
        offerType = type;
        
        [LazyImageLoader downloadDirectlyToFile:YES];
    }
    
    return self;
}

-(void)dealloc
{
    [LazyImageLoader cancelAllConnections];
    
    currentOfferRequest.delegate=nil;
    [currentOfferRequest clearDelegatesAndCancel];
    [currentOfferRequest release];
    currentOfferRequest=nil;

    [imageCache release];
    imageCache = nil;
    
    visit=nil;
    
    [offers release];
    [mechanicalOffers release];
    [collissionOffers release];
    
    [_mechanicalOfferListViewControler release];
    [_collisionOfferListViewControler release];
    [_visitedOfferIDs release];
    
    [filterPopoverController release];
    [searchBar release];
    

    [super dealloc];
}

-(SSMemoryCache *)imageCache
{
    if(imageCache==nil)
    {
        imageCache = [[SSMemoryCache alloc]init];
       // [imageCache  setCountLimit:15];
        imageCache.cacheCount=16;

    }
    
    return imageCache;
}

-(UIButton *)filterButton
{
    if(!filterButton)
    {
    filterButton =[UIButton buttonWithType:UIButtonTypeCustom];
    filterButton.frame = CGRectMake(0, 0, 117.0, 44.0); //117.0
    [filterButton setTitle:@"Filter" forState:UIControlStateNormal];
    filterButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
    [filterButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"segmented_button.png"] forState:UIControlStateNormal];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"segmented_button_selected.png"] forState:UIControlStateHighlighted];
        [filterButton setBackgroundImage:[UIImage imageNamed:FILTER_BUTTON_IMAGE] forState:UIControlStateSelected];

    [filterButton addTarget:self action:@selector(filterAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return filterButton;
}

-(void)setupFilterButton
{
    [self.view addSubview:[self filterButton]];
    
}

-(void)setupSearchBar
{
    searchBar=[[UISearchBar alloc] initWithFrame:CGRectMake(filterButton.right+235.0, 0, self.view.width-filterButton.width-235.0, 44.5)];
    searchBar.backgroundColor=[UIColor clearColor];;
    searchBar.placeholder = @"Search...";
   // searchBar.placeholder = @"Search...                                                           \n";
    //searchBar.tintColor=LOGIN_BG_COLOR;
    searchBar.autoresizingMask=UIViewAutoresizingFlexibleWidth;
    searchBar.delegate= self;
    [self.view addSubview:searchBar];
    
    // [searchBar addBackgroundImage:[[UIImage imageNamed:@"segmented_button.png"] stretchableImageWithLeftCapWidth:1 topCapHeight:0]];
    [searchBar addBackgroundImage:[UIImage imageNamed:@"search_bar_big.png"]];
}

-(void)setupTopBar
{
    CGRect _bounds = self.view.bounds;
    
    headerBarHeight = 44.0;
    
    UIImage *topBarImage = [UIImage imageNamed:TOP_BAR_IMAGE];
    UIImageView *headerBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_bounds), headerBarHeight)];
    headerBarView.image = topBarImage;
    headerBarView.backgroundColor = [UIColor grayColor];
    [self.view addSubview:headerBarView];
    [headerBarView release];
    
    [self setupFilterButton];
    [self setupSearchBar];
}

-(void)setupView
{
    CGRect _bounds = self.view.bounds;
    
    [self setupTopBar];
    
    CGFloat gap = 10.0f;
    
    CGRect _firstHalfRect  = CGRectMake(gap, headerBarHeight, CGRectGetWidth(_bounds)/2.0-gap, CGRectGetHeight(_bounds)-headerBarHeight-gap);
    
    CGRect _secondHalfRect = CGRectMake(CGRectGetMaxX(_firstHalfRect)+gap/2, CGRectGetMinY(_firstHalfRect), CGRectGetWidth(_firstHalfRect), CGRectGetHeight(_firstHalfRect));
    
      CGRect _fullRect  = CGRectMake(gap, headerBarHeight, CGRectGetWidth(_bounds)-2*gap, CGRectGetHeight(_bounds)-headerBarHeight-gap);
    
    switch (offerType) 
    {
        case CategoryTypeDefault:
        {
            _mechanicalOfferListViewControler = [[OfferListViewControler alloc] initWithOffers:nil type:CategoryTypeMechanical];
            _collisionOfferListViewControler  = [[OfferListViewControler alloc] initWithOffers:nil type:CategoryTypeCollision];
            
            BOOL _showCategory = [self showCategory];
            _mechanicalOfferListViewControler.showCategory = _showCategory;
            _collisionOfferListViewControler.showCategory  = _showCategory;
            
            _collisionOfferListViewControler.view.frame  = _firstHalfRect;
            _mechanicalOfferListViewControler.view.frame = _secondHalfRect;
            
            [self.view addSubview:_mechanicalOfferListViewControler.view];
            [self.view addSubview:_collisionOfferListViewControler.view];
            
            _collisionOfferListViewControler.view.autoresizingMask = UIViewAutoresizingFlexibleWidth |  UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
            
             _mechanicalOfferListViewControler.view.autoresizingMask = UIViewAutoresizingFlexibleWidth |  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
        }
        break;
            
        case CategoryTypeMechanical:
        {
            _mechanicalOfferListViewControler = [[OfferListViewControler alloc] initWithOffers:nil type:CategoryTypeMechanical];

            BOOL _showCategory = [self showCategory];
            _mechanicalOfferListViewControler.showCategory = _showCategory;
            
            _mechanicalOfferListViewControler.view.frame = _fullRect;
            [self.view addSubview:_mechanicalOfferListViewControler.view];
            
            _mechanicalOfferListViewControler.view.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
        }
        break;
            
        case CategoryTypeCollision:
        {
            _collisionOfferListViewControler  = [[OfferListViewControler alloc] initWithOffers:nil type:CategoryTypeCollision];
            
            
            BOOL _showCategory = [self showCategory];
            _collisionOfferListViewControler.showCategory = _showCategory;
            
            _collisionOfferListViewControler.view.frame = _fullRect;
            [self.view addSubview:_collisionOfferListViewControler.view];
            
            _collisionOfferListViewControler.view.autoresizingMask = UIViewAutoresizingFlexibleWidth |  UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
        }
        break;
            
        case CategoryTypeAll:
        {
            _mechanicalOfferListViewControler = [[OfferListViewControler alloc] initWithOffers:nil type:CategoryTypeAll];
            
            BOOL _showCategory = [self showCategory];
            _mechanicalOfferListViewControler.showCategory = _showCategory;
            
            _mechanicalOfferListViewControler.view.frame = _fullRect;
            [self.view addSubview:_mechanicalOfferListViewControler.view];
            
            _mechanicalOfferListViewControler.view.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
        }
            break;

            
        default: break;
    }
    
    if(_collisionOfferListViewControler)
    {
        _collisionOfferListViewControler.parentController  = self;
        _collisionOfferListViewControler.visit  = self.visit;
        _collisionOfferListViewControler.visitedOfferIDs  = _visitedOfferIDs;
        _collisionOfferListViewControler.imageCache=[self imageCache];
        [self addChildViewController:_collisionOfferListViewControler];
         [_collisionOfferListViewControler didMoveToParentViewController:self];
    }
    
    if(_mechanicalOfferListViewControler)
    {
        _mechanicalOfferListViewControler.parentController = self;
        _mechanicalOfferListViewControler.visit = self.visit;
        _mechanicalOfferListViewControler.visitedOfferIDs = _visitedOfferIDs;
        _mechanicalOfferListViewControler.imageCache = [self imageCache];
        [self addChildViewController:_mechanicalOfferListViewControler];
        [_mechanicalOfferListViewControler didMoveToParentViewController:self];
    }
    
    NSLog(@"_visitedOfferIDs=%@",_visitedOfferIDs);
}

-(BOOL)showCategory
{
    return YES;
}

-(UIView *)showFilterPopoverFromView
{
    return self.view;
}




- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    //app=[(UIApplication *)[UIApplication sharedApplication]delegate];
      
    [self.navigationController.navigationBar addBackGroundImage:@"header_blank.png"];
    [self addTitle:@"CUSTOMER OFFERS" withFont:[UIFont subaruBoldFontOfSize:23.0]]; 
     [self.view addLeftBoderWithColor:[UIColor colorWithWhite:0.8f alpha:1.0f] borderWidth:1.0f];
    [self addRefreshButtonWithSelector:@selector(refreshButtonAction)];
    
    self.view.backgroundColor = [UIColor whiteColor];

    NSArray *_offerIds = [self.visit.visitedOfferIDs componentsSeparatedByString:@","];
    _visitedOfferIDs = [_offerIds count] ? [[NSMutableArray alloc] initWithArray:_offerIds] : [[NSMutableArray alloc] init];
    
    NSLog(@"Visited offer IDS:%@",_visitedOfferIDs);

    [self setupView];
    
    @try 
    {
        [self fetchOffersFromServer];
        [self fetchOffers];
    }
    @catch (NSException *exception) 
    {
        NSLog(@"Exception=%@",exception.reason);
    }
    @finally 
    {
        
    }
    
    
    __block  CustomerOfferViewController *weakSelf = self;
    __block OfferListViewControler *meckOfferListVC      = self.mechanicalOfferListViewControler;
    __block OfferListViewControler *collisionOfferListVC = self.collisionOfferListViewControler;

    
    [_mechanicalOfferListViewControler addDidSelectOfferCallback:^(Offer *selectedOffer, NSIndexPath *indexPath) 
    {
        [weakSelf didSelectOffer:selectedOffer forIndexPath:indexPath fromOfferList:[meckOfferListVC currentArray]];
    }];
    
    [_collisionOfferListViewControler addDidSelectOfferCallback:^(Offer *selectedOffer, NSIndexPath *indexPath) 
    {
         [weakSelf didSelectOffer:selectedOffer forIndexPath:indexPath fromOfferList:[collisionOfferListVC currentArray]];
    }];
    
} 

-(void)viewWillDisappear:(BOOL)animated
{
        [super viewWillDisappear:animated];
    
    if(self.visit)
    {
      self.visit.visitedOfferIDs = [_visitedOfferIDs componentsJoinedByString:@","];
    }
    
    
    //if([self imageCache])
      //  [[self imageCache] removeAllObjects];
    
   // [_mechanicalOfferListViewControler viewWillDisappear:animated];
   // [_collisionOfferListViewControler viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //[_mechanicalOfferListViewControler viewWillAppear:animated];
   // [_collisionOfferListViewControler viewWillAppear:animated];
}

-(void)reload
{
    [_mechanicalOfferListViewControler.tableView reloadData];
    [_collisionOfferListViewControler.tableView reloadData];
}

-(void)sortOffers
{
    NSSortDescriptor *sortDescriptor1 = [[[NSSortDescriptor alloc] initWithKey:@"activeFrom" ascending:NO] autorelease];
    NSSortDescriptor *sortDescriptor2 = [[[NSSortDescriptor alloc] initWithKey:@"offerName" ascending:NO] autorelease];

    //NSArray *descriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *descriptors = [NSArray arrayWithObjects:sortDescriptor1,sortDescriptor2,nil];
    [_mechanicalOfferListViewControler sortOffersWithDescriptors:descriptors];
    [_collisionOfferListViewControler sortOffersWithDescriptors:descriptors];

}

-(void)refreshButtonAction
{
    [self fetchOffersFromServer];
    
    //============ RESET SEARCHING ON REFRESH =============
    if(isSearching)
    {
        searchBar.text=nil;
        [searchBar resignFirstResponder];
        
        
        [self.mechanicalOfferListViewControler resetSearchMode];
        [self.collisionOfferListViewControler resetSearchMode];
        
        [self filterButton].selected = NO;
        self.filterPopoverController=nil;
        
        [self searchWithText:nil];
        isSearching=NO;
    }
}

-(void)fetchOffersFromServer
{
    if(isFetchingFromServer) return;
    
    isFetchingFromServer = YES;
    

    Rep *_rep          = [CoreDataHandler currentRep];
    NSNumber *wdSiteID = _rep.parentWdSite.wdSiteID;
    NSNumber *wdID     = _rep.parentWdSite.parentWdID;
    
    ASIHTTPRequest *request = [ConnectionManager createOffersConnectionWithWithRepID:_rep.repID wdSiteID:wdSiteID wdID:wdID];
    request.delegate = self;
    self.currentOfferRequest = request;
    
    /* [request setCompletionBlock:^{
        
           isFetchingFromServer = NO;
           
        NSLog(@"Response:%@",[request responseString]);
        
        NSDictionary *jsonDict = [request responseJSON];
           
        NSLog(@"jsonDict offer:%@",jsonDict);
           
           
        if(jsonDict && [jsonDict isKindOfClass:[NSDictionary class]])
        {
            BOOL _isSiteIDChanged = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
            if(_isSiteIDChanged)
            {
                [app appDidChangeSiteID];
            }
            else 
            {
                
            NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
            [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_OFFERS];
                
            NSString *timeZoneName = [jsonDict valueForKey:@"timeZoneName"];
            app.offerTimeZoneName  = NULL_NIL(timeZoneName)? timeZoneName :@"Australia/Sydney";
                
            NSArray *_offers     = [jsonDict valueForKey:JSON_DETAILS_KEY];
            NSArray *_updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
            NSArray *_deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
                
   
            _offers = NULL_NIL(_offers);
            _updatedIDs = NULL_NIL(_updatedIDs);
            _deletedIDs = NULL_NIL(_deletedIDs);
                
                            
                if(![_offers count] && ![_deletedIDs count] && ![_updatedIDs count]) 
                {
                    [_mechanicalOfferListViewControler didUpdateFromServer];
                    [_collisionOfferListViewControler didUpdateFromServer];
                }
                
            
   
            [CoreDataHandler populateOffersPerformingDelete:_deletedIDs?_deletedIDs:[NSArray array] update:_updatedIDs?_updatedIDs:[NSArray array] insertOnArray:_offers completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
            {
                
                // ================================= DELETE ===============================================
            
                if([deletedResult count])
                {
                    if(self.offerType==OfferTypeAll)
                    {
                          [_mechanicalOfferListViewControler deleteOffers:deletedResult];
                    }
                    else
                    {

                    NSMutableArray * deletedMechanicalOffers = [CoreDataHandler mechanicalOffersFromOffers:deletedResult];
                    NSMutableArray * deletedCollissionOffers = [CoreDataHandler collisionOffersFromOffers:deletedResult];
                    
                    [_mechanicalOfferListViewControler deleteOffers:deletedMechanicalOffers];
                    [_collisionOfferListViewControler deleteOffers:deletedCollissionOffers];
                    }
                }
                
                //================== UPDATED ===============================
                
                //=============== LOGIC SHOULD BE REFRACTORED =================
                if([updatedResult count])
                {
                     NSLog(@"update");
                    
                    if(self.offerType==OfferTypeAll)
                    {
                       [_mechanicalOfferListViewControler updateOffers:updatedResult];
                    }
                    else
                    {
                    
                     NSMutableArray * updatedMechanicalOffers = [CoreDataHandler mechanicalOffersFromOffers:updatedResult];
                     NSMutableArray * updatedCollissionOffers = [CoreDataHandler collisionOffersFromOffers:updatedResult];
                    
                    [_mechanicalOfferListViewControler updateOffers:updatedMechanicalOffers];
                    [_collisionOfferListViewControler updateOffers:updatedCollissionOffers];
                    }
                }

                
                // ================================== INSERTED =================================================
                if([insertedResult count])
                {
                    if(self.offerType==OfferTypeAll)
                    {
                         [_mechanicalOfferListViewControler insertOffers:insertedResult];
                    }
                    else
                    {
                    NSMutableArray * insertedMechanicalOffers = [CoreDataHandler mechanicalOffersFromOffers:insertedResult];
                    NSMutableArray * insertedCollissionOffers = [CoreDataHandler collisionOffersFromOffers:insertedResult];
                
                    
                    [_mechanicalOfferListViewControler insertOffers:insertedMechanicalOffers];
                    [_collisionOfferListViewControler insertOffers:insertedCollissionOffers];
                    }
                }
            
                [_mechanicalOfferListViewControler didUpdateFromServer];
                [_collisionOfferListViewControler didUpdateFromServer];
                
                [self sortOffers];
                [self reload];
                
                
                
                
            }];
           }
           
        }
           
    }];
    
       [request setFailedBlock:^{
           
           [_mechanicalOfferListViewControler didUpdateFromServer];
           [_collisionOfferListViewControler didUpdateFromServer];
           isFetchingFromServer = NO;
       }]; */
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    isFetchingFromServer = NO;
    
    NSLog(@"Response:%@",[request responseString]);
    NSDictionary *jsonDict = [request responseJSON];
    NSLog(@"jsonDict offer:%@",jsonDict);
    
    if(jsonDict && [jsonDict isKindOfClass:[NSDictionary class]])
    {
        /*
        BOOL _isSiteIDChanged = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
        
        BOOL _isRepDeactivated = [[jsonDict valueForKey:JSON_REP_DEACTIVATE_KEY] boolValue];
        
        NSNumber *isUnderMaintenance  = [jsonDict objectForKey:JSON_UNDER_MAINTENANCE_KEY];
        isUnderMaintenance = NULL_NIL(isUnderMaintenance);

        if(_isSiteIDChanged)
        {
            [app appDidChangeSiteID];
        }
        else if(_isRepDeactivated)
        {
            [app repDidDeactivate];
        }

        else if([isUnderMaintenance boolValue])
        {
            [app showUnderMaintainenceScreen:YES];
        }
        else
        */
        
        BOOL _isValidResponse = [ConnectionManager isValidResponse:jsonDict];
        if(_isValidResponse)
        {
            NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
            
            
            NSString *timeZoneName = [jsonDict valueForKey:@"timeZoneName"];
            app.offerTimeZoneName  = NULL_NIL(timeZoneName)? timeZoneName :@"Australia/Sydney";
            
            NSArray *_offers     = [jsonDict valueForKey:JSON_DETAILS_KEY];
            NSArray *_deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
            //NSArray *_updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
            
            _offers = NULL_NIL(_offers);
            _deletedIDs = NULL_NIL(_deletedIDs);
            lastRequested = NULL_NIL(lastRequested);
             // _updatedIDs = NULL_NIL(_updatedIDs);
            
            //if(!([_offers count] || ([_deletedIDs count] || [_updatedIDs count])))
            if(!([_offers count] || [_deletedIDs count]))
            {
                [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_OFFERS];
                
                [_mechanicalOfferListViewControler didUpdateFromServer];
                [_collisionOfferListViewControler didUpdateFromServer];
            }
            
            /*
            [CoreDataHandler populateOffersPerformingDelete:_deletedIDs?_deletedIDs:[NSArray array] update:_updatedIDs?_updatedIDs:[NSArray array] insertOnArray:_offers completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
             {
               
                 
                [self fetchOffers];
                 
                 [self performSelector:@selector(updateFetchFromServerStatus) withObject:nil afterDelay:10.0];
                 
             }];
             */
            
            [Offer populateAsyncPerformingDelete:_deletedIDs insertOnArray:_offers onAttributes:[NSArray arrayWithObject:@"offerID"] completion:^(NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error){
                
                [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_OFFERS];
                [self fetchOffers];
                
                [self performSelector:@selector(updateFetchFromServerStatus) withObject:nil afterDelay:10.0];
            }
            deletionBlock:^(NSMutableArray *deletedResult) {
                
                
           }];
        }
        
    }
    
}

/*
 // ================================= DELETE ===============================================
 
 if([deletedResult count])
 {
 if(self.offerType==OfferTypeAll)
 {
 [_mechanicalOfferListViewControler deleteOffers:deletedResult];
 }
 else
 {
 
 NSMutableArray * deletedMechanicalOffers = [CoreDataHandler mechanicalOffersFromOffers:deletedResult];
 NSMutableArray * deletedCollissionOffers = [CoreDataHandler collisionOffersFromOffers:deletedResult];
 
 [_mechanicalOfferListViewControler deleteOffers:deletedMechanicalOffers];
 [_collisionOfferListViewControler deleteOffers:deletedCollissionOffers];
 }
 }
 
 //================== UPDATED ===============================
 
 //=============== LOGIC SHOULD BE REFRACTORED =================
 if([updatedResult count])
 {
 NSLog(@"update");
 
 if(self.offerType==OfferTypeAll)
 {
 [_mechanicalOfferListViewControler updateOffers:updatedResult];
 }
 else
 {
 
 NSMutableArray * updatedMechanicalOffers = [CoreDataHandler mechanicalOffersFromOffers:updatedResult];
 NSMutableArray * updatedCollissionOffers = [CoreDataHandler collisionOffersFromOffers:updatedResult];
 
 [_mechanicalOfferListViewControler updateOffers:updatedMechanicalOffers];
 [_collisionOfferListViewControler updateOffers:updatedCollissionOffers];
 }
 }
 
 
 // ====================== INSERTED ======================================
 if([insertedResult count])
 {
 if(self.offerType==OfferTypeAll)
 {
 [_mechanicalOfferListViewControler insertOffers:insertedResult];
 }
 else
 {
 NSMutableArray * insertedMechanicalOffers = [CoreDataHandler mechanicalOffersFromOffers:insertedResult];
 NSMutableArray * insertedCollissionOffers = [CoreDataHandler collisionOffersFromOffers:insertedResult];
 
 
 [_mechanicalOfferListViewControler insertOffers:insertedMechanicalOffers];
 [_collisionOfferListViewControler insertOffers:insertedCollissionOffers];
 }
 }
 
 [_mechanicalOfferListViewControler didUpdateFromServer];
 [_collisionOfferListViewControler didUpdateFromServer];
 
 [self sortOffers];
 [self reload];
 */

-(void)updateFetchFromServerStatus
{
    [_mechanicalOfferListViewControler didUpdateFromServer];
    [_collisionOfferListViewControler didUpdateFromServer];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    [self updateFetchFromServerStatus];
    isFetchingFromServer = NO;
}
 -(void)fetchOffers
 {
     
 [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
 [CoreDataHandler asyncFetchOffersOnCompletion:^(NSArray *result, NSError *error)
 {
     NSArray *allOffers = nil;
     
     if(self.visit.parentCustomer) // FROM CUSTOMER OFFER
     {
         //******* UNCOMMENT TO SHOW INTERESTED OFFER ONLY FOR PUBLISHED REPORT ************
         
           BOOL isReportPublished = [self.visit.isReportCompleted boolValue];
         
        NSMutableArray *_interesestedOfferIDS = isReportPublished ? [self.visit.visitedOfferIDs numbersSeparatedByString:@","] : nil;
         
         //****************** *******************************
         
         NSArray *dealingOems = [self.visit.parentCustomer.dealingOEMs componentsSeparatedByString:@","];
         
         NSMutableArray *oemDealings = [NSMutableArray array];
         for(NSString *oemID in dealingOems)
         {
             if([oemID integerValue])
             {
                 [oemDealings addObject:oemID];
             }
         }

         
        
         NSArray *oemsToCheck = [oemDealings count]? [OEM fetchWithPredicate:[NSPredicate predicateWithFormat:@"self.oemID IN %@",oemDealings] error:nil] : nil;
         
         if([oemsToCheck count])
         {
             NSArray *oemNamesToCheck = oemsToCheck?[oemsToCheck valueForKey:@"name"]:nil;
             
             NSLog(@"OEMS to check:%@",oemNamesToCheck);
         
         if([oemNamesToCheck count])
         {
             NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"self.userType = %@ AND NOT (self.createdByUserName IN %@)",[NSNumber numberWithInt:2], oemNamesToCheck];
             NSArray * _allOffers = [result filteredArrayUsingPredicate:filterPredicate];
             
             NSLog(@"offers to delete: %@",[_allOffers valueForKey:@"createdByUserName"]);
             
             NSMutableArray *arr = [NSMutableArray arrayWithArray:result];
             [arr removeObjectsInArray:_allOffers];
             
            //****************** UNCOMMENT TO SHOW INTERESTED OFFER ONLY FOR PUBLISHED REPORT *******************************
             
              if(isReportPublished && _interesestedOfferIDS)
               {
                 [arr filterUsingPredicate:[NSPredicate predicateWithFormat:@"self.offerID IN %@",_interesestedOfferIDS]];
               }
             
              //****************** UNCOMMENT TO SHOW INTERESTED OFFER ONLY FOR PUBLISHED REPORT *******************************
             
               allOffers=arr;
         }
             
         }
         else
         {
             //===== UNCOMMENT TO SHOW DEALERGROUP OFFER =====
             
                 NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"self.userType = %@",[NSNumber numberWithInt:2]];
                 NSArray  *_allOffers = [result filteredArrayUsingPredicate:filterPredicate];
                 
                 NSLog(@"offers to delete: %@",[_allOffers valueForKey:@"createdByUserName"]);
                 
                 NSMutableArray *arr = [NSMutableArray arrayWithArray:result];
                 [arr removeObjectsInArray:_allOffers];
             
              //****************** UNCOMMENT TO SHOW INTERESTED OFFER ONLY FOR PUBLISHED REPORT *******************************
             
              if(isReportPublished && _interesestedOfferIDS)
              {
                 [arr filterUsingPredicate:[NSPredicate predicateWithFormat:@"self.offerID IN %@",_interesestedOfferIDS]];
              }
             
              //****************** UNCOMMENT TO SHOW INTERESTED OFFER ONLY FOR PUBLISHED REPORT *******************************
                 
                 allOffers=arr;
              
         }
         
         
        // [_interesestedOfferIDS release];
     }
     else // FROM ORGANISE OFFER
     {
         allOffers = result;
     }
    
     
     //allOffers = result;
     
     NSLog(@"Offers arr:%d",[allOffers count]);
 
    //if(allOffers.count)
    {
        if(self.offerType==CategoryTypeAll)
        {
            _mechanicalOfferListViewControler.isLoadingFromServer = isFetchingFromServer;
            _mechanicalOfferListViewControler.dataSourceArray = allOffers?[NSMutableArray arrayWithArray:allOffers]:[NSMutableArray array];
        }
        else
        {
          self.mechanicalOffers = [CoreDataHandler mechanicalOffersFromOffers:allOffers];
          self.collissionOffers = [CoreDataHandler collisionOffersFromOffers:allOffers];
            
            NSLog(@"Mech Offers count:%d",[self.mechanicalOffers count]);

            _mechanicalOfferListViewControler.isLoadingFromServer = isFetchingFromServer;
            _collisionOfferListViewControler.isLoadingFromServer = isFetchingFromServer;
            
          _mechanicalOfferListViewControler.dataSourceArray = self.mechanicalOffers;
          _collisionOfferListViewControler.dataSourceArray  = self.collissionOffers;
            
        }
    }
 
      if(!isFetchingFromServer)
      [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
 }];

}


-(void)didSelectOffer:(Offer *)selectedOffer forIndexPath:(NSIndexPath *)indexPath fromOfferList:(NSMutableArray *)offerList
{
  
    NSLog(@"Selected Offer:%@",selectedOffer.offerName);

    OfferDetailViewController *offerDetailController=[[OfferDetailViewController alloc]initWithOfferIndex:indexPath.section offers:offerList];
    offerDetailController.enableMarking = self.visit!=nil;
    offerDetailController.visitedOfferIDs =_visitedOfferIDs;
  
    
    [offerDetailController addDidMarkOffer:^(Offer *offer, NSUInteger index) 
    {
       // NSString *offerIDStr = [offer.offerID description];
        NSString *offerIDStr = [NSString stringWithFormat:@"%d",[offer.offerID integerValue]];
        
        if([_visitedOfferIDs containsObject:offerIDStr])
        {
            [_visitedOfferIDs removeObject:offerIDStr];
        }
        else
        {
            [_visitedOfferIDs addObject:offerIDStr]; 
        }
        
       // app.arrSelectedOfferID=_visitedOfferIDs;
        NSLog(@" _visitedOfferIDs %@",_visitedOfferIDs);
      //  NSLog(@" app.arrSelectedOfferID %@",app.arrSelectedOfferID);

        
        if(_mechanicalOfferListViewControler)
        [_mechanicalOfferListViewControler didMarkOffer:offer atIndex:index];
        if(_collisionOfferListViewControler)
        [_collisionOfferListViewControler didMarkOffer:offer atIndex:index];
    }];
    
    
    [offerDetailController addWillLoadImageForOffer:^UIImage *(Offer *offer)
    {
        NSString *thumbImageURL =(offer.offerImage) ? makeImageURL(offer.offerImage, @"offer", YES) : nil;
        
        NSString *key       = [NSString stringWithFormat:@"%u",[thumbImageURL hash]];
        
        UIImage *thumbmage = [[self imageCache] objectForKey:key];
        
        /*
        if(!thumbmage)
        {
           thumbmage = [LazyImageLoader _imageFromDiskForURL:thumbImageURL];
        }
        
        if(thumbmage)
        [[self imageCache] setObject:thumbmage forKey:key];
        */
        
        return thumbmage;
         
    }];
    
      [self presentOfferDetailController:offerDetailController];
    
    [offerDetailController release];
    
}

-(void)presentOfferDetailController:(OfferDetailViewController *)offerDetailController
{
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:offerDetailController];
   // [self presentModalViewController:navController animated:YES];
    [self presentViewController:navController animated:YES completion:^{}];
    
    [offerDetailController addDidDismissOfferDetail:^{
        
        //[self dismissModalViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:^{}];
    }];
    
    [navController release];

}


#pragma mark FILTER ACTION
-(Customer *)filterOfferForCustomer
{
    return nil;
}
-(void)filterAction:(UIButton *)sender
{
    UIPopoverController *popover = self.filterPopoverController;
    
    if(!popover)
    {
        
    Customer *_parentCustomer=self.visit.parentCustomer;

    FilterOfferViewController *filterOfferVc=[[FilterOfferViewController alloc] initWithCustomer:_parentCustomer];
    filterOfferVc.delegate = self;
    //UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:filterTaskVc];
    UIPopoverController *popover=[[UIPopoverController alloc] initWithContentViewController:filterOfferVc];
    popover.popoverContentSize = CGSizeMake(350.0, 310.0);
    self.filterPopoverController = popover;
    
   // __block CustomerOfferViewController *weakSelf = self;


   /* [filterOfferVc addFilterCallback:^(NSDictionary *filterInfo)
     {
          sender.selected = YES;
         
         NSString *oemName    = [filterInfo objectForKey:@"OEM"];
         NSString *filterType = [filterInfo objectForKey:@"TYPE"];
         
         
         NSPredicate *filterPredicate = nil;
        if(oemName.length)
        {
            //USER TYPE ID=2 FOR OEM
           //filterPredicate = [NSPredicate predicateWithFormat:@"self.userType=%@ AND self.createdByUserName = %@",[NSNumber numberWithInteger:2], oemName];
            
            filterPredicate = [NSPredicate predicateWithFormat:@"self.createdByUserName = %@", oemName];

        }
         
        
        NSSortDescriptor *sortDescriptor = nil;
         
         if([filterType isEqualToString:@"Starting Date"])
         {
             sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"activeFrom" ascending:YES];
         }
         else if([filterType isEqualToString:@"Ending Date"])
         {
             sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"activeTo" ascending:NO];
         }
         
         
         NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
         
         [weakSelf.mechanicalOfferListViewControler filterWithPredicate:filterPredicate sortDescriptos:sortDescriptors];
         [weakSelf.collisionOfferListViewControler filterWithPredicate:filterPredicate sortDescriptos:sortDescriptors];
        
         isSearching=YES;
         
         if(sortDescriptor)[sortDescriptor release];
         
         [sortDescriptors release];
         [weakSelf.filterPopoverController dismissPopoverAnimated:YES];
         
     }]; */
    
   /* [filterOfferVc addCancelCallback:^{
        
        sender.selected = NO;
        isSearching=NO;
        [weakSelf.mechanicalOfferListViewControler resetFilter];
        [weakSelf.collisionOfferListViewControler resetFilter];
        
        [weakSelf.filterPopoverController dismissPopoverAnimated:YES];
        weakSelf.filterPopoverController = nil;
    }]; */
        
    //[navController release];
    [popover release];
    [filterOfferVc release];
        
    }
    
    UIView *fromView = [self showFilterPopoverFromView];
    [self.filterPopoverController presentPopoverFromRect:sender.frame inView:fromView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

-(void)filterViewController:(FilterOfferViewController *)filterVC willFilterWithOptioins:(NSDictionary *)filterOptions
{
    filterButton.selected = YES;
    
    NSString *oemName    = [filterOptions objectForKey:@"OEM"];
    NSString *filterType = [filterOptions objectForKey:@"TYPE"];
    
    
    NSPredicate *filterPredicate = nil;
    if(oemName.length)
    {
        //USER TYPE ID=2 FOR OEM
        //filterPredicate = [NSPredicate predicateWithFormat:@"self.userType=%@ AND self.createdByUserName = %@",[NSNumber numberWithInteger:2], oemName];
        
        filterPredicate = [NSPredicate predicateWithFormat:@"self.createdByUserName = %@", oemName];
        
    }
    
    
    NSSortDescriptor *sortDescriptor = nil;
    
    if([filterType isEqualToString:@"Starting Date"])
    {
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"activeFrom" ascending:YES];
    }
    else if([filterType isEqualToString:@"Ending Date"])
    {
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"activeTo" ascending:NO];
    }
    
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    
    [self.mechanicalOfferListViewControler filterWithPredicate:filterPredicate sortDescriptos:sortDescriptors];
    [self.collisionOfferListViewControler filterWithPredicate:filterPredicate sortDescriptos:sortDescriptors];
    
    isSearching=YES;
    
    if(sortDescriptor)[sortDescriptor release];
    
    [sortDescriptors release];
    [self.filterPopoverController dismissPopoverAnimated:YES];
    
}


-(void)filterViewControllerDidCancel:(FilterOfferViewController *)filterVC
{
    
    filterButton.selected = NO;
   // isSearching=NO;
    
    isSearching = [self.mechanicalOfferListViewControler isSearchModeOn] || [self.collisionOfferListViewControler isSearchModeOn];
    
    [self.mechanicalOfferListViewControler resetFilter];
    [self.collisionOfferListViewControler resetFilter];
    
    [self.filterPopoverController dismissPopoverAnimated:YES];
    self.filterPopoverController = nil;
}



#pragma mark -


-(void)searchWithText:(NSString *)text
{
    if(_mechanicalOfferListViewControler)
        [_mechanicalOfferListViewControler searchWithText:text];
    
    if(_collisionOfferListViewControler)
        [_collisionOfferListViewControler searchWithText:text];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    isSearching = searchText.length>0;
    
    [self searchWithText:searchText];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
   
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    isSearching=NO;
    [self searchWithText:nil];
}


-(void)imageLoaderDidLoad:(LazyImageLoader *)loader
{
    if(_mechanicalOfferListViewControler)
        [_mechanicalOfferListViewControler imageLoaderDidLoad:loader];
    
    if(_collisionOfferListViewControler)
        [_collisionOfferListViewControler imageLoaderDidLoad:loader];
}

-(void)imageLoader:(LazyImageLoader *)loader didFail:(NSError *)error
{
    if(_mechanicalOfferListViewControler)
        [_mechanicalOfferListViewControler imageLoader:loader didFail:error];
    
    if(_collisionOfferListViewControler)
        [_collisionOfferListViewControler imageLoader:loader didFail:error];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
#if LOCAL
    [UIAlertView showWarningAlertWithTitle:@"memory warning" message:@"at Offer view"];
#endif
    
    if(imageCache)
    {
        //[imageCache removeAllObjects];
        [imageCache clear];
    }
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
