//
//  OfferListTableViewCell.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 27/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "OfferListTableViewCell.h"
#import "Offer.h"

@implementation OfferListTableViewCell
@synthesize offerThumbImageView;
@synthesize titleLabel;
@synthesize descriptionLabel;
@synthesize validityLabel;
@synthesize categoryLabel;
@synthesize oemLabel;
@synthesize byLabel;
@synthesize startDateLabel;
@synthesize endDateLabel;
@synthesize markedCheck = _markedCheck;
@synthesize endDateValueLabel,startDateValueLabel;
@synthesize loaderView=_loaderView;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) 
    {
       
        offerThumbImageView =[[UIImageView alloc] init];
        offerThumbImageView.backgroundColor = LOGIN_BG_COLOR;
        offerThumbImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:offerThumbImageView];
        
        
        
        UIImage *checkImage = [UIImage imageNamed:@"rounded_tick.png"];
        CGRect _checkboxFrame = CGRectMake(3.0,3.0 ,checkImage.size.width,checkImage.size.height);
        _markedCheck = [[UIImageView alloc] initWithFrame:_checkboxFrame];
        _markedCheck.image = checkImage;
        [self.contentView addSubview:_markedCheck];
        _markedCheck.hidden = YES;
        //_markedCheck.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
         
              
        
        titleLabel  = [[SOLabel alloc] init];
        titleLabel.numberOfLines = 2;
        titleLabel.textColor = LOGIN_BG_COLOR;
        titleLabel.backgroundColor = [UIColor clearColor];
        //titleLabel.font = [UIFont grotesqueFontOfSize:13.0];
        titleLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
        titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
        
        [self.contentView addSubview:titleLabel];
        
        /*
        descriptionLabel  = [[UILabel alloc] init];
        descriptionLabel.textColor = [UIColor blackColor];
        descriptionLabel.backgroundColor = [UIColor clearColor];
        descriptionLabel.font = [UIFont grotesqueFontOfSize:13.0];
        descriptionLabel.numberOfLines = 2;
        [self.contentView addSubview:descriptionLabel];
        */
        
        
        byLabel  = [[UILabel alloc] init];
        byLabel.textColor = [UIColor blackColor];
        byLabel.backgroundColor = [UIColor clearColor];
        byLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
        byLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [self.contentView addSubview:byLabel];
        
        oemLabel  = [[UILabel alloc] init];
        oemLabel.textColor = [UIColor blackColor];
        oemLabel.backgroundColor = [UIColor clearColor];
        oemLabel.font = [UIFont grotesqueFontOfSize:13.0];
        oemLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [self.contentView addSubview:oemLabel];
        
        categoryLabel  = [[UILabel alloc] init];
        categoryLabel.textColor = [UIColor blackColor];
        categoryLabel.backgroundColor = [UIColor clearColor];
        categoryLabel.font = [UIFont grotesqueFontOfSize:13.0];
        [self.contentView addSubview:categoryLabel];
        
        startDateLabel  = [[UILabel alloc] init];
        startDateLabel.textColor = [UIColor darkGrayColor];
        startDateLabel.backgroundColor = [UIColor clearColor];
        startDateLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
       // startDateLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:startDateLabel];
        
        
        
        startDateValueLabel  = [[UILabel alloc] init];
        startDateValueLabel.textColor = [UIColor darkGrayColor];
        startDateValueLabel.backgroundColor = [UIColor clearColor];
        startDateValueLabel.font = [UIFont grotesqueFontOfSize:13.0];
        startDateValueLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:startDateValueLabel];

        
        endDateLabel  = [[UILabel alloc] init];
        endDateLabel.textColor = [UIColor darkGrayColor];
        endDateLabel.backgroundColor = [UIColor clearColor];
        endDateLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
      //  endDateLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:endDateLabel];
        
        endDateValueLabel  = [[UILabel alloc] init];
        endDateValueLabel.textColor = [UIColor darkGrayColor];
        endDateValueLabel.backgroundColor = [UIColor clearColor];
        endDateValueLabel.font = [UIFont grotesqueFontOfSize:13.0];
        endDateValueLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:endDateValueLabel];
        
        
        validityLabel  = [[UILabel alloc] init];
        validityLabel.textColor = [UIColor darkGrayColor];
        validityLabel.backgroundColor = [UIColor clearColor];
        validityLabel.font = [UIFont grotesqueFontOfSize:13.0];
        validityLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:validityLabel];
        
        _loaderView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _loaderView.hidesWhenStopped = YES;
        [self.contentView addSubview:_loaderView];
        [_loaderView setAutoresizingMasks];
        
        self.contentView.backgroundColor = [UIColor colorWithRed:232.0f/255.0f green:232.0f/255.0f blue:232.0f/255.0f alpha:1.0];
        
        offerThumbImageView.backgroundColor = self.contentView.backgroundColor;
    }
    return self;
}

-(void)dealloc
{
    [offerThumbImageView release];
    [titleLabel release];
    [descriptionLabel release];
    [validityLabel release];
    [_markedCheck release];
    [categoryLabel release];
    [oemLabel release];
    [byLabel release];
    [startDateLabel release];
    [startDateValueLabel release];
    [endDateValueLabel release];
    [endDateLabel release];
    [_loaderView release];
    
    [super dealloc];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect _bounds = self.contentView.bounds;
    
    CGFloat spaceX = 5.0f;
    CGFloat locX   = 0;
    CGFloat locY   = 0;
    
    CGRect offerThumbImageViewFrame = CGRectMake(locX, locY, 117.0f, CGRectGetHeight(_bounds));
    
    locX = CGRectGetMaxX(offerThumbImageViewFrame)+spaceX;
    locY = spaceX;
    
    CGFloat titleWidth    = CGRectGetWidth(_bounds)-locX-spaceX;
    CGSize titleLabelSize = [titleLabel.text sizeWithFont:titleLabel.font constrainedToSize:CGSizeMake(titleWidth, 36) lineBreakMode:UILineBreakModeWordWrap];
    
    //CGRect titleLabelFrame = CGRectMake(locX, locY, titleWidth, MAX(15.0,titleLabelSize.height-17));
    CGRect titleLabelFrame = CGRectMake(locX, locY, titleWidth, MAX(15.0,titleLabelSize.height));
    
    //int numLines = (int)(titleLabelSize.height/15.0);

    locY = CGRectGetHeight(_bounds)-spaceX-15.0;
    CGRect validityLabelFrame = CGRectMake(locX, locY, CGRectGetWidth(_bounds)-locX-spaceX/2.0, 15.0);
    startDateLabel.frame=CGRectMake(locX, locY, 50, 15);
    startDateValueLabel.frame=CGRectMake(locX+50, locY, 50, 15);
    endDateLabel.frame=CGRectMake(CGRectGetMaxX(startDateValueLabel.frame)+20, locY, 40, 15);
    endDateValueLabel.frame=CGRectMake(CGRectGetMaxX(endDateLabel.frame)-1, locY, 50, 15);


    locY =CGRectGetMaxY(titleLabelFrame)+ ((CGRectGetMinY(validityLabelFrame)-CGRectGetMaxY(titleLabelFrame)) -13.2)/2.0;
    
    
    offerThumbImageView.frame = offerThumbImageViewFrame;
    titleLabel.frame = titleLabelFrame;
    byLabel.frame=CGRectMake(locX, locY, 30, 14.5);
    
    CGRect oemLabelFrame = CGRectMake(locX+29, locY, CGRectGetWidth(_bounds)-locX-spaceX, 14.5);
    oemLabel.frame = oemLabelFrame;

        //endDateLabel.frame=CGRectMake(locX, locY, 40, 15);
     validityLabel.frame = validityLabelFrame;
    
 
    
    _loaderView.center = CGPointMake(CGRectGetMidX(offerThumbImageViewFrame), CGRectGetMidY(offerThumbImageViewFrame));
    
}

-(void)setOfferInfo:(Offer *)offer
{
    titleLabel.text    =  offer.offerName;
    byLabel.text=@"By: ";
    oemLabel.text=offer.createdByUserName;
    categoryLabel.text = (offer.categoryID.intValue==0) ? @"ALL" : (offer.categoryID.intValue==1)? @"Mechanical" : @"Collission";
    
    NSString *timeZoneName = app.offerTimeZoneName.length ? app.offerTimeZoneName : @"Australia/Sydney";
    
    NSString *startDate = [NSDate dateStringFromWCFTimeInterval:offer.activeFrom.doubleValue withFormat:OFFER_DATE_FORMAT forTimeZone:timeZoneName];
    
    NSString *endDate   = [NSDate dateStringFromWCFTimeInterval:offer.activeTo.doubleValue withFormat:OFFER_DATE_FORMAT  forTimeZone:timeZoneName];
    
    startDateLabel.text=@"Starts: ";
    startDateValueLabel.text=startDate;
    endDateLabel.text=@"Ends: ";
    endDateValueLabel.text=endDate;
    
    //validityLabel.text  = [NSString stringWithFormat:@"Starts: %@   Ends: %@ ",startDate,endDate];
    
   // _markedCheck.hidden = ![offer.isMarked boolValue];
    
    // [self setNeedsLayout];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    offerThumbImageView.backgroundColor =[UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0]; //LOGIN_BG_COLOR;
    titleLabel.textColor       = selected ? [UIColor whiteColor] : LOGIN_BG_COLOR ;
    descriptionLabel.textColor = selected ? [UIColor whiteColor] : [UIColor blackColor] ;
    validityLabel.textColor    = selected ? [UIColor whiteColor] : [UIColor darkGrayColor];
}

@end
