//
//  FilterOfferViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 28/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "FilterOfferViewController.h"

#import "Rep.h"
#import "WholesaleDealer.h" 
#import "OEM.h"
#import "Customer.h"
#import "UIImage+Resize.h"

@interface FilterOfferViewController ()
{
    NSMutableArray *dataSource;
    
}
@property(nonatomic,assign)Customer *customer;
@property(nonatomic,retain) NSIndexPath *selectedTagIndexPath;
@property(nonatomic,retain) NSIndexPath *selectedFilterTypeIndexPath;

@property(nonatomic,copy)FilterOfferCallback filterCallback;
@property(nonatomic,copy)CancelFilterOfferCallback cancelCallback;

@end

@implementation FilterOfferViewController
@synthesize selectedTagIndexPath;
@synthesize selectedFilterTypeIndexPath;
@synthesize filterCallback,cancelCallback;
@synthesize customer;
@synthesize delegate;

- (id)initWithCustomer:(Customer *)_customer
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) 
    {
        customer=_customer;
        dataSource= [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) 
    {
        dataSource= [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)dealloc
{
    customer=nil;
    delegate = nil;
    [dataSource release];
    [selectedTagIndexPath release];
    [selectedFilterTypeIndexPath release];
    [arrOemName release];
 
    if(filterCallback)
        [filterCallback release];
    
    filterCallback = nil;
    
    if(cancelCallback)
        [cancelCallback release];
    
    cancelCallback = nil;
    
    
    [super dealloc];
}

-(void)setupDataSource
{
    NSArray *arr1=[NSArray arrayWithObjects:@"Starting Date",@"Ending Date", nil];
    
    Rep *_rep = [CoreDataHandler currentRep];
    NSString *dealerGroupName = _rep.parentWdSite.parentWdName;
    NSLog(@"%@",dealerGroupName);
    
    NSMutableArray *tagsArr=[NSMutableArray array];
    
    if(customer)
    {
        // NSArray *arr=[app.strDealingOemForVisitPhoto componentsSeparatedByString:@","];
        
      NSArray *arr=[customer.dealingOEMs componentsSeparatedByString:@","];
        
      NSArray *dealingOEMs = [CoreDataHandler intersectWithDealingOEMs:arr];
    
      if([dealingOEMs count])
      {
        [tagsArr addObjectsFromArray:[dealingOEMs valueForKey:@"name"]];
      }
    }
    else
    {
        NSArray *arrOEM = [CoreDataHandler connectedOEMS];
        
        for(OEM *oem in arrOEM)
        {
            if(oem.name.length)
            {
              [tagsArr addObject:oem.name];
            }
        }
    }
    
    
    
    //============= SORT  ALPHABETICALLY ====================
    [tagsArr sortUsingSelector:@selector(caseInsensitiveCompare:)];
    
    if(dealerGroupName.length)
    {
        [tagsArr addObject:dealerGroupName];
    }
    
    [tagsArr insertObject:@"All" atIndex:0];
    
    [dataSource addObject:arr1];
    [dataSource addObject:tagsArr];
}

-(void)setupFooterView
{
    UIView *tableFooterView=[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 59.0f)];
    
    UIButton *cancelButton=[UIButton repToolCancelButton];
    [cancelButton setTitle:@"RESET" forState:UIControlStateNormal];
    CGRect frm=cancelButton.frame;
    frm.origin = CGPointMake(10.0f, 2.0f);
    cancelButton.frame = frm;
    cancelButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    [tableFooterView addSubview:cancelButton];
    [cancelButton addTarget:self action:@selector(cancelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *saveButton=[UIButton repToolButtonWithText:@"CONFIRM"]; //@"FILTER"
    frm=saveButton.frame;
    frm.origin = CGPointMake(CGRectGetMaxX(cancelButton.frame)+5.0f, CGRectGetMinY(cancelButton.frame));
    saveButton.frame = frm;
    saveButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    [tableFooterView addSubview:saveButton];
    [saveButton addTarget:self action:@selector(filterButtonAction:) forControlEvents:UIControlEventTouchUpInside];

    self.tableView.tableFooterView=tableFooterView;
    [tableFooterView release];
}

-(void)addFilterCallback:(FilterOfferCallback)callBack
{
    //[self.popoverController dismissPopoverAnimated:YES];
    if(callBack) self.filterCallback=callBack;
}

-(void)addCancelCallback:(CancelFilterOfferCallback )callback
{
    if(callback) self.cancelCallback=callback;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.selectedTagIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    self.selectedFilterTypeIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self setupDataSource];
    self.tableView.bounces = NO;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.tableView.sectionHeaderHeight = 10.0f;
        self.tableView.sectionFooterHeight = 10.0;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //self.tableView.contentInset = UIEdgeInsetsMake(0, 10.0, 0, 20.0);
    }
    else
    {
        self.tableView.sectionHeaderHeight = 10.0f;
    }
    
    [self setupFooterView];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.contentSizeForViewInPopover = CGSizeMake(350.0, MAX(310.0,self.tableView.contentSize.height));
}


-(void)cancelButtonAction:(UIButton *)sender
{
    if(self.cancelCallback)
        self.cancelCallback();
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(filterViewControllerDidCancel:)])
    {
        [self.delegate filterViewControllerDidCancel:self];
    }
    
}

-(void)filterButtonAction:(UIButton *)sender
{
    NSString *selectedFilterType = @"";
    NSString *selectedOemName    = @"";
    
    if(self.selectedTagIndexPath.row==0)
    {
        selectedOemName = @"";
    }
    else
    {
       NSArray *rows    = [dataSource objectAtIndex:self.selectedTagIndexPath.section];
       selectedOemName  = [rows objectAtIndex:self.selectedTagIndexPath.row];
    }
    
    
    NSArray *_rows     = [dataSource objectAtIndex:self.selectedFilterTypeIndexPath.section];
    selectedFilterType = [_rows objectAtIndex:self.selectedFilterTypeIndexPath.row];
    
    NSDictionary *fiilterOption = [[NSMutableDictionary alloc] initWithObjectsAndKeys:selectedOemName,@"OEM",selectedFilterType,@"TYPE", nil];
    
    if(self.filterCallback) self.filterCallback(fiilterOption);
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(filterViewController: willFilterWithOptioins:)])
    {
        [self.delegate filterViewController:self willFilterWithOptioins:fiilterOption];
    }
    
    [fiilterOption release];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return [dataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *rows = section < [dataSource count] ? [dataSource objectAtIndex:section] : nil;
    return [rows count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(!cell)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
         cell.textLabel.font = [UIFont grotesqueFontOfSize:16.0];
         cell.textLabel.textColor = [UIColor blackColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    

      if(indexPath.section == 0)
      {
          cell.accessoryType = [self.selectedFilterTypeIndexPath isEqual:indexPath] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
      }
      else if(indexPath.section == 1)
      {
          cell.accessoryType = [self.selectedTagIndexPath isEqual:indexPath] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
      }
    
    NSArray *rows = indexPath.section < [dataSource count] ? [dataSource objectAtIndex:indexPath.section] : nil;
    NSString *title = (indexPath.row < [rows count]) ?  [rows objectAtIndex:indexPath.row] : nil;

    cell.textLabel.text = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [NSString stringWithFormat:@"  %@",title] :   title;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return (section == 1 ) ? 40.0f : self.tableView.sectionHeaderHeight;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *HeaderIdentifier = @"cell_header";
    
    NSString *title = (section == 1 ) ? @"TAGS" : nil;
    
    if(!title) return nil;
    
    UILabel *headerLabel = [tableView dequeueReusableHeaderFooterViewWithIdentifier:HeaderIdentifier];
    if(!headerLabel)
    {
     headerLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 40.0)] autorelease];
     headerLabel.backgroundColor = [UIColor clearColor];
     headerLabel.textColor = [UIColor blackColor];
     headerLabel.font = [UIFont subaruBoldFontOfSize:23.0];
     headerLabel.shadowColor  = [UIColor lightGrayColor];
     headerLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    }
    headerLabel.text =[NSString stringWithFormat:@"    %@",title];
    
    return headerLabel;
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor groupTableViewBackgroundColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.tag = 1;
    
    
    
    NSInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.selectedTagIndexPath isEqual:indexPath] || [self.selectedFilterTypeIndexPath isEqual:indexPath]) return;
    
    NSMutableArray *indexPathsToReload = [[NSMutableArray alloc] init];
    
     if(indexPath.section == 0)
     {
         if(self.selectedFilterTypeIndexPath)
           [indexPathsToReload addObject:self.selectedFilterTypeIndexPath];
         
         self.selectedFilterTypeIndexPath = indexPath;
     }
     else if(indexPath.section == 1)
     {
         if(self.selectedTagIndexPath)
             [indexPathsToReload addObject:self.selectedTagIndexPath];
         
         self.selectedTagIndexPath = indexPath;
     }
    
     [indexPathsToReload addObject:indexPath];
    
    if([indexPathsToReload count])
    [tableView reloadRowsAtIndexPaths:indexPathsToReload withRowAnimation:UITableViewRowAnimationNone];
    [indexPathsToReload release];
}

@end
