//
//  OfferListViewControler.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 27/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerOfferViewController.h"

@class Offer,Visit,LazyImageLoader,SSMemoryCache;
typedef void (^DidSelectOfferCallback) (Offer *selectedOffer,NSIndexPath *indexPath);

@interface OfferListViewControler : UIViewController
{
    UITableView *_tableView;
}

@property(nonatomic,assign) CustomerOfferViewController *parentController;
@property(nonatomic,retain)           NSMutableArray *dataSourceArray;
@property (nonatomic,readonly,retain) UITableView *tableView;
@property (nonatomic,readonly)        CategoryType offerType;
@property (nonatomic,assign)        BOOL isLoadingFromServer;

@property (nonatomic,assign) BOOL showCategory;

@property(nonatomic,assign) Visit *visit;
@property(nonatomic,retain)  NSMutableArray *visitedOfferIDs;
@property (nonatomic,retain) NSPredicate *filterPredicte;
@property (nonatomic,retain) NSPredicate *searchPredicte;
@property (nonatomic,retain) SSMemoryCache *imageCache;

-(BOOL)isSearchModeOn;
-(void)resetSearchMode;


- (id)initWithOffers:(NSMutableArray *)arrayList type:(CategoryType)_offerType;
- (void)addDidSelectOfferCallback:(DidSelectOfferCallback)callback;

-(NSMutableArray *)currentArray;

-(void)sortOffersWithDescriptors:(NSArray *)descriptors;
- (void)searchWithText:(NSString *)searchKey;
-(void)filterWithPredicate:(NSPredicate *)predicte sortDescriptos:(NSArray *)sortDescriptors;
-(void)resetFilter;
-(void)didMarkOffer:(Offer *)offer atIndex:(NSUInteger)index;

-(void)updateOffers:(NSArray *)updatedOffers;
-(void)deleteOffers:(NSArray *)offers;
-(void)insertOffers:(NSArray *)offers;
-(void)didUpdateFromServer;

-(void)imageLoaderDidLoad:(LazyImageLoader *)loader;
-(void)imageLoader:(LazyImageLoader *)loader didFail:(NSError *)error;

@end
