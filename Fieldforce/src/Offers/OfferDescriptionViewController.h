//
//  OfferDescriptionViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 10/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Offer;
@interface OfferDescriptionViewController : UIViewController
{
    UITextView *offerTextView;
}

@property(nonatomic,assign)UINavigationController *navController;
@property (nonatomic,assign) Offer *offer;
- (id)initWithOffer:(Offer *)_offerDict;

-(void)viewOfferImage:(id)sender;

@end
