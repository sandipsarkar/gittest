//
//  CustomerOfferInVisitSessionController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 28/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomerOfferInVisitSessionController.h"
#import "OfferDetailViewController.h"
#import "Visit.h"

@interface CustomerOfferInVisitSessionController ()

@end

@implementation CustomerOfferInVisitSessionController

-(id)initWithCategoryName:(NSString *)catName visit:(Visit *)aVisit
{
    CategoryType _type = CategoryTypeDefault;
    
    NSString *_catNameUpper = [catName uppercaseString];
    
    if([_catNameUpper isEqualToString:@"ALL"])
        _type = CategoryTypeAll;
    
       else if([_catNameUpper isEqualToString:@"MECHANICAL"])
        _type = CategoryTypeMechanical;
    
      else if([_catNameUpper isEqualToString:@"COLLISION"])
        _type = CategoryTypeCollision;
    
      else _type = CategoryTypeOther;
       
    self = [super initWithOfferType:_type];
    
    if(self)
    {
        self.visit = aVisit;
        
        
        //NSLog(@"self.offers = %@", self.offers);
    }
    
    return self;
}

/*
-(Customer *)filterOfferForCustomer
{
    return self.visit.parentCustomer;
}
*/

-(void)setupTopBar
{
    
}

-(BOOL)showCategory
{
    return NO;
}


-(UIView *)showFilterPopoverFromView
{
    return self.navigationController.navigationBar;
}

/*
-(void)didSelectOffer:(Offer *)selectedOffer forIndexPath:(NSIndexPath *)indexPath fromOfferList:(NSMutableArray *)offerList
{
    OfferDetailViewController *offerDetailController=[[OfferDetailViewController alloc]initWithOfferIndex:indexPath.section offers:offerList];
    UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:offerDetailController];
    UINavigationController *parentNavController= [self nextResponderNavigationController];
    [parentNavController presentModalViewController:navController animated:YES];
    [offerDetailController release];
    [navController release];
}
*/

-(void)presentOfferDetailController:(OfferDetailViewController *)offerDetailController
{
    UIInterfaceOrientation _currentOrientation = [UIApplication sharedApplication].statusBarOrientation;
    
    UINavigationController *parentNavController= [self nextResponderNavigationController];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionMoveIn;
    transition.subtype = (_currentOrientation==UIInterfaceOrientationLandscapeLeft) ?  kCATransitionFromRight : kCATransitionFromLeft;
    [parentNavController.view.layer addAnimation:transition forKey:nil];
    
   // UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:offerDetailController];
     offerDetailController.enableMarking = ![self.visit.isVisitCompleted boolValue];
    offerDetailController.customer = self.visit.parentCustomer;
    [parentNavController pushViewController:offerDetailController animated:NO];
     // [parentNavController presentModalViewController:navController animated:YES];
    //[navController release];
    
    
    [offerDetailController addDidDismissOfferDetail:^{
        
        //[navController dismissModalViewControllerAnimated:YES];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.4;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        transition.type = kCATransitionReveal;
        transition.subtype = (_currentOrientation==UIInterfaceOrientationLandscapeLeft) ?  kCATransitionFromLeft : kCATransitionFromRight;
        [offerDetailController.navigationController.view.layer addAnimation:transition forKey:nil];
        [offerDetailController.navigationController popViewControllerAnimated:NO];
         
    }];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar addBackGroundImage:@"right_sub_banner.png"];
    self.navigationItem.titleView = nil;
    self.navigationItem.title = nil;
    self.navigationItem.rightBarButtonItem = nil;
    

    UIBarButtonItem *filterButtonItem=[[UIBarButtonItem alloc] initWithCustomView:self.filterButton];
    self.navigationItem.leftBarButtonItem = filterButtonItem;
    [filterButtonItem release];
}

-(UIButton *)filterButton
{
    UIImage *editButImage = [UIImage imageNamed:@"edit_blank.png"];
    UIButton *filterButton =[UIButton buttonWithType:UIButtonTypeCustom];
    filterButton.frame = CGRectMake(0, 0, editButImage.size.width, editButImage.size.height);
    [filterButton setTitle:@"Filter" forState:UIControlStateNormal];
    filterButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
    [filterButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [filterButton setBackgroundImage:editButImage forState:UIControlStateNormal];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"edit_blank_selected.png"] forState:UIControlStateSelected];
    [filterButton addTarget:self action:@selector(filterAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return filterButton;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}

-(void)markAsComplete
{

    if(_visitedOfferIDs)
    {
         self.visit.visitedOfferIDs = [_visitedOfferIDs componentsJoinedByString:@","];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
