//
//  CustomerOfferViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 27/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#define SCROLL_NOTIFICATION_KEY @"FF_scrollNotificationKey"



@class OfferListViewControler,OfferDetailViewController;
@interface CustomerOfferViewController : UIViewController
{
    OfferListViewControler *_mechanicalOfferListViewControler;
    OfferListViewControler *_collisionOfferListViewControler;
    
    NSMutableArray *  _visitedOfferIDs;
}

@property(nonatomic,readonly) OfferListViewControler *mechanicalOfferListViewControler;
@property(nonatomic,readonly) OfferListViewControler *collisionOfferListViewControler;
@property(nonatomic,readonly) CategoryType offerType;

@property(nonatomic,retain) NSMutableArray *mechanicalOffers;
@property(nonatomic,retain) NSMutableArray *collissionOffers;
@property(nonatomic,retain) NSArray *offers;
@property(nonatomic,assign) Visit *visit;

- (id)initWithOfferType:(CategoryType)type;

//=============== OVERRIDABLE BY SUBCLASS ============
-(void)setupTopBar;
-(void)didSelectOffer:(Offer *)selectedOffer forIndexPath:(NSIndexPath *)indexPath fromOfferList:(NSMutableArray *)offerList;

-(void)presentOfferDetailController:(OfferDetailViewController *)offerDetailController;

-(UIButton *)filterButton;
-(BOOL)showCategory;
-(BOOL)enableMarking;
-(UIView *)showFilterPopoverFromView;
-(Customer *)filterOfferForCustomer;

-(void)fetchOffersFromServer;
-(void)refreshButtonAction;

//=======

-(void)searchWithText:(NSString *)text;


@end
