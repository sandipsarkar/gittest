//
//  OfferDetailViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 18/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "OfferDetailViewController.h"
#import "OfferDescriptionViewController.h"
#import "SSCheckBox.h"
#import "UIImageView+LazyLoading.h"
#import "LazyImageLoader.h"
#import "Offer.h"
#import "ConnectionManager.h"
#import "UIImage+Resize.h"
#import "OfflineManager.h"
#import "Customer.h"
#import "WebLinkViewController.h"
#import "CustomerListViewController.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Rep.h"

#import "SSMemoryCache.h"

#define FORCE_IMAGE_DECOMPRESSION 0



#define OFFER_IMAGE_URL @"http://www.brighthand.com/assets/"

@interface OfferDetailViewController()<MFMailComposeViewControllerDelegate,ImageLoaderDelegate>
{
    CGSize _originalContentSize;
    UIActivityIndicatorView *_activityIndicator;
    UISwipeGestureRecognizer *oneFingerSwipeRight;
    UISwipeGestureRecognizer *oneFingerSwipeLeft;
    
   // NSCache *imageCache;
    
    SSMemoryCache *imageCache;

}

@property (nonatomic,retain) UIBarButtonItem *rightBarButton;
@property (nonatomic,retain) UIBarButtonItem *leftBarButton;
@property (nonatomic,copy)   DidMarkOffer didMarkOffer;
@property (nonatomic,copy)   DidDismissOfferDetail didDismissOfferDetail;
@property (nonatomic,copy)   WillLoadImageForOffer willLoadImageForOffer;
@property (nonatomic,retain) UIPopoverController *customerPopover;
@property (nonatomic,retain) NSString *currentImageUrl;

-(void)viewDescription:(id)sender;

-(void)showPrevOffer;
-(void)showNextOffer;

-(void)showCustomersPopoverAtView:(UIView *)view;
-(void)loadOfferImageForOffer:(Offer *)offer;

@end

@implementation OfferDetailViewController
@synthesize offers;
@synthesize offerIndex;
@synthesize selectedOffer;
@synthesize isPopover;
@synthesize rightBarButton;
@synthesize leftBarButton;
@synthesize didMarkOffer;
@synthesize didDismissOfferDetail;
@synthesize enableMarking;
@synthesize visitedOfferIDs;
@synthesize customer;
@synthesize customerPopover;
@synthesize currentImageUrl;

@synthesize willLoadImageForOffer;

- (id)initWithOfferIndex:(NSUInteger)_offerIndex offers:(NSArray *)_offers
{
    self = [super init];
    
    if (self)
    {
        self.title = @"OFFER";
        
        offers = _offers ? [[NSArray alloc] initWithArray:_offers ] : nil;
        
        self.offerIndex = _offerIndex;
        
        currentIndex=self.offerIndex ;
        NSLog(@"currentIndex = %d", currentIndex);
        
        self.selectedOffer= (currentIndex<[self.offers count] && currentIndex>=0) ? [self.offers objectAtIndex:currentIndex] : nil;
        
        NSLog(@"name selected offer=%@",self.selectedOffer.offerName);
        
        
    }
    return self;
}

-(void)dealloc
{
   //==========================================
   //  [LazyImageLoader cancelAllConnections];
    //=======================================
    
    [offers release];
    [offerImageView release];
    
    [imageCache release];
    
    if(didMarkOffer) [didMarkOffer release];
    if(didDismissOfferDetail) [didDismissOfferDetail release];
    if(willLoadImageForOffer) [willLoadImageForOffer release];

    
    [titleHeader release];
    [_completedCheckBox release];
    [_activityIndicator release];
    
    [visitedOfferIDs release];
    offerIndex=NSNotFound;
    [dateLabel release];
    [selectedOffer release];
    [_offerDescController release];
    [offerContainerView release];
    [rightBarButton release];
    [leftBarButton release];
    [customerPopover release];
    
    [customer release];
    
    [oneFingerSwipeRight release];
    [oneFingerSwipeLeft release];
    
    [currentImageUrl release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
   
    
    // Release any cached data, images, etc that aren't in use.
  
    
    if(imageCache)
       // [imageCache removeAllObjects];
        [imageCache clear];
    
#if LOCAL
        [UIAlertView showWarningAlertWithTitle:@"memory warning" message:@"at Offer detail"];
#endif
    
     [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

-(void)addDidMarkOffer:(DidMarkOffer)callback
{
    self.didMarkOffer = callback;
}



-(void)addDidDismissOfferDetail:(DidDismissOfferDetail)callback
{
    self.didDismissOfferDetail = callback;
}

-(void)addWillLoadImageForOffer:(WillLoadImageForOffer )callback
{
    self.willLoadImageForOffer = callback;
}

-(void)setupRightBarButtonItem
{
    return;
    if(isPopover)
    {
        
    BOOL isMarked=[[self.selectedOffer valueForKey:@"isMarked"] boolValue];
    
    // NEED TO ADD A DONE BUTTON
    UIImage *image=[UIImage imageNamed:@"done_button_popup.png"];
    UIView *rightCustomView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 85+image.size.width+10, 44.0)];
    
   _completedCheckBox=[[SSCheckBox alloc] initWithFrame:CGRectMake(0, 0, 85, 40.0)];
    [_completedCheckBox setBackgroundImageForNormalState:[UIImage imageNamed:@"mark_offer_deselected_button.png"] selectedSate:[UIImage imageNamed:@"mark_offer_button.png"]];
        _completedCheckBox.backgroundColor = [UIColor clearColor];
    _completedCheckBox.autoresizingMask=UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    [_completedCheckBox addTarget:self action:@selector(markOfferAction:) forControlEvents:UIControlEventTouchUpInside];
    _completedCheckBox.checked=isMarked;
    //_completedCheckBox.userInteractionEnabled=!isMarked;
    //_completedCheckBox.alpha=isMarked?0.7:1.0;
    [rightCustomView addSubview:_completedCheckBox];
   
     
 
    //float height=image.size.height;
    UIButton *_backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    _backButton.frame=CGRectMake(CGRectGetMaxX(_completedCheckBox.frame)+8.0, (CGRectGetHeight(rightCustomView.frame)-image.size.height)/2.0+2.0, image.size.width, image.size.height);
    [_backButton setBackgroundImage:image forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [rightCustomView addSubview:_backButton];
   
    UIBarButtonItem *markOfferButton=[[UIBarButtonItem alloc] initWithCustomView:rightCustomView];
    self.navigationItem.rightBarButtonItem=markOfferButton;
    [markOfferButton release];
    [_completedCheckBox release];
    
    [rightCustomView release];
        
    }
    else 
    {
       UIBarButtonItem *_rightBarButton=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(backButtonAction:)];
     self.navigationItem.rightBarButtonItem=_rightBarButton;
       [_rightBarButton release];
    }
    
    self.rightBarButton=self.navigationItem.rightBarButtonItem;
    
    UIBarButtonItem *readBarButton=[[UIBarButtonItem alloc] initWithTitle:@"Read More" style:UIBarButtonItemStyleDone target:self action:@selector(readBarButtonAction:)];
    self.navigationItem.leftBarButtonItem=readBarButton;
    [readBarButton release];
    
    self.leftBarButton=readBarButton;
}

-(void)readBarButtonAction:(id)sender
{
     [self viewDescription:nil];
}

-(SSMemoryCache *)imageCache
{
    if(!imageCache)
    {
       // imageCache = [[NSCache alloc] init];
       // imageCache.name = @"OfferDetail";
        //[imageCache setTotalCostLimit:1024*1024];
       // [imageCache setCountLimit:5];
        //[imageCache setEvictsObjectsWithDiscardedContent:YES];
        
        imageCache = [[SSMemoryCache alloc] init];
        imageCache.cacheCount = 7;
    
    }
    
    return imageCache;
}

-(void)loadOfferImageForOffer:(Offer *)offer
{
    NSString *imageUrl = (offer.offerImage) ? makeImageURLString(offer.offerImage, @"offer", ImageSizeTypeLarge) : nil;
    
    NSLog(@"image URL=%@",imageUrl);
    
    //========================== CANCEL LAST INVISIBLE IMAGE REQUEST =========================
   // if(self.currentImageUrl) [LazyImageLoader cancelRequestForURLString:self.currentImageUrl];
    
    
    self.currentImageUrl = imageUrl;
    
    if(imageUrl)
    {
        NSNumber *urlKey =  [NSNumber numberWithLong:[imageUrl hash]];
        UIImage *image   =  [[self imageCache] objectForKey:urlKey];
        
        BOOL isImageInMemory = image!=nil;
        
        if(!isImageInMemory)
        {
            /*
            NSString *thumbImageURL = (self.selectedOffer.offerImage) ? makeImageURLString(self.selectedOffer.offerImage, @"offer", ImageSizeTypeThumb) : nil;
             offerImageView.image = [LazyImageLoader _imageFromDiskForURL:thumbImageURL];
            */
            
            if(self.willLoadImageForOffer)
            {
              offerImageView.image =  self.willLoadImageForOffer(offer);
            }
           
            
            _activityIndicator.activityIndicatorViewStyle = offerImageView.image ?UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;

            
            [_activityIndicator startAnimating];
            image =  [LazyImageLoader loadImageForUrlString:imageUrl indexPath:nil delegate:self];
            
#if FORCE_IMAGE_DECOMPRESSION
            if(image)
            image = [image decompressedImage];
#endif
        }
        
        if(!image) return;
        
        //if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&([UIScreen mainScreen].scale == 2.0))
        
        if([UIDevice isRetina])
        {
            NSLog(@"Retina");
            
            if(!isImageInMemory)
            image = [UIImage imageWithCGImage:image.CGImage scale:2.0 orientation:image.imageOrientation];
            
           // image = [UIImage imageWithImage:image scaledToMaxWidth:CGRectGetWidth(offerImageView.frame) maxHeight:CGRectGetHeight(offerImageView.frame)];
            
            offerImageView.image = image;
        }
        else
        {
            NSLog(@"non-Retina");
            //image=[image resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(image.size.width/2.0, image.size.height/2.0) interpolationQuality:kCGInterpolationHigh];
            
            if(!isImageInMemory)
            image = [UIImage imageWithImage:image scaledToMaxWidth:CGRectGetWidth(offerImageView.frame) maxHeight:CGRectGetHeight(offerImageView.frame)];
            
            offerImageView.image = image;
        }
        
        [_activityIndicator stopAnimating];
        
         [[self imageCache] setObject:image forKey:urlKey];
    }
    else
    {
      [_activityIndicator stopAnimating];
    }
    
}

-(void)loadOfferImageFormURL:(NSString *)imageUrl
{

    NSLog(@"image URL=%@",imageUrl);
    
    self.currentImageUrl = imageUrl;
    
    if(imageUrl)
    {
        NSNumber *urlKey =  [NSNumber numberWithLong:[imageUrl hash]];
        UIImage *image   =  [[self imageCache] objectForKey:urlKey];
        
        BOOL isImageInMemory = image!=nil;
        
         if(!isImageInMemory)
         {
             [_activityIndicator startAnimating];
             
           image =  [LazyImageLoader loadImageForUrlString:imageUrl indexPath:nil delegate:self];
         }
        
         if(!image) return;
         
         //if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&([UIScreen mainScreen].scale == 2.0))
         if([UIDevice isRetina])
         {
              NSLog(@"Retina");
             
             if(!isImageInMemory)
             image = [UIImage imageWithCGImage:image.CGImage scale:2.0 orientation:image.imageOrientation];
             
            // image = [UIImage imageWithImage:image scaledToMaxWidth:CGRectGetWidth(offerImageView.frame) maxHeight:CGRectGetHeight(offerImageView.frame)];
             
              offerImageView.image = image;
         }
         else 
         {
             NSLog(@"non-Retina");
             //image=[image resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(image.size.width/2.0, image.size.height/2.0) interpolationQuality:kCGInterpolationHigh];
           //if(!isImageInMemory)
           // image = [UIImage imageWithImage:image scaledToMaxWidth:CGRectGetWidth(offerImageView.frame) maxHeight:CGRectGetHeight(offerImageView.frame)];
         
            offerImageView.image = image;
         }
        
        
         [_activityIndicator stopAnimating];
        
        [[self imageCache] setObject:image forKey:urlKey ];
    }
    
}

-(int)sizeOfImage:(UIImage *)_image
{
    return (_image.size.height*_image.size.width*_image.scale);
    
    int height = _image.size.height,
    width = _image.size.width;
    int bytesPerRow = 4*width;
    if (bytesPerRow % 16)
        bytesPerRow = ((bytesPerRow / 16) + 1) * 16;
    int dataSize = height*bytesPerRow;
    
    return dataSize;
}

-(void)imageLoaderDidLoad:(LazyImageLoader *)loader
{
    NSNumber *_urlKey = [NSNumber numberWithLong:[loader.urlString hash]];
    UIImage *image    = loader.image;
    
    if([self.currentImageUrl isEqualToString:loader.urlString])
    {
        
        //if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&([UIScreen mainScreen].scale == 2.0))
        if([UIDevice isRetina])
        {
            NSLog(@"Retina");
            
            if(image)
            {
                image = [UIImage imageWithCGImage:image.CGImage scale:2.0 orientation:image.imageOrientation];
            
#if FORCE_IMAGE_DECOMPRESSION
                image =[image decompressedImage];
#endif
                
                offerImageView.image = image;
            }
        }
        else 
        {
            NSLog(@"non-Retina");
            
            if(image)
            {
                image = [UIImage imageWithImage:image scaledToMaxWidth:CGRectGetWidth(offerImageView.frame) maxHeight:CGRectGetHeight(offerImageView.frame)];
            
               offerImageView.image = image;
            }
        }
    
        [_activityIndicator stopAnimating];
        
        /*
        CATransition *anim = [CATransition animation];
        [anim setDuration:0.3];
        [anim setType:kCATransitionPush];
        [anim setSubtype:kCATransitionFade];
        [anim setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [[offerImageView layer] addAnimation:anim forKey:@"fade in"];
        */
    }

    if(image)
    {
      [[self imageCache] setObject:image forKey:_urlKey ];
    }
}

-(void)imageLoader:(LazyImageLoader *)loader didFail:(NSError *)error
{
    if([self.currentImageUrl isEqualToString:loader.urlString])
     [_activityIndicator stopAnimating];
}


-(void)setupImageContainer
{
    offerContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-58.0)];
    offerContainerView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin;
    offerContainerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:offerContainerView];
    
    
    NSString *offerImageName=nil;//[self.selectedOffer valueForKey:@"OFFERFS"];
    
    //offerImageName=@"offer_full_screen_image.png";
    UIImage *offerImage=[UIImage imageNamed:offerImageName];
    offerImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(offerContainerView.bounds),CGRectGetHeight(offerContainerView.bounds))];
    //offerImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, offerImage.size.width,offerImage.size.height)];
    offerImageView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin;
    offerImageView.backgroundColor=[UIColor clearColor];
    //offerImageView.contentMode=UIViewContentModeScaleAspectFit;
    offerImageView.userInteractionEnabled=YES;
    offerImageView.image=offerImage;
    [offerContainerView addSubview:offerImageView];
    
    //NSLog(@"OfferImageFrmae:%@",NSStringFromCGRect(offerImageView.bounds));
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    _activityIndicator.hidesWhenStopped = YES;
    [offerImageView addSubview:_activityIndicator];
    _activityIndicator.center = offerImageView.center;
    _activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    [_activityIndicator startAnimating];
    
   
    

    /*
     NSString *imageURL = (self.selectedOffer.offerImage) ? makeImageURLString(self.selectedOffer.offerImage, @"offer", ImageSizeTypeLarge) : nil;

    NSLog(@"Offer Image URL:%@",imageURL);
    
    if(imageURL)
    {
        [self loadOfferImageFormURL:imageURL];
    }
    else 
    {
        [_activityIndicator stopAnimating];
    }
    */
    
    [self loadOfferImageForOffer:self.selectedOffer];
    
    
    
    // ----------------------------------
	// Double tap to flip horizontally
	// ----------------------------------
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapGestureAction:)];
    tapGesture.numberOfTapsRequired=2;
    [offerContainerView addGestureRecognizer:tapGesture]; 
    [tapGesture release];
    
    /*
    UITapGestureRecognizer *singleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureAction:)];
    singleTapGesture.numberOfTapsRequired=1;
    [offerContainerView addGestureRecognizer:singleTapGesture]; 
    [singleTapGesture release];
    */

}

-(void)setupFooterView
{
    BOOL isMarked=[visitedOfferIDs containsObject:[self.selectedOffer.offerID description]];
    //[[self.selectedOffer valueForKey:@"isMarked"] boolValue];
    
    UIImage *footerImage =  [UIImage imageNamed:@"offer_banner_bottom.png"];
    UIImageView *footerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,CGRectGetHeight(self.view.bounds)-footerImage.size.height,CGRectGetWidth(self.view.bounds),footerImage.size.height)];
    footerImageView.image = footerImage;
    footerImageView.backgroundColor=[UIColor clearColor];
    footerImageView.userInteractionEnabled = YES;
    footerImageView.autoresizingMask=UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:footerImageView];
    
    UIImage *pageButtonImage = [UIImage imageNamed:@"offer_details_blank_button.png"];
    nextPageButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [nextPageButton setBackgroundImage:pageButtonImage forState:UIControlStateNormal];
    nextPageButton.frame = CGRectMake(3.0, 1.0, pageButtonImage.size.width, pageButtonImage.size.height);
    
    [nextPageButton setTitle:@"Offer\nDetails" forState:UIControlStateNormal];
    [nextPageButton setTitle:@"Offer\nImage" forState:UIControlStateSelected];
    nextPageButton.titleLabel.numberOfLines = 2.0;
    nextPageButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    nextPageButton.titleLabel.textAlignment = UITextAlignmentCenter;
    nextPageButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
    [footerImageView addSubview:nextPageButton];
    [nextPageButton addTarget:self action:@selector(viewDescription:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //==============
    
   // UIImage *emailButtonImage = [UIImage imageNamed:@"offer_details_blank_button.png"];
    pdfButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [pdfButton setBackgroundImage:pageButtonImage forState:UIControlStateNormal];
    pdfButton.frame = CGRectMake(CGRectGetMaxX(nextPageButton.frame)+5.0, 1.0, pageButtonImage.size.width, pageButtonImage.size.height);
    [pdfButton setTitle:@"View\nPDF" forState:UIControlStateNormal];
    pdfButton.titleLabel.numberOfLines = 2.0;
    pdfButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    pdfButton.titleLabel.textAlignment = UITextAlignmentCenter;
    pdfButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
    [footerImageView addSubview:pdfButton];
    [pdfButton addTarget:self action:@selector(openPDFAction:) forControlEvents:UIControlEventTouchUpInside];
    
    pdfButton.hidden = !self.selectedOffer.offerPdf.length;

    //============
    
    
    _completedCheckBox=[[SSCheckBox alloc] initWithFrame:CGRectMake(CGRectGetWidth(footerImageView.bounds)-(89.0+3.0), 2, 89, CGRectGetHeight(footerImageView.bounds)-2.0)];
    [_completedCheckBox setBackgroundImageForNormalState:[UIImage imageNamed:@"mark_offer_blank_button.png"] selectedSate:[UIImage imageNamed:@"mark_offer_blank_button_checked.png"]];
    _completedCheckBox.backgroundColor  = [UIColor clearColor];
    _completedCheckBox.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [_completedCheckBox addTarget:self action:@selector(markOfferAction:) forControlEvents:UIControlEventTouchUpInside];
    _completedCheckBox.checked=isMarked;
    [footerImageView addSubview:_completedCheckBox];
     _completedCheckBox.hidden = !self.enableMarking;
    
    UILabel *markOfferLabel = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 0, 35.0, CGRectGetHeight(_completedCheckBox.bounds))];
    markOfferLabel.numberOfLines = 2;
    markOfferLabel.textColor = [UIColor whiteColor];
    markOfferLabel.backgroundColor = [UIColor clearColor];
    markOfferLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
    [_completedCheckBox addSubview:markOfferLabel];
    markOfferLabel.text =@"Mark Offer";
    [markOfferLabel release];
    
    
    NSString *endDateString = [NSDate dateStringFromWCFTimeInterval: self.selectedOffer.activeTo.doubleValue withFormat:OFFER_DATE_FORMAT];
    dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(nextPageButton.bounds)+5.0,0,CGRectGetWidth(self.view.bounds)-CGRectGetWidth(_completedCheckBox.bounds)*2,CGRectGetHeight(footerImageView.bounds))];
    dateLabel.backgroundColor  = [UIColor clearColor];
    dateLabel.textColor        = [UIColor whiteColor];
    dateLabel.textAlignment    = NSTextAlignmentCenter;
    dateLabel.font             = [UIFont subaruBoldFontOfSize:23.0];
    dateLabel.shadowColor      = [UIColor colorWithWhite:0.1 alpha:0.5];
    dateLabel.shadowOffset     = CGSizeMake(1.0, 2.0);
    dateLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    [footerImageView addSubview:dateLabel];
    
    dateLabel.text=[NSString stringWithFormat:@"OFFER EXPIRES %@", [endDateString uppercaseString]];
    //[footerBar release];
    [footerImageView release];
}

-(void)setupEmailButton
{
    UIBarButtonItem *emailBarButton = nil;
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
    
    UIButton *emailButton=[UIButton repToolButtonWithImageName:@"email_button_blank.png" text:@"Email"];
    emailButton.titleLabel.textColor = [UIColor whiteColor];
    emailButton.titleLabel.font = [UIFont subaruBoldFontOfSize:16.0];
    [emailButton addTarget:self action:@selector(emailPDFAction:) forControlEvents:UIControlEventTouchUpInside];
    emailBarButton = [[UIBarButtonItem alloc] initWithCustomView:emailButton];
    self.navigationItem.rightBarButtonItem = emailBarButton;
    [emailBarButton release];
    
}
else
{
    emailBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Email" style:UIBarButtonItemStyleDone target:self action:@selector(emailPDFAction:)];
    self.navigationItem.rightBarButtonItem = emailBarButton;
    [emailBarButton release];
}
     
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.backgroundColor=[UIColor whiteColor];
    [self.navigationController.navigationBar addBackGroundImage:@"navigation_bar.png"];

    /*
    self.navigationController.navigationBarHidden=NO;
    //[self.navigationItem setHidesBackButton:YES animated:YES];

    if(!isPopover)
    {
      [self.navigationController.navigationBar addBackGroundImage:@"navigation_bar.png"];
    }
    else 
    {
        [self.navigationController.navigationBar hideBackgroundImage];
       // self.navigationController.navigationBar.tintColor=[UIColor blueColor];
    }
    self.title=@"OFFER";
    */
    
    [self addBackButtonWithSelector:@selector(backButtonAction:)];
    
    self.selectedOffer=self.offerIndex < [self.offers count] ?  [self.offers objectAtIndex:self.offerIndex] : nil;
    
    NSString *title=[NSString stringWithFormat:@"%@ Offer: %@",self.selectedOffer.createdByUserName,self.selectedOffer.offerName ];

    float  maxWidth= self.navigationController.navigationBar.frame.size.width-150.0;
    titleHeader=[[UILabel alloc] initWithFrame:CGRectMake(0, 0,maxWidth, 44.0)];
    titleHeader.textColor=[UIColor whiteColor];
    titleHeader.backgroundColor=[UIColor clearColor];
    titleHeader.shadowColor = [UIColor colorWithWhite:0.1 alpha:0.5];
    titleHeader.shadowOffset = CGSizeMake(1.0, 2.0);
    titleHeader.textAlignment = NSTextAlignmentCenter;
    titleHeader.font=[UIFont subaruBoldFontOfSize:23.0];
    titleHeader.text=title;
    //[self.selectedOffer valueForKey:@"NAME"];
    self.navigationItem.titleView=titleHeader;
    
   // [self setupRightBarButtonItem];
    //=================== Email Button ================//
    [self setupEmailButton];
    //================================================//
    
    [self setupImageContainer];
    [self setupFooterView];
    
    oneFingerSwipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onOneFingerSwipeLeft:)];
    [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [offerContainerView addGestureRecognizer:oneFingerSwipeRight];
   
	// --------------------------------
	// One finger, swipe left
	// --------------------------------
    oneFingerSwipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onOneFingerSwipeRight:)];
    [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [offerContainerView addGestureRecognizer:oneFingerSwipeLeft]; 
   
    

    // USER TYPE == 2 FOR OEM
    if([self.selectedOffer.userType intValue]==2)
    [[OfflineManager sharedManager] addViewedOffer:self.selectedOffer];

    
   }

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //[LazyImageLoader cancelAllConnections];
}

-(void)onOneFingerSwipeLeft:(UISwipeGestureRecognizer *)recognizer
{
    if(currentIndex<=0) return;
    
    CATransition *animation = [CATransition animation];
    [animation setDelegate:self];
    [animation setDuration:0.40f];
    [animation setTimingFunction: UIViewAnimationCurveEaseInOut];
    [animation setType: kCATransitionPush];
    [animation setSubtype: kCATransitionFromLeft];
    [[offerImageView layer] addAnimation:animation forKey:@"transitionViewAnimation"];
    
    //[webView  exchangeSubviewAtIndex:0 withSubviewAtIndex:1];
    //[self previousInfoChunk]; 
    
    [self showPrevOffer];
}

-(void)onOneFingerSwipeRight:(UISwipeGestureRecognizer *)recognizer
{
    NSUInteger totalOffersCount=self.offers.count;
    if(currentIndex>=totalOffersCount-1) return;
    
    CATransition *animation = [CATransition animation];
    [animation setDelegate:self];
    [animation setDuration:0.40f];
    [animation setTimingFunction: UIViewAnimationCurveEaseInOut];
    [animation setType: kCATransitionPush];
    [animation setSubtype: kCATransitionFromRight];
    [[offerImageView layer] addAnimation:animation forKey:@"transitionViewAnimation"];
    
    
    //[webView  exchangeSubviewAtIndex:0 withSubviewAtIndex:1];
   // [self nextInfoChunk];

    [self showNextOffer];
}

-(void)doubleTapGestureAction:(UITapGestureRecognizer *)recognizer
{
    
    if(recognizer.state==UIGestureRecognizerStateEnded)
    {
        if(recognizer.numberOfTapsRequired == 2)
        [self viewDescription:nil];
    }
}


-(void)singleTapGestureAction:(UITapGestureRecognizer *)recognizer
{
    _completedCheckBox.checked=YES;
}

-(void)handleCheckboxSelection
{
    //BOOL isMarked=[[self.selectedOffer valueForKey:@"isMarked"] boolValue];
     BOOL isMarked=[visitedOfferIDs containsObject:[self.selectedOffer.offerID description]];
    _completedCheckBox.checked=isMarked;
    //_completedCheckBox.userInteractionEnabled=!isMarked;
    //_completedCheckBox.alpha=isMarked?0.7:1.0;
}

-(void)showPrevOffer
{
    currentIndex--;
    
    currentIndex=MAX(0,currentIndex);
    
    NSLog(@"Prev=%d",currentIndex);
    
     if(currentIndex < [self.offers count] && currentIndex>=0)
     {
        self.selectedOffer=  [self.offers objectAtIndex:currentIndex];
         
         NSString *endDateString =[NSDate dateStringFromWCFTimeInterval: self.selectedOffer.activeTo.doubleValue withFormat:OFFER_DATE_FORMAT];
         
         dateLabel.text=[NSString stringWithFormat:@"Expires on %@", endDateString];
        // titleHeader.text=self.selectedOffer.offerName;
         //offerImageView.image = [UIImage imageNamed:[self.selectedOffer valueForKey:@"OFFERFS"]];

        NSString *strOfferName=[NSString stringWithFormat:@"%@ Offer: %@",self.selectedOffer.createdByUserName,self.selectedOffer.offerName ];
         titleHeader.text=strOfferName;
         
         pdfButton.hidden = !self.selectedOffer.offerPdf.length;
         
         [_activityIndicator startAnimating];
         //Get the url for offer image and load the image lazily here
        // NSString *imageURL = [self.selectedOffer valueForKey:@"imageURL"];
         
         offerImageView.image=nil;
        
         
         
        // NSString *imageURL = (self.selectedOffer.offerImage) ? makeImageURLString(self.selectedOffer.offerImage, @"offer", ImageSizeTypeLarge) : nil;

         //[OFFER_IMAGE_URL stringByAppendingString:self.selectedOffer.offerImage];
         
       //  [self loadOfferImageFormURL:imageURL];
         
         [self loadOfferImageForOffer:self.selectedOffer];
         /////////////////////////////////////////////////////////////
         
        [self handleCheckboxSelection];
     }
    
 
}

-(void)showNextOffer
{
    NSUInteger totalOffersCount = [self.offers count];
    currentIndex++;
    currentIndex = MIN(currentIndex,totalOffersCount);
    
    NSLog(@"Next=%d",currentIndex);
    
    if(currentIndex < [self.offers count]  && currentIndex>=0)
    {
        self.selectedOffer=  [self.offers objectAtIndex:currentIndex];
        
          NSString *endDateString =[NSDate dateStringFromWCFTimeInterval: self.selectedOffer.activeTo.doubleValue withFormat:OFFER_DATE_FORMAT];
        
        dateLabel.text=[NSString stringWithFormat:@"Expires on %@", endDateString];
        //titleHeader.text=self.selectedOffer.offerName;
        NSString *strOfferName=[NSString stringWithFormat:@"%@ Offer: %@",self.selectedOffer.createdByUserName,self.selectedOffer.offerName ];
        titleHeader.text=strOfferName;
   
        BOOL _isPdfAvailable = self.selectedOffer.offerPdf.length>0;
        pdfButton.hidden = !_isPdfAvailable;
        
        [_activityIndicator startAnimating];
        
        offerImageView.image=nil;
     
       // NSString *imageURL = (self.selectedOffer.offerImage) ? makeImageURLString(self.selectedOffer.offerImage, @"offer", ImageSizeTypeLarge) : nil;
        
      // [self loadOfferImageFormURL:imageURL];
        
        [self loadOfferImageForOffer:self.selectedOffer];
        /////////////////////////////////////////////////////////////
        
        [self handleCheckboxSelection];
    }
    
 
}

-(void)backButtonAction:(UIButton *)sender
{
    if([sender isKindOfClass:[UIButton class]])
    sender.userInteractionEnabled = NO;
    
      [LazyImageLoader cancelAllConnections];
    
    //if(imageCache)
     //   [imageCache removeAllObjects];
    
    if(self.customerPopover && self.customerPopover.isPopoverVisible)
    {
        [self.customerPopover dismissPopoverAnimated:NO];
    }
    
    
    if(self.didDismissOfferDetail) self.didDismissOfferDetail();
    
}

-(void)markOfferAction:(SSCheckBox *)checkBox
{
    
    checkBox.checked = !checkBox.isChecked;
    
   // BOOL checked=checkBox.isChecked;
    
  // [self.selectedOffer setValue:[NSNumber numberWithBool:checked] forKey:@"isMarked"];
    
    NSLog(@"%@",self.selectedOffer);
    
    if(self.didMarkOffer) self.didMarkOffer(self.selectedOffer,currentIndex);
}

-(void)viewDescription:(id)sender
{
    isOnDescriptionPage=!isOnDescriptionPage;
    
    if(isOnDescriptionPage)
    {
     if(!_offerDescController)
     {
         _offerDescController=[[OfferDescriptionViewController alloc] initWithOffer:self.selectedOffer];
         [self addChildViewController:_offerDescController];
          _offerDescController.navController=self.navigationController;
         _offerDescController.view.frame=offerImageView.frame;
         [offerContainerView addSubview:_offerDescController.view];
         [_offerDescController didMoveToParentViewController:self];
         
     }
    }
    else 
    {
        if(_offerDescController)
        {
            [_offerDescController didMoveToParentViewController:nil];
            [_offerDescController.view removeFromSuperview];
            [_offerDescController removeFromParentViewController];
            [_offerDescController release];
            _offerDescController = nil;
        }
    }
    
    [offerContainerView  animateWithTransition:isOnDescriptionPage ?  UIViewAnimationTransitionFlipFromLeft : UIViewAnimationTransitionFlipFromRight duration:0.5];
    
    
    nextPageButton.selected = isOnDescriptionPage;
    
    oneFingerSwipeRight.enabled = !isOnDescriptionPage;
    oneFingerSwipeLeft.enabled = oneFingerSwipeRight.enabled;
}

#pragma mark -
#pragma mark email PDF
-(void)emailPDFAction:(id)sender
{
    NSMutableArray *receipents=[NSMutableArray array];
    if (self.customer) 
    {
        if(self.customer.email.length)
        {
          [receipents addObject:self.customer.email];
        }
        else 
        {
            NSString *message = [NSString stringWithFormat:OFFER_EMAIL_ALERT_FORMAT,self.customer.name];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];  
            [alert release]; 
        }
        
        [self performSelector:@selector(showEmailClientWithToRecipents:) withObject:receipents];
    }
    else
    {
        [self showCustomersPopoverAtView:sender];
    }
}


-(void)showCustomersPopoverAtView:(UIView *)view
{
    UIPopoverController *popover=self.customerPopover;
    
    if(!popover)
    {
        
        
        CustomerListViewController *_customerListViewController=[[CustomerListViewController alloc] initWithCustomerName:@""];
        _customerListViewController.allowMultipleSelection=YES;
        
        /*
        [_customerListViewController addCustomerSelectCallback:^(Customer *_customer)
         {
             NSMutableArray *receipents=[NSMutableArray array];
              if(_customer.email.length)
              [receipents addObject:_customer.email];
             [self performSelector:@selector(showEmailClientWithToRecipents:) withObject:receipents];
             
             [self.customerPopover dismissPopoverAnimated:NO];
         }];
        */
        
       __block OfferDetailViewController *weakSelf = self;
        [_customerListViewController addDidSelectCustomersCallback:^(NSMutableArray *selectedCustomers) {
            
            NSMutableArray *nameOfCustomers = [NSMutableArray array];
             NSMutableArray *receipents     = [NSMutableArray array];
            for(Customer *_customer in selectedCustomers)
            {
                if(_customer.email.length)
                {
                    NSLog(@"Email=%@",_customer.email);
                    [receipents addObject:_customer.email];
                }
                else 
                {
                    if(_customer.name.length)
                    [nameOfCustomers addObject:_customer.name];
                }
            }
            
            if([nameOfCustomers count])
            {
                NSString *_lastCustomerName  = [nameOfCustomers lastObject];
                NSString *lastCustomerName   = [_lastCustomerName copy];
                NSMutableArray* nameOfCustomersTempArray = [nameOfCustomers copy];
                [nameOfCustomers removeObject:_lastCustomerName];
            
                NSString *customersNameStr = ([nameOfCustomers count]>=1)? [nameOfCustomers componentsJoinedByString:@", "] : nil;
                
                customersNameStr = customersNameStr.length ?  [NSString stringWithFormat:@"%@ and %@",customersNameStr,lastCustomerName] : lastCustomerName;
                
                
                
                NSLog(@"Customers name=%@",customersNameStr);
                
                NSString *message = [NSString stringWithFormat:([nameOfCustomersTempArray count]>1)?OFFER_EMAIL_ALERT_FORMAT_MULTIPLE:OFFER_EMAIL_ALERT_FORMAT,customersNameStr];
                [nameOfCustomersTempArray release];
                [lastCustomerName release];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];  
                [alert release];  
            }
            
            [weakSelf performSelector:@selector(showEmailClientWithToRecipents:) withObject:receipents];
            [weakSelf.customerPopover dismissPopoverAnimated:NO];
            weakSelf.customerPopover=nil;

        }];
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:_customerListViewController];
        popover=[[UIPopoverController alloc] initWithContentViewController:navController];
        popover.popoverContentSize = CGSizeMake(335.0+15.0, 800.0);
        
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        popover.popoverBackgroundViewClass = [MBPopoverBackgroundView class];

        
        self.customerPopover = popover;
        
        [popover release];
        [navController release];
        [_customerListViewController release];
    }
    
  
    if([view isKindOfClass:[UIBarButtonItem class]])
    {
        [popover presentPopoverFromBarButtonItem:(UIBarButtonItem *)view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else 
    {
        
      CGRect targetRect = view.frame;
    
//if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
//{
     // CGRect rect = targetRect;
     // rect.size.width = 4	;
     // popover.popoverLayoutMargins = UIEdgeInsetsMake(rect.origin.x, 0, 0, 0);
//}

        
    [popover presentPopoverFromRect:targetRect inView:view.superview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }

}

-(NSString *)setupMailBody
{
    /*
     NSString *_formatString =  @"<html><body style=\"padding:0 0 0 0; margin:0 auto;\"> <table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td style=\"padding:0 0 0 0; margin:0 0 0 0; width:10px;\"></td> <td><table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td style=\"padding:0 0 0 0; margin:0 0 0 0; height:10px;\"></td> </tr> <tr> <td style=\"padding:0 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;\">Hi,</td> </tr> <tr> <td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;\">As your business <span style=\"color:#175999;\">'Customer Business Name'</span> has been performing exceptionally well, we are delighted to inform you that you have been entitled to the offer <span style=\"color:#175999;\">'Offer Name'</span>. Please find the details of the offer below</td> </tr> <tr> <td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;\"><img src=\"%@\" alt=\"\" title=\"\" />Offer Image</td> </tr> <tr> <td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;\">\"%@\"</td> </tr> <tr> <td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;\">To view pdf: <a href=\"%@\" style=\"color:#175999;\">Click here</a></td> </tr> <tr> <td style=\"padding:0 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;\">For further information: <a href=\"%@\" style=\"color:#175999;\">URL</a></td> </tr> <tr> <td style=\"padding:0 0 0 0; margin:0 0 0 0; height:10px;\"></td> </tr> </table></td> <td style=\"padding:0 0 0 0; margin:0 0 0 0;  width:10px;\"></td> </tr> </table> </body> </html>";
     */
    
    //NSString *customerName = self.customer ? self.customer.name : @"";
    NSString *offerName    = self.selectedOffer.offerName;
    
    // NSString *imageURL    = self.selectedOffer.offerImage? makeImageURL(self.selectedOffer.offerImage, @"offer", NO):nil;
    NSString *imageURL     = self.selectedOffer.offerImage? makeImageURLString(self.selectedOffer.offerImage, @"offer", ImageSizeTypeMediumLarge):nil;
    NSLog(@"Large Medium image url=%@",imageURL);
    
    NSString *offerDescription = self.selectedOffer.offerDescription;
    NSString *pdfURL  = self.selectedOffer.offerPdf;
    NSString *infoURL = self.selectedOffer.offerLinks;
    
    /*
     NSString *_formatString=[NSString stringWithFormat:@"Hi\n\nPlease find attached details for the '%@'.",offerName];
     NSMutableString *_formatStr= [NSMutableString stringWithString:_formatString];
     if(offerDescription.length)
     {
     [_formatStr appendFormat:@"\n\nMore information can be found below:\n%@",offerDescription];
     }
    */
    
    NSString *_formatString =  @"<html><body style=\"padding:0 0 0 0; margin:0 auto;\"> <table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td style=\"padding:0 0 0 0; margin:0 0 0 0; width:10px;\"></td> <td><table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td style=\"padding:0 0 0 0; margin:0 0 0 0; height:10px;\"></td> </tr> <tr> <td style=\"padding:0 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\">Hi,</td> </tr>";
    
    NSMutableString *_formatStr= [NSMutableString stringWithString:_formatString];
    
    
    //    NSString *customerString = customerName.length? [NSString stringWithFormat:@"<tr><td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\">As your business <span style=\"color:#175999;\">'%@'</span> has been performing exceptionally well, we are delighted to inform you that you have been entitled to the offer <span style=\"color:#175999;\">'%@'</span>. Please find the details of the offer below</td> </tr>",customerName,offerName] :  [NSString stringWithFormat:@"<tr><td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\">As your business <span style=\"color:#175999;\"></span> has been performing exceptionally well, we are delighted to inform you that you have been entitled to the offer <span style=\"color:#175999;\">'%@'</span>. Please find the details of the offer below</td> </tr>",offerName] ;
    
    NSString *customerString = [NSString stringWithFormat:@"<tr><td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\"> <span style=\"color:#175999;\"></span> Please find below offer information: <span style=\"color:#175999;\">%@</span></td> </tr>",@""] ;
    
    
    [_formatStr appendString:customerString];
    
    
    if(imageURL.length)
    {
        NSString *imageLinkStr =[NSString stringWithFormat:@"<tr><td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\"><img src=\"%@\" alt=\"\" title=\"\" /></td> </tr>",imageURL];
        
        [_formatStr appendString:imageLinkStr];
    }
    
    
    
    if(offerDescription.length)
    {
        NSString *descStr =[NSString stringWithFormat:@"<tr><td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\"> </td> </tr><tr><td style=\"padding:10px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\">%@</td> </tr>",offerDescription];
        
        [_formatStr appendString:descStr];
    }
    
    //============ INFO URL MOVED TO TOP FROM BOTTOM ==================
    
    if(infoURL.length)
    {
        if([infoURL rangeOfString:@"http://"].location==NSNotFound)
        {
            infoURL = [@"http://" stringByAppendingString:infoURL];
        }
        
        NSString *furtherInfoStr=[NSString stringWithFormat:@"<tr><td style=\"padding:10px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\">&nbsp;</td> </tr><tr><td style=\"padding:0 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\">For more information please visit: <a href=\"%@\" style=\"color:#175999;\">%@</a></td> </tr> <tr>",infoURL,infoURL];
        
        [_formatStr appendString:furtherInfoStr];
        
    }

    
    if(pdfURL.length)
    {
        NSString *pdfLink = makePDFURL(pdfURL, @"offer");
        NSString *pdfStr=[NSString stringWithFormat:@"<tr><td style=\"padding:10px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\">&nbsp;</td> </tr><tr><td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\">To download related documents please <a href=\"%@\" style=\"color:#175999;\">click here</a></td> </tr>",pdfLink];
        [_formatStr appendString:pdfStr]; 
    }
    
    
    
        
    
    NSString *endStr=@"<td style=\"padding:0 0 0 0; margin:0 0 0 0; height:10px;\"></td> </tr> </table></td> <td style=\"padding:0 0 0 0; margin:0 0 0 0;  width:10px;\"></td> </tr> </table> </body> </html>";
    [_formatStr appendString:endStr];
    
    
    
    return _formatStr;
}


#pragma mark -
#pragma mark email Client
-(void)showEmailClientWithToRecipents:(NSArray *)receipents
{
    //if(![receipents count]) return;
    Class mailclass = (NSClassFromString(@"MFMailComposeViewController"));
    if(mailclass!=nil && [mailclass canSendMail])
    {
        
        // NSString *imageURL     = self.selectedOffer.offerImage? makeImageURLString(self.selectedOffer.offerImage, @"offer", ImageSizeTypeLarge):nil;
    
    
        ProgressHUD *progress = [ProgressHUD showHUDAddedTo:self.view animated:YES];
        progress.mode = MBProgressHUDModeIndeterminate;
        progress.tag  = NSIntegerMax-1;
        progress.labelText = @"Attaching...";   
        
   // [offerImageView loadImageForUrl:imageURL completionHandler:^(UIImage *image, NSString *urlString, NSError *error)
   //  {
        
      //   if((!error && image) && [imageURL isEqualToString:urlString])
      //   {
             NSString *strOfferName=[NSString stringWithFormat:@"%@ Offer: %@",self.selectedOffer.createdByUserName,self.selectedOffer.offerName ];
             NSString *mailSubject =  strOfferName;
             NSString *mailBody    = [self setupMailBody];
             
             NSLog(@"mailbody = %@", mailBody);

             
             MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
             picker.mailComposeDelegate=self;
             picker.navigationBar.tintColor=[UIColor blackColor];
             [picker setSubject:mailSubject];
             [picker setMessageBody:mailBody isHTML:YES];
        
             ///[picker setToRecipients:receipents];
        
            if([receipents count]>1)
            {
                //Rep *_rep = [CoreDataHandler currentRep];
                NSString *dealersiteName = [CoreDataHandler currentRep].parentWdSite.name;
                
             [picker setToRecipients:[NSArray arrayWithObject:[NSString stringWithFormat:OFFER_TO_EMAILID_FORMAT,dealersiteName]]];
             [picker setBccRecipients:receipents];
            }
            else
            {
               [picker setToRecipients:receipents];
            }
        
            // NSString *mimeType = [[imageURL lastPathComponent] pathExtension];
            // NSData *imageDate = (mimeType && [mimeType isEqualToString:@"png"])?  UIImagePNGRepresentation(image) : UIImageJPEGRepresentation(image, 1.0);
             
          //   NSString *attachedFileName = [imageURL lastPathComponent];
         //    [picker addAttachmentData:imageDate mimeType:[NSString stringWithFormat:@"image/%@",mimeType] fileName:attachedFileName.length?attachedFileName:@"offerImage"];
             
            
             [self presentModalViewController:picker animated:YES];
             [picker release];   
             
        // }
         
         [ProgressHUD hideHUDForView:self.view animated:YES];
   // }];
       
            
    }
    else
    {
        [UIAlertView showWarningAlertWithTitle:@"" message:NO_EMAIL_CLIENT_MESSAGE];
    }
    
    /*
 ///================================== SEND MAIL BY WEBSERVICE ===========================================
    
    ProgressHUD *progress = [ProgressHUD showHUDAddedTo:self.view animated:YES];
    progress.mode = MBProgressHUDModeIndeterminate;
    progress.tag  = NSIntegerMax-1;
    progress.labelText = @"Sending...";
    
    app.window.userInteractionEnabled = NO;
    
     NSString *strOfferName=[NSString stringWithFormat:@"%@: %@",self.selectedOffer.createdByUserName,self.selectedOffer.offerName ];
    NSString *mailSubject = strOfferName;
    
    Rep *_rep = [CoreDataHandler currentRep];
   ASIHTTPRequest *request =  [ConnectionManager sendMailWithRep:_rep wdSiteID:_rep.parentWdSite.wdSiteID deviceInfo:nil offer:self.selectedOffer subjet:mailSubject customerIDs:receipents];
    
    [request setCompletionBlock:^{
        
        app.window.userInteractionEnabled = YES;
        NSDictionary *responseDict = [request responseJSON];
        
        NSString *subject = nil;
        
        if(responseDict && [[responseDict valueForKey:@"status"] intValue]==1)
        {
            subject = @"Email Status :  Sent Successfully";
            
        }
        else 
        {
            subject = @"Email Status: Failed";
        }
        
        
        if(subject)
        {
            
//		    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
//                                                         message:subject
//                                                        delegate:nil
//                                               cancelButtonTitle:nil
//                                               otherButtonTitles:@"Ok",nil];
//		    [alert show];
//            [alert release],alert=nil;
             
            [self performSelector:@selector(showAlertForMessage:) withObject:subject afterDelay:5.0];
        }
        
         [ProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
    [request setFailedBlock:^{
        
        app.window.userInteractionEnabled = YES;
        
        NSString *subject  = @"Email Status: Failed";
        
        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
//                                                        message:subject
//                                                       delegate:nil
//                                              cancelButtonTitle:nil
//                                              otherButtonTitles:@"Ok",nil];
//        [alert show];
//        [alert release],alert=nil;
         
        [self performSelector:@selector(showAlertForMessage:) withObject:subject afterDelay:5.0];
        
         [ProgressHUD hideHUDForView:self.view animated:YES];
    }];
 
 ///================================== SEND MAIL BY WEBSERVICE ===========================================
  */

}
#pragma mark -

-(void)showAlertForMessage:(NSString *)text
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:text
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"Ok",nil];
    [alert show];
    [alert release],alert=nil;

}

- (void)mailComposeController:(MFMailComposeViewController*)controller
		  didFinishWithResult:(MFMailComposeResult)result
						error:(NSError*)error 
{
	if (result == MFMailComposeResultSent) 
    {
		BOOL _isNetworkAvailable = [[OfflineManager sharedManager] isNetworkAvailable];
        NSString *success = _isNetworkAvailable? @" Sent Successfully" : @"Email will be sent when you are online";		UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Email Status:"
                                                         message:success
                                                        delegate:self
                                               cancelButtonTitle:nil
                                               otherButtonTitles:@"Ok",nil];
		[alert1 show];
        [alert1 release],alert1=nil;
		
	}
    else if (result == MFMailComposeResultFailed) 
    {
		NSString *msg = @"Failed";
		UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Email Status:"
                                                         message:msg
                                                        delegate:self
                                               cancelButtonTitle:nil
                                               otherButtonTitles:@"Ok",nil];
		[alert1 show];
        [alert1 release],alert1=nil;
		
	}
    
    else if (result == MFMailComposeResultCancelled) 
    {
        /*
		NSString *msg = @"Cancelled";
		UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Email Status:"
                                                         message:msg
                                                        delegate:self
                                               cancelButtonTitle:nil
                                               otherButtonTitles:@"Ok",nil];
		[alert1 show];
        [alert1 release],alert1=nil;
		*/
	}
	
	
	[self dismissModalViewControllerAnimated:YES];
}

/*
-(void)backToImagePage
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    [UIView beginAnimations:nil context:context];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationDelegate:self];
    _offerDescController.view.alpha=1;
    _offerDescController.view.alpha=0.04;
    [UIView setAnimationDidStopSelector:@selector(flipped:finished:context:) ];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.view.superview cache:NO]; 
	[UIView commitAnimations];
    
    
    isOnDescriptionPage=NO;
    [self.navigationItem setLeftBarButtonItem:self.leftBarButton animated:YES];
}

- (void)flipped:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context 
{
    [_offerDescController.view removeFromSuperview];
    
    for(UISwipeGestureRecognizer *recognizer in self.view.gestureRecognizers)
    {
        recognizer.enabled=YES;
    }
}
 */

#pragma mark -
#pragma mark Open PDF action
-(void)openPDFAction:(UIButton *)sender
{
    NSString *pdfName= [self.selectedOffer valueForKey:@"offerPdf"];
    
    if(!pdfName) return;
    
    NSString *pdfUrlString = makePDFURL(pdfName, @"offer");
    
    NSLog(@"Offer PDF link=%@",pdfUrlString);
    
    // NSString *pdfCachePath = [DirectoryManager pdfDirectoryPath];
    // pdfCachePath = [pdfCachePath stringByAppendingPathComponent:[pdfUrlString lastPathComponent]];
    
    WebLinkViewController *linkVC=[[WebLinkViewController alloc]initWithUrlString:pdfUrlString];
    linkVC.showEmailButton=YES;
   // linkVC.shouldCache = YES;
   // linkVC.cachePath = pdfCachePath;
    
    __block OfferDetailViewController *weakSelf = self;
    
    [linkVC addEmailDidSelectCallback:^(id sender) 
    {
       [weakSelf emailPDFAction:sender];
        
    }];
    
    [self.navigationController pushViewController:linkVC animated:YES];
    [linkVC release];
}
#pragma mark -


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
     self.navigationController.navigationBarHidden=YES;
 
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=NO;
  
   
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    self.contentSizeForViewInPopover=CGSizeMake(900.0, 625.0);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
