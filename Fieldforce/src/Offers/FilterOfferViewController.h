//
//  FilterOfferViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 28/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^FilterOfferCallback)(NSDictionary *dict);
typedef void(^CancelFilterOfferCallback)();

@protocol OfferFilterDelegate;

@interface FilterOfferViewController : UITableViewController
{
    NSMutableArray *arrOemName;
}

@property(nonatomic,assign) id <OfferFilterDelegate> delegate;

- (id)initWithCustomer:(Customer *)_customer;

-(void)addFilterCallback:(FilterOfferCallback)callBack;
-(void)addCancelCallback:(CancelFilterOfferCallback )callback;

@end


@protocol OfferFilterDelegate <NSObject>

@optional

-(void)filterViewController:(FilterOfferViewController *)filterVC willFilterWithOptioins:(NSDictionary *)filterOptions;
-(void)filterViewControllerDidCancel:(FilterOfferViewController *)filterVC;

@end