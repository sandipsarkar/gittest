//
//  OfferListTableViewCell.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 27/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOLabel.h"

@interface OfferListTableViewCell : UITableViewCell

@property(nonatomic,readonly) UIImageView *offerThumbImageView;
@property(nonatomic,readonly) SOLabel *titleLabel;
@property(nonatomic,readonly) UILabel *descriptionLabel;
@property(nonatomic,readonly) UILabel *oemLabel;
@property(nonatomic,readonly) UILabel *categoryLabel;
@property(nonatomic,readonly) UILabel *validityLabel;
@property(nonatomic,readonly) UIImageView *markedCheck;
@property(nonatomic,readonly) UILabel *byLabel;
@property(nonatomic,readonly) UILabel *startDateLabel;
@property(nonatomic,readonly) UILabel *endDateLabel;
@property(nonatomic,readonly) UILabel *endDateValueLabel;
@property(nonatomic,readonly) UILabel *startDateValueLabel;
@property(nonatomic,readonly) UIActivityIndicatorView *loaderView;

-(void)setOfferInfo:(Offer *)offer;

@end
