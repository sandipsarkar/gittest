//
//  OfferDetailViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 18/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SSCheckBox,Offer,Customer;
@class OfferDescriptionViewController;


typedef void(^DidMarkOffer) (Offer *offer, NSUInteger index);
typedef void(^DidDismissOfferDetail)(void) ;
typedef UIImage * (^WillLoadImageForOffer) (Offer *offer);

@interface OfferDetailViewController : UIViewController
{
    OfferDescriptionViewController *_offerDescController;
    
    UIView *offerContainerView;
    UIImageView *offerImageView;
    NSInteger currentIndex;
    UILabel *dateLabel;
    SSCheckBox  *_completedCheckBox;
    
    UILabel *titleHeader;
    
    BOOL isOnDescriptionPage;
    UIButton *nextPageButton;
    
    UIButton *pdfButton;
   
    
}
@property (nonatomic,assign) NSUInteger  offerIndex;
@property (nonatomic,retain) NSArray *offers;
@property (nonatomic,retain) Offer *selectedOffer;
@property(nonatomic,assign) BOOL isPopover;
@property(nonatomic,assign) BOOL enableMarking;
@property(nonatomic,retain) NSMutableArray *visitedOfferIDs;
@property(nonatomic,retain) Customer *customer;

- (id)initWithOfferIndex:(NSUInteger)_offerIndex offers:(NSArray *)_offers;

-(void)addDidMarkOffer:(DidMarkOffer)callback;
-(void)addDidDismissOfferDetail:(DidDismissOfferDetail)callback;
-(void)addWillLoadImageForOffer:(WillLoadImageForOffer )callback;

-(void)doubleTapGestureAction:(UITapGestureRecognizer *)recognizer;

@end
