//
//  OfferListViewControler.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 27/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//http://192.168.1.143/OEMIPAD/resource/offer/thumb/2ugodz.jpg

#import "OfferListViewControler.h"
#import "OfferListTableViewCell.h"
#import "UITableView+Reload.h"
#import "Visit.h"
#import "Offer.h"
#import "LazyImageLoader.h"
#import "Customer.h"
#import "OEM.h"
#import "SSMemoryCache.h"

@interface OfferListViewControler ()<UITableViewDelegate,UITableViewDataSource,ImageLoaderDelegate>
{
    UILabel *noCurrentOfferLabel;
    BOOL _isSearching;
    
   // NSCache *_imageCache;
    
    SSMemoryCache *_imageCache;

}

@property(nonatomic,readwrite,retain) UITableView *tableView;
@property(nonatomic,copy) DidSelectOfferCallback didSelectOfferCallback;
@property(nonatomic,retain) NSMutableArray *searchedArray;
@property (nonatomic,retain) NSArray *filterDescriptors;
@end

@implementation OfferListViewControler
@synthesize     dataSourceArray;
@synthesize     tableView = _tableView;
@synthesize     offerType;
@synthesize     didSelectOfferCallback;
@synthesize searchedArray;
@synthesize showCategory;

@synthesize visit;
@synthesize visitedOfferIDs;

@synthesize filterPredicte;
@synthesize searchPredicte;
@synthesize parentController;
@synthesize isLoadingFromServer;
@synthesize imageCache=_imageCache;


- (id)initWithOffers:(NSMutableArray *)arrayList type:(CategoryType)_offerType
{
    self = [super init];
    
    if (self) 
    {
        _isSearching = NO;
        dataSourceArray = arrayList? [arrayList retain] : [[NSMutableArray alloc] init];
        offerType       = _offerType;
        
        //if(!_imageCache) _imageCache=[[NSCache alloc]init];
    }
    return self;
}

-(void)dealloc
{
    visit = nil;
    parentController = nil;
    
    if(_imageCache) [_imageCache release];
    _imageCache = nil;
    
    [visitedOfferIDs release];

    [dataSourceArray release];
    [_tableView release];
    [searchedArray release];
    
    [filterPredicte release];
    [searchPredicte release];

    if(didSelectOfferCallback)
    [didSelectOfferCallback release];
    
    [noCurrentOfferLabel release];
    [_filterDescriptors release];
    
    [super dealloc];
}

-(SSMemoryCache *)imageCache
{
     if(!_imageCache)
     {
         _imageCache = [[SSMemoryCache alloc]init];
         _imageCache.cacheCount = 15;
       
     }
    
    return _imageCache;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
    
    if(_imageCache)
    {
        [_imageCache clear];
    }
    
#if LOCAL
    
        [UIAlertView showWarningAlertWithTitle:@"memory warning" message:@"at Offer list"];
#endif
    
}

- (void)handleMemoryWarning
{
	// Unload non-visible pages in case the memory is scarse
	
}

-(void)setDataSourceArray:(NSMutableArray *)_dataSourceArray
{
    if(dataSourceArray!=_dataSourceArray)
    {
        [dataSourceArray release];
        dataSourceArray = [_dataSourceArray retain];
        
        
        [self.tableView reloadData];
        
        if(![_dataSourceArray count] && !isLoadingFromServer) noCurrentOfferLabel.text = @"NO CURRENT OFFERS";
    }
}

-(void)didUpdateFromServer
{
    
    /*
    if(self.visit.parentCustomer) // FROM CUSTOMER OFFER
    {
        NSArray *dealingOems = [self.visit.parentCustomer.dealingOEMs componentsSeparatedByString:@","];
        
        NSMutableArray *oemDealings = [NSMutableArray array];
        for(NSString *oemID in dealingOems)
        {
            if([oemID intValue])
            {
                [oemDealings addObject:oemID];
            }
        }
        
       
        NSArray *oemsToCheck = [oemDealings count]? [OEM fetchWithPredicate:[NSPredicate predicateWithFormat:@"self.oemID IN %@",oemDealings] error:nil] : nil;
        
        
        if([oemsToCheck count])
        {
        NSArray *oemNamesToCheck = oemsToCheck?[oemsToCheck valueForKey:@"name"]:nil;
        
        NSLog(@"OEMS to check:%@",oemNamesToCheck);
        
        if([oemNamesToCheck count])
        {
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"self.userType = %@ AND NOT (self.createdByUserName IN %@)",[NSNumber numberWithInt:2], oemNamesToCheck];
          NSArray  *_allOffers = [dataSourceArray filteredArrayUsingPredicate:filterPredicate];
            
            NSLog(@"offers to delete: %@",[_allOffers valueForKey:@"createdByUserName"]);
            
            NSMutableArray *arr = [NSMutableArray arrayWithArray:dataSourceArray];
            [arr removeObjectsInArray:_allOffers];
            
            [dataSourceArray setArray:arr];
        }
        }
        else
        {
            //========== DO NOT SHOW DEALERGROUP OFFERS==========
            //[dataSourceArray removeAllObjects];
            
             //========  SHOW DEALERGROUP OFFER ===============
            
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"self.userType = %@",[NSNumber numberWithInt:2]];
            NSArray  *_allOffers = [dataSourceArray filteredArrayUsingPredicate:filterPredicate];
            
            NSLog(@"offers to delete: %@",[_allOffers valueForKey:@"createdByUserName"]);
            
            NSMutableArray *arr = [NSMutableArray arrayWithArray:dataSourceArray];
            [arr removeObjectsInArray:_allOffers];
            
            [dataSourceArray setArray:arr];
            
        }
    }
//#if SHOW_ACTIVE_OFFERS_ONLY
       
    
    //=========== ONLY FILTER CURRENT AND FUTURE OFFERS =========
    if([dataSourceArray count])
    {
        //============== FILTERED ONLY BY DATE NOT TIME
        NSDate *_today = [NSDate today];
         //NSDate *_today = [NSDate todayOnTimeZone:[NSTimeZone timeZoneWithName:@""Australia/Sydney""]];
        double _currentWCFDate=[_today GMTTimeIntervalSince1970] *1000.0 ;
        
        //NSPredicate *fetchPredicte = [NSPredicate predicateWithFormat:@"self.activeFrom.doubleValue<=%lf AND self.activeTo.doubleValue>=%lf",_currentWCFDate,_currentWCFDate];
        NSPredicate *fetchPredicte = [NSPredicate predicateWithFormat:@"self.activeTo.doubleValue>=%lf",_currentWCFDate];
        
       NSArray *_offfers=[dataSourceArray filteredArrayUsingPredicate:fetchPredicte];
        self.dataSourceArray= [NSMutableArray arrayWithArray:_offfers];
    }
    
//#else
    */

     if(![dataSourceArray count] && !_isSearching) noCurrentOfferLabel.text = @"NO CURRENT OFFERS";
    
   // if(![dataSourceArray count] && !_isSearching) noCurrentOfferLabel.text = @"NO CURRENT OFFERS";
    
     //  [_imageCache removeAllObjects];
}

-(void)sortOffersWithDescriptors:(NSArray *)descriptors
{
    if([dataSourceArray count])
    [dataSourceArray sortUsingDescriptors:descriptors];
}

-(void)setupView
{
    CGRect _bounds = self.view.bounds;
    CGFloat _locY = showCategory ?  55.0 : 10.0;
    
    //============= setup HeaderView ==============//
    
    if(showCategory)
    {
    UIImage *catImage = [UIImage imageNamed:(offerType==CategoryTypeMechanical) ? @"mechanical_logo.png" : @"collision_logo.png"];
    
    UIImageView *categoryIconView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, (_locY-catImage.size.height)/2.0, catImage.size.width, catImage.size.height)];
    categoryIconView.image = catImage;
    [self.view addSubview:categoryIconView];
    [categoryIconView release];
    
    CGFloat locX = CGRectGetMaxX(categoryIconView.frame)+10.0;
    UILabel *catLabel = [[UILabel alloc] initWithFrame:CGRectMake(locX, (_locY-20.0)/2.0+1.0, CGRectGetWidth(_bounds)-locX-15.0, 20.0)];
    catLabel.textColor = [UIColor blackColor];
    catLabel.backgroundColor = [UIColor clearColor];
    catLabel.font = [UIFont subaruBoldFontOfSize:22.0];
    
    catLabel.text = (offerType==CategoryTypeMechanical) ? @"MECHANICAL REPAIRERS" : @"COLLISION REPAIRERS";
    [self.view addSubview:catLabel];
    [catLabel release];
    }
    
    //==============================================//
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _locY, CGRectGetWidth(_bounds), CGRectGetHeight(_bounds)-_locY) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:_tableView];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.bounces = NO;
    
    _tableView.layer.cornerRadius = 8.0;
    _tableView.layer.masksToBounds = YES;
    
}

-(void)setupBackgroundView
{
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:self.tableView.bounds];
    bgImageView.image = [UIImage imageNamed:@"no_current_offers_container.png"];
    self.tableView.backgroundView = bgImageView;
    [bgImageView release];
    
    noCurrentOfferLabel=[[UILabel alloc] initWithFrame:CGRectMake(10.0, (CGRectGetHeight(bgImageView.bounds)-30.0)/2.0, CGRectGetWidth(bgImageView.bounds)-20.0, 30.0)];
    noCurrentOfferLabel.text =@"Loading..."; //@"NO CURRENT OFFERS";
    noCurrentOfferLabel.textAlignment = NSTextAlignmentCenter;
    noCurrentOfferLabel.textColor= [UIColor whiteColor];
    noCurrentOfferLabel.font = [UIFont subaruBoldFontOfSize:23.0];
    noCurrentOfferLabel.shadowColor = [UIColor colorWithWhite:0.1 alpha:0.4];
    noCurrentOfferLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    noCurrentOfferLabel.backgroundColor = [UIColor clearColor];
    [bgImageView addSubview:noCurrentOfferLabel];
    noCurrentOfferLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin| UIViewAutoresizingFlexibleRightMargin  | UIViewAutoresizingFlexibleLeftMargin;
}


-(void)filterDatasource
{
    if(!searchedArray) searchedArray = [[NSMutableArray alloc] init];
    
    NSArray *result = nil;
    
    if(self.filterPredicte && self.searchPredicte)
    {
        _isSearching = YES;
        
        NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:self.filterPredicte,self.searchPredicte, nil]];
        
        result =  [self.dataSourceArray filteredArrayUsingPredicate:predicate];
        
    }
    else if(self.filterPredicte)
    {
        _isSearching = YES;
        result =  [self.dataSourceArray filteredArrayUsingPredicate:self.filterPredicte];
       
    }
    else if(self.searchPredicte)
    {
        _isSearching = YES;
        result =  [self.dataSourceArray filteredArrayUsingPredicate:self.searchPredicte];
    }
    else
    {
        _isSearching = NO;
        result = self.dataSourceArray;
    }
    
    if(result)
    {
      [self.searchedArray setArray:result];
    }
    
    if([self.filterDescriptors count] && [self.searchedArray count])
    {
        _isSearching = YES;
        [self.searchedArray sortUsingDescriptors:self.filterDescriptors];
    }
}

-(void)filterWithPredicate:(NSPredicate *)predicte sortDescriptos:(NSArray *)sortDescriptors
{
    //_isSearching=YES;
    
    if(!self.searchedArray) self.searchedArray = [NSMutableArray array];
    
    self.filterPredicte = predicte;
    
    self.filterDescriptors = sortDescriptors;
    
    /*
    if(predicte)
    {
      NSArray *result =  [self.dataSourceArray filteredArrayUsingPredicate:predicte];
     [self.searchedArray setArray:result];
    }
    else
    {
        [self.searchedArray setArray:self.dataSourceArray];
    }
    */
    
    [self filterDatasource];
    
    
     if(![self.searchedArray count]) noCurrentOfferLabel.text = @"NO CURRENT OFFERS";
    
    [self.tableView reloadData];
}

-(void)resetFilter
{
    //_isSearching=NO;
    
    //[self.tableView reloadData];
    //[self reloadTableView];
    
    self.filterPredicte = nil;
    self.filterDescriptors = nil;
    
    [self filterDatasource];
    
    [self.tableView reloadDataWithRowAnimation:UITableViewRowAnimationFade];
}

-(BOOL)isSearchModeOn
{
    return ((self.filterPredicte || self.filterDescriptors) &&  self.searchPredicte);
}

-(void)resetSearchMode
{
    self.filterPredicte = nil;
    self.filterDescriptors = nil;
    self.searchPredicte = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif

    //self.clearsSelectionOnViewWillAppear = YES;
    
    NSLog(@"visitedOfferIDs=%@",visitedOfferIDs);

    self.view.backgroundColor = [UIColor clearColor];
    [self setupView];
    
    self.tableView.rowHeight = 78.0; //116.0;
    self.tableView.sectionFooterHeight = 1.0;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //self.view.layer.borderColor= [UIColor colorWithWhite:0.2 alpha:0.1].CGColor;
    //self.view.layer.borderWidth = 1.0;
   // self.view.layer.masksToBounds = 1;
    
    [self setupBackgroundView];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
   [LazyImageLoader shouldSaveToDisk:YES];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //self.visit.visitedOfferIDs = [visitedOfferIDs componentsJoinedByString:@","];
}




-(void)addDidSelectOfferCallback:(DidSelectOfferCallback)callback
{
     self.didSelectOfferCallback = callback;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

-(void)updateOffers:(NSArray *)updatedOffers
{
    NSArray *offerIds = [updatedOffers valueForKey:@"offerID"];
    
    //NSLog(@" updatedOffers %@",updatedOffers);
    NSMutableIndexSet *deleteIndexes = [NSMutableIndexSet indexSet];
    
    for(Offer *offer in updatedOffers)
    {
        if(![self.dataSourceArray containsObject:offer])
        {
            [self.dataSourceArray addObject:offer];
        }
         //else{
            
        //}
    }
    
        NSMutableDictionary *updateDict = [NSMutableDictionary dictionary];

       // NSLog(@" ff dataSourceArray %@",self.dataSourceArray);
        
        [self.dataSourceArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
         {
             Offer *offer = (Offer *)obj;
             
             if([offer.categoryID intValue]!=0 && [offer.categoryID intValue]!=offerType)
             {
                 [deleteIndexes addIndex:idx];
                // NSLog(@" deleteIndexes %@",deleteIndexes);
             }
             /* else if([offer.categoryID intValue]==0)
              {
                [arrToInsert addObject:offer];
              }*/
             
             if([offerIds containsObject:offer.offerID])
             {
                 [updateDict setObject:offer forKey:[NSNumber numberWithInt:idx]];
             }
         }];
        
        @try
        {
            for(NSNumber *indexNum in updateDict)
            {
                Offer *offerToReplace = [updateDict objectForKey:indexNum];
                [self.dataSourceArray replaceObjectAtIndex:[indexNum intValue] withObject:offerToReplace];
            }
            
            [self.dataSourceArray removeObjectsAtIndexes:deleteIndexes];
                       
        }
        @catch (NSException *exception)
        {
            
        }
        @finally
        {
            
        }
        
        
//    }

    
  [_tableView reloadData];
    
    /*
    NSIndexSet *indexes =  [self.dataSourceArray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop)
        {
            Offer *offer=(Offer *)obj;
     
            if([offer.categoryID intValue]!=0 && [offer.categoryID intValue]!=offerType)
            {
                [deleteIndexes addIndex:idx];
            }
            return [offerIds containsObject:offer.offerID];
        }];
    
        
    @try 
    {
        [self.dataSourceArray replaceObjectsAtIndexes:indexes withObjects:updatedOffers];
        [self.dataSourceArray removeObjectsAtIndexes:deleteIndexes];
    }
    @catch (NSException *exception) 
    {
        NSLog(@"exception:Name=%@ Reason=%@",exception.name,exception.reason);
    }
    @finally 
    {
        
    }
    */
    
    //===========================================================================================//
    
      
   // [_tableView reloadData];*/
    /* for (id obj in updatedOffers) {
        if (![self.dataSourceArray  containsObject:obj]) {
            [self.dataSourceArray addObject:obj];
        }
    }*/
    
    //===============================================================================================//

}

-(void)deleteOffers:(NSArray *)offers
{
     [self.dataSourceArray removeObjectsInArray:offers];
}

-(void)insertOffers:(NSArray *)offers
{
    [self.dataSourceArray addObjectsFromArray:offers];
}

-(NSMutableArray *)currentArray
{
    return _isSearching ? self.searchedArray : self.dataSourceArray;
}

-(void)didMarkOffer:(Offer *)offer atIndex:(NSUInteger)index
{
    if(index<[[self currentArray] count])
    {
                  
      NSIndexPath *indexPathToReload = [NSIndexPath indexPathForRow:0 inSection:index];
      
     if(indexPathToReload)
      [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPathToReload] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    NSUInteger count =[[self currentArray] count];
      //NSLog(@" dataSourceArray %@",self.dataSourceArray);
    noCurrentOfferLabel.hidden = count>0;
    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    OfferListTableViewCell *cell = (OfferListTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(!cell)
    {
        cell = [[[OfferListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.opaque= YES;
        
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
        UIView * selectedBackgroundView = [[UIView alloc] initWithFrame:cell.bounds];
        [selectedBackgroundView setBackgroundColor: [UIColor cellSelectedColorBlue]];
        [cell setSelectedBackgroundView:selectedBackgroundView];
        [selectedBackgroundView release];
}

    }
    
    NSMutableArray *_currentArr = [self currentArray];
    //NSLog(@" _currentArr %@",_currentArr);
    
    Offer *_offer = (indexPath.section < [_currentArr count]) ? [_currentArr objectAtIndex:indexPath.section] : nil;
    
     [cell setOfferInfo:_offer];
    
    NSNumber *_offerID = _offer.offerID;
    NSString *offerID = [NSString stringWithFormat:@"%d",[_offerID integerValue]];
     BOOL isMarked = [visitedOfferIDs containsObject:offerID];
     cell.markedCheck.hidden = !isMarked;
    
    
    NSString *imageURL = (_offer.offerImage) ? makeImageURL(_offer.offerImage, @"offer", YES) : nil;
    
    NSLog(@"ImageURL:%@",imageURL);
    
    NSLog(@"indexPath row:%d",indexPath.section);
    
    NSString *key       = [NSString stringWithFormat:@"%u",[imageURL hash]];
    UIImage *offerImage = [[self imageCache] objectForKey:key];
    if(offerImage)
    {
         cell.offerThumbImageView.image = offerImage;
        [cell.loaderView stopAnimating];
    }
    else if(imageURL && (!self.tableView.decelerating && !self.tableView.isDragging))
    {
        NSString *identifier = [NSString stringWithFormat:@"offerlist_%d",self.offerType];
       
       UIImage *image = [LazyImageLoader loadImageForUrlString:imageURL indexPath:indexPath delegate:self.parentController identifier:identifier];
        
       if(image) 
       {
           cell.offerThumbImageView.image = image;
           [[self imageCache] setObject:image forKey:key];
           //[cell.offerThumbImageView setNeedsDisplay];
           
       }
       else
       {
           cell.offerThumbImageView.image=nil;
           [cell.loaderView startAnimating];
       }
    }
    else 
    {
         cell.offerThumbImageView.image=nil;
    }
    

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    OfferListTableViewCell *_cell = (OfferListTableViewCell *)cell;
    _cell.offerThumbImageView.image = nil;
    
    //***************************************** CANCEL OFFSCREEN CELL IMAGE REQUEST ********************************************
    /*
    NSMutableArray *_currentArr = [self currentArray];
    Offer *_offer = (indexPath.section < [_currentArr count]) ? [_currentArr objectAtIndex:indexPath.section] : nil;
    NSString *imageURL = (_offer.offerImage) ? makeImageURL(_offer.offerImage, @"offer", YES) : nil;
    [LazyImageLoader cancelRequestForURLString:imageURL];
     */
}

/*
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1.0f;
}
*/

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 1.0f)];
    view.backgroundColor = [UIColor lightGrayColor];
    view.layer.shadowColor = [UIColor grayColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(1.0, 1.0);
    view.layer.shadowOpacity = 0.5;
    return [view autorelease];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
     NSMutableArray *_currentArr = [self currentArray];
    Offer *_offer = (indexPath.section < [_currentArr count]) ? [_currentArr objectAtIndex:indexPath.section] : nil;
    
    if(self.didSelectOfferCallback)
    self.didSelectOfferCallback(_offer,indexPath);
}

/*
#if LOCAL
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    float offset = (scrollView.contentOffset.y - (scrollView.contentSize.height - scrollView.frame.size.height));
    if (offset >= 0 && offset <= 5)
    {
      
        NSLog(@"Load More ..");
    }
}
#endif
*/

-(void)searchWithText:(NSString *)searchKey
{
    //_isSearching = searchKey.length>0;
    
    //===========================//
    if(!searchKey.length) 
    {
        self.searchPredicte = nil;
        
        [self filterDatasource];
        [self.tableView reloadData];
        return;
    }
    //===========================//
    
    if(!searchedArray) searchedArray = [[NSMutableArray alloc] init];
    
    
     NSPredicate *_searchPredicate = [NSPredicate predicateWithFormat:@"self.offerName beginswith[c] %@ OR self.createdByUserName beginswith[cd] %@",searchKey,searchKey];
    
    //================================   SEARCH BY OFFER NAME AND CREATED BY USERNAME ====================================
    
   // NSPredicate *_searchPredicate = [NSPredicate predicateWithFormat:@"self.offerName contains[c] %@ OR self.createdByUserName contains[cd] %@",searchKey,searchKey];
    
    self.searchPredicte = _searchPredicate;
    
    /*
    NSArray *arr = [self.dataSourceArray filteredArrayUsingPredicate:_searchPredicate];
    [self.searchedArray setArray:arr];
    [self.tableView reloadData];
    */
    
      [self filterDatasource];
    
     if(![self.searchedArray count]) noCurrentOfferLabel.text = @"NO CURRENT OFFERS";
      [self.tableView reloadData];
}


/*
-(BOOL)imageLoaderShouldAvoidLoading
{
    return (self.tableView.decelerating || self.tableView.isDragging);
}
*/

- (void)loadImagesForOnscreenRows
{
    NSMutableArray *_currentArr = [self currentArray];
    NSArray *rows = [self.tableView indexPathsForVisibleRows];
    
    for(NSIndexPath *indexPath in rows)
    {
        OfferListTableViewCell *cell = (OfferListTableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
        
        Offer *_offer = (indexPath.section < [_currentArr count]) ? [_currentArr objectAtIndex:indexPath.section] : nil;
        NSString *imageURL = (_offer.offerImage) ? makeImageURL(_offer.offerImage, @"offer", YES) : nil;
        
        NSString *key = [NSString stringWithFormat:@"%u",[imageURL hash]];
        UIImage *offerImage = [[self imageCache] objectForKey:key];
        if(offerImage)
        {
            cell.offerThumbImageView.image = offerImage;
            [cell.loaderView stopAnimating];
        }
        else if(imageURL)
        {
            NSString *identifier = [NSString stringWithFormat:@"offerlist_%d",self.offerType];
            UIImage *image = [LazyImageLoader loadImageForUrlString:imageURL indexPath:indexPath delegate:self.parentController identifier:identifier];
            
            if(image) 
            {
                cell.offerThumbImageView.image = image;
                [cell.loaderView stopAnimating];
                // [cell.offerThumbImageView setNeedsDisplay];
                [[self imageCache] setObject:image forKey:key];
            }
            else 
            {
                [cell.loaderView startAnimating];
            }
        }
    }

}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
    {
        
        [self loadImagesForOnscreenRows];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
    
}


-(void)imageLoaderDidLoad:(LazyImageLoader *)loader
{
    if(!loader.image) return;
    
    UIImage *_image = [loader.image retain];
    
    NSString *key = [NSString stringWithFormat:@"%u",[loader.urlString hash]];
    
    [[self imageCache] setObject:_image forKey:key];
    
    NSIndexPath *indexPath = nil;
    
     NSString *identifier = [NSString stringWithFormat:@"offerlist_%d",self.offerType];
    
    if([loader.identifier isEqualToString:identifier])
    {
          indexPath = loader.indexPath;
    }
    else
    {
      //  NSArray *rows = [self.tableView indexPathsForVisibleRows];
      //  [self.tableView reloadRowsAtIndexPaths:rows withRowAnimation:UITableViewRowAnimationNone];

        
      NSString *imageName = [loader.urlString lastPathComponent];
      NSUInteger index = [self.dataSourceArray indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop)
        {
           
           Offer *_offer = (Offer *)obj;
           
          BOOL b= [_offer.offerImage isEqualToString:imageName];
          if(b) *stop = YES;
          
          return b;
           
       }];
        
        if(index!=NSNotFound)
        indexPath = [NSIndexPath indexPathForRow:0 inSection:index];
    }
    
       
    
        if(!indexPath)
        {
            [_image release];
            return;
        }
    
        OfferListTableViewCell *cell = (OfferListTableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
        
        if(!cell)
        {
          //  NSArray *rows = [self.tableView indexPathsForVisibleRows];
          //  [self.tableView reloadRowsAtIndexPaths:rows withRowAnimation:UITableViewRowAnimationNone];

            [_image release];
            
            return;
        }
        
        cell.offerThumbImageView.image = _image;
        [_image release];
    
        [cell.loaderView stopAnimating];
        //[cell.offerThumbImageView setNeedsDisplay];
        
        CATransition *anim = [CATransition animation];
        [anim setDuration:0.3];
        [anim setType:kCATransitionPush];
        [anim setSubtype:kCATransitionFade];
        [anim setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [[cell.offerThumbImageView layer] addAnimation:anim forKey:@"fade in"];
        
        //========TEMP SOLUTION===========
        //NSArray *rows = [self.tableView indexPathsForVisibleRows];
        //[self.tableView reloadRowsAtIndexPaths:rows withRowAnimation:UITableViewRowAnimationNone];
}

-(void)imageLoader:(LazyImageLoader *)loader didFail:(NSError *)error
{
    NSIndexPath *indexPath = nil;
    NSString *identifier = [NSString stringWithFormat:@"offerlist_%d",self.offerType];
    
    if([loader.identifier isEqualToString:identifier])
    {
        indexPath = loader.indexPath;
    }
    else
    {
        //  NSArray *rows = [self.tableView indexPathsForVisibleRows];
        //  [self.tableView reloadRowsAtIndexPaths:rows withRowAnimation:UITableViewRowAnimationNone];
        
        
        NSString *imageName = [loader.urlString lastPathComponent];
        NSUInteger index= [self.dataSourceArray indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop)
            {
                Offer *_offer = (Offer *)obj;
                BOOL b= [_offer.offerImage isEqualToString:imageName];
                if(b) *stop = YES;
                return b;
                               
            }];
        
        if(index!=NSNotFound)
            indexPath = [NSIndexPath indexPathForRow:0 inSection:index];
    }

    
    OfferListTableViewCell *cell = indexPath ?  (OfferListTableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath] : nil;
     [cell.loaderView stopAnimating];
}

@end
