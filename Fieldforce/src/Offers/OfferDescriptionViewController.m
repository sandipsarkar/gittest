//
//  OfferDescriptionViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 10/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "OfferDescriptionViewController.h"
#import "Offer.h"
#import "UnderlineButton.h"
#import "AttributedLabel.h"
#import "NSAttributedString+Attributes.h"
#import "OfferDetailViewController.h"

@interface OfferDescriptionViewController ()<AttributedLabelDelegate>


@end

@implementation OfferDescriptionViewController
@synthesize offer;
@synthesize navController;

- (id)initWithOffer:(Offer *)_offerDict
{
    self = [super init];
    if (self)
    {
        self.offer=_offerDict;
    }
    return self;
}

-(void)dealloc
{
    offer=nil;
    [offerTextView release];
    navController=nil;
    
    [super dealloc];
}

-(void)setupExternalLink
{
    NSString *externalLink =  self.offer.offerLinks ? self.offer.offerLinks : @"";
    
    
    NSString *_labelText = [NSString stringWithFormat:@"%@ %@",@"For further information please visit",externalLink];
    CGSize _linkButtonSize = [_labelText sizeWithFont:[UIFont subaruBookFontOfSize:20.0] constrainedToSize:CGSizeMake(CGRectGetWidth(offerTextView.frame), 20)];
    
    AttributedLabel* _furtherInfoLabel = [[AttributedLabel alloc] init];
    _furtherInfoLabel.frame = CGRectMake((CGRectGetWidth(self.view.bounds)-_linkButtonSize.width)/2.0, CGRectGetHeight(self.view.bounds)-38.0, _linkButtonSize.width, 20.0);
    _furtherInfoLabel.text = _labelText;
    _furtherInfoLabel.textColor = [UIColor blackColor];
    _furtherInfoLabel.font = [UIFont subaruMediumFontOfSize:20.0];
    _furtherInfoLabel.autoresizingMask=UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin;
    _furtherInfoLabel.delegate=self;
    [self.view addSubview:_furtherInfoLabel];
    
    if(externalLink.length)
    {
        NSRange r = [_furtherInfoLabel.text rangeOfString:externalLink];
        [_furtherInfoLabel addLinkToURL:[NSURL URLWithString:externalLink] withRange:r];
    }
    
    _furtherInfoLabel.hidden  = !externalLink.length;
     [_furtherInfoLabel release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.navController.navigationItem.leftBarButtonItem=nil;
    self.view.backgroundColor=[UIColor whiteColor];
	// Do any additional setup after loading the view.
    
     //[app.window animateWithTransition:UIViewAnimationTransitionFlipFromRight duration:0.9];
    
    offerTextView=[[UITextView alloc] initWithFrame:CGRectMake(20,20,CGRectGetWidth(self.view.bounds)-40.0,CGRectGetHeight(self.view.bounds)-80.0)];
    offerTextView.backgroundColor=[UIColor clearColor];
    offerTextView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    offerTextView.textColor=[UIColor blackColor];
    offerTextView.dataDetectorTypes = UIDataDetectorTypeLink;
    offerTextView.font=[UIFont boldSystemFontOfSize:20];
    offerTextView.textAlignment = NSTextAlignmentLeft;
    offerTextView.userInteractionEnabled=YES;
    offerTextView.editable=NO;
    [self.view addSubview:offerTextView];
    
    NSString *_descString = self.offer.offerDescription;
    
    NSLog(@"desc text:%@",_descString);
    
    _descString = [_descString stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    if(_descString.length)
    offerTextView.text=[NSString stringWithFormat:@"\n\n\n%@",_descString];

    
    //offerTextView.scrollsToTop = YES;
   
    //For further information
    
    [self setupExternalLink]; 
    
    // -----------------------------
	// Double tap to flip horizontally
	// -----------------------------
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapGestureAction:)];
    tapGesture.numberOfTapsRequired=2;
    [offerTextView addGestureRecognizer:tapGesture]; 
    [tapGesture release];
    

   // [self.view animateWithTransition:UIViewAnimationTransitionFlipFromLeft duration:0.5];
    
   // NSLog(@"nav controller:%@",self.navController);
    
    UIBarButtonItem *backButton=[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(viewOfferImage)];
    if(self.navController)
    {
      self.navController.navigationItem.leftBarButtonItem=backButton;
    }
    
    [backButton release];

}

#pragma mark AttributedLabelDelegate


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    [offerTextView setContentOffset:CGPointMake(0, 0) animated:NO];

    
}
- (void)attributedLabel:(AttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if(!url) return;
    
    if([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        //ERROR HANDLING
    }
}

#pragma mark -

/*
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    
    return YES;
}
*/

-(void)viewOfferImage:(id)sender
{
  // [app.window animateWithTransition:UIViewAnimationTransitionFlipFromRight duration:0.5];
    CGContextRef context = UIGraphicsGetCurrentContext();
    [UIView beginAnimations:nil context:context];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationDelegate:self];
    self.view.alpha=1;
    self.view.alpha=0.04;
    [UIView setAnimationDidStopSelector:@selector(flipped:finished:context:) ];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.view.superview cache:NO]; 
	[UIView commitAnimations];
    
    //[self dismissModalViewControllerAnimated:YES];
}

- (void) flipped:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context 
{
    [self.view removeFromSuperview];
}

-(void)doubleTapGestureAction:(UITapGestureRecognizer *)recognizer
{
    if(recognizer.state==UIGestureRecognizerStateEnded)
    {
       // [self viewOfferImage:nil];
        
          if(self.navController)
          {
              OfferDetailViewController *_offerDetailVC =(OfferDetailViewController *) [self.navController topViewController];
              
              if(_offerDetailVC)
              [_offerDetailVC doubleTapGestureAction:recognizer];
          }
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
