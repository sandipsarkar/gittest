//
//  CustomerOfferInVisitSessionController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 28/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomerOfferViewController.h"

@interface CustomerOfferInVisitSessionController : CustomerOfferViewController


-(id)initWithCategoryName:(NSString *)catName visit:(Visit *)aVisit;

-(void)markAsComplete;

@end
