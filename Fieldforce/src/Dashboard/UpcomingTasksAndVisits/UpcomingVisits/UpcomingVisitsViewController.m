//
//  UpcomingVisitsViewController.m
//  RepVisitationTool
//
//  Created by Somsankar Ray on 14/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "UpcomingVisitsViewController.h"
#import "Customer.h"
#import "CustomEventStore.h"
#import "CalendarViewController.h"
#import "Rep.h"
#import "WholesaleDealer.h"
#import "Events.h"
#import "NSDate+Formatter.h"
#import "ScheduledVisit.h"
#import "InsetTableViewCell.h"

#define UPCOMING_LAST_DATE 60*60*24*365 //1 year

#define UPCOMING_VISITS_HEADER_IMAGE @"upcoming_visits.png"
#define CURRENT_DATE_IMAGE           @"dashboard_date_bg.png"
#define VIEW_CALENDAR_FOOTER_IMAGE  @"view_calendar_button.png"
#define UPCOMING_VISITS_TABLEVIEW_CELL_SEPARATOR_IMAGE @"upcoming_visit_cell_separator.png"
#define VIEW_BACKGROUND_COLOR   [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0]
#define VISIT_CELL_BACKGROUND_COLOR   [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0]

#define UPCOMING_VISITS_CELL_HEIGHT 46
#define MAX_VISIT 5

@interface UpcomingVisitsViewController () <UITableViewDelegate, UITableViewDataSource>
{
    BOOL isFetchingFromServer;
    
}

@property(nonatomic,retain) ASIHTTPRequest *currentVisitsRequest;

-(void)showCalenderForVisit:(EKEvent *)aVisit;
@end


@implementation UpcomingVisitsViewController
@synthesize visitsDataSource = _visitsDataSource;
@synthesize labelOnNoVisits;
@synthesize currentVisitsRequest;

- (id)init
{
    self = [super init];
    if (self) 
    {
        _visitsDataSource   =   [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)dealloc
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    currentVisitsRequest.delegate = nil;
    [currentVisitsRequest clearDelegatesAndCancel];
    [currentVisitsRequest release];
    currentVisitsRequest = nil;
    
    [upcomingVisitsTableView release];
    [noUpcomingVisitImageView release] ;
    
    noUpcomingVisitImageView = nil;
    [_loaderView release] ;
    
    _loaderView = nil;
    [_visitsDataSource release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
-(void)loadUpcommingVisits
{
    NSDate *__startDate= [NSDate date];
    //[formatter dateFromString:self._startDate];
    NSDate *__endDate= [__startDate dateByAddingTimeInterval:60*60*24*30];
    //[formatter dateFromString:self._endDate];
    
    NSString *_calenderIdentifier = [[NSUserDefaults standardUserDefaults] objectForKey:CALENDER_IDENTIFIER_KEY];
    EKCalendar *calender = [[CustomEventStore sharedInstance] customCalendarWithIdentifier:_calenderIdentifier title:@"com.reptool.calender"];
    if(!_calenderIdentifier)
    {
        _calenderIdentifier = calender.calendarIdentifier;
        [[NSUserDefaults standardUserDefaults] setObject:_calenderIdentifier forKey:CALENDER_IDENTIFIER_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    
    //ADDED BY SANDIP TO SEARCH ANY EVENT WITH SAME START DATE AND END DATE
    NSPredicate *predicate = [[CustomEventStore sharedInstance].eventStore predicateForEventsWithStartDate:__startDate endDate:__endDate calendars:[NSArray arrayWithObject:calender]];
    
    NSArray *_events = [[CustomEventStore sharedInstance].eventStore eventsMatchingPredicate:predicate];
    
    if(!_events.count)
    {
      if([_visitsDataSource count]) [_visitsDataSource setArray:_events];
        
        return;
    }
    
    EKEvent *latestEvent=[_events objectAtIndex:0];
    
    NSLog(@"Events=%@",latestEvent);
    
    __startDate=latestEvent.startDate;
    __endDate=[__startDate dateByAddingTimeInterval:60*60*24*7];
    
    // NSLog(@"startDate=%@ & endDate=%@",__startDate,__endDate);
    
  
    NSString *locationStr==[AppDelegate eventLocationString];
    
    NSPredicate *searchPredicte=[NSPredicate predicateWithFormat:@"(self.startDate>=%@ AND self.startDate<=%@) AND self.notes beginswith[c] %@ AND self.location = %@",__startDate,__endDate,@"Visit",locationStr];
    NSArray *visits=  [_events filteredArrayUsingPredicate:searchPredicte];
    [_visitsDataSource setArray:visits];
    
}
*/

-(void)loadUpcommingVisits
{
    NSDate *__startDate = [NSDate date];
    NSDate *__endDate   = [__startDate dateByAddingTimeInterval:UPCOMING_LAST_DATE]; // 1 year
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.visitStartDate>=%@ AND self.visitEndDate<=%@",__startDate,__endDate];
    
    [ScheduledVisit fetchAsynWithPredicate:predicate onAttributes:nil sortDescriptors:nil limit:0 startIndex:0 completion:^(NSArray *result, NSError *error) 
    {
        //[_visitsDataSource setArray:result];
        
        //=========CHANGED: FILTER SCHEDULED VISIT FOR FIRST DAY ONLY ==================
        ScheduledVisit *firstVisit =[result count]? [result objectAtIndex:0]:nil;
        
        if(firstVisit)
        {
            NSDate *_visitEndDate= [firstVisit.visitStartDate dateByAddingTimeInterval:60*60*24*1];
            NSArray *scheduledVisitsOnSameDate = [result filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.visitStartDate>=%@ AND self.visitStartDate<=%@",firstVisit.visitStartDate,_visitEndDate]];
            [_visitsDataSource setArray:scheduledVisitsOnSameDate];
        
        }
       //========= FILTER SCHEDULED VISIT FOR FIRST DAY ONLY ==============================
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //_visitsDataSource
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.backgroundColor   =   VIEW_BACKGROUND_COLOR;
    [self addDefaultBorder];
    
    UIView* headerView              =   [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, 42)];
    headerView.backgroundColor      =   VIEW_BACKGROUND_COLOR;
    UIView* headerImageView         =   [self pageHeaderViewForImageName:UPCOMING_VISITS_HEADER_IMAGE];
    CGRect headerImageViewBounds    =   headerImageView.bounds;
    headerImageViewBounds.origin    =   CGPointMake(headerView.bounds.origin.x+19, headerView.bounds.origin.y+15);
    headerImageView.frame           =   headerImageViewBounds;
    [headerView addSubview: headerImageView];
    [self.view addSubview:headerView];
    
    
    noOfVisitsLabel=[UILabel labelWithText:@"noOfVisits" frame:CGRectMake(190.0,15.0, 150.0, 20.0) textColor:[UIColor blackColor] bgColor:[UIColor clearColor] fontsize:13.0];
    noOfVisitsLabel.font = [UIFont subaruBoldFontOfSize:16.0];
    noOfVisitsLabel.textAlignment  =   UITextAlignmentRight;
    noOfVisitsLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [headerView addSubview:noOfVisitsLabel];
    
     [headerView release];  
    
    currrentDateImageView  =   [UIView imageWithFrame:CGRectMake(CGRectGetMinX(headerView.bounds)+9.0, CGRectGetMaxY(headerView.bounds), 117.0, 141) fileName:CURRENT_DATE_IMAGE];
    currrentDateImageView.autoresizingMask  =   UIViewAutoresizingNone;
    
   dayLabel   =   [UIView labelWithText:@"" frame:CGRectMake((CGRectGetWidth(currrentDateImageView.bounds)-115.0)/2.0, 10.0, 115.0, 20.0) textColor:[UIColor whiteColor] bgColor:[UIColor clearColor] fontsize:13.0];
    dayLabel.font = [UIFont subaruBoldFontOfSize:15.0];
    dayLabel.textAlignment  =   UITextAlignmentCenter;
    [currrentDateImageView addSubview:dayLabel];
    
     dateLabel  =   [UIView labelWithText:@"" frame:CGRectMake((CGRectGetWidth(currrentDateImageView.bounds)-80.0)/2.0, (CGRectGetHeight(currrentDateImageView.bounds)-62.0)/2.0, 80.0, 62.0) textColor:[UIColor whiteColor] bgColor:[UIColor clearColor] fontsize:0.0];
    dateLabel.font =    [UIFont subaruBoldFontOfSize:75.0];
    dateLabel.textAlignment =   UITextAlignmentCenter;
    [currrentDateImageView addSubview:dateLabel];
    
    monthAndYearLabel  =   [UIView labelWithText:@"" frame:CGRectMake((CGRectGetWidth(currrentDateImageView.bounds)-115.0)/2.0, 110.0, 115.0, 20.0) textColor:[UIColor whiteColor] bgColor:[UIColor clearColor] fontsize:13.0];
    monthAndYearLabel.font = [UIFont subaruBoldFontOfSize:15.0];
    monthAndYearLabel.textAlignment =   UITextAlignmentCenter;
    [currrentDateImageView addSubview:monthAndYearLabel];
    
    [self.view addSubview:currrentDateImageView];
    
    upcomingVisitsTableView    =   [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(currrentDateImageView.bounds)-6.0, CGRectGetMaxY(headerView.bounds), 243.5, 154) style:UITableViewStyleGrouped];
    upcomingVisitsTableView.sectionHeaderHeight=0.2;
   
    upcomingVisitsTableView.delegate    =   self;
    upcomingVisitsTableView.dataSource  =   self;
    upcomingVisitsTableView.rowHeight   =   UPCOMING_VISITS_CELL_HEIGHT;
    upcomingVisitsTableView.bounces     =   NO;
    
    
//    if ([_visitsDataSource count] < 3) {
//        upcomingVisitsTableView.scrollEnabled = NO;
//    }
    
   if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
   {
       UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(upcomingVisitsTableView.bounds), 1.0)];
       view.backgroundColor = [UIColor clearColor];
       upcomingVisitsTableView.tableHeaderView = view;
       [view release];
       
       upcomingVisitsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
       
   }
    
    UIImageView* tableBGView =   [[UIImageView alloc] initWithFrame:upcomingVisitsTableView.bounds];
    tableBGView.backgroundColor = VIEW_BACKGROUND_COLOR ;//[UIColor clearColor];//   ;
    tableBGView.image = [UIImage imageNamedNoCache:@"no_upcoming_visits_field_blank.png"];
    upcomingVisitsTableView.backgroundView =   tableBGView;
    [tableBGView release]; 
    
    [self.view addSubview:upcomingVisitsTableView];
    [self.view bringSubviewToFront:headerView];
    [self.view bringSubviewToFront:currrentDateImageView];
    
    
    noUpcomingVisitImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(currrentDateImageView.frame), CGRectGetMinY(currrentDateImageView.frame), 337.0, 141.0)];
    noUpcomingVisitImageView.image = [UIImage imageNamedNoCache:@"no_upcoming_visits_field.png"];
    [self.view addSubview:noUpcomingVisitImageView];
    
    _loaderView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _loaderView.frame = CGRectMake((CGRectGetWidth(noUpcomingVisitImageView.frame)-CGRectGetWidth(_loaderView.frame))/2.0, (CGRectGetHeight(noUpcomingVisitImageView.frame)-CGRectGetHeight(_loaderView.frame)-50.0)/2.0, CGRectGetWidth(_loaderView.frame), CGRectGetHeight(_loaderView.frame));
    [noUpcomingVisitImageView addSubview:_loaderView];
    _loaderView.hidesWhenStopped = YES;
    [_loaderView startAnimating];

    [self performSelector:@selector(stopLoading) withObject:nil afterDelay:25.0];
    
    UIButton* viewCalendarFooterButton  =   [UIView buttonWithTitle:@"" type:UIButtonTypeCustom frame:CGRectMake(CGRectGetMinX(currrentDateImageView.frame), CGRectGetMaxY(currrentDateImageView.frame)-2.0, 337.0, 44.0) bgImage:VIEW_CALENDAR_FOOTER_IMAGE titleColor:nil target:self action:@selector(showCalender)];
    viewCalendarFooterButton.autoresizingMask   =   UIViewAutoresizingNone;
    [self.view addSubview:viewCalendarFooterButton];
    [self.view bringSubviewToFront:viewCalendarFooterButton];
    
//#if LOAD_TASK_SCHEDULED_VISIT
   // [self fetchFromServer];
//#endif
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willRefresh) name:DashBoardWillRefresh object:nil];
}

-(void)stopLoading
{
    if(_loaderView.isAnimating)
        [_loaderView stopAnimating];
}

-(void)_fetchFromServer
{
    if(!app.isCustomerLoaded) return;
    
    if(isFetchingFromServer) return;
    isFetchingFromServer = YES;
    
    if(!_loaderView.isAnimating) [_loaderView startAnimating];
    
   __block  UpcomingVisitsViewController *weakSelf = self;
   ASIHTTPRequest *request = [ConnectionManager fetchAndProcessScheduledVisitsOnCompletion:^(BOOL success, NSError *error) {
        
        weakSelf->isFetchingFromServer = NO;
        
        if(success)
        {
          [weakSelf reload];
        }
        
        [weakSelf->_loaderView stopAnimating];
    }];
    
    self.currentVisitsRequest = request;
}

-(void)fetchFromServer
{
    if(!app.isCustomerLoaded) return;
    
    if(isFetchingFromServer) return;
    isFetchingFromServer = YES;

    if(!_loaderView.isAnimating) [_loaderView startAnimating];
    
   // Rep *_rep = [CoreDataHandler currentRep];
   // ASIHTTPRequest *request =  [ConnectionManager createUpcommingVisitsConnectionWithRepId:_rep.repID wdSiteID:_rep.parentWdSite.wdSiteID];
    
     ASIHTTPRequest *request =  [ConnectionManager createUpcommingVisitsConnection];
    request.delegate = self;
    self.currentVisitsRequest = request;
    
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    isFetchingFromServer = NO;
    
    NSDictionary *jsonDict = [request responseJSON];
    NSLog(@"json=%@",[request responseString]);
    [_loaderView stopAnimating];
    
    if(jsonDict && [jsonDict isKindOfClass:[NSDictionary class]])
    {
        BOOL _isSiteIDChanged = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
        if(_isSiteIDChanged)
        {
            [app appDidChangeSiteID];
        }
        else
        {
            
            NSArray *_visits     = [jsonDict valueForKey:JSON_DETAILS_KEY];
            NSArray *_updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
            NSArray *_deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
            
            _visits = NULL_NIL(_visits);
            _updatedIDs = NULL_NIL(_updatedIDs);
            _deletedIDs = NULL_NIL(_deletedIDs);
            
            NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
            
            lastRequested = NULL_NIL(lastRequested);
            
            if(!([_visits count] ||[_deletedIDs count]))
            {
                [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_VISITS];
            }
            
            
            
            [ScheduledVisit populateAsyncPerformingDelete:_deletedIDs?_deletedIDs:[NSArray array] update:_updatedIDs?_updatedIDs:[NSArray array] insertOnArray:_visits?_visits:[NSArray array] onAttribute:@"scheduleVisitID" completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
             {
                 
                   [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_VISITS];
                 
                   dispatch_async([app eventQueue], ^{
                     
                     NSManagedObjectContext *context=[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType] ;
                       context.parentContext = DEFAULT_CONTEXT;
                     //[context setPersistentStoreCoordinator:DEFAULT_CONTEXT.persistentStoreCoordinator];
                     [context setUndoManager:nil];
                       
                     for(ScheduledVisit *visit in insertedResult)
                     {
                         ScheduledVisit *_visit =(ScheduledVisit *)[visit inContext:context];
                         
                         NSLog(@"Customer ID:%@",_visit.customerID);
                         
                         NSMutableArray *arr = [Customer fetchOnContext:context predicate:[NSPredicate predicateWithFormat:@"self.customerID = %@",_visit.customerID] onAttributes:nil error:nil];
                         
                         Customer *_customer = [arr count]? [arr objectAtIndex:0] : nil;
                         
                         if(_customer)
                         {
                             //_visit.customer= _customer;
                             [_customer addScheduledVisitsObject:_visit];
                         }
                         else
                         {
                             //  [context deleteObject:_visit];
                         }
                         
                     }
                       
                       for(ScheduledVisit *visit in updatedResult)
                       {
                           ScheduledVisit *_visit =(ScheduledVisit *)[visit inContext:context];
                           
                           if(_visit.customer)
                           {
                               if(![_visit.customer.customerID isEqualToNumber:_visit.customerID])
                               {
                                   [_visit.customer removeScheduledVisitsObject:_visit];
                               }
                           }
                           NSLog(@"Customer ID:%@",_visit.customerID);
                           
                           if(!_visit.customer || ![_visit.customer.customerID isEqualToNumber:_visit.customerID])
                           {
                             NSMutableArray *arr = [Customer fetchOnContext:context predicate:[NSPredicate predicateWithFormat:@"self.customerID = %@",_visit.customerID] onAttributes:nil error:nil];
                           
                             Customer *_customer = [arr count]? [arr objectAtIndex:0] : nil;
                           
                             if(_customer)
                              {
                               //_visit.customer= _customer;
                                 [_customer addScheduledVisitsObject:_visit];
                              }
                              else
                              {
                                 //  [context deleteObject:_visit];
                              }
                           }
                       }

                     
                     ///////////////////////////////////////////////////////////////////
                     [[SSCoreDataManager sharedManager] saveContext:context error:nil];
                     //////////////////////////////////////////////////////////////////
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                       
                         
                         [self reload];
                         
                     });
     
                     for(ScheduledVisit *visit in insertedResult)
                     {
                         if([visit respondsToSelector:NSSelectorFromString(@"eventIdentifier")])
                         {
                             ScheduledVisit *_visit =(ScheduledVisit *)[visit inContext:context];
                             
                             EKEvent * event =nil;
                             NSString *eventIdentifier = _visit.eventIdentifier;
                             
                             NSString *title =[NSString stringWithFormat:@"Visit: %@", _visit.customer?_visit.customer.name:_visit.customerName.length?_visit.customerName:@"Visit"];
                             
                             
                             if(eventIdentifier.length)
                                 event = [[CustomEventStore sharedInstance].eventStore eventWithIdentifier:eventIdentifier];
                             
                             if(!event)
                             {
                                 event = [CustomEventStore addEventWithTitle:title startDate:_visit.visitStartDate endDate:_visit.visitEndDate location:nil notes:nil];
                             }
                             else
                             {
                                 event =[CustomEventStore editEventForIdentifier:_visit.eventIdentifier withTitle:title startDate:_visit.visitStartDate endDate:_visit.visitEndDate location:nil notes:nil];
                             }
                             
                             if(event && event.eventIdentifier.length)
                                 _visit.eventIdentifier = event.eventIdentifier;
                         }
                     }
                     
                     if([_updatedIDs count])
                     {
                         for(ScheduledVisit *visit in updatedResult)
                         {
                             ScheduledVisit *_visit =(ScheduledVisit *)[visit inContext:context];
                             
                             if([_visit respondsToSelector:NSSelectorFromString(@"eventIdentifier")] && _visit.eventIdentifier.length)
                             {
                                 NSString *title =[NSString stringWithFormat:@"Visit: %@", _visit.customer?_visit.customer.name:_visit.customerName.length?_visit.customerName:@"Visit"];
                                 
                                 EKEvent *event= [CustomEventStore editEventForIdentifier:_visit.eventIdentifier withTitle:title startDate:_visit.visitStartDate endDate:_visit.visitEndDate location:nil notes:nil];
                                 
                                 if(event && event.eventIdentifier.length)
                                     _visit.eventIdentifier = event.eventIdentifier;
                             }
                         }
                     }
                     app.eventFetchCount++;
                     
                     [[SSCoreDataManager sharedManager] saveContext:context error:nil];
                     
                     [context reset];
                     [context release];
                 });
             }];
        }
    }
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    isFetchingFromServer = NO;
    
    [_loaderView stopAnimating];
}

-(void)willRefresh
{
    [self reload];

     [self fetchFromServer];

}

-(void)showCalender
{
    [self showCalenderForVisit:nil];
}

-(void)showCalenderForVisit:(EKEvent *)aVisit
{
   __block CalendarViewController *calendarViewController   = [[CalendarViewController alloc] init];
    //calendarViewController.selectedTabIndex=2;
    calendarViewController.title = @"Calendar";
    UINavigationController   *navController=[[UINavigationController alloc] initWithRootViewController:calendarViewController];
 
    [app.splitViewController presentViewController:navController animated:YES completion:^{
        
        if(aVisit)
        {
             [calendarViewController selectScheduledVisit:aVisit];
            
        }

    }];
    
    /*
    [calendarViewController addCalenderDidLoadCallbak:^{
        
     [calendarViewController selectScheduledVisit:aVisit];
       
    }];
    */
    
    [calendarViewController release];
    [navController release];
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

/*
-(void)reload
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        BOOL _wasVisitsAvailable = [_visitsDataSource count]>0;
        [self loadUpcommingVisits]; 
        
        EKEvent *event = _visitsDataSource.count ? [_visitsDataSource objectAtIndex:0] : nil;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(event)
            {
                NSDate *startDate=event.startDate;
                
                NSString *dateString=[startDate stringFromDateWithFormat:@"dd,EEEE,MMMM,yyyy"];
                NSArray *dateComponents=[dateString componentsSeparatedByString:@","];
        
                dayLabel.text = [[dateComponents objectAtIndex:1] uppercaseString];
                dateLabel.text = [dateComponents objectAtIndex:0];
                NSString *month=[dateComponents objectAtIndex:2];
                NSString *year=[dateComponents objectAtIndex:3];
                
                NSString* monthAndYear = [[month uppercaseString] stringByAppendingFormat:@" %@",[year uppercaseString]];
                monthAndYearLabel.text = monthAndYear;
                
                [upcomingVisitsTableView reloadData];
                [self manageVisibility];
            }
            else if(_wasVisitsAvailable)
            {
                [upcomingVisitsTableView reloadData]; 
                [self manageVisibility];
            }

        });
    });
        
}
*/

-(void)reload
{
    BOOL _wasVisitsAvailable = [_visitsDataSource count]>0;
    NSDate *__startDate  = [[NSDate date] currentDateHourMinute]; //[NSDate today];
    NSDate *__endDate    = [__startDate dateByAddingTimeInterval:UPCOMING_LAST_DATE]; // 1 year
    
   // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.visitStartDate>=%@ AND self.visitStartDate<=%@",__startDate,__endDate];
    
   // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.visitEndDate>=%@ AND self.visitEndDate<=%@",__startDate, __endDate];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.isVisitPublished=%@ AND (self.visitEndDate>=%@ AND self.visitEndDate<=%@)",[NSNumber numberWithBool:NO], __startDate, __endDate];
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"visitStartDate" ascending:YES];
    
    [ScheduledVisit fetchAsynWithPredicate:predicate onAttributes:nil sortDescriptors:[NSArray arrayWithObject:descriptor] limit:0 startIndex:0 completion:^(NSArray *result, NSError *error) 
     {
         
         [_visitsDataSource setArray:result];
         ScheduledVisit *event = result.count ? [result objectAtIndex:0] : nil;
         
         //=========CHANGED: FILTER SCHEDULED VISIT FOR FIRST DAY ONLY ==================
         if(event)
         {
             NSDate *_visitEndDate = [event.visitStartDate dateEnd];
             
             //NSLog(@"Visit end Date = %@",_visitEndDate);
             
             //===================== CALCULATE ACTUAL START DATE AND END DATE ====================================================
            // NSDate *_visitEndDate= [event.visitStartDate dateByAddingTimeInterval:60*60*24*1];
             NSLog(@"Visit StartDate=%@ AND Enddate= %@",event.visitStartDate, _visitEndDate);
             
             NSArray *scheduledVisitsOnSameDate = [result filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.visitStartDate>=%@ AND self.visitStartDate<=%@",event.visitStartDate,_visitEndDate]];
             [_visitsDataSource setArray:scheduledVisitsOnSameDate];
         }
         //======================== FILTER SCHEDULED VISIT FOR FIRST DAY ONLY =======================================================
      
         
         if(event)
         {
             NSDate *startDate=event.visitStartDate;
             NSString *dateString=[startDate stringFromDateWithFormat:@"dd,EEEE,MMMM,yyyy"];
             NSArray *dateComponents=[dateString componentsSeparatedByString:@","];
             
             dayLabel.text = [[dateComponents objectAtIndex:1] uppercaseString];
             dateLabel.text = [dateComponents objectAtIndex:0];
             NSString *month=[dateComponents objectAtIndex:2];
             NSString *year=[dateComponents objectAtIndex:3];
             
             NSString* monthAndYear = [[month uppercaseString] stringByAppendingFormat:@" %@",[year uppercaseString]];
             monthAndYearLabel.text = monthAndYear;
             
             [upcomingVisitsTableView reloadData];
             [self manageVisibility];
         }
         else if(_wasVisitsAvailable)
         {
             [upcomingVisitsTableView reloadData]; 
             [self manageVisibility];
         }

     }];
    
    [descriptor release];

}

-(void)manageVisibility
{
    int _noOfVisits =[self.visitsDataSource count];
    BOOL b =_noOfVisits>0;
    
    //self.view.hidden=!b;
    self.labelOnNoVisits.hidden     = b;
    currrentDateImageView.hidden    =!b;
    upcomingVisitsTableView.hidden  =!b;
    noUpcomingVisitImageView.hidden = b;

    if(b)
    {
        noOfVisitsLabel.hidden = NO;
        noOfVisitsLabel.text   = [NSString stringWithFormat:@"%d %@",_noOfVisits,_noOfVisits==1?@"VISIT":@"VISITS"];
    }
    else
    {
        noOfVisitsLabel.hidden = YES;
    }
     upcomingVisitsTableView.backgroundView.hidden  = (_noOfVisits>=3);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self reload];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark Table View implementations..

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    int _visitCount = MIN(MAX_VISIT,[_visitsDataSource count]);
    return _visitCount;
    // int _visitCount = [_visitsDataSource count];
    //return _visitCount>5?5:_visitCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     Class TableViewCellClass = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [InsetTableViewCell class] : [UITableViewCell class];
    
    static NSString* UpcomingVisitCellIdentifier = @"upcomingVisitCellIdentifier";
    
    UIImageView* cellSeparatorImage =   nil;
    UILabel* showTimeofVisitLabel = nil;
    UILabel* showVisitingCustomerNameLabel = nil;
    UILabel* showVisitingCustomerLocationSuburbLabel = nil;
    
    UITableViewCell* upcomingVisitCell  =   [tableView dequeueReusableCellWithIdentifier:UpcomingVisitCellIdentifier];
    
    if (upcomingVisitCell == nil) 
    {
        upcomingVisitCell = [[[TableViewCellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:UpcomingVisitCellIdentifier] autorelease];
        upcomingVisitCell.backgroundColor   =   VISIT_CELL_BACKGROUND_COLOR;
        cellSeparatorImage =   [UIView imageWithFrame:CGRectMake(75.0, 0.0, 2.0, UPCOMING_VISITS_CELL_HEIGHT) fileName:UPCOMING_VISITS_TABLEVIEW_CELL_SEPARATOR_IMAGE];
        cellSeparatorImage.tag  =   1;
        cellSeparatorImage.autoresizingMask =   UIViewAutoresizingNone;
        [upcomingVisitCell.contentView addSubview:cellSeparatorImage];
        upcomingVisitCell.selectionStyle    =   UITableViewCellSelectionStyleNone;
        
        showTimeofVisitLabel = [UIView labelWithText:@"" frame:CGRectMake(13.0, 2.0, 100.0, UPCOMING_VISITS_CELL_HEIGHT-4.0) textColor:LOGIN_BG_COLOR bgColor:[UIColor clearColor] fontsize:13.5];
        showTimeofVisitLabel.font  = [UIFont grotesqueFontOfSize:13.5];
        showTimeofVisitLabel.numberOfLines = 2;
        [upcomingVisitCell.contentView addSubview:showTimeofVisitLabel];
        showTimeofVisitLabel.textAlignment = NSTextAlignmentCenter;
        showTimeofVisitLabel.tag=2;
        
        showVisitingCustomerNameLabel = [UIView labelWithText:@"" frame:CGRectMake(114.0, 5.0, 205.0, 20.0) textColor:LOGIN_BG_COLOR bgColor:[UIColor clearColor] fontsize:13.0];
        showVisitingCustomerNameLabel.font  = [UIFont grotesqueFontOfSize:13.0];
        [upcomingVisitCell.contentView addSubview:showVisitingCustomerNameLabel];
        showVisitingCustomerNameLabel.tag=3;
        
        showVisitingCustomerLocationSuburbLabel = [UIView labelWithText:@"" frame:CGRectMake(114.0, 20.0, 205.0, 20.0) textColor:LOGIN_BG_COLOR bgColor:[UIColor clearColor] fontsize:10.0];
        showVisitingCustomerLocationSuburbLabel.font  = [UIFont grotesqueFontOfSize:10.0];
        [upcomingVisitCell.contentView addSubview:showVisitingCustomerLocationSuburbLabel];
         showVisitingCustomerLocationSuburbLabel.tag=4;
    }  
    else
    {
        cellSeparatorImage   = (UIImageView*)[upcomingVisitCell.contentView viewWithTag:1];
        showTimeofVisitLabel = (UILabel *)[upcomingVisitCell.contentView viewWithTag:2];
        showVisitingCustomerNameLabel = (UILabel *)[upcomingVisitCell.contentView viewWithTag:3];
        showVisitingCustomerLocationSuburbLabel = (UILabel *)[upcomingVisitCell.contentView viewWithTag:4];
    }
    
    [upcomingVisitCell.contentView bringSubviewToFront:cellSeparatorImage];
    
    ScheduledVisit *event = indexPath.row < [_visitsDataSource count] ? [_visitsDataSource objectAtIndex:indexPath.row] : nil;
    
    if(event)
    {
        [event retain];
        
        //NSLog(@"CustomerName:%@",event.customerName);
        // NSLog(@"customer.name:%@",event.customer.name);
        
        showVisitingCustomerNameLabel.text=event.customer ? event.customer.name : event.customerName;
        showVisitingCustomerNameLabel.textColor=LOGIN_BG_COLOR;
        
        //=======================================================================
        // PENDING: CustomerName and suburbName to be fetched in a optimized way.
        //=======================================================================

        /*
        NSMutableArray *_customers=[[CoreDataHandler allCustomers] retain];
        NSArray *arr= [_customers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.name=%@",event.title]];
        [_customers release];
        if([arr count])
        {
            Customer *_customer=[arr objectAtIndex:0];
            showVisitingCustomerLocationSuburbLabel.text=_customer.suburb;
        }
        */
        /*
        NSString *notes=event.notes;
        NSArray *notesArr=[notes componentsSeparatedByString:@"+"];
        if([notesArr count]>=2)
        {
            NSString *suburbName = [notesArr objectAtIndex:1];
            showVisitingCustomerLocationSuburbLabel.text = suburbName;
        }
        */
        
        showVisitingCustomerLocationSuburbLabel.text = event.customer ? event.customer.suburb :  event.stateName;

        //=======================================================================
        // PENDING: CustomerName and suburbName to be fetched in a optimized way.
        //=======================================================================

        //NSString *dateFormatString = indexPath.row==0?@"hh:mm a":@"dd-MM-yy hh:mm a";
        NSString *dateFormatString = @"h:mm a";// @"hh:mm a";
        NSString *dateString=[event.visitStartDate stringFromDateWithFormat:dateFormatString];
        showTimeofVisitLabel.text=dateString;
        showTimeofVisitLabel.textColor=LOGIN_BG_COLOR;
        
        [event release];
    }
    else 
    {
        showVisitingCustomerNameLabel.text=@"NO VISIT";
        showVisitingCustomerNameLabel.textColor=[UIColor lightGrayColor];
        showTimeofVisitLabel.text=@"N/A";
        showTimeofVisitLabel.textColor=[UIColor lightGrayColor];
        
        
    }
 

    return upcomingVisitCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EKEvent *event = indexPath.row < [_visitsDataSource count] ? [_visitsDataSource objectAtIndex:indexPath.row] : nil;
    if(!event) return;
    
    [self showCalenderForVisit:event];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = upcomingVisitsTableView.separatorColor;
    _bgView.fillColor = CELL_BACKGROUND_COLOR;
    _bgView.tag = 1;
    
    
    int numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
}

#pragma mark ---

@end
