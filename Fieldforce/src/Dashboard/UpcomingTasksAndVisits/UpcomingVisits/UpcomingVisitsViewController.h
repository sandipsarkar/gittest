//
//  UpcomingVisitsViewController.h
//  RepVisitationTool
//
//  Created by Somsankar Ray on 14/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpcomingVisitsViewController : UIViewController
{
    NSMutableArray* _visitsDataSource;
    UITableView* upcomingVisitsTableView;
    UIImageView* currrentDateImageView;
    
    UIImageView *noUpcomingVisitImageView;
    
    UILabel* dayLabel ;
    UILabel* dateLabel;
    UILabel* monthAndYearLabel;
    UILabel *noOfVisitsLabel;
    
    UIActivityIndicatorView *_loaderView;
}

@property (nonatomic,retain) NSMutableArray* visitsDataSource;
@property(nonatomic,assign) UILabel *labelOnNoVisits;

-(void)reload;
-(void)manageVisibility;
-(void)fetchFromServer;
-(void)willRefresh;

@end
