//
//  UpcomingTasksViewController.m
//  RepVisitationTool
//
//  Created by Somsankar Ray on 07/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//
#define TASK_FROM_CUSTOMER 0

#import "UpcomingTasksViewController.h"
#import "Task.h"
#import "TaskTableViewCell.h"
#import "TaskDetailViewController.h"
#import "MenuViewController.h"
#import "CustomersViewController.h"
#import "OrganizeViewController.h"
#import "CustomerProfileViewController.h"
#import "Rep.h"
#import "Customer.h"
#import "CustomEventStore.h"
#import "OfflineManager.h"

#define UPCOMING_TASKS_HEADER_IMAGE   @"upcoming_tasks.png"
#define UPCOMING_TASKS_FOOTER_IMAGE   @"view_task_list_button.png"
#define UPCOMING_TASKS_NO_TASKS_IMAGE @"no_upcoming_tasks_field.png"
#define UPCOMING_TASKS_TABLE_BG_IMAGE @"no_upcoming_tasks_field_blank.png"

#define VIEW_BACKGROUND_COLOR   [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0]

#define MAX_ROW 4

@interface UpcomingTasksViewController()

@property(nonatomic,retain) ASIHTTPRequest *currentTasksRequest;
@end

@implementation UpcomingTasksViewController

@synthesize tasksDataSource = _tasksDataSource;
@synthesize currentTasksRequest;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    
    if (self)
    {
        _tasksDataSource = [[NSMutableArray alloc] init];  
        
        
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

-(void)dealloc
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    currentTasksRequest.delegate = nil;
    [currentTasksRequest clearDelegatesAndCancel];
    [currentTasksRequest release];
    currentTasksRequest = nil;
    
    [_tasksDataSource release] ;
    _tasksDataSource     = nil;
    
    [noUpcommingTaskLabel release] ;
    noUpcommingTaskLabel = nil;
    
    [_loaderView release];
    _loaderView = nil;
    
    [super dealloc];
}

-(void)_manageIsUpcommingTaskAvailable:(BOOL)isTaskAvailable
{
    noUpcommingTaskLabel.hidden = isTaskAvailable;
    // self.tableView.tableHeaderView.hidden = !isTaskAvailable;
    //viewTaskListButton.hidden = !isTaskAvailable;
    self.tableView.backgroundView.hidden = !isTaskAvailable;
}

#pragma mark - View lifecycle

-(void)setupViewTaskList
{
    UIImage* buttonFooterImage       =   [UIImage imageNamedNoCache:UPCOMING_TASKS_FOOTER_IMAGE];
    CGRect footerImageFrame          =   CGRectMake((CGRectGetWidth(self.view.bounds)-buttonFooterImage.size.width)/2.0, CGRectGetHeight(self.tableView.bounds)-self.tableView.contentInset.bottom-2.0, buttonFooterImage.size.width, buttonFooterImage.size.height);
    
    viewTaskListButton     =   [UIButton buttonWithType:UIButtonTypeCustom];
    viewTaskListButton.frame = footerImageFrame;
    viewTaskListButton.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin  | 
                                         UIViewAutoresizingFlexibleRightMargin |
                                         UIViewAutoresizingFlexibleTopMargin;
    
    [viewTaskListButton setBackgroundImage:buttonFooterImage forState:UIControlStateNormal];
    [self.view addSubview:viewTaskListButton];
    [viewTaskListButton addTarget:self action:@selector(openOrganizeTaskList) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setupTableFooterView
{
    NSMutableArray *dataSource = self.tasksDataSource;
    
    CGFloat __bottomInset =  self.tableView.contentInset.bottom;
    const CGFloat viewListButtonHeight = __bottomInset;
    CGFloat totalHeight=CGRectGetHeight(self.tableView.bounds)-__bottomInset;
    
    CGFloat footerHeight =totalHeight - self.tableView.rowHeight * [dataSource count]-viewListButtonHeight;
    
    
    UIView *footerView = self.tableView.tableFooterView;
    if(!footerView)
    {
    footerView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), footerHeight)];
    //footerView.backgroundColor     = [[UIColor greenColor] colorWithAlphaComponent:0.6];
        footerView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = footerView;
    [footerView release];  
    }
    else 
    {
        CGRect footerRect=footerView.frame;
        footerRect.size.height = footerHeight;
        footerView.frame = footerRect;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.tableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 60.0, 0); //58.0
    self.tableView.scrollEnabled    =   NO;   
    self.tableView.rowHeight = 56;
    self.tableView.sectionFooterHeight = 1.0;
    
    
    CGFloat _headerHeight = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? 44.0 : 34.0;
    UIView* headerView              =   [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, _headerHeight)];
    headerView.backgroundColor      =   VIEW_BACKGROUND_COLOR;
    UIView* headerImageView         =   [self pageHeaderViewForImageName:UPCOMING_TASKS_HEADER_IMAGE];
    CGRect headerImageViewBounds    =   headerImageView.bounds;
    headerImageViewBounds.origin    =   CGPointMake(headerView.bounds.origin.x+19, headerView.bounds.origin.y+15);
    
   // headerImageViewBounds.origin.y += 5.0;
    headerImageView.frame           =   headerImageViewBounds;
    [headerView addSubview: headerImageView];
    self.tableView.tableHeaderView  =   headerView;
    [headerView release];
    
    
    //UIView* tableBGView =   [[UIView alloc] initWithFrame:self.view.frame];
    UIImageView* tableBGView =   [[UIImageView alloc] initWithFrame:self.view.bounds];
    tableBGView.image = [UIImage imageNamedNoCache:UPCOMING_TASKS_TABLE_BG_IMAGE];
    tableBGView.contentMode = UIViewContentModeScaleAspectFill;
    tableBGView.backgroundColor   =  VIEW_BACKGROUND_COLOR;//  ;
    self.tableView.backgroundView =   tableBGView;
    [tableBGView release];
    
   if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
   {
       self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
   }
    
    /*
     noUpcommingTaskLabel = [[UILabel alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.view.bounds)-200.0)/2.0, (CGRectGetHeight(self.view.bounds)-30.0)/2.0, 200.0, 30.0)];
     noUpcommingTaskLabel.backgroundColor = [UIColor clearColor];
     noUpcommingTaskLabel.autoresizingMask =  UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
     noUpcommingTaskLabel.text = @"No upcomming tasks";
     noUpcommingTaskLabel.textAlignment = UITextAlignmentCenter;
     noUpcommingTaskLabel.textColor = [UIColor darkGrayColor];
     noUpcommingTaskLabel.font = [UIFont subaruBoldFontOfSize:18];
     [self.view addSubview:noUpcommingTaskLabel];
     */
    
    UIImage *noTaskImage = [UIImage imageNamedNoCache:UPCOMING_TASKS_NO_TASKS_IMAGE];
    noUpcommingTaskLabel = [[UIImageView alloc ] initWithImage:noTaskImage];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        noUpcommingTaskLabel.frame = CGRectMake((CGRectGetWidth(self.view.bounds)-noTaskImage.size.width)/2.0, _headerHeight, noTaskImage.size.width, noTaskImage.size.height);
        
    }
    else
    {
        /*
        noUpcommingTaskLabel.frame = CGRectMake((CGRectGetWidth(self.view.bounds)-noTaskImage.size.width)/2.0, (CGRectGetHeight(self.view.bounds)-noTaskImage.size.height)/2.0, noTaskImage.size.width, noTaskImage.size.height);
        
         noUpcommingTaskLabel.autoresizingMask =  UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
         */
        
        noUpcommingTaskLabel.frame = CGRectMake((CGRectGetWidth(self.view.bounds)-noTaskImage.size.width)/2.0, _headerHeight+10.0, noTaskImage.size.width, noTaskImage.size.height);
        
    }
   
    noUpcommingTaskLabel.backgroundColor = [UIColor clearColor];
     noUpcommingTaskLabel.autoresizingMask =  UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin ;
    
   
    [self.view addSubview:noUpcommingTaskLabel];
    

    
    
    _loaderView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _loaderView.frame = CGRectMake((CGRectGetWidth(noUpcommingTaskLabel.frame)-CGRectGetWidth(_loaderView.frame))/2.0, (CGRectGetHeight(noUpcommingTaskLabel.frame)-CGRectGetHeight(_loaderView.frame)-50.0)/2.0, CGRectGetWidth(_loaderView.frame), CGRectGetHeight(_loaderView.frame));
    [noUpcommingTaskLabel addSubview:_loaderView];
    _loaderView.hidesWhenStopped = YES;
    [_loaderView startAnimating];
    
    [self performSelector:@selector(stopLoading) withObject:nil afterDelay:25.0];
    
    
    [self setupTableFooterView];
    [self setupViewTaskList];
    
    [self _manageIsUpcommingTaskAvailable:NO];
    
    
    
}

-(void)stopLoading
{
    if(_loaderView.isAnimating)
       [_loaderView stopAnimating];
}

-(void)reloadData
{
    
    [CoreDataHandler fetchUpcommingTasksOnCompletion:^(NSArray *result, NSError *error)
     {
        NSArray *upcommingTasks=result;
        if([upcommingTasks count]>=MAX_ROW)
        {
            upcommingTasks = [upcommingTasks subarrayWithRange:NSMakeRange(0, MAX_ROW)];
        }
        
         BOOL isUpcommingTasksAvailable  = [upcommingTasks count] > 0;

         [self _manageIsUpcommingTaskAvailable:isUpcommingTasksAvailable];
         
         self.tableView.backgroundView.hidden = ([upcommingTasks count]>=MAX_ROW);
        
         [self.tasksDataSource setArray:upcommingTasks];
         
         [self setupTableFooterView];
         [self.tableView reloadData];
         
         //if(_loaderView.isAnimating)
            // [_loaderView stopAnimating];
    }];
   
}

#pragma mark -UNDER TESTING
-(void)_fetchTasksFromServer
{
    if(!app.isCustomerLoaded) return;
    if(isFetchingFromServer) return;
    
    isFetchingFromServer = YES;
    
    if(!_loaderView.isAnimating)
        [_loaderView startAnimating];
    
    
  __block  UpcomingTasksViewController *weakSelf =self;
    ASIHTTPRequest *request = [ConnectionManager fetchAndProcessTasksOnCompletion:^(BOOL success, NSError *error)
    {
        if(success)
        {
           [weakSelf reloadData];
        }
        
         weakSelf->isFetchingFromServer = NO;
        
        if(weakSelf->_loaderView.isAnimating)
            [weakSelf->_loaderView stopAnimating];
        
    }];
    
    self.currentTasksRequest = request;
    
}


-(void)fetchTasksFromServer
{
    if(!app.isCustomerLoaded) return;
    if(isFetchingFromServer) return;
    
    isFetchingFromServer = YES;
    
    if(!_loaderView.isAnimating)
      [_loaderView startAnimating];
    
    Rep *_rep = [CoreDataHandler currentRep];
    
    ASIHTTPRequest *request = [ConnectionManager createTaskConnectionWithRepID:_rep.repID wdSiteID:_rep.parentWdSite.wdSiteID wdID:_rep.parentWdSite.parentWdID];
    request.delegate = self;
    
    self.currentTasksRequest = request;

}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    isFetchingFromServer = NO;

    
    NSDictionary *jsonDict = [request responseJSON];
    
    NSLog(@"json=%@",[request responseString]);
    
    if(jsonDict && [jsonDict isKindOfClass:[NSDictionary class]])
    {
        NSArray *_tasks = [jsonDict valueForKey:JSON_DETAILS_KEY];
        NSArray *_updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
        NSArray *_deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
        
        _tasks      = NULL_NIL(_tasks);
        _updatedIDs = NULL_NIL(_updatedIDs);
        _deletedIDs = NULL_NIL(_deletedIDs);
        
        NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
       
        lastRequested = NULL_NIL(lastRequested);
        
        if(!([_tasks count] ||[_deletedIDs count]))
        {
            [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_TASKS];
        }
        
        
        [Task populateAsyncPerformingDelete:_deletedIDs?_deletedIDs:[NSArray array] update:_updatedIDs?_updatedIDs:[NSArray array] insertOnArray:_tasks?_tasks:[NSArray array] onAttribute:@"taskID" completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
         {
             [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_TASKS];
             
             dispatch_async([app eventQueue], ^{
                 
                 NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType] ;
                 [context setParentContext:DEFAULT_CONTEXT];
                 [context setUndoManager:nil];
                 
                 for(Task *task in insertedResult)
                 {
                     Task *_task =(Task *)[task inContext:context];
                     
                     if(_task.customerID)
                     {
                         
                         NSMutableArray *arr = [Customer fetchOnContext:context predicate:[NSPredicate predicateWithFormat:@"self.customerID = %@",_task.customerID] onAttributes:nil error:nil];
                         
                         Customer *_customer=[arr count]?[arr objectAtIndex:0] : nil;
                         if(_customer) [_customer addTasksObject:_task];
                         else
                         {
                             //[context deleteObject:_task];
                         }
                         
                     }
                 }
                 
                 for(Task *task in updatedResult)
                 {
                      Task *_task =(Task *)[task inContext:context];
                     
                     if(_task.parentCustomer)
                     {
                         if(![_task.parentCustomer.customerID isEqualToNumber:_task.customerID])
                         {
                             [_task.parentCustomer removeTasksObject:_task];
                         }
                     }
                     NSLog(@"Customer ID:%@",_task.customerID);
                     
                     if(!_task.parentCustomer || ![_task.parentCustomer.customerID isEqualToNumber:_task.customerID])
                     {
                          NSMutableArray *arr = [Customer fetchOnContext:context predicate:[NSPredicate predicateWithFormat:@"self.customerID = %@",_task.customerID] onAttributes:nil error:nil];
                         
                         Customer *_customer = [arr count]? [arr objectAtIndex:0] : nil;
                         
                         if(_customer)
                         {
                             //_visit.customer= _customer;
                            [_customer addTasksObject:_task];
                         }
                         else
                         {
                             //  [context deleteObject:_visit];
                         }
                     }
                 }
                 
                 ///////////////////////////////////////////////////////////////////
                 [[SSCoreDataManager sharedManager] saveContext:context error:nil];
                 //////////////////////////////////////////////////////////////////
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     
                     
                     if(_loaderView.isAnimating)
                         [_loaderView stopAnimating];
                     
                     [self reloadData];
                     
                     
                     //[[NSNotificationCenter defaultCenter] postNotificationName:CalendarShouldUpdate object:nil];
                 });
                 
          
                 
                 for(Task *task  in insertedResult)
                 {
                     Task *_task =(Task *)[task inContext:context];
                     
                     if([task respondsToSelector:NSSelectorFromString(@"eventIdentifier")])
                     {
                         EKEvent * event =nil;
                         NSString *eventIdentifier = _task.eventIdentifier;
                         
                         NSString *customerName =[NSString stringWithFormat:@"Task: %@", _task.subject.length?_task.subject : @"task"];
                         
                         if(eventIdentifier.length)
                             event = [[CustomEventStore sharedInstance].eventStore eventWithIdentifier:eventIdentifier];
                         
                         if(!event)
                         {
                             event = [CustomEventStore addEventWithTitle:customerName startDate:_task.dueDate endDate:[[_task.dueDate dateByAddingTimeInterval:60.0] currentDateHourMinute] location:nil notes:nil];
                             
                             NSLog(@"Task dueDate=%@ eventDate=%@",_task.dueDate,event.startDate);
                             [event retain];
                         }
                         else
                         {
                             event = [CustomEventStore editEventForIdentifier:_task.eventIdentifier withTitle:customerName startDate:_task.dueDate endDate:[[_task.dueDate dateByAddingTimeInterval:60.0] currentDateHourMinute] location:nil notes:nil];
                             
                             [event retain];
                         }
                         
                         if(event && event.eventIdentifier.length)
                             _task.eventIdentifier = event.eventIdentifier;
                         
                         [event release];
                     }
                     
                     
                 }
                 
                 if([_updatedIDs count])
                 {
                     
                     for(Task *task in updatedResult)
                     {
                         Task *_task = (Task *)[task inContext:context];
                         
                         if([_task respondsToSelector:NSSelectorFromString(@"eventIdentifier")])
                         {
                             NSString *customerName =[NSString stringWithFormat:@"Task: %@", _task.subject.length?_task.subject : @"task"];
                             
                             EKEvent *_event =  [CustomEventStore editEventForIdentifier:_task.eventIdentifier withTitle:customerName startDate:_task.dueDate endDate:[[_task.dueDate dateByAddingTimeInterval:60.0] currentDateHourMinute] location:nil notes:nil];
                             
                             if(_event && _event.eventIdentifier.length)
                                 _task.eventIdentifier = _event.eventIdentifier;
                         }
                     }
                     
                 }
                 
                 app.eventFetchCount++;
                 [[SSCoreDataManager sharedManager] saveContext:context error:nil];
                 
                 [context reset];
                 [context release];
             });
         }];
    }
    
    if(_loaderView.isAnimating)
        [_loaderView stopAnimating];
    
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    isFetchingFromServer = NO;
    
    if(_loaderView.isAnimating)
        [_loaderView stopAnimating];
}

-(void)willRefresh
{
    [self reloadData];

    [self fetchTasksFromServer];

}

-(void)openOrganizeTaskList
{
     MenuViewController *masterController=(MenuViewController *) app.splitViewController.masterController;
     [masterController selectSection:2];
}
- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   // return MAX(self.tasksDataSource.count,MAX_ROW) + 1;
    
    return MIN(self.tasksDataSource.count,MAX_ROW);
   // return self.tasksDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TaskCellIdentifier = @"taskCell";
   // static NSString *ViewTaskListIdentifier = @"viewTaskList";
    
    UITableViewCell *taskCell = nil;
    
    /*
    if (indexPath.row == MAX_ROW) 
    {
        taskCell= [tableView dequeueReusableCellWithIdentifier:ViewTaskListIdentifier];
        if (taskCell == nil) {
            taskCell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ViewTaskListIdentifier] autorelease];
        }
        UIImage* buttonFooterImage  =   [UIImage imageNamed:UPCOMING_TASKS_FOOTER_IMAGE];
        CGRect footerImageFrame     =   CGRectMake(5, 0, buttonFooterImage.size.width, buttonFooterImage.size.height);
        UIImageView* footerImageView    =   [UIView imageWithFrame:footerImageFrame fileName:UPCOMING_TASKS_FOOTER_IMAGE];
        taskCell.backgroundView =   footerImageView;
        taskCell.selectionStyle =   UITableViewCellSelectionStyleNone;
    } 
    else 
     */
    {
        
        Task *_task =  (indexPath.row < [self.tasksDataSource count]) ? [self.tasksDataSource objectAtIndex:indexPath.row] : nil;
        
        taskCell= [tableView dequeueReusableCellWithIdentifier:TaskCellIdentifier];
        
        if (taskCell == nil) 
        {
            taskCell = [[[TaskTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TaskCellIdentifier] autorelease];
            
          //  [cell.completedCheckBox addTarget:self action:@selector(completeTaskAction:) forControlEvents:UIControlEventTouchUpInside];
        }

        TaskTableViewCell *_taskCell = (TaskTableViewCell *)taskCell;
        
        if(_task)
        {
            _taskCell.userInteractionEnabled = YES;
            _taskCell.taskInfo = _task;
            _taskCell.nameLabel.textColor = [UIColor blackColor];
            _taskCell.selectionStyle = UITableViewCellSelectionStyleBlue;
            _taskCell.hideCustomer = !_task.parentCustomer.name;
        }
        else
        {
            _taskCell.nameLabel.text  = @"No Task";  
            _taskCell.nameLabel.textColor = [UIColor grayColor];
            _taskCell.selectionStyle = UITableViewCellSelectionStyleNone;
            _taskCell.userInteractionEnabled = NO;
        }

    } 
    
    return taskCell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = CELL_BACKGROUND_COLOR;
    
    
    CellBackgroundView *_selectedBgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _selectedBgView.backgroundColor = [UIColor clearColor];
    _selectedBgView.contentInset = CGPointMake(10.0, 0.0);
    _selectedBgView.cornerRadius = 8.0;
    _selectedBgView.borderColor = self.tableView.separatorColor;
    _selectedBgView.fillColor = [UIColor cellSelectedColorBlue];
    
    
    
    int numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    _selectedBgView.position = _bgView.position;
    
    
    cell.backgroundView = _bgView;
    cell.selectedBackgroundView = _selectedBgView;
    
    [_bgView release];
    [_selectedBgView release];
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   Task *_task =  (indexPath.row < [self.tasksDataSource count]) ? [self.tasksDataSource objectAtIndex:indexPath.row] : nil;
    
    if(!_task) return;
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
     MenuViewController *masterController = (MenuViewController *) app.splitViewController.masterController;
    
    if(_task.parentCustomer)  
    {
        
 #if TASK_FROM_CUSTOMER
        
        //SELECT CUSTOMER-> TASKLIST-> THE TASK
         [masterController selectSection:1];
         
         UINavigationController *navController=(UINavigationController *)[app.splitViewController detailController] ;
         CustomersViewController *custController =(CustomersViewController *) [navController topViewController];
         
         [custController addCustomersDidLoadCallback:^{
         
         [custController selectCustomerForTask:_task];
         
         }];
#else
        
        //========================= ADDED ON JAN 3 2012 ==========================================
        
        CustomerProfileViewController *_customerProfileVc=[[CustomerProfileViewController alloc] initWithCustomer:_task.parentCustomer];
   
        _customerProfileVc.parentTabbarControlller.selectedIndex=1;
        UINavigationController *_navController = [self nextResponderNavigationController];
        if(_navController) 
        {
            [_navController pushViewController:_customerProfileVc animated:YES];
            
            if(_customerProfileVc.parentTabbarControlller)
           [_customerProfileVc.tasksViewController selectTask:_task animated:NO];
        }

        [_customerProfileVc release];
        
        /*
        TasksViewController *tasksViewController = [[TasksViewController alloc] initWithCustomer:_task.parentCustomer];
        
        
        UINavigationController *_navController = [self nextResponderNavigationController];
        if(_navController) 
        {
            [_navController pushViewController:tasksViewController animated:YES];
           // [tasksViewController.taskListViewController selectTask:_task animated:NO];
            [tasksViewController selectTask:_task animated:NO];
        }
        */
        /*
        UINavigationController *_navController =[[UINavigationController alloc] initWithRootViewController:tasksViewController];
        _navController.modalPresentationStyle=UIModalPresentationPageSheet;
        _navController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [app.splitViewController presentModalViewController:_navController animated:YES];
         [tasksViewController selectTask:_task animated:NO];
        [_navController release];
         */
        
       // [tasksViewController release];
        //========================================================================================================
#endif
        
    }
    else  //SELECT ORGANIZE TASKLIST -> SELECTED TASK
    {
      
        [masterController selectSection:2];
        UINavigationController *navController=(UINavigationController *)[app.splitViewController detailController] ;
        OrganizeViewController *organizeController =(OrganizeViewController *) [navController topViewController];
        
       __block OrganizeViewController *weakOrganizeController = organizeController;
        
       // [organizeController.tasksViewController.taskListViewController selectTask:_task animated:NO];
        
        
     __block   BOOL _taskSelected = NO;
        [organizeController.tasksViewController addOrganiseTasksDidLoadCallback:^{
            
        //[organizeController.tasksViewController.taskListViewController selectTask:_task animated:NO];
            
            if(!_taskSelected)
            {
               [weakOrganizeController.tasksViewController selectTask:_task animated:YES];
                _taskSelected = YES;
            }
            
        }];
    }

}

-(void)completeTaskAction:(SSCheckBox *)checkBox
{
    UIAlertView *alert = [UIAlertView alertViewWithTitle:@"" message:TASK_COMPLETE_MESSAGE cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObjects:@"Yes",nil] onDismiss:^(int buttonIndex)
                          {
                              checkBox.checked = YES;
                              checkBox.userInteractionEnabled = NO;
                              
                              BOOL _isCompleted = checkBox.checked;
                              
                              TaskTableViewCell *taskCell =(TaskTableViewCell *)[[checkBox superview] superview];
                              Task *_task = taskCell.taskInfo;
                              
                              if(!_task) return;
                              _task.completedDate = [NSDate date];
                              _task.isCompleted   = [NSNumber numberWithBool:_isCompleted];
                              taskCell.starView.userInteractionEnabled = NO;

                              
                              [[OfflineManager sharedManager] addTask:_task willDelete:NO isEditMode:YES];
 
                              
                          }
                            onCancel:^{
                                                    
                                checkBox.checked = NO;
                                checkBox.userInteractionEnabled = YES;
                        }];
    
    [alert release];
    
    
}

@end
