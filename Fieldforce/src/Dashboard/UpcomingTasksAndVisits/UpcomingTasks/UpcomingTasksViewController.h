//
//  UpcomingTasksViewController.h
//  RepVisitationTool
//
//  Created by Somsankar Ray on 07/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpcomingTasksViewController : UITableViewController
{
    NSMutableArray* _tasksDataSource;
    
    UIImageView *noUpcommingTaskLabel;
    UIButton    *viewTaskListButton;
    UIActivityIndicatorView *_loaderView;
    
    BOOL isFetchingFromServer;
}

@property (nonatomic,retain) NSMutableArray* tasksDataSource;


-(void)fetchTasksFromServer;
-(void)willRefresh;

@end
