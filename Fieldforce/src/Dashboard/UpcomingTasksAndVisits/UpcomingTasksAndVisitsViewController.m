//
//  UpcomingTasksAndVisitsViewController.m
//  RepVisitationTool
//
//  Created by Somsankar Ray on 05/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "UpcomingTasksAndVisitsViewController.h"
#import "UpcomingTasksViewController.h"
#import "UpcomingVisitsViewController.h"

#define UPCOMING_VIEW_HEADER_IMAGE @"upcoming_heading.png"
#define VIEW_BACKGROUND_COLOR   [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0]

@interface  UpcomingTasksAndVisitsViewController ()
{
    UILabel *notaskLabel;
}

@end

@implementation UpcomingTasksAndVisitsViewController

-(void)dealloc
{
    [upcomingTaskVC release];
    upcomingTaskVC = nil;
    [upcomingVisitsVC release];
    upcomingVisitsVC = nil;
    [notaskLabel release];
    notaskLabel=nil;
    
    [super dealloc];
}

- (id)init 
{
    self = [super init];    
    if (self) 
    {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.backgroundColor       =   [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0];
    UIImageView* headerImageView    =   [self pageHeaderViewForImageName:UPCOMING_VIEW_HEADER_IMAGE];
    [self.view addSubview:headerImageView];
    //[self addDefaultBorder];
    
    
    CGRect headerImageFrame =   headerImageView.bounds;
    CGRect upcomingTasksFrame  =   CGRectMake(CGRectGetMinX(headerImageFrame), CGRectGetMaxY(headerImageFrame), CGRectGetWidth(headerImageFrame)-30, 330);
    upcomingTaskVC              =   [[UpcomingTasksViewController alloc] initWithStyle:UITableViewStyleGrouped];
    upcomingTaskVC.view.frame   =   upcomingTasksFrame;
    upcomingTaskVC.view.autoresizingMask  =   UIViewAutoresizingFlexibleRightMargin;
     [self addChildViewController:upcomingTaskVC];
    [self.view addSubview:upcomingTaskVC.view];
    [upcomingTaskVC didMoveToParentViewController:self];
    
    CGRect visitViewFrame   = CGRectMake(CGRectGetMinX(upcomingTasksFrame), CGRectGetMinY(upcomingTasksFrame)+CGRectGetHeight(upcomingTasksFrame), CGRectGetWidth(upcomingTasksFrame), 260);
    
    /*
    notaskLabel=[[UILabel alloc] initWithFrame:visitViewFrame];
    notaskLabel.text=@"No upcomming visits";
    notaskLabel.backgroundColor = VIEW_BACKGROUND_COLOR;
    notaskLabel.font = [UIFont subaruBoldFontOfSize:18];
    notaskLabel.textAlignment = UITextAlignmentCenter;
    notaskLabel.autoresizingMask  =   UIViewAutoresizingFlexibleRightMargin;  
    notaskLabel.textColor = [UIColor darkGrayColor];
    [self.view addSubview:notaskLabel];
    notaskLabel.hidden=YES;
     */

    upcomingVisitsVC = [[UpcomingVisitsViewController alloc] init];
    upcomingVisitsVC.labelOnNoVisits = notaskLabel;
    upcomingVisitsVC.view.frame =   visitViewFrame;
    upcomingVisitsVC.view.autoresizingMask  =   UIViewAutoresizingFlexibleRightMargin;
     [self addChildViewController:upcomingVisitsVC];
    [self.view addSubview:upcomingVisitsVC.view];
    [upcomingVisitsVC didMoveToParentViewController:self];
    
    [self.view addLeftBoderWithColor:[UIColor colorWithWhite:0.2 alpha:0.4] borderWidth:1.0];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
   // [upcomingTaskVC viewWillAppear:animated];
   // [upcomingVisitsVC viewWillAppear:animated];
    
    [upcomingVisitsVC manageVisibility];
}

-(void)refresh
{
    [upcomingVisitsVC willRefresh];
    [upcomingTaskVC willRefresh];
}

-(void)fetchFromServer
{
      NSLog(@"Fetching upcomming");
      [upcomingVisitsVC fetchFromServer];
      [upcomingTaskVC fetchTasksFromServer];
    
}

-(void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
   
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark -- designing of the view


#pragma mark----

@end
