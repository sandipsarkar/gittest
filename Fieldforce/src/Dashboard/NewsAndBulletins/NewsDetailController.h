//
//  NewsDetailController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 11/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>


@class  News,EGOTextView;
@interface NewsDetailController : UIViewController
{
    UILabel *_titleLabel;
    UILabel *_dateLabel;
   // UITextView *_descriptionTextView;
    
    UITextView *_descriptionTextView;
    
    UIImageView *_newsImageView;
    
    UISwipeGestureRecognizer *oneFingerSwipeRight;
    UISwipeGestureRecognizer *oneFingerSwipeLeft;
    NSInteger currentIndex;
    NSMutableArray *arrNews;
    UIView *newsContainerView;
    
    UIButton *pdfButton;
    
}

@property(nonatomic,retain) NSMutableArray *arrNews;
@property(nonatomic,retain) News *newsInfo;


//-(id)initWithNewsInfo:(News *)_newsInfo;
-(id)initWithNewsInfo:(News *)_newsInfo allNews:(NSMutableArray *)news _newsIndex:(NSInteger )newsIndex;
@end
