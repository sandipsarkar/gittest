//
//  NewsDetailController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 11/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "NewsDetailController.h"
#import "UIImageView+LazyLoading.h"
#import "News.h"
#import "OfflineManager.h"
#import "EGOTextView.h"
#import "WebLinkViewController.h"
#import "SSMemoryCache.h"

#define IMAGE_SIZE CGSizeMake(350.0, 300.0)

@interface NewsDetailController ()<UITextViewDelegate>
{
    SSMemoryCache *_imageCache;
}
@property (nonatomic,copy)NSString *currentImageURL;

-(void)resizeTextViewForNewsImage:(UIImage *)image;
-(void)viewPDFAction:(UIButton *)button;
-(void)loadImageForURLString:(NSString *)urlString;

@property(nonatomic,retain) UIBarButtonItem *viewPdfBarButton;
@end

@implementation NewsDetailController
@synthesize newsInfo;
@synthesize arrNews;
@synthesize viewPdfBarButton;
@synthesize currentImageURL;


- (id)init
{
    self = [super init];
    
    if (self) 
    {
      
    }
    return self;
}

-(id)initWithNewsInfo:(News *)_newsInfo allNews:(NSMutableArray *)news _newsIndex:(NSInteger )newsIndex
{
    if((self=[super init]))
    {
        self.newsInfo=_newsInfo;
        self.arrNews=news;
        currentIndex=newsIndex;
    }
    
    return self;
}

-(SSMemoryCache *)imageCache
{
    if(!_imageCache)
    {
    _imageCache=[[SSMemoryCache alloc] init];
    _imageCache.cacheCount = 10;
    }
    
    return _imageCache;
}

-(void)setupViewPDF
{
  if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
  {
    pdfButton=[UIButton repToolButtonWithImageName:@"view_pdf_button_blank.png" text:@"View PDF"];
   // pdfButton.frame = CGRectMake(CGRectGetMaxX(nextPageButton.frame)+5.0, 1.0, pageButtonImage.size.width, pageButtonImage.size.height);
    pdfButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    pdfButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    pdfButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
    [pdfButton addTarget:self action:@selector(openPDFAction:) forControlEvents:UIControlEventTouchUpInside];
    
    viewPdfBarButton = [[UIBarButtonItem alloc] initWithCustomView:pdfButton];
   }

   else
   {
    viewPdfBarButton = [[UIBarButtonItem alloc] initWithTitle:@"View PDF" style:UIBarButtonItemStyleDone target:self action:@selector(openPDFAction:)];
   }
    
    self.navigationItem.rightBarButtonItem = self.newsInfo.newsPdf.length ? viewPdfBarButton : nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    //[self.navigationController.navigationBar addBackGroundImage:@"autoprofiler_banner.png"];
    [self.navigationController.navigationBar addBackGroundImage:@"navigation_bar.png"];

    
    NSString *strTitle = @"ARTICLE BY: ";
    strTitle=[strTitle stringByAppendingString:self.newsInfo.creator?self.newsInfo.creator: @""];
    //NSLog(@" strTitle %@",strTitle);
    [self addTitle:strTitle withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    //[self addTitle:@"THE AUTOPROFILER HAS BEEN LAUNCHED!" withFont:[UIFont subaruBoldFontOfSize:23.0]]; 
    //[self addTitleImage:@"news_bulletins.png"];
    
    self.navigationItem.leftBarButtonItem=nil;
    self.navigationItem.hidesBackButton=YES;
    
    [self setupViewPDF];
    
     [self addBackButtonWithSelector:@selector(backButtonAction:)];
    
   // NSString *imageName =  [self.newsInfo valueForKey:@"newsImage"];
   // UIImage *_newsImage = [UIImage imageNamed:imageName];
    
    newsContainerView  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds))];
    newsContainerView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin;
    newsContainerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:newsContainerView];

    CGRect _frame=self.view.bounds;
    CGSize imageSize = IMAGE_SIZE;

    //==========================================  TITLE LABEL =========================================================================
    _titleLabel=[[UILabel labelWithText:@"THE SUBARU REP TOOL HAS BEEN LAUNCHED!" frame:CGRectMake(10, 5.0, CGRectGetWidth(_frame)-15.0, 60.0) textColor:LOGIN_BG_COLOR bgColor:[UIColor clearColor] fontsize:27.0] retain];
    _titleLabel.numberOfLines=2;
    [newsContainerView addSubview:_titleLabel];
    _titleLabel.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
    _titleLabel.text=self.newsInfo.newsTitle;
    _titleLabel.font = [UIFont subaruBoldFontOfSize:27.0];
    //==========================================  TITLE LABEL ==========================================================================
    
    
    //============================== DATE LABEL ======================================
    _dateLabel=[[UILabel labelWithText:@"Title text" frame:CGRectMake(10, CGRectGetMaxY(_titleLabel.frame)+10.0, CGRectGetWidth(_frame)/2-15.0, 15.0) textColor:LOGIN_BG_COLOR bgColor:[UIColor clearColor] fontsize:12.0] retain];
    //_dateLabel.text=self.newsInfo.createdDate;
    _dateLabel.text = [NSDate dateStringFromWCFTimeInterval:self.newsInfo.createdDate.doubleValue withFormat:NEWS_DATE_FORMAT];
    _dateLabel.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
    [newsContainerView addSubview:_dateLabel];
    _dateLabel.font = [UIFont grotesqueFontOfSize:12.0];
    //=============================== DATE LABEL ==================================================
    
    
    //=============================== DESCRIPTION TEXTVIEW ======================================
    
    CGRect _textViewFrame = CGRectMake(5, CGRectGetMaxY(_dateLabel.frame)+10, CGRectGetWidth(_frame)-25.0, CGRectGetHeight(_frame)-CGRectGetMaxY(_dateLabel.frame)-20.0);
   // _descriptionTextView=[[UITextView alloc] initWithFrame:_textViewFrame];
    //CGRectGetWidth(_titleLabel.frame)
    _descriptionTextView = [[UITextView alloc] initWithFrame:_textViewFrame];
    _descriptionTextView.delegate = self;
    _descriptionTextView.dataDetectorTypes = UIDataDetectorTypeLink;
    
    _descriptionTextView.font=[UIFont grotesqueFontOfSize:15.0];
    _descriptionTextView.textColor=[UIColor blackColor];
    _descriptionTextView.backgroundColor=[UIColor clearColor];
    _descriptionTextView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight;
    _descriptionTextView.editable=NO;
    [newsContainerView addSubview:_descriptionTextView];
    
    NSString *descString =self.newsInfo.newsDescription;
   // NSArray *descArr = [_descString componentsSeparatedByString:@"\n\n"];
   // NSString *descString = [descArr componentsJoinedByString:@"\n"];

    descString = [descString stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    _descriptionTextView.text= descString;
    
     if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        [_descriptionTextView setContentOffset:CGPointMake(0, 0) animated:NO];
    


    //========================== DESCRIPTION TEXTVIEW =============================================
    
    
   // _descriptionTextView.text=[ self.newsInfo valueForKey:@"NEWS_DETAILS"];
    /*
     NSString *_newsUrl=self.newsInfo.newsImage;
    _newsUrl=@"http://www.crunchbase.com/assets/images/resized/0005/4061/54061v1-max-250x250.jpg";
    */
    
    
    UIImage *_newsImage = nil;
   
    _newsImageView = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectGetWidth(_frame)-imageSize.width)/2.0, CGRectGetMinY(_descriptionTextView.frame)+10, 0, 0)];
    _newsImageView.backgroundColor = [UIColor clearColor];
    _newsImageView.contentMode = UIViewContentModeScaleAspectFit;
    _newsImageView.userInteractionEnabled=YES;
    [newsContainerView addSubview:_newsImageView];
    _newsImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
   // _newsImageView.image = _newsImage;
    
    [self resizeTextViewForNewsImage:_newsImage];
    

   NSString *_newsImageUrl = (self.newsInfo.newsImage) ? makeImageURL(self.newsInfo.newsImage, @"news", NO) : nil;
    
    NSLog(@"News image url:%@",_newsImageUrl);
    
    [self loadImageForURLString:_newsImageUrl];
    
    oneFingerSwipeRight =
  	[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onOneFingerSwipeLeft:)];
    [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [newsContainerView addGestureRecognizer:oneFingerSwipeRight];
    
	// --------------------------------
	// One finger, swipe left
	// --------------------------------
    oneFingerSwipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onOneFingerSwipeRight:)];
    [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [newsContainerView addGestureRecognizer:oneFingerSwipeLeft];
    

   
       /*
   [self performSelector:@selector(loadNewsImage:) withObject:[UIImage imageNamed:@"airbags_col.png"] afterDelay:2.0];
     */
    

    if([[newsInfo.userTypeName uppercaseString] isEqualToString:@"OEM"])
       [[OfflineManager sharedManager] addViewedNews:self.newsInfo];


}

-(void)loadImageForURLString:(NSString *)urlString
{
    if(!urlString) return;
    
   UIImage *_image = [[self imageCache] objectForKey:urlString];
    
    if(!_image)
    {
        self.currentImageURL = urlString;
        
    _image = [LazyImageLoader loadImageForUrlString:urlString indexPath:nil callBack:^(BOOL success, UIImage *image, NSString *urlString, NSIndexPath *indexPath, NSError *error)
     {
         if(!error && image)
         {
             [[self imageCache] setObject:image forKey:urlString];
             
             if([self.currentImageURL isEqualToString:urlString])
             {
                [self performSelector:@selector(loadNewsImage:) withObject:image];
             }
             else
             {
                 [self resizeTextViewForNewsImage:nil];
             }
         }
     }];
    }
    
    if(_image)
    {
        [_image retain];
        [self performSelector:@selector(loadNewsImage:) withObject:_image];
        [[self imageCache] setObject:_image forKey:urlString];
        [_image release];
        
    }
}
-(void)onOneFingerSwipeLeft:(UISwipeGestureRecognizer *)recognizer
{
    if(currentIndex<=0) return;
    
    CATransition *animation = [CATransition animation];
    [animation setDelegate:self];
    [animation setDuration:0.40f];
    [animation setTimingFunction: UIViewAnimationCurveEaseInOut];
    [animation setType: kCATransitionPush];
    [animation setSubtype: kCATransitionFromLeft];
    [[newsContainerView layer] addAnimation:animation forKey:@"transitionViewAnimation"];
    
    
    [self showPrevOffer];
}

-(void)onOneFingerSwipeRight:(UISwipeGestureRecognizer *)recognizer
{
    NSUInteger totalOffersCount=[self.arrNews count];
    if(currentIndex>=totalOffersCount-1) return;
    
    CATransition *animation = [CATransition animation];
    [animation setDelegate:self];
    [animation setDuration:0.40f];
    [animation setTimingFunction: UIViewAnimationCurveEaseInOut];
    [animation setType: kCATransitionPush];
    [animation setSubtype: kCATransitionFromRight];
    [[newsContainerView layer] addAnimation:animation forKey:@"transitionViewAnimation"];
    
    
    [self showNextOffer];
}
-(void)showPrevOffer
{
    currentIndex--;
    
    currentIndex=MAX(0,currentIndex);
    
    NSLog(@"Prev=%d",currentIndex);
    
    if(currentIndex < [self.arrNews count] && currentIndex>=0)
    {
        self.newsInfo=[self.arrNews objectAtIndex:currentIndex];
        NSString *strTitle=@"ARTICLE BY: ";
        strTitle=[strTitle stringByAppendingString:self.newsInfo.creator?self.newsInfo.creator: @""];
        //NSLog(@" strTitle %@",strTitle);
        [self addTitle:strTitle withFont:[UIFont subaruBoldFontOfSize:23.0]];

        _titleLabel.text=self.newsInfo.newsTitle;
        _dateLabel.text = [NSDate dateStringFromWCFTimeInterval:self.newsInfo.createdDate.doubleValue withFormat:NEWS_DATE_FORMAT];
        
        NSString *descString =self.newsInfo.newsDescription;
        //NSArray *descArr=[_descString componentsSeparatedByString:@"\n\n"];
       // NSString *descString = [descArr componentsJoinedByString:@"\n"];
         descString = [descString stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
        _descriptionTextView.text= descString;
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
           [_descriptionTextView setContentOffset:CGPointMake(0, 0) animated:NO];
       
        
         self.navigationItem.rightBarButtonItem = self.newsInfo.newsPdf.length ? self.viewPdfBarButton : nil;
            
        _newsImageView.image=nil;
        NSString *imageURL = (self.newsInfo.newsImage) ? makeImageURL(self.newsInfo.newsImage, @"news", NO) : nil;
        
        //[OFFER_IMAGE_URL stringByAppendingString:self.selectedOffer.offerImage];
        if (![self.newsInfo.isRead boolValue]) {
            self.newsInfo.isRead = [NSNumber numberWithBool:YES];
        }
       
        [self loadImageForURLString:imageURL];
        
        
        /////////////////////////////////////////////////////////////
        
        //[self handleCheckboxSelection];
    }
}

-(void)showNextOffer
{
    NSUInteger totalOffersCount=self.arrNews.count;
    currentIndex++;
    currentIndex=MIN(currentIndex,totalOffersCount);
    
    NSLog(@"Next=%d",currentIndex);
    
    if(currentIndex < [self.arrNews count]  && currentIndex>=0)
    {
        self.newsInfo=  [self.arrNews objectAtIndex:currentIndex];
        NSString *strTitle=@"ARTICLE BY: ";
        strTitle=[strTitle stringByAppendingString:self.newsInfo.creator?self.newsInfo.creator: @""];
        //NSLog(@" strTitle %@",strTitle);
        [self addTitle:strTitle withFont:[UIFont subaruBoldFontOfSize:23.0]];
  
        _titleLabel.text=self.newsInfo.newsTitle;
        _dateLabel.text = [NSDate dateStringFromWCFTimeInterval:self.newsInfo.createdDate.doubleValue withFormat:NEWS_DATE_FORMAT];
        NSString *descString =self.newsInfo.newsDescription;
         descString = [descString stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
        _descriptionTextView.text= descString;
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            [_descriptionTextView setContentOffset:CGPointMake(0, 0) animated:NO];
              
        self.navigationItem.rightBarButtonItem = self.newsInfo.newsPdf.length ? self.viewPdfBarButton : nil;
        
        if (![self.newsInfo.isRead boolValue])
        {
            self.newsInfo.isRead = [NSNumber numberWithBool:YES];
        }
         
        
        _newsImageView.image=nil;
        NSString *imageURL = (self.newsInfo.newsImage) ? makeImageURL(self.newsInfo.newsImage, @"news", NO) : nil;
        
        //[OFFER_IMAGE_URL stringByAppendingString:self.selectedOffer.offerImage];;
        [self loadImageForURLString:imageURL];        //////////////////////////////////
        
       // [self handleCheckboxSelection];
    }
}

-(void)resizeTextViewForNewsImage:(UIImage *)image
{
    CGSize imageSize = image.size;//IMAGE_SIZE;
    
    CGRect _textViewRect  =  _descriptionTextView.frame;
    CGRect _imageViewRect =  _newsImageView.frame;
    
    
    _textViewRect.size.width = CGRectGetWidth(self.view.bounds)-20.0-(image!=nil?imageSize.width:0.0);
    _imageViewRect.size=image!=nil?imageSize:CGSizeZero;
    _imageViewRect.origin.x=CGRectGetWidth(self.view.bounds)-(image!=nil?imageSize.width:0.0)-10.0;
    
    /*
    _textViewRect.origin.y = CGRectGetMaxY(_dateLabel.frame)+10.0+ (image!=nil ? imageSize.height : 0) ;
    _imageViewRect.size=image!=nil?imageSize:CGSizeZero;
   */
    
    _descriptionTextView.frame = _textViewRect;
    _newsImageView.frame = _imageViewRect;
    
    _newsImageView.image=image;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        [_descriptionTextView setContentOffset:CGPointMake(0, 0) animated:NO];
}

-(void)loadNewsImage:(UIImage *)image
{
    if(!image) return;
    UIImage *_newsImage = [UIImage imageWithCGImage:image.CGImage];// image;
    
    [self resizeTextViewForNewsImage:_newsImage];
    
    [UIView animateWithDuration:0.4 animations:^{
        _newsImageView.alpha = 0.04;
        _newsImageView.alpha = 1.0;
    }];
    
    //[ProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)backButtonAction:(UIButton *)sender
{
    if([sender isKindOfClass:[UIButton class]])
    {
        sender.userInteractionEnabled = NO;
    }
    
     [LazyImageLoader cancelAllConnections];
    [self dismissModalViewControllerAnimated:YES];
   // [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
   // if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    //    [_descriptionTextView setContentOffset:CGPointMake(0, 0) animated:NO];

}


#pragma mark -
#pragma mark Open PDF action
-(void)openPDFAction:(UIButton *)sender
{
    NSString *pdfName = [self.newsInfo valueForKey:@"newsPdf"];
    
    if(!pdfName) return;
    
    NSString *pdfUrlString = makePDFURL(pdfName, @"news");
    
    NSLog(@"News PDF link=%@",pdfUrlString);
    
    //NSString *pdfCachePath = [DirectoryManager pdfDirectoryPath];
   // pdfCachePath = [pdfCachePath stringByAppendingPathComponent:[pdfUrlString lastPathComponent]];
    
    WebLinkViewController *linkVC=[[WebLinkViewController alloc]initWithUrlString:pdfUrlString];
   // linkVC.shouldCache = YES;
   // linkVC.cachePath = pdfCachePath;
    [self.navigationController pushViewController:linkVC animated:YES];
    [linkVC release];
}
#pragma mark -

#pragma mark EGOTextViewDelegate
- (void)egoTextView:(EGOTextView*)textView didSelectURL:(NSURL*)URL
{
    NSLog(@"Selected URL:%@", URL);
    
    if(!URL) return;
    
    if([[UIApplication sharedApplication] canOpenURL:URL])
    {
        [[UIApplication sharedApplication] openURL:URL];
    }
    else 
    {
        [UIAlertView showWarningAlertWithTitle:CANNOT_OPEN_URL_HEADER message:CANNOT_OPEN_URL_MSG];
    }
    
}
#pragma mark -

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    
    // Release any cached data, images, etc that aren't in use.
    [super didReceiveMemoryWarning];
}

-(void)dealloc
{
    [LazyImageLoader cancelAllConnections];
    
    [newsInfo release];
    [arrNews release];
    
    [viewPdfBarButton release];
    
    [_titleLabel release];
    _titleLabel=nil;
    
    [_dateLabel release];
    [_descriptionTextView release];
    [_newsImageView release];
    [newsContainerView release];
    
    [oneFingerSwipeRight release];
    [oneFingerSwipeLeft release];
    
    [currentImageURL release];
    currentImageURL = nil;
    
    [_imageCache release];
    _imageCache  = nil;
    
    [super dealloc];
}

@end
