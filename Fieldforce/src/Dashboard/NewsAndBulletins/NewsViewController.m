//
//  NewsViewController.m
//  RepVisitationTool
//
//  Created by Somsankar Ray on 22/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsAndBulletinCell.h"
#import "NewsDetailController.h"
#import "Rep.h"
#import "News.h"
#import "WholesaleDealer.h"

//http://192.168.1.143/OEMIPAD/resource/news/mIR2uq.jpg

#define TABLE_HEADERVIEW_IMAGE_FILENAME  @"news_bulletins_heading.png"

#define MAX_NEWS_COUNT 50

@interface NewsViewController()
{
     BOOL isFetchingFromServer;
}
@property(nonatomic,retain) ASIHTTPRequest *currentNewsRequest;

-(void)sortNews;

@end

@implementation NewsViewController

@synthesize newsDataSource = _newsDataSource;
@synthesize tableView=_tableView;
@synthesize currentNewsRequest;

- (id)init
{
    self = [super init];
    if (self) 
    {
        _newsDataSource = [[NSMutableArray alloc] init];  
       
    }
    return self;
}

-(void)dealloc
{
    [currentNewsRequest clearDelegatesAndCancel];
    [currentNewsRequest release];
    currentNewsRequest = nil;
    
    [_tableView release];
    [_newsDataSource release];
    
    [super dealloc];
}

-(void)setupTableview
{
   
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)refreshAction
{
    [self fetchNewsFromServer];
}

-(void)fetchNewsFromServer
{
    if(isFetchingFromServer) return;
    
      isFetchingFromServer = YES;
    
    Rep *_rep          = [CoreDataHandler currentRep];
    NSNumber *wdSiteID = _rep.parentWdSite.wdSiteID;
    NSNumber *wdID     = _rep.parentWdSite.parentWdID;
    
    ASIHTTPRequest *request = [ConnectionManager createNewsConnectionWithRepID:_rep.repID wdSiteID:wdSiteID wdID:wdID];
    request.delegate = self;
    self.currentNewsRequest = request;
    
    /*
    [request setCompletionBlock:^{
        
        isFetchingFromServer = NO;
        
         [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
        
       // NSString *jsonString = [request responseString];
        NSLog(@"News json:%@",jsonString);
        
        NSDictionary *jsonDict = [request responseJSON];
        
        // NSLog(@"News jsonDict:%@",jsonDict);
        if(jsonDict && [jsonDict isKindOfClass:[NSDictionary class]])
        {
            BOOL _isSiteIDChanged = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
            if(_isSiteIDChanged)
            {
                [app appDidChangeSiteID];
            }
            else 
            {
                
            NSArray *news = [jsonDict valueForKey:JSON_DETAILS_KEY];
            NSArray *updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY]; //[[NSArray alloc]init];
            NSArray *deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
           // NSArray *deletedIDs=nil;
                
            news = NULL_NIL(news);
            updatedIDs = NULL_NIL(updatedIDs);
            deletedIDs = NULL_NIL(deletedIDs);
        
            
            NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
            [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_NEWS];
            
            [CoreDataHandler populateNewsPerformingDelete:deletedIDs?deletedIDs:[NSArray array] update:updatedIDs?updatedIDs:[NSArray array] insertOnArray:news completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
             {
                 
                // [deletedIDs release];
                // [updatedIDs release];
                 
                 if([deletedResult count])
                 [_newsDataSource removeObjectsInArray:deletedResult];
                 
                 
                 for(News *news in updatedResult)
                 {
                     if(![news.isRead boolValue]) continue;
                     news.isRead = [NSNumber numberWithBool:NO]; 
                 }

                 
                 if([insertedResult count])
                     [_newsDataSource addObjectsFromArray:insertedResult];

                 
                 [self sortNews];
                 [self.tableView reloadData];
                 
                  //[ProgressHUD hideHUDForView:self.view animated:YES];
             }];
            }
        }
    }];
    [request setFailedBlock:^{
        
        [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
        isFetchingFromServer = NO;
        //[ConnectionManager handleError:request.error];
        //[ProgressHUD hideHUDForView:self.view animated:YES]; 
    }];
    */
}
- (void)requestFinished:(ASIHTTPRequest *)request
{
    isFetchingFromServer = NO;
    //[self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
    
    // NSString *jsonString = [request responseString];
    NSLog(@"News json:%@",[request responseString]);
    
    NSDictionary *jsonDict = [request responseJSON];

    if(jsonDict && [jsonDict isKindOfClass:[NSDictionary class]])
    {
        /*
        BOOL _isSiteIDChanged  = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
        
        BOOL _isRepDeactivated =[[jsonDict valueForKey:JSON_REP_DEACTIVATE_KEY] boolValue];
        
        NSNumber *isUnderMaintenance  = [jsonDict objectForKey:JSON_UNDER_MAINTENANCE_KEY];
        
        isUnderMaintenance = NULL_NIL(isUnderMaintenance);
        
        if(_isSiteIDChanged)
        {
            [app appDidChangeSiteID];
        }
        else if(_isRepDeactivated)
        {
            [app repDidDeactivate];
        }
        else if([isUnderMaintenance boolValue])
        {
            [app showUnderMaintainenceScreen:YES];
        }
        else
        */
        
        BOOL _isValidResponse = [ConnectionManager isValidResponse:jsonDict];
        if(_isValidResponse)
        {
            NSArray *news = [jsonDict valueForKey:JSON_DETAILS_KEY];
           // NSArray *updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
            NSArray *deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
            NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
            
            
            news = NULL_NIL(news);
            deletedIDs = NULL_NIL(deletedIDs);
            lastRequested = NULL_NIL(lastRequested);
            
            
         
            if(!([deletedIDs count] || [news count]))
            {
                [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_NEWS];
                [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
                [self.tableView reloadData];
            }

       
            
            [News populateAsyncPerformingDelete:deletedIDs?deletedIDs:[NSArray array] update:nil insertOnArray:news onAttribute:@"newsID" completion:^( NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
             {
                 [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_NEWS];
                 
                 for(News *news in updatedResult)
                 {
                     if(![news.isRead boolValue]) continue;
                     news.isRead = [NSNumber numberWithBool:NO];
                 }
                 
                   [[SSCoreDataManager sharedManager] save:nil];
                 
                 
                 if([insertedResult count])
                     [_newsDataSource addObjectsFromArray:insertedResult];
                 
                 [self sortNews];
                 
                 [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
                 [self.tableView reloadData];
                 
                 //[ProgressHUD hideHUDForView:self.view animated:YES];

             }
             
            deletionBlock:^(NSMutableArray *deletedResult)
             {
                 if([deletedResult count])
                     [_newsDataSource removeObjectsInArray:deletedResult];
                 
             }];
        
        }
    }
    else
    {
        [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
    isFetchingFromServer = NO;
    
    //[ConnectionManager handleError:request.error];
    //[ProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)hideProgressLoader
{
    [ProgressHUD hideHUDForView:self.view animated:YES];
    
   
}

-(void)fetchNews
{

[CoreDataHandler asyncFetchNewsOnCompletion:^(NSArray *result, NSError *error)
 {
     if(!_newsDataSource.count)
     {
       [_newsDataSource setArray:result];
         
      // [self sortNews];
       [self.tableView reloadData];
     }
     
     if([result count])
      [self performSelector:@selector(hideProgressLoader) withObject:nil afterDelay:0.5];
    
 }];
    
     
}

-(void)sortNews
{
    if(![_newsDataSource count]) return;
    
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"createdDate" ascending:NO];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"newsTitle" ascending:YES];
    
  //  NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate" ascending:NO];
    
    [_newsDataSource sortUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor1,sortDescriptor2, nil]];
    
    [sortDescriptor1 release];
    [sortDescriptor2 release];
    
    //======================= SHOW LATEST 50 NEWS =======================
    
    if([_newsDataSource count]>MAX_NEWS_COUNT)
    {
       // [_newsDataSource subarrayWithRange:NSMakeRange(MAX_NEWS_COUNT, _newsDataSource.count-MAX_NEWS_COUNT)];
        
      [_newsDataSource removeObjectsInRange:NSMakeRange(MAX_NEWS_COUNT, _newsDataSource.count-MAX_NEWS_COUNT)];
    }

    //=====================================================================================================
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    UIImage* image = [UIImage imageNamedNoCache:TABLE_HEADERVIEW_IMAGE_FILENAME];
    UIImageView	*headerView =[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    headerView.image  = image;
    headerView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:headerView];
    [headerView release];


    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(headerView.frame), CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-image.size.height-3) style:UITableViewStylePlain]; //438
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight| UIViewAutoresizingFlexibleBottomMargin;
    _tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor   =   [UIColor clearColor];
    _tableView.rowHeight =   NEWS_AND_BULLETIN_CELL_HEIGHT;
    _tableView.bounces=NO;
    //_tableView.separatorColor = [UIColor lightGrayColor];

    [self.view addSubview:_tableView];
    
    //[self addDefaultBorder];
    [self.view addLeftBoderWithColor:[UIColor colorWithWhite:0.2 alpha:0.4] borderWidth:1.0];
    
   [ProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    [self fetchNews];
    [self fetchNewsFromServer];
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    NSError *_saveError = nil;
    [[SSCoreDataManager sharedManager] save:_saveError];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return MAX([_newsDataSource count],isFetchingFromServer?0:1);;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView=[[UIView alloc] initWithFrame:CGRectZero];
    footerView.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.7];
    
    return [footerView autorelease];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier  = @"NewsAndBulletinCell";
    static NSString *CellIdentifier2 = @"NewsAndBulletinCell2";
    
    NSUInteger count = [_newsDataSource count];
    
    if(!count)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        
        if (cell == nil)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            
            cell.textLabel.text = isFetchingFromServer? @"" : @"No News";
            // cell.textLabel.text =  @"No News";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }

        return cell;
    }
    else
    {
        
    NewsAndBulletinCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[[NewsAndBulletinCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
    }
    
    News *newsInfo=(indexPath.section < [_newsDataSource count]) ? [_newsDataSource objectAtIndex:indexPath.section] : nil;
   
    cell.newsInfo=newsInfo;
   // BOOL isRead =   [[newsInfo valueForKey:@"isRead"] boolValue];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
    }
    
    return nil;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    News *newsInfo=(indexPath.section < [_newsDataSource count]) ? [_newsDataSource objectAtIndex:indexPath.section] : nil;
    
    if(!newsInfo) return;
    [newsInfo setValue:[NSNumber numberWithBool:YES] forKey:@"isRead"];
    
    
     NewsDetailController *newsDetailController=[[NewsDetailController alloc] initWithNewsInfo:newsInfo allNews:_newsDataSource _newsIndex:indexPath.section];
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:newsDetailController];
    [app.splitViewController presentViewController:navController animated:YES completion:^{
        
        if(indexPath)
        {
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
    
    [newsDetailController release];
    [navController release];
    
    
    [[SSCoreDataManager sharedManager] save:nil];
}

@end
