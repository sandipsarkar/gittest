//
//  NewsAndBulletinCell.h
//  RepVisitationTool
//
//  Created by Somsankar Ray on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AttributedLabel.h"
#import "NSAttributedString+Attributes.h"
#import "EPCLabel.h"

#define NEWS_AND_BULLETIN_CELL_HEIGHT  155.0f //126.0f //95

@interface NewsAndBulletinCell : UITableViewCell
{
    UILabel *_titleLabel;
    EPCLabel *_newsDetailsLabel;
    AttributedLabel* _descriptionLabel;
    
    CAGradientLayer *_shadowLayer;
    float  calculatedHeight;
}

@property(nonatomic,readonly) EPCLabel *newsDetailsLabel;
@property (nonatomic,readonly) AttributedLabel* descriptionLabel;
@property (nonatomic,readonly) UILabel *titleLabel;
@property (nonatomic,retain) id newsInfo;
@end
