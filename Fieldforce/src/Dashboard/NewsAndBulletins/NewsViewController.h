//
//  NewsViewController.h
//  RepVisitationTool
//
//  Created by Somsankar Ray on 22/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray* _newsDataSource;
    
    UITableView *_tableView;
}

@property(nonatomic,retain)  UITableView *tableView;
@property (nonatomic,retain) NSMutableArray* newsDataSource;

-(void)refreshAction;

@end
