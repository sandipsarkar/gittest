//
//  NewsAndBulletinCell.m
//  RepVisitationTool
//
//  Created by Somsankar Ray on 24/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "NewsAndBulletinCell.h"



@implementation NewsAndBulletinCell

@synthesize descriptionLabel=_descriptionLabel;
@synthesize titleLabel=_titleLabel;
@synthesize newsInfo;
@synthesize newsDetailsLabel=_newsDetailsLabel;

-(void)dealloc
{
    [_titleLabel release];
    _titleLabel=nil;
    
    [_descriptionLabel release];
    _descriptionLabel=nil;
    
    [_newsDetailsLabel release];
    _newsDetailsLabel=nil;
    
    [newsInfo release];
    newsInfo=nil;
    
    [super dealloc];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) 
    {
        _titleLabel=[[UILabel alloc] init];
        _titleLabel.textColor   =   LOGIN_BG_COLOR;
        _titleLabel.font=[UIFont subaruBoldFontOfSize:18.0];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.frame=CGRectMake(10, 10, 336, 75);
        _titleLabel.numberOfLines=3;
        _titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
        [self.contentView addSubview:_titleLabel];
        
        _descriptionLabel = [[AttributedLabel alloc] init];
        _descriptionLabel.backgroundColor = [UIColor clearColor];
        _descriptionLabel.numberOfLines=2;//3
        _descriptionLabel.lineBreakMode = UILineBreakModeCharacterWrap;
        [self.contentView addSubview:_descriptionLabel];
        
        _newsDetailsLabel = [[EPCLabel alloc] init];
         [_newsDetailsLabel setAlignTextOnTop:YES];
        _newsDetailsLabel.font=[UIFont subaruThinFontOfSize:13.0];
        _newsDetailsLabel.backgroundColor = [UIColor clearColor];
        _newsDetailsLabel.lineBreakMode=UILineBreakModeWordWrap;
        _newsDetailsLabel.numberOfLines=0;
        _newsDetailsLabel.frame=CGRectMake(10, 10, 100, 50);
        [self.contentView addSubview:_newsDetailsLabel];
    }
    return self;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    calculatedHeight=0.0;
  
      BOOL isRead =   [[newsInfo valueForKey:@"isRead"] boolValue];
	
    NSString *text=[newsInfo valueForKey:@"newsTitle"];
    
    CGRect _bounds=self.contentView.bounds;

    UIFont *titleLabelFont =  isRead ? [UIFont subaruBookFontOfSize:18.0] : [UIFont subaruBoldFontOfSize:18.0];
    
    CGSize expectedLabelSize;
    
   // expectedLabelSize=[text sizeWithFont:titleLabelFont constrainedToSize:CGSizeMake(_bounds.size.width-15.0-titleLabelFont.leading, CGFLOAT_MAX) lineBreakMode:_titleLabel.lineBreakMode]; //75
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        /*
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = _titleLabel.lineBreakMode;;
        paragraphStyle.alignment = NSTextAlignmentLeft;
        
        NSDictionary * stringAttributes = @{NSFontAttributeName :titleLabelFont,
                                      NSParagraphStyleAttributeName : paragraphStyle};
        
        
        expectedLabelSize = [text boundingRectWithSize:CGSizeMake(_bounds.size.width-15.0, CGFLOAT_MAX)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                                      attributes:stringAttributes context:nil].size;
        
        [paragraphStyle release];
        
       // int numberOfLine = ceil((expectedLabelSize.width) / _titleLabel.frame.size.width);
         */
        
       expectedLabelSize = [_titleLabel sizeThatFits:CGSizeMake(_bounds.size.width-15.0, CGFLOAT_MAX)];
        
    }
    else
    {
     expectedLabelSize = [text sizeWithFont:titleLabelFont
                                constrainedToSize:CGSizeMake(_bounds.size.width-15.0, CGFLOAT_MAX)
                                    lineBreakMode:_titleLabel.lineBreakMode]; //75
    }
    
    NSLog(@" hh %f", expectedLabelSize.height);
    
  
    CGRect _titleLabelFrame    =  CGRectMake(10, 5, _bounds.size.width-15, ceilf(expectedLabelSize.height) );
    
    CGRect _descriptionLabelFrame = CGRectMake(10, CGRectGetMaxY(_titleLabelFrame), _bounds.size.width-15, 38);

   
    
    if(!CGRectEqualToRect(_titleLabelFrame, _titleLabel.frame))
        _titleLabel.frame=_titleLabelFrame;
  
    
    if(!CGRectEqualToRect(_descriptionLabelFrame, _descriptionLabel.frame))
        _descriptionLabel.frame=_descriptionLabelFrame;
    
    
    calculatedHeight=10+expectedLabelSize.height+10+_descriptionLabel.frame.size.height+10;
    //calculatedHeight=126.0-calculatedHeight;
    calculatedHeight=CGRectGetHeight(self.contentView.bounds)-calculatedHeight;
    NSLog(@" calculatedHeight %f",calculatedHeight);
    NSString *textInfo=[newsInfo valueForKey:@"newsDescription"];
    
    CGSize expectedLabelSize1 = [textInfo sizeWithFont:_newsDetailsLabel.font
                                    constrainedToSize:CGSizeMake(_bounds.size.width-15, 50)
                                    lineBreakMode:UILineBreakModeWordWrap];
    
    // expectedLabelSize1 = [_newsDetailsLabel sizeThatFits:CGSizeMake(_bounds.size.width-15, 50)];
    
    NSLog(@" hh %f", expectedLabelSize1.height);
    
    CGFloat _newsDetailHeight= _bounds.size.height-CGRectGetMaxY(_descriptionLabelFrame);
    
   _newsDetailsLabel.frame=CGRectMake(10, CGRectGetMaxY(_descriptionLabel.frame), _bounds.size.width-15, MIN(expectedLabelSize1.height,_newsDetailHeight));
    
    
}


- (CAGradientLayer *)shadowLayer
{
    CAGradientLayer *newShadow = [[[CAGradientLayer alloc] init] autorelease];
    CGRect newShadowFrame =
    CGRectMake(0, 0, self.frame.size.width,20.0);
    newShadow.frame = newShadowFrame;
    
    CGColorRef darkColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha: 0.1].CGColor;
    CGColorRef lightColor =[self.backgroundColor colorWithAlphaComponent:0.0].CGColor;
    
    newShadow.colors =[NSArray arrayWithObjects:(UIColor *)darkColor,(UIColor *)lightColor,nil];
    return newShadow;
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    
    NSLog(@"Selected:%@",selected?@"YES":@"NO");
    
}

-(void)selectCell:(BOOL)bSelected
{
    //  self.contentView.backgroundColor = bSelected ? [[UIColor lightGrayColor] colorWithAlphaComponent:0.5] : [UIColor clearColor] ;
    
    _titleLabel.textColor   = bSelected ? [UIColor blackColor] :  LOGIN_BG_COLOR;
    _titleLabel.font        = bSelected ? [UIFont subaruBookFontOfSize:18.0] : [UIFont subaruBoldFontOfSize:18.0];
    
    if(bSelected)
    {
        // _shadowLayer = [self shadowLayer];
        UIImageView *bgView=(UIImageView *) self.backgroundView;
        if(!bgView)
        {
            bgView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
            bgView.image = [UIImage imageNamedNoCache:@"article_row_selected.png"];
            //bgView.contentMode = UIViewContentModeScaleAspectFit;
            self.backgroundView = bgView;
        }
        bgView.hidden=NO;
        
        //[bgView.layer addSublayer:_shadowLayer];
        
        /*
         [bgView.layer setMasksToBounds:YES ];
         [bgView.layer setShadowColor:[[UIColor blackColor ] CGColor ] ];
         [bgView.layer setShadowOpacity:0.65 ];
         [bgView.layer setShadowRadius:6.0 ];
         [bgView.layer setShadowOffset:CGSizeMake( -2 , -2 ) ];
         [bgView.layer setShouldRasterize:YES ];
         [bgView.layer setShadowPath:[[UIBezierPath bezierPathWithRect:bgView.layer.bounds ] CGPath ] ];
         */
    }
    else 
    {
        // self.backgroundView = nil;
        self.backgroundView.hidden=YES;
    }
    
}

-(void)setNewsInfo:(id)dict
{
   // if(newsInfo!=dict)
    {
        [newsInfo release];
        newsInfo = [dict retain];
    
        NSString *titleString = [newsInfo valueForKey:@"newsTitle"];
        
        _titleLabel.text=[titleString uppercaseString];
         
        BOOL isRead =   [[newsInfo valueForKey:@"isRead"] boolValue];
        

        
    NSNumber *dateVal       = [newsInfo valueForKey:@"createdDate"];

    
    NSString *dateString = [NSDate dateStringFromWCFTimeInterval:dateVal.doubleValue withFormat:NEWS_DATE_FORMAT];
        
    NSString *detailString = [newsInfo valueForKey:@"newsDescription"];
    NSString *creatorNameString = [newsInfo valueForKey:@"creator"];

    //NSString *descString = [NSString stringWithFormat:@"%@ | %@",dateString,creatorNameString];
     NSString *descString = [NSString stringWithFormat:@"%@  By: %@",dateString,creatorNameString];
        
    NSMutableAttributedString *str=[NSMutableAttributedString attributedStringWithString:descString];
  //  NSRange _range=[descString rangeOfString:@"|"];
    NSRange _range=[descString rangeOfString:@"By:"];
    _range=NSMakeRange(0, _range.location);
    
    [str setFont:[UIFont grotesqueBoldFontOfSize:15] range:_range];//[UIFont systemFontOfSize:18]
    [str setFont:[UIFont grotesqueBoldFontOfSize:15] range:NSMakeRange(_range.location+1, descString.length-_range.location-1)];
        //UIFont systemFontOfSize:16]
    [str setTextColor:isRead?[UIColor blackColor] : LOGIN_BG_COLOR range:_range];
    _descriptionLabel.text=str;
   
    _newsDetailsLabel.text=detailString;
  
    [self selectCell:isRead];
}
    
}
@end
