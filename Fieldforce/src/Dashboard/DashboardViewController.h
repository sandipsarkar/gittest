//
//  DashboardViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 17/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NewsViewController;
@class UpcomingTasksAndVisitsViewController;



@interface DashboardViewController : UIViewController<UISplitViewControllerDelegate>
{
    NewsViewController *newsViewController;
    UpcomingTasksAndVisitsViewController *upcomingTasksAndVisitsViewController;
}

@end
