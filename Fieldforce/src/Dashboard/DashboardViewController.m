//
//  DashboardViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 17/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "DashboardViewController.h"
#import "NewsViewController.h"
#import "UpcomingTasksAndVisitsViewController.h"
#import "Rep.h"
#import "Customer.h"
#import "Territory.h"
#import "OfflineManager.h"

NSString * const DashBoardWillRefresh = @"_DashBoardWillRefresh_";

@interface DashboardViewController()

@property (nonatomic,retain) ASIHTTPRequest *fetchCustomerRequest;
@end

@implementation DashboardViewController
@synthesize fetchCustomerRequest;

- (void)dealloc 
{
    
    [fetchCustomerRequest clearDelegatesAndCancel];
    [fetchCustomerRequest release];
    fetchCustomerRequest = nil;
    
    [newsViewController release] ;
    [upcomingTasksAndVisitsViewController release];
    
    newsViewController = nil;
    upcomingTasksAndVisitsViewController = nil;

    
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {
        newsViewController = [[NewsViewController alloc] init]; 
        upcomingTasksAndVisitsViewController    = [[UpcomingTasksAndVisitsViewController alloc] init];  
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle



- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController.navigationBar addBackGroundImage:@"header_blank.png"];
    [self addTitleImage:@"dashboard.png"];
    [self addRefreshButtonWithSelector:@selector(refreshButtonAction:)];
    
    CGRect bounds=self.view.bounds;
    CGRect _containerLeftHalf=CGRectMake(0, 0, CGRectGetWidth(bounds)/2.0, CGRectGetHeight(bounds));
    CGRect _containerRightHalf=CGRectMake(CGRectGetWidth(_containerLeftHalf), 0, CGRectGetWidth(bounds)/2.0, CGRectGetHeight(bounds));
    
    newsViewController.view.frame   =   _containerLeftHalf;
    upcomingTasksAndVisitsViewController.view.frame =   _containerRightHalf;

    [newsViewController.view setAutoresizingMasks];
    [upcomingTasksAndVisitsViewController.view setAutoresizingMasks];
    
    [self addChildViewController:newsViewController];
    [self addChildViewController:upcomingTasksAndVisitsViewController];
    
    [self.view addSubview:newsViewController.view];    
    [self.view addSubview:upcomingTasksAndVisitsViewController.view];
    
    [newsViewController didMoveToParentViewController:self];
    [upcomingTasksAndVisitsViewController didMoveToParentViewController:self];
    

    if(!app.isCustomerLoaded)
    {
        [self fetchCustomersFromServer];
    }
    else if(!app.isCustomerFetching)
    {
        [upcomingTasksAndVisitsViewController fetchFromServer];
    }

   // [[OfflineManager sharedManager] performOnlineJob];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


-(void)refreshButtonAction:(id)sender
{
    
    /*
    NSNotification *myNotification = [NSNotification notificationWithName:DashBoardWillRefresh object:nil];
    [[NSNotificationQueue defaultQueue] enqueueNotification:myNotification postingStyle:NSPostNow coalesceMask:NSNotificationCoalescingOnName forModes:nil];
     */
    
    [upcomingTasksAndVisitsViewController refresh];
    [newsViewController refreshAction];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
   // [app performSelector:@selector(fixRoundedSplitViewCorner) withObject:NULL afterDelay:0];
}


#pragma mark - FETCH CUSTOMERS
-(void)_fetchCustomersFromServer
{
    if(app.isCustomerFetching) return;
    app.isCustomerFetching = YES;
    

     Rep *currentRep=[CoreDataHandler currentRep];
     
     NSNumber *wdSiteID= currentRep.parentWdSite.wdSiteID;
     //[NSNumber numberWithInteger:18];//
     
     NSArray *activeTerritories = [Territory fetchAll:nil];//currentRep.territories.allObjects;
     NSArray *territories = [activeTerritories valueForKey:@"territoryID"];
     NSString *territoryIDS = [territories componentsJoinedByString:@","];
     NSLog(@"Territories=%@",territoryIDS);
    
    NSMutableArray *allCustomers = [Customer fetchWithPredicate:nil onAttributes:[NSArray arrayWithObject:@"customerID"] sortDescriptors:nil limit:0 error:nil];
    NSArray *  _allCustomersIDs = [allCustomers count]? [allCustomers valueForKey:@"customerID"] : nil;
    
    NSString *customerIDString = [_allCustomersIDs count] ? [_allCustomersIDs componentsJoinedByString:@","] : @"";
    
    //customerIDString = @"";
     NSLog(@"CustomerIDs=%@",customerIDString);
    
   //__block DashboardViewController *weakSelf = self;
    
   __block ASIHTTPRequest *request = [ConnectionManager createCustomersConnectionWithRepID:currentRep.repID wdSiteID:wdSiteID territoryIDs:territoryIDS customerIDs:customerIDString];
    self.fetchCustomerRequest = request;

     [request setCompletionBlock:^{
         
        // DashboardViewController *strongSelf = weakSelf;
         
       //  app.isCustomerFetching =NO;
         NSLog(@"Response:%@",[request responseString]);
         
         NSDictionary *jsonDict = [request responseJSON];
         
         jsonDict = NULL_NIL(jsonDict);

         
         BOOL _isValidResponse = [ConnectionManager isValidResponse:jsonDict];
         if(_isValidResponse)
         {
                 app.isCustomerLoaded=YES;
            
                 NSArray *_customers = [jsonDict valueForKey:JSON_DETAILS_KEY];
                 NSArray *updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
                 NSArray *deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
                 
                 NSNumber *_status = [jsonDict valueForKey:JSON_STATUS_KEY];
              
                 int status=1;

                 status = [_status intValue];

                 NSLog(@"status=%d",status);
                 
                 if(status==1)
                 {
                     NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
                     [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_CUSTOMERS];
                     
                     //======================== FETCH ACCORDING TO TERRITORY ========================
                     NSArray *currentTerritories = [jsonDict valueForKey:@"repTerritoryDetails"];
                     currentTerritories = NULL_NIL(currentTerritories);
                     
                     NSMutableArray *_currentTerritories = [NSMutableArray array];
                     for(NSDictionary *territoryDict in currentTerritories)
                     {
                         Territory *aTerritory = (Territory *) [Territory createObjectFromDictionary:territoryDict inContext:DEFAULT_CONTEXT];
                         
                         [_currentTerritories addObject:aTerritory];
                         
                     }
                     
                     
                     NSMutableArray *territoryToDelete = [NSMutableArray array];
                     NSArray *delete= [_currentTerritories count] ?  [activeTerritories filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (self.territoryID IN %@)",[_currentTerritories valueForKey:@"territoryID"]]] : nil;
                     
                     if([delete count])
                         [territoryToDelete addObjectsFromArray:[delete valueForKey:@"territoryID"]];
                     
                     //============== REMOVE PREVIOUS TERROTORIES AND ADD CURRENT TERRITORIES ===========
                     
                     if([_currentTerritories count])
                     {
                         for(Territory *territory in activeTerritories)
                         {
                             [currentRep removeTerritoriesObject:territory];
                             
                             [SSCoreDataManager deleteObject:territory];
                         }
                         
                         NSSet *territorySet = [NSSet setWithArray:_currentTerritories];
                         if(territories)
                             [currentRep addTerritories:territorySet];
                     }
                     
                     
                     //============ DELETE CUSTOMER NOT FALLS INTO CURRENT TERRITORY =============== //
                     
                     if([territoryToDelete count])
                     {
                         NSPredicate *deleteCustomerPredicate = [NSPredicate predicateWithFormat:@"self.territoryID IN %@",territoryToDelete];
                         
                         NSMutableArray *customersToDelete = [Customer fetchWithPredicate:deleteCustomerPredicate onAttributes:[NSArray arrayWithObject:@"customerID"] sortDescriptors:nil limit:0 error:nil];
                         
                         // NSMutableArray *customersToDelete = [Customer fetchWithPredicate:deleteCustomerPredicate error:nil];
                         
                         NSArray *  _deletedIDs = [customersToDelete valueForKey:@"customerID"];
                         
                         NSMutableArray *idsToDelete = deletedIDs ? [NSMutableArray arrayWithArray:deletedIDs] : [NSMutableArray array];
                         [idsToDelete addObjectsFromArray:_deletedIDs];
                         
                         deletedIDs =[NSArray arrayWithArray:idsToDelete];
                         
              
                         
                        app.isCustomerFetching =NO;
                     }
                 }
                 else if(status==2)
                 {
//#ifndef LIVE
                     
                     _customers = nil;
                     updatedIDs = nil;
                     
                     NSMutableArray *customersToDelete = [Customer fetchWithPredicate:nil onAttributes:[NSArray arrayWithObject:@"customerID"] sortDescriptors:nil limit:0 error:nil];
                     
                     deletedIDs = [customersToDelete valueForKey:@"customerID"];
                     
//#endif
                       app.isCustomerFetching =NO;
                 }
                  
                 
                 if(status!=0)
                 {
                     if([_customers count] || [deletedIDs count])
                     {
                     
                         
                         [Customer populateAsyncPerformingDelete:deletedIDs?deletedIDs:[NSArray array] update:updatedIDs?updatedIDs:[NSArray array] insertOnArray:_customers?_customers:[NSArray array] onAttributes:[NSArray arrayWithObjects:@"customerID",@"oldCustomerID", nil] completion:^( NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
                          {
                              
                              app.isCustomerFetching =NO;
                              
                              NSNumber *success = [NSNumber numberWithBool:YES];
                              [[NSNotificationCenter defaultCenter] postNotificationName:CustomersDidFinishLoad object:success];
                              
                              [self->upcomingTasksAndVisitsViewController fetchFromServer];
                              
                              
                          }
                          
                          deletionBlock:^(NSMutableArray *deletedResult)
                          {
                              
                              
                          }];
                                          
                    }
                     else
                     {
                         app.isCustomerFetching =NO;
                     }
                 }
                 
                 }
         else
         {
             app.isCustomerFetching =NO;
         }
         }];
     
     [request setFailedBlock:^{
         
         app.isCustomerFetching =NO;
         
         NSNumber *success = [NSNumber numberWithBool:NO];
         [[NSNotificationCenter defaultCenter] postNotificationName:CustomersDidFinishLoad object:success];
         
         //[ProgressHUD hideAnimated:YES];
         }];
    
}


-(void)fetchCustomersFromServer
{
    if(app.isCustomerFetching) return;
     app.isCustomerFetching = YES;

    //__block DashboardViewController *weakSelf = self;
    
    [ConnectionManager fetchAndProcessCustomersOnCompletion:^(BOOL success, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error) {
       
      // DashboardViewController *strongSelf = weakSelf;
       
        app.isCustomerFetching =NO;
        
        if(success)
        {
            NSNumber *_success = [NSNumber numberWithBool:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:CustomersDidFinishLoad object:_success];
            
            [self->upcomingTasksAndVisitsViewController fetchFromServer];
        }
        else
        {
            NSNumber *success = [NSNumber numberWithBool:NO];
            [[NSNotificationCenter defaultCenter] postNotificationName:CustomersDidFinishLoad object:success];

        }
        
     } deletionBlock:^(NSMutableArray *deletedResult) {
        
         
    }];
}
#pragma mark - Creating the News Bulletin view and the Upcoming Visits view


#pragma -

@end
