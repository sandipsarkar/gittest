//
//  CustomSegmentedControl.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 18/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "CustomSegmentedControl.h"
@interface CustomSegmentedControl()

@property(nonatomic,assign) UIButton *lastSelectedItem;
@property(nonatomic,copy) DidSelectSegmentedControl didSelectSegmentedControl;

-(void)didSelectAction:(UIButton *)sender;
@end

@implementation CustomSegmentedControl
@synthesize items;
@synthesize lastSelectedItem;
@synthesize didSelectSegmentedControl;
@synthesize selectedSegmentIndex;

- (id)initWithFrame:(CGRect)frame items:(NSArray *)aItems
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        selectedSegmentIndex=NSNotFound;
        self.items = aItems;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    NSUInteger numItems = self.items.count;
    CGFloat itemWidth = CGRectGetWidth(rect)/numItems;
    
    int i=0;
    for(UIButton *button in self.items)
    {
        button.frame = CGRectMake(itemWidth*i, 0, itemWidth, CGRectGetHeight(rect));
        button.tag = i+1;
        button.exclusiveTouch = YES;
        [button setBackgroundImage:[button backgroundImageForState:UIControlStateNormal] forState:UIControlStateHighlighted];
        [self addSubview:button];
        [button addTarget:self action:@selector(didSelectAction:) forControlEvents:UIControlEventTouchUpInside];
        
        if(i==selectedSegmentIndex)
        {
            [self didSelectAction:button];
        }
        
        i++;
    }
}

-(void)setSelectedSegmentIndex:(int)aSegmentIndex
{
    if(selectedSegmentIndex!=aSegmentIndex)
    {
        /*
        if(aSegmentIndex==NSNotFound)
        {
            self.lastSelectedItem.selected = NO;
            self.lastSelectedItem.userInteractionEnabled = YES;
            self.lastSelectedItem = nil;
        }
        else
         */
        {
          selectedSegmentIndex  = aSegmentIndex;
          UIButton *segmentItem =(UIButton *) [self viewWithTag:aSegmentIndex+1];
          [self didSelectAction:segmentItem];
        }
    }
}

-(void)addDidSelectSegmentedControlCallback:(DidSelectSegmentedControl)callback
{
    if(callback)
        self.didSelectSegmentedControl = callback;
}
-(void)didSelectAction:(UIButton *)sender
{
    if(!sender) return;
    NSInteger _selectedIndex = sender.tag-1;
    
    sender.selected = YES;
    sender.userInteractionEnabled = NO;
    
    if(self.lastSelectedItem)
    {
        self.lastSelectedItem.selected = NO;
        self.lastSelectedItem.userInteractionEnabled = YES;
    }
    
    self.lastSelectedItem = sender;
    
    selectedSegmentIndex  = _selectedIndex;
    
    if(_selectedIndex >=0 && _selectedIndex!=NSNotFound)
    {
        self.didSelectSegmentedControl(self,_selectedIndex);
    }
}

-(void)dealloc
{
    [items release] ;
    items=nil;
    lastSelectedItem = nil;
    
    if(didSelectSegmentedControl) [didSelectSegmentedControl release];
    
    [super dealloc];
}

@end
