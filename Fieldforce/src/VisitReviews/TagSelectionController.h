//
//  TagSelectionController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^TagSelectedCallback)(NSString *tag);
typedef void(^WillDismissCallback)(void);

@protocol TagSelectionDelegate;
@interface TagSelectionController : UIViewController
{
    UIPickerView *pickerView;
    NSString  *strOems;
    NSMutableArray *arrOemName;
}

@property(nonatomic,retain)  NSString  *strOems;
@property(nonatomic,retain) NSMutableArray *tags;
@property(nonatomic,assign) id <TagSelectionDelegate> delegate;

+(NSArray *)dataSource;

-(void)addDidSelectCallback:(TagSelectedCallback)aCallback;
-(void)addDismissCallback:(WillDismissCallback)aCallback;

@end

@protocol TagSelectionDelegate <NSObject>

@optional
-(void)tagSelectionController:(TagSelectionController *)tagSelectionController didSelectTag:(NSString *)tag;

-(void)tagSelectionControllerWillDissmiss:(TagSelectionController *)tagSelectionController;
@end
