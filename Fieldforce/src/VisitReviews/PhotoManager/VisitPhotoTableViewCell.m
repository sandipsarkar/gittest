//
//  VisitPhotoTableViewCell.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 26/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "VisitPhotoTableViewCell.h"
#import "VisitPhoto.h"
#import "UIImage+Resize.h"

@implementation VisitPhotoTableViewCell
@synthesize photoImageView;
@synthesize titleLabel;
@synthesize captionLabel;
@synthesize emailButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) 
    {
        photoImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        photoImageView.backgroundColor = LOGIN_BG_COLOR;
        //[UIColor colorWithRed:238.0f/255.0f green:58.0f/255.0f blue:67.0f/255.0f alpha:1.0];
        photoImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:photoImageView];
        
        titleLabel = [[UILabel alloc] init];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont grotesqueBoldFontOfSize:14.0];
        [self.contentView addSubview:titleLabel];
        
        captionLabel = [[UILabel alloc] init];
        captionLabel.textColor = [UIColor blackColor];
        captionLabel.backgroundColor = [UIColor clearColor];
        captionLabel.numberOfLines   = 4;
        captionLabel.font = [UIFont grotesqueFontOfSize:13.0];
        [self.contentView addSubview:captionLabel];
        
        
        emailButton=[UIButton repToolButtonWithImageName:@"email_button.png" text:nil];
        self.accessoryView = emailButton;
        
        
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect _bounds = self.contentView.bounds;
    
    CGFloat _locX = 0.0f;
    CGFloat _locY = 0;
    CGRect photoFrame = CGRectMake(_locX, _locY, 100.0, CGRectGetHeight(_bounds));
    
    _locX = CGRectGetMaxX(photoFrame)+10.0f;
    _locY = 5.0f;
    CGRect titleFrame = CGRectMake(_locX, _locY, CGRectGetWidth(_bounds)-_locX-10.0,18.0);
    
    _locY = CGRectGetMaxY(titleFrame) + 5.0;
    CGRect captionFrame = CGRectMake(_locX, _locY, CGRectGetWidth(_bounds)-_locX-10.0, CGRectGetHeight(_bounds)-_locY-5.0);
    
    photoImageView.frame = photoFrame;
    titleLabel.frame     = titleFrame;
    captionLabel.frame   = captionFrame;
}

-(void)setVisitPhotoInfo:(VisitPhoto *)visitPhoto
{
    NSString *thumbImagePath = visitPhoto.thumbImagePath;
    
    if(thumbImagePath.length)
    {
        NSData *imgData = [[NSData alloc] initWithContentsOfFile:thumbImagePath options:NSDataReadingMappedIfSafe error:nil];
        UIImage *_image  = [[UIImage alloc] initWithData:imgData];
       //  UIImage *_image  = [[UIImage alloc] initWithContentsOfFile:thumbImagePath];
        photoImageView.image = _image;
        [_image release];
    }

    
    titleLabel.text  = visitPhoto.oemName.length ? visitPhoto.oemName : @"No OEM Selected";
    captionLabel.text = visitPhoto.caption;
    
     [self performBlock:^{
         
         [photoImageView setNeedsDisplay];
     } afterDealy:0.3];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    titleLabel.textColor = selected ? [UIColor whiteColor] : [UIColor blackColor];
    captionLabel.textColor = titleLabel.textColor ;
    // Configure the view for the selected state
}

-(void)dealloc
{
    [photoImageView release];
    [titleLabel release];
    [captionLabel release];
    
    [super dealloc];
}

@end
