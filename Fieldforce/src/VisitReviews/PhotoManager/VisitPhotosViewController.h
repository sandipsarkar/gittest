//
//  VisitPhotosViewController.h
//  TestSoundNote
//
//  Created by RANDEM MAC on 21/06/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddPhotoCaptionViewController.h"

@interface VisitPhotosViewController : UIViewController
{
    UITableView *_tableView;
    UIButton *takePhotoButton;
}

@property(nonatomic,retain)NSMutableArray *photos;
@property (nonatomic,assign) Visit *visit;
@property(nonatomic,retain,readonly) UITableView *tableView;

- (id)initWithVisit:(Visit *)aVisit;
- (id)initWithPhotos:(NSMutableArray *)_photos;
-(void)reloadData;

-(void)markAsComplete;

@end
