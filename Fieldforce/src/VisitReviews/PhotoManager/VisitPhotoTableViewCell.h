//
//  VisitPhotoTableViewCell.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 26/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InsetTableViewCell.h"

@class VisitPhoto;


@interface VisitPhotoTableViewCell : InsetTableViewCell
{
    
}

@property (nonatomic,readonly) UIImageView *photoImageView;
@property (nonatomic,readonly) UILabel *titleLabel;
@property (nonatomic,readonly) UILabel *captionLabel;
@property (nonatomic,readonly) UIButton *emailButton;

-(void)setVisitPhotoInfo:(VisitPhoto *)visitPhoto;

@end
