//
//  OEMListViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 16/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "OEMListViewController.h"
#import "OEM.h"
@interface OEMListViewController ()
{
    NSMutableArray *arrOemName;
    NSString* _lastSelectedOEM;
    NSUInteger lastSelectedIndex;
}
-(void)currentSelectedOEMIndex;
@property (nonatomic, copy)  NSString* lastSelectedOEM;

@property(nonatomic,copy) DidSelectOEMCallback didSelectOEMCallback;
@end

@implementation OEMListViewController
@synthesize dataSource;
@synthesize didSelectOEMCallback;
@synthesize lastSelectedOEM = _lastSelectedOEM;

-(void)dealloc
{
    [dataSource release];
    [_lastSelectedOEM release];
    
    [didSelectOEMCallback release];
    
    [super dealloc];
}

- (id)initWithOEMID:(NSUInteger) oemID andName:(NSString*) oemName;
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    if (self) 
    {
        _lastSelectedOEM = [oemName copy];
        lastSelectedIndex = NSNotFound;
    }
    return self;
}

-(void)currentSelectedOEMIndex
{
    if(![dataSource count] || !self.lastSelectedOEM.length) return;
    NSPredicate* oemPredicate = [NSPredicate predicateWithFormat:@"self.name = %@", self.lastSelectedOEM];
    NSArray* oemArray = [dataSource filteredArrayUsingPredicate:oemPredicate];
    
    if([oemArray count])
    {
        NSDictionary *oemDict = [oemArray objectAtIndex:0];
        NSUInteger index=  [dataSource indexOfObject:oemDict];
        
        if(index!=NSNotFound)
        {
            lastSelectedIndex = index;
        }
    }
}

-(void)addDidSelectOEMCallback:(DidSelectOEMCallback)callback
{
    self.didSelectOEMCallback = callback;
}

#pragma mark SHOULD BE OPTIMIZED
-(void)calculateOEMList
{
    if(!dataSource)
    {
        NSMutableArray *arrLoc=[[NSMutableArray alloc]init];
        self.dataSource=arrLoc;
        //[self.dataSource addObject:@"General"];
        [arrLoc release];
    }

    NSArray *arr=[app.strDealingOemForVisitPhoto componentsSeparatedByString:@","];
        
    /*
     
     NSMutableArray  *arrOEM =(NSMutableArray *) [CoreDataHandler connectedOEMS];
    arrOemName=[[NSMutableArray alloc]init];
    
    for(int i=0;i<[arrOEM count];i++)
    {
        OEM *oem =  [arrOEM objectAtIndex:i];
        NSMutableArray *arr=[[NSMutableArray alloc]init];
        [arr addObject:[NSString stringWithFormat:@"%@",oem.oemID]];
        [arr addObject:oem.name];
        [arrOemName addObject:arr];
        [arr release];
        
    }
    NSLog(@" arrOemName  %@",arrOemName);
    
    
    
    if([arr count]==1)
    {
        for(int i=0;i<[arrOemName count];i++)
        {
            if([[[arrOemName objectAtIndex:i]objectAtIndex:0] isEqualToString:[arr objectAtIndex:0]])
            {
                NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                [dict setValue:[[arrOemName objectAtIndex:i]objectAtIndex:0] forKey:@"oemID"];
                [dict setValue:[[arrOemName objectAtIndex:i]objectAtIndex:1] forKey:@"name"];
                [self.dataSource addObject:dict];
                [dict release];
            }
            else{
            }
        }
    }
    else
    {
        for(int i=0;i<[arrOemName count];i++)
        {
            for(int j=0;j<[arr count];j++)
            {
                if([[[arrOemName objectAtIndex:i]objectAtIndex:0] isEqualToString:[arr objectAtIndex:j]])
                {
                    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                    [dict setValue:[[arrOemName objectAtIndex:i]objectAtIndex:0] forKey:@"oemID"];
                    [dict setValue:[[arrOemName objectAtIndex:i]objectAtIndex:1] forKey:@"name"];
                    [self.dataSource addObject:dict];
                    [dict release];
                }
                else{
                }
            }
        }
    }
    */
    
    NSArray *dealingOEMS = [CoreDataHandler intersectWithDealingOEMs:arr];
    [dataSource setArray:dealingOEMS];
    
}
#pragma mark -
-(void)sortOEMList
{
    if([self.dataSource count])
    {
        //NSSortDescriptor *_descriptor = [[NSSortDescriptor alloc] initWithKey:@"oemname" ascending:YES];
        
        NSSortDescriptor *_descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        
        [self.dataSource sortUsingDescriptors:[NSArray arrayWithObject:_descriptor]];
        [_descriptor release];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.tableView.showsVerticalScrollIndicator = NO;
    NSLog(@" fff %@", app.strDealingOemForVisitPhoto);
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 10.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        self.tableView.tableHeaderView = _headerView;
        [_headerView release];
    }
    
    [self calculateOEMList];
    [self sortOEMList];
    
    [self currentSelectedOEMIndex];


    
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.contentSizeForViewInPopover = CGSizeMake(320.0, MAX(100.0,self.tableView.contentSize.height)-2.0);
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.contentSizeForViewInPopover = CGSizeMake(320.0, MAX(100.0,self.tableView.contentSize.height));
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(!cell)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
   
    
    //OEM *oem = indexPath.row < [self.dataSource count] ? [self.dataSource objectAtIndex:indexPath.row] : nil;
    
    OEM *oem = indexPath.row <[self.dataSource count] ?  [self.dataSource objectAtIndex:indexPath.row] : nil;
    
    cell.textLabel.text = [oem valueForKey:@"name"];
    
    cell.accessoryType =  (lastSelectedIndex== indexPath.row)? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];

    
    CellBackgroundView *_selectedBgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _selectedBgView.backgroundColor = [UIColor clearColor];
    _selectedBgView.contentInset = CGPointMake(10.0, 0.0);
    _selectedBgView.cornerRadius = 8.0;
    _selectedBgView.borderColor = self.tableView.separatorColor;
    _selectedBgView.fillColor = [UIColor cellSelectedColorBlue];

    
    
    NSInteger numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    _selectedBgView.position    = _bgView.position;
    cell.backgroundView         = _bgView;
    cell.selectedBackgroundView = _selectedBgView;
    
    cell.textLabel.text = [NSString stringWithFormat:@"  %@",cell.textLabel.text];
    
    [_bgView release];
    [_selectedBgView release];
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    OEM *oem = indexPath.row < [dataSource count] ? [dataSource objectAtIndex:indexPath.row] : nil;
    
    if(!oem) return;
    
    NSString *strOemName= [oem valueForKey:@"name"];
    NSString *strID = [oem valueForKey:@"oemID"];
    
    if (lastSelectedIndex == indexPath.row)
    {
        strOemName  = nil;
        strID       = nil;
    }
    
    lastSelectedIndex = indexPath.row;
    
    
    if(self.didSelectOEMCallback) self.didSelectOEMCallback(strID,strOemName);
    
}


- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
   UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor whiteColor];
}
- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    [self performBlock:^{
        cell.textLabel.textColor = [UIColor blackColor];
    } afterDealy:0.3];
    
}


@end
