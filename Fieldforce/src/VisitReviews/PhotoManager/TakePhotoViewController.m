//
//  TakePhotoViewController.m
//  TestSoundNote
//
//  Created by RANDEM MAC on 21/06/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import "TakePhotoViewController.h"
#import "AddPhotoCaptionViewController.h"
#import "CaptureSessionManager.h"
#import "UIImage+UIImage_Orientation.h"
#import "VisitPhoto.h"
#import "Visit.h"
#import "NonRotatingUIImagePickerController.h"
#import "UIImage+Resize.h"

@interface TakePhotoViewController()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate>
{
    CaptureSessionManager  *_captureManager;
    UIPopoverController *imagePickerPopover;
    UIPopoverController *viewPhotosPopover;
    
    CGSize _originalLandScapeSize;
    
    UIImagePickerController*  imagePicker;
   /// NonRotatingUIImagePickerController*  imagePicker;
}

-(void)selectImage:(UIImage *)image;
-(void)cameraDidCaptureImage:(UIImage *)image;


@property(nonatomic,copy)BackActionCallback backActionCallback;
@property(nonatomic,copy)ApproveActionCallback approveActionCallback;

@end


@implementation TakePhotoViewController
@synthesize photos;
@synthesize backActionCallback;
@synthesize approveActionCallback;
@synthesize visit;
@synthesize allowCaptionEntry;
@synthesize delegate;

- (id)init
{
    self = [super init];
    if (self) {
    
        allowCaptionEntry = YES;
        _captureManager=[[CaptureSessionManager alloc]init];
        [_captureManager addVideoInputFrontCamera:NO];
        [_captureManager addStillImageOutput];
        [_captureManager addVideoPreviewLayer];
        [_captureManager setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
        
    }
    return self;
}

-(id)initWithPhotos:(NSMutableArray *)aPhotos
{
    self=[self init];
    
    if(self)
    {
        self.photos=aPhotos;
    }
    
    return self;
}

-(void)dealloc
{
    delegate = nil;
    [visit release];
    visit=nil;
    [photos release];
    photos=nil;
   // [[_captureManager captureSession] stopRunning];
    
    [_captureManager release];
    
    [imagePicker release] ;
    imagePicker=nil;
    [capturedImageView release];
    [imagePickerPopover release];
    [viewPhotosPopover release];
    [cameraView release]; 
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kImageCapturedSuccessfully object:nil];

    
    if(backActionCallback)
    [backActionCallback release];
    
     if(approveActionCallback)
    [approveActionCallback release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
        
    if(!DISABLE_LOG)
     showAlert(@"Memory warning", @"Take photo:Did receive memory warning");
    
    [super didReceiveMemoryWarning];

    
}

#pragma mark - View lifecycle

-(void)addBackActionCallback:(BackActionCallback)callback
{
    if(callback)
    self.backActionCallback=callback;
}

-(void)addApproveActionCallback:(ApproveActionCallback)callback
{
    if(callback) self.approveActionCallback = callback;
}

-(void)setupFooterBar
{
    UIImageView *toolbarContainerView=[[UIImageView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds)-58.0, CGRectGetWidth(self.view.bounds), 58.0)];
    toolbarContainerView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
   // toolbarContainerView.autoresizingMask=UIViewAutoresizingFlexibleTopMargin;
    toolbarContainerView.autoresizesSubviews = YES;
    toolbarContainerView.clipsToBounds = YES;
    toolbarContainerView.backgroundColor=[UIColor clearColor];
    toolbarContainerView.userInteractionEnabled=YES;
    toolbarContainerView.image=[UIImage imageNamed:@"tool_bar.png"];
    [self.view addSubview:toolbarContainerView];
    
    UIImage *uploadButtonImage = [UIImage imageNamed:@"upload_button.png"];
    UIButton *importButton=[UIButton buttonWithType:UIButtonTypeCustom];
  CGRect buttonFrame=CGRectMake(10, 0.5, uploadButtonImage.size.width,uploadButtonImage.size.height);
    importButton.frame=buttonFrame;
    [importButton setBackgroundImage:uploadButtonImage forState:UIControlStateNormal];
    [importButton setBackgroundImage:[UIImage imageNamed:@"upload_button_selected.png"] forState:UIControlStateHighlighted];
    importButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    importButton.exclusiveTouch=YES;
    [importButton addTarget:self action:@selector(importFromLibaryAction:) forControlEvents:UIControlEventTouchUpInside];
    [toolbarContainerView addSubview:importButton];
    
    
    CGFloat _widthX = CGRectGetWidth(toolbarContainerView.bounds);
    
    UIImage *denyButImage = [UIImage imageNamed:@"retake_button.png"];
    denyButton=[UIButton buttonWithType:UIButtonTypeCustom];
    //denyButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    denyButton.frame=CGRectMake((_widthX-70.0)/2.0, 0.5, denyButImage.size.width,denyButImage.size.height);
    [denyButton setBackgroundImage:denyButImage forState:UIControlStateNormal];
     [denyButton setBackgroundImage:[UIImage imageNamed:@"retake_button_selected.png"] forState:UIControlStateHighlighted];
    [denyButton addTarget:self action:@selector(denyAction:) forControlEvents:UIControlEventTouchUpInside];
    denyButton.exclusiveTouch=YES;
    denyButton.enabled=NO;
    [toolbarContainerView addSubview:denyButton];
  //  denyButton.autoresizingMask= UIViewAutoresizingFlexibleRightMargin;
    
    UIImage *approveImage = [UIImage imageNamed:@"approved_button.png"];
    approveButton=[UIButton buttonWithType:UIButtonTypeCustom];
    approveButton.frame=CGRectMake(CGRectGetMaxX(denyButton.frame)+10.0, 0.5, approveImage.size.width,approveImage.size.height);
    //approveButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    [approveButton setBackgroundImage:approveImage forState:UIControlStateNormal];
    [approveButton setBackgroundImage:[UIImage imageNamed:@"approved_button_selected.png"] forState:UIControlStateHighlighted];
    [approveButton addTarget:self action:@selector(approveAction:) forControlEvents:UIControlEventTouchUpInside];
    approveButton.exclusiveTouch=YES;
    approveButton.enabled=NO;
    //approveButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    [toolbarContainerView addSubview:approveButton];
    
    [toolbarContainerView release];
}

-(void)setupCameraView
{
    //cameraView=[[UIView alloc ]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds),  CGRectGetHeight(self.view.bounds)-58.0)];
    cameraView=[[UIView alloc ]initWithFrame:self.view.bounds];
    cameraView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    cameraView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:cameraView];
    
    
    AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = _captureManager.previewLayer;
    // captureVideoPreviewLayer.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.5].CGColor;
    //captureVideoPreviewLayer.frame = CGRectMake(0, 0, cameraView.bounds.size.height+58, cameraView.bounds.size.width-102.0);
    
    captureVideoPreviewLayer.frame = CGRectMake(0, 0, 1024.0, 768.0);
    
   // captureVideoPreviewLayer.frame = CGRectMake(0, 0, cameraView.bounds.size.height+58, cameraView.bounds.size.width-55.0);
    
    if(CGSizeEqualToSize(_originalLandScapeSize, CGSizeZero))
        _originalLandScapeSize=captureVideoPreviewLayer.bounds.size;
    
    // captureVideoPreviewLayer.transform = CATransform3DRotate(CATransform3DIdentity, M_PI/2.0f, 0, 0, 1);
    
    //===================
    CALayer *viewLayer = [cameraView layer];
    [viewLayer setMasksToBounds:YES];
    
    //CGRect bounds = [cameraView bounds];
    //[captureVideoPreviewLayer setFrame:bounds];
    
    [captureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    [viewLayer insertSublayer:captureVideoPreviewLayer below:[[viewLayer sublayers] objectAtIndex:0]];
   // [cameraView.layer addSublayer:captureVideoPreviewLayer];
     //========================
    
    if([captureVideoPreviewLayer isOrientationSupported])
    {
        [captureVideoPreviewLayer setOrientation:[UIDevice currentDevice].orientation];
    }
    
    capturedImageView=[[UIImageView alloc]initWithFrame:self.view.bounds];
    capturedImageView.backgroundColor=[UIColor clearColor];
    capturedImageView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    capturedImageView.contentMode=UIViewContentModeScaleAspectFit;
    [self.view addSubview:capturedImageView];
    
    //BY DEFAULT IT IS HIDDEN. LATER NEED TO SET IT VISIBLE WHEN IMAGE IS CAPTURED OR IMPORTED FROM PHOTO LIB.
    capturedImageView.hidden=YES;
    
    captureButton=[UIButton buttonWithType:UIButtonTypeCustom];
    captureButton.frame=CGRectMake(CGRectGetWidth(self.view.frame)-88.0, (CGRectGetHeight(cameraView.frame)-85.0)/2.0, 88.0,85.0);
    [captureButton setBackgroundImage:[UIImage imageNamed:@"snap_button.png"] forState:UIControlStateNormal];
    
    [captureButton addTarget:self action:@selector(captureAction:) forControlEvents:UIControlEventTouchUpInside];
    captureButton.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    captureButton.exclusiveTouch=YES;
    [self.view addSubview:captureButton];
    captureButton.enabled=isSupportedCamera;
   
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.backgroundColor=[UIColor colorWithWhite:0.2 alpha:0.5];
    self.navigationController.navigationBarHidden=NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    [self addBackButtonWithSelector:@selector(backButtonAction:)];
    [self.navigationController.navigationBar addBackGroundImage:@"navigation_bar.png"];
    [self addTitle:@"TAKE PHOTO" withFont:[UIFont subaruBoldFontOfSize:23.0]];
    
    isSupportedCamera=[UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    
      //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageDidCapture:) name:kImageCapturedSuccessfully object:nil];
    
    [self setupCameraView];
    [self setupFooterBar];
    
    [self forcefullySetFrameForDenyApproveButtonsForOrientation:self.interfaceOrientation];
    
    if (!isSupportedCamera) 
	{
        
	  	UIAlertView *aAlert = [[UIAlertView alloc] initWithTitle:NO_CAMERA_HEADER message:NO_CAMERA_FOUND_MSG delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[aAlert show];
		[aAlert release];
        
        return;
	}
    
   // [_captureManager setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];/
    [_captureManager setVideoOrientation:[UIDevice currentDevice].orientation];
 
    [[_captureManager captureSession] startRunning];
    
   // if ([captureVideoPreviewLayer isOrientationSupported])
     //   [captureVideoPreviewLayer setOrientation:[[UIDevice currentDevice] orientation]];
    
    NSLog(@"Current orientation=%d",self.interfaceOrientation);
    
}

-(void)backButtonAction:(id)sender
{
    if(isImageAvailable)
    {
        UIAlertView *alert=  [UIAlertView alertViewWithTitle:CAMERA_VIEW_LEAVE_ALERT_HEADER message:CAMERA_VIEW_LEAVE_ALERT_MESSAGE cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObject:@"Yes"] onDismiss:^(int buttonIndex) 
                              {
                                  self.approveActionCallback = nil;
                                  self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
                                  
                                  // [self.navigationController popViewControllerAnimated:YES];
                                  // [self dismissModalViewControllerAnimated:YES];
                                  
                                 if(self.isLaunchedFromCustomerSection)
                                 {
                                    [self dismissModalViewControllerAnimated:YES];
                                 }
                                 else
                                 {
                                      [self.navigationController popViewControllerAnimated:YES];
                                 }

                              }
                              onCancel:^{
                                  
                              }];
        [alert release];
                              
    }
    else {
        self.approveActionCallback = nil;
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
       // [self.navigationController popViewControllerAnimated:YES];
       // [self dismissModalViewControllerAnimated:YES];
        
        if(self.isLaunchedFromCustomerSection)
        {
            [self dismissModalViewControllerAnimated:YES];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(void)forcefullySetLandscapeOrientation
{

    UIInterfaceOrientation _curOrientation=[UIApplication sharedApplication].statusBarOrientation;
    if(!UIInterfaceOrientationIsLandscape(_curOrientation))
    {
        //[[UIDevice currentDevice] setOrientation:UIDeviceOrientationLandscapeLeft];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
     [self forcefullySetLandscapeOrientation];
}

-(void)viewWillDisappear:(BOOL)animated
{
    //[super viewWillDisappear:animated];
    
    
   // [self forcefullySetLandscapeOrientation];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    //[[_captureManager captureSession] stopRunning];
    [_captureManager stopRunning];
}

-(void)captureAction:(id)sender
{
    ProgressHUD *hud=[ProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Capturing...";
    
    captureButton.enabled=NO;
    //[_captureManager captureStillImage];
   // capturedImageView.hidden=NO;
    
    [_captureManager captureStillImage:^(UIImage *image) {
        
        [self cameraDidCaptureImage:image];
        
    }];
}



-(void)denyAction:(id)sender
{
    isImageAvailable=NO;
    [[_captureManager captureSession] startRunning];
    capturedImageView.hidden=YES;
    cameraView.hidden = NO;
    
    denyButton.enabled=NO;
    approveButton.enabled=NO;
    captureButton.enabled=YES;
}


-(void)selectImage:(UIImage *)image
{
    isImageAvailable =YES;// (image!=nil);
    capturedImageView.hidden = NO;
    capturedImageView.image  = image;
    
    cameraView.hidden = YES;
    
    denyButton.enabled=YES;
    approveButton.enabled=YES;
    captureButton.enabled=NO;
    
    //capturedImageView.image  = image;
    
    // image = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:capturedImageView.frame.size interpolationQuality:kCGInterpolationDefault];
    
    if(image)
    {
        image = [UIImage imageWithImage:image scaledToMaxWidth:CGRectGetWidth(capturedImageView.frame) maxHeight:CGRectGetHeight(capturedImageView.frame)];
    }
    
    //ANIMATE LIKE PAGE CURL
    
    NSLog(@"image width=%f height=%f",image.size.width,image.size.height);
}

-(void)approveAction:(UIButton *)sender
{
    if(allowCaptionEntry)
    {
        sender.userInteractionEnabled = NO;
        CGRect r=((UIButton*)sender).frame;
        CGRect tRect=[((UIButton*)sender) convertRect:((UIButton*)sender).frame toView:self.view];
        tRect.origin.x=r.origin.x;
        
          // ========================== CREATE NEW VISIT PHOTO =====================
           VisitPhoto *_newVisitPhoto = (VisitPhoto *)[VisitPhoto insertNewObject];
           _newVisitPhoto.createdDate = [NSDate date];
          //_newVisitPhoto.parentVisit=self.visit;
          // ========================== CREATE NEW VISIT PHOTO =====================
        
           // NSMutableDictionary *newPhotoDict=[NSMutableDictionary dictionary];
           // AddPhotoCaptionViewController *photoCaptionViewController=[[AddPhotoCaptionViewController alloc]initWithPhotoDict:newPhotoDict];
        
          AddPhotoCaptionViewController *photoCaptionViewController = [[AddPhotoCaptionViewController alloc]initWithVisitPhoto:_newVisitPhoto];
            
            photoCaptionViewController.contentSizeForViewInPopover=CGSizeMake(352.0, 510.0);
        
            if(capturedImageView.image)
            photoCaptionViewController.originalImage=capturedImageView.image;
        
            UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:photoCaptionViewController];
            
            
          if(viewPhotosPopover) [viewPhotosPopover release];
            viewPhotosPopover=[[UIPopoverController alloc] initWithContentViewController:navController];
           photoCaptionViewController.popoverController=viewPhotosPopover;
           
            [photoCaptionViewController addRetakeActionCallback:^{
                
                [viewPhotosPopover dismissPopoverAnimated:YES];
                [self denyAction:nil];
            }];
            
            [photoCaptionViewController addConfirmActionCallback:^{
               
                //[self.photos addObject:newPhotoDict];
               // NSError *error=nil;
                //[[SSCoreDataManager sharedManager] save:error];
                
                [self.visit addPhotosObject:_newVisitPhoto];
                
                [viewPhotosPopover dismissPopoverAnimated:YES];
                [self.navigationController popViewControllerAnimated:YES];
                if(self.backActionCallback) self.backActionCallback();
                self.backActionCallback = nil;
                
                if(self.delegate && [self.delegate respondsToSelector:@selector(takePhotoViewControllerDidBack:)])
                {
                    [self.delegate takePhotoViewControllerDidBack:self];
                }
            }];
            
            [navController release];
            [photoCaptionViewController release];

            [viewPhotosPopover presentPopoverFromRect:tRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    if(self.approveActionCallback) self.approveActionCallback(capturedImageView.image);
    
    self.approveActionCallback = nil;
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(takePhotoViewController: didCaptureImage:)])
    {
        [self.delegate takePhotoViewController:self didCaptureImage:capturedImageView.image];
    }
 }

- (UIImage *)rotateImage:(UIImage *) img
{
    CGSize imgSize = [img size];
    UIGraphicsBeginImageContext(imgSize);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextRotateCTM(context, M_PI_2);
    CGContextTranslateCTM(context, 0, -640);
    [img drawInRect:CGRectMake(0, 0, imgSize.height, imgSize.width)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)cameraDidCaptureImage:(UIImage *)image
{
    self.navigationItem.leftBarButtonItem.enabled=NO;
    
    [[_captureManager captureSession] stopRunning];
    
      UIImage *oriented_image =nil;
    
    // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
    oriented_image = image;// [image fixOrientation];
    
    //  dispatch_async(dispatch_get_main_queue(), ^{
    self.navigationItem.leftBarButtonItem.enabled=YES;
    capturedImageView.image  = image;
    
    isImageAvailable=YES;
    [self selectImage:oriented_image];
    
    [ProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)imageDidCapture:(NSNotification *)notification
{
    self.navigationItem.leftBarButtonItem.enabled=NO;
    
    [[_captureManager captureSession] stopRunning];
    
     UIImage *image = [notification object];
    
     UIImage *oriented_image =nil;
    
   // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
      oriented_image = image;// [image fixOrientation];
        
      //  dispatch_async(dispatch_get_main_queue(), ^{
        self.navigationItem.leftBarButtonItem.enabled=YES;
       capturedImageView.image  = image;
    
        isImageAvailable=YES;
        [self selectImage:oriented_image];
    
    [ProgressHUD hideHUDForView:self.view animated:YES];
    

      //  });
   // });
    
  
    /*
    if(UIInterfaceOrientationIsPortrait(_curOrientation))
    {
       // AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = _captureManager.previewLayer;
        
       // CGSize _imageSize = CGSizeMake(1024, 1024.0);
        
        //oriented_image = [oriented_image resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:_imageSize interpolationQuality:kCGInterpolationHigh];
        
        oriented_image = [image croppedImage:CGRectMake(0, 0, 1024, 900.0)];
        oriented_image =[oriented_image fixOrientation];;
        
        //UIImage * PortraitImage = [[UIImage alloc] initWithCGImage: image.CGImage
                                                           //  scale: 1.0
                                                      // orientation: UIImageOrientationUp];
        
       // oriented_image = PortraitImage;
        
//        /
//         AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = _captureManager.previewLayer;
//        CGRect rect = [captureVideoPreviewLayer  frame];
//        UIGraphicsBeginImageContextWithOptions(rect.size,YES,0.0f);
//        CGContextRef context = UIGraphicsGetCurrentContext();
//        [captureVideoPreviewLayer renderInContext:context];   
//        UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
        /
        
    }
    else 
    {
         //oriented_image = [image fixOrientation];
    }
    */
   
    //_captureManager.stillImage
}

-(void)importFromLibaryAction:(id)sender
{
    CGRect r=((UIButton*)sender).frame;
    CGRect tRect=[((UIButton*)sender) convertRect:((UIButton*)sender).frame toView:self.view];
    tRect.origin.x=r.origin.x;

    if (!imagePickerPopover)
    {
        if(!imagePicker)
           imagePicker = [[UIImagePickerController alloc] init];
            //imagePicker = [[NonRotatingUIImagePickerController alloc] init];

            
        
        if(imagePicker)
        {
          imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
          imagePicker.delegate = self;
          //imagePickerPopover.delegate = self;
          imagePickerPopover= [[UIPopoverController alloc] initWithContentViewController:imagePicker];
        }
        
        imagePickerPopover.popoverContentSize=CGSizeMake(320, 500);
        //[imagePicker release];
    }
        
    //popover.delegate = self;
    [imagePickerPopover presentPopoverFromRect:tRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    //UIPopoverArrowDirectionUp
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //[picker dismissModalViewControllerAnimated:YES];
    
}

- (void)imagePickerController:(UIImagePickerController *)aPicker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(imagePickerPopover && imagePickerPopover.isPopoverVisible) 
        [imagePickerPopover dismissPopoverAnimated:YES];
    
   // [aPicker dismissModalViewControllerAnimated:YES];
    UIImage *image = (UIImage *)[info valueForKey:UIImagePickerControllerOriginalImage];
    //image=[image fixOrientation];
    [self selectImage:image];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    [imagePicker release] ;
    imagePicker=nil;
    
    
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
    
        if ([navigationController isKindOfClass:[UIImagePickerController class]] &&
            ((UIImagePickerController *)navigationController).sourceType == UIImagePickerControllerSourceTypePhotoLibrary)
        {
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque animated:NO];
        }
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
     [[_captureManager captureSession] stopRunning];
}

#pragma mark GUPI to solve ipad landscape orientation bug
-(void)forcefullySetFrameForDenyApproveButtonsForOrientation:(UIInterfaceOrientation)orientation
{
    BOOL isLandScape=UIInterfaceOrientationIsLandscape(orientation);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    if(isLandScape)
    {
        CGRect  butFrame = denyButton.frame;
        butFrame.origin.x=470;
        denyButton.frame = butFrame;
        
        butFrame = approveButton.frame;
        butFrame.origin.x=558;
        approveButton.frame = butFrame;
    }
    else
    {
        CGRect  butFrame = denyButton.frame;
        butFrame.origin.x=325;
        denyButton.frame = butFrame;
        
        butFrame = approveButton.frame;
        butFrame.origin.x=415;
        approveButton.frame = butFrame;
    }

    [UIView commitAnimations];
}
#pragma mark -


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
   /// [_captureManager setVideoOrientation:interfaceOrientation];
    
   // AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = _captureManager.previewLayer;
    
    //if ([captureVideoPreviewLayer isOrientationSupported]) {
       // [captureVideoPreviewLayer setOrientation:interfaceOrientation];
   // }
    
  //  BOOL isLandScape=UIInterfaceOrientationIsLandscape(interfaceOrientation);
    
    
    /*
    AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = _captureManager.previewLayer;
    CGRect videoLayerFrame=captureVideoPreviewLayer.frame;
    
    if(isLandScape)
    {
        videoLayerFrame.size=_originalLandScapeSize;
        
    }
    else
    {
       // videoLayerFrame.size.width  = _originalLandScapeSize.height+44.0+20.0;
       // videoLayerFrame.size.height = _originalLandScapeSize.width-(44.0+20.0+40.0);
        
        videoLayerFrame.size.width =768;
        videoLayerFrame.size.height =1024;
        
       // videoLayerFrame.size.width  = _originalLandScapeSize.height+58.0+20.0+44;
       // videoLayerFrame.size.height = _originalLandScapeSize.width-(58.0+20.0+40.0);
         //videoLayerFrame.size.height = _originalLandScapeSize.width-(58.0+20.0);

        
        // Navbar height = 44.0 StatusBar height = 20.0 bottomBar height = 40.0
    }
    
    captureVideoPreviewLayer.frame=videoLayerFrame;
     
    */
    
    //self.navigationItem.leftBarButtonItem.enabled=isLandScape;
    
    [self forcefullySetFrameForDenyApproveButtonsForOrientation:interfaceOrientation];

    /*
    if(isImageAvailable)
    {
      [_captureManager setVideoOrientation:interfaceOrientation];
      return YES;
    }
    else
    {
        BOOL b=UIInterfaceOrientationIsLandscape(interfaceOrientation);
        if(b)
        [_captureManager setVideoOrientation:interfaceOrientation];
         
        return b;
    }
    */
    
    return YES;
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (BOOL)shouldAutorotate
{
    return YES;// [self shouldAutorotateToInterfaceOrientation:[[UIDevice currentDevice] orientation]];
}
 
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationMaskAll;
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
     [self forcefullySetFrameForDenyApproveButtonsForOrientation:[[UIDevice currentDevice] orientation]];
    
    AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = _captureManager.previewLayer;
    capturedImageView.frame = self.view.bounds;
    captureVideoPreviewLayer.frame = capturedImageView.bounds;
   // captureVideoPreviewLayer.orientation = [[UIDevice currentDevice] orientation];
    if ([captureVideoPreviewLayer isOrientationSupported])
        [captureVideoPreviewLayer setOrientation:[[UIDevice currentDevice] orientation]];
    
    
    
     if(viewPhotosPopover.isPopoverVisible) [viewPhotosPopover dismissPopoverAnimated:YES];
}


@end
