//
//  VisitPhotosViewController.m
//  TestSoundNote
//
//  Created by RANDEM MAC on 21/06/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import "VisitPhotosViewController.h"
#import "VisitPhoto.h"
#import "TakePhotoViewController.h"
#import "Visit.h"
#import "Customer.h"
#import "VisitPhotoTableViewCell.h"
#import "OfflineManager.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface VisitPhotosViewController()<UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate>
{
    AddPhotoCaptionViewController *addPhotoCaptionViewcontroller;
}
@property(nonatomic,retain,readwrite) UITableView *tableView;
@end

@implementation VisitPhotosViewController
@synthesize photos;
@synthesize visit;
@synthesize tableView=_tableView;

- (id)initWithPhotos:(NSMutableArray *)_photos
{
   // self = [super initWithStyle:UITableViewStylePlain];
    self = [super init];
    
    if (self) 
    {
        self.photos = _photos;
        self.title  = @"PHOTO LIST";
 
    }
    return self;
}

- (id)initWithVisit:(Visit *)aVisit
{
   // self = [super initWithStyle:UITableViewStylePlain];
    self = [super init];
    if (self) 
    {
        self.visit = aVisit;
        self.title=  @"PHOTO LIST";
    }
    return self;
}



-(void)dealloc
{
     [addPhotoCaptionViewcontroller release];
    visit = nil;
    [photos release];
    [_tableView release] , _tableView=nil;
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


-(void)setupView
{
    UIImage *takePhotoImage = [UIImage imageNamed:@"take_a_photo.png"];
    takePhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    takePhotoButton.frame = CGRectMake((CGRectGetWidth(self.view.bounds)-takePhotoImage.size.width)/2.0, 10.0,takePhotoImage.size.width , takePhotoImage.size.height);
    [takePhotoButton setBackgroundImage:takePhotoImage forState:UIControlStateNormal];
    [takePhotoButton setBackgroundImage:[UIImage imageNamed:@"take_a_photo_selected.png"] forState:UIControlStateSelected];
    [self.view addSubview:takePhotoButton];
    takePhotoButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin |UIViewAutoresizingFlexibleLeftMargin;
    [takePhotoButton addTarget:self action:@selector(takePhotoAction:) forControlEvents:UIControlEventTouchUpInside];
    takePhotoButton.enabled = ![self.visit.isVisitCompleted boolValue];
    
    CGFloat locY=CGRectGetMaxY(takePhotoButton.frame)+10.0;
    
    CGRect _tableViewFrame = CGRectMake(0, locY, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-locY);
    
    _tableView =  [[UITableView tableViewWithFrame:_tableViewFrame style:UITableViewStyleGrouped delegate:self datasource:self] retain];
    _tableView.bounces = NO;
    _tableView.rowHeight = 85;
    _tableView.sectionHeaderHeight = 0.0;
    _tableView.backgroundColor = [UIColor clearColor];
     [self.view addSubview:_tableView];

    _tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth  | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    
    UIView *bgView = [[UIView alloc] initWithFrame:_tableView.bounds];
    bgView.backgroundColor = [UIColor clearColor];
    //[UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
    _tableView.backgroundView = bgView;
    [bgView release];
    
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIView *_headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 1.0)];
        _headerView.backgroundColor = [UIColor clearColor];
        self.tableView.tableHeaderView = _headerView;
        [_headerView release];
    }

}

-(void)reloadData
{
    NSArray *_photos = [self.visit.photos allObjects];
    self.photos= _photos ?  [NSMutableArray arrayWithArray:_photos] : [NSMutableArray array];
    
    if([self.photos count])
    {
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate" ascending:YES];
    [self.photos sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    [descriptor release];
    }
    
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    //[LazyImageLoader shouldSaveToDisk:YES];
    
    [self setupView];
    
    //self.view.userInteractionEnabled = ![self.visit.isReportCompleted boolValue];
    
    if(!self.photos)
    {
        [self reloadData];
    }
}

-(void)markAsComplete
{
     takePhotoButton.enabled = ![self.visit.isVisitCompleted boolValue];
    
    if(addPhotoCaptionViewcontroller)
    {
        [addPhotoCaptionViewcontroller reload];
    }
}

-(void)takePhotoAction:(id)sender
{
    TakePhotoViewController *takePhotoVc=[[TakePhotoViewController alloc] init];
    takePhotoVc.visit = self.visit;
    takePhotoVc.allowCaptionEntry = NO;
    
     __block VisitPhotosViewController *weakSelf = self;
    
    [takePhotoVc addBackActionCallback:^{
        
        //[self viewPhotosAction:viewPhotosButton];
        [self reloadData];
    }];
    
   
    
    [takePhotoVc addApproveActionCallback:^(UIImage *image) 
    {
        NSManagedObjectContext *newContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        newContext.persistentStoreCoordinator= DEFAULT_CONTEXT.persistentStoreCoordinator;
        newContext.undoManager = nil;
        
        VisitPhoto *_newVisitPhoto = (VisitPhoto *)[VisitPhoto insertNewObjectInContext:newContext];
        _newVisitPhoto.createdDate = [NSDate date];
        //_newVisitPhoto.parentVisit = self.visit;
        
        
     __block AddPhotoCaptionViewController  *photoCaptionViewController=[[AddPhotoCaptionViewController alloc]initWithVisitPhoto:_newVisitPhoto];
        photoCaptionViewController.isNewPhoto = YES;
        
        if(image)
        photoCaptionViewController.originalImage=image;
        
        [self.navigationController pushViewController:photoCaptionViewController animated:NO];
        [takePhotoVc.navigationController popViewControllerAnimated:YES];
        
        [photoCaptionViewController addConfirmActionCallback:^{
            
            Visit *_parentVisit=(Visit *)[newContext objectWithID:self.visit.objectID];
            [_parentVisit addPhotosObject:_newVisitPhoto];
            NSError *error=nil;
            [[SSCoreDataManager sharedManager] saveContext:newContext error:error];
            
            [photoCaptionViewController.navigationController popViewControllerAnimated:YES];
            [weakSelf reloadData];

            [newContext reset];
            [newContext release];
        }];
        
        [photoCaptionViewController addCancelActionCallback:^{
            
            [newContext reset];
            [newContext release];
        }];
        
        [photoCaptionViewController release];
    }];
    
    UINavigationController *navcontroller = [self nextResponderNavigationController];
    [navcontroller pushViewController:takePhotoVc animated:YES];
    [takePhotoVc release];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //[self.tableView reloadData];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.photos.count;//MAX(self.photos.count, 1);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier  = @"Cell";
    static NSString *CellIdentifier2 = @"Cell_nodata";
    
    int _photosCount = self.photos.count;
    
    if(_photosCount==0)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil) 
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier2] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.textColor = [UIColor lightGrayColor];
            
            cell.backgroundColor =  [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
        }

        cell.textLabel.text = @"No Photo(s)";
        
        return cell;
    }
    else 
    {
        
    VisitPhoto *_visitPhoto = (indexPath.row<_photosCount) ? [self.photos objectAtIndex:indexPath.row] : nil;
        
     VisitPhotoTableViewCell *cell =(VisitPhotoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
     if (cell == nil) 
        {
          cell = [[[VisitPhotoTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
            
            [cell.emailButton addTarget:self action:@selector(emailPhotoAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        [cell setVisitPhotoInfo:_visitPhoto];
        
         //cell.emailButton.hidden = [self.visit.isReportCompleted boolValue];
        
        if([self.visit.isReportCompleted boolValue]) cell.accessoryView = nil;
        
        
        return cell;
    }   
        
    return nil;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    int count = [self.photos count];
    if(!count) return;
    
    VisitPhotoTableViewCell *_cell = (VisitPhotoTableViewCell *)cell;
    
    UIRectCorner corner=UIRectCornerBottomLeft;
    if(indexPath.row == 0) //FIRST ROW
    {
        
        corner = (count == 1) ? UIRectCornerTopLeft | UIRectCornerBottomLeft :  UIRectCornerTopLeft;
    }
    else if(indexPath.row == [self.photos count]-1) //LAST ROW
    {   
        corner = UIRectCornerBottomLeft;
    }
    
   
    [_cell.photoImageView roundCorners:corner withRedius:CGSizeMake(8.0, 8.0)];
    
    
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor clearColor];

    
    CellBackgroundView *_selectedBgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _selectedBgView.backgroundColor = [UIColor clearColor];
    _selectedBgView.contentInset = CGPointMake(10.0, 0.0);
    _selectedBgView.cornerRadius = 8.0;
    _selectedBgView.borderColor = self.tableView.separatorColor;
    _selectedBgView.fillColor = [UIColor cellSelectedColorBlue];
    
    
    int numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    _selectedBgView.position = _bgView.position;
    
    
    cell.backgroundView = _bgView;
    cell.selectedBackgroundView = _selectedBgView;
    
    [_bgView release];
    [_selectedBgView release];

}




#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    if([self.visit.isReportCompleted boolValue]) 
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    */
    
    VisitPhoto *_visitPhoto = (indexPath.row<[self.photos count]) ? [self.photos objectAtIndex:indexPath.row] : nil;
    
    if(!_visitPhoto) return;
    
    if(addPhotoCaptionViewcontroller)
    {
        [addPhotoCaptionViewcontroller release];
    }   addPhotoCaptionViewcontroller = nil;
    
    addPhotoCaptionViewcontroller = [[AddPhotoCaptionViewController alloc] initWithVisitPhoto:_visitPhoto];
    addPhotoCaptionViewcontroller.isEditMode = ![self.visit.isReportCompleted boolValue];
    
    addPhotoCaptionViewcontroller.visitPhotos = self.photos;
    addPhotoCaptionViewcontroller.selectedPhotoIndex = indexPath.row;
    
    __block VisitPhotosViewController *weakSelf = self;
    [addPhotoCaptionViewcontroller addConfirmActionCallback:^{
        
        // NSError *error=nil;
        //[[SSCoreDataManager sharedManager] save:error];
        
        [weakSelf.navigationController popViewControllerAnimated:YES];
        
        //[self.popoverController dismissPopoverAnimated:YES];
       // [self.navigationController popViewControllerAnimated:NO];
       // if(self.backActionCallback) self.backActionCallback();
    }];
    
    [addPhotoCaptionViewcontroller addDeleteActionCallback:^{
        
        NSUInteger _index = [weakSelf.photos indexOfObject:_visitPhoto];
        if(_index != NSNotFound)
        {
            [weakSelf.photos removeObjectAtIndex:_index];
            [weakSelf.tableView reloadData];
        }
    }];
     
    [self.navigationController pushViewController:addPhotoCaptionViewcontroller animated:YES];
    
   // [addPhotoCaptionViewcontroller  release];
}

-(NSString *)setupMailBodyForVisitPhoto:(VisitPhoto *)photo
{
    NSString *photoDescription = photo.caption;
    NSString *photoOEMName = photo.oemName;
    
       NSString* ifDetailsStringPresent = (photoDescription.length)?@"and the details ":@"";
    
    NSString *_formatString=[NSString stringWithFormat:@"Hi,\n\nPlease find the photo %@below:",ifDetailsStringPresent];
     NSMutableString *_formatStr= [NSMutableString stringWithString:_formatString];
    
    if(photoDescription.length)
     [_formatStr appendFormat:@"\n\n%@",photoDescription];
    
    if(photoOEMName.length)
    {
        [_formatStr appendFormat:@"\n\nOEM : %@",photoOEMName];
        
    }
    

    /*
    NSString *_formatString =  @"<html><body style=\"padding:0 0 0 0; margin:0 auto;\"> <table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td style=\"padding:0 0 0 0; margin:0 0 0 0; width:10px;\"></td> <td><table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td style=\"padding:0 0 0 0; margin:0 0 0 0; height:10px;\"></td> </tr> <tr> <td style=\"padding:0 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;\">Hi,</td> </tr>";
    
    NSMutableString *_formatStr= [NSMutableString stringWithString:_formatString];
    
 
    
    NSString *detailsHeaderString = [NSString stringWithFormat:@"<tr><td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;\">Please find the photo %@below:</td></tr>", ifDetailsStringPresent];
    [_formatStr appendString:detailsHeaderString];
    
    NSString* descriptionFormatter = [NSString stringWithFormat:@"<tr><td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;\">%@</td></tr>", photoDescription];
    [_formatStr appendString:descriptionFormatter];
  
    
    if(photoOEMName.length)
    {
        [_formatStr appendFormat:@"<tr><td style=\"padding:20px 0 0 0; margin:0 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333;\">OEM : %@</td></tr>",photoOEMName];
    }
    
    NSString* endString = [NSString stringWithFormat:@"<tr><td style=\"padding:0 0 0 0; margin:0 0 0 0; height:10px;\"></td></tr></table></td><td style=\"padding:0 0 0 0; margin:0 0 0 0;  width:10px;\"></td></tr></table></body></html>"];

    [_formatStr appendString:endString];
     */
    
    return _formatStr;
}

-(void)emailPhotoAction:(UIButton *)sender
{
    Class mailclass = (NSClassFromString(@"MFMailComposeViewController"));
    if(mailclass!=nil && [mailclass canSendMail])
    {
        
   //VisitPhotoTableViewCell *cell = (VisitPhotoTableViewCell *) [sender superview];
    VisitPhotoTableViewCell *cell = (VisitPhotoTableViewCell *) [sender superViewOfType:[UITableViewCell class]];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        
    if(!indexPath) return;
    
    VisitPhoto *_visitPhoto = (indexPath.row<[self.photos count]) ? [self.photos objectAtIndex:indexPath.row] : nil;
    
    NSString *mailSubject =  [NSString stringWithFormat:@"VISIT PHOTO : %@", [self.visit.parentCustomer.name uppercaseString]];
    NSString *mailBody    = [self setupMailBodyForVisitPhoto:_visitPhoto];
    
    NSLog(@"mailbody = %@", mailBody);
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate=self;
    picker.navigationBar.tintColor=[UIColor blackColor];
    [picker setSubject:mailSubject];
    [picker setMessageBody:mailBody isHTML:NO];
    
    NSString *emailID = self.visit.parentCustomer.email;
    
    if(emailID.length)
    {
        [picker setToRecipients:[NSArray arrayWithObject:emailID]];
    }
    else 
    {
        [UIAlertView showWarningAlertWithTitle:@"" message:[NSString stringWithFormat:OFFER_EMAIL_ALERT_FORMAT ,self.visit.parentCustomer.name]];
    }
        
    ProgressHUD *progress = [ProgressHUD showHUDAddedTo:self.view animated:YES];
    progress.mode = MBProgressHUDModeIndeterminate;
    progress.tag  = NSIntegerMax-1;
    progress.labelText = @"Attaching..."; 
    
    NSString *imagePath = _visitPhoto.imagePath;
    UIImage *image = (imagePath.length) ?  [UIImage imageWithContentsOfFile:imagePath] : nil;
    
    if(!image) 
    {
        [UIAlertView showWarningAlertWithTitle:IMAGE_NOT_AVAILABLE_HEADER_MSG message:@""];
        
        [ProgressHUD hideHUDForView:self.view animated:YES];
        return;
    }
    
    NSString *mimeType = [[imagePath lastPathComponent] pathExtension];
    NSData *imageData = (mimeType && [mimeType isEqualToString:@"png"])?  UIImagePNGRepresentation(image) : UIImageJPEGRepresentation(image, 8.0);
    
    NSString *attachedFileName = [imagePath lastPathComponent];
    
    
    
    [picker addAttachmentData:imageData mimeType:[NSString stringWithFormat:@"image/%@",mimeType] fileName:attachedFileName.length?attachedFileName:@"offerImage"];
    
   //

        
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
    [self presentViewController:picker animated:YES completion:nil];
}
else
{
    [app.splitViewController.modalViewController presentModalViewController:picker animated:YES];
    //[app.splitViewController.presentingViewController presentViewController:picker animated:YES completion:nil];
}

    
    [picker release];   
    
    [ProgressHUD hideHUDForView:self.view animated:YES];
        
    }
    else 
    {
        [UIAlertView showWarningAlertWithTitle:@"" message:NO_EMAIL_CLIENT_MESSAGE];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
		  didFinishWithResult:(MFMailComposeResult)result
						error:(NSError*)error 
{
	if (result == MFMailComposeResultSent) 
    {
		BOOL _isNetworkAvailable = [[OfflineManager sharedManager] isNetworkAvailable];
        NSString *success = _isNetworkAvailable? @" Sent Successfully" : @"Email will be sent when you are online";
		UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Email Status:"
                                                         message:success
                                                        delegate:self
                                               cancelButtonTitle:nil
                                               otherButtonTitles:@"Ok",nil];
		[alert1 show];
        [alert1 release],alert1=nil;
		
	}
    else if (result == MFMailComposeResultFailed) 
    {
		NSString *msg = @"Failed";
		UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Email Status:"
                                                         message:msg
                                                        delegate:self
                                               cancelButtonTitle:nil
                                               otherButtonTitles:@"Ok",nil];
		[alert1 show];
        [alert1 release],alert1=nil;
		
	}
    
    else if (result == MFMailComposeResultCancelled) 
    {
        /*
         NSString *msg = @"Cancelled";
         UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Email Status:"
         message:msg
         delegate:self
         cancelButtonTitle:nil
         otherButtonTitles:@"Ok",nil];
         [alert1 show];
         [alert1 release],alert1=nil;
         */
	}
	
	
	//[app.splitViewController.modalViewController dismissModalViewControllerAnimated:YES];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    [self dismissViewControllerAnimated:YES completion:nil];
    else
    [app.splitViewController.modalViewController dismissModalViewControllerAnimated:YES];
   
}



@end
