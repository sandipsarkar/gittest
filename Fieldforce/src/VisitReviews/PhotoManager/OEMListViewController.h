//
//  OEMListViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 16/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OEM;
typedef void (^DidSelectOEMCallback)(NSString *oemId,NSString *oemName);
@interface OEMListViewController : UITableViewController

@property(nonatomic, retain) NSMutableArray *dataSource;

-(id)initWithOEMID:(NSUInteger) oemID andName:(NSString*) oemName;

-(void)addDidSelectOEMCallback:(DidSelectOEMCallback)callback;

@end
