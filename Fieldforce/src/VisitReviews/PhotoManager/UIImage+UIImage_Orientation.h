//
//  UIImage+UIImage_Orientation.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 06/07/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImage_Orientation)

- (UIImage *)normalizedImage;

- (UIImage *)fixOrientation ;

@end
