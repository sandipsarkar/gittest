#import "CaptureSessionManager.h"
#import <ImageIO/ImageIO.h>

@implementation CaptureSessionManager


@synthesize captureSession;
@synthesize previewLayer;
@synthesize stillImageOutput;
@synthesize stillImage;

#pragma mark Capture Session Configuration

- (id)init 
{
	if ((self = [super init]))
    {
		captureSession=[[AVCaptureSession alloc] init];
        
        captureSession.sessionPreset = AVCaptureSessionPresetPhoto;
        
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange) name:UIDeviceOrientationDidChangeNotification object:nil];
        
        _orientation = AVCaptureVideoOrientationLandscapeLeft | AVCaptureVideoOrientationLandscapeRight;
	}
    
	return self;
}

- (void)deviceOrientationDidChange
{	
	UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    
	if (deviceOrientation == UIDeviceOrientationPortrait)
		_orientation = AVCaptureVideoOrientationPortrait;
	else if (deviceOrientation == UIDeviceOrientationPortraitUpsideDown)
		_orientation = AVCaptureVideoOrientationPortraitUpsideDown;
	
	// AVCapture and UIDevice have opposite meanings for landscape left and right (AVCapture orientation is the same as UIInterfaceOrientation)
	else if (deviceOrientation == UIDeviceOrientationLandscapeLeft)
		_orientation = AVCaptureVideoOrientationLandscapeRight;
	else if (deviceOrientation == UIDeviceOrientationLandscapeRight)
		_orientation = AVCaptureVideoOrientationLandscapeLeft;
	
	// Ignore device orientations for which there is no corresponding still image orientation (e.g. UIDeviceOrientationFaceUp)
    [self setVideoOrientation:_orientation];
}

-(AVCaptureVideoOrientation)currentOrientation
{
    AVCaptureVideoOrientation	__orientation;
        UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
        
        if (deviceOrientation == UIDeviceOrientationPortrait)
            __orientation = AVCaptureVideoOrientationPortrait;
        else if (deviceOrientation == UIDeviceOrientationPortraitUpsideDown)
            __orientation = AVCaptureVideoOrientationPortraitUpsideDown;
        
        // AVCapture and UIDevice have opposite meanings for landscape left and right (AVCapture orientation is the same as UIInterfaceOrientation)
        else if (deviceOrientation == UIDeviceOrientationLandscapeLeft)
            __orientation = AVCaptureVideoOrientationLandscapeRight;
        else if (deviceOrientation == UIDeviceOrientationLandscapeRight)
            __orientation = AVCaptureVideoOrientationLandscapeLeft;
        
        // Ignore device orientations for which there is no corresponding still image orientation (e.g. UIDeviceOrientationFaceUp)
    
    
    
    return __orientation;

}


- (void)addVideoPreviewLayer {
	[self setPreviewLayer:[[[AVCaptureVideoPreviewLayer alloc] initWithSession:[self captureSession]] autorelease]];
	[[self previewLayer] setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    //[[self previewLayer] setVideoGravity:AVLayerVideoGravityResizeAspect];
  
}

- (void)addVideoInputFrontCamera:(BOOL)front {
    NSArray *devices = [AVCaptureDevice devices];
    AVCaptureDevice *frontCamera=nil;
    AVCaptureDevice *backCamera=nil;
    
    if(![devices count])
    {
        NSLog(@"No capture devices found.");
        return;
    }
    
    for (AVCaptureDevice *device in devices) 
    {
        
        NSLog(@"Device name: %@", [device localizedName]);
        
        if ([device hasMediaType:AVMediaTypeVideo]) 
        {
            
            if ([device position] == AVCaptureDevicePositionBack) {
                NSLog(@"Device position : back");
                backCamera = device;
            }
            else {
                NSLog(@"Device position : front");
                frontCamera = device;
            }
        }
    }
    
    NSError *error = nil;
    
    if (front)
    {
        AVCaptureDeviceInput *frontFacingCameraDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:frontCamera error:&error];
        if (!error)
        {
            /*
            AVCaptureSession *session= [self captureSession];
            AVCaptureInput* input = [session.inputs count]? [session.inputs objectAtIndex:0] : nil;
            if(input)
                [session removeInput:input];
            */
            
            BOOL availabe=[[[self captureSession] inputs] containsObject:frontFacingCameraDeviceInput];
            
            if (!availabe && [[self captureSession] canAddInput:frontFacingCameraDeviceInput]) {
                [[self captureSession] addInput:frontFacingCameraDeviceInput];
            } 
            else 
            {
                NSLog(@"Couldn't add front facing video input");
            }
        }
    } 
    else 
    {
        AVCaptureDeviceInput *backFacingCameraDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:backCamera error:&error];
        if (!error) 
        {
            /*
            AVCaptureSession *session= [self captureSession];
            AVCaptureVideoDataOutput* output = [session.outputs count] ? [session.outputs objectAtIndex:0] : nil;
            if(output)
                [session removeOutput:output];
             */
            
            BOOL availabe=[[[self captureSession] inputs] containsObject:backFacingCameraDeviceInput];
            
            if (!availabe &&  [[self captureSession] canAddInput:backFacingCameraDeviceInput])
            {
                [[self captureSession] addInput:backFacingCameraDeviceInput];
            } 
            else
            {
                NSLog(@"Couldn't add back facing video input");
            }
        }
    }
    
    
    //dispatch_queue_t queue = dispatch_queue_create("augm_reality", NULL);
  //  [output setSampleBufferDelegate: self queue: queue];
   // dispatch_release(queue);
}

-(AVCaptureConnection *)videoConnection
{
    AVCaptureConnection *videoConnection = nil;
	for (AVCaptureConnection *connection in [[self stillImageOutput] connections]) 
    {
		for (AVCaptureInputPort *port in [connection inputPorts]) 
        {
			if ([[port mediaType] isEqual:AVMediaTypeVideo])
            {
				videoConnection = connection;
				break;
			}
		}
		if (videoConnection) 
        { 
            break; 
        }
	}
    
        
    return videoConnection;
}


- (void)addStillImageOutput 
{
  [self setStillImageOutput:[[[AVCaptureStillImageOutput alloc] init] autorelease]];
  NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:AVVideoCodecJPEG,AVVideoCodecKey,nil];
  [[self stillImageOutput] setOutputSettings:outputSettings];
    [outputSettings release],
  
    /*
  AVCaptureConnection *videoConnection = nil;
  for (AVCaptureConnection *connection in [[self stillImageOutput] connections]) {
    for (AVCaptureInputPort *port in [connection inputPorts]) {
      if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
        videoConnection = connection;
        break;
      }
    }
    if (videoConnection) { 
      break; 
    }
  }
    */
  
  [[self captureSession] addOutput:[self stillImageOutput]];
}

-(void)setVideoOrientation:(AVCaptureVideoOrientation)orientation
{
    AVCaptureConnection *videoConnection = [self videoConnection];
   // videoConnection.videoOrientation = orientation;
    [videoConnection setVideoOrientation:orientation];
    [self previewLayer].orientation=orientation;
    
    _orientation=orientation;
}

- (void)captureStillImage
{  
	AVCaptureConnection *videoConnection = [self videoConnection];
    
    if ([videoConnection isVideoOrientationSupported])
        [videoConnection setVideoOrientation:_orientation];
    
    if (!videoConnection) return;
    
	NSLog(@"about to request a capture from: %@", [self stillImageOutput]);
	[[self stillImageOutput] captureStillImageAsynchronouslyFromConnection:videoConnection 
                                                       completionHandler:^(CMSampleBufferRef imageSampleBuffer, NSError *error)
    {
        /*
        CFDictionaryRef exifAttachments = CMGetAttachment(imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
        
            if (exifAttachments)
            {
              NSLog(@"attachements: %@", exifAttachments);
            }
            else
            {
                NSLog(@"no attachments");
            }
         */
        
         NSData *imageData = [AVCaptureStillImageOutput 
                              jpegStillImageNSDataRepresentation:imageSampleBuffer];    
         UIImage *image = [[UIImage alloc] initWithData:imageData];
         [self setStillImage:image];
         
         [[NSNotificationCenter defaultCenter] postNotificationName:kImageCapturedSuccessfully object:image];
           [image release];
        
       // if (exifAttachments) CFRelease(exifAttachments);
        
      //========================================
      //CMSampleBufferInvalidate(imageSampleBuffer);  CFRelease(imageSampleBuffer);
        // imageSampleBuffer = nil;
     //=======================================
        
    }];
}


- (void)captureStillImage:(void(^)(UIImage *image))callback
{
	AVCaptureConnection *videoConnection = [self videoConnection];
    
    if ([videoConnection isVideoOrientationSupported])
        [videoConnection setVideoOrientation:_orientation];
    
    if (!videoConnection) return;
    
	NSLog(@"about to request a capture from: %@", [self stillImageOutput]);
	[[self stillImageOutput] captureStillImageAsynchronouslyFromConnection:videoConnection
                                                         completionHandler:^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
         /*
          CFDictionaryRef exifAttachments = CMGetAttachment(imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
          
          if (exifAttachments)
          {
          NSLog(@"attachements: %@", exifAttachments);
          }
          else
          {
          NSLog(@"no attachments");
          }
          */
         
         NSData *imageData = [AVCaptureStillImageOutput
                              jpegStillImageNSDataRepresentation:imageSampleBuffer];
         UIImage *image = [[UIImage alloc] initWithData:imageData];
         [self setStillImage:image];
         
         if(callback) callback(image);
         
        // [[NSNotificationCenter defaultCenter] postNotificationName:kImageCapturedSuccessfully object:image];
         
         [image release];
         
         // if (exifAttachments) CFRelease(exifAttachments);
         
         //========================================
         //CMSampleBufferInvalidate(imageSampleBuffer);  CFRelease(imageSampleBuffer);
         // imageSampleBuffer = nil;
         //=======================================
         
     }];
}

-(void)stopRunning
{
    AVCaptureSession *session = [self captureSession];
    
    //====================================================================================
    AVCaptureInput* input = [session.inputs count]? [session.inputs objectAtIndex:0] : nil;
    if(input)
        [session removeInput:input];
    
    AVCaptureVideoDataOutput* output = [session.outputs count] ? [session.outputs objectAtIndex:0] : nil;
    if(output)
        [session removeOutput:output];
    
    [session stopRunning];
    
    if([previewLayer superlayer])
    [previewLayer removeFromSuperlayer];
    
    [previewLayer release];
    [captureSession release];
    
    previewLayer = nil;
    captureSession = nil;
    
}

- (void)dealloc
{
    AVCaptureSession *session = [self captureSession];
    //====================================================================================
    AVCaptureInput* input = [session.inputs count]? [session.inputs objectAtIndex:0] : nil;
    if(input)
    [session removeInput:input];
    
    AVCaptureVideoDataOutput* output = [session.outputs count] ? [session.outputs objectAtIndex:0] : nil;
    if(output)
    [session removeOutput:output];
    //=====================================================================================
    
    if(captureSession)
	[captureSession stopRunning];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
	[[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];

  [previewLayer release];
	
  [stillImageOutput release];
  [stillImage release];
  [captureSession release];
   
	[super dealloc];
}

@end
