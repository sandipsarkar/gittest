#import <AVFoundation/AVFoundation.h>
//#import "CaptureSessionManager.h"
//#import <ImageIO/ImageIO.h>

#define kImageCapturedSuccessfully @"imageCapturedSuccessfully"


@interface CaptureSessionManager : NSObject
{

    AVCaptureVideoOrientation _orientation;
}

@property (retain) AVCaptureVideoPreviewLayer *previewLayer;
@property (retain) AVCaptureSession *captureSession;
@property (retain) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic, retain) UIImage *stillImage;

- (void)addVideoPreviewLayer;
- (void)addStillImageOutput;
- (void)captureStillImage;
- (void)captureStillImage:(void(^)(UIImage *image))callback;
- (void)addVideoInputFrontCamera:(BOOL)front;

-(void)setVideoOrientation:(AVCaptureVideoOrientation)orientation;

-(AVCaptureVideoOrientation)currentOrientation;

-(void)stopRunning;

@end
