//
//  AddPhotoCaptionViewController.m
//  TestSoundNote
//
//  Created by RANDEM MAC on 21/06/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import "AddPhotoCaptionViewController.h"
#import "DirectoryManager.h"
#import "UIImage+UIImage_Orientation.h"
#import "Visit.h"
#import "VisitPhoto.h"
#import "UIImage+Resize.h"
#import "TakePhotoViewController.h"
#import "TagSelectionController.h"
#import "OfflineManager.h"
#import "OEMListViewController.h"
#import "OEM.h"
#import "CustomerFullImageViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "Customer.h"

//#define ORIGINAL_IMAGE_COMPRESSED_SIZE CGSizeMake(960.0,640.0)
#define ORIGINAL_IMAGE_COMPRESSED_SIZE CGSizeMake(1020.0,680.0)

@interface AddPhotoCaptionViewController()<UITextViewDelegate,MFMailComposeViewControllerDelegate>
{
    UIButton *selectOemButton;
}

@property(nonatomic,copy)RetakeAction retakeAction;
@property(nonatomic,copy)ConfirmAction confirmAction;
@property(nonatomic,copy) CancelAction cancelAction;
@property(nonatomic,copy) DeleteAction deleteAction;
@property(nonatomic,retain) UIPopoverController *oemPopover;

@end

@implementation AddPhotoCaptionViewController
@synthesize retakeAction;
@synthesize confirmAction;
@synthesize cancelAction;
@synthesize deleteAction;
@synthesize originalImage;
@synthesize visitPhoto;
@synthesize isNewPhoto;
@synthesize oemPopover;
@synthesize isEditMode;

@synthesize visitPhotos;
@synthesize selectedPhotoIndex;


- (id)initWithVisitPhoto:(VisitPhoto *)_photo
{
    self = [super init];
    
    if (self) 
    {
        self.visitPhoto = _photo;
        
        isSaved=NO;
        isEditMode = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidAppear:) name:UIKeyboardWillShowNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidDismiss:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}


-(void)dealloc
{
    selectedPhotoIndex=-1;
    visitPhotos=nil;
    
    [visitPhoto release];
    
    if(retakeAction)   [retakeAction release];
    if(confirmAction)  [confirmAction release];
    
    if(cancelAction)   [cancelAction release];
    if(deleteAction)   [deleteAction release];
    
    [scrollView release]; 
    [imageView release];
    [captionTextView release];
    [originalImage release];
    [oemPopover release];
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)reload
{
    _isVisitCompleted  = [self.visitPhoto.parentVisit.isVisitCompleted boolValue];
    _isReportPublished = [self.visitPhoto.parentVisit.isReportCompleted boolValue];
    
     captionTextView.editable = !_isReportPublished;
    selectOemButton.enabled   = !_isReportPublished;
    
    self.navigationItem.rightBarButtonItem.enabled = !_isVisitCompleted;
    self.navigationItem.rightBarButtonItem.customView.hidden = _isVisitCompleted;
}

-(void)addRetakeButton
{
    UIImage *retakeButImage = [UIImage imageNamed:@"edit_blank.png"];
    UIButton *retakeButton  = [UIButton buttonWithType:UIButtonTypeCustom];
    retakeButton.frame = CGRectMake(0,0,retakeButImage.size.width,retakeButImage.size.height);
    [retakeButton setBackgroundImage:retakeButImage forState:UIControlStateNormal];
    retakeButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
    [retakeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [retakeButton setTitle:@"Retake" forState:UIControlStateNormal];

    [retakeButton addTarget:self action:@selector(retakeAction:) forControlEvents:UIControlEventTouchUpInside];
    
   // retakeButton.enabled = isEditMode;
    retakeButton.hidden = !isEditMode;
    
    UIBarButtonItem *retakeBarButton=[[UIBarButtonItem alloc] initWithCustomView:retakeButton];
    self.navigationItem.rightBarButtonItem=retakeBarButton;
    retakeBarButton.enabled = isEditMode;
    [retakeBarButton release];
    
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
    
    [self.navigationController.navigationBar addBackGroundImage:@"right_sub_banner.png"];
    [self addBackButtonWithSelector:@selector(cancelAction:)];

    
    self.view.backgroundColor    = [UIColor clearColor];
    UIColor *_contentBGColor     = [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
    UIColor *_contentBorderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5];
    
    scrollView=[[UIScrollView alloc] initWithFrame:self.view.bounds];
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    scrollView.backgroundColor  = [UIColor clearColor];
    scrollView.bounces= NO;
    scrollView.autoresizesSubviews = YES;
    scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:scrollView];
    
    
    [self addRetakeButton];     
    
    CGFloat _targetImageHeight = 247.0;
    CGRect _bounds     = self.view.bounds; //[self.view convertRect:self.view.bounds toView:nil];
    CGSize contentSize = self.contentSizeForViewInPopover;
    contentSize.width = 352.0;
    float locY=10.0;
    
    UIImage *thumbImage = nil;
    NSString *imagePath = self.visitPhoto.thumbImagePath;
 
    if([[NSFileManager defaultManager] fileExistsAtPath:imagePath])
    {
        NSData *imageData = [[NSData alloc] initWithContentsOfFile:imagePath options:NSDataReadingMappedIfSafe error:nil];
        UIImage *image  = [UIImage imageWithData:imageData];
        [imageData release];
        
       // UIImage *image=[UIImage imageWithContentsOfFile:imagePath];
        
        thumbImage = image;
    }
    else //FOR NEW PHOTO
    {
        thumbImage = [self.originalImage fixOrientation];
        thumbImage = [thumbImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(contentSize.width, _targetImageHeight)interpolationQuality:kCGInterpolationDefault]; //235
    }
    
    
    CGSize imageSize = thumbImage.size;
   // CGSize viewSize  = CGSizeMake(contentSize.width, _targetImageHeight);
    CGSize viewSize  = CGSizeMake(contentSize.width-10.0*2, _targetImageHeight);
    CGSize actualSize;
    
    /*
    if (imageSize.width > imageSize.height) 
    {
        actualSize.width = imageSize.width > viewSize.width ? viewSize.width : imageSize.width;
        
        actualSize.height = imageSize.height * actualSize.width / imageSize.width;
    }
    else 
    {
        actualSize.height = imageSize.height > viewSize.height ? viewSize.height : imageSize.height;
        
        actualSize.width = imageSize.width * actualSize.height / imageSize.height;
    }
    */
    
    //========= *********************************************** ==========
    
    CGFloat scaleFactor = (imageSize.width > imageSize.height) ? viewSize.width / imageSize.width : viewSize.height / imageSize.height;
    actualSize.height   = imageSize.height * scaleFactor;
    actualSize.width    = imageSize.width * scaleFactor;
    
    //========= ************************************************ ===========
    
    //actualSize=thumbImage.size;

    imageView = [[UIImageView alloc]init];
    CGRect imageFrame = (actualSize.width>=actualSize.height)?CGRectMake((CGRectGetWidth(scrollView.bounds)-actualSize.width)/2.0, locY, actualSize.width, actualSize.height):CGRectMake((CGRectGetWidth(scrollView.bounds)-contentSize.width-20.0)/2.0, locY, contentSize.width-20.0, _targetImageHeight);
    imageView.frame=imageFrame;
    imageView.backgroundColor=_contentBGColor;
    imageView.contentMode=UIViewContentModeScaleAspectFit;
    imageView.autoresizingMask =UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
    [scrollView addSubview:imageView];
    imageView.layer.borderColor=_contentBorderColor.CGColor;
    imageView.layer.borderWidth  = 1.0;
    imageView.layer.cornerRadius = 8.0;
    imageView.layer.masksToBounds = YES;
    imageView.userInteractionEnabled = !isNewPhoto;
    [imageView addInnerShadowWithColor:[UIColor colorWithWhite:0.2 alpha:1]];
    
    if(!isNewPhoto)
    {
      UITapGestureRecognizer *tapRecognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
      [imageView addGestureRecognizer:tapRecognizer];
      [tapRecognizer release];
    }
    
    
   // imageView.image=[self.thumbImage scaledToSize:imageView.bounds.size];
    
    locY=CGRectGetMaxY(imageView.frame)+10.0;
    
    UIView *textViewContainerview=[[UIView alloc]initWithFrame:CGRectMake(20.0, locY,CGRectGetWidth(scrollView.bounds)-40.0, 100)];
    textViewContainerview.backgroundColor=_contentBGColor;
    textViewContainerview.layer.borderWidth  = 1.0;
    textViewContainerview.layer.cornerRadius = 8.0;
    textViewContainerview.layer.borderColor  = _contentBorderColor.CGColor;
    textViewContainerview.autoresizingMask   =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
    [scrollView addSubview:textViewContainerview];
    
    captionTextView=[[UITextView alloc] initWithFrame:CGRectMake(5.0, 5.0,CGRectGetWidth(textViewContainerview.bounds)-10.0, CGRectGetHeight(textViewContainerview.bounds)-10.0)];
    captionTextView.font = [UIFont grotesqueFontOfSize:15.0];
    //captionTextView.contentInset=UIEdgeInsetsMake(10.0, 8.0, 10.0, 8.0);
    captionTextView.autocorrectionType = UITextAutocorrectionTypeNo;
    captionTextView.showsHorizontalScrollIndicator= NO;
    captionTextView.bounces = NO;
    captionTextView.backgroundColor = [UIColor clearColor];
    captionTextView.delegate=self;
    captionTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
    [textViewContainerview addSubview:captionTextView];
    [textViewContainerview addInnerShadowWithColor:[UIColor colorWithWhite:0.2 alpha:1]];
    [textViewContainerview release];
    captionTextView.editable = isEditMode;
    
    locY=CGRectGetMaxY(textViewContainerview.frame)+10.0;
    
    UIView *oemContainerview=[[UIView alloc]initWithFrame:CGRectMake(20.0, locY, _bounds.size.width-40.0, 50)];
    oemContainerview.backgroundColor=_contentBGColor;
    oemContainerview.layer.borderWidth  = 1.0;
    oemContainerview.layer.cornerRadius = 8.0;
    oemContainerview.layer.borderColor  = _contentBorderColor.CGColor;
    oemContainerview.autoresizingMask   =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
    [oemContainerview addInnerShadowWithColor:[UIColor colorWithWhite:0.2 alpha:1]];
    //[oemContainerview addInnerShadowToTop:YES toLeft:YES];
    
    /*
    UIView *shadowLayer = [[UIView alloc] initWithFrame:CGRectInset(oemContainerview.frame, 2.0, 2.0)];
    shadowLayer.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowLayer.bounds].CGPath;
    shadowLayer.layer.shadowColor = [UIColor blackColor].CGColor;
    // shadowLayer.layer.shadowRadius = 8.0;
    shadowLayer.layer.shadowOffset = CGSizeMake(-15, 20);

    //shadowLayer.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    [scrollView addSubview:shadowLayer];
    */
    
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(10.0, (CGRectGetHeight(oemContainerview.frame)-20)/2.0, 40, 20)];
    titleLabel.text=@"OEM";
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor=[UIColor blackColor];
    titleLabel.font=[UIFont grotesqueBoldFontOfSize:14.0];
    [oemContainerview addSubview:titleLabel];
    [titleLabel release];
    
    selectOemButton=[UIButton buttonWithType:UIButtonTypeCustom];
    selectOemButton.frame=CGRectMake(CGRectGetMaxX(titleLabel.frame), 0, CGRectGetWidth(oemContainerview.frame)-CGRectGetMaxX(titleLabel.frame)-10.0, CGRectGetHeight(oemContainerview.frame));
    selectOemButton.backgroundColor = [UIColor clearColor];
    [selectOemButton setTitle:self.visitPhoto.oemName.length?self.visitPhoto.oemName:@"Select OEM" forState:UIControlStateNormal];
    //selectOemButton.titleLabel.text = self.visitPhoto.oemName.length?self.visitPhoto.oemName:@"Select OEM";
    selectOemButton.titleLabel.font = [UIFont grotesqueFontOfSize:14.0];
    [selectOemButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    selectOemButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentRight; 
    selectOemButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [selectOemButton addTarget:self action:@selector(selectOEMAction:) forControlEvents:UIControlEventTouchUpInside];
    [oemContainerview addSubview:selectOemButton];
    selectOemButton.enabled = isEditMode;
    

    [scrollView addSubview:oemContainerview];
    [oemContainerview release];
    
    locY=CGRectGetMaxY(oemContainerview.frame);
    
    //if(!isSaved)
    {
        /*
        UIButton *confirmButton=[UIButton buttonWithType:UIButtonTypeCustom];
        confirmButton.frame=CGRectMake(2.0, locY+10.0, CGRectGetWidth(oemContainerview.frame)-4.0, 44);
        [confirmButton setBackgroundImage:[UIImage imageNamed:@"confirm_button.png"] forState:UIControlStateNormal];
        //[confirmButton setTitle:@"CONFIRM" forState:UIControlStateNormal];
        [confirmButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [confirmButton addTarget:self action:@selector(confirmAction:) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:confirmButton];
        confirmButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
         */
        /*
        if(!isNewPhoto)
        {
            UIImage *deleteButtonImage = [UIImage imageNamed:@"red_button.png"];
            UIButton *deleteButton=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(25.0, locY+10.0,302.0,47.0) bgImage:@"red_button.png" titleColor:nil target:self action:@selector(deleteAction:)];
            [deleteButton setTitle:@"DELETE THIS PHOTO" forState:UIControlStateNormal];
            deleteButton.titleLabel.textColor = [UIColor whiteColor];
            deleteButton.titleLabel.font = [UIFont subaruBoldFontOfSize:22.0];
            deleteButton.titleLabel.shadowColor = [UIColor colorWithWhite:0.1 alpha:0.5];
            deleteButton.titleLabel.shadowOffset = CGSizeMake(1.0, 1.0);
           // deleteButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            deleteButton.autoresizingMask =  UIViewAutoresizingNone;
            [deleteButton setBackgroundImage:deleteButtonImage forState:UIControlStateNormal];
            [scrollView addSubview:deleteButton];
            
            locY=CGRectGetMaxY(deleteButton.frame);
        }
        */
        
       // UIImage *cancelButtonImage = [UIImage imageNamed:@"cancel_popover_button.png"];
        UIButton *cancelButton = [UIButton repToolCancelButton];
        //[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(10.0, locY+10.0,cancelButtonImage.size.width,cancelButtonImage.size.height) bgImage:@"cancel_popover_button.png" titleColor:nil target:self action:@selector(cancelAction:)];
        CGRect cancelFrame = cancelButton.frame;
        cancelFrame.origin.y = locY+10.0f;
        cancelFrame.origin.x = 10.0f;
        cancelButton.frame = cancelFrame;
        cancelButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
        cancelButton.tag=1;
       // [cancelButton setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
        [cancelButton setTitle:isNewPhoto?@"CANCEL" : @"DELETE" forState:UIControlStateNormal];
        [scrollView addSubview:cancelButton];
        cancelButton.autoresizingMask =UIViewAutoresizingFlexibleRightMargin;
        [cancelButton addTarget:self action:isNewPhoto? @selector(cancelAction:) : @selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
        cancelButton.enabled = isEditMode;
        
        
        UIButton  *saveButton=[UIButton repToolButtonWithText:@"SAVE"];
        CGRect saveFrame = saveButton.frame;
        saveFrame.origin.x = CGRectGetWidth(scrollView.bounds)-saveFrame.size.width-10.0;
        saveFrame.origin.y = CGRectGetMinY(cancelButton.frame);
        saveButton.frame = saveFrame;
        [saveButton addTarget:self action:@selector(confirmAction:) forControlEvents:UIControlEventTouchUpInside];
        saveButton.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin;
        saveButton.tag=2;
        [scrollView addSubview:saveButton];
        saveButton.enabled = isEditMode;
         //saveButton.autoresizingMask =  UIViewAutoresizingFlexibleRightMargin;
        
        locY=CGRectGetMaxY(cancelButton.frame)+10.0;
    }
    
    scrollView.contentSize=CGSizeMake(scrollView.contentSize.width, locY );
                             
    NSString *photoCaption=self.visitPhoto.caption;
    //[self.photoDict valueForKey:@"CAPTION"];
                         
    imageView.image=thumbImage;
    captionTextView.text=photoCaption;
    
    //self.navigationItem.rightBarButtonItem.enabled = isEditMode;
    
    [self reload];
}

#pragma mark -
#pragma mark MANAGE KEYBOARD
-(void)keyboardDidAppear:(NSNotification *)aNotification
{
    if(self.popoverController) return;
    
    CGRect keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
    
    //if(UIEdgeInsetsEqualToEdgeInsets(tableViewContentInsets, UIEdgeInsetsZero))
    //  tableViewContentInsets = self.tableView.contentInset;
    
    UIEdgeInsets _tableInset = scrollView.contentInset;
    _tableInset.bottom = keyboardRect.size.height;
    scrollView.contentInset=_tableInset;
    
    CGPoint scrollToOffset=captionTextView.superview.frame.origin;
    scrollToOffset.x= scrollView.contentOffset.x;
    [scrollView setContentOffset:scrollToOffset animated:YES];
    //[scrollView scrollRectToVisible:captionTextView.superview.frame animated:YES];
}

-(void)keyboardDidDismiss:(NSNotification *)aNotification
{
    if(self.popoverController) return;
    
    if(self.oemPopover && self.oemPopover.isPopoverVisible)
    {
        [self.oemPopover dismissPopoverAnimated:YES];
        
       // [self.oemPopover presentPopoverFromRect:selectOemButton.frame inView:selectOemButton.superview permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        
      scrollView.contentInset=UIEdgeInsetsMake(0, 0, 0, 0);
        
    }];
}

#pragma mark -

-(void)deleteAction:(UIButton *)sender
{
    //sender.userInteractionEnabled = NO;
    if(self.visitPhoto)
    {
       UIAlertView *alert = [UIAlertView alertViewWithTitle:@"" message:PHOTO_REMOVAL_DELETION_IMAGE cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObject:@"Yes"] onDismiss:^(int buttonIndex) 
        {
            [SSCoreDataManager deleteObject:self.visitPhoto];
            [self.navigationController popViewControllerAnimated:YES];
            if(self.deleteAction) self.deleteAction();
        }
        onCancel:^{
            
        }];
       
        [alert release];
    }
}

-(void)confirmAction:(UIButton *)sender
{
    //[self.photoDict setValue:captionTextView.text forKey:@"CAPTION"];
    self.visitPhoto.caption=captionTextView.text;
    
    if(self.originalImage)
    {
      // NSString *fileName       = [[NSDate date] stringFromDateWithFormat:@"yyMMddhhmmssSS"];
       NSString *fileName       = [app globalString];
       NSString *imagePath      = [DirectoryManager imageDirectoryPath];
       NSString *thumbImagePath = [DirectoryManager thumbImageDirectoryPath]; 
        
       // imagePath=[imagePath stringByAppendingFormat:@"/%@.png",fileName];
       // thumbImagePath=[thumbImagePath stringByAppendingFormat:@"/%@_thumb.png",fileName];
        
        imagePath=[imagePath stringByAppendingFormat:@"/%@.jpg",fileName];
        
        thumbImagePath=[thumbImagePath stringByAppendingFormat:@"/%@_thumb.jpg",fileName];
        
        //ADD GCD HERE
        
        //ADD UIIndicatorView here
       ProgressHUD *progressIndicator = [ProgressHUD showHUDAddedTo:self.view animated:YES];
        progressIndicator.labelText = @"Saving...";
        

        sender.enabled=NO;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSError *error=nil;
            
            UIImage *_originalImage = [self.originalImage fixOrientation];
            
            _originalImage=[_originalImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:ORIGINAL_IMAGE_COMPRESSED_SIZE interpolationQuality:kCGInterpolationDefault];
            
            NSLog(@"Original: w=%f H=%f",_originalImage.size.width,_originalImage.size.height);
            
           // NSData *imageData=UIImagePNGRepresentation(_originalImage); //self.originalImage
            NSData *imageData=UIImageJPEGRepresentation(_originalImage, 0.8);
            BOOL written=imagePath?[imageData  writeToFile:imagePath options:NSDataWritingFileProtectionNone error:&error]:NO;
            
            if(written)
            {
                //imageData = UIImagePNGRepresentation(imageView.image);
                imageData = UIImageJPEGRepresentation(imageView.image, 0.8);
                written   = [imageData  writeToFile:thumbImagePath atomically:YES];
            }
            
            NSLog(@"error: %@",[error description]);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(written)
                {
                    //===================== REMOVE PREVIOUS IMAGES ===================
                    NSFileManager *fileManager=[NSFileManager defaultManager];
                    NSString *_imagePath = self.visitPhoto.imagePath;
                    if(_imagePath.length && [fileManager fileExistsAtPath:_imagePath])
                    {
                        [fileManager removeItemAtPath:_imagePath error:nil];
                    }
                    
                    NSString *_thumbImagePath = self.visitPhoto.thumbImagePath;
                    if(_thumbImagePath.length && [fileManager fileExistsAtPath:_thumbImagePath])
                    {
                        [fileManager removeItemAtPath:_thumbImagePath error:nil];
                    }
                    //================================================================
                    
                    self.visitPhoto.imagePath      = imagePath;
                    self.visitPhoto.thumbImagePath = thumbImagePath;
                    //[self.photoDict setValue:imagePath forKey:@"THUMB_PATH"];
                }
                
                
                 NSError *error=nil;
                 [[SSCoreDataManager sharedManager] save:error];
                [ProgressHUD hideHUDForView:self.view animated:YES];
                
                 if(self.confirmAction) self.confirmAction();
                
                self.confirmAction = nil;
            });
            
        });
        
    }
    else 
    {
        NSError *error=nil;
        [[SSCoreDataManager sharedManager] save:error];
        if(self.confirmAction) self.confirmAction();
        
        self.confirmAction = nil;
    }
    
}

-(void)cancelAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    [self.popoverController dismissPopoverAnimated:YES];
    
    if(self.cancelAction) self.cancelAction();
}

-(void)retakeAction:(id)sender
{
    TakePhotoViewController *takePhotoVc=[[TakePhotoViewController alloc] init];
    takePhotoVc.allowCaptionEntry = NO;
    
    [takePhotoVc addApproveActionCallback:^(UIImage *image) 
     {
         if(image) self.originalImage = image;
         
         CGSize contentSize = self.contentSizeForViewInPopover;
         contentSize.width = 352.0;
          CGFloat _targetImageHeight = 247.0;
         
         UIImage  *thumbImage = [self.originalImage fixOrientation];
         thumbImage = [thumbImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(contentSize.width, _targetImageHeight)interpolationQuality:kCGInterpolationDefault];
         imageView.image =thumbImage;

         [takePhotoVc.navigationController popViewControllerAnimated:YES];
     }];
    
    UINavigationController *navcontroller = [self nextResponderNavigationController];
    [navcontroller pushViewController:takePhotoVc animated:YES];
    [takePhotoVc release];
    
    if(self.retakeAction) self.retakeAction();
}

-(void)addRetakeActionCallback:(RetakeAction)callback
{
    if(callback) self.retakeAction=callback;
}

-(void)addConfirmActionCallback:(ConfirmAction)callback
{
    if(callback) self.confirmAction=callback;
}
-(void)addCancelActionCallback:(CancelBlock)callback
{
    if(callback) self.cancelAction = callback;
}
-(void)addDeleteActionCallback:(DeleteAction)callback
{
    if(callback) self.deleteAction = callback;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    if(self.popoverController)
    {
       [self.navigationController setNavigationBarHidden:YES animated:NO];
       // self.navigationItem.leftBarButtonItem.customView.hidden = YES;
    }
    else 
    {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
  //  [self addBackButtonWithSelector:@selector(cancelAction:)];
    self.contentSizeForViewInPopover = CGSizeMake(352.0,MIN(scrollView.contentSize.height,498.5));

    
    //============= ADDED TO SOLVE UIBarButtonItem BUG on iOS======================//
    [self performSelector:@selector(addBackButton) withObject:nil afterDelay:0.4];
     
}

-(void)addBackButton
{
  [self addBackButtonWithSelector:@selector(cancelAction:)];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if(self.oemPopover.isPopoverVisible) [self.oemPopover dismissPopoverAnimated:NO];
       
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [scrollView setContentOffset:CGPointMake(0, 260) animated:YES];
}

-(void)textViewDidChange:(UITextView *)textView
{

}

#pragma mark SELECT OEM ACTION
-(void)selectOEMAction:(UIButton *)sender
{
    //self.visitPhoto.parentVisit.parentCustomer.dealingOEMs;
    
    OEMListViewController *oemListViewController = [[OEMListViewController alloc] initWithOEMID:[self.visitPhoto.oemID integerValue] andName:self.visitPhoto.oemName];
    
    __block AddPhotoCaptionViewController *weakSelf = self;
    
    [oemListViewController addDidSelectOEMCallback:^(NSString *oemId, NSString *oemName)
     {
         AddPhotoCaptionViewController *weakStrong = weakSelf;
         
         
         weakStrong.visitPhoto.oemID = [NSNumber numberWithInt:[oemId intValue]];
         weakStrong.visitPhoto.oemName = oemName;
         [weakStrong->selectOemButton setTitle:(oemName) ? oemName : @"Select OEM" forState:UIControlStateNormal];
         [weakStrong.oemPopover dismissPopoverAnimated:YES];
         weakStrong.oemPopover = nil;
        
     }];

    UIPopoverController *popover = self.oemPopover;
    
    if(!popover)
    {
      popover = [[UIPopoverController alloc] initWithContentViewController:oemListViewController];
      popover.popoverContentSize = CGSizeMake(320.0, 100);
      self.oemPopover = popover;
      [popover release];
    }
    else
    {
        popover.contentViewController = oemListViewController;
    }
        
    [self.oemPopover presentPopoverFromRect:sender.frame inView:sender.superview permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    
    [oemListViewController release];
}

-(void)tapGestureAction:(UITapGestureRecognizer *)recognizer
{
    if(isNewPhoto) return;
    
    CustomerFullImageViewController *_fullImage = [[CustomerFullImageViewController alloc]initWithCustomerVisitPhotos:self.visitPhotos selectedIndex:self.selectedPhotoIndex];
    
    // UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:_fullImage];
   //  [app.splitViewController presentModalViewController:navController animated:YES];
    
    UINavigationController *navController=[self nextResponderNavigationController];
    [navController pushViewController:_fullImage animated:YES];
   

  //  [navController release];
    [_fullImage release];
    
}

#pragma mark Email Photo
-(void)emailPhotoAction
{
    NSString *mailSubject =  @"";
    NSString *mailBody    =  @"";
    
    NSLog(@"mailbody = %@", mailBody);
    
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate=self;
    picker.navigationBar.tintColor=[UIColor blackColor];
    [picker setSubject:mailSubject];
    [picker setMessageBody:mailBody isHTML:NO];
    
    NSString *emailID = self.visitPhoto.parentVisit.parentCustomer.email;
    
    if(emailID.length)
    {
      [picker setToRecipients:[NSArray arrayWithObject:emailID]];
    }
    else 
    {
        [UIAlertView showWarningAlertWithTitle:@"" message:[NSString stringWithFormat:OFFER_EMAIL_ALERT_FORMAT,self.visitPhoto.parentVisit.parentCustomer.name]];
    }
    
    UIImage *image = imageView.image;
    
    NSString *mimeType = [[self.visitPhoto.imagePath lastPathComponent] pathExtension];
    NSData *imageDate = (mimeType && [mimeType isEqualToString:@"png"])?  UIImagePNGRepresentation(image) : UIImageJPEGRepresentation(image, 1.0);
    
    NSString *attachedFileName = [self.visitPhoto.imagePath lastPathComponent];
    [picker addAttachmentData:imageDate mimeType:[NSString stringWithFormat:@"image/%@",mimeType] fileName:attachedFileName.length?attachedFileName:@"offerImage"];
    
    
    [self presentModalViewController:picker animated:YES];
    [picker release];   

}

- (void)mailComposeController:(MFMailComposeViewController*)controller
		  didFinishWithResult:(MFMailComposeResult)result
						error:(NSError*)error 
{
	if (result == MFMailComposeResultSent) 
    {
        BOOL _isNetworkAvailable = [[OfflineManager sharedManager] isNetworkAvailable];
		NSString *success = _isNetworkAvailable? @" Sent Successfully" : @"Email will be sent when you are online";
		UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Email Status:"
                                                         message:success
                                                        delegate:self
                                               cancelButtonTitle:nil
                                               otherButtonTitles:@"Ok",nil];
		[alert1 show];
        [alert1 release],alert1=nil;
		
	}
    else if (result == MFMailComposeResultFailed) 
    {
		NSString *msg = @"Failed";
		UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Email Status:"
                                                         message:msg
                                                        delegate:self
                                               cancelButtonTitle:nil
                                               otherButtonTitles:@"Ok",nil];
		[alert1 show];
        [alert1 release],alert1=nil;
		
	}
    
    else if (result == MFMailComposeResultCancelled) 
    {
        /*
         NSString *msg = @"Cancelled";
         UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Email Status:"
         message:msg
         delegate:self
         cancelButtonTitle:nil
         otherButtonTitles:@"Ok",nil];
         [alert1 show];
         [alert1 release],alert1=nil;
         */
	}
	
	
	[self dismissModalViewControllerAnimated:YES];
}




#pragma mark -
- (void)viewDidUnload
{
    [super viewDidUnload];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
       
    
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
