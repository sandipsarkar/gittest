//
//  TakePhotoViewController.h
//  TestSoundNote
//
//  Created by RANDEM MAC on 21/06/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^BackActionCallback) (void);
typedef void (^ApproveActionCallback) (UIImage *image);

@class CaptureSessionManager,Visit;

@protocol TakePhotoDelegate;
@interface TakePhotoViewController : UIViewController
{
    UIImageView *capturedImageView;
    UIButton *denyButton;
    UIButton *approveButton;
    UIButton *captureButton;
    
    BOOL isImageAvailable;
    BOOL isSupportedCamera;
    
    UIView *cameraView;
    
    BOOL isImageCaptured;
}

@property(nonatomic,retain) NSMutableArray *photos;
@property(nonatomic,retain) Visit *visit;
@property(nonatomic,assign) BOOL allowCaptionEntry;
@property(nonatomic,assign) id <TakePhotoDelegate> delegate;
@property(nonatomic,assign) BOOL isLaunchedFromCustomerSection;

-(id)initWithPhotos:(NSMutableArray *)aPhotos;

-(void)addBackActionCallback:(BackActionCallback)callback;
-(void)addApproveActionCallback:(ApproveActionCallback)callback;

//============= Overridable by subclass ==================
-(void)approveAction:(UIButton *)sender;

@end

@protocol TakePhotoDelegate <NSObject>

@optional

-(void)takePhotoViewController:(TakePhotoViewController*)takePhotoVC didCaptureImage:(UIImage *)image;
-(void)takePhotoViewControllerDidBack:(TakePhotoViewController*)takePhotoVC;

@end
