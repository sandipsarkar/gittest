//
//  AddPhotoCaptionViewController.h
//  TestSoundNote
//
//  Created by RANDEM MAC on 21/06/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <QuartzCore/QuartzCore.h>

typedef void(^RetakeAction)(void);
typedef void(^ConfirmAction)(void);
typedef void(^CancelAction)(void);
typedef void(^DeleteAction)(void);

@class VisitPhoto;
@interface AddPhotoCaptionViewController : UIViewController
{
    UIImageView *imageView;
    
    UITextView *captionTextView;
    
    UIScrollView *scrollView;
    
    BOOL isSaved;
    
    BOOL _isVisitCompleted;
    BOOL _isReportPublished;
}


@property(nonatomic,retain) VisitPhoto *visitPhoto;

@property(nonatomic,retain) UIImage *originalImage;
@property (nonatomic ,assign) BOOL isNewPhoto;
@property(nonatomic,assign) BOOL isEditMode;

@property(nonatomic,assign) NSMutableArray *visitPhotos;
@property(nonatomic,assign) NSUInteger selectedPhotoIndex;


- (id)initWithVisitPhoto:(VisitPhoto *)_photo;

-(void)addRetakeActionCallback:(RetakeAction)callback;
-(void)addConfirmActionCallback:(ConfirmAction)callback;
-(void)addCancelActionCallback:(CancelBlock)callback;
-(void)addDeleteActionCallback:(DeleteAction)callback;

-(void)reload;

@end
