//
//  CustomSegmentedControl.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 18/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomSegmentedControl;

typedef void(^DidSelectSegmentedControl)( CustomSegmentedControl * control,int index);

@interface CustomSegmentedControl : UIView

@property(nonatomic,retain)NSArray *items;
@property(nonatomic,assign) int selectedSegmentIndex;

-(void)addDidSelectSegmentedControlCallback:(DidSelectSegmentedControl)callback;

- (id)initWithFrame:(CGRect)frame items:(NSArray *)aItems;

@end
