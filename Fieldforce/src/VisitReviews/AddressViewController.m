//
//  AddressViewController.m
//  TestSuburb
//
//  Created by Subhojit Dey on 11/28/12.
//  Copyright (c) 2012 Subhojit Dey. All rights reserved.
//

#import "AddressViewController.h"
//#import "NSMutableArray+DB.h"

#define  ENABLE_LOCAL_STORAGE 1

#define TAG_NO_DATA_FOUND					@"Not Found."
#define NSBUNDLE_DB_FILE_NAME				@"suburb.plist"
#define UPDATE_SUBURBS_NOTIFICATION			@"UpdateSuburbsNotification"
#define TAG_DATA							@"data"
#define TAG_SUBURBS							@"suburbs"
#define DOC_DIRECTORY_NAME					@"Reserved"

/*BOOL     sVIsNext   = YES;	//Is Next Enabled Or Not
static int sVPageNo = 0;		//Page Number
static int sVStartIndex = 0;		//Starting Index Number
static int sVEndIndex = 0;		//Ending Index Number
static int sVPageSize = 0;	*/	//Each Page Size

#define suburbCellHeight 75.0f

static int _timerTick=0;
#define MAX_TICK_COUNT 2

#define SECTION_HEADER_HEIGHT 0.0


@interface AddressViewController (Private)
- (void) suggestiveSearchForText :(NSString *)searchText;
//Operations
- (NSOperationQueue *) startOperationQueue;
- (void) cancelLastOperation;
- (void) cancelAllSearchOperations;
- (void) oneOperationFinished;
//Operations
- (void) reloadTableDataSource;

//New functions added
-(void)loadAllSuburbs;
- (NSMutableArray *)readNewSuburbsPlist;
- (NSMutableArray *)readLocalSuburbsPlist;
-(void)saveToLocalFile;

@end
//Soumya

@implementation AddressViewController
//Soumya
@synthesize headerView,currentSearchkey;
@synthesize activeDbOutlets;
@synthesize suburbTableView, suburbTableNoRecordLbl;
@synthesize addressSelectedCallback;
@synthesize dbDict;
@synthesize searchValues,operationQue;
@synthesize filteredSearchContent;
@synthesize delegate;

#pragma mark creates and returns NSOperationQueue object

#pragma mark - New fuction added by SANDIP
-(void)loadAllSuburbs
{
    /*
    STEPS:
    1. FIRST CHECK & LOAD FROM LOCAL FILE.
    2. IF LOCAL FILE IS NOT AVAILABLE , LOAD  PREPOPULATED DATA FROM BUNDLE.
    3. CHECK AND UPDATE SUBURB LIST IF AVAILABLE.
    4. IF UPDATED WRITE TO LOCAL FILE.
    */
    
   
    NSMutableArray *subArr = [self readLocalSuburbsPlist];  //STEP 1 :
    
    if(![subArr count])   //STEP 2 :
    {
        /*
        NSDictionary *dict = [self readBundlePlistNamed:NSBUNDLE_DB_FILE_NAME];
        if(dict)
        {
            NSString *path = [NSString stringWithFormat:@"%@.%@",TAG_DATA, TAG_SUBURBS];
            subArr = [dict valueForKeyPath:path];
        }
        */
        
        subArr = [self readBundlePlistNamed:NSBUNDLE_DB_FILE_NAME];
        
    }
    
    self.searchValues = subArr;
    
    
    if([self checkAndUpdateSuburbs])  //STEP 3 :
     {
        
        [self saveToLocalFile];      //STEP 4 :
     }
    
}

- (BOOL)checkAndUpdateSuburbs
{
    
    BOOL isAnyUpdated = NO;
    
    //------------------ ADDED BY SANDIP ON 6th Dec, 2013 ---------------------------
	NSMutableArray *subArr = self.searchValues;
    
    //LOAD NEW SUBUBS HERE
    NSArray *newSubArr = [self readNewSuburbsPlist];
    
    
    //CHECK THIS INTO PREPUPULATED subArr
    for(NSDictionary *newsuburb in newSubArr)
    {
        NSString *name      = [newsuburb valueForKey:TAG_NAME];
        NSString *postcode  = [newsuburb valueForKey:TAG_SUBURB_PINCODE];
        NSString *state     = [newsuburb valueForKey:TAG_STATE];
        
       // NSNumber *suburbID  = [newsuburb valueForKey:TAG_SUBURB_ID];
        
        NSNumber *isDeleted = [newsuburb valueForKey:@"isDeleted"];
        
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"%K matches[c] %@ AND (%K =%@ AND %K matches[c] %@)",TAG_NAME,name,TAG_SUBURB_PINCODE,postcode,TAG_STATE,state];
        
        //NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"%K = %@",TAG_SUBURB_ID,suburbID];
        
        NSArray *searchArr =  [subArr filteredArrayUsingPredicate:searchPredicate];
        
        NSDictionary *suburbDict = [searchArr count] ? [searchArr objectAtIndex:0] : nil;
        
         if(suburbDict && [isDeleted boolValue])
         {
             [subArr removeObject:suburbDict];
         }
         else if(suburbDict) //UPDATE
          {
             // NSInteger _index = [subArr indexOfObject:suburbDict];
            
               [suburbDict setValue:name forKey:TAG_NAME];
               [suburbDict setValue:postcode forKey:TAG_SUBURB_PINCODE];
               [suburbDict setValue:state forKey:TAG_STATE];
        
            // [subArr replaceObjectAtIndex:_index withObject:suburbDict];
          }
          else // INSERT
          {
               [subArr addObject:newsuburb];
          }
        
        if(!isAnyUpdated) isAnyUpdated = YES;
    }
    
    
    return isAnyUpdated;
}

- (NSMutableArray *)readLocalSuburbsPlist
{
	NSString *filePath = [DirectoryManager suburbsPath];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:filePath]) return nil;
    
	NSMutableArray *arr = [NSMutableArray arrayWithContentsOfFile:filePath];
	return [arr count]? arr: nil;
}

- (NSMutableArray *)readNewSuburbsPlist
{
    NSString *filePath = [DirectoryManager newSuburbsPath];
    
     if(![[NSFileManager defaultManager] fileExistsAtPath:filePath]) return nil;
    
	NSMutableArray *newSuburbs = [NSMutableArray arrayWithContentsOfFile:filePath];
	return [newSuburbs count]? newSuburbs: nil;
}

-(void)saveToLocalFile
{
    NSString *newSuburbsFilePath = [DirectoryManager newSuburbsPath];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:newSuburbsFilePath]) return;
    
    NSString *filePath = [DirectoryManager suburbsPath];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
       // @autoreleasepool
        {
            
          BOOL success =  [self.searchValues writeToFile:filePath atomically:NO];
        
           if(success)
          {
              [[NSFileManager defaultManager] removeItemAtPath:newSuburbsFilePath error:nil];
          }

        }
    });
    
}

#pragma mark -


- (void) readNSBundleDBfile
{
    self.dbDict = [self readBundlePlistNamed:NSBUNDLE_DB_FILE_NAME];
    
	if(dbDict!= nil) //Read from NSBunble folder
	{
		[self populateSuburb];
	}
	else
	{
       
	}
}




- (void) populateSuburb
{
	NSString *path = [NSString stringWithFormat:@"%@.%@",TAG_DATA, TAG_SUBURBS];

	NSMutableArray *subArr = [self.dbDict valueForKeyPath:path];
    
    
    //------------------ ADDED BY SANDIP ON 6th Dec, 2013 ---------------------------
    //LOAD NEW SUBUBS HERE
    NSArray *newSubArr = [self readNewSuburbsPlist];
    
    //CHECK THIS INTO PREPUPULATED subArr
    for(NSDictionary *newsuburb in newSubArr)
    {
        NSString *name = [newsuburb valueForKey:TAG_NAME];
        NSString *postcode = [newsuburb valueForKey:TAG_SUBURB_PINCODE];
        NSString *state = [newsuburb valueForKey:TAG_STATE];
        
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"%K matches[c] %@ AND (%K =%@ AND %K matches[c] %@)",TAG_NAME,name,TAG_SUBURB_PINCODE,postcode,TAG_STATE,state];
        
         NSArray *searchArr =  [subArr filteredArrayUsingPredicate:searchPredicate];
        
        NSDictionary *suburbDict = [searchArr count] ? [searchArr objectAtIndex:0] : nil;
        
        if(suburbDict) //UPDATE
        {
           // NSInteger _index = [subArr indexOfObject:suburbDict];
            
            [suburbDict setValue:name forKey:TAG_NAME];
            [suburbDict setValue:postcode forKey:TAG_SUBURB_PINCODE];
            [suburbDict setValue:state forKey:TAG_STATE];
            
           // [subArr replaceObjectAtIndex:_index withObject:suburbDict];
        }
        else // INSERT
        {
            [subArr addObject:newsuburb];
        }
    }
    
    //--------------------------------------------------------------------------------
    
	self.searchValues = subArr;
    
}



/*
- (NSMutableDictionary *) readBundlePlistNamed: (NSString *) file
{
	NSString *type  = @"plist";
	NSString *fileName = [file stringByDeletingPathExtension];
	NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:type];
	NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:filePath];
	return [dict count]? dict: nil;
}
*/

- (NSMutableArray *) readBundlePlistNamed: (NSString *) file
{
	NSString *type       = @"plist";
	NSString *fileName   = [file stringByDeletingPathExtension];
	NSString *filePath   = [[NSBundle mainBundle] pathForResource:fileName ofType:type];
    
	NSMutableArray *suburbs = [NSMutableArray arrayWithContentsOfFile:filePath];
    
	return [suburbs count]? suburbs: nil;
}


- (NSString *) documentDirectoryPath
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *appDirectory = [paths objectAtIndex:0];
	appDirectory=[appDirectory stringByAppendingPathComponent:DOC_DIRECTORY_NAME];
	NSFileManager *fileManager = [NSFileManager defaultManager];
    
	if ([fileManager fileExistsAtPath:appDirectory] == NO)
	{
		[fileManager createDirectoryAtPath:appDirectory withIntermediateDirectories:NO attributes:nil error:nil];
	}
	return appDirectory;
}

- (NSMutableArray *) populateOutputWithPredicate: (NSPredicate *) predicate  AndData: (NSMutableArray *) data
{
    //if(DEVELOPER_DEBUG) NSLog(@"Predicate Query format %@",[predicate predicateFormat]);
	NSArray *oArray = nil;
	@try
	{
        NSArray *qArray = [NSArray arrayWithArray: data];
        oArray = [qArray filteredArrayUsingPredicate: predicate];
	}
	@catch (NSException *exception)
	{
		NSLog(@"An error in predicate found as:: %@", [exception reason] );
	}
    return [NSMutableArray arrayWithArray:oArray];
}
//Start a new operation
- (NSOperationQueue *) startOperationQueue
{
	NSOperationQueue *_operationQue=self.operationQue;
	if(_operationQue==nil)
	{
		_operationQue=[[NSOperationQueue alloc] init];
		self.operationQue=_operationQue;
		[_operationQue release];
	}
	[_operationQue setMaxConcurrentOperationCount:2];
	return _operationQue;
}

//Cancel last operation
- (void) cancelLastOperation
{
	@try
	{
		id currentOperation=[[[self startOperationQueue] operations] lastObject]; //Cancel last operation
		if([currentOperation isExecuting]) [currentOperation cancel];
	}
	@catch (NSException * exception)
	{
		NSLog(@"Can not stop last operation for error::%@ with reason::%@",[exception name],[exception reason]);
	}
}

//Cancel all operations
- (void) cancelAllSearchOperations
{
	@try
	{
		[ [self startOperationQueue] cancelAllOperations];
	}
	@catch (NSException *e)
	{
		NSLog(@"Search operation cancellation stopped for error name : %@ and reason %@",[e name],  [e reason]);
	}
}

//One Operation finished
- (void) oneOperationFinished
{
	[self reloadTableDataSource];
}

//Reload table data
- (void) reloadTableDataSource
{
	[self.suburbTableView reloadData];
}

-(void)sortArray
{
    
    [filteredSearchContent sortUsingComparator:^(id obj1, id obj2)
     {
         NSDictionary* s1 = (NSDictionary*)obj1;
         NSDictionary* s2 = (NSDictionary*)obj2;
         
         NSString *name1 = [s1 valueForKey:TAG_NAME];
         NSString *name2 = [s2 valueForKey:TAG_NAME];
         
         return [name1 caseInsensitiveCompare:name2] ;
     }];
    
    /*
    NSSortDescriptor *descriptor1 = [[NSSortDescriptor alloc] initWithKey:TAG_NAME ascending:YES comparator:^NSComparisonResult(id obj1, id obj2) {
        
       
        
        NSString *name1 = (NSString *)obj1;
        NSString *name2 = (NSString *)obj2;
        
        return [name1 caseInsensitiveCompare:name2];
        
        
    }];
    
    NSSortDescriptor *descriptor2 = [[NSSortDescriptor alloc] initWithKey:TAG_NAME ascending:YES comparator:^NSComparisonResult(id obj1, id obj2) {
        
        NSString *p1 = (NSString *)obj1;
        NSString *p2 = (NSString *)obj2;
        return [p1 caseInsensitiveCompare:p2];
        
        
    }];
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects:descriptor1,descriptor2, nil];
    
    [filteredSearchContent sortUsingDescriptors:sortDescriptors];
    */
}


- (void)viewWillAppear:(BOOL)animated
{
    //[self.navigationController.navigationBar addBackGroundImage:@"navigation_bar_blank.png"];
    
	[suburbSearchBar becomeFirstResponder];
	
}

- (void)viewWillDisappear:(BOOL)animated
{
	//Drop keyboard
	[suburbSearchBar resignFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
   if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
  
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self addTitle:@"Select Location" withFont:[UIFont subaruBoldFontOfSize:22.0]];
    
    self.currentSearchkey=nil;
   
    dbDict = [[NSMutableDictionary alloc] init];
    searchValues=[[NSMutableArray alloc]init];

    filteredSearchContent = [[NSMutableArray alloc] init];
    suburbSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 40)];
	suburbSearchBar.delegate = self;
	//[suburbSearchBar becomeFirstResponder];
	suburbSearchBar.placeholder = @"Search..";
    suburbSearchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	[self.view addSubview:suburbSearchBar];
    
	// Table View Creation
    CGRect _tableFrame = CGRectMake(0, 40, self.view.bounds.size.width, self.view.bounds.size.height-40.0);;
    suburbTableView = [[UITableView alloc] initWithFrame:_tableFrame style:UITableViewStylePlain];
    suburbTableView.dataSource = self;
    suburbTableView.delegate   = self;
    suburbTableView.hidden     = NO;
    suburbTableView.bounces    = NO;
    suburbTableView.backgroundColor=[UIColor clearColor];
    //CHANGED
   // self.suburbTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    suburbTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:suburbTableView];

    
	
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
    {
        #if ENABLE_LOCAL_STORAGE
        [self loadAllSuburbs];
        #else
        [self readNSBundleDBfile];
        #endif
        
        NSPredicate *searchPredicate = nil;
        NSMutableArray *arr = nil;
        searchPredicate = [NSPredicate predicateWithFormat:@"(%K BEGINSWITH[cd] %@)",TAG_NAME,@"a"];
        arr=[self populateOutputWithPredicate:searchPredicate AndData:self.searchValues];
        [filteredSearchContent setArray:arr];
        
        if([filteredSearchContent count])
        {
            [self sortArray];
        }

        
    dispatch_async(dispatch_get_main_queue(), ^(void)
    {
       
        [suburbTableView reloadData];
           
    });
        
  });
    
}
#pragma mark UISearchBarDelegate
//============================================================
//						UISearchBarDelegate
//============================================================
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;
{
	[suburbSearchBar resignFirstResponder];
}


///************ By SANDIP **************//
-(void)_invalidTimer
{
	if(_timer) [_timer invalidate];
	_timer=nil;
	_timerTick=0;
}

-(void)_requestForSearch
{
	if(_timer==nil)
		_timer=[NSTimer scheduledTimerWithTimeInterval:0.10 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
}

-(void)timerAction:(NSTimer *)aTimer
{
	_timerTick++;
    
	if(_timerTick>MAX_TICK_COUNT)
    {
		[self _invalidTimer];
		
		[filteredSearchContent removeAllObjects];
		self.suburbTableView.hidden = NO;
        self.suburbTableNoRecordLbl.hidden = YES;
        //[self modifySearchPagingView];
        
        
        if([self.currentSearchkey length]==0)
        {
            self.currentSearchkey=@"A";///ADDED BY SUBHO
        }
        
		if( [self.currentSearchkey length] > 0 && self.currentSearchkey!=nil )	//User has something to search
          {
                [self cancelLastOperation];
				
				[[self startOperationQueue] setSuspended:NO];
				CustomInvocationOperation *operation = [[CustomInvocationOperation alloc]  initWithName:self.currentSearchkey target:self selector:@selector(suggestiveSearchForText:) object:self.currentSearchkey];
				[ [self startOperationQueue] addOperation:operation]; //Add operation to thread
				[operation release];
          }
          else
          {
            
          }
        }
		else
        {
           
        }
    }




- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	self.currentSearchkey=searchText;
	_timerTick=0;
	[self _requestForSearch];
}


//Suggestive search
- (void) suggestiveSearchForText :(NSString *) searchText
{
	if(![self.currentSearchkey isEqualToString:searchText]) return;
    
    
	NSUInteger userSearchCount = [searchText length];
	if( userSearchCount)
    {
		[self cancelLastOperation];	//Cancel last operation

        NSPredicate *searchPredicate = nil;
        NSMutableArray *arr = nil;
		
        searchPredicate = [NSPredicate predicateWithFormat:@"%K BEGINSWITH [cd] %@ OR %K BEGINSWITH %@ ",TAG_NAME, searchText, TAG_SUBURB_PINCODE, searchText ];
        arr=[self populateOutputWithPredicate:searchPredicate AndData:self.searchValues ];
        [filteredSearchContent setArray: arr];
        
        if([filteredSearchContent count])
        {
            [self sortArray];
        }
 
        
        dispatch_async(dispatch_get_main_queue() , ^{
            
            [suburbTableView reloadData];
            suburbTableView.hidden=NO;
        });
    }
}


-(void)operationDidFinished:(CustomInvocationOperation *)aOperation
{
	if([aOperation.name isEqualToString:self.currentSearchkey])
    {
		[self oneOperationFinished];
    }
}

#pragma mark -
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return SECTION_HEADER_HEIGHT;//30.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"BySuburbCell";
    static NSString *CellIdentifier2 = @"Cell_NoData";

    UILabel *lblpin=nil;
    
    UITableViewCell *cell=nil;
    if([filteredSearchContent count]==0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.text = @"No Data Found";
        }
    }
    else{
           cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.detailTextLabel.font = [UIFont grotesqueFontOfSize:15.0];
                cell.textLabel.numberOfLines = 2;
                cell.textLabel.font = [UIFont grotesqueBoldFontOfSize:15.0];
                
                lblpin=[[UILabel alloc]init];
                lblpin.tag=101;
                lblpin.frame=CGRectMake(CGRectGetWidth(cell.contentView.bounds)-100.0, (CGRectGetHeight(cell.contentView.bounds)-25.0)/2.0, 90, 25);
                lblpin.backgroundColor=[UIColor clearColor];
                lblpin.font = [UIFont grotesqueFontOfSize:15.0];
                lblpin.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
                [cell.contentView addSubview:lblpin];
                [lblpin release];
            
            }
            else
            {
                lblpin=(UILabel *)[cell.contentView viewWithTag:101];
            }
            
                if([filteredSearchContent count])
                {
                    NSDictionary *tempDict =indexPath.row<[filteredSearchContent count] ? [filteredSearchContent  objectAtIndex:indexPath.row] : nil;
                    
                    NSString *suburbName = nil;
                    NSString *suburbPincode = nil;
                    NSString *suburbState = nil;
                    ///Suburb wise
                    
                        suburbName = [NSString stringWithFormat:@"%@",  [tempDict valueForKey:TAG_NAME]!=nil ?[tempDict valueForKey:TAG_NAME] : TAG_NO_DATA_FOUND  ];
                    
                        suburbPincode = [NSString stringWithFormat:@"%@",  [tempDict valueForKey:TAG_SUBURB_PINCODE]!=nil ?[tempDict valueForKey:TAG_SUBURB_PINCODE] : TAG_NO_DATA_FOUND  ];
                    
                        suburbState = [NSString stringWithFormat:@"%@",  [tempDict valueForKey:TAG_STATE]!=nil ?[tempDict valueForKey:TAG_STATE] : TAG_NO_DATA_FOUND  ];
                    
                    
                    if([suburbName length]>22)
                    {
                        cell.textLabel.text=[suburbName substringToIndex:22];
                        cell.textLabel.text=[cell.textLabel.text stringByAppendingString:@"..."];
                                             

                    }
                    else
                    {
                         cell.textLabel.text=suburbName;
                    }
                    
                   
                    cell.detailTextLabel.text=suburbPincode;
                    lblpin.text=suburbState;
                    
              
                    
                }
       
    }
   	return cell;
}
//==========================End================================


#pragma mark UITableViewDelegate
//============================================================
//						UITableViewDelegate
//============================================================
- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
   
    
    return MAX(1,[filteredSearchContent count]) ;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return suburbCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if( [filteredSearchContent count] )
	{

		NSMutableDictionary *tempDict = indexPath.row <[filteredSearchContent count] ? [filteredSearchContent  objectAtIndex:indexPath.row] : nil;
        
        if(!tempDict) return;
		
        NSString *suburbName  = [NSString stringWithFormat:@"%@",  [tempDict valueForKey:TAG_NAME]!=nil ?[tempDict valueForKey:TAG_NAME] : TAG_NO_DATA_FOUND  ];
        
        NSString *suburbPincode = [NSString stringWithFormat:@"%@",  [tempDict valueForKey:TAG_SUBURB_PINCODE]!=nil ?[tempDict valueForKey:TAG_SUBURB_PINCODE] : TAG_NO_DATA_FOUND  ];
        
        NSString *suburbState = [NSString stringWithFormat:@"%@",  [tempDict valueForKey:TAG_STATE]!=nil ?[tempDict valueForKey:TAG_STATE] : TAG_NO_DATA_FOUND  ];
        
        if(self.addressSelectedCallback) self.addressSelectedCallback(suburbName,suburbState,suburbPincode,tempDict);
        
        if(self.delegate && [self.delegate respondsToSelector:@selector(addressViewController: didSelectAddress:)])
        {
            NSMutableDictionary *addressDict = [[NSMutableDictionary alloc] initWithDictionary:tempDict];
            [self.delegate addressViewController:self didSelectAddress:addressDict];
            [addressDict release];
        }
    }
}

-(void)addAddressSelectedCallback:(AddressSelectedCallback)aAddressSelectedCallback
{
    if(aAddressSelectedCallback)  self.addressSelectedCallback=aAddressSelectedCallback;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    delegate = nil;
    
    [suburbTableView release];
    suburbTableView=nil;
    
    [filteredSearchContent release];
    filteredSearchContent=nil;
    
    [suburbSearchBar release];
    suburbSearchBar=nil;
    
    [addressSelectedCallback release];
    addressSelectedCallback=nil;
    
    [dbDict release];
    dbDict=nil;
    
    [searchValues release];
    searchValues = nil;
    
   // dispatch_release(backgroundQueue);
    
    [super dealloc];
}
@end
