//
//  VisitTypesViewcontroller.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 18/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "VisitTypesViewcontroller.h"



@interface VisitTypesViewcontroller()
{
    BOOL _isNewVisitSession;
}
@property(nonatomic,retain) NSIndexPath *selectedIndexPath;
@property (nonatomic,copy)  ActionCallBack actionCallback;
@end

@implementation VisitTypesViewcontroller
@synthesize actionCallback,selectedIndexPath;
@synthesize isReadonlyMode;
@synthesize delegate;

-(void)dealloc
{
    delegate = nil;
    if(actionCallback)
    [actionCallback release];
    [selectedIndexPath release];
    [_visitTypes release];
    
    [super dealloc];
}

/*
+(NSArray *)visitTypes
{
    //return [NSArray arrayWithObjects:@"General",@"Cold Call",@"Order",@"Credit",@"Payment/Account",@"Other", nil];
    
    return [NSArray arrayWithObjects:@"General",@"Cold Call",@"Order",@"Credit",@"Payment/Account",@"Delivery",@"Other",nil];
}


+(NSArray *)internalVisitTypes
{
    return [NSArray arrayWithObjects:@"General",@"Cold Call",@"Order",@"Credit",@"Payment/Account",@"Other",@"Delivery", nil];
}
*/

+(NSInteger)indexForSelectedNatureOfVisit:(NSString *)natureOfVisit
{
    if(!natureOfVisit) return NSNotFound;
    
    NSArray *visitTypes = [self visitTypes];
    
   // return [visitTypes indexOfObject:natureOfVisit];
    
    NSUInteger _index =  [visitTypes indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
       
       NSDictionary *_dict = (NSDictionary *)obj;
       
        NSString *name = [_dict valueForKey:@"name"];
        
        BOOL _found = (name && [name isEqualToString:natureOfVisit]);
        
        if(_found)
        {
            *stop = YES;
        }
        
        return _found;
   }];
    
    return _index;
    
}



- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self)
    {
        // Custom initialization
        _isNewVisitSession=YES;
       // _visitTypes=[[NSArray alloc] initWithArray:[VisitTypesViewcontroller visitTypes]];
        
          _visitTypes=[[NSArray alloc] initWithArray:[self _setupData]];
        
        self.title=@"NATURE OF VISIT";
    }
    return self;
    
}

/*
- (id)initWithNatureOfVisit:(NSString *)natureOfVisit
{
    self = [self init];
    if (self)
    {
        // Custom initialization
        
        NSInteger selectedIndex = natureOfVisit.length ?  [_visitTypes indexOfObject:natureOfVisit] : 0;
        self.selectedIndexPath=[NSIndexPath indexPathForRow:selectedIndex inSection:0];
        
    }
    return self;
}
*/

- (id)initWithNatureOfVisitID:(NSInteger )natureOfVisitID
{
    self = [self init];
    if (self)
    {
        // Custom initialization
        _isNewVisitSession=NO;
        NSInteger selectedIndex = (natureOfVisitID <[_visitTypes count] && natureOfVisitID>=0)? natureOfVisitID : NSNotFound;
        self.selectedIndexPath=[NSIndexPath indexPathForRow:selectedIndex inSection:0];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)setupFooterView
{
    /*
    CGSize popoverContentSize=self.contentSizeForViewInPopover;
    UIView *footerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 60)];
    footerView.contentMode=UIViewContentModeCenter;
    
    UIImage *startSessionImage = [UIImage imageNamed:@"start_session_button.png"];
    UIButton *startSessionButton=[UIButton buttonWithTitle:nil type:UIButtonTypeCustom frame:CGRectMake(5.0, 10.0,MIN(310, startSessionImage.size.width), startSessionImage.size.height) bgImage:@"start_session_button.png" titleColor:nil target:self action:@selector(startSessionAction:)];
    startSessionButton.autoresizingMask =  UIViewAutoresizingFlexibleRightMargin;
    [footerView addSubview:startSessionButton];
    self.tableView.tableFooterView=footerView;
    [footerView release];
    */
 
}

-(void)setIsReadonlyMode:(BOOL)_isReadonlyMode
{
    isReadonlyMode=_isReadonlyMode;

    if(!isReadonlyMode) [self setupFooterView];
    else self.tableView.tableFooterView=nil;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif

    /*
    UIBarButtonItem *rightButton=[[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeAction:)];
    self.navigationItem.rightBarButtonItem=rightButton;
    [rightButton release];
     */
    
    [self addTitle:@"NATURE OF VISIT" withFont:[UIFont subaruBoldFontOfSize:24.0]];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.tintColor = LOGIN_BG_COLOR;
    
    self.tableView.showsVerticalScrollIndicator=NO;
    self.tableView.bounces=NO;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 10.0)];
        headerView.backgroundColor = [UIColor clearColor];
        self.tableView.tableHeaderView = headerView;
        [headerView release];
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        if(self.navigationController.viewControllers.count>1)
        [self addBackButtonWithImage:[UIImage imageNamed:NAV_BACK_BUTTON] title:@"Back" selector:nil];
    }

    
    //if(!self.selectedIndexPath)
    // self.selectedIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    
   // if(isReadonlyMode) return;
   // [self setupFooterView];
}

-(void)addNatureOfVisitDidSelectCallback:(ActionCallBack )callback
{
    
    if(callback)
    self.actionCallback=callback;
}

-(void)closeAction:(id)sender
{
    [self.popoverController dismissPopoverAnimated:YES];
}

-(NSInteger)selectedNatureOfVisitID
{
    NSInteger natureOfVisitID = self.selectedIndexPath.row;
    
    switch (self.selectedIndexPath.row)
    {
        case 5:
            natureOfVisitID = NatureOfVisitDelivery-1;
        break;
            
        case 6:
            natureOfVisitID = NatureOfVisitOther-1;
            break;
            
        default:
             natureOfVisitID = self.selectedIndexPath.row;
            break;
    }
    
    return natureOfVisitID;
    
}

#pragma mark - TODO

-(NSArray *)_setupData
{
   // if([_visitTypes count]) return _visitTypes;
    
    NSArray *_natureOfVisitList = [NATURE_OF_VISIT_TEXT componentsSeparatedByString:@","];
    
    
    NSMutableArray *_dataSource = [NSMutableArray array];
    
    NSString *natureOfVisitName = nil;
    NSInteger natureOfVisitID;
    
    for(NSUInteger i=0; i<[_natureOfVisitList count]; i++)
    {
        switch (i)
        {
            case 0:
            {
                
               // natureOfVisitName = @"General";
                natureOfVisitID = NatureOfVisitGeneral;
            }
            break;
                
            case 1:
            {
                //natureOfVisitName = @"Cold Call";
                natureOfVisitID = NatureOfVisitColdCall;
            }
                break;
                
            case 2:
            {
               // natureOfVisitName = @"Order";
                natureOfVisitID = NatureOfVisitOrder;
            }
                break;
                
            case 3:
            {
               // natureOfVisitName = @"Credit";
                natureOfVisitID = NatureOfVisitCredit;
            }
                break;
                
            case 4:
            {
               // natureOfVisitName = @"Payment/Account";
                natureOfVisitID = NatureOfVisitPaymentAccount;
            }
                break;
                
            case 5:
            {
               // natureOfVisitName = @"Delivery";
                natureOfVisitID = NatureOfVisitDelivery;
            }
                break;
                
            case 6:
            {
               // natureOfVisitName = @"Other";
                natureOfVisitID = NatureOfVisitOther;
            }
                break;
                
                
            default:
                break;
        }
        
        natureOfVisitName = [_natureOfVisitList objectAtIndex:i];
        
        natureOfVisitID -= 1;
        
        NSNumber *_natureOfVisitID = [NSNumber numberWithInteger:natureOfVisitID];
        
        NSMutableDictionary *_dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:natureOfVisitName,@"name",_natureOfVisitID,@"ID", nil];
        [_dataSource addObject:_dict];
        [_dict release];
        
    }
    
    return _dataSource;
    
    //_visitTypes = [[NSArray alloc] initWithArray:_dataSource];
}

+(NSString *)nameForNatureOfVisitID:(NSInteger)natureOfVisitID
{
    NSString *natureOfVisitName = nil;
    
    /*
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"self.ID=%@",natureOfVisitID];
    NSArray *_filteredArr = [_visitTypes filteredArrayUsingPredicate:filterPredicate];
    
    for(NSDictionary *_dict in _filteredArr)
    {
        natureOfVisitName =  [_dict valueForKey:@"name"];
        
        break;
    }
    */
    
    NSInteger _natureOfVisitID = natureOfVisitID + 1;
 
    switch (_natureOfVisitID)
    {
        case NatureOfVisitGeneral:
        {
            natureOfVisitName = @"General";
        }
        break;
            
        case NatureOfVisitColdCall:
        {
            natureOfVisitName = @"Cold Call";
        }
        break;
            
        case NatureOfVisitOrder:
        {
            natureOfVisitName = @"Order";
       
        }
        break;
            
        case NatureOfVisitCredit:
        {
            natureOfVisitName = @"Credit";
        }
        break;
            
        case NatureOfVisitPaymentAccount:
        {
            natureOfVisitName = @"Payment/Account";
        }
        break;
            
        case NatureOfVisitDelivery:
        {
            natureOfVisitName = @"Delivery";
        }
        break;
            
        case NatureOfVisitOther:
        {
            natureOfVisitName = @"Other";
        }
        break;
            
            
        default: break;
    }
    
    return natureOfVisitName;
}

#pragma mark -

-(void)startSessionAction:(id)sender
{
    [self.popoverController dismissPopoverAnimated:YES];
    
    //NSInteger _natureOfSelectedVisitID = [self selectedNatureOfVisitID];
    
    NSDictionary *_dict = (self.selectedIndexPath.row < [_visitTypes count]) ? [_visitTypes objectAtIndex:self.selectedIndexPath.row] : nil;
    
    NSInteger _natureOfSelectedVisitID = [[_dict valueForKey:@"ID"] integerValue];
    
    if(self.actionCallback)
    {
        //self.actionCallback(self.selectedIndexPath.row);
        self.actionCallback(_natureOfSelectedVisitID);
    }
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(visitTypesViewcontroller: didSelectNatureOfVisit:)])
    {
        //[self.delegate visitTypesViewcontroller:self didSelectNatureOfVisit:self.selectedIndexPath.row];
        [self.delegate visitTypesViewcontroller:self didSelectNatureOfVisit:_natureOfSelectedVisitID];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    
    self.contentSizeForViewInPopover=CGSizeMake(320.0, self.tableView.contentSize.height+1);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
     self.contentSizeForViewInPopover=CGSizeMake(320.0, self.tableView.contentSize.height);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [_visitTypes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        cell.textLabel.font = [UIFont grotesqueFontOfSize:16.0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor = [UIColor blackColor];
    }
    
    NSDictionary *_dict= (indexPath.row<[_visitTypes count]) ?  [_visitTypes objectAtIndex:indexPath.row] : nil;
    NSString *titleText = [_dict valueForKey:@"name"];
    
  //  NSString *titleText= (indexPath.row<[_visitTypes count]) ?  [_visitTypes objectAtIndex:indexPath.row] : nil;
    
    cell.textLabel.text = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") ? [NSString stringWithFormat:@"  %@",titleText] : titleText;
    
    BOOL isCellSelected=([indexPath isEqual:self.selectedIndexPath]);
    cell.accessoryType=isCellSelected?UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone;
    //cell.textLabel.textColor=isCellSelected?LOGIN_BG_COLOR:[UIColor blackColor];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) return;
    
    cell.backgroundColor= [UIColor groupTableViewBackgroundColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    CellBackgroundView *_bgView =[[CellBackgroundView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.contentView.bounds), CGRectGetHeight(cell.contentView.bounds))];
    _bgView.backgroundColor = [UIColor clearColor];
    _bgView.contentInset = CGPointMake(10.0, 0.0);
    _bgView.cornerRadius = 8.0;
    _bgView.borderColor = self.tableView.separatorColor;
    _bgView.fillColor = [UIColor whiteColor];
    _bgView.tag = 1;
    
    
    int numRows = [tableView numberOfRowsInSection:indexPath.section];
    
    if(numRows==1)
    {
        _bgView.position = CellPositionSingle;
    }
    else
    {
        //	IF LAST ROW THEN ROUND RECT BOTTOM-LEFT AND BOTTOM-RIGHT CORNERS
        
        if(indexPath.row==0)
        {
            _bgView.position = CellPositionTop;
        }
        else if(indexPath.row == numRows-1)
        {
            _bgView.position = CellPositionBottom;
        }
        else
        {
            _bgView.position = CellPositionMiddle ;
        }
    }
    
    
    cell.backgroundView = _bgView;
    
    [_bgView release];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/
/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/
/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/
/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(isReadonlyMode) return;
    
    if([self.selectedIndexPath isEqual:indexPath]) return;
    
    NSMutableArray *indexPathsToReload=[[NSMutableArray alloc]init];
    
    if(self.selectedIndexPath!=nil)
    {
        [indexPathsToReload addObject:self.selectedIndexPath];
    }
        
    [indexPathsToReload addObject:indexPath];
    self.selectedIndexPath=indexPath;
    
    [self.tableView reloadRowsAtIndexPaths:indexPathsToReload withRowAnimation:UITableViewRowAnimationNone];
    
    [indexPathsToReload release];
    
    NSDictionary *_dict = (indexPath.row<[_visitTypes count]) ? [_visitTypes objectAtIndex:indexPath.row] : nil;
    NSInteger _natureOfSelectedVisitID = [[_dict valueForKey:@"ID"] integerValue];
    
    
   // NSInteger _natureOfSelectedVisitID = [self selectedNatureOfVisitID];
    
    
    if(self.actionCallback)
    {
        //self.actionCallback(self.selectedIndexPath.row);
        
        self.actionCallback(_natureOfSelectedVisitID);
    }
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(visitTypesViewcontroller: didSelectNatureOfVisit:)])
    {
        //[self.delegate visitTypesViewcontroller:self didSelectNatureOfVisit:self.selectedIndexPath.row];
        
        [self.delegate visitTypesViewcontroller:self didSelectNatureOfVisit:_natureOfSelectedVisitID];
    }

}

@end
