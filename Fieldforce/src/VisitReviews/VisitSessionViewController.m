//
//  VisitSessionViewController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 10/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "VisitSessionViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "Visit.h"
#import "Customer.h"
#import "Task.h"
#import "VisitTypesViewcontroller.h"
#import "TakePhotoViewController.h"
#import "VisitPhotosViewController.h"
#import "TaskListInVisitSession.h"
#import "CustomerOfferViewController.h"
#import "CustomerOfferInVisitSessionController.h"
#import "CreateTaskViewController.h"
#import "VisitNoteViewController.h"
#import "CustomSegmentedControl.h"
#import "DrawingCanvasView.h"
#import "AudioManager.h"
#import "VisitPhoto.h"
#import "TagSelectionController.h"
#import "TrackEventUIApplication.h"
#import "OfflineManager.h"
#import "ScheduledVisit.h"
#import "LocationManager.h"
#import "DirectoryWatcher.h"

#define MAX_AUDIO_DURATION 60.0*60.0*1

const CGFloat kTextNoteControllerWidth = 632.0;
const CGFloat kSideControllerWidth = 352.0;

//static NSString *recordingDirectory = @"SessionAudio";

@interface VisitSessionViewController ()<TagSelectionDelegate,DirectoryWatcherDelegate>
{
    VisitNoteViewController *_visitNoteViewController;
    DrawingCanvasView *_paintingView;
    TagSelectionController *tagSelectionController;
    CustomerOfferInVisitSessionController *customerOfferViewController;
    TaskListInVisitSession *taskListViewController;
    VisitPhotosViewController *photosViewController;
    
    
    UILabel *durationLeftLabel;
    UILabel *durationLabel;
    UILabel *recordDurationLabel;
    UISlider *slider;
    
    BOOL isForCustomer;
    BOOL isNewSession;
    
    //UIButton *viewOffersButton;
    //UIButton *taskListButton;
    //UIButton *photosButton;
    
    CGFloat locY;
    
    UINavigationController *currentNavController;
    NSInteger _selectedControllerIndex;
    UIImageView *noteControllerBottomBar;
}

@property(nonatomic,retain)Customer *customer;
@property(nonatomic,retain) UIPopoverController *natureOfVisitPopOver;
@property(nonatomic,retain)  DirectoryWatcher *docWatcher;


- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval ;
-(void)_selectControllerForIndex:(NSInteger)index;
-(void)showOffers;
-(void)showTaskList;
-(void)showPhotos;
-(void)tagTextNoteAction;
-(void)deleteTextNoteAction;
-(void)animateEditBottombar;
-(void)manageTagSelectionController:(BOOL)show;

-(NSString *)_tempCombinedFilePath;
-(float)durationOfFileAtPath:(NSString *)soundPath;
-(NSString *)_tempRecordFilePath;
-(NSString *)_recordFilePath;
-(NSString *)_playFilePath;
-(void)uploadVisitDetails:(void (^)(BOOL success))completion;

-(void)stopRecord:(UIButton *)sender onCompletion:(void(^)(void))completion;

@end

@implementation VisitSessionViewController
@synthesize customer;
@synthesize visit;
@synthesize audioManager=_audioManager;
@synthesize natureOfVisitPopOver;
@synthesize isReportCompleted;
@synthesize isVisitCompleted;
@synthesize lastLocalNotificationDate;

@synthesize isFromCalendar;
@synthesize docWatcher;

-(id)initWithVisit:(Visit *)aVisit forCustomer:(Customer *)aCustomer
{
    self=[super init];
    
    if(self)
    {
        self.visit=aVisit;
        self.customer=aCustomer;
        
        if(!self.visit.parentCustomer && aCustomer)
        {
           // self.visit.parentCustomer = aCustomer;
            
            [aCustomer addVisitsObject:self.visit];
        }
        
    
        NSLog(@"%@",self.customer.dealingOEMs);

        isReportCompleted = [self.visit.isReportCompleted boolValue];
        isVisitCompleted = [self.visit.isVisitCompleted boolValue];
        
        isForCustomer=YES;
        isNewSession=YES;
        _selectedControllerIndex= NSNotFound;
        app.isVisitSessionRunning = YES;
        
        if(!_audioManager)
            _audioManager = [[AudioManager alloc] init];

        recodedAudioDuration= [self.visit.recordingDuration doubleValue];
        // self.visit.endDateIntervalSinceRefDate = [NSNumber numberWithDouble:[NSDate timeIntervalSinceReferenceDate]];
        
        NSLog(@"Visit ID=%d",[self.visit.visitID hash]);
        
        [self trackCurrentLocation];
        
    }
    
    return self;
}

#pragma mark -
#pragma mark DEALLOC
-(void)dealloc
{
      [[NSNotificationCenter defaultCenter] removeObserver:self];
    
     [_audioManager release];
    [_visitNoteViewController release] ;
    [tagSelectionController release];
    [customer release];
   
    [natureOfVisitPopOver release];
    [currentNavController release];
    [noteControllerBottomBar release];
     [visit release];
    [_paintingView release];
   
    [customerOfferViewController release];
    [taskListViewController release];
    [photosViewController release];
    [lastLocalNotificationDate release];
    
    [docWatcher release];

    [super dealloc];
}

-(void)clear
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_audioManager            release];
    [_visitNoteViewController release] ;
    [tagSelectionController release];
    [customer release];
    
    [natureOfVisitPopOver release];
    [currentNavController release];
    [noteControllerBottomBar release];
    [visit release];
    [_paintingView release];
    
    [customerOfferViewController release];
    [taskListViewController release];
    [photosViewController release];
    [lastLocalNotificationDate release];
    
    //=================================//
    _audioManager                = nil;
    _visitNoteViewController     = nil;
    tagSelectionController       = nil;
    customer                     = nil;
    
    natureOfVisitPopOver         = nil;
    currentNavController         = nil;
    noteControllerBottomBar      = nil;
    visit                        = nil;
    _paintingView                = nil;
    
    customerOfferViewController  = nil;
    taskListViewController       = nil;
    photosViewController         = nil;
    lastLocalNotificationDate    = nil;
    //==================================//
}

#pragma mark -

-(void)trackCurrentLocation
{
    if([self.visit.isReportCompleted boolValue]) return;
    
    BOOL isCompleted = [self.visit.isVisitCompleted boolValue];
    
    
#if BACKGROUND_GPS_ENABLED
    CLLocation *newLocation = [LocationManager sharedManager].currentLocation;
    
    if(!newLocation) return;
    
    if(!self.visit.sessionStartLat && !self.visit.sessionStartLong)
    {
        self.visit.sessionStartLat  = [NSString stringWithFormat:@"%lf",newLocation.coordinate.latitude];
        self.visit.sessionStartLong = [NSString stringWithFormat:@"%lf",newLocation.coordinate.longitude];
    }
    else if(isCompleted && (!self.visit.sessionEndLat && !self.visit.sessionEndLong))
    {
        self.visit.sessionEndLat  = [NSString stringWithFormat:@"%lf",newLocation.coordinate.latitude];
        self.visit.sessionEndLong = [NSString stringWithFormat:@"%lf",newLocation.coordinate.longitude];
    }
#else
    
    __block VisitSessionViewController *weakSelf = self;
    __block LocationManager *locationManager = [LocationManager sharedManager];
    
    [locationManager addLocationDidUpdateCallback:^(CLLocation *newLocation)
     {
         NSLog(@"NewLocation Lat={%lf,%lf)",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
         
         if(!isCompleted)[locationManager stopUpdatingLocation];
         
         if(!weakSelf.visit.sessionStartLat && !weakSelf.visit.sessionStartLong)
         {
             weakSelf.visit.sessionStartLat  = [NSString stringWithFormat:@"%lf",newLocation.coordinate.latitude];
             weakSelf.visit.sessionStartLong = [NSString stringWithFormat:@"%lf",newLocation.coordinate.longitude];
         }
         else if(isCompleted && (!weakSelf.visit.sessionEndLat && !weakSelf.visit.sessionEndLong))
         {
             weakSelf.visit.sessionEndLat  = [NSString stringWithFormat:@"%lf",newLocation.coordinate.latitude];
             weakSelf.visit.sessionEndLong = [NSString stringWithFormat:@"%lf",newLocation.coordinate.longitude];
         }
         
     }];
    
    [locationManager startUpdatingLocation];
#endif

}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval
{
    // NSInteger ti = (NSInteger)interval;
    //NSInteger seconds = ti % 60;
    //NSInteger minutes = (ti / 60) % 60;
    
    NSInteger minutes = floor(interval/60);
    NSInteger seconds = interval - (minutes * 60);
    //NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02i:%02i", minutes, seconds]; //@"%02i:%02i"
}

-(void)invalidateTimer
{
    if(sliderTimer)
    {
        [sliderTimer invalidate];
        sliderTimer=nil;
    }
}

-(void)setupNatureOfVisit
{
  // NSString *natureOfVisit = [[VisitTypesViewcontroller visitTypes] objectAtIndex:[self.visit.natureOfVisitID intValue]];
    
    UILabel *natureOfVisitLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 600.0, 40.0)];
  //  natureOfVisitLabel.text = [NSString stringWithFormat:@"VISIT SESSION: %@",[natureOfVisit uppercaseString]];
    
    Customer *_customer = self.customer ? self.customer : self.visit.parentCustomer;
    
    NSLog(@"Customer ID=%@",_customer.customerID);
    
    natureOfVisitLabel.text = [NSString stringWithFormat:@"VISIT SESSION: %@",[_customer.name.length ? _customer.name : self.visit.scheduledVisit.customerName uppercaseString]];
    natureOfVisitLabel.textColor = [UIColor whiteColor];
    natureOfVisitLabel.font = [UIFont subaruBoldFontOfSize:23.0];
    natureOfVisitLabel.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    natureOfVisitLabel.shadowOffset = CGSizeMake(1.0, 2.0);
    natureOfVisitLabel.textAlignment = NSTextAlignmentCenter;
    natureOfVisitLabel.backgroundColor = [UIColor clearColor];

    
    /*
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(natureVisitButtonAction:)];
    tapGesture.numberOfTapsRequired=1;
    [natureOfVisitLabel addGestureRecognizer:tapGesture];
    tapGesture.enabled = !isVisitCompleted;
    [tapGesture release];
    */
    
    self.navigationItem.titleView = natureOfVisitLabel;
    //self.navigationItem.titleView.userInteractionEnabled = YES;
    [natureOfVisitLabel release];
}

-(void)setupSliderView
{
    locY=0.0;
    UIView *sliderContainerView=[[UIView alloc]initWithFrame:CGRectMake(0, locY, CGRectGetWidth(self.view.bounds), 44)];
    sliderContainerView.autoresizingMask=  UIViewAutoresizingFlexibleWidth ;
    
    sliderContainerView.backgroundColor=[UIColor clearColor];
    //[UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
    [self.view addSubview:sliderContainerView];
    sliderContainerView.layer.borderColor=[UIColor colorWithWhite:0.2 alpha:0.25].CGColor;;
    sliderContainerView.layer.borderWidth = 1;
    
    UIImage *recordButtonImage = [UIImage imageNamed:@"record_button_active.png"];
    recordButton=[UIButton buttonWithType:UIButtonTypeCustom];
    recordButton.frame=CGRectMake(10, 0, recordButtonImage.size.width,recordButtonImage.size.height);
    [recordButton setBackgroundImage:recordButtonImage forState:UIControlStateNormal];
    [recordButton setBackgroundImage:[UIImage imageNamed:@"stop_button.png"] forState:UIControlStateSelected];
    [recordButton setBackgroundImage:[UIImage imageNamed:@"record_button.png"] forState:UIControlStateDisabled];
    //recordButton.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    [recordButton addTarget:self action:@selector(recordAction:) forControlEvents:UIControlEventTouchUpInside];
    recordButton.exclusiveTouch=YES;
    [sliderContainerView addSubview:recordButton];
    
   // recordButton.hidden=!isNewSession;
    
    recordButton.userInteractionEnabled=!isVisitCompleted;
    recordButton.alpha=isVisitCompleted?0.6:1.0;
    
    __unused  float  locX=CGRectGetMaxX(recordButton.frame)+10;
    
    /*
    durationLeftLabel=[[UILabel alloc] initWithFrame:CGRectMake(locX, (CGRectGetHeight(sliderContainerView.frame)-20.0)/2.0, 60, 20)];
    durationLeftLabel.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    durationLeftLabel.textAlignment = UITextAlignmentCenter;
    durationLeftLabel.textColor=[UIColor darkGrayColor];
    durationLeftLabel.backgroundColor=[UIColor clearColor];
    [sliderContainerView addSubview:durationLeftLabel];
    [durationLeftLabel release];
    durationLeftLabel.text = [self stringFromTimeInterval:0.0];
    */
    
   // locX=CGRectGetMaxX(durationLeftLabel.frame)+10;
    
    UIImage *sliderImage = [UIImage imageNamed:@"slider.png"];
    slider=[[UISlider alloc] initWithFrame:CGRectMake(locX, (CGRectGetHeight(sliderContainerView.frame)-sliderImage.size.height)/2.0-5.0, sliderImage.size.width, sliderImage.size.height)];
    //slider=[[UISlider alloc] initWithFrame:CGRectMake(locX, (CGRectGetHeight(sliderContainerView.frame)-sliderImage.size.height)/2.0, sliderImage.size.width, sliderImage.size.height)];

    //slider.autoresizingMask=UIViewAutoresizingFlexibleWidth;
    [slider setThumbImage:[UIImage imageNamed:@"slider_button.png"] forState:UIControlStateNormal];
    [slider setMinimumTrackImage:sliderImage forState:UIControlStateNormal];
    [slider setMaximumTrackImage:sliderImage forState:UIControlStateNormal];
    [slider addTarget:self action:@selector(sliderValueDidChange:) forControlEvents:UIControlEventValueChanged];
    slider.minimumValue=0.0;
    slider.continuous=NO;
    slider.userInteractionEnabled=NO;
    slider.value=0.0;
    slider.backgroundColor = [UIColor clearColor];
     slider.autoresizingMask=UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    [sliderContainerView addSubview:slider];
    
    slider.hidden = (recodedAudioDuration==0);
    
    locX=CGRectGetMaxX(slider.frame)+10.0;
    
    durationLabel=[[UILabel alloc] initWithFrame:CGRectMake(locX, CGRectGetMinY(slider.frame), 80.0, 20)];
    durationLabel.font = [UIFont grotesqueFontOfSize:19.0];
    durationLabel.autoresizingMask=UIViewAutoresizingFlexibleRightMargin;
    durationLabel.textAlignment = UITextAlignmentCenter;
    durationLabel.textColor = [UIColor blackColor];
    durationLabel.backgroundColor=[UIColor clearColor];
    [sliderContainerView addSubview:durationLabel];
     durationLabel.text  = [self stringFromTimeInterval:recodedAudioDuration];
    [durationLabel release];
    durationLabel.hidden =  (slider.hidden = recodedAudioDuration==0);
    
    locX=CGRectGetMinX(slider.frame);
    recordDurationLabel=[[UILabel alloc] initWithFrame:CGRectMake(locX, (CGRectGetHeight(sliderContainerView.frame)-30.0)/2.0, CGRectGetWidth(slider.frame)+CGRectGetWidth(durationLabel.bounds), 30.0)];
    recordDurationLabel.textAlignment = UITextAlignmentCenter;
    recordDurationLabel.backgroundColor=[UIColor clearColor];
    recordDurationLabel.font = [UIFont grotesqueFontOfSize:19.0];
    recordDurationLabel.textColor = [UIColor redColor];
    recordDurationLabel.hidden = YES;
    [sliderContainerView addSubview:recordDurationLabel];
   // recordDurationLabel.text  = [self stringFromTimeInterval:recodedAudioDuration];
    [recordDurationLabel release];

    UIImage *playButtonImage = [UIImage imageNamed:@"play_button.png"];
    locX=CGRectGetWidth(sliderContainerView.bounds)-playButtonImage.size.width-10.0;
    
    playButton=[UIButton buttonWithType:UIButtonTypeCustom];
    playButton.frame=CGRectMake(locX, 0, playButtonImage.size.width, playButtonImage.size.height);
    
    [playButton setBackgroundImage:[UIImage imageNamed:@"play_button_active.png"] forState:UIControlStateNormal];
    [playButton setBackgroundImage:playButtonImage forState:UIControlStateDisabled];
    [playButton setBackgroundImage:[UIImage imageNamed:@"pause_button_active.png"] forState:UIControlStateSelected];
    [playButton setBackgroundImage:[playButton imageForState:UIControlStateNormal] forState:UIControlStateHighlighted];
    
    [playButton addTarget:self action:@selector(playAction:) forControlEvents:UIControlEventTouchUpInside];
    playButton.exclusiveTouch=YES;
    playButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [sliderContainerView addSubview:playButton];
    playButton.enabled = recodedAudioDuration>0 || !isReportCompleted;
    playButton.hidden =  (recodedAudioDuration==0);
    
    locY=CGRectGetMaxY(sliderContainerView.frame);
    
    [sliderContainerView release];
}

-(void)setupTabbarView
{
    UIView *tabbarContainerView=[[UIView alloc]initWithFrame:CGRectMake(0, locY, CGRectGetWidth(self.view.bounds), 54.0)];
    tabbarContainerView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
   // tabbarContainerView.backgroundColor=[UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
    [self.view addSubview:tabbarContainerView];
    
    tabbarContainerView.backgroundColor = [UIColor clearColor];
    
    locY=CGRectGetMaxY(tabbarContainerView.frame);
    
    UIImage *notesImage=[UIImage imageNamed:@"notes_banner.png"];
    UIImageView *visitingNotesImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 10.0, notesImage.size.width, notesImage.size.height)];
    visitingNotesImageView.image = notesImage;
    visitingNotesImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin;
    visitingNotesImageView.backgroundColor = [UIColor clearColor];
    visitingNotesImageView.userInteractionEnabled=YES;
    [tabbarContainerView addSubview:visitingNotesImageView];
    
    
    UILabel *visitNotesLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 5.0, 150.0, 40.0)];
    visitNotesLabel.font = [UIFont subaruBoldFontOfSize:23.0];
    visitNotesLabel.textColor = [UIColor whiteColor];
    visitNotesLabel.backgroundColor = [UIColor clearColor];
    visitNotesLabel.textAlignment = UITextAlignmentLeft;
    visitNotesLabel.text = @"VISIT NOTES";
    visitNotesLabel.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    visitNotesLabel.shadowOffset = CGSizeMake(1.0, 2.0);
    [visitingNotesImageView addSubview:visitNotesLabel];
    [visitNotesLabel release];
    

    UIButton *leftSegmentButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [leftSegmentButton setTitle:@"Text" forState:UIControlStateNormal];
    leftSegmentButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
    [leftSegmentButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [leftSegmentButton setBackgroundImage:[UIImage imageNamed:@"left_segment.png"] forState:UIControlStateNormal];
    [leftSegmentButton setBackgroundImage:[UIImage imageNamed:@"left_segment_selected.png"] forState:UIControlStateSelected];
    
    UIButton *rightSegmentButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [rightSegmentButton setTitle:@"Draw" forState:UIControlStateNormal];
     rightSegmentButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
    [rightSegmentButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightSegmentButton setBackgroundImage:[UIImage imageNamed:@"right_segment.png"] forState:UIControlStateNormal];
    [rightSegmentButton setBackgroundImage:[UIImage imageNamed:@"right_segment_selected.png"] forState:UIControlStateSelected];
    
    CustomSegmentedControl *segmentedControl = [[CustomSegmentedControl alloc] initWithFrame:CGRectMake(350.0, (CGRectGetHeight(visitingNotesImageView.bounds)-30.0)/2.0, 200.0, 30.0) items:[NSArray arrayWithObjects:leftSegmentButton,rightSegmentButton, nil]];
    //segmentedControl.selectedSegmentIndex = 0;
    segmentedControl.backgroundColor = [UIColor clearColor];
    [visitingNotesImageView addSubview:segmentedControl];
    if(!isVisitCompleted)segmentedControl.selectedSegmentIndex = 0;
    segmentedControl.userInteractionEnabled =  !isReportCompleted;
    segmentedControl.alpha = isReportCompleted ? 0.7f : 1.0f;
    [segmentedControl release];
    
    __block  VisitSessionViewController *weakSelf = self;
    
    [segmentedControl addDidSelectSegmentedControlCallback:^(CustomSegmentedControl *control, int index) 
    {
        NSLog(@"Selected index:%d",index);
        
        switch (index) 
        {
            case 0: //TEXT MODE
            {
                
                weakSelf->_visitNoteViewController.currentMode = NoteModeText;
                
            }
            break;
                
            case 1://DRAW MODE
            {
                weakSelf->_visitNoteViewController.currentMode = NoteModeDraw;
            }
            break;
        }
        
        // BOOL isEditMode = [_visitNoteViewController isEditModeEnabled];
       // [self animateEditBottombar];
        //[self showEditBottombar:isEditMode];

    }];
    
    //CHANGE IT
    UIImage *editButImage = [UIImage imageNamed:@"edit_blank.png"];
    editButton=[UIButton buttonWithType:UIButtonTypeCustom];
    editButton.frame = CGRectMake(CGRectGetWidth(visitingNotesImageView.bounds)-editButImage.size.width-10.0, (CGRectGetHeight(visitingNotesImageView.bounds)-30)/2.0, editButImage.size.width, editButImage.size.height);
    //[editButton setTitle:@"Edit" forState:UIControlStateNormal];
    [editButton setBackgroundImage:editButImage forState:UIControlStateNormal];
    editButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
    [editButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [visitingNotesImageView addSubview:editButton];
    [editButton setTitle:@"Edit" forState:UIControlStateNormal];
    [editButton setTitle:@"Done" forState:UIControlStateSelected];
    //@"edit_blank_selected.png"
    [editButton setBackgroundImage:[UIImage imageNamed:@"done_green_selected.png"] forState:UIControlStateSelected];
    [editButton addTarget:self action:@selector(setupEditNoteControllerBottomBar:) forControlEvents:UIControlEventTouchUpInside];
    editButton.enabled = !isReportCompleted;
    editButton.alpha = editButton.enabled ? 1.0f : 0.6f;

    
    UIImage *rightBannerImage=[UIImage imageNamed:@"right_banner.png"];
    UIImageView *rightBannerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(visitingNotesImageView.frame)+20.0, 10.0, rightBannerImage.size.width, rightBannerImage.size.height)];
    rightBannerImageView.image = rightBannerImage;
    rightBannerImageView.backgroundColor = [UIColor clearColor];
    rightBannerImageView.userInteractionEnabled = YES;
    [tabbarContainerView addSubview:rightBannerImageView];
   

    leftSegmentButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [leftSegmentButton setTitle:@"Offers" forState:UIControlStateNormal];
    leftSegmentButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
    [leftSegmentButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [leftSegmentButton setBackgroundImage:[UIImage imageNamed:@"left_segment.png"] forState:UIControlStateNormal];
    [leftSegmentButton setBackgroundImage:[UIImage imageNamed:@"left_segment_selected.png"] forState:UIControlStateSelected];
    
   UIButton *middleSegmentButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [middleSegmentButton setTitle:@"Task List" forState:UIControlStateNormal];
    middleSegmentButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
    [middleSegmentButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [middleSegmentButton setBackgroundImage:[UIImage imageNamed:@"mid_segment.png"] forState:UIControlStateNormal];
    [middleSegmentButton setBackgroundImage:[UIImage imageNamed:@"mid_segment_selected.png"] forState:UIControlStateSelected];
    
    rightSegmentButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [rightSegmentButton setTitle:@"Photos" forState:UIControlStateNormal];
    rightSegmentButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
    [rightSegmentButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightSegmentButton setBackgroundImage:[UIImage imageNamed:@"right_segment.png"] forState:UIControlStateNormal];
    [rightSegmentButton setBackgroundImage:[UIImage imageNamed:@"right_segment_selected.png"] forState:UIControlStateSelected];

    CustomSegmentedControl *menuSegmentedControl = [[CustomSegmentedControl alloc] initWithFrame:CGRectMake((CGRectGetWidth(rightBannerImageView.bounds)-300.0)/2.0, (CGRectGetHeight(rightBannerImageView.bounds)-30.0)/2.0, 300.0, 30.0) items:[NSArray arrayWithObjects:leftSegmentButton,middleSegmentButton,rightSegmentButton, nil]];
    menuSegmentedControl.selectedSegmentIndex = 0;
    menuSegmentedControl.backgroundColor = [UIColor clearColor];
    [rightBannerImageView addSubview:menuSegmentedControl];
    [menuSegmentedControl release];
    
    [menuSegmentedControl addDidSelectSegmentedControlCallback:^(CustomSegmentedControl *control, int index) 
     {
         [weakSelf->_visitNoteViewController resignResponder];
         [weakSelf _selectControllerForIndex:index];
         NSLog(@"Selected index:%d",index);
     }];
    

    [visitingNotesImageView release];
     [rightBannerImageView release];
    [tabbarContainerView release];    
}

-(void)setupTextNoteController
{
    CGRect _visitNoteControllerFrame = CGRectMake(10.0, locY, kTextNoteControllerWidth, CGRectGetHeight(self.view.bounds)-locY);
    
    /*
    UIImageView *notesBgView = [[UIImageView alloc] initWithFrame:_visitNoteControllerFrame];
    notesBgView.image = [UIImage imageNamed:@"note_bg.png"];
    [self.view addSubview:notesBgView];
    [notesBgView release];
    */
    
    NSString *objectId = [NSString stringWithFormat:@"%u", [self.visit.visitID hash]];
    NSString *drawingPath = [DirectoryManager drawingDirectoryPathForName:objectId];
    NSData *data=[NSData dataWithContentsOfFile:drawingPath];
    NSMutableArray *notes = data?[NSKeyedUnarchiver unarchiveObjectWithData:data]:nil;
    
    _visitNoteViewController = [[VisitNoteViewController alloc] initWithNotes:notes];
    _visitNoteViewController.view.frame = _visitNoteControllerFrame;
    _visitNoteViewController.view.backgroundColor = [UIColor clearColor];
    _visitNoteViewController.view.autoresizesSubviews = YES;
    _visitNoteViewController.view.autoresizingMask =  UIViewAutoresizingFlexibleHeight;
    _visitNoteViewController.view.layer.borderWidth = 1;
    _visitNoteViewController.view.layer.borderColor = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:0.1].CGColor;
    _visitNoteViewController.visitSessionViewController = self;
    [self.view addSubview:_visitNoteViewController.view];
    
    ///=============================== BUG =============================
    if(isReportCompleted) [_visitNoteViewController makeReportCompleted];
}

-(void)setupDrawingController
{
    CGRect _paintingControllerFrame = _visitNoteViewController.view.frame ;
    
    _paintingView=[[DrawingCanvasView alloc] initWithShapes:nil];
    _paintingView.frame=_paintingControllerFrame;
    _paintingView.autoresizingMask= UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
    _paintingView.backgroundColor = [UIColor clearColor];
    _visitNoteViewController.view.layer.borderWidth = 1;
    _visitNoteViewController.view.layer.borderColor = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:0.1].CGColor;

    //[self.view insertSubview:_paintingView.view belowSubview:_visitNoteViewController.view];
    [self.view addSubview:_paintingView];
    _paintingView.currentMode=PaintingModeDraw;
    _paintingView.hidden = YES;
    
   // paintingViewController.visitReviewSessionViewController = self;
    //noteViewController.paintingViewController = paintingViewController;
}

-(void)setupSideController
{
    UIImage *publishReportImage = [UIImage imageNamed:@"publish_visit_session_button.png"];
    publishReportButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [publishReportButton setTitle:@"END VISIT" forState:UIControlStateNormal];
    [publishReportButton setTitle:@"PUBLISH REPORT" forState:UIControlStateSelected];
    [publishReportButton setTitle:@"REPORT PUBLISHED" forState:UIControlStateDisabled];
    
    publishReportButton.titleLabel.font = [UIFont subaruBoldFontOfSize:23.0];
    [publishReportButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    publishReportButton.titleLabel.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    publishReportButton.titleLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    
    [publishReportButton setBackgroundImage:[UIImage imageNamed:@"end_visit_session_button.png"] forState:UIControlStateNormal];
    [publishReportButton setBackgroundImage:publishReportImage forState:UIControlStateSelected];
    [publishReportButton setBackgroundImage:publishReportImage forState:UIControlStateDisabled];
    //[publishReportButton setBackgroundImage:[UIImage imageNamed:@"end_visit_session_button.png"] forState:UIControlStateDisabled];
    
    publishReportButton.frame = CGRectMake(10.0+kTextNoteControllerWidth+20.0, CGRectGetHeight(self.view.bounds)-44.0-10.0, 352.0, 44.0);
    publishReportButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:publishReportButton];
    [publishReportButton addTarget:self action:@selector(publishReportAction:) forControlEvents:UIControlEventTouchDown];
   
    if(isReportCompleted)
    {
       publishReportButton.enabled  = NO;
    }
    else
    {
        publishReportButton.selected = isVisitCompleted;
    }
    publishReportButton.userInteractionEnabled = !isReportCompleted;
    
    
    //bg_right
    
    UIImage *bgImage = [UIImage imageNamed:@"bg_right.png"];
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame: CGRectMake(10.f+kTextNoteControllerWidth+20.0f, locY, bgImage.size.width, bgImage.size.height)];
    bgImageView.image=bgImage;
    bgImageView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:bgImageView];
    [bgImageView release];
    
}

-(void)animateEditBottombar
{
    CGRect visibleFrame= CGRectMake(10.0, CGRectGetHeight(self.view.bounds)-44.0, kTextNoteControllerWidth, 44.0);
    CGRect hiddenFrame = visibleFrame;
    hiddenFrame.origin.y = CGRectGetHeight(self.view.bounds);
    
    //BOOL isDrawingmode = (_visitNoteViewController.currentMode == NoteModeDraw);
    BOOL isEditMode = [_visitNoteViewController isEditModeEnabled];
    
    //=========== Animate From bottom and vice-versa ==============//
    [UIView animateWithDuration:0.3 animations:^{
        
        //noteControllerBottomBar.frame = isEditMode && !isDrawingmode ?  hiddenFrame  : visibleFrame;
        //noteControllerBottomBar.frame = isEditMode && !isDrawingmode ?  visibleFrame : hiddenFrame;
        noteControllerBottomBar.frame = isEditMode ?  visibleFrame : hiddenFrame;
        
    }];

}

-(void)showEditBottombar:(BOOL)show
{
    CGRect visibleFrame= CGRectMake(10.0, CGRectGetHeight(self.view.bounds)-44.0, kTextNoteControllerWidth, 44.0);
    CGRect hiddenFrame = visibleFrame;
    hiddenFrame.origin.y = CGRectGetHeight(self.view.bounds);
    
    //=========== Animate From bottom and vice-versa ==============//
    [UIView animateWithDuration:0.3 animations:^{
        
        noteControllerBottomBar.frame = show ?  visibleFrame : hiddenFrame;
        
    }];
}


-(void)resetSegmentedControl:(CustomSegmentedControl *)control
{
    control.selectedSegmentIndex=NSNotFound;
}

-(void)enableTagging:(BOOL)yesNo
{
    UIButton *leftSegmentButton = (UIButton *)[noteControllerBottomBar viewWithTag:1];
    leftSegmentButton.enabled = yesNo;
}

-(BOOL)isTaggingEnabled
{
    UIButton *leftSegmentButton = (UIButton *)[noteControllerBottomBar viewWithTag:1];
   
    return leftSegmentButton.isEnabled;

}

#pragma mark EDIT BOTTOM BAR
-(void)setupEditNoteControllerBottomBar:(UIButton *)sender
{
    /*
    if(!sender.selected  && !_visitNoteViewController.notes.count)
    {
        return;
    }
    */
    
    [self manageTagSelectionController:NO];
    
    CGRect frame= CGRectMake(10.0, CGRectGetHeight(self.view.bounds), kTextNoteControllerWidth, 44.0);
   
    //CustomSegmentedControl *segmentedControl =nil;
    
    if(!noteControllerBottomBar)
    {
        UIImage *notesHeaderImage = [UIImage imageNamed:@"notes_banner.png"];
        noteControllerBottomBar = [[UIImageView alloc] initWithFrame:frame];
        noteControllerBottomBar.image = notesHeaderImage;
        noteControllerBottomBar.backgroundColor = [UIColor clearColor];
        noteControllerBottomBar.userInteractionEnabled = YES;
        [self.view addSubview:noteControllerBottomBar];
        noteControllerBottomBar.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        
        
        UIButton *leftSegmentButton=[UIButton buttonWithType:UIButtonTypeCustom];
        [leftSegmentButton setTitle:@"Tag Text" forState:UIControlStateNormal];
        leftSegmentButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
        [leftSegmentButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [leftSegmentButton setBackgroundImage:[UIImage imageNamed:@"left_segment.png"] forState:UIControlStateNormal];
        [leftSegmentButton setBackgroundImage:[UIImage imageNamed:@"left_segment_selected.png"] forState:UIControlStateHighlighted];
        leftSegmentButton.tag = 1;
        leftSegmentButton.frame = CGRectMake((CGRectGetWidth(noteControllerBottomBar.bounds)-200.0)/2.0, (CGRectGetHeight(noteControllerBottomBar.bounds)-30.0)/2.0, 100.0, 30.0);
       // leftSegmentButton.enabled = NO;
        [noteControllerBottomBar addSubview:leftSegmentButton];
        [leftSegmentButton addTarget:self action:@selector(editSegmentedAction:) forControlEvents:UIControlEventTouchUpInside];
        

        UIButton *rightSegmentButton=[UIButton buttonWithType:UIButtonTypeCustom];
        [rightSegmentButton setTitle:@"Delete" forState:UIControlStateNormal];
        rightSegmentButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:12.0];
        [rightSegmentButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [rightSegmentButton setBackgroundImage:[UIImage imageNamed: @"right_segment.png"] forState:UIControlStateNormal];
        [rightSegmentButton setBackgroundImage:[UIImage imageNamed:@"right_segment_selected.png"] forState:UIControlStateHighlighted];
        rightSegmentButton.tag = 2;
        rightSegmentButton.frame = CGRectMake(CGRectGetMaxX(leftSegmentButton.frame), CGRectGetMinY(leftSegmentButton.frame), 100.0, 30.0);
        
        [noteControllerBottomBar addSubview:rightSegmentButton];
        
        [rightSegmentButton addTarget:self action:@selector(editSegmentedAction:) forControlEvents:UIControlEventTouchUpInside];
        
        /*
        segmentedControl = [[CustomSegmentedControl alloc] initWithFrame:CGRectMake((CGRectGetWidth(noteControllerBottomBar.bounds)-200.0)/2.0, (CGRectGetHeight(noteControllerBottomBar.bounds)-30.0)/2.0, 200.0, 30.0) items:[NSArray arrayWithObjects:leftSegmentButton,rightSegmentButton, nil]];
        segmentedControl.backgroundColor = [UIColor clearColor];
        //segmentedControl.tag = 1;
        [noteControllerBottomBar addSubview:segmentedControl];
        [segmentedControl release];
        
        [segmentedControl addDidSelectSegmentedControlCallback:^(CustomSegmentedControl *control, int index) 
         {
             NSLog(@"Selected index:%d",index);
             
             if(index==0)
             {
                 [self tagTextNoteAction];
             }
             else 
             {
                 [self deleteTextNoteAction];
             }
             
           // segmentedControl.selectedSegmentIndex=NSNotFound;
             
             [self performSelector:@selector(resetSegmentedControl:) withObject:segmentedControl afterDelay:0.2];
         }];
         */
    }
    
    UIButton *leftSegmentButton =(UIButton *)[noteControllerBottomBar viewWithTag:1];
    leftSegmentButton.enabled = NO;
    
    sender.selected = !sender.selected;
    BOOL isEditMode = sender.selected;
    
  //  segmentedControl = (CustomSegmentedControl *) [noteControllerBottomBar viewWithTag:1];
  //  segmentedControl.selectedSegmentIndex=NSNotFound;
    
   // [[noteControllerBottomBar viewWithTag:1] setEnabled:YES];
    [_visitNoteViewController enableEditMode:isEditMode];
  
    //if(_visitNoteViewController.currentMode != NoteModeText) return;
    

   // [self animateEditBottombar];  
    [self showEditBottombar:isEditMode];
    
    ///===========RESET SELECTION OF CONTAINERS FOR TAGGING ONLY=========
    if(!isEditMode) 
    [_visitNoteViewController resetSelection];
}

-(void)editSegmentedAction:(UIButton *)sender
{
    int tag=sender.tag;
    if(tag==1)
    {
        [self tagTextNoteAction];
    }
    else 
    {
        [self deleteTextNoteAction];
    }

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif
    
    //UIApplicationLaunchOptionsLocalNotificationKey
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localNotificationDidArrive:) name:kApplicationDidTimeoutNotification object:nil];
    
	[self.navigationController.navigationBar addBackGroundImage:@"navigation_bar.png"];
    [self addBackButtonWithSelector:@selector(backButtonAction:)];
    //[self.navigationController.navigationBar addBackGroundImage:@"header_blank.png"];
    
  
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidChangeSiteID:) name:AppDidChangeSiteNotification object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(repDidDeactivate:) name:RepDidDeactivateNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    //================================= DELETE BUTTON =============================================
    
  
    UIImage *deleteImage = [UIImage imageNamed:@"delete_button_visit.png"];
   
    UIButton *deleteButton = [UIButton buttonWithTitle:@"Delete" type:UIButtonTypeCustom frame:CGRectMake(0, 0, deleteImage.size.width, deleteImage.size.height) bgImage:nil titleColor:[UIColor whiteColor] target:self action:@selector(deleteAction:)];
    [deleteButton setBackgroundImage:deleteImage forState:UIControlStateNormal];
    deleteButton.titleLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
    deleteButton.titleLabel.shadowColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4];
    deleteButton.titleLabel.shadowOffset = CGSizeMake(0, 1.0);
    deleteButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    deleteButton.titleEdgeInsets = UIEdgeInsetsMake(-3.0, 0, 0, 0);
    
    UIBarButtonItem *deleteBarButton = [[UIBarButtonItem alloc] initWithCustomView:deleteButton];
    self.navigationItem.rightBarButtonItem = deleteBarButton;
    [deleteBarButton release];
    
    //================================== DELETE BUTTON ====================================
    
    self.view.backgroundColor = [UIColor colorWithRed:241.0/255.0 green:239.0/255.0 blue:238.0/255.0 alpha:1];
    
    [self setupNatureOfVisit];
    [self setupSliderView];
    [self setupTabbarView];
    [self setupTextNoteController];
    //[self setupDrawingController];
    [self setupEditNoteControllerBottomBar:nil];
    [self setupSideController];
    
    
    ///============================= CALCULATE INACTIVE SECONDS ==================================
    NSTimeInterval _inactiveSec = [self.visit.inactiveSeconds doubleValue];
     _inactiveSec += self.visit.lastDraftDate ? [[NSDate date] timeIntervalSinceDate:self.visit.lastDraftDate] : 0 ;
    self.visit.inactiveSeconds = [NSNumber numberWithDouble:_inactiveSec];
    
    app.strDealingOemForVisitPhoto = self.customer.dealingOEMs;
    
    app.fromCustomerTask=1;
   
/*
#if LOCAL
    [self watchFileChangeInItunes];
#endif
*/
}

-(void)watchFileChangeInItunes
{
     NSString *docuDir = [DirectoryManager documentsDirectory];
     self.docWatcher = [DirectoryWatcher watchFolderWithPath:docuDir delegate:self];
 }


- (void)directoryDidChange:(DirectoryWatcher *)folderWatcher
{
    [UIAlertView showWarningAlertWithTitle:@"DocDidChange" message:nil];
    
    return;
    
    /*
    NSString *docuDir = [DirectoryManager documentsDirectory];
    
    NSString *recordFilePath = [self _recordFilePath];
    NSDictionary *docuDict =  [[NSFileManager defaultManager] attributesOfItemAtPath:recordFilePath error:nil];
    NSUInteger fileNumber = [[docuDict valueForKey:@"NSFileSystemFileNumber"] unsignedIntegerValue];;
    NSArray *subfolders = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:docuDir error:nil];
    
    NSString *renamedFolderName=nil;
    for(NSString *folderName in subfolders)
    {
        NSString *folderPath     = [docuDir stringByAppendingPathComponent:folderName];
        NSDictionary *folderDict =  [[NSFileManager defaultManager] attributesOfItemAtPath:folderPath error:nil];
        
        NSUInteger _fileNumber = [[folderDict valueForKey:@"NSFileSystemFileNumber"] unsignedIntegerValue];
        
        if(_fileNumber == fileNumber)
        {
            renamedFolderName = folderName;
            break;
        }
    }
  */
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)forcefullySetLandscapeOrientation
{
    UIInterfaceOrientation _curOrientation=[UIApplication sharedApplication].statusBarOrientation;
    if(!UIInterfaceOrientationIsLandscape(_curOrientation))
    {
        [[UIDevice currentDevice] setOrientation:UIDeviceOrientationLandscapeLeft];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    //[self forcefullySetLandscapeOrientation];
}

-(void)deleteAction:(id)sender
{
  UIAlertView *alert=  [UIAlertView alertViewWithTitle:@"" message:VISIT_DELETE_MESSAGE cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObject:@"Yes"] onDismiss:^(int buttonIndex) 
    {
        [sender setUserInteractionEnabled : NO];
        
        self.view.userInteractionEnabled = NO;
        
        if(isPlaying)
        {
            [self stopPlay];
        }
        
        if(isRecording)
        {
            [_audioManager stopRecord];
            [self invalidateTimer];
            recordTick=0.0;
        }
        
       // [self stopPlayOrRecording];
        
    
         [CoreDataHandler deleteVisit:self.visit];
    
        if(customerOfferViewController)
            customerOfferViewController.visit = nil;
        
        if(photosViewController)
            photosViewController.visit = nil;
        
         [[SSCoreDataManager sharedManager] save:nil];
        
        [self dismiss];
        
       
        NSDictionary *userInfoDict = [NSDictionary dictionaryWithObjectsAndKeys:@"delete",@"action", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:VisitReviewDidUpdate object:nil userInfo:userInfoDict];
        //[alert release];
    } 
     
    onCancel:^{
       //[alert release];
        
    }];
    
    [alert release];
}

-(void)deleteCurrentSession
{
    
}
#pragma mark -
#pragma mark NATURE OF VISIT
-(void)natureVisitButtonAction:(UITapGestureRecognizer *)sender
{
    [_visitNoteViewController resignResponder];
    
    UIPopoverController *popover =self.natureOfVisitPopOver;
    
    if(!popover)
    {
    VisitTypesViewcontroller *visitTypesController=[[VisitTypesViewcontroller alloc]initWithNatureOfVisitID:[self.visit.natureOfVisitID integerValue] ];
    
    // visitTypesController.isReadonlyMode=YES;
    visitTypesController.contentSizeForViewInPopover=CGSizeMake(320, 310);
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController: visitTypesController];

    
    popover=[[UIPopoverController alloc] initWithContentViewController:navController];
    self.natureOfVisitPopOver = popover;
    
    [visitTypesController addNatureOfVisitDidSelectCallback:^(NSInteger visitNatureID)
     {
         if(visitNatureID!=NSNotFound)
             self.visit.natureOfVisitID = [NSNumber numberWithInteger:visitNatureID];
         
          NSString *natureOfVisit = [[VisitTypesViewcontroller internalVisitTypes] objectAtIndex:visitNatureID];
         UILabel *natureOfVisitLabel=(UILabel *)self.navigationItem.titleView;
         // natureOfVisitButton.titleLabel.text = [NSString stringWithFormat:@"VISIT SESSION: %@",[natureOfVisit uppercaseString]];
         natureOfVisitLabel.text = [NSString stringWithFormat:@"VISIT SESSION: %@",[natureOfVisit uppercaseString]];
         
         [natureOfVisitPopOver dismissPopoverAnimated:YES];
         
     }];
    
    [popover release];
    [visitTypesController release];
    [navController release];
    }
    
    [natureOfVisitPopOver presentPopoverFromRect:[sender.view frame] inView:self.navigationController.navigationBar permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];

}

#pragma mark -
#pragma mark MANAGE SIDE CONTROLLER VIEW
-(void)_showControllerOnSide:(UIViewController *)controller
{
    currentNavController = [[UINavigationController alloc] initWithRootViewController:controller];
    controller.view.backgroundColor = [UIColor clearColor];
    currentNavController.navigationBarHidden = YES;
    currentNavController.view.frame = CGRectMake(10.f+kTextNoteControllerWidth+20.0f, locY, kSideControllerWidth, CGRectGetHeight(self.view.bounds)-locY-64.0f);
   
    //currentNavController.view.layer.borderWidth=1.0f;
    //currentNavController.view.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.25].CGColor;
    
    //==================== ROUND ONLY LOWER CORNERS =====================
    [currentNavController.view roundCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight withRedius:CGSizeMake(9.0f, 9.0f)];
    //currentNavController.view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:currentNavController.view];
}


-(void)_controllerDidSelect:(UIButton *)sender
{
    int tag=sender.tag;
    sender.selected=YES;
    sender.userInteractionEnabled=NO;
    
    for(int i=1;i<=3;i++)
    {
        if(i!=tag)
        {
            UIButton *but = (UIButton *)[[sender superview] viewWithTag:i];
            but.selected=NO;
            but.userInteractionEnabled=YES;
        }
    }

    [self _selectControllerForIndex:tag-1];
}


-(void)_selectControllerForIndex:(NSInteger)index
{
    if(_selectedControllerIndex == index) return;
    
     /*
    [UIView animateWithDuration:0.3 animations:^{
        
        currentNavController.view.alpha = 0.4;
    } 
    completion:^(BOOL finished) 
    {
    */
    
      if(currentNavController)
        {
            [currentNavController.view removeFromSuperview];
            [currentNavController release];
            currentNavController=nil;
        }
        
        switch (index) 
        {
            case 0: //VIEW OFFERS
            {
                [self showOffers];
                
            }
                break;
                
            case 1: //TASK LIST
            {
                
                [self showTaskList];
            }
                break;
                
            case 2: //PHOTOS
            {
                
                [self showPhotos];
            }
                break;
                
            default: break;
        }
        
        _selectedControllerIndex = index;
    
        currentNavController.view.alpha = 0.3;
        [UIView animateWithDuration:0.2 animations:^{
            
        //currentNavController.view.alpha = 0.09;
         currentNavController.view.alpha = 1.0;
   
        }];
 
   // }];
    
}
#pragma mark -
#pragma mark OFFERS
-(void)showOffers
{
    
    if(!customerOfferViewController)
    {
      customerOfferViewController   = [[CustomerOfferInVisitSessionController alloc] initWithCategoryName:self.customer.categoryName visit:self.visit];
    }
    else //RELOAD FROM SERVER
    {
        [customerOfferViewController fetchOffersFromServer];
    }
    
    [self _showControllerOnSide:customerOfferViewController];
    [customerOfferViewController.navigationController.navigationBar addBackGroundImage:@"right_sub_banner.png"];
}

#pragma mark TASKS
-(void)showTaskList
{
    if(!taskListViewController)
    {
    NSMutableArray *tasks= isForCustomer ?  [CoreDataHandler allTasksForCustomer:self.customer] : [CoreDataHandler allTasks];
    taskListViewController = [[TaskListInVisitSession alloc] initWithTasks:tasks];
    taskListViewController.visit = self.visit;
    
    }
    
    [self _showControllerOnSide:taskListViewController];
    

   __block VisitSessionViewController *weakSelf = self;
    
    [taskListViewController addNewTaskCallback:^{
        
        VisitSessionViewController *strongSelf = weakSelf;
        
       __block CreateTaskViewController *createTaskViewController = nil;
        createTaskViewController=[[CreateTaskViewController alloc] initWithTask:nil];
        [weakSelf->currentNavController pushViewController:createTaskViewController animated:YES];
        //if(showCreateTask) 
        weakSelf->currentNavController.navigationBarHidden=YES;
        //createTaskViewController.view.backgroundColor = [UIColor clearColor];
        //[_newTask release];
        
        [createTaskViewController addSaveTaskCallback:^(Task *task) 
         {
             
             //task.parentCustomer=self.customer;
             if(strongSelf.customer)
             [strongSelf.customer addTasksObject:task];
             
             NSError *error=nil;
             [[SSCoreDataManager sharedManager] save:error];
             
             if(!error)
             {
                 //[taskListViewController.tasks addObject:task];
                 //[taskListViewController.tableView reloadData];
                 
                 [strongSelf->taskListViewController addNewTask:task];
                 
                 [strongSelf->currentNavController popViewControllerAnimated:YES];
             }
             else
             {
                // [UIAlertView showWarningAlertWithTitle:@"Error!" message:@"Error creating a new task."];
             }
         }];
        
        [createTaskViewController addCancelTaskCallback:^{
           
            [strongSelf->currentNavController popViewControllerAnimated:YES];
        }];
        
        [createTaskViewController release];
    }];

}

#pragma mark PHOTOS
-(void)showPhotos
{
    if(!photosViewController)
    photosViewController=[[VisitPhotosViewController alloc]initWithVisit:self.visit];

    [self _showControllerOnSide:photosViewController];
     currentNavController.navigationBarHidden=YES;
    //[photosViewController release];
}



#pragma mark -
#pragma mark TAG NOTE REPORT

-(void)manageTagSelectionController:(BOOL)show
{
    CGRect visibleRect = _visitNoteViewController.view.frame;
    CGRect hiddenRect = visibleRect;
    hiddenRect.origin.y+=CGRectGetHeight(visibleRect);
    
    if(show)
    {
        tagSelectionController = [[TagSelectionController alloc] init];
        tagSelectionController.strOems=self.customer.dealingOEMs;
        tagSelectionController.delegate = self;
        
        /*
      __block  VisitSessionViewController *weakSelf = self;
        
        [tagSelectionController addDidSelectCallback:^(NSString *tag)
        {
            if(tag)
            [weakSelf->_visitNoteViewController tagWithOEM:tag];
            
            
        }];
        
        [tagSelectionController addDismissCallback:^{
            
            [weakSelf manageTagSelectionController:NO];
            
        }];
        */
        
        tagSelectionController.view.frame = visibleRect;
        [self.view addSubview:tagSelectionController.view];
    }
    else if(!tagSelectionController)
    {
        return;
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        
        tagSelectionController.view.frame = show ? hiddenRect  : visibleRect;
        tagSelectionController.view.frame = show ? visibleRect : hiddenRect;
        
    } 
    completion:^(BOOL finished) 
     {
         if(!show)
         {
             
             [tagSelectionController.view removeFromSuperview];
             [tagSelectionController release];
             tagSelectionController = nil;

         }
         
     }];

}

-(void)tagSelectionController:(TagSelectionController *)tagSelectionController didSelectTag:(NSString *)tag;
{
    if(tag)
    [_visitNoteViewController tagWithOEM:tag];
}

-(void)tagSelectionControllerWillDissmiss:(TagSelectionController *)tagSelectionController
{
     [self manageTagSelectionController:NO];
}

-(void)tagTextNoteAction
{
    [self manageTagSelectionController:YES];
}
#pragma mark -
#pragma mark DELETE NOTE REPORT
-(void)deleteTextNoteAction
{
    [_visitNoteViewController deleteSelectedContainers];
}
#pragma mark -

#pragma mark RECORD

-(void)stopRecord:(UIButton *)sender onCompletion:(void(^)(void))completion
{
    UIButton *btn=(UIButton *)sender;
    NSString *tempRecordFilePath = [self _tempRecordFilePath];
    
    if(isRecording)
    {
        ProgressHUD *hud = [ProgressHUD showHUDAddedTo:self.view animated:YES];
        //hud.labelText =@"Stoping...";
        hud.labelText =@"Recording Stopped";
        
        [_audioManager stopRecord];
        [self invalidateTimer];
        
        NSString *mainRecordFilePath = [self _recordFilePath];
        NSString *tempCombinedPath   = [self _tempCombinedFilePath];
        
        playButton.enabled= NO;
        playButton.hidden = NO;
        _visitNoteViewController.tableView.userInteractionEnabled =NO;
        [_audioManager appendSoundAtPath:tempRecordFilePath toPath:mainRecordFilePath temporaryCombinePath:tempCombinedPath completionHandler:^(BOOL success) 
         {
             double _duration = [self durationOfFileAtPath:mainRecordFilePath];
             _visitNoteViewController.tableView.userInteractionEnabled =YES;
             recodedAudioDuration =_duration;
             //self.visit.recordingDuration = [NSNumber numberWithDouble:recodedAudioDuration];
             recordTick=0;
             
             if(isPlaying)               slider.maximumValue=_duration;
             slider.hidden = NO;
             recordDurationLabel.hidden =  YES;
             durationLabel.hidden       =  NO;
             durationLabel.text         = [self stringFromTimeInterval:recodedAudioDuration];
             isRecording=!isRecording;
             btn.selected = isRecording;
             playButton.enabled=!isRecording && recodedAudioDuration>0;
             //playButton.hidden = !playButton.enabled;

             [ProgressHUD hideHUDForView:self.view animated:YES];
             
             if(completion) completion();
         }];
    }
    else 
    {
         if(completion) completion();
    }

}
-(void)recordAction:(UIButton *)sender
{
    UIButton *btn=(UIButton *)sender;
    NSString *tempRecordFilePath = [self _tempRecordFilePath];
    
    if(isRecording)
    {
        [_audioManager stopRecord];
        [self invalidateTimer];
        
        NSString *mainRecordFilePath = [self _recordFilePath];
        NSString *tempCombinedPath   = [self _tempCombinedFilePath];
        
        playButton.enabled=NO;
        playButton.hidden = NO;
        [_audioManager appendSoundAtPath:tempRecordFilePath toPath:mainRecordFilePath temporaryCombinePath:tempCombinedPath completionHandler:^(BOOL success) 
         {
             double _duration = [self durationOfFileAtPath:mainRecordFilePath];
             recodedAudioDuration =_duration;
             //self.visit.recordingDuration = [NSNumber numberWithDouble:recodedAudioDuration];
             recordTick=0;
             
             if(isPlaying) slider.maximumValue=_duration;
             
             //=========AFTER RECORD TO START PLAYING FROM BEGINING ==========//
              slider.value = 0;
              timerTick = 0;
            
             //===============================================================//
             
             slider.hidden = NO;
             recordDurationLabel.hidden =  YES;
             durationLabel.hidden       =  NO;
            
             durationLabel.text         = [self stringFromTimeInterval:recodedAudioDuration];
             
             isRecording=!isRecording;
           
             
             btn.selected = isRecording;

             
             playButton.enabled=!isRecording && recodedAudioDuration>0;
             //playButton.hidden = !playButton.enabled;
             
             //=========== RELOAD NOTE TABLE ==============
             [_visitNoteViewController.tableView reloadData];
         }];
    }
    else
    {
        //self.recordFilePath=recordFilePath
        
#if !TARGET_IPHONE_SIMULATOR
        NSError *error=nil;
        [_audioManager startRecordAtPath:tempRecordFilePath encodingFormat:ENC_AAC error:error]; //ENC_AAC //ENC_ALAC
#endif
        
        [self invalidateTimer];
        
        recordTick=0.0;
        
        slider.hidden=YES;
        recordDurationLabel.hidden=NO;
        durationLabel.hidden=YES;
        durationLeftLabel.hidden=YES;
        
        // slider.maximumValue=MAX_AUDIO_DURATION;
        slider.value=0.0;
        
        sliderTimer=[NSTimer scheduledTimerWithTimeInterval:0.005 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
        
        durationLeftLabel.text = [self stringFromTimeInterval:slider.value];
        
        isRecording=!isRecording;
        btn.selected = isRecording;
        
        // slider.userInteractionEnabled=!isRecording;
        playButton.enabled=!isRecording;
        
        //============== RELOAD  NOTE TABLE ==========
        [_visitNoteViewController.tableView reloadData];
    }
}

#pragma mark -
#pragma mark PLAY

-(void)playAction:(UIButton *)sender
{
        NSString *recordedFilePath=[self _playFilePath];
        
        if(isPlaying)
        {
            if(isNewSession)
            {
                [_audioManager.audioPlayer pause];
                [self invalidateTimer];
                isPaused=YES;
                
                slider.userInteractionEnabled=YES;
                
          
                recordButton.enabled =  YES;
                
                if([self.visit.isVisitCompleted boolValue])
                recordButton.enabled=NO;
            }
            else
            {
                [self invalidateTimer];   
                [_audioManager stopPlayingAudio];
                
                slider.value=0.0;
                timerTick=0;
                
                slider.userInteractionEnabled=NO;
            }
        }
        else
        {
            slider.hidden=NO;
            slider.userInteractionEnabled=YES;
            durationLabel.hidden = NO;
            
            
            if(isPaused)
            {
                [_audioManager.audioPlayer play];
                
                if(!_audioManager.audioPlayer.isPlaying)
                {
                     [_audioManager playAudioAtPath:recordedFilePath error:nil completionHandler:^(AVAudioPlayer *audioPlayer, NSError *error, BOOL success)
                      {
                          isPlaying=!success;
                          
                          if(sliderTimer)
                          {
                              [sliderTimer invalidate];
                              sliderTimer=nil;
                          }
                          
                       
                          
                          sender.selected = isPlaying;
                          
                          timerTick=0.0;
                          slider.value=0.0;
                          
                          // durationLeftLabel.text =[self stringFromTimeInterval:slider.value];
                          durationLabel.text     =[self stringFromTimeInterval:slider.maximumValue-slider.value];
                          
                          
                          if([self.visit.isVisitCompleted boolValue])
                              recordButton.enabled=NO;
                          else
                              recordButton.enabled =YES;
                          
                      }];
                   }
               
                isPaused=NO;
                
                //================== //
                isPlaying=_audioManager.audioPlayer.isPlaying;
                
                sliderTimer=[NSTimer scheduledTimerWithTimeInterval:0.005 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
                
            }
            else
            {
                
                NSError *error=nil;
                
                [_audioManager.audioPlayer setVolume:1];
                [_audioManager playAudioAtPath:recordedFilePath error:error completionHandler:^(AVAudioPlayer *audioPlayer, NSError *error, BOOL success) 
                 {
                     if(success)
                     {
                         isPlaying=!success;
                         
                         if(sliderTimer)
                         {
                             [sliderTimer invalidate];
                             sliderTimer=nil;
                         }
                         
                        
                         
                         sender.selected = isPlaying;
                         
                         timerTick=0.0;
                         slider.value=0.0;
                         
                        // durationLeftLabel.text =[self stringFromTimeInterval:slider.value];
                         durationLabel.text     =[self stringFromTimeInterval:slider.maximumValue-slider.value];
                         
                         if([self.visit.isVisitCompleted boolValue])
                             recordButton.enabled=NO;
                         else
                             recordButton.enabled =YES;
                     }
                     
                 }];
            }
            
            [_audioManager.audioPlayer setCurrentTime:timerTick];
            float _duration = [self durationOfFileAtPath:recordedFilePath];
            NSTimeInterval interval=recodedAudioDuration>0.0?recodedAudioDuration:_duration;
           //   NSTimeInterval interval=_duration;
            recodedAudioDuration=_duration;
            // MAX(recodedAudioDuration,_audioManager.audioPlayer.duration);  //_audioManager.audioPlayer.duration;
            
            slider.maximumValue=(float)interval;
            durationLabel.text=[self stringFromTimeInterval:interval];
            
            //slider.value=0.0;
            //timerTick=0;
            
            if(sliderTimer) 
            {
                [sliderTimer invalidate];
                sliderTimer=nil;
            }
            
            sliderTimer=[NSTimer scheduledTimerWithTimeInterval:0.005 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
            
        }
        
        //isPlaying=!isPlaying;
    
    //========
     isPlaying=_audioManager.audioPlayer.isPlaying;
        
    sender.selected = isPlaying;
    
    //================== TEST ==============
    if([self.visit.isVisitCompleted boolValue])
        recordButton.enabled=NO;
    else
        recordButton.enabled =!isPlaying;
    //========================================
}
#pragma mark -
#pragma mark SLIDER
-(void)sliderValueDidChange:(UISlider *)_slider
{
    // if(isPlaying || isPaused)     
    {
        //if(_audioManager.audioPlayer && _audioManager.audioPlayer.isPlaying)
        //if (_slider.value == 0) {
        //    _slider.value = 0.1;
        //}
        {
            timerTick=_slider.value;
            [_audioManager.audioPlayer setCurrentTime:_slider.value];
            
            //durationLeftLabel.text = [self stringFromTimeInterval:slider.value];
            durationLabel.text     = [self stringFromTimeInterval:slider.maximumValue-_slider.value]; 
        }
    }
    
}
#pragma mark -
#pragma mark Timer UPDATE TIME
-(void)updateTime:(NSTimer *)timer
{
    if(_audioManager.audioPlayer.isPlaying)//_audioManager.audioPlayer.isPlaying
    {
        BOOL isTracking =  slider.isTracking;
        if(isTracking)
        {
            //durationLeftLabel.text =[self stringFromTimeInterval:slider.value];
            durationLabel.text =[self stringFromTimeInterval:slider.maximumValue-slider.value];
        }
        else
        {
            timerTick= _audioManager.audioPlayer.currentTime;
            slider.value = timerTick;
           // timerTick+=1.0*timer.timeInterval;
            
            //durationLeftLabel.text =[self stringFromTimeInterval:slider.value];
            durationLabel.text     =[self stringFromTimeInterval:slider.maximumValue-slider.value];
        }
    }
    else if(isRecording)
    {
        //slider.value = recordTick;
        
      //  recordTick+=1.0*timer.timeInterval;
      //  recordDurationLabel.text = [self stringFromTimeInterval:recordTick+recodedAudioDuration];
        
        NSTimeInterval _recordCurTime = _audioManager.audioRecorder.currentTime;
        recordDurationLabel.text = [self stringFromTimeInterval:_recordCurTime+recodedAudioDuration];
    }
    
    NSLog(@"Total Audio Duration = %lf",_audioManager.audioPlayer.duration);
    
    //if(slider.value>=recodedAudioDuration)
    if(slider.value>=(float)recodedAudioDuration || !_audioManager.audioPlayer.isPlaying)
    {
        //recordTick=0.0;
        
        if(isPlaying)
        {
            //[self invalidateTimer];
            //timerTick=0.0;
            
            //[self btnPlayClicked:playButton];
            
            [self invalidateTimer];        
            slider.value=0.0;
            timerTick=0;
            
           // if(_audioManager.audioPlayer.isPlaying)
           // [_audioManager stopPlayingAudio];
            
            slider.value=0.0;
            //durationLeftLabel.text =[self stringFromTimeInterval:slider.value];
            durationLabel.text =[self stringFromTimeInterval:slider.maximumValue-slider.value];
            
            isPlaying=NO;
            //isPlaying = !_audioManager.audioPlayer.isPlaying;
            playButton.selected = isPlaying;
        }
    }
    

   // recordButton.enabled = (!isPlaying || isPaused)     
     //recordButton.enabled = (!_audioManager.audioPlayer.isPlaying|| isPaused) ;
    
    //recordButton.enabled = !_audioManager.audioPlayer.isPlaying && slider.value==slider.minimumValue;
    
    /*
    recordButton.enabled =slider.value==slider.minimumValue;
    
    if([self.visit.isVisitCompleted boolValue])
        recordButton.enabled=NO;
     */
    
    //recordButton.enabled = !isPlaying;
    playButton.enabled   = !isRecording;
    
}

-(void)tagAudio:(NSTimeInterval)tag forText:(NSString *)text
{
    timerTick=tag;
    slider.value=tag;
    
    
    ///================= START AUDIO PLAYING WHEN ANY AUDIO TAG IS TAPPED 16JAN13 ======================///

    if(!_audioManager.audioRecorder.isRecording)
    {
       if(!_audioManager.audioPlayer.isPlaying)
       {
         [self playAction:playButton];
       }
    }
    ///================= START AUDIO PLAYING WHEN ANY AUDIO TAG IS TAPPED ======================///
    
    ///=================== CHANGE ======================///
    [_audioManager.audioPlayer setCurrentTime:timerTick];
    ///=================== CHANGE ======================///
}

-(NSTimeInterval)currentRecordingTime
{
    return recodedAudioDuration+_audioManager.audioRecorder.currentTime; //recordTick
}

-(NSString *)_tempCombinedFilePath
{
   // NSArray *paths   = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
   // NSString *recDir = [paths objectAtIndex:0];
    
    NSString *recDir = [DirectoryManager libraryDirectory];
    NSString *tempRecordPath = [recDir stringByAppendingPathComponent:@"tempRecordCombined.m4a"];
    
    NSLog(@"Temp Record Path: %@",tempRecordPath);
    
    return tempRecordPath;
}
-(NSString *)_tempRecordFilePath
{
    //NSArray *paths   = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *recDir = [paths objectAtIndex:0];
    
    NSString *recDir = [DirectoryManager libraryDirectory];

    
    if(![[NSFileManager defaultManager] fileExistsAtPath:recDir])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:recDir withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSString *tempRecordPath = [recDir stringByAppendingPathComponent:@"tempRecord.m4a"];
    
    NSLog(@"Temp Record Path: %@",tempRecordPath);
    
    return tempRecordPath;
}

-(NSString *)_recordFilePath
{
    NSString *customerId = [NSString stringWithFormat:@"%u",[self.visit.visitID hash]];
    NSString *newName    = [NSString stringWithFormat:@"%@_record",customerId];
    
    NSString *recordFileName = self.visit.recordFilePath;
    recordFileName           = recordFileName? recordFileName : [newName stringByAppendingPathExtension:@"m4a"];
    
    /*
    NSString *docuDir = [DirectoryManager documentsDirectory];
    NSString *recDir  = [docuDir stringByAppendingPathComponent:@"SessionAudio"];

    if(![[NSFileManager defaultManager] fileExistsAtPath:recDir])
    {
              NSURL *fileURL = [NSURL fileURLWithPath:recDir];
        
        [[NSFileManager defaultManager] createDirectoryAtURL:fileURL withIntermediateDirectories:YES attributes:nil error:nil];
    }
     
      NSString *_recordFilePath = [recDir stringByAppendingPathComponent:recordFileName];
    */
    
    NSString *_recordFilePath = [DirectoryManager recoredAudioPathForName:recordFileName];
    self.visit.recordFilePath = recordFileName;
    
    return _recordFilePath;
}

-(NSString *)_playFilePath
{
  //  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  //  NSString *recDir = [paths objectAtIndex:0];
    
    NSString *docuDir = [DirectoryManager documentsDirectory];
    NSString *recDir  = [docuDir stringByAppendingPathComponent:@"SessionAudio"];
    
    NSString *recordFileName = self.visit.recordFilePath;
    NSString *_recordFilePath = [recDir stringByAppendingPathComponent:recordFileName];
    
    return _recordFilePath;
}


-(float)durationOfFileAtPath:(NSString *)soundPath
{
    NSURL *afUrl = [NSURL fileURLWithPath:soundPath];
    AudioFileID fileID;
    __unused OSStatus result = AudioFileOpenURL((CFURLRef)afUrl, kAudioFileReadPermission, 0, &fileID);
    Float64 outDataSize = 0;
    UInt32 thePropSize = sizeof(Float64);
    result = AudioFileGetProperty(fileID, kAudioFilePropertyEstimatedDuration, &thePropSize, &outDataSize);
    AudioFileClose(fileID);
    
    return outDataSize;
}

#pragma mark -
#pragma mark SAVE DATA

-(void)saveData
{
    //recodedAudioDuration+=recordTick;
    
    self.visit.lastDraftDate = [NSDate date];
    
    if(recodedAudioDuration>0.0)
        self.visit.recordingDuration = [NSNumber numberWithDouble:recodedAudioDuration];
    
    @try
    {
        NSString *objectId = [NSString stringWithFormat:@"%u", [self.visit.visitID hash]];
    
        [_visitNoteViewController saveBeforeExit];
       NSMutableArray *notes = _visitNoteViewController.notes;
        
       if([notes count])
       {
        //=============== NAME SHOULD BE CHANGED TO VISIT TITLE ===========================
           
          NSString *drawingPath = [DirectoryManager drawingDirectoryPathForName:objectId];
          NSData *data = [NSKeyedArchiver archivedDataWithRootObject:notes];
           
          if(data.length && drawingPath)
          {
            [[NSFileManager defaultManager] createFileAtPath:drawingPath contents:data attributes:nil];
          }
           
           //NSDictionary *attb= [[NSFileManager defaultManager]attributesOfItemAtPath:drawingPath error:nil];
           //NSLog(@"Attb=%@",attb);
       }
        
        NSError *error=nil;
        [[SSCoreDataManager sharedManager] save:error];
     }
    @catch (NSException *exception) 
    {
        NSLog(@"Exception name=%@ reason=%@", [exception name] , [exception reason]);
        [exception print];
    }
    @finally 
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:VisitReviewDidUpdate object:nil];
    }
}


#pragma mark -

#pragma mark STOP PLAY/RECORD

-(void)stopPlay
{
    if(_audioManager)
    [_audioManager stopPlayingAudio];
    [self invalidateTimer];        
    slider.value=0.0;
    timerTick=0;
    
    isPlaying=!isPlaying;
    
    slider.userInteractionEnabled=isPlaying;
}

-(void)stopPlayOrRecording
{
    if(isRecording)
    {
        [self recordAction:recordButton];
    }
    
    if(isPlaying)
    {
        [self stopPlay];
    }
}

#pragma mark DISMISS
-(void)dismiss
{
#if  !BACKGROUND_GPS_ENABLED
    [[LocationManager sharedManager] stopUpdatingLocation];
#endif
    
    app.isVisitSessionRunning = NO;
    if(self.isFromCalendar)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self dismissModalViewControllerAnimated:YES];
    }
    
    //[self clear];
}
#pragma mark
#pragma mark -
#pragma mark BACK ACTION
-(void)backButtonAction:(id)sender
{
   // if(self.visit.isVisitCompleted.boolValue || self.visit.isReportCompleted.boolValue)
   
    if(isPlaying || isRecording)
    {
        //NSString *message = [NSString stringWithFormat:@"Please stop %@ before exit.",isPlaying?@"playing":isRecording?@"recording":@""];
        NSString *message = AUDIO_WARNING_MSG;
        
    UIAlertView *_alert= [UIAlertView alertViewWithTitle:AUDIO_WARNING message:message cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObject:@"Yes"] onDismiss:^(int buttonIndex) 
         {
            // [self stopPlayOrRecording];
             [sender setUserInteractionEnabled:NO];
             
             if(isPlaying)
             {
                 [self stopPlay];
             }
             
            
            [self stopRecord:recordButton onCompletion:^{
                     
                [self saveData];
                [self dismiss];

            }];
             
             /*
             [self saveData];
              app.isVisitSessionRunning = NO;
             if(self.isFromCalendar)
             {
                 [self.navigationController popViewControllerAnimated:YES];
             }
             else
             {
                 [self dismissModalViewControllerAnimated:YES];
             }
             */
         } 
        onCancel:^{
                                   
                                   
        }];
        
        [_alert release];
        
        return;
    }
    
    if(self.visit.isReportCompleted.boolValue || self.visit.isVisitCompleted.boolValue)
    {
        [sender setUserInteractionEnabled:NO];
        
        /*
        if(isPlaying || isRecording) [self stopPlayOrRecording];
        
        [self saveData];
        app.isVisitSessionRunning = NO;
        
        if(self.isFromCalendar)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [self dismissModalViewControllerAnimated:YES];
        }
        return;
         */
        
        
        if(isPlaying)
        {
            [self stopPlay];
        }
        
        [self stopRecord:recordButton onCompletion:^{
            
            [self saveData];
            [self dismiss];

        }];
        
        return;
    }

   
  UIAlertView *_alert= [UIAlertView alertViewWithTitle:@"Warning!" message:VISIT_EXIT_MESSAGE cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObject:@"Yes"] onDismiss:^(int buttonIndex) 
    {
        [sender setUserInteractionEnabled:NO];
        
        /*
        if(self.natureOfVisitPopOver && self.natureOfVisitPopOver.isPopoverVisible)
        [self.natureOfVisitPopOver dismissPopoverAnimated:NO];
        
        [self stopPlayOrRecording];
        [self saveData];
        app.isVisitSessionRunning = NO;
        if(self.isFromCalendar)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [self dismissModalViewControllerAnimated:YES];
        }
        */
        
        if(self.natureOfVisitPopOver && self.natureOfVisitPopOver.isPopoverVisible)
            [self.natureOfVisitPopOver dismissPopoverAnimated:NO];
        
        if(isPlaying)
        {
            [self stopPlay];
        }
        
        [self stopRecord:recordButton onCompletion:^{
            
            [self saveData];
            [self dismiss];
        }];

    } 
    onCancel:^{
                                                      
        
   }];
    
    [_alert release];
}
#pragma mark -
#pragma mark PUBLISH REPORT
-(void)publishReportAction:(UIButton *)sender
{
    BOOL _isVisitCompleted=self.visit.isVisitCompleted.boolValue;
    
    if(!_isVisitCompleted)
    {
       UIAlertView *alertView = [UIAlertView alertViewWithTitle:@"" message:VISIT_COMPLETE_MSG cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObject:@"Yes"] onDismiss:^(int buttonIndex) 
        {
             app.isVisitSessionRunning = NO;
             self.visit.isVisitCompleted   = [NSNumber numberWithBool:YES];
             self.visit.visitCompletedDate = [NSDate date];
             sender.selected = YES;
            
             [self trackCurrentLocation];
            
           /*
            if(isRecording)
            {
                [self recordAction:recordButton];
            }
            */
            
            [self stopRecord:recordButton onCompletion:^{
                
                recordButton.enabled = NO;
                playButton.enabled = recodedAudioDuration>0;
                
                recordButton.userInteractionEnabled=NO;
                recordButton.alpha=0.6;
                
                [taskListViewController markAsComplete];
                [customerOfferViewController markAsComplete];
                //[_visitNoteViewController makeReportCompleted];
                [photosViewController markAsComplete];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:VisitDidEndNotification object:nil];

            }];
                  
        } 
        onCancel:^{}];
        
        //[alertView show];
        [alertView release];
    }
    else 
    {
        BOOL _isReportCompleted=self.visit.isReportCompleted.boolValue;
        
        if(!_isReportCompleted)
        {
           UIAlertView *alertView=[UIAlertView alertViewWithTitle:@"" message:REPORT_COMPLETE_MSG cancelButtonTitle:@"No" otherButtonTitles:[NSArray arrayWithObject:@"Yes"] onDismiss:^(int buttonIndex) 
              {
                                   
                [self stopPlayOrRecording];
                  
                 

                 
                //=============== UPLOAD VISIT DETAILS ========================
                self.visit.reportCompletedDate = [NSDate date];
                  
                [self uploadVisitDetails:^(BOOL success)
                {
                  if(success)
                   {
                     sender.enabled = NO;
                     self.visit.isReportCompleted   = [NSNumber numberWithBool:YES];
                     self.visit.isVisitCompleted    = [NSNumber numberWithBool:YES];
                     self.visit.scheduledVisit.isVisitPublished = [NSNumber numberWithBool:YES];
                     // self.visit.reportCompletedDate = [NSDate date];
                     self.visit.endDateIntervalSinceRefDate =[NSNumber numberWithDouble:[NSDate timeIntervalSinceReferenceDate]];
                     
                     if(photosViewController)
                     [photosViewController markAsComplete];
                     
                     [self saveData];
                     [self dismiss];
                  }
                  else
                   {
                    // [UIAlertView showWarningAlertWithTitle:@"" message:POOR_CONNECTION_MSG_REPORT_COULDNOT_BE_PUBLISHED];
                   }
                                         
              }];

            } 
                                                      
            onCancel:^{}];
        
        //[alertView show];
        [alertView release];
    }
    
    }
}

#pragma mark -

-(void)uploadVisitDetails:(void (^)(BOOL success))completion
{
    /*
     if(!self.visit.reportSummary.length)  
     {
     [UIAlertView showWarningAlertWithTitle:nil message:@"Please input report summary"];
     
     return;
     }
     */
    
    if(![[OfflineManager sharedManager] isNetworkAvailable])
    {
        [UIAlertView showWarningAlertWithTitle:@"" message:POOR_CONNECTION_MSG_TRY_AGAIN_LATER];
        if(completion) completion(NO);
        return;
    }
        
    
   __block UIView *uploadingView = [[UIView alloc] initWithFrame:app.window.bounds];
    uploadingView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    uploadingView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [app.window addSubview:uploadingView];
    
   __block ProgressHUD *progress = [ProgressHUD showHUDAddedTo:self.view animated:YES];
    progress.mode = MBProgressHUDModeDeterminate;
    progress.tag=NSIntegerMax;
    progress.labelText = @"Uploading images...";
    //[progress showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];
    
    __block VisitSessionViewController *weakSelf = self;
    
    //====== IF PHOTOS NOT AVAILABLE OR IMAGES ALREADY UPLOADED, UPLOAD VISIT DETAILS INSTANT ========
    if(![self.visit.photos count] || self.visit.isImagesUploaded.boolValue)
    {
        //============================ NOW UPLOAD VISIT TEXT NOTES ===============================
        progress.mode = MBProgressHUDModeIndeterminate;
        progress.labelText = @"Saving..";
        
        NSArray *visitNotes = [_visitNoteViewController textNotes];
        
      __block  ASIHTTPRequest *request = [ConnectionManager uploadVisitDetailsForVisit:self.visit visitReports:visitNotes];
        request.timeOutSeconds = 50.0;
        
        [request setCompletionBlock:^{
            
           // NSString *responseString = request.responseString;
            
            NSDictionary* responseDict = request.responseJSON;
            
            responseDict = NULL_NIL(responseDict);
            
            NSLog(@"Response:status =  %@",[responseDict valueForKey:@"status"]);
           // NSLog(@"Response:%@",responseString);
            [ProgressHUD hideHUDForView:weakSelf.view animated:YES];
            [uploadingView removeFromSuperview];
            [uploadingView release];
            
           BOOL isResponseStatus = [[responseDict valueForKey:@"status"] boolValue];
            
            NSNumber *isUnderMaintenance  = [responseDict objectForKey:JSON_UNDER_MAINTENANCE_KEY];
            isUnderMaintenance = NULL_NIL(isUnderMaintenance);
            if([isUnderMaintenance boolValue])
            {
                [app showUnderMaintainenceScreen:YES];
            }
            else
            {
            
            //======== UPDATE SCHEDULED VISITID IF NOT SYNCED WITH SERVER ==========
            
            if(![weakSelf.visit.scheduledVisit.isScheduleVisitSentToServer boolValue])
            {
                NSNumber *scheduleVisitID = [responseDict valueForKey:@"scheduleVisitID"];
                scheduleVisitID = NULL_NIL(scheduleVisitID);
                
                if([scheduleVisitID longLongValue])
                {
                    if(![weakSelf.visit.scheduledVisit.scheduleVisitID isEqualToNumber:scheduleVisitID])
                    {
                        weakSelf.visit.scheduledVisit.scheduleVisitID =scheduleVisitID;
                        weakSelf.visit.scheduledVisit.isScheduleVisitSentToServer = [NSNumber numberWithBool:YES];
                        // [[SSCoreDataManager sharedManager] save:nil];
                    }
                }
            }
            
            //======== UPDATE SCHEDULED VISITID IF NOT SYNCED WITH SERVER ==========
            
           
            
            NSLog(@"Visit data uploaded %@",isResponseStatus?@"Successfully":@"Failed");
            
            //NSLog(@"Visit data uploaded succesfully");
            // [self dismissModalViewControllerAnimated:YES];
            
            if(!isResponseStatus &&  ![isUnderMaintenance boolValue])
                [UIAlertView showWarningAlertWithTitle:@"" message:POOR_CONNECTION_MSG_REPORT_COULDNOT_BE_PUBLISHED];
            }
            
            if(completion) completion(isResponseStatus);
            
        }];
        
        [request setFailedBlock:^{
            
            [ProgressHUD hideHUDForView:weakSelf.view animated:YES];
            [uploadingView removeFromSuperview];
            [uploadingView release];
            
            [UIAlertView showWarningAlertWithTitle:@"" message:POOR_CONNECTION_MSG_FOR_DATA_UPLOAD_FAILED];
            
            if(completion) completion(NO);
        }];

        return;
    }
    
    //============= IF VISIT PHOTOS AVAILABLE FIRST UPLOAD IMAGES THEN UPLOAD VISIT DETAILS =========
    
  __block  ASIHTTPRequest *imageRequest =[ConnectionManager uploadImagesForVisit:self.visit];
    imageRequest.showAccurateProgress = YES;
    [imageRequest setUploadProgressDelegate:progress];
    imageRequest.timeOutSeconds = 40.0;
    
    // ==================== UPLOAD PROGRESS BLOCK ===============================
    /*
     if ([imageRequest totalBytesSent] > 0)
     {
       float progressAmount = (float) ([imageRequest totalBytesSent]/[imageRequest postLength]);
       progress.progress = progressAmount;
     }
     */
    // ============================ UPLOAD PROGRESS BLOCK ===============================
    
    [imageRequest setCompletionBlock:^{
        
        progress.progress = 1.0;
        progress.mode = MBProgressHUDModeIndeterminate;
        progress.labelText = @"Saving..";
        
        NSString *res = [imageRequest responseString];
        res= [res stringByReplacingOccurrencesOfString:@"^%#*" withString:@"="];
        NSString *response= [res stringByReplacingOccurrencesOfString:@"*%@$" withString:@","];
        
        NSArray *arr = [response componentsSeparatedByString:@"originalFileName="];
        
        NSLog(@"Arr= %@",arr);
        
        
        NSMutableSet *allPhotos= [NSMutableSet setWithSet:weakSelf.visit.photos];
        
        for(NSString *str in arr)
        {
            if(!str.length) continue;
            
            NSArray *infos = [str componentsSeparatedByString:@","];
            
            if([infos count]< 3) continue;
            
            NSString *originalImageName = [infos objectAtIndex:0];
            NSString *uploadedImageLink = [infos objectAtIndex:2];
            uploadedImageLink = [uploadedImageLink stringByReplacingOccurrencesOfString:@"path=" withString:@""];
            
            // ================== LOGIC SHOULD BE OPTIMIZED HERE =====================
            
            VisitPhoto *currentPhoto=nil;
            for(VisitPhoto *photo in allPhotos)
            {
                NSString *imageFilePath  = photo.imagePath;
                NSString *imageFileName= [imageFilePath lastPathComponent];
                
                /*
                //=================== REMOVE LARGE IMAGE AFTER UPLOADING TO SERVER =======================
                if([[NSFileManager defaultManager] fileExistsAtPath:imageFilePath])
                {
                    [[NSFileManager defaultManager] removeItemAtURL:[NSURL fileURLWithPath:imageFilePath] error:nil];
                }
                */
                
                //========================================================================================
                
                if([imageFileName isEqualToString:originalImageName])
                {
                    photo.serverImageLink = uploadedImageLink;
                    photo.imageName = [uploadedImageLink lastPathComponent];
                    currentPhoto = photo;
                    break;
                }
            }
            
            //Remove the already processed object so that we dont need to check it in the above loop.
            if(currentPhoto) [allPhotos removeObject:currentPhoto];
            // ============== LOGIC SHOULD BE OPTIMIZED HERE =====================
            
            NSLog(@"ImageLink = %@",uploadedImageLink);
            NSLog(@"Original File Name = %@",originalImageName);
        }
        
        weakSelf.visit.isImagesUploaded = [NSNumber numberWithBool:YES];
        //============================ NOW UPLOAD VISIT TEXT NOTES ===============================
       
        
        NSArray *visitNotes     = [weakSelf->_visitNoteViewController textNotes];
      __block  ASIHTTPRequest *request = [ConnectionManager uploadVisitDetailsForVisit:weakSelf.visit visitReports:visitNotes];
        request.timeOutSeconds = 40.0;
        
        
        [request setCompletionBlock:^{
            
          //  NSString *responseString   = request.responseString;
            
            NSDictionary* responseDict = request.responseJSON;
            NSLog(@"Response:status =  %@",[responseDict valueForKey:@"status"]);
            NSLog(@"Response:%@",responseString);
            // [ProgressHUD showSuccessWithStatus:@"Great Success!"];
            [ProgressHUD hideHUDForView:weakSelf.view animated:YES];
            [uploadingView removeFromSuperview];
            [uploadingView release];
            
            BOOL IsResponseStatus = [[responseDict valueForKey:@"status"] boolValue];
            NSNumber *isUnderMaintenance  = [responseDict objectForKey:JSON_UNDER_MAINTENANCE_KEY];
            
            isUnderMaintenance = NULL_NIL(isUnderMaintenance);
            
            if([isUnderMaintenance boolValue])
            {
                [app showUnderMaintainenceScreen:YES];
            }
            else
            {
            
            //======== UPDATE SCHEDULED VISITID IF NOT SYNCED WITH SERVER ==========
            
            if(![weakSelf.visit.scheduledVisit.isScheduleVisitSentToServer boolValue])
            {
              NSNumber *scheduleVisitID = [responseDict valueForKey:@"scheduleVisitID"];
              scheduleVisitID = NULL_NIL(scheduleVisitID);
            
               if([scheduleVisitID intValue])
               {
                 if(![weakSelf.visit.scheduledVisit.scheduleVisitID isEqualToNumber:scheduleVisitID])
                 {
                   weakSelf.visit.scheduledVisit.scheduleVisitID =scheduleVisitID;
                   weakSelf.visit.scheduledVisit.isScheduleVisitSentToServer = [NSNumber numberWithBool:YES];
                  // [[SSCoreDataManager sharedManager] save:nil];
                 }
              }
             }
            
             //======== UPDATE SCHEDULED VISITID IF NOT SYNCED WITH SERVER ==========
            }
            if (IsResponseStatus) 
            {
                NSLog(@"Visit data uploaded succesfully");
                completion(YES);
            }
            else 
            {
                if(![isUnderMaintenance boolValue])
                [UIAlertView showWarningAlertWithTitle:@"" message:POOR_CONNECTION_MSG_REPORT_COULDNOT_BE_PUBLISHED];
                
                completion(NO);
            }
            //NSLog(@"Visit data uploaded succesfully");
            // [self dismissModalViewControllerAnimated:YES];
            
            //if(completion) completion(YES);
        }];
        [request setFailedBlock:^{
            
            [ProgressHUD hideHUDForView:weakSelf.view animated:YES];
            [uploadingView removeFromSuperview];
            [uploadingView release];
            
            [UIAlertView showWarningAlertWithTitle:@"" message:POOR_CONNECTION_MSG_FOR_DATA_UPLOAD_FAILED];
            
            if(completion) completion(NO);
        }];
        
    }];
    
    
    [imageRequest setFailedBlock:^{
        
        //@"Poor connection, data failed to upload. Data will upload automatically once proper connection established"
        [UIAlertView showWarningAlertWithTitle:@"" message:POOR_CONNECTION_MSG_FOR_DATA_UPLOAD_FAILED];
        [ProgressHUD hideHUDForView:weakSelf.view animated:YES];
        [uploadingView removeFromSuperview];
        [uploadingView release];
        
        if(completion) completion(NO);
    }];
}

-(void)localNotificationDidArrive:(NSNotification *)notification
{
    UILocalNotification *localNotif = (UILocalNotification *)notification.object ;
    
    NSTimeInterval _interval=[[NSDate date] timeIntervalSinceDate:self.lastLocalNotificationDate];
    
    if(self.lastLocalNotificationDate && _interval<kApplicationTimeoutInMinutes*60)
    {
        return;
    }
    
   self.lastLocalNotificationDate = [NSDate date];
    
   UIAlertView *alert = [UIAlertView alertViewWithTitle:@"" message:localNotif.alertBody cancelButtonTitle:@"No"  otherButtonTitles:[NSArray arrayWithObject:@"Yes"] onDismiss:^(int buttonIndex)
    {
        if(buttonIndex==0)
        {
            //self.visit.lastDraftDate = [[NSDate date] dateByAddingTimeInterval:-kApplicationTimeoutInMinutes*60.0f];
            
            NSTimeInterval _inactiveSec = [self.visit.inactiveSeconds doubleValue];
            _inactiveSec+=kApplicationTimeoutInMinutes*60.0f;
            self.visit.inactiveSeconds = [NSNumber numberWithDouble:_inactiveSec];
        }
    } 
    onCancel:^{
        
        app.isVisitSessionRunning = NO;
        self.lastLocalNotificationDate = nil;
        [self.natureOfVisitPopOver dismissPopoverAnimated:NO];
        [self stopPlayOrRecording];

        [self saveData];
        [self dismiss];
    }];
    
   // [alert retain]; 
      [alert show];
         
   // [self performSelector:@selector(dismissAlert) withObject:alert afterDelay:1.0];
   // [alert release];
}

#pragma mark -
#pragma mark AppDidChangeSiteID notification
-(void)appDidChangeSiteID:(NSNotification *)notif
{
    app.isVisitSessionRunning = NO;
    [self.natureOfVisitPopOver dismissPopoverAnimated:NO];
    [self stopPlayOrRecording];
    
    //[self saveData];
    [self dismiss];
}
#pragma mark -

-(void)repDidDeactivate:(NSNotification *)notif
{
    app.isVisitSessionRunning = NO;
    [self.natureOfVisitPopOver dismissPopoverAnimated:NO];
    [self stopPlayOrRecording];
    
    //[self saveData];
    [self dismiss];
}

-(void)appWillResignActive:(NSNotification *)notif
{
    if(isPlaying)
    {
        [self playAction:playButton];
    }
}

-(void)dismissAlert:(UIAlertView *)alert
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}
- (void)viewDidUnload
{
     [[NSNotificationCenter defaultCenter] removeObserver:self];
    
     [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
