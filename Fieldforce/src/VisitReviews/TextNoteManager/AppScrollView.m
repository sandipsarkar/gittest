//
//  AppScrollView.m
//  TestLayer
//
//  Created by elie maalouly on 3/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppScrollView.h"

@implementation AppScrollView

- (id)initWithFrame:(CGRect)frame 
{
    return [super initWithFrame:frame];
}

// This fixes it for iOS 5 / Xcode 4.2
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.nextResponder touchesBegan:touches withEvent:event];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event 
{
    if (self.dragging)
    {
        [super touchesEnded:touches withEvent:event];
        
    }
    else
    {
        NSLog(@"self.nextResponder=%@",self.nextResponder);
        [self.nextResponder touchesEnded:touches withEvent:event];
    }
}

@end
