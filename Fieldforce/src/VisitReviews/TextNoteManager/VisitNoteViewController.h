//
//  VisitNoteViewController.h
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AttributedLabel.h"
#import "NSAttributedString+Attributes.h"

typedef enum {
    
    NoteModeText=0,
    NoteModeDraw,
    
} NoteMode;

@class VisitSessionViewController;
@interface VisitNoteViewController : UITableViewController
{
    BOOL _isReportCompleted;
    BOOL _isEditMode;
    NoteMode _currentMode;
}


@property(nonatomic,retain) NSMutableArray *notes;
@property(nonatomic,assign) NoteMode currentMode;
@property(nonatomic,assign) VisitSessionViewController *visitSessionViewController;

- (id)initWithNotes:(NSMutableArray *)_notes;

-(void)resetSelection;
-(void)enableEditMode:(BOOL)editMode;
-(BOOL)isEditModeEnabled;

-(void)tagWithOEM:(NSString *)oemName;
-(void)deleteSelectedContainers;
-(NSArray *)textNotes;

-(void)resignResponder;
-(void)makeReportCompleted;

-(BOOL)saveBeforeExit;

@end
