//
//  VisitNoteViewController.m
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "VisitNoteViewController.h"
#import "HPGrowingTextView.h"
#import "AttributedLabel.h"
#import "NSAttributedString+Attributes.h"
#import "EGOTextView.h"
#import "DrawingCanvasView.h"
#import "Shape.h"
#import "SSCheckBox.h"
#import "VisitSessionViewController.h"
#import "AudioManager.h"

#define DRAWING_CONTAINER_SPACE_Y 40.0 //20.0
#define KEY_TEXT @"text"
#define KEY_DRAWING @"drawing"
#define KEY_TIME @"audioTime"
#define KEY_TAG @"_oem"
#define KEY_TAG_ID @"_oem_id"
#define KEY_FOOTER_HEIGHT @"footerHeight"

@interface VisitNoteViewController()<HPGrowingTextViewDelegate,UITextViewDelegate,DrawingDelegate,EGOTextViewDelegate>
{
    UITextView *entryTextView;
    DrawingCanvasView *_paintingView;
    
    UITextView *activeTextView;
    BOOL _isAreaExpanded;
    
    BOOL _refreshPainting;
    
    NSTimeInterval _drawingAudioTag;
    NSTimeInterval _textAudioTag;
    
    BOOL _scrollEntryTextViewToTop;
    BOOL _isKeyboardAppeared;
    
   
}

-(void)enableDrawMode;
-(void)enableTextMode;
-(void)reloadTableView;
-(void)manageVisibilityOfInputView;
-(void)scrollToNewContainer;
-(UITableViewCell *)cellForView:(UIView *)view;

@property(nonatomic,retain)  NSIndexPath *editingIndexPath;
@property(nonatomic,retain)  NSIndexPath *insertedTempIndexPath;
@property (nonatomic,retain) NSMutableIndexSet *selectedIndexes;

@end

@implementation VisitNoteViewController
@synthesize notes;
@synthesize currentMode=_currentMode;
@synthesize editingIndexPath;
@synthesize insertedTempIndexPath;
@synthesize visitSessionViewController;
@synthesize selectedIndexes;

#pragma mark -
#pragma mark INITIALIZE
- (id)init
{
    self = [super initWithStyle:UITableViewStylePlain];
    
    if (self) 
    {
        _isEditMode = NO;
        _drawingAudioTag = -1.0;
        _textAudioTag = -1.0;
        
        notes = [[NSMutableArray alloc] init];
        
        selectedIndexes = [[NSMutableIndexSet alloc] init];

    }
    
    return self;
}

- (id)initWithNotes:(NSMutableArray *)_notes
{
    self = [super initWithStyle:UITableViewStylePlain];
    
    if (self) 
    {
        _isEditMode = NO;
        _drawingAudioTag = -1.0;
        _textAudioTag = -1.0;
        
        notes = _notes ? [_notes retain] :[[NSMutableArray alloc] init];
        
        selectedIndexes = [[NSMutableIndexSet alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidAppear:) name:UIKeyboardDidShowNotification object:nil];
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidDismiss:) name:UIKeyboardWillHideNotification object:nil];
        
    }
    
    return self;
}

#pragma mark -
#pragma mark DEALLOC
-(void)dealloc
{
     [[NSNotificationCenter defaultCenter] removeObserver:self];
    visitSessionViewController = nil;
    [notes release];
    [entryTextView release]  ;
    [editingIndexPath release] ;
    [_paintingView release] ;
    [insertedTempIndexPath release];

    [selectedIndexes release];
    
   
    [super dealloc];
}

#pragma mark -

#pragma mark TEXTNOTES

-(NSArray *)textNotes
{
    NSMutableArray *_textNotes=[NSMutableArray array];
    
    for(NSDictionary *dict in self.notes)
    {
        NSString *textNote  = [dict valueForKey:KEY_TEXT];
        NSString *oemTag    = [dict valueForKey:KEY_TAG];
        NSString *oemTagID  = [dict valueForKey:KEY_TAG_ID];
        
        if(!textNote.length) continue;
        
        
        if(!oemTagID.length) oemTagID = @"0";
        
        NSMutableDictionary *uploadNoteDict = [NSMutableDictionary dictionary];
        
        [uploadNoteDict setValue:textNote forKey:@"visitNote"];
        [uploadNoteDict setValue:oemTagID forKey:@"oemID"];
        [uploadNoteDict setValue:oemTag?oemTag:@"" forKey:@"oemName"];
        
        [_textNotes addObject:uploadNoteDict];
        
        /*
        if(textNote)
        {
            NSString *noteText = oemTagID.length? [oemTagID stringByAppendingFormat:@"=%@",textNote] : textNote;
            [_textNotes addObject:noteText];
        }
        */
    }
    
    NSLog(@"Text Notes:%@",_textNotes);
    
/*
   //OEMName = <text note> + OEMName = <text note> + OEMName = <text note>
   return [_textNotes count] ? [_textNotes componentsJoinedByString:@"+"] : nil;
*/
    
    return _textNotes;
}

#pragma mark -

-(void)makeReportCompleted
{
    self.tableView.tableFooterView.userInteractionEnabled = NO;
    entryTextView.userInteractionEnabled = NO;
    UITapGestureRecognizer *tapGestureRecognizer =[self.tableView.tableFooterView.gestureRecognizers objectAtIndex:0];
    tapGestureRecognizer.enabled = NO;
    
}
#pragma mark MANAGE KEYBOARD
-(void)keyboardWillAppear:(NSNotification *)aNotification
{
    CGRect keyboardRect = [[[aNotification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
    
    //if(UIEdgeInsetsEqualToEdgeInsets(tableViewContentInsets, UIEdgeInsetsZero))
      //  tableViewContentInsets = self.tableView.contentInset;
    
    UIEdgeInsets _tableInset = self.tableView.contentInset;
    _tableInset.bottom = keyboardRect.size.height;
    
    [UIView animateWithDuration:0.5 animations:^{
        
        self.tableView.contentInset=_tableInset;
    
    }];
    
   // [self performSelector:@selector(animateEntyTextViewToTop) withObject:nil afterDelay:0.2];
}

-(void)keyboardDidAppear:(NSNotification *)aNotification
{
    _isKeyboardAppeared=YES;
    
    if(!_scrollEntryTextViewToTop) return;
    
    [self performSelector:@selector(animateEntyTextViewToTop) withObject:nil afterDelay:0.4];
    
    _scrollEntryTextViewToTop = NO;
}

-(void)keyboardDidDismiss:(NSNotification *)aNotification
{
    _isKeyboardAppeared = NO;
    self.tableView.contentInset=UIEdgeInsetsMake(0, 0, 0, 0);
    
}

-(void)animateEntyTextViewToTop
{
    if(_isEditMode) return;
    CGRect targetRect = self.tableView.tableFooterView.frame;
    targetRect = [self.tableView convertRect:targetRect fromView:self.tableView];
    CGPoint _targetOffset = targetRect.origin;
    //_targetOffset.y-=self.tableView.contentInset.bottom;
    
    CGPoint _curContentOffset = self.tableView.contentOffset;
    float diffY = _targetOffset.y - _curContentOffset.y;
    NSTimeInterval _animationInterval = MAX(0.5, diffY/640.0);
   // _animationInterval = MIN(_animationInterval, 0.9);
    
    [UIView animateWithDuration:_animationInterval animations:^{
        
        [self.tableView setContentOffset:_targetOffset animated:NO];
    } 
     completion:^(BOOL finished) 
    {
        
     
    }];
}



#pragma mark -

-(void)resignResponder
{
    if([self respondsToSelector:@selector(resignFirstResponder)])
       [self resignFirstResponder];
    
    //if([entryTextView.internalTextView isFirstResponder])
     //  [entryTextView.internalTextView resignFirstResponder];
    
    if([entryTextView isFirstResponder])
        [entryTextView resignFirstResponder];
    
    if([activeTextView isFirstResponder])
       [activeTextView resignFirstResponder];
}
#pragma mark CALCULATE DRAWING CONTAINER HEIGHT
-(CGFloat)calculateHeightOfDrawingContainer:(NSMutableArray *)drawings
{
    CGFloat minY=-CGFLOAT_MIN;
    CGFloat maxY=0;
    
    @try
    {
        
    for(Shape *shape in drawings)
    {
        CGRect shapeBounds = [shape totalBounds];
        
        if(CGRectIsEmpty(shapeBounds) || CGRectIsInfinite(shapeBounds)) continue;
        
        if(minY ==-CGFLOAT_MIN || CGRectGetMinY(shapeBounds)<minY)
        {
            minY = CGRectGetMinY(shapeBounds);
            
            minY = MAX(minY, 0);
        }
        
        if(CGRectGetMaxY(shapeBounds)>maxY)
            maxY = CGRectGetMaxY(shapeBounds);
        
       // [shape translateBy:CGPointMake(0,-CGRectGetMinY(shapeBounds)+10.0)];
    }
    
    
    //============ REDUCE OFFSET Y FROM EACH SHAPE ==============
    
    if(minY>0)
    //if(minY!=0)
    {
        
        for(Shape *shape in drawings)
        {
           [shape translateBy:CGPointMake(0,-minY+DRAWING_CONTAINER_SPACE_Y/4.0)]; //-minY+10.0
        }
    }
    //===========================================================
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception=%@",exception.reason);
    }
    @finally
    {
        CGFloat height= (minY!=maxY) ? (maxY-minY)  : self.tableView.tableFooterView.frame.size.height;
        // CGFloat height= (minY!=maxY) ? fabsf(maxY-minY)  : maxY;
        
         return height;
    }
}

-(BOOL)saveBeforeExit
{
    @try {
        
        [self createDrawingContainer];
        [self createTextContainerFromTextView:entryTextView];
    }
    @catch (NSException *exception)
    {
        [exception print];
    }
    @finally
    {
        
        return YES;
    }
   
}

#pragma mark -
#pragma mark CREATE DRAWING CONTAINER
-(void)createDrawingContainer
{
    //=========== CHANGED FROM ================
    
    if(self.notes.count)
    {
         ///=============== CHANGED TO ===================
         if(![_paintingView.shapes count])
         {
            _paintingView.userInteractionEnabled = NO;
         return;
         }
    }
    else
    {
     
    if(![_paintingView.shapes count])
    {
        
        ///////////======================================================//////////
        BOOL isDrawingMode = (self.currentMode == NoteModeDraw);
        
        if(isDrawingMode)
        {
           UITapGestureRecognizer *tapGestureRecognizer =[self.tableView.tableFooterView.gestureRecognizers objectAtIndex:0];
           tapGestureRecognizer.enabled = NO;
           _paintingView.userInteractionEnabled = YES;
        }
        ///=============================================================///////
        
        return;
    }
        
    }
    
    
    
    //////////////////////////////////////////
      _paintingView.userInteractionEnabled = NO;
    ///////////////////////////////////////////
    
       UITapGestureRecognizer *tapGestureRecognizer =[self.tableView.tableFooterView.gestureRecognizers objectAtIndex:0];
       tapGestureRecognizer.enabled = YES;
    
        NSMutableDictionary *noteDict  = [NSMutableDictionary dictionary];
        NSInteger sectionToInsert     = [notes count];
        
        NSMutableArray *drawings =[NSMutableArray arrayWithArray:_paintingView.shapes];
        [noteDict setValue:drawings forKey:KEY_DRAWING];
    
    //================================== CALCULATE CONTAINER HEIGHT ======================================
        CGFloat containerHeight = [self calculateHeightOfDrawingContainer:drawings];    
        [noteDict setValue:[NSNumber numberWithFloat:containerHeight+DRAWING_CONTAINER_SPACE_Y] forKey:@"drawingHeight"];
        [notes addObject:noteDict];
    
        [_paintingView clear];
    
        if(_isEditMode)
        self.insertedTempIndexPath = [NSIndexPath indexPathForRow:0 inSection:sectionToInsert];
        
        [self.tableView beginUpdates];
        [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionToInsert] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];

}

#pragma mark -
#pragma mark CREATE TEXT CONTAINER
//-(void)createTextContainerFromTextView:(HPGrowingTextView *)_textView
-(void)createTextContainerFromTextView:(UITextView *)_textView
{
    if(!notes) notes = [[NSMutableArray alloc] init];
    
    NSString *text = _textView.text;
    if(text.length)
    {
        NSDictionary *noteDict=[[NSMutableDictionary alloc] initWithCapacity:2];
        
        
        //========================= SAVE THE CURRENT TIME-OFFSET HERE ==================
        //if(_textAudioTag!=-1.0  && [self.visitSessionViewController.audioManager isRecording])
        if(_textAudioTag!=-1.0)
        {
            // NSString *_currentWord=[_textView.text stringByAppendingFormat:@"%d%d",];
            // [self setAudioOffset:_curTimeOffset forWord:_currentWord];
            [noteDict setValue:[NSNumber numberWithDouble:_textAudioTag] forKey:KEY_TIME];
            _textAudioTag=-1.0;
        }
        //========================= SAVE THE CURRENT TIME-OFFSET HERE ==================
        
        
        NSInteger sectionToInsert = [notes count];
        [noteDict setValue:text forKey:KEY_TEXT];
       // [noteDict setValue:[NSNumber numberWithDouble:_textView.internalTextView.contentSize.height+30.0] forKey:@"height"];
         [noteDict setValue:[NSNumber numberWithFloat:_textView.contentSize.height+30.0] forKey:@"height"];
        [notes addObject:noteDict];
        [noteDict release];
        
        if(_isEditMode)
        self.insertedTempIndexPath = [NSIndexPath indexPathForRow:0 inSection:sectionToInsert];
        
        [self.tableView beginUpdates];
        [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionToInsert] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
        
        _textView.text = nil;
    }
    
}
#pragma mark -
#pragma mark MANAGE EDIT MODE
-(void)enableEditMode:(BOOL)editMode
{
    if(_isEditMode==editMode) return;
    
    _isEditMode = editMode;
    
    //================= changed ===================
   // _paintingView.hidden = editMode;
     //entryTextView.hidden = editMode;
    
    /*
    if(editMode)
    {
        _paintingView.userInteractionEnabled =NO;;
        entryTextView.userInteractionEnabled = NO;

    }
     */
    //=============================================
    
    /////////////////////////////////////////////////////////////////////
    self.tableView.tableFooterView.userInteractionEnabled = !editMode;
    //[entryTextView.internalTextView resignFirstResponder];
    [entryTextView resignFirstResponder];
    ////////////////////////////////////////////////////////////////////
    
    self.tableView.tableFooterView.backgroundColor = editMode ? [[UIColor lightGrayColor] colorWithAlphaComponent:0.5] : [UIColor clearColor];
    
    if([activeTextView isFirstResponder])
        [activeTextView resignFirstResponder];
    
    
    self.selectedIndexes = [NSMutableIndexSet indexSet];
    
    
   // if(_currentMode == NoteModeDraw && self.editingIndexPath)
    if(self.editingIndexPath)
    {
        
    NSMutableDictionary *noteDict  = (self.editingIndexPath.section < [self.notes count]) ? [self.notes objectAtIndex:self.editingIndexPath.section] : nil;
        
    NSMutableArray *drawings = [noteDict valueForKey:KEY_DRAWING];
    NSString *textNote = [noteDict valueForKey:KEY_TEXT];
    
    if(drawings)
    {
    //========================= CALCULATE CONTAINER HEIGHT ===========================
        
    CGFloat containerHeight = [self calculateHeightOfDrawingContainer:drawings];    
    [noteDict setValue:[NSNumber numberWithFloat:containerHeight+DRAWING_CONTAINER_SPACE_Y] forKey:@"drawingHeight"];
        
     UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.editingIndexPath];
     DrawingCanvasView *paintingView = (DrawingCanvasView *)[cell.contentView viewWithTag:1];
     [noteDict setValue:paintingView.shapes forKey:KEY_DRAWING];
     paintingView.currentMode = PaintingModeSelect;
     [paintingView disblePanning:YES];
     [paintingView reload];
        
    _paintingView.userInteractionEnabled = NO;
    
    _refreshPainting = YES;
        
     if(self.editingIndexPath)
     {
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:self.editingIndexPath] withRowAnimation:UITableViewRowAnimationNone];
     }
    }
    else if(textNote.length)
    {
       
    }
     
    _isAreaExpanded = NO;
        
   // [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:self.editingIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        
    //[self.tableView reloadData];
        
        NSArray *visibleRows = [self.tableView indexPathsForVisibleRows];
        if([visibleRows count])
        [self.tableView reloadRowsAtIndexPaths:visibleRows withRowAnimation:UITableViewRowAnimationFade];
      
     if(self.editingIndexPath)
     {
         NSIndexPath *indexPathToScroll = [NSIndexPath indexPathForRow:self.editingIndexPath.row inSection:self.editingIndexPath.section];
         
         [self.tableView scrollToRowAtIndexPath:indexPathToScroll atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
     }
        
    self.tableView.scrollEnabled = YES;
    self.editingIndexPath = nil;  
    //_refreshPainting = NO;
        
    }
    else 
    {
        BOOL shouldCreateNewContainer = YES;
        
        if(!_isEditMode && self.insertedTempIndexPath)
        {
            NSDictionary *noteDict =self.insertedTempIndexPath.section<[self.notes count] ? [self.notes objectAtIndex:self.insertedTempIndexPath.section] : nil;
            
            if(_currentMode==NoteModeText)
            {
              entryTextView.text = [noteDict valueForKey:KEY_TEXT];
            
            }
            else if(_currentMode==NoteModeDraw)
            {
                if(noteDict)
                {
                    NSMutableArray *drawings = [noteDict valueForKey:KEY_DRAWING];
                    if([drawings count])
                    {
                       _paintingView.shapes = drawings;
                       [_paintingView reload];
                       [self scrollToNewContainer];
                    }
                }
                
            }
            
            shouldCreateNewContainer = NO;
            if(noteDict)
            [self.notes removeObjectAtIndex:self.insertedTempIndexPath.section];
            self.insertedTempIndexPath=nil;
        }
        
        
        if(shouldCreateNewContainer)
        {
           
            [self createDrawingContainer];
            [self createTextContainerFromTextView:entryTextView];
        }
        
        
       [self.tableView reloadData];
        
       // [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.notes.count-1] atScrollPosition:UITableViewScrollPositionNone animated:YES];
        
        //======================================//
          self.tableView.scrollEnabled = YES;
        //======================================//
    }
}

-(BOOL)isEditModeEnabled
{
    return _isEditMode;
}
#pragma mark -

#pragma mark SET CURRENT MODE
-(void)setCurrentMode:(NoteMode)mode
{
    if(_currentMode != mode)
    {
        if(_isEditMode) self.insertedTempIndexPath=nil;
        
        if(!_isEditMode && _currentMode == NoteModeDraw)
        {
            self.tableView.scrollEnabled = YES;
            [self createDrawingContainer];
        }
                
       _currentMode = mode;
        
        /*
        switch (_currentMode) 
        {
            case NoteModeText:
            [self enableTextMode];
            break;
                
            case NoteModeDraw:
            [self enableDrawMode];
            break;        
        }
        */
        
        //[self manageVisibilityOfInputView];
        [self scrollToNewContainer];
        //[self reloadTableView];
        [self.tableView reloadData];
    }
}
#pragma mark -

-(void)resetSelection
{
    //=====================================================================================
    [self.selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop)
     {
         NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:idx];
         
         UITableViewCell *containerCell = [self.tableView cellForRowAtIndexPath:indexPath];
         
         if(containerCell)
         {
              SSCheckBox  *_checkBox = (SSCheckBox *) [containerCell.contentView viewWithTag:2];
             _checkBox.checked = NO;
         }
     }];
    //=====================================================================================
    
    [self.selectedIndexes removeAllIndexes];
}
#pragma mark TAG
-(void)tagWithOEM:(NSString *)oemName
{
    if(!oemName) return;
    
    if(![self.selectedIndexes count])
    {
        [UIAlertView showWarningAlertWithTitle:@"" message:VISIT_TAG_SELECTION_MSG];
        return;
    }
    
    NSMutableIndexSet *indexSetToReload = [NSMutableIndexSet indexSet];
        
    [self.selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop)
    {
        NSDictionary *textNoteDict = (idx < [self.notes count]) ? [self.notes objectAtIndex:idx] : nil;
        
        NSString *text = [textNoteDict valueForKey:KEY_TEXT];
        
        if(text.length)
        {
          [textNoteDict setValue:([oemName isEqualToString:@"None"]) ? @"" : oemName forKey:KEY_TAG];
          // [textNoteDict setValue:([oemName isEqualToString:@"General"] || [oemName isEqualToString:@"None"]) ? @"" : oemName forKey:KEY_TAG];
        
          int oemID = [CoreDataHandler oemIDForOEMName:oemName];
          if(oemID==NSNotFound) oemID = 0;
        
          [textNoteDict setValue:[NSString stringWithFormat:@"%d",oemID] forKey:KEY_TAG_ID];
            
            [indexSetToReload addIndex:idx];
        }
    }];
    
      if([indexSetToReload count])
    [self.tableView reloadSections:indexSetToReload withRowAnimation:UITableViewRowAnimationFade];
    
    //[self resetSelection];
    
    //========================= RESET TEMP CONTAINER =============
    self.insertedTempIndexPath = nil;
}

#pragma mark -
#pragma mark DELETE CONTAINERS
-(void)deleteSelectedContainers
{
    if(![self.selectedIndexes count])
    {
        [UIAlertView showWarningAlertWithTitle:@"" message:VISIT_DELETE_CONTAINER_MSG];
        return;
    }
    @try
    {
        
    [self.notes removeObjectsAtIndexes:self.selectedIndexes];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Debug Exception=%@",exception.reason);
    }
    @finally
    {
        
    [self.tableView beginUpdates];
    [self.tableView deleteSections:self.selectedIndexes withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
  
    [self resetSelection];
    
    //======================= RESET TEMP CONTAINER =======================
    self.insertedTempIndexPath = nil;
    }
}

-(void)deleteContainerAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section >=0 && indexPath.section < [self.notes count])
    {
        [self.notes removeObjectAtIndex:indexPath.section];
        [self.tableView beginUpdates];
        [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}
#pragma mark -

-(void)enableTextMode
{
    
}

-(void)enableDrawMode
{
    /*
    if(![self.textNotes count])
    {
        NSDictionary *noteDict = [NSDictionary dictionary];
        [self.textNotes addObject:noteDict];
    }
    */
    
    //[self reloadTableView];
}

-(void)reloadTableView
{
    NSInteger numRows = [self.tableView numberOfSections];
    NSIndexSet *indexSets = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, MAX(0,numRows-1))];
    [self.tableView reloadSections:indexSets withRowAnimation:UITableViewRowAnimationFade];
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark -
#pragma mark MANAGE FOOTER VIEW
-(void)setupFooterView
{
    CGFloat _footerHeight = 610;
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), _footerHeight)];
    footerView.backgroundColor = [UIColor clearColor];
    footerView.autoresizesSubviews = YES;
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds), 1)];
    lineView.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.9] ;// self.tableView.separatorColor;
    [footerView addSubview:lineView];
    [lineView release];
}
    
    CGRect contentFrame = CGRectMake(65.0, 5.0, CGRectGetWidth(footerView.bounds)-70.0, _footerHeight-10.0);
    
   // entryTextView = [[HPGrowingTextView alloc] init];
    entryTextView = [[UITextView alloc] init];
  
    CGRect entryTextViewFrame = contentFrame;
    entryTextViewFrame.size.height -= 355.0;
    
    entryTextView.frame= entryTextViewFrame;
  
    //TO DO: TO BE SET PERFECTLY LATER
   // entryTextView.contentInset = UIEdgeInsetsMake(-5.0, -5.0, 0, -5.0);
    
   // entryTextView.minNumberOfLines = 1;
   // entryTextView.maxNumberOfLines = 500;
    entryTextView.returnKeyType = UIReturnKeyNext; //just as an example
    entryTextView.delegate = self;
    //entryTextView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 5, 0, 0);
    entryTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 5, 0, 0);
    entryTextView.backgroundColor = [UIColor clearColor];
    entryTextView.textColor       = [UIColor blackColor];
    entryTextView.font = [UIFont grotesqueFontOfSize:14.0];
    entryTextView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight;
    [footerView addSubview:entryTextView];
    
    
    _paintingView = [[DrawingCanvasView alloc] initWithShapes:nil];
    _paintingView.frame=contentFrame;
    _paintingView.delegate = self;
    _paintingView.backgroundColor=[UIColor clearColor];
    _paintingView.currentMode = PaintingModeDraw;
    //[footerView addSubview:_paintingView];
    [footerView insertSubview:_paintingView atIndex:0];
    _paintingView.userInteractionEnabled = NO;
    _paintingView.autoresizingMask =  UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    //tapGestureRecognizer.cancelsTouchesInView = NO;
    [footerView addGestureRecognizer:tapGestureRecognizer];
    [tapGestureRecognizer release];
    
    
    self.tableView.tableFooterView = footerView;
    [footerView release];
    
    [self manageVisibilityOfInputView];
    
   // [entryTextView.internalTextView resignFirstResponder];
    [entryTextView resignFirstResponder];
}

-(void)manageVisibilityOfInputView
{
    if(visitSessionViewController.isReportCompleted) return;
    
    BOOL isDrawingMode = (self.currentMode == NoteModeDraw);
    
    entryTextView.hidden = isDrawingMode;
    _paintingView.hidden = !isDrawingMode;
    
    _paintingView.currentMode = isDrawingMode ? PaintingModeDraw : PaintingModeEdit;
    
    
      // UITapGestureRecognizer *tapGestureRecognizer =[self.tableView.tableFooterView.gestureRecognizers objectAtIndex:0];
     //  tapGestureRecognizer.enabled = !isDrawingMode;
     //self.tableView.tableFooterView.backgroundColor = _isEditMode ? [[UIColor lightGrayColor] colorWithAlphaComponent:0.5] : [UIColor clearColor];
    
    if(!isDrawingMode && !_isEditMode)
    {
       // if(entryTextView.internalTextView.isFirstResponder)
           // [entryTextView.internalTextView resignFirstResponder];
        
        if(entryTextView.isFirstResponder)
            [entryTextView resignFirstResponder];
          //  [self performSelector:@selector(animateEntyTextViewToTop) withObject:nil afterDelay:0.6]; //0.2
        //else 
            [entryTextView becomeFirstResponder];
    }
    
   
    if(!_isEditMode && isDrawingMode)
    //if(!_isEditMode )
    {
        //if([entryTextView.internalTextView isFirstResponder])
           // [entryTextView.internalTextView resignFirstResponder];
        
        if(entryTextView.isFirstResponder)
            [entryTextView resignFirstResponder];
        
        self.tableView.scrollEnabled = !isDrawingMode;
        [self performSelector:@selector(animateEntyTextViewToTop) withObject:nil afterDelay:0.6];
    }
}

-(void)tapGestureAction:(UITapGestureRecognizer *)recognizer
{
     BOOL isDrawingMode = (self.currentMode == NoteModeDraw);
    
    if(_isEditMode) return;
        
    if(isDrawingMode)
    {
        recognizer.enabled = NO;
        //_paintingView.userInteractionEnabled = YES;
        [self scrollToNewContainer];
    }
    else 
    {
       // entryTextView.userInteractionEnabled=YES;
        [entryTextView becomeFirstResponder];
    }
}
#pragma mark -

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
    
#endif
   
    self.tableView.rowHeight = 44.0;
    self.tableView.bounces = NO;
    self.tableView.separatorColor = [UIColor darkGrayColor];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    
     self.clearsSelectionOnViewWillAppear = NO;
 
    [self setupFooterView];
    
    UIImageView *notesBgView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    notesBgView.image = [UIImage imageNamed:@"note_bg.png"];
    self.tableView.backgroundView = notesBgView;
    [notesBgView release];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
      [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidAppear:) name:UIKeyboardWillShowNotification object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidDismiss:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

-(void)setTextFromNoteDict:(NSDictionary *)dict toTextView:(EGOTextView *)_textView
{
    NSString *tagText   = [dict valueForKey:@"_tag"];
    NSString *textNote  = [dict valueForKey:KEY_TEXT];
    
    
    NSString *fullText =tagText.length ? [NSString stringWithFormat:@"%@:%@",tagText,textNote] : textNote;
    
    NSMutableAttributedString *str=[NSMutableAttributedString attributedStringWithString:fullText];
    NSRange _range=[fullText rangeOfString:@":"];
    
    NSRange _rannge2;
    if(_range.location!=NSNotFound)
    {
        _range   = NSMakeRange(0, _range.location);
        _rannge2 = NSMakeRange(_range.location+1, fullText.length-_range.location-1);
    }
    else 
    {
        _rannge2 = NSMakeRange(0, fullText.length);
    }
    
    if(_range.length)
    {
     [str setFont:[UIFont grotesqueFontOfSize:15] range:_range];//[UIFont systemFontOfSize:18]
     [str setTextColor: LOGIN_BG_COLOR range:_range];
    }
    [str setFont:[UIFont grotesqueFontOfSize:15] range:_rannge2];
    
    if(![_textView.attributedString isEqualToAttributedString:str])
    _textView.attributedString = str;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return [[self notes] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 1;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
     if(_isAreaExpanded && self.editingIndexPath.section==indexPath.section) return tableView.tableFooterView.frame.size.height;
    
     NSDictionary *textNoteDict = (indexPath.section < [self.notes count]) ? [self.notes objectAtIndex:indexPath.section] : nil;
    
    NSArray  *drawings  = [textNoteDict valueForKey:@"drawing"];
    NSString *textNote  = [textNoteDict valueForKey:KEY_TEXT];
    
    if(textNote.length)
    {
       NSNumber *heightNum=[textNoteDict valueForKey:@"height"];
    
        CGFloat _height=[heightNum floatValue];
      return heightNum ? _height : tableView.rowHeight;
    }
    else 
    {
        if([drawings count])
        {
             NSArray *drawings   = [textNoteDict valueForKey:@"drawing"];
             NSNumber *heightNum = [textNoteDict valueForKey:@"drawingHeight"];
            
            CGFloat _height = [heightNum floatValue];
            
            CGFloat _h= [drawings count] ? _height : 0.0;
            
            return _h;
        }
    }
    
    return tableView.rowHeight;
}


/*
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //if(_isAreaExpanded && self.editingIndexPath.section==indexPath.section) return tableView.tableFooterView.frame.size.height;
    
    NSDictionary *textNoteDict = (indexPath.section < [self.notes count]) ? [self.notes objectAtIndex:indexPath.section] : nil;
    
    NSArray  *drawings  = [textNoteDict valueForKey:@"drawing"];
    NSString *textNote  = [textNoteDict valueForKey:KEY_TEXT];
    
    if(textNote.length)
    {
        if(_isAreaExpanded && self.editingIndexPath.section==indexPath.section)
            return tableView.tableFooterView.frame.size.height-20.0;
        
        NSNumber *heightNum=[textNoteDict valueForKey:@"height"];
        return heightNum ? [heightNum doubleValue] : tableView.rowHeight;
    }
    else 
    {
        if([drawings count])
        {
           // NSArray *drawings   = [textNoteDict valueForKey:@"drawing"];
            
            
            if(_isAreaExpanded && self.editingIndexPath.section==indexPath.section)
                return tableView.tableFooterView.frame.size.height;
            
            NSNumber *heightNum = [textNoteDict valueForKey:@"drawingHeight"];
            return [drawings count] ? [heightNum doubleValue] : 0.0;
        }
    }
    
    return tableView.rowHeight;
}
*/

-(NSMutableAttributedString *)attributedTextAppendingTagText:(NSString *)tagText textNote:(NSString *)textNote
{
    NSMutableAttributedString *mainText =  [NSMutableAttributedString attributedStringWithString:@""];
    
    if(tagText)
    {
        //NSFontAttributeName
        NSString *tagAtbText = [NSString stringWithFormat:@"%@: ",tagText];
        NSDictionary *atbDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                 LOGIN_BG_COLOR, NSForegroundColorAttributeName,[UIFont grotesqueBoldFontOfSize:13.0],NSFontAttributeName,nil];
        
        NSMutableAttributedString *tagAs = [[NSMutableAttributedString alloc] initWithString:tagAtbText attributes:atbDict] ;
        
        if(tagAs)
        {
            [mainText appendAttributedString:tagAs];
        }
        [tagAs release];
    }
    
    if(textNote.length)
    {
        NSAttributedString *textNoteAtbText = [NSAttributedString attributedStringWithString:textNote];
        if(textNoteAtbText)
            [mainText appendAttributedString:textNoteAtbText];
    }

    return mainText;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier  = @"Cell_draw";
    static NSString *CellIdentifier2 = @"Cell_paint";
    

     NSDictionary *textNoteDict = (indexPath.section < [self.notes count]) ? [self.notes objectAtIndex:indexPath.section] : nil;
    
    NSArray *drawings   = [textNoteDict valueForKey:@"drawing"];
    NSString *textNote  = [textNoteDict valueForKey:KEY_TEXT];
    
    UITableViewCell *cell=nil;
    if(textNote.length)
    {
        SSCheckBox *_checkBox    = nil;
        UIButton *_playButton    = nil;
        UITextView *textNoteView = nil;
        // EGOTextView *textNoteView = nil;
        //UILabel *tagLabel = nil;
        UIControl *leftSideTapControl=nil;
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        cell.backgroundColor = [UIColor clearColor];
        
        CGFloat lineWidth = 45.0;
        CGFloat locX = lineWidth + 10.0;
        
        CGRect _controlFrame = CGRectMake(0, 0,lineWidth,CGRectGetHeight(cell.contentView.bounds));
        leftSideTapControl=[[UIControl alloc] initWithFrame:_controlFrame];
        leftSideTapControl.tag = 6;
        leftSideTapControl.backgroundColor = [UIColor clearColor];
        leftSideTapControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleHeight;
        [cell.contentView addSubview:leftSideTapControl];
        [leftSideTapControl addTarget:self action:@selector(leftSideDidTap:) forControlEvents:UIControlEventTouchUpInside];
        [leftSideTapControl release];
        
        UIImage *checkImage = [UIImage imageNamed:@"rounded_tick.png"];
        CGRect _checkboxFrame = CGRectMake((lineWidth-checkImage.size.width)/2.0, (CGRectGetHeight(cell.contentView.bounds)-checkImage.size.height)/2.0,checkImage.size.width,checkImage.size.height);
        
        _checkBox = [[SSCheckBox alloc] initWithFrame:_checkboxFrame];
        [_checkBox setBackgroundImageForNormalState:[UIImage imageNamed:@"rounded_blank.png"] selectedSate:checkImage];
        _checkBox.tag = 2;
        [cell.contentView addSubview:_checkBox];
        _checkBox.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [_checkBox addTarget:self action:@selector(checkAction:) forControlEvents:UIControlEventTouchUpInside];
        [_checkBox release];
        
        
        UIImage *playImage = [UIImage imageNamed:@"rounded_play.png"];
        _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _playButton.frame = CGRectMake((lineWidth-playImage.size.width)/2.0, (CGRectGetHeight(cell.contentView.bounds)-playImage.size.height)/2.0, playImage.size.width, playImage.size.height);
        [_playButton setBackgroundImage:playImage forState:UIControlStateNormal];
        _playButton.tag = 4;
        [cell.contentView addSubview:_playButton];
        _playButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [_playButton addTarget:self action:@selector(playAction:) forControlEvents:UIControlEventTouchUpInside];
        
        /*
        tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 8.0, 0.0, 13.0)];
        tagLabel.textColor  = LOGIN_BG_COLOR;
        tagLabel.font = [UIFont grotesqueBoldFontOfSize:13.0];
        tagLabel.backgroundColor = [UIColor clearColor];
        tagLabel.tag = 5;
        tagLabel.hidden = YES;
        //[cell.contentView addSubview:tagLabel];
         */
       
        textNoteView = [[UITextView alloc ] initWithFrame:CGRectMake(locX, 10.0, CGRectGetWidth(cell.contentView.bounds)-(locX+5.0), CGRectGetHeight(cell.contentView.bounds)-20.0)];
         //textNoteView = [[EGOTextView alloc ] initWithFrame:CGRectMake(locX, 10.0, CGRectGetWidth(cell.contentView.bounds)-(locX+5.0), CGRectGetHeight(cell.contentView.bounds)-20.0)];
        textNoteView.delegate = self;
       // textNoteView.font = tagLabel.font;
        textNoteView.tag = 1;
       // textNoteView.contentInset = UIEdgeInsetsMake(0.0, -5.0, 0, 5.0);
        textNoteView.backgroundColor  = [UIColor clearColor];
        textNoteView.font = [UIFont grotesqueFontOfSize:14.0];
        [cell.contentView addSubview:textNoteView];
        textNoteView.autoresizingMask =  UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [textNoteView release];
        
       // [textNoteView addSubview:tagLabel];
       // [tagLabel release];
        
    }
    else
    {
        textNoteView = (UITextView *) [cell.contentView viewWithTag:1];
        //textNoteView = (EGOTextView *) [cell.contentView viewWithTag:1];
        leftSideTapControl = (UIControl *)[cell.contentView viewWithTag:6];
        _checkBox    = (SSCheckBox *) [cell.contentView viewWithTag:2];
        _playButton  = (UIButton *)   [cell.contentView viewWithTag:4];
       // tagLabel = (UILabel *) [cell.contentView viewWithTag:5];
    }
        
    NSNumber *tagVal = [textNoteDict valueForKey:KEY_TIME];
        
     _checkBox.hidden   = !_isEditMode || _isAreaExpanded;
    _playButton.hidden = _isEditMode || !tagVal;
    leftSideTapControl.hidden = _checkBox.hidden;
    
    BOOL _isRecording  = [self.visitSessionViewController.audioManager isRecording];
    _playButton.enabled = !_isRecording;
        
    if(!_checkBox.hidden)
    {
        _checkBox.checked = [self.selectedIndexes containsIndex:indexPath.section];
    }
        
        NSString *textNote  = [textNoteDict valueForKey:KEY_TEXT];
        NSString *tagText   = [textNoteDict valueForKey:KEY_TAG];
        
         textNote = [textNote trimmedString];
        
        //============

        NSMutableAttributedString *mainText = [self attributedTextAppendingTagText:tagText textNote:textNote];
        
        /*
        int tagLen  = tagText.length;
        if(tagText.length)
        {
            tagText   = [tagText stringByAppendingString:@": "];
            tagLen  =  MAX(tagText.length,9);
        }
        //@"Mitsubishi:";
        CGSize _size = [tagText sizeWithFont:tagLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 13.0)];
        
        //===================== RESIZE TAG LABEL ACCORDING TO TEXT NOTE ================
        if(_size.width && _size.height)
        {
        CGRect tagLabelFrame = tagLabel.frame;
        tagLabelFrame.size = _size;
        tagLabel.frame = tagLabelFrame;
        }
        
 
        //============================ ADD SPACES ACCORDING TO TAG TEXT SIZE 
        
        NSMutableString *spaceString=[NSMutableString string];
        for(int i = 0 ; i<tagLen ; i++)
        {
            [spaceString appendString:@"  "];
        }
         
         tagLabel.text = tagText;
          textNote = [textNote trimmedString];
        */

       
       // textNoteView.text = [spaceString stringByAppendingString:textNote] ;
        textNoteView.attributedText = mainText;
        textNoteView.editable =  _isAreaExpanded;//_isEditMode;
        textNoteView.userInteractionEnabled =  _isAreaExpanded;// _isEditMode;
         
    //[self setTextFromNoteDict:textNoteDict toTextView:textNoteView];
        
    }
    else //if([drawings count])
    {
        NSMutableArray *drawings = [textNoteDict valueForKey:@"drawing"];
        
        SSCheckBox *_checkBox=nil;
        DrawingCanvasView *paintingView=nil;
         UIControl *leftSideTapControl=nil;
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        
        if (cell == nil) 
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2] autorelease];
            
            CGFloat lineWidth = 45.0;
            CGFloat locX = lineWidth + 10.0;
            
            CGRect _controlFrame = CGRectMake(0, 0,lineWidth,CGRectGetHeight(cell.contentView.bounds));
            leftSideTapControl=[[UIControl alloc] initWithFrame:_controlFrame];
            leftSideTapControl.tag = 6;
            leftSideTapControl.backgroundColor = [UIColor clearColor];
            leftSideTapControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleHeight;
            [cell.contentView addSubview:leftSideTapControl];
            [leftSideTapControl addTarget:self action:@selector(leftSideDidTap:) forControlEvents:UIControlEventTouchUpInside];
            [leftSideTapControl release];

            
            UIImage *checkImage = [UIImage imageNamed:@"rounded_tick.png"];
            CGRect _checkboxFrame = CGRectMake((lineWidth-checkImage.size.width)/2.0, (CGRectGetHeight(cell.contentView.bounds)-checkImage.size.height)/2.0,checkImage.size.width,checkImage.size.height);
            
            _checkBox = [[SSCheckBox alloc] initWithFrame:_checkboxFrame];
            [_checkBox setBackgroundImageForNormalState:[UIImage imageNamed:@"rounded_blank.png"] selectedSate:checkImage];
            _checkBox.tag = 2;
            [cell.contentView addSubview:_checkBox];
            _checkBox.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
            [_checkBox addTarget:self action:@selector(checkAction:) forControlEvents:UIControlEventTouchUpInside];
            [_checkBox release];

            
            paintingView = [[DrawingCanvasView alloc] initWithShapes:drawings];
            paintingView.frame=CGRectMake(50.0, 10.0, CGRectGetWidth(cell.contentView.bounds)-55.0 , CGRectGetHeight(cell.contentView.bounds)-20.0);
            paintingView.delegate = self;
            paintingView.backgroundColor=[UIColor clearColor];
            paintingView.tag = 1;
            //paintingView.currentMode = PaintingModeDraw;
            [cell.contentView addSubview:paintingView];
             paintingView.autoresizingMask =  UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
            [paintingView release];

        }
        else 
        {
            paintingView = (DrawingCanvasView *)[cell.contentView viewWithTag:1];
            _checkBox    = (SSCheckBox *) [cell.contentView viewWithTag:2];
            leftSideTapControl = (UIControl *)[cell.contentView viewWithTag:6];
        }
        
        _checkBox.hidden   = !_isEditMode || _isAreaExpanded;
        leftSideTapControl.hidden = _checkBox.hidden;
        
        if(!_checkBox.hidden)
        {
            _checkBox.checked = [self.selectedIndexes containsIndex:indexPath.section];
        }


        if(drawings) paintingView.shapes = drawings;
        
        if(_refreshPainting) 
        {
            [paintingView reload];
            _refreshPainting = NO;
        }
        
        //BOOL isDrawingMode = (self.currentMode == NoteModeDraw);
        //paintingView.currentMode = isDrawingMode && !_refreshPainting ? PaintingModeEdit : PaintingModeSelect;
        
        paintingView.currentMode = _isEditMode ? PaintingModeEdit : PaintingModeSelect;
        paintingView.userInteractionEnabled = !_isEditMode || _isAreaExpanded;
        [paintingView disblePanning:!_isEditMode];
    }
    
     if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
     cell.backgroundColor = [UIColor clearColor];
     
    
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // [self scrollToNewContainer];
    
    //if(_isEditMode && _currentMode==NoteModeDraw)
    if(_isEditMode )
    {
        self.editingIndexPath = indexPath;
        _isAreaExpanded = YES;
        
        NSDictionary *textNoteDict = (indexPath.section < [self.notes count]) ? [self.notes objectAtIndex:indexPath.section] : nil;
        NSArray *drawings = [textNoteDict valueForKey:@"drawing"];
         NSString *textNote  = [textNoteDict valueForKey:KEY_TEXT];

        if([drawings count])
        {
           if([activeTextView isFirstResponder]) [activeTextView resignFirstResponder];
            
            [visitSessionViewController showEditBottombar:NO];
        //================================= EXPAND DRAWINGS FOR EDIT ====================================
            _refreshPainting = YES;
            
            //[textNoteDict setValue:[NSNumber numberWithDouble:CGRectGetHeight(_paintingView.frame)] forKey:@"drawingHeight"];
            if(indexPath)
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];

             tableView.scrollEnabled = NO;
            
                        
             CGRect targetRect =[tableView rectForRowAtIndexPath:indexPath];
            
            NSTimeInterval _animationInterval =0.3;
            
            
            [UIView animateWithDuration:_animationInterval animations:^{
                
                [self.tableView setContentOffset:targetRect.origin animated:NO];
              //  _refreshPainting = NO;
                
            } completion:^(BOOL finished) {
                
                
            }];
           
        //===============================================================================================
            
        }
        else if(textNote.length)
        {
            [visitSessionViewController showEditBottombar:NO];
             _isAreaExpanded = YES;
            
            //[textNoteDict setValue:[NSNumber numberWithDouble:CGRectGetHeight(_paintingView.frame)] forKey:@"height"];
            
            if(indexPath)
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            tableView.scrollEnabled = NO;
            
            // [self.tableView scrollRectToVisible:targetRect animated:NO];
            
            CGRect targetRect =[tableView rectForRowAtIndexPath:indexPath];
            
            // TO SHOW A PORTION OF UPPER ROW
            if(indexPath.section>0)
            {
                //_goto
                targetRect.origin.y-=30.0;
            }
            
            UITableViewCell *currentCell = [tableView cellForRowAtIndexPath:indexPath];
            UITextView *textNoteView = (UITextView *) [currentCell.contentView viewWithTag:1];
            [textNoteView becomeFirstResponder];
            
            [UIView animateWithDuration:0.6 animations:^{
                
                [self.tableView setContentOffset:targetRect.origin animated:NO];
                
            } completion:^(BOOL finished) 
            {
                
                
            }];
        }
    }
    else 
    {
        [self scrollToNewContainer];

    }
}


-(void)scrollToNewContainer
{
    //[self.tableView scrollRectToVisible:self.tableView.tableFooterView.frame animated:YES];
    [self manageVisibilityOfInputView];
     _paintingView.userInteractionEnabled = YES;
}

#pragma mark CHECKBOX ACTION
-(void)checkAction:(SSCheckBox *)checkbox
{
    checkbox.checked = !checkbox.checked;
    //UITableViewCell *cell  = (UITableViewCell *)[checkbox.superview superview];
    UITableViewCell *cell  = (UITableViewCell *)[self cellForView:checkbox];
    NSIndexPath *indexPath = (NSIndexPath *) [self.tableView indexPathForCell:cell];
    
    if([self.selectedIndexes containsIndex:indexPath.section])
    {
        [self.selectedIndexes removeIndex:indexPath.section];
        
        //[self.visitSessionViewController enableTagging:YES];
    }
    else
    {
        [self.selectedIndexes addIndex:indexPath.section];
    }
    
    
     //====================== ENABLE / DISABLE TAGGING =============================
    
    __block BOOL _isTaggingEnabled = NO;
    [self.selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop)
     {
         NSDictionary *textNoteDict = (idx < [self.notes count]) ? [self.notes objectAtIndex:idx] : nil;
         NSString *textNote  = [textNoteDict valueForKey:KEY_TEXT];
         
         if(textNote.length)
         {
            _isTaggingEnabled = YES;
             *stop=YES;
         }
         else
         {
             _isTaggingEnabled = NO;
         }

    }];
    
    [self.visitSessionViewController enableTagging:_isTaggingEnabled];
   
    //====================== ENABLE / DISABLE TAGGING =============================
}
#pragma mark -
-(void)leftSideDidTap:(UIControl *)control
{
   // UITableViewCell *cell  = (UITableViewCell *)[control.superview superview];
    UITableViewCell *cell  = (UITableViewCell *)[self cellForView:control];
    SSCheckBox  *_checkBox = (SSCheckBox *) [cell.contentView viewWithTag:2];
    _checkBox.checked = !_checkBox.isChecked;
    [self checkAction:_checkBox];
}

#pragma mark PLAY ACTION
-(void)playAction:(UIButton *)sender
{
    //UITableViewCell *cell  = (UITableViewCell *)[sender.superview superview];
    UITableViewCell *cell  = (UITableViewCell *)[self cellForView:sender];
    NSIndexPath *indexPath = (NSIndexPath *) [self.tableView indexPathForCell:cell];
    
    NSDictionary *textNoteDict = (indexPath.section < [self.notes count]) ? [self.notes objectAtIndex:indexPath.section] : nil;
    
    NSNumber *tagVal = [textNoteDict valueForKey:KEY_TIME];
    
    if(tagVal)
    {
        double _tagInterval = [tagVal doubleValue];
        [visitSessionViewController tagAudio:_tagInterval forText:[textNoteDict valueForKey:KEY_TEXT]];
    }
}
#pragma mark -
#pragma mark HPGrowingTextViewDelegate


/*
- (BOOL)growingTextViewShouldBeginEditing:(HPGrowingTextView *)growingTextView
{
    //activeTextView = growingTextView;
    
    //CGRect targetRect =self.tableView.tableFooterView.frame;
    //[self.tableView scrollRectToVisible:targetRect animated:NO];
    return YES;
}
*/


-(void)growingTextViewDidBeginEditing:(HPGrowingTextView *)growingTextView
{
     _scrollEntryTextViewToTop=YES;
}


- (BOOL)growingTextView:(HPGrowingTextView *)_textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    //NSString *str = [_textView.text stringByReplacingCharactersInRange:range withString:text];
    
    if([text isEqualToString:@"\n"])
    {
        [_textView resignFirstResponder]; _textView.text=@"";
        _scrollEntryTextViewToTop=YES;
        [_textView becomeFirstResponder];
        
        //[self createTextContainerFromTextView:_textView];
        
        return NO;
    }
    
    /*
    if(!_textView.text.length && str.length==1)
    {
        NSLog(@"First word");
        
        
        //==================== STORE THE CURRENT TIME OFFSET HERE ======================= //
        
         if(_textAudioTag==-1.0 && [self.visitSessionViewController.audioManager isRecording])
         {
             _textAudioTag = [self.visitSessionViewController currentRecordingTime];
         }
        
         //=================== STORE THE CURRENT TIME OFFSET HERE ========================= //
        
    }
    */
    
    if(_textAudioTag==-1.0 && [self.visitSessionViewController.audioManager isRecording])
    {
        _textAudioTag = [self.visitSessionViewController currentRecordingTime];
    }
    
    return YES;
}


- (void)growingTextViewDidEndEditing:(HPGrowingTextView *)_textView
{
    //========================
   //  if(_isEditMode) return;
    //==========================
    
    [self createTextContainerFromTextView:_textView];
       
    if([activeTextView isKindOfClass:[_textView class]])
        activeTextView=nil;
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    CGFloat diff = (growingTextView.frame.size.height - height);
    CGRect r = growingTextView.frame;
    r.size.height += diff;
    
    if(r.size.height<=200)
    growingTextView.frame = r;
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView didChangeHeight:(float)height
{
    /*
     float _height=self.scrollView.frame.size.height-KEYBOARDHEIGHT_PORTRAIT;
     if(growingTextView.frame.size.height>=_height)
     {
     float x=self.scrollView.contentOffset.x;
     float y=growingTextView.frame.size.height-_height; //growingTextView.frame.origin.y+growingTextView.frame.size.height;
     [self.scrollView setContentOffset:CGPointMake(x, growingTextView.frame.origin.y+y) animated:YES];
     }
     else if(diff>0.0f)
     {
     float x=self.scrollView.contentOffset.x;
     float y=growingTextView.frame.origin.y;
     [self.scrollView setContentOffset:CGPointMake(x,y) animated:YES];
     }
     */
}
#pragma mark -
#pragma mark TEXTVIEW DELEGATE
-(void)textViewDidBeginEditing:(UITextView *)_textView
{
    if(_textView==entryTextView)
    {
         _scrollEntryTextViewToTop=YES;
        
        return;
    }
    
    activeTextView = _textView;
    
    //NSUInteger length = _textView.text.length;  
    //_textView.selectedRange = NSMakeRange(0, length);
    //_textView.selectedRange = NSMakeRange(length, 0);
    //[_textView setContentOffset:CGPointMake(0, length) animated:YES];

}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if(textView==entryTextView)
    {
        //NSString *str=[textView.text stringByReplacingCharactersInRange:range withString:text];
        
        
        if([text isEqualToString:@"\n"])
        {
            [textView resignFirstResponder]; textView.text = @"";
            _scrollEntryTextViewToTop=YES;
            [textView becomeFirstResponder];
            //[self createTextContainerFromTextView:_textView];
            
            return NO;
        }
        
        /*
         if(!_textView.text.length && str.length==1)
         {
         NSLog(@"First word");
         
         
         //==================== STORE THE CURRENT TIME OFFSET HERE ======================= //
         
         if(_textAudioTag==-1.0 && [self.visitSessionViewController.audioManager isRecording])
         {
         _textAudioTag = [self.visitSessionViewController currentRecordingTime];
         }
         
         //=================== STORE THE CURRENT TIME OFFSET HERE ========================= //
         
         }
         */
        
        if(_textAudioTag==-1.0 && [self.visitSessionViewController.audioManager isRecording])
        {
            _textAudioTag = [self.visitSessionViewController currentRecordingTime];
        }
        
        return YES;

    }
    
    /*
    //UITableViewCell *cell  = (UITableViewCell *) [[textView superview] superview];
    UITableViewCell *cell  = (UITableViewCell *) [self cellForView:textView];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSMutableDictionary *dict = (indexPath.section < [notes count]) ? [notes objectAtIndex:indexPath.section] : nil;
    NSString *tagString = [dict valueForKey:KEY_TAG];
    if(tagString.length && range.location<=tagString.length*2+3) return NO;
     */
    
    NSLog(@"RangeLoc:%d length:%d",range.location,range.length);
    
    return YES;
}

/*
- (void)textViewDidChange:(UITextView *)_textView
{
    if([_textView isKindOfClass:[HPGrowingTextView class]]) return;
    
    UITableViewCell *cell  = (UITableViewCell *) [[_textView superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSMutableDictionary *dict = (indexPath.section < [notes count]) ? [notes objectAtIndex:indexPath.section] : nil;
    
    if(!dict) return;
    
    CGFloat fontHeight = (_textView.font.ascender - _textView.font.descender) + 1;
    
    CGRect newTextFrame = _textView.frame;
    newTextFrame.size = _textView.contentSize;
    newTextFrame.size.height = newTextFrame.size.height + fontHeight;
    
    CGFloat rowHeight = MAX(newTextFrame.size.height+10, self.tableView.rowHeight);
    
    [dict setValue:[NSNumber numberWithDouble:rowHeight] forKey:@"height"];
    [dict setValue:_textView.text forKey:KEY_TEXT];
    //_textViewHeight=newTextFrame.size.height+1.0;
    [self.tableView beginUpdates];
    _textView.frame = newTextFrame;
    [self.tableView endUpdates];
}
 */

-(void)textViewDidEndEditing:(UITextView *)_textView
{
    if(_textView==entryTextView)
    {
         [self createTextContainerFromTextView:_textView];
        
        return;
    }
    
    if([activeTextView isKindOfClass:[_textView class]])
        activeTextView = nil;
    
  //  UITableViewCell *cell  = (UITableViewCell *) [[_textView superview] superview];
     UITableViewCell *cell  = (UITableViewCell *) [self cellForView:_textView];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSMutableDictionary *dict = (indexPath.section < [notes count]) ? [notes objectAtIndex:indexPath.section] : nil;
    
    if(!dict) return;
    
    CGFloat fontHeight = (_textView.font.ascender - _textView.font.descender) + 1;
    
    CGRect newTextFrame = _textView.frame;
    newTextFrame.size = _textView.contentSize;
    newTextFrame.size.height = newTextFrame.size.height + fontHeight;
    
    CGFloat rowHeight = MAX(newTextFrame.size.height+10, self.tableView.rowHeight);
    
    [dict setValue:[NSNumber numberWithFloat:rowHeight] forKey:@"height"];
    
    NSString *tagText = [dict valueForKey:KEY_TAG];
    NSString *_text   = _textView.text;
    
    if(tagText)
    {
        tagText = [NSString stringWithFormat:@"%@: ",tagText];
        NSRange tagRange = [_text rangeOfString:tagText];
        if(tagRange.location!=NSNotFound && tagRange.location==0)
        {
            _text =[_text stringByReplacingCharactersInRange:tagRange withString:@""];
        }
    }
       
    [dict setValue:_text forKey:KEY_TEXT];
    //_textViewHeight=newTextFrame.size.height+1.0;
}

#pragma mark -

-(void)egoTextViewDidChange:(EGOTextView *)_textView
{
   // UITableViewCell *cell  = (UITableViewCell *) [[_textView superview] superview];
    UITableViewCell *cell  = (UITableViewCell *) [self cellForView:_textView];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSMutableDictionary *dict = (indexPath.section < [notes count]) ? [notes objectAtIndex:indexPath.section] : nil;
    
    if(!dict) return;
    
    CGFloat fontHeight = (_textView.font.ascender - _textView.font.descender) + 1;
    
    CGRect newTextFrame = _textView.frame;
    newTextFrame.size   = _textView.contentSize;
    newTextFrame.size.height = newTextFrame.size.height + fontHeight;
    
    CGFloat rowHeight = MAX(newTextFrame.size.height+10, self.tableView.rowHeight);
    [dict setValue:[NSNumber numberWithFloat:rowHeight] forKey:@"height"];
    //_textViewHeight=newTextFrame.size.height+1.0;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    _textView.frame = newTextFrame;
    //[_textView setNeedsDisplay];
}

#pragma mark -
#pragma mark DrawingDelegate

-(void)canvas:(DrawingCanvasView *)canvas willDrawShape:(Shape *)ashape
{
    //GET AUDIO TIME OFFESET HERE
    
    if(_drawingAudioTag==-1.0 && [self.visitSessionViewController.audioManager isRecording])
    {
        _drawingAudioTag = [self.visitSessionViewController currentRecordingTime];
    }
}

-(void)canvas:(DrawingCanvasView *)canvas didFinishDrawingShape:(Shape *)ashape
{
    //SET AUDIO TIME OFFSET HERE
    
    if(_drawingAudioTag>=0)
    {
        ashape.audioTag = _drawingAudioTag;
        
        _drawingAudioTag=-1.0;
    }
    else
    {
        ashape.audioTag = -1.0;
    }
}


-(void)canvas:(DrawingCanvasView *)canvas didSelectShape:(Shape *)ashape
{
    //GET  AUDIO TAG FROM THE SHAPE
    //Play AUDIO FROM THE TAG
    
    if(canvas.currentMode==PaintingModeSelect)
    {
        if(ashape)
        {
            NSLog(@"Drawing tag=%lf",ashape.audioTag);
            
          if(ashape.audioTag>=0)
          [visitSessionViewController tagAudio:ashape.audioTag forText:nil];
        }
        else
        {
            //UITableViewCell *cell =(UITableViewCell *) [[canvas superview] superview];
            //NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            //self.editingIndexPath = indexPath;
            [self scrollToNewContainer];
        }
    }
}

-(UITableViewCell *)cellForView:(UIView *)view
{
    UIView *superView = [view superview];
    
    while( superView && ![superView isKindOfClass:[UITableViewCell class]])
    {
        superView = [superView superview];
        
        if([superView isKindOfClass:[UITableViewCell class]])
        {
            break;
        }
    }
    
    return (UITableViewCell *)superView;
}

-(void)canvasDidDeleteShape:(DrawingCanvasView *)canvas
{
    if(![canvas.shapes count])
    {
        UITableViewCell *cell  = [self cellForView:canvas];
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        
        if(indexPath)
        [self deleteContainerAtIndexPath:indexPath];
        
        //BUG HERE
        if(indexPath.section == self.editingIndexPath.section)
        {
           self.editingIndexPath = nil;
            _isAreaExpanded = NO;
            _paintingView.userInteractionEnabled = YES;
        }
    }
}

#pragma mark -

#pragma mark ScrollViewDelegate


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int maxRow = self.notes.count;
        maxRow = MAX(0,maxRow-1);
    
    //CGRect targetRect = maxRow>0 ? [self.tableView rectForRowAtIndexPath: [NSIndexPath indexPathForRow:0 inSection:maxRow]] : self.tableView.tableFooterView.frame;
    
    CGRect targetRect = self.tableView.tableFooterView.frame;
   // targetRect = [self.tableView convertRect:targetRect fromView:self.tableView];
    CGPoint _targetOffset = targetRect.origin;

    CGPoint _curContentOffset = self.tableView.contentOffset;
    
    if(_curContentOffset.y>_targetOffset.y)
    {
        [self.tableView setContentOffset:_targetOffset];
    }
}


/*
- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    CGRect targetRect = self.tableView.tableFooterView.frame;
    // targetRect = [self.tableView convertRect:targetRect fromView:self.tableView];
    CGPoint _targetOffset = targetRect.origin;
    
    CGPoint _curContentOffset = self.tableView.contentOffset;
    
    return _curContentOffset.y>_targetOffset.y;
    
}
 */

#pragma mark -

@end
