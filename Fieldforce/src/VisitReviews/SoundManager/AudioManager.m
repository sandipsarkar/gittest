//
//  SoundManager.m
//  SoundRecordTest
//
//  Created by RANDEM MAC on 02/03/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import "AudioManager.h"

#define RECORD_DURATION 60*60*1

@interface AudioManager()<AVAudioPlayerDelegate,AVAudioSessionDelegate>
@property(nonatomic,retain,readwrite) AVAudioRecorder * audioRecorder;
@property(nonatomic,retain,readwrite) AVAudioPlayer   * audioPlayer;
@property (nonatomic,copy) PlayBackBlock playbackBlock;
@end

@implementation AudioManager
@synthesize audioRecorder,audioPlayer;
@synthesize playbackBlock;

-(void)dealloc
{
    [audioRecorder release];
    [audioPlayer release];
    
    [playbackBlock release];
    
    [super dealloc];
}

-(id)init
{
    if(self=[super init])
    {
        recordEncoding = ENC_PCM;
    } 
    
    return self;
}

-(BOOL)isRecording
{
   return  self.audioRecorder.isRecording;
}

-(void)setPlaybackBlock:(PlayBackBlock)_playbackBlock
{
    if(playbackBlock!=_playbackBlock)
    {
       [playbackBlock release];
        playbackBlock=[_playbackBlock copy];
    }
}

- (void)startRecordAtPath:(NSString *)recordPath encodingFormat:(RecordEncodingTypes)encodingType error:(NSError *)error
{
    if(!recordPath) return;
    
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    audioSession.delegate = self;
    [audioSession setCategory:AVAudioSessionCategoryRecord error:nil];
    [audioSession setActive: YES error: nil];
    
    
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    recordEncoding=encodingType;
    
    if (encodingType == ENC_PCM) 
    {
        [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatLinearPCM]  forKey:AVFormatIDKey];
        [recordSetting setValue:[NSNumber numberWithFloat:44100.0]              forKey:AVSampleRateKey];
        [recordSetting setValue:[NSNumber numberWithInt:1]                      forKey:AVNumberOfChannelsKey];
        
        [recordSetting setValue:[NSNumber numberWithInt:16]                     forKey:AVLinearPCMBitDepthKey];
        [recordSetting setValue:[NSNumber numberWithBool:NO]                    forKey:AVLinearPCMIsBigEndianKey];
        [recordSetting setValue:[NSNumber numberWithBool:NO]                    forKey:AVLinearPCMIsFloatKey];
    } 
    else 
    {
        
        NSNumber *formatObject;
        
        switch (recordEncoding) 
        {
            case ENC_AAC:
                formatObject = [NSNumber numberWithInt:kAudioFormatMPEG4AAC];
                break;
                
            case ENC_ALAC:
                formatObject = [NSNumber numberWithInt:kAudioFormatAppleLossless];
                break;
                
            case ENC_IMA4:
                formatObject = [NSNumber numberWithInt:kAudioFormatAppleIMA4];
                break;
                
            case ENC_ILBC:
                formatObject = [NSNumber numberWithInt:kAudioFormatiLBC];
                break;
                
            case ENC_ULAW:
                formatObject = [NSNumber numberWithInt:kAudioFormatULaw];
                break;
                
            default:
                formatObject = [NSNumber numberWithInt:kAudioFormatAppleIMA4];
                break;
        }
        
       
        [recordSetting setValue:formatObject                                forKey:AVFormatIDKey];
        [recordSetting setValue:[NSNumber numberWithFloat:44100.0]          forKey:AVSampleRateKey]; //
        [recordSetting setValue:[NSNumber numberWithInt:2]                  forKey:AVNumberOfChannelsKey];
        //[recordSetting setValue:[NSNumber numberWithInt:64000]              forKey:AVEncoderBitRateKey];
        
        [recordSetting setValue:[NSNumber numberWithInt:16]                 forKey:AVLinearPCMBitDepthKey];
        [recordSetting setValue:[NSNumber numberWithInt:AVAudioQualityMedium] forKey:AVEncoderAudioQualityKey];
       // AVAudioQualityMedium
        
        //[recordSetting setValue :[NSNumber numberWithInt:8] forKey:AVEncoderBitDepthHintKey];/
       // [recordSetting setValue :[NSNumber numberWithInt:8] forKey:AVEncoderBitRatePerChannelKey];
        
    }
    
    NSURL *url = [NSURL fileURLWithPath:recordPath];
    
   AVAudioRecorder *_audioRecorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&error];
    [recordSetting release];
    [_audioRecorder recordForDuration:RECORD_DURATION]; //5 hours
    self.audioRecorder = _audioRecorder;
    [_audioRecorder release];
    
      if(!_audioRecorder) 
      {
        NSLog(@"audioRecorder: %@ %d %@", [error domain], [error code], [[error userInfo] description]);
        return;
      }
    
    //audioRecorder.meteringEnabled = YES;
    //    
    BOOL audioHWAvailable = audioSession.inputIsAvailable;
    if (! audioHWAvailable) 
     {
        UIAlertView *cantRecordAlert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: @"Audio input hardware not available"
                                  delegate: nil
                         cancelButtonTitle:@"Ok"
                         otherButtonTitles:nil];
        [cantRecordAlert show];
        [cantRecordAlert release]; 
        return;
    }
    
    if ([self.audioRecorder prepareToRecord]) 
    {
        [self.audioRecorder record];
        NSLog(@"recording");
    }
    else 
    {
        //        int errorCode = CFSwapInt32HostToBig ([error code]); 
        //        NSLog(@"Error: %@ [%4.4s])" , [error localizedDescription], (char*)&errorCode);
        NSLog(@"recorder: %@ %d %@", [error domain], [error code], [[error userInfo] description]);
    }
}

- (void)stopRecord
{
    if(self.audioRecorder) [self.audioRecorder  stop];
    
    if (self.audioPlayer) [self.audioPlayer stop];

    
    [[AVAudioSession sharedInstance] setActive: NO error: nil];
}

- (void)playAudioAtPath:(NSString *)filePath error:(NSError *)error completionHandler:(PlayBackBlock)block
{
    if(!filePath) return;
    
    if(self.audioPlayer && self.audioPlayer.isPlaying)
      [self.audioPlayer stop];
    
    UInt32 doChangeDefaultRoute = kAudioSessionOverrideAudioRoute_Speaker;
    //kAudioSessionProperty_OverrideAudioRoute
    AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryDefaultToSpeaker, sizeof(doChangeDefaultRoute), &doChangeDefaultRoute);
    
    if(block)
        self.playbackBlock=block;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
     [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    [audioSession setActive: YES error: nil];
   
    NSURL *url = [NSURL fileURLWithPath:filePath];
    AVAudioPlayer *_audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    _audioPlayer.delegate=self;
    self.audioPlayer=_audioPlayer;
    [_audioPlayer release];

    
    [self.audioPlayer play];
    
    NSLog(@"Playing file >");
}

-(void)stopPlayingAudio
{
    if (self.audioPlayer) 
    {
        if (audioPlayer.isPlaying)
        {
            [audioPlayer stop];
           // self.playbackBlock = nil;
        }
        
        [[AVAudioSession sharedInstance] setActive: NO error: nil];
    }
}

- (void)pausePlayingAudio
{
    if (self.audioPlayer) 
    {
        if (audioPlayer.isPlaying) [audioPlayer pause];
    }
}

-(void)appendSoundAtPath:(NSString *)firstPath toPath:(NSString *)secondPath temporaryCombinePath:(NSString *)aTempCombinePath completionHandler:(void (^)(BOOL success) )callback
{
    __block  BOOL success=NO;

    NSString *tempCombinePath = aTempCombinePath;
    
    NSFileManager *fileManager= [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath:secondPath ] == YES)
    {
        NSLog (@"File exists");
        
        
        AVMutableComposition* composition = [AVMutableComposition composition];
        AVURLAsset* audioAsset1 = [[AVURLAsset alloc]initWithURL:[NSURL fileURLWithPath:secondPath] options:nil];
        AVURLAsset* audioAsset2 = [[AVURLAsset alloc]initWithURL:[NSURL fileURLWithPath:firstPath] options:nil];
        
        AVMutableCompositionTrack *audioTrack1 = [composition addMutableTrackWithMediaType:AVMediaTypeAudio 
                                                                          preferredTrackID:kCMPersistentTrackID_Invalid];
        
        NSArray *arrtracks=[audioAsset1 tracksWithMediaType:AVMediaTypeAudio];
        
        NSError *error=nil;
        
        if([arrtracks count])
        {
          [audioTrack1 insertTimeRange:CMTimeRangeMake(kCMTimeZero, audioAsset1.duration)
                             ofTrack:[arrtracks objectAtIndex:0]
                              atTime:kCMTimeZero
                               error:&error];
        }
        
        NSArray *arrtracks2=[audioAsset2 tracksWithMediaType:AVMediaTypeAudio];
        
        if([arrtracks2 count])
        {
         [audioTrack1 insertTimeRange:CMTimeRangeMake(kCMTimeZero, audioAsset2.duration)
                             ofTrack:[arrtracks2 objectAtIndex:0]
                              atTime:audioAsset1.duration
                               error:&error];
        }
        
      
        AVAssetExportSession *exportSession = [AVAssetExportSession
                                               exportSessionWithAsset:composition
                                               presetName:AVAssetExportPresetAppleM4A]; //AVAssetExportPresetPassthrough
        //AVAssetExportPresetAppleM4A
        //AVAssetExportPresetMediumQuality
       // AVAssetExportPresetPassthrough

        if (nil == exportSession) 
            NSLog(@"nil");
        
        // configure export session  output with all our parameters
        exportSession.outputURL = [NSURL fileURLWithPath:tempCombinePath];
        exportSession.outputFileType = AVFileTypeAppleM4A;
        exportSession.shouldOptimizeForNetworkUse=YES;
        
        // perform the export
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            
            if (AVAssetExportSessionStatusCompleted == exportSession.status)
            {
                NSLog(@"AVAssetExportSessionStatusCompleted");
                
                if([fileManager fileExistsAtPath:secondPath])
                {
                    [fileManager removeItemAtPath:secondPath error:nil];
                }
                
                if ([fileManager copyItemAtPath:tempCombinePath toPath:secondPath error: NULL]  == YES)
                {
                    
                    [fileManager removeItemAtPath:tempCombinePath error:nil];
                }
                
                success=YES;
            } 
            else if (AVAssetExportSessionStatusFailed == exportSession.status)
            {
                NSLog(@"AVAssetExportSessionStatusFailed");
                
                success=NO;
            } 
            else 
            {
                NSLog(@"Export Session Status: %d", exportSession.status);
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(callback) callback(success);
            });
        }];
        
        [audioAsset1 release];
        [audioAsset2 release];
    }
    else
    {
        NSLog (@"File not found");
        
        if ([fileManager copyItemAtPath:firstPath toPath:secondPath error: NULL]  == YES)
        {
            NSLog (@"Copy successful");
            [fileManager removeItemAtPath:firstPath error:nil];
            
            success=YES;
        }
        else
        {
            NSLog (@"Copy failed"); 
            
            success=NO;
        }
        
        if(callback) callback(success);
    }
}


- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    if( self.playbackBlock)
    {
         self.playbackBlock(player,nil,flag);
        
    }
}

/* if an error occurs while decoding it will be reported to the delegate. */
- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    if(self.playbackBlock)
    {
      self.playbackBlock(player,error,NO);
    }
}

 - (void)inputIsAvailableChanged:(BOOL)isInputAvailable
{
    
}


@end
