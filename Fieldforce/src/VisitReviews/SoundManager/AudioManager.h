//
//  SoundManager.h
//  SoundRecordTest
//
//  Created by RANDEM MAC on 02/03/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioServices.h>


typedef void(^PlayBackBlock)(AVAudioPlayer *audioPlayer,NSError *error,BOOL success);

typedef enum
{
    ENC_AAC = 1,
    ENC_ALAC ,
    ENC_IMA4 ,
    ENC_ILBC ,
    ENC_ULAW ,
    ENC_PCM ,
} RecordEncodingTypes;

@interface AudioManager : NSObject
{
  AVAudioPlayer   * audioPlayer;
  AVAudioRecorder * audioRecorder;
  RecordEncodingTypes recordEncoding;

}

@property(nonatomic,retain,readonly) AVAudioPlayer   * audioPlayer;
@property(nonatomic,retain,readonly) AVAudioRecorder * audioRecorder;

-(id)init;
-(BOOL)isRecording;
- (void)playAudioAtPath:(NSString *)filePath error:(NSError *)error completionHandler:(PlayBackBlock)block;
- (void)stopPlayingAudio;
- (void)pausePlayingAudio;

- (void)startRecordAtPath:(NSString *)recordPath encodingFormat:(RecordEncodingTypes)encodingType error:(NSError *)error;
- (void)stopRecord;

-(void)appendSoundAtPath:(NSString *)firstPath toPath:(NSString *)secondPath temporaryCombinePath:(NSString *)aTempCombinePath completionHandler:(void (^)(BOOL success) )callback;

@end
