//
//  VisitTypesViewcontroller.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 18/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef  enum {
    
NatureOfVisitGeneral = 1,
NatureOfVisitColdCall = 2,
NatureOfVisitOrder=3,
NatureOfVisitCredit=4,
NatureOfVisitPaymentAccount=5,
NatureOfVisitOther=6,
NatureOfVisitDelivery=7,
    
}NatureOfVisit;


/* // 64-bit 
typedef enum NatureOfVisit : NSUInteger {
    
    NatureOfVisitGeneral = 1,
    NatureOfVisitColdCall = 2,
    NatureOfVisitOrder=3,
    NatureOfVisitCredit=4,
    NatureOfVisitPaymentAccount=5,
    NatureOfVisitOther=6,
    NatureOfVisitDelivery=7,
    
}NatureOfVisit;
*/

#define NATURE_OF_VISIT_TEXT @"General,Cold Call,Order,Credit,Payment/Account,Delivery,Other"

typedef void(^ActionCallBack)(NSInteger visitNatureID);

@protocol VisitTypesDelegate;
@interface VisitTypesViewcontroller : UITableViewController
{
    @private
    NSArray *_visitTypes;
}

@property(nonatomic,assign)BOOL isReadonlyMode;
@property(nonatomic,assign) id <VisitTypesDelegate> delegate;

+(NSArray *)visitTypes;
+(NSArray *)internalVisitTypes;

+(NSString *)nameForNatureOfVisitID:(NSInteger)natureOfVisitID;

-(void)addNatureOfVisitDidSelectCallback:(ActionCallBack )callback;

//- (id)initWithNatureOfVisit:(NSString *)natureOfVisit;
- (id)initWithNatureOfVisitID:(NSInteger )natureOfVisitID;

+(NSInteger)indexForSelectedNatureOfVisit:(NSString *)natureOfVisit;

@end

@protocol VisitTypesDelegate <NSObject>

@optional
-(void)visitTypesViewcontroller:(VisitTypesViewcontroller*)visitTypesVC didSelectNatureOfVisit:(NSInteger) visitNatureID;

@end
