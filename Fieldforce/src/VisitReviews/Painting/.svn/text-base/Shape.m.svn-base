//
//  Shape.m
//  PathHitTesting
//
//  Created by Ole Begemann on 30.01.12.
//  Copyright (c) 2012 Ole Begemann. All rights reserved.
//

#import "Shape.h"
#import "ANPathIntersection.h"

@interface Shape ()
{
    BOOL isTransformed;
}



- (UIBezierPath *)tapTargetForPath:(UIBezierPath *)path;

+ (ShapeType)randomShapeType;
+ (CGRect)randomRectInBounds:(CGRect)maxBounds;
+ (UIColor *)randomColor;
+ (CGFloat)randomLineWidth;
+ (UIBezierPath *)houseInRect:(CGRect)bounds;
+ (UIBezierPath *)arcInRect:(CGRect)bounds;
- (UIBezierPath *)interactionPathForPath:(UIBezierPath *)path;
-(BOOL)intersectsAtPoint:(CGPoint)aPoint;

@property(nonatomic,retain)UIBezierPath *interactionPath;
@end


@implementation Shape

@synthesize path = _path;
@synthesize lineColor = _lineColor;
@synthesize tapTarget = _tapTarget;
@dynamic totalBounds;

@synthesize pathBitmap;

///////////////////////////////
@synthesize childrenShapes;
@synthesize parentShape;
@synthesize level;
@synthesize interactionPath;
///////////////////////////////

+ (id)randomShapeInBounds:(CGRect)maxBounds
{
    UIBezierPath *path = nil;
    CGRect bounds = [self randomRectInBounds:maxBounds];
    ShapeType type = [self randomShapeType];
    
    switch (type) 
    {
        case ShapeTypeRect:
            path = [UIBezierPath bezierPathWithRect:bounds];
            break;
        case ShapeTypeEllipse:
            path = [UIBezierPath bezierPathWithOvalInRect:bounds];
            break;
        case ShapeTypeHouse:
            path = [self houseInRect:bounds];
            break;
        case ShapeTypeArc:
            path = [self arcInRect:bounds];
            break;
        default:
            path = [UIBezierPath bezierPathWithRect:bounds];
            break;
    }

    path.lineWidth = [self randomLineWidth];
    UIColor *lineColor = [self randomColor];
    
    return [[[self alloc] initWithPath:path lineColor:lineColor] autorelease];
}

+ (id)shapeWithPath:(UIBezierPath *)path lineColor:(UIColor *)lineColor
{
    return [[[self alloc] initWithPath:path lineColor:lineColor] autorelease];
}

+(id)emptyShapeWithLineWidth:(float)lineWidth lineColor:(UIColor *)lineColor
{
     UIBezierPath *newPath = [UIBezierPath bezierPath];
     newPath.lineWidth = lineWidth;
    
    return [[[self alloc] initWithPath:newPath lineColor:lineColor] autorelease];
}

- (id)initWithPath:(UIBezierPath *)path lineColor:(UIColor *)lineColor
{
    self = [super init];
    if (self != nil) 
    {
        _path = [path retain];
        interactionPath=[_path copy];
        _lineColor = [lineColor retain];
        _tapTarget = [[self tapTargetForPath:_path] retain];
      
        //Creates Weak MutableArray that does not retain its items
        CFArrayCallBacks scb = { 0, NULL, NULL, CFCopyDescription, CFEqual };
        childrenShapes = (NSMutableArray *)CFArrayCreateMutable(NULL, 0, &scb);

        [childrenShapes addObject:self];

    }
    return self;
}

-(void)dealloc
{
    [_path release];
    [_lineColor release];
    [_tapTarget release];
    [interactionPath release];
    
    [childrenShapes release],childrenShapes=nil;
    
    if(pathBitmap)
      [pathBitmap release];
    
    parentShape=nil;
    
    [super dealloc];
}

-(void)addPoint:(CGPoint)aPoint
{
    if([self.path isEmpty])
    {
        [self.path moveToPoint:aPoint];
    }
    else
    {
        [self.path addLineToPoint:aPoint];
    }
}

-(void)finishDrawingPath
{
   self.tapTarget = [self tapTargetForPath:_path];
    
  //  [connectedShapes addObject:self];
    
   
}

- (id)init
{
    UIBezierPath *defaultPath = [UIBezierPath bezierPathWithRect:CGRectMake(0.0f, 0.0f, 100.0f, 100.0f)];
    UIColor *defaultLineColor = [UIColor blackColor];
    return [self initWithPath:defaultPath lineColor:defaultLineColor];
}


#pragma mark - Description

- (NSString *)description
{
    return [NSString stringWithFormat:@"<Shape: %p - Bounds: %@ - Color: %@>", self, NSStringFromCGRect(self.path.bounds), self.lineColor];
}


#pragma mark - Hit Testing

- (UIBezierPath *)tapTargetForPath:(UIBezierPath *)path
{
    if (path == nil) 
    {
        return nil;
    }
    
    CGPathRef tapTargetPath = CGPathCreateCopyByStrokingPath(path.CGPath, NULL, fmaxf(25.0f, path.lineWidth), path.lineCapStyle, path.lineJoinStyle, path.miterLimit);
    
    if (tapTargetPath == NULL) 
    {
        return nil;
    }
    
    UIBezierPath *tapTarget = [UIBezierPath bezierPathWithCGPath:tapTargetPath];
    CGPathRelease(tapTargetPath);
    return tapTarget;
}



- (BOOL)containsPoint:(CGPoint)point
{
    return [self.tapTarget containsPoint:point];
}

-(BOOL)intersectsAtPoint:(CGPoint)aPoint
{
    return [self.tapTarget containsPoint:aPoint];
}


#pragma mark - Bounds

- (CGRect)totalBounds
{
    if (self.path == nil) 
    {
        return CGRectZero;
    }
    
    return CGRectInset(self.path.bounds, -(self.path.lineWidth + 1.0f), -(self.path.lineWidth + 1.0f));
}


#pragma mark - Modifying Shapes

- (void)moveBy:(CGPoint)delta
{
    CGAffineTransform transform = CGAffineTransformMakeTranslation(delta.x, delta.y);
    [self.path applyTransform:transform];
    [self.tapTarget applyTransform:transform];

    CGMutablePathRef _aPath=  CGPathCreateMutableCopyByTransformingPath(self.interactionPath.CGPath, &transform);

    self.interactionPath = [UIBezierPath bezierPathWithCGPath:_aPath];
    CGPathRelease(_aPath);
    
    
}

-(CGRect)translateBy:(CGPoint)delta
{
    CGRect originalBounds = self.totalBounds;
    CGRect newBounds = CGRectApplyAffineTransform(originalBounds, CGAffineTransformMakeTranslation(delta.x, delta.y));
    CGRect rectToRedraw = CGRectUnion(originalBounds, newBounds);
    
    [self moveBy:delta];
    
    return rectToRedraw;
}



#pragma mark - Random Shape Generator Methods

+ (ShapeType)randomShapeType
{
    return arc4random_uniform(SHAPE_TYPE_COUNT);
}

+ (CGRect)randomRectInBounds:(CGRect)maxBounds
{
    CGRect normalizedBounds = CGRectStandardize(maxBounds);
    uint32_t minOriginX = normalizedBounds.origin.x;
    uint32_t minOriginY = normalizedBounds.origin.y;
    uint32_t minWidth = 44;
    uint32_t minHeight = 44;

    uint32_t maxOriginX = normalizedBounds.size.width - minWidth;
    uint32_t maxOriginY = normalizedBounds.size.height - minHeight;

    uint32_t originX = arc4random_uniform(maxOriginX - minOriginX) + minOriginX;
    uint32_t originY = arc4random_uniform(maxOriginY - minOriginY) + minOriginY;
    
    uint32_t maxWidth = normalizedBounds.size.width - originX;
    uint32_t maxHeight = normalizedBounds.size.height - originY;

    uint32_t width = arc4random_uniform(maxWidth - minWidth) + minWidth;
    uint32_t height = arc4random_uniform(maxHeight - minHeight) + minHeight;
    
    CGRect randomRect = CGRectMake(originX, originY, width, height);
    return randomRect;
}

+ (UIColor *)randomColor
{
    NSArray *colors = [NSArray arrayWithObjects:
                       [UIColor blueColor], 
                       [UIColor redColor], 
                       [UIColor greenColor], 
                       [UIColor yellowColor], 
                       [UIColor magentaColor], 
                       [UIColor brownColor], 
                       [UIColor purpleColor], 
                       [UIColor orangeColor], 
                       nil];
    uint32_t colorIndex = arc4random_uniform([colors count]);
    return [colors objectAtIndex:colorIndex];
}

+ (CGFloat)randomLineWidth
{
    uint32_t maxLineWidth = 15;
    CGFloat lineWidth = arc4random_uniform(maxLineWidth) + 1.0f; // avoid lineWidth == 0.0f
    return lineWidth;
}

+ (UIBezierPath *)houseInRect:(CGRect)bounds
{
    CGPoint bottomLeft 	= CGPointMake(CGRectGetMinX(bounds), CGRectGetMinY(bounds));
    CGPoint topLeft		= CGPointMake(CGRectGetMinX(bounds), CGRectGetMinY(bounds) + CGRectGetHeight(bounds) * 2.0f/3.0f);
    CGPoint bottomRight = CGPointMake(CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
    CGPoint topRight	= CGPointMake(CGRectGetMaxX(bounds), CGRectGetMinY(bounds) + CGRectGetHeight(bounds) * 2.0f/3.0f);
    CGPoint roofTip		= CGPointMake(CGRectGetMidX(bounds), CGRectGetMaxY(bounds));
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:bottomLeft];
    [path addLineToPoint:topLeft];
    [path addLineToPoint:roofTip];
    [path addLineToPoint:topRight];
    [path addLineToPoint:topLeft];
    [path addLineToPoint:bottomRight];
    [path addLineToPoint:topRight];
    [path addLineToPoint:bottomLeft];
    [path addLineToPoint:bottomRight];
    
    path.lineJoinStyle = kCGLineJoinRound;

    CGAffineTransform transform = CGAffineTransformIdentity;
    transform = CGAffineTransformTranslate(transform, bounds.origin.x, bounds.origin.y);
    transform = CGAffineTransformTranslate(transform, 0.0, bounds.size.height);
    transform = CGAffineTransformScale(transform, 1.0f, -1.0f);
    transform = CGAffineTransformTranslate(transform, -bounds.origin.x, -bounds.origin.y);
    [path applyTransform:transform];
    
    return path;
}

+ (UIBezierPath *)arcInRect:(CGRect)bounds
{
    CGPoint center      = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    CGPoint centerRight = CGPointMake(CGRectGetMaxX(bounds), CGRectGetMidY(bounds));
    CGFloat radius      = CGRectGetWidth(bounds) / 2.0f;
    CGPoint center2     = CGPointMake(center.x, center.y - radius / 2.0f);
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:centerRight];
    [path addArcWithCenter:center radius:radius startAngle:0.0f endAngle:M_PI * 1.5f clockwise:YES];
    [path addArcWithCenter:center2 radius:radius / 2.0f startAngle:M_PI * 1.5f endAngle:M_PI clockwise:YES];
    
    path.lineJoinStyle = kCGLineJoinRound;
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    transform = CGAffineTransformTranslate(transform, bounds.origin.x, bounds.origin.y);
    transform = CGAffineTransformTranslate(transform, 0.0, bounds.size.height);
    transform = CGAffineTransformScale(transform, 1.0f, -1.0f);
    transform = CGAffineTransformTranslate(transform, -bounds.origin.x, -bounds.origin.y);
    [path applyTransform:transform];
    
    return path;
}

-(ANPathBitmap *)pathBitmap
{
   // if(!pathBitmap)
    {
    if(pathBitmap) [pathBitmap release];
    
    ANPathBitmap * bm1 = [[ANPathBitmap alloc] initWithPath:self.interactionPath.CGPath];
    bm1.lineCap = kCGLineCapRound;
    bm1.lineThickness = 4;
    [bm1 generateBitmap];
    
    pathBitmap=bm1;
    }
    
    return pathBitmap;
}


#pragma mark SANDIP to check interactio
/*
-(BOOL)intersectsWithShape:(Shape *)aShape
{
    BOOL isIntersect=NO;
    
    UIBezierPath *path1 = [self.path copy];
    UIBezierPath *path2 = [aShape.path copy];
    
    UIBezierPath *interactionPath1 = [self.interactionPath copy];
    UIBezierPath *interactionPath2 = [aShape.interactionPath copy];
    
    CGRect bounds1 = path1.bounds;
    CGRect bounds2 = path2.bounds;
    
    if(!CGRectIntersectsRect(bounds1, bounds2)) return NO;
    
    CGRect originalBounds = CGRectIntersection(bounds1, bounds2);
    CGRect newBounds;
    
    CGAffineTransform transform = CGAffineTransformMakeScale(0.3, 0.3);
     newBounds = CGRectApplyAffineTransform(originalBounds, transform);
    [path1 applyTransform:transform];
    [path2 applyTransform:transform];
    [interactionPath1 applyTransform:transform];
    [interactionPath2 applyTransform:transform];
    
    NSLog(@"original width=%f height=%f", originalBounds.size.width, originalBounds.size.height);
    //NSLog(@"size:%f", newBounds.size.width);
    
 
    for(int x=CGRectGetMinX(newBounds); x<CGRectGetMaxX(newBounds); x+=2)
    {
        for(int y=CGRectGetMinY(newBounds); y<CGRectGetMaxY(newBounds); y+=2)
        {
            CGPoint aPoint= CGPointMake(x, y);
            
            if([interactionPath1 containsPoint:aPoint])
            {
                if([interactionPath2 containsPoint:aPoint])
                {
                    isIntersect =YES;
                    
                    break;
                }
            }
        }
        
        if(isIntersect) break;
    }

  
    return isIntersect;
}
 */

-(BOOL)intersectsWithShape:(Shape *)aShape
{
    BOOL intersects=NO;
    
    ANPathBitmap * bm1 = self.pathBitmap;
    ANPathBitmap * bm2 = aShape.pathBitmap;
   
    
    CGPoint intPoint;
    ANPathIntersection * intersection = [[ANPathIntersection alloc] initWithPathBitmap:bm1
                                                                           anotherPath:bm2];
    if ([intersection pathLinesIntersect:&intPoint]) 
    {
        NSLog(@"Point: %@", NSStringFromCGPoint(intPoint));
        intersects=YES;
        
    } 
    else 
    {
        NSLog(@"No intersection");
        
        intersects=NO;
    }
    
    [intersection release];
    
    return intersects;
    
}

-(void)setParentShape:(Shape *)aParentShape
{
    if(parentShape!=aParentShape)
    {
      //  [parentShape.connectedShapes removeObject:self];
        parentShape=aParentShape;
        [parentShape.childrenShapes addObject:self];
        
    }
}

@end
