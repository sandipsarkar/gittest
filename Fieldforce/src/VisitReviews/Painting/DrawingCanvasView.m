//
//  DrawingCanvasView.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "DrawingCanvasView.h"



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


//
//  SSViewController.m
//  DrawingTest
//
//  Created by RANDEM MAC on 02/04/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import "DrawingCanvasView.h"
#import "DrawingView.h"
#import "Shape.h"
#import "VisitSessionViewController.h"

@interface DrawingCanvasView() <DrawingViewDataSource,UIGestureRecognizerDelegate>
{
    NSUInteger _selectedShapeIndex;
    BOOL isDoubleTappedMode;
    
    UIMenuController *menuController;
    NSMutableArray *parentShapes;
    
    BOOL isProcessRunning;
    
    UIScrollView *scrollView;
    
    BOOL enableUndoStack;
    UIPanGestureRecognizer *panRecognizer;
    
    // NSMutableArray *_recognizers;
    
    BOOL _drawNewShape;
    
}

@property (nonatomic ,readwrite) NSUInteger selectedShapeIndex;
@property (nonatomic ,retain,readwrite) Shape *selectedShape;
@property(nonatomic,retain) NSMutableIndexSet *selectedIndexes;
@property(nonatomic,retain) NSMutableSet *selectedShapes;

//-(void)deleteShape:(Shape *)currentShape;
-(void)refreshSelectedShape;

@end


@implementation DrawingCanvasView
@synthesize shapes;
@synthesize drawingView;
@synthesize selectedShapeIndex=_selectedShapeIndex;
@synthesize selectedShape;
@synthesize selectedIndexes;
@synthesize currentMode;
@synthesize selectedShapes;
@synthesize visitSessionViewController;
@synthesize delegate;


-(id)init
{
    
    if(self=[super init])
    {
       // CFArrayCallBacks scb = { 0, NULL, NULL, CFCopyDescription, CFEqual };
       // parentShapes = (NSMutableArray *)CFArrayCreateMutable(NULL, 0, &scb);
        //parentShapes=[[NSMutableArray alloc] init];
        
        selectedIndexes=[[NSMutableIndexSet alloc] init];
        currentMode=PaintingModeDraw;
        _selectedShapeIndex = NSNotFound;
        shapes = [[NSMutableArray alloc] init];
        
        //_recognizers=[[NSMutableArray alloc] init];
        
        [self viewDidLoad];
    }
    
    return self;
}
-(id)initWithShapes:(NSMutableArray *)_shapes
{
    
    if(self=[super init])
    {
       // CFArrayCallBacks scb = { 0, NULL, NULL, CFCopyDescription, CFEqual };
       // parentShapes = (NSMutableArray *)CFArrayCreateMutable(NULL, 0, &scb);
        //parentShapes=[[NSMutableArray alloc] init];
        
        selectedIndexes=[[NSMutableIndexSet alloc] init];
        currentMode=PaintingModeDraw;
        _selectedShapeIndex = NSNotFound;
        if(!_shapes)
        shapes = [[NSMutableArray alloc] init];
        else
        self.shapes=_shapes;
        
        [self viewDidLoad];
    }
    
    return self;
}

/*
-(void)setCurrentMode:(PaintingMode)aCurrentMode
{
    if(currentMode!=aCurrentMode)
    {
        currentMode = aCurrentMode;
        
        panRecognizer.enabled = (aCurrentMode!=PaintingModeSelect);
        
    }
}
*/

-(void)disblePanning:(BOOL)disable
{
    panRecognizer.enabled = !disable;
}

-(void)clear
{
    [self.shapes removeAllObjects];
    [self.drawingView setNeedsDisplay];
}

-(void)reload
{
    [drawingView setNeedsDisplay];
}

-(void)setShapes:(NSMutableArray *)_shapes
{
    if(shapes!=_shapes || [shapes count] != [_shapes count])
    {
        [shapes release];
        shapes = [_shapes retain];
        
        [drawingView setNeedsDisplay];
    }
}

-(void)dealloc
{
    delegate=nil;
    visitSessionViewController = nil;
    [shapes release];
    [drawingView release];
    [selectedShape release];
    //[parentShapes release];
    [selectedIndexes release];
    [panRecognizer release];
    // [scrollView release];
    
    [super dealloc];
}

- (BOOL) canBecomeFirstResponder 
{
    return YES;
}


- (BOOL)canPerformAction: (SEL)action withSender: (id)sender 
{
    BOOL answer = NO;
    
    if (action == @selector(deleteShapeAction:))
        answer = YES;
    
    return answer;
}

#pragma mark - View lifecycle

-(void)showDeleteMenuItem
{
    if(currentMode!=PaintingModeEdit) return;
    
    [self becomeFirstResponder];
    
    if(menuController ==nil)
    {
        menuController = [UIMenuController sharedMenuController];
        
        UIMenuItem *testMenuItem = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(deleteShapeAction:)];
        menuController.menuItems = [NSArray arrayWithObject:testMenuItem];
        [testMenuItem release];
    }
    
    CGRect targetRect = self.selectedShape.totalBounds;
    targetRect.origin = [self.selectedShape startPoint];

    [menuController setTargetRect:targetRect inView:self.drawingView];//self.view
    
    if(!menuController.isMenuVisible)
        [menuController setMenuVisible:YES animated:NO];
}



- (void)viewDidLoad
{
    //[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib
    
    self.backgroundColor = [UIColor clearColor];
    self.userInteractionEnabled = YES;
    
    drawingView =[[DrawingView alloc] initWithFrame:self.bounds];
    drawingView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    drawingView.backgroundColor = [UIColor clearColor];
    drawingView.contentSize=CGSizeMake(0, CGRectGetHeight(drawingView.bounds));
    drawingView.showsVerticalScrollIndicator=YES;
    drawingView.scrollEnabled = (currentMode==PaintingModeNone);;
    drawingView.bounces = NO;
    drawingView.dataSource  = self;
    drawingView.contentSize = drawingView.bounds.size;
    drawingView.multipleSelectionEnabled = NO;
    [self addSubview:drawingView];
    
    
    //ADD CUSTOM GESTURE RECOGNIZERS TO SCROLLVIEW
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    //tapRecognizer.cancelsTouchesInView = NO;
    [self.drawingView addGestureRecognizer:tapRecognizer];
    [tapRecognizer release];
    
       
    
    panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
    [self.drawingView addGestureRecognizer:panRecognizer];
    
    
    // self.currentMode=PaintingModeNone;
    
    
    [self.drawingView reloadData];
}


-(void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    //[self viewDidLoad];
}

-(void)editAction:(UIBarButtonItem *)sender
{
    //isDrawingMode = !isDrawingMode;
    
    if(currentMode==PaintingModeDraw)      currentMode=PaintingModeEdit;
    else if(currentMode==PaintingModeEdit) currentMode=PaintingModeDraw;
    
    [sender setTitle: (currentMode==PaintingModeDraw)? @"Draw": @"Drag"];
    
    
}

- (void)addShape:(Shape *)newShape
{
    //[[self.undoManager prepareWithInvocationTarget:self] deleteShape:newShape];
    
    [self.shapes addObject:newShape];
    [self.drawingView reloadDataInRect:newShape.totalBounds];
    
}

-(void)deleteShape:(Shape *)currentShape
{
    //[[self.undoManager prepareWithInvocationTarget:self] addShape:currentShape];
    
    /*
    //======================== MULTIPLE SELECT ============================
    CGRect tolRectToRedraw=CGRectZero;
    for(Shape *shape in self.selectedShapes)
    {
        CGRect rectToRedraw = shape.totalBounds;
        [self.shapes removeObject:shape];
        
        tolRectToRedraw=CGRectUnion(tolRectToRedraw, rectToRedraw);
    }
    
    self.selectedShapeIndex = NSNotFound;
    [self.drawingView reloadDataInRect:tolRectToRedraw];
    */
    
    //======================== SINGLE SELECT ============================
    if(currentShape)
    {
       CGRect rectToRedraw = currentShape.totalBounds;
        if([self.shapes containsObject:currentShape])
        {
          [self.shapes removeObject:currentShape];
          [self.drawingView reloadDataInRect:rectToRedraw];
          self.selectedShapeIndex = NSNotFound;
            
            if([self.delegate respondsToSelector:@selector(canvasDidDeleteShape:)])
            {
                [self.delegate canvasDidDeleteShape:self];
            }
        }
    }
}


- (void)deleteShapeAction:(id)sender
{
    if (self.selectedShapeIndex == NSNotFound)
    {
        return;
    }
    
    Shape *currentShape = [self.shapes objectAtIndex:self.selectedShapeIndex];
    [self deleteShape:currentShape];
}


- (void)setSelectedShapeIndex:(NSUInteger)selectedShapeIndex
{
    if(selectedShapeIndex==_selectedShapeIndex) return;
    
    __unused  CGRect oldSelectionBounds = CGRectZero;
    if (_selectedShapeIndex < [self.shapes count]) 
    {
        oldSelectionBounds = self.selectedShape.totalBounds;
    }
    _selectedShapeIndex = selectedShapeIndex;
    
    //if(currentMode!=PaintingModeDraw)
    {
    ///=============SINGLE SELECTION OF SHAPE. SHOULD BE DISABLE FOR MULTIPLE SELECTION=======
     CGRect newSelectionBounds = self.selectedShape.totalBounds;
     CGRect rectToRedraw = CGRectUnion(oldSelectionBounds, newSelectionBounds);
     [self.drawingView setNeedsDisplayInRect:rectToRedraw];
     //self.deleteShapeButton.enabled = (_selectedShapeIndex != NSNotFound);
        
    }
}

- (Shape *)selectedShape
{
    if (_selectedShapeIndex == NSNotFound) 
    {
        return nil;
    }
    return _selectedShapeIndex < [self.shapes count] ? [self.shapes objectAtIndex:_selectedShapeIndex] : nil;
}

- (NSUInteger)hitTest:(CGPoint)point
{
    __block NSUInteger hitShapeIndex = NSNotFound;
    [self.shapes enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(Shape * shape, NSUInteger idx, BOOL *stop)
     {
        if ([shape containsPoint:point]) 
        {
            hitShapeIndex = idx;
            *stop = YES;
        }
    }];
    return hitShapeIndex;
}

-(NSMutableSet *)_selectAllShapesForShape:(Shape *)aSelectedShape fromShapes:(NSMutableSet *)shapesArr
{
    if(![shapesArr count]) return nil;
    
    NSMutableSet *arr            = [NSMutableSet setWithSet:shapesArr];
    NSMutableSet *shapesToIgnore = [NSMutableSet setWithObjects:aSelectedShape,nil];
    
    for(Shape *aShpe in shapesArr)
    {
        if([shapesToIgnore containsObject:aShpe]) 
        {
            // NSLog(@"Ignore");
            continue;
        }
        
        BOOL b=(CGRectIntersectsRect(aSelectedShape.totalBounds, aShpe.totalBounds));
        
        if(b)
            b=[aSelectedShape intersectsWithShape:aShpe];
        
        if(b)
        {
            [shapesToIgnore addObject:aShpe];
            
            [arr minusSet:shapesToIgnore];
            
            NSMutableSet *connectedShapes = [self _selectAllShapesForShape:aShpe fromShapes:arr];
            
            //if(connectedShapes)
            //[shapesToIgnore addObjectsFromArray:connectedShapes.allObjects];
            
            
            [connectedShapes enumerateObjectsUsingBlock:^(id obj, BOOL *stop) 
             {
                 
                 [shapesToIgnore addObject:obj];
             }];
            
        }
    }
    
    return shapesToIgnore;
}


#pragma mark - Touch handling

- (void)tapDetected:(UIPanGestureRecognizer *)tapRecognizer
{
    // isDoubleTappedMode=NO;
    CGPoint tapLocation = [tapRecognizer locationInView:self.drawingView];
    self.selectedShapeIndex = [self hitTest:tapLocation];
    
        if(self.selectedShapeIndex != NSNotFound)
        {
            _drawNewShape = NO;
            
            if(currentMode==PaintingModeEdit)
             {
               [self showDeleteMenuItem];
             }
            else if(currentMode==PaintingModeSelect)
            {
                //[self.drawingView setNeedsDisplay];
                if([self.delegate respondsToSelector:@selector(canvas:didSelectShape:)])
                {
                    [self.delegate canvas:self didSelectShape:self.selectedShape];
                }
            }
            
        }
        else
        {
            _drawNewShape = YES;
            
            if([self.delegate respondsToSelector:@selector(canvas:didSelectShape:)])
            {
                [self.delegate canvas:self didSelectShape:nil];
            }
            
           // panRecognizer.enabled = (currentMode!=PaintingModeSelect);
           
        }
    /*
    if(currentMode!=PaintingModeDraw && !isProcessRunning)
    {
        isProcessRunning=YES;
        
        // CALCULATE ALL SHAPES CONNECTED TO SELECTED SHAPE.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            //  IF CURRENT SELECTED SHAPE REMAINS INSIDE PREV SELECTED SHAPES THEN WE DONT NEED TO RE-CALCULATE.
            if(![self.selectedShapes containsObject:self.selectedShape])
            {
                NSMutableSet *allShapes=[[NSMutableSet alloc] initWithArray:self.shapes];
                NSMutableSet *connectedShapes= [self _selectAllShapesForShape:self.selectedShape fromShapes:allShapes];
                self.selectedShapes=connectedShapes;
                
                [allShapes release];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.drawingView setNeedsDisplay];
                isProcessRunning=NO;
                
                if(self.selectedShapeIndex != NSNotFound )
                {
                    [self showDeleteMenuItem];
                }
                
            });
            
        });
        
    }
    */
}



- (void)doubleTapDetected:(UITapGestureRecognizer *)tapRecognizer
{
    //CGPoint tapLocation = [tapRecognizer locationInView:self.drawingView];
    // isDoubleTappedMode=YES;
}

-(void)refreshSelectedShape
{
    CGRect oldSelectionBounds = CGRectZero;
    if (_selectedShapeIndex < [self.shapes count]) 
    {
        oldSelectionBounds = self.selectedShape.totalBounds;
    }
    
    CGRect newSelectionBounds = self.selectedShape.totalBounds;
    CGRect rectToRedraw = CGRectUnion(oldSelectionBounds, newSelectionBounds);
    [self.drawingView setNeedsDisplayInRect:rectToRedraw];
}

-(void)addNewShapeAtPoint:(CGPoint)tapLocation
{
    Shape *newShape =  [Shape emptyShapeWithLineWidth:2.0 lineColor:[UIColor blackColor]];
    [newShape addPoint:tapLocation];
    [self addShape:newShape]; 
    
    self.selectedShapeIndex = [self.shapes count]>0 ? [self.shapes count]-1 : 0;
    [self.drawingView reloadDataInRect:self.selectedShape.totalBounds];
}

- (void)panDetected:(UIPanGestureRecognizer *)_panRecognizer
{
   // if(isProcessRunning) return;
    
    switch (_panRecognizer.state) 
    {
        case UIGestureRecognizerStateBegan: 
        {
            //============================
            
            _drawNewShape = (_selectedShapeIndex==NSNotFound) ;
            
            CGPoint tapLocation = [panRecognizer locationInView:self.drawingView];
            //if(currentMode==PaintingModeDraw)
            if(_drawNewShape && (currentMode==PaintingModeDraw || currentMode == PaintingModeEdit))
            {
                [self addNewShapeAtPoint:tapLocation];
                
                if([self.delegate respondsToSelector:@selector(canvas:willDrawShape:)])
                {
                    [self.delegate canvas:self willDrawShape:self.selectedShape];
                }
            }
        break;
        }
        case UIGestureRecognizerStateChanged: 
        {
             if(_drawNewShape && ( currentMode==PaintingModeDraw || currentMode == PaintingModeEdit))
            //if(currentMode==PaintingModeDraw) //isDrawingMode
            {
                CGPoint tapLocation = [panRecognizer locationInView:self.drawingView];
                [self.selectedShape addPoint:tapLocation];
                [self.drawingView reloadDataInRect:self.selectedShape.totalBounds];
            }
           // else if(currentMode==PaintingModeEdit)
            else
            {
                CGPoint translation = [panRecognizer translationInView:self.drawingView];
                CGRect rectToRedraw=CGRectZero;
                
                //========== MULTIPLE SHAPE ONLY ===============
                /*
                for(Shape *shape in self.selectedShapes)
                {
                    CGRect _rectToRedraw=[shape translateBy:translation];
                    rectToRedraw=CGRectUnion(rectToRedraw, _rectToRedraw);
                }
                */
                //=============== SINGLE SHAPE ONLY ===============
                if(self.selectedShape)
                {
                   rectToRedraw=[self.selectedShape translateBy:translation];
                }
                
                [self.drawingView reloadDataInRect:rectToRedraw];
                [panRecognizer setTranslation:CGPointZero inView:self.drawingView];
                //enableUndoStack=YES;
            }
        break;
        }
            
        case UIGestureRecognizerStateEnded:
        {
            //if(currentMode==PaintingModeDraw) //isDrawingMode
             if(_drawNewShape && (currentMode==PaintingModeDraw || currentMode == PaintingModeEdit))
            {
                [self.selectedShape finishDrawingPath];
                
                if([self.delegate respondsToSelector:@selector(canvas:didFinishDrawingShape:)])
                {
                    [self.delegate canvas:self didFinishDrawingShape:self.selectedShape];
                }
            }
            
            //self.selectedShape = nil;
            [self refreshSelectedShape];
            _selectedShapeIndex = NSNotFound;

            /*
             if(isDoubleTappedMode)
             {
             
             if(self.selectedShape.parentShape)
             {
             [self.selectedShape.parentShape.connectedShapes removeObject:self.selectedShape];
             self.selectedShape.parentShape=nil;
             }
             else
             {
             [self.selectedShape.connectedShapes removeAllObjects];
             [self.selectedShape.connectedShapes addObject:self.selectedShape];
             }
             
             if(self.selectedShape && ![parentShapes containsObject:self.selectedShape])
             [parentShapes addObject:self.selectedShape];
             }
             else
             */
            
        }
        break;
            
        default:
        break;
    }
}



#pragma mark - DrawingViewDataSource

- (NSUInteger)numberOfShapesInDrawingView:(DrawingView *)drawingView
{
    return [self.shapes count];
}

- (UIBezierPath *)drawingView:(DrawingView *)drawingView pathForShapeAtIndex:(NSUInteger)shapeIndex
{
    Shape *shape = [self.shapes objectAtIndex:shapeIndex];
    return shape.path;
}

- (UIColor *)drawingView:(DrawingView *)drawingView lineColorForShapeAtIndex:(NSUInteger)shapeIndex
{
    Shape *shape = [self.shapes objectAtIndex:shapeIndex];
    return shape.lineColor;
}

//=============================== SINGLE SELECT ============================
 - (NSUInteger)indexOfSelectedShapeInDrawingView:(DrawingView *)drawingView
 {
     //if(self.currentMode==PaintingModeDraw) return NSNotFound;
      if(_drawNewShape) return NSNotFound;
     
    return self.selectedShapeIndex;
 }


/* =============================== MULTIPLE SELECT =========================
- (NSIndexSet *)indexesOfSelectedShapeInDrawingView:(DrawingView *)drawingView
{
    if(self.currentMode!=PaintingModeEdit) return nil;
    
    NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
    
    for(Shape *shape in self.selectedShapes)
    {
        NSInteger index = [self.shapes indexOfObject:shape];
        if(index!=NSNotFound) [indexSet addIndex:index];
    }
    
    return [indexSet autorelease];
}
*/



@end

