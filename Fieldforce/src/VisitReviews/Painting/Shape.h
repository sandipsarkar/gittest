//
//  Shape.h
//  PathHitTesting
//
//  Created by Ole Begemann on 30.01.12.
//  Copyright (c) 2012 Ole Begemann. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum 
{
    ShapeTypeRect,
    ShapeTypeEllipse,
    ShapeTypeHouse,
    ShapeTypeArc,
    SHAPE_TYPE_COUNT
    
} ShapeType;


@class ANPathBitmap;
@interface Shape : NSObject<NSCoding>

+(id)emptyShapeWithLineWidth:(float)lineWidth lineColor:(UIColor *)lineColor;
+ (id)randomShapeInBounds:(CGRect)maxBounds;
+ (id)shapeWithPath:(UIBezierPath *)path lineColor:(UIColor *)lineColor;
- (id)initWithPath:(UIBezierPath *)path lineColor:(UIColor *)lineColor;

- (BOOL)containsPoint:(CGPoint)point;
- (void)moveBy:(CGPoint)delta;
-(CGRect)translateBy:(CGPoint)delta;

@property (nonatomic, strong) UIBezierPath *path;
@property (nonatomic, strong) UIBezierPath *tapTarget;;
@property (nonatomic, strong) UIColor *lineColor;
@property (nonatomic, assign, readonly) CGRect totalBounds;
@property (nonatomic, retain, readonly) ANPathBitmap *pathBitmap;
@property (nonatomic,assign) double audioTag;

@property (nonatomic,readonly)   NSMutableArray *childrenShapes;
@property(nonatomic,assign) Shape *parentShape;

-(CGPoint)startPoint;
-(void)setParentShape:(Shape *)aParentShape;
-(BOOL)intersectsWithShape:(Shape *)aShape;
-(void)addPoint:(CGPoint)aPoint;
-(void)finishDrawingPath;
//-(CGRect)actualBounds;

@end
