//
//  DrawingCanvasView.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum
{
    PaintingModeNone=-1,
    PaintingModeDraw=0,
    PaintingModeSelect,
    PaintingModeEdit,
    
}PaintingMode;


@class DrawingView;
@class Shape;
@class VisitSessionViewController;
@protocol DrawingDelegate;
@interface DrawingCanvasView : UIView

@property (nonatomic,retain) NSMutableArray *shapes;
@property (nonatomic ,readonly) NSUInteger selectedShapeIndex;
@property (nonatomic ,retain,readonly) Shape *selectedShape;
@property (nonatomic,assign) id<DrawingDelegate> delegate;

@property(nonatomic,assign)PaintingMode currentMode;
@property (nonatomic,readonly) DrawingView *drawingView;
@property (nonatomic,assign) VisitSessionViewController *visitSessionViewController;

-(id)initWithShapes:(NSMutableArray *)_shapes;

-(void)clear;
-(void)reload;
-(void)deleteShape:(Shape *)currentShape;
-(void)addShape:(Shape *)newShape;
-(void)disblePanning:(BOOL)disable;

@end

@protocol DrawingDelegate <NSObject>

@optional

-(void)canvas:(DrawingCanvasView *)canvas willDrawShape:(Shape *)ashape;
-(void)canvas:(DrawingCanvasView *)canvas didFinishDrawingShape:(Shape *)ashape;
-(void)canvas:(DrawingCanvasView *)canvas didSelectShape:(Shape *)ashape;
-(void)canvasDidDeleteShape:(DrawingCanvasView *)canvas; 

@end
