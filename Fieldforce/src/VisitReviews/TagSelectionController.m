//
//  TagSelectionController.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 24/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "TagSelectionController.h"
#import "CustomersViewController.h"
#import "CoreDataHandler.h"
#import "OEM.h"

@interface TagSelectionController ()<UIPickerViewDelegate,UIPickerViewDataSource>

@property(nonatomic,copy) TagSelectedCallback callback;
@property(nonatomic,copy) WillDismissCallback dismissCallback;
@end

@implementation TagSelectionController
@synthesize tags;
@synthesize callback;
@synthesize dismissCallback;
@synthesize strOems;
@synthesize delegate;

- (id)init
{
    self = [super init];
    if (self) 
    {
       
          }
    return self;
}

-(void)dealloc
{
    delegate = nil;
    [tags release] ;
    tags = nil;
    if(callback) [callback release];
    if(dismissCallback) [dismissCallback release];
    [strOems release];
    [arrOemName release];
    [pickerView release];
    
    [super dealloc];
}

+(NSArray *)dataSource
{
    return [NSArray arrayWithObjects:@"None",@"General",@"Subaru",@"Suzuki",@"Honda",@"Mitsubishi", nil];
}


-(void)addDidSelectCallback:(TagSelectedCallback)aCallback
{
    if(aCallback) self.callback = aCallback;
}

-(void)addDismissCallback:(WillDismissCallback)aCallback
{
    if(aCallback) self.dismissCallback = aCallback;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if TRACK_ERROR
    //BREADCRUMBS
    [BugSenseController leaveBreadcrumb:NSStringFromClass([self class])];
#endif
    
    self.view.backgroundColor = [UIColor clearColor];
    NSLog(@"%@",self.strOems);
    

    NSMutableArray *arrLoc=[[NSMutableArray alloc]init];
    self.tags=arrLoc;
    [arrLoc release];
    
     NSArray *arr=[self.strOems componentsSeparatedByString:@","];
    
    NSArray *dealingOEMS = [CoreDataHandler intersectWithDealingOEMs:arr];
    
    if([dealingOEMS count])
        [self.tags setArray:[dealingOEMS valueForKey:@"name"]];
    
  
 
    [self setupPickerView];
    
    if([self.tags count])
    {
        [self.tags sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
       
    }
    
     [self.tags insertObject:@"General" atIndex:0];
    
    [pickerView reloadAllComponents];
    
}

-(void)setupPickerView
{
    
  if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
  {
    
    if(pickerView==nil)
    {
        pickerView = [[UIPickerView alloc] initWithFrame:CGRectZero];
        CGSize pickerSize = [pickerView sizeThatFits:self.view.bounds.size];
        
        UIView *pickerViewContainer = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds)-pickerSize.height, self.view.bounds.size.width, pickerSize.height)];
        pickerViewContainer.backgroundColor = CELL_BACKGROUND_COLOR;
        pickerView.frame = CGRectMake(0, 0, pickerViewContainer.bounds.size.width, pickerSize.height);
         pickerViewContainer.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        
        pickerView.delegate = self;
        pickerView.dataSource = self;
        pickerView.showsSelectionIndicator = YES;
        pickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
       
        [pickerViewContainer addSubview:pickerView];
        [self.view addSubview:pickerViewContainer];
        
        [pickerViewContainer release];
        pickerViewContainer.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4] .CGColor;
        pickerViewContainer.layer.borderWidth = 1.0;
        
    }

  }
 else
 {
    
    pickerView = [[UIPickerView alloc] initWithFrame:CGRectZero];
    CGSize pickerSize = [pickerView sizeThatFits:self.view.bounds.size];
     pickerView.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds)-pickerSize.height, self.view.bounds.size.width, pickerSize.height);
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    pickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:pickerView];
    
 }
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[pickerView selectRow:2 inComponent:0 animated:NO];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    NSString *tag= row<[self.tags count] ?  [self.tags objectAtIndex:row] : nil;
    
    if(self.callback) self.callback(tag);
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(tagSelectionController: didSelectTag:)])
    {
        [self.delegate tagSelectionController:self didSelectTag:tag];
    }
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView 
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component 
{
    return [self.tags count];
}

/*
// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component 
{
    NSString *tag= row<[self.tags count] ?  [self.tags objectAtIndex:row] : nil;
    return tag;
}
*/


-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 45.0;
}


-(UIView *)pickerView:(UIPickerView *)_pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *itemLabel = (UILabel *)view;
    if(!itemLabel)
    {
        itemLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_pickerView.bounds), 45.0)] autorelease];
        itemLabel.backgroundColor = [UIColor clearColor];
        itemLabel.textColor = [UIColor blackColor];
        itemLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        itemLabel.font = [UIFont grotesqueBoldFontOfSize:20.0];
    }
    
    NSString *tag= row<[self.tags count] ?  [self.tags objectAtIndex:row] : nil;
    itemLabel.text =[NSString stringWithFormat:@"      %@",tag];
    
    return itemLabel;
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(self.dismissCallback) self.dismissCallback();
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(tagSelectionControllerWillDissmiss:)])
    {
        [self.delegate tagSelectionControllerWillDissmiss:self];
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
   
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return   UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
