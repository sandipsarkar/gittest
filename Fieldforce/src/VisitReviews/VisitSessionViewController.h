//
//  VisitSessionViewController.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 10/09/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Visit,Customer;
@class AudioManager;

@interface VisitSessionViewController : UIViewController
{
    UIButton *editButton;
    UIButton *playButton;
    UIButton *recordButton;
    UIButton *publishReportButton;
    UIButton *completeReportButton;
    
    BOOL isReportCompleted;
    BOOL isRecording;
    BOOL isPlaying;
    BOOL isPaused;
    AudioManager *_audioManager;
    
    double recodedAudioDuration;
    double recordTick;
    double timerTick;
    NSTimer *sliderTimer;
    
    NSTimeInterval _avoidInterval;
    
}

@property (nonatomic,retain)   Visit *visit;
@property (nonatomic,readonly) AudioManager *audioManager;
@property (nonatomic,readonly) BOOL isReportCompleted;
@property (nonatomic,readonly) BOOL isVisitCompleted;
@property (nonatomic,retain)   NSDate *lastLocalNotificationDate;

@property(nonatomic,assign) BOOL isFromCalendar;

-(id)initWithVisit:(Visit *)aVisit forCustomer:(Customer *)aCustomer;
-(NSTimeInterval)currentRecordingTime;
-(void)tagAudio:(NSTimeInterval)tag forText:(NSString *)text;
-(void)saveData;
-(void)showEditBottombar:(BOOL)show;
-(void)enableTagging:(BOOL)yesNo;
-(BOOL)isTaggingEnabled;

-(void)clear;

@end
