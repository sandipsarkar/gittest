//
//  AddressViewController.h
//  TestSuburb
//
//  Created by Subhojit Dey on 11/28/12.
//  Copyright (c) 2012 Subhojit Dey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomInvocationOperation.h"

#define  TAG_NAME               @"name"      //@"suburb"
#define  TAG_SUBURB_PINCODE		@"postcode"  //@"postCode"
#define  TAG_STATE				@"state"
#define  TAG_CITY               @"city"
#define  TAG_REGION             @"region"
#define  TAG_SUBURB_ID          @"suburbID"

typedef void (^AddressSelectedCallback)(NSString  *suburbName,NSString  *stateName,NSString  *pinCode,NSDictionary *addressDict);

@protocol AddressViewDelegate;

@interface AddressViewController : UIViewController<UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource,InvocationOperationDelegate>
{
  

	UISearchBar *	suburbSearchBar;
	UITableView *	suburbTableView;
	
	
	NSMutableArray * mainSearchContent;	// The content filtered as a result of a search.
	NSMutableArray * filteredSearchContent;	// The content filtered as a result of a search with paging.
	UIButton * butNext;
	UIButton * butPrevious;
	UIView * headerView;
	NSMutableArray * activeDbOutlets;
	UILabel * suburbTableNoRecordLbl;
	
	
	NSTimer * _timer;
	NSString * currentSearchkey;
    NSMutableDictionary * dbDict;
    NSMutableArray* searchValues;
      NSOperationQueue * operationQue;
    dispatch_queue_t backgroundQueue;
    
}
-(void)addAddressSelectedCallback:(AddressSelectedCallback)aAddressSelectedCallback;
- (NSMutableArray *) populateOutputWithPredicate: (NSPredicate *) predicate  AndData: (NSMutableArray *) data;
//- (void) modifySearchPagingView;
// (void) reloadSearchPagingHeader;
@property(nonatomic,retain)	NSMutableArray* filteredSearchContent;
@property(nonatomic,retain)NSMutableDictionary *dbDict;
@property(nonatomic,retain)NSMutableArray *searchValues;
@property(nonatomic,retain)NSOperationQueue *operationQue;;
@property (nonatomic, retain) UIView *headerView;
@property (nonatomic,retain) NSString *currentSearchkey;
@property (nonatomic,retain) NSMutableArray *activeDbOutlets;
@property (nonatomic,retain) UITableView       *suburbTableView;
@property (nonatomic,retain) UILabel	   *suburbTableNoRecordLbl;
@property(nonatomic,copy) AddressSelectedCallback addressSelectedCallback;

@property(nonatomic,assign) id < AddressViewDelegate > delegate;

@end

@protocol AddressViewDelegate <NSObject>

@optional

-(void)addressViewController:(AddressViewController *)addressVC didSelectAddress:(NSDictionary *)addressDict;

@end
