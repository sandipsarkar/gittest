 //
//  ConnectionManager.m
//  ARonFBTest
//
//  Created by RANDEM MAC on 03/05/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.
//

#import "ConnectionManager.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "OfflineManager.h"
#import "OfflineStore.h"
#import "Rep.h"
#import "Visit.h"
#import "Customer.h"
#import "VisitPhoto.h"
#import "JSON.h"
#import "VisitTypesViewcontroller.h"
#import "ScheduledVisit.h"
#import "WholesaleDealer.h"
#import "CustomerPhoto.h"
#import "Offer.h"
#import "LocationManager.h"
#import "Territory.h"
#import "Task.h"
#import "Reminder.h"
#import "CustomEventStore.h"

#define WCF_REQUEST_FORMAT(str) [NSString stringWithFormat:@"<RequestData xmlns=\"http://www.eysnap.com/mPlayer\"><details>%@</details></RequestData>",str]

#define WCF_REQUEST_FORMAT2(str,deviceStr) [NSString stringWithFormat:@"<RequestData xmlns=\"http://www.eysnap.com/mPlayer\"><details>%@</details><device>%@</device></RequestData>",str,deviceStr]

//#define APP_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]

static NSString *formatForXML(NSString *input)
{
    static NSDictionary *_xmlFormatDict = nil;
    
    if(!_xmlFormatDict)
    {
        _xmlFormatDict = [[NSDictionary alloc]  initWithObjectsAndKeys:
                          @"&amp;",@"&",
                          @"&lt;",@"<",
                          @"&gt;",@">",
                          nil];
        
       
    }
    
    if(!input.length) return input;
    
    
    for(NSString *key in _xmlFormatDict)
    {
        if(!input.length) break;
        
        NSString *target = [_xmlFormatDict valueForKey:key];
        input = [input stringByReplacingOccurrencesOfString:key withString:target];
    }
    
    return input;
}


@implementation ConnectionManager

/*
static NSString * makeURL(NSString *serverLink, NSString *wcfLink, NSString *methodName)
{
    if(!serverLink.length || !wcfLink.length || !methodName.length) return nil;
 
    NSString *finalUrl = [@"http://" stringByAppendingFormat:@"%@/%@/%@",serverLink,wcfLink,methodName];
    return finalUrl;
}
 */
NSString * makeURL(NSString *methodName)
{
    if(!methodName.length) return nil;
    
    NSString *finalUrl = [@"http://" stringByAppendingFormat:@"%@/%@/%@",SERVER_LINK,OEM_WCF_URL,methodName];
    return finalUrl;
}

NSString * makeImageURL(NSString *imageName,NSString *imageType,BOOL isThumb)
{
    if(!imageName.length || !imageType.length) return nil;
    
    NSString *finalUrl = [@"http://" stringByAppendingFormat:@"%@/%@/%@/",DOWNLOAD_SERVER_LINK,IMAGE_LINK,imageType];
    if(isThumb)
        finalUrl = [finalUrl stringByAppendingString:@"thumb/"];
    finalUrl = [finalUrl stringByAppendingString:imageName];
    
    return finalUrl;
}

NSString * makeImageURLString(NSString *imageName,NSString *imageType,ImageSizeType imageSizeType)
{
    if(!imageName.length || !imageType.length) return nil;
    
    NSString *finalUrl = [@"http://" stringByAppendingFormat:@"%@/%@/%@/",DOWNLOAD_SERVER_LINK,IMAGE_LINK,imageType];
  
    switch (imageSizeType) 
    {
        case ImageSizeTypeDefault:
            
        break;
            
        case ImageSizeTypeLarge:
            finalUrl = [finalUrl stringByAppendingString:@"large/"];
        break;
            
        case ImageSizeTypeMedium:
            finalUrl = [finalUrl stringByAppendingString:@"medium/"];
        break;
            
        case ImageSizeTypeThumb:
            finalUrl = [finalUrl stringByAppendingString:@"thumb/"];
        break;
            
        case ImageSizeTypeMediumLarge:
           finalUrl = [finalUrl stringByAppendingString:@"MediumLarge/"];
            break;
            
        default: break;
    }
        
    finalUrl = [finalUrl stringByAppendingString:imageName];
    return finalUrl;
}

NSString * makePDFURL(NSString *pdfName,NSString *sourceType)
{
    if(!pdfName.length || !sourceType.length) return nil;
    
    NSString *finalUrl = [@"http://" stringByAppendingFormat:@"%@/%@/%@/Pdf/",DOWNLOAD_SERVER_LINK,PDF_LINK,sourceType];
  
    finalUrl = [finalUrl stringByAppendingString:pdfName];
    
    return finalUrl;
}


+(void)handleError:(NSError *)error
{
    [UIAlertView alertViewWithTitle:@"Network Error!" message:error.description cancelButtonTitle:@"Ok"];
}

+(ASIHTTPRequest *)createWCFConnectionWithUrl:(NSString *)aUrl jsonParam:(NSString *)jsonString HTTPMethod:(NSString *)method
{
    if(!aUrl.length) return nil;
    
   // NSString *operationKey  = [aUrl lastPathComponent];
   // NSString *baseURLString = [aUrl stringByDeletingLastPathComponent];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:aUrl]];
    
     NSString *deviceTypeStr = [NSString stringWithFormat:@"%@,%@",[[UIDevice currentDevice] model],[[UIDevice currentDevice] uniqueGlobalDeviceIdentifier]];
    
    if([method isEqualToString:@"POST"] && jsonString.length)
    {
        NSString *paramString = jsonString.length ?  formatForXML(jsonString) : nil;
        paramString = paramString.length ?  WCF_REQUEST_FORMAT(paramString) : nil;
        
       // NSString *paramString = jsonString.length ?  WCF_REQUEST_FORMAT(jsonString) : nil;
    
        NSLog(@"DeviceTypeString=%@",deviceTypeStr);
        
        NSData *postData = paramString.length ?  [paramString dataUsingEncoding:NSUTF8StringEncoding] : nil;
        NSUInteger contentLength = [postData length];
        if(contentLength)
        {
            //====== IF CONTENT LENGTH IS LARGER THAN 100 KB=============
            //request.shouldStreamPostDataFromDisk = contentLength>100*1024;
            [request appendPostData:postData];
        }

        /*
        if(paramString.length)[request appendPostData:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
        */
        
        NSLog(@"POST DATA=%@",paramString);
        
        [request addRequestHeader:@"Content-Length" value:[NSString stringWithFormat:@"%d",contentLength]];
        [request setRequestMethod:[method uppercaseString]];
        [request addRequestHeader:@"User-Agent" value:deviceTypeStr];
        [request addRequestHeader:@"Content-Type" value:@"application/xml"];
        [request addRequestHeader:@"Version" value:APP_VERSION];
        //[request addRequestHeader:@"operationKey" value:operationKey];
        
    }
    else if ([method isEqualToString:@"GET"])
    {
        [request setRequestMethod:[method uppercaseString]];
        [request addRequestHeader:@"User-Agent" value:deviceTypeStr];
        [request addRequestHeader:@"Version" value:APP_VERSION];
    }
    
    request.timeOutSeconds = CONNECTION_TIME_OUT;
    request.shouldContinueWhenAppEntersBackground=YES;
    
    [request startAsynchronous];
    
    return request;
}

+(ASIHTTPRequest *)createWCFConnectionWithUrl:(NSString *)aUrl jsonParam:(NSString *)jsonString
{
   return  [self createWCFConnectionWithUrl:aUrl jsonParam:jsonString HTTPMethod:@"POST"];
     
}

+(ASIHTTPRequest *)createWCFConnectionWithUrl:(NSString *)aUrl
{
  return  [self createWCFConnectionWithUrl:aUrl jsonParam:nil HTTPMethod:@"GET"];
}
@end


@implementation ConnectionManager(RepToolConnection)

#pragma mark - VALIDATION

+(BOOL)isValidResponse:(NSDictionary *)jsonDict
{
    if(!jsonDict) return NO;
    
    BOOL isValid = YES;
    
    BOOL _isSiteIDChanged  = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
    BOOL _isRepDeactivated = [[jsonDict valueForKey:JSON_REP_DEACTIVATE_KEY] boolValue];

    NSNumber *isUnderMaintenance  = [jsonDict objectForKey:JSON_UNDER_MAINTENANCE_KEY];
    isUnderMaintenance = NULL_NIL(isUnderMaintenance);

    if(_isSiteIDChanged)
    {
        [app appDidChangeSiteID];
        
        isValid = NO;
    }
    else if(_isRepDeactivated)
    {
        [app repDidDeactivate];
        
        isValid = NO;
    }
    else if([isUnderMaintenance boolValue])
    {
        [app showUnderMaintainenceScreen:YES];
        
        isValid = NO;
    }
    
    return isValid;
}


#pragma mark - LAST REQUESTED

+(NSNumber *)lastRequestedForConnectionType:(NSString *)requestType
{
    //id number = [[NSUserDefaults standardUserDefaults] objectForKey:requestType];
    
    
    id number=nil;
    
    OfflineStore *currentOfflineStore = [CoreDataHandler offlineStore];
    
    if([requestType isEqualToString:LAST_REQUESTED_SUBURBS])
    {
        number = currentOfflineStore.lastRequestedSuburbDate;
    }
    else if([requestType isEqualToString:LAST_REQUESTED_NEWS])
    {
        number = currentOfflineStore.lastRequestedNewsDate;
    }
    else if([requestType isEqualToString:LAST_REQUESTED_OFFERS])
    {
        number = currentOfflineStore.lastRequestedOffersDate;
    }
    else if([requestType isEqualToString:LAST_REQUESTED_CUSTOMERS])
    {
        number = currentOfflineStore.lastRequestedCustomersDate;
    }
    else if([requestType isEqualToString:LAST_REQUESTED_CONTACTS])
    {
        number = currentOfflineStore.lastRequestedCustomersDate;
    }
    else if([requestType isEqualToString:LAST_REQUESTED_VISITS])
    {
        number = currentOfflineStore.lastRequestedScheduleVisitDate;
    }
    else if([requestType isEqualToString:LAST_REQUESTED_TASKS])
    {
        number = currentOfflineStore.lastRequestedTaskDate;
    }
    else if([requestType isEqualToString:LAST_REQUESTED_REMINDERS])
    {
        number = currentOfflineStore.lastRequestedReminderDate;
    }
    else if([requestType isEqualToString:LAST_REQUESTED_RESOURCES])
    {
        number = currentOfflineStore.lastRequestedResourcesDate;
    }
    // NSLog(@" number %@",number);
    
    if(!number) number = [NSNull null];
   
    return (NSNumber *) number;
}

+(void)setLastRequested:(NSNumber *)number forConnectionType:(NSString *)connectionType
{
    //if(!number) return;
    
    // [[NSUserDefaults standardUserDefaults] setObject:number forKey:connectionType];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    
    
    OfflineStore *currentOfflineStore = [CoreDataHandler offlineStore];
    if([connectionType isEqualToString:LAST_REQUESTED_SUBURBS])
    {
        currentOfflineStore.lastRequestedSuburbDate = number;
    }
    else if([connectionType isEqualToString:LAST_REQUESTED_NEWS])
    {
        currentOfflineStore.lastRequestedNewsDate = number;
    }
    else if([connectionType isEqualToString:LAST_REQUESTED_OFFERS])
    {
        currentOfflineStore.lastRequestedOffersDate = number;
    }
    else if([connectionType isEqualToString:LAST_REQUESTED_CUSTOMERS])
    {
        currentOfflineStore.lastRequestedCustomersDate = number;
    }
    else if([connectionType isEqualToString:LAST_REQUESTED_VISITS])
    {
        currentOfflineStore.lastRequestedScheduleVisitDate = number;
    }
    else if([connectionType isEqualToString:LAST_REQUESTED_TASKS])
    {
        currentOfflineStore.lastRequestedTaskDate = number;
    }
    else if([connectionType isEqualToString:LAST_REQUESTED_REMINDERS])
    {
        currentOfflineStore.lastRequestedReminderDate = number;
    }
    else if([connectionType isEqualToString:LAST_REQUESTED_RESOURCES])
    {
        currentOfflineStore.lastRequestedResourcesDate = number;
    }
    
    [[SSCoreDataManager sharedManager]save:nil];
}

#pragma mark - APPLICATION
+(ASIHTTPRequest *)fetchLogoForWDSiteID:(NSNumber *)wdSiteID repID:(NSNumber *)repID
{
    
    NSString *connectionURL = makeURL(FETCH_LOGO_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    NSMutableDictionary *uploadDict = [NSMutableDictionary dictionary];
    
    [uploadDict setValue: wdSiteID ? wdSiteID:[NSNull null] forKey:@"wdSiteID"];
    [uploadDict setValue: repID ? repID:[NSNull null]       forKey:@"repID"];
    
    //------------------------------ ADDED BY SANDIP ON 6 DECEMBER 2013 ----------------------------
    
    // [uploadDict setValue: [NSNumber numberWithBool:YES] forKey:@"shouldFetchState"];
    
    NSNumber *lastUpdatedStateDate = [self lastRequestedForConnectionType:LAST_REQUESTED_SUBURBS];
    
    if([lastUpdatedStateDate isEqual:[NSNull null]] || [lastUpdatedStateDate doubleValue]==0)
    {
        NSNumber *_lastUpdatedStateDate =  [[NSUserDefaults standardUserDefaults] objectForKey:BUILD_VERSION_UPLOADED_DATE]; //LAST_VERSION_UPDATED_DATE
        if(_lastUpdatedStateDate)
            lastUpdatedStateDate = _lastUpdatedStateDate;
    }
    
    [uploadDict setValue:lastUpdatedStateDate ? lastUpdatedStateDate : [NSNull null] forKey:@"lastRequested"];
    
       
   
    //-----------------------------------------------------------------------------------------------
    
    NSString * json = [uploadDict JSONRepresentation];
    
    NSLog(@"Log JSON %@",json);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:json HTTPMethod:@"POST"];
    
    return request;
    
}

+(ASIHTTPRequest *)sendMailWithRep:(Rep *)rep wdSiteID:(NSNumber *)wdId deviceInfo:(NSString *)dInfo  offer:(Offer *)offer subjet:(NSString *)sText customerIDs:(NSArray *)customerIDS
{
    NSString *connectionURL = makeURL(SEND_OFFER_EMAIL_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    NSMutableDictionary *uploadDict = [NSMutableDictionary dictionary];
    [uploadDict setValue:rep.repID forKey:@"repID"];
    [uploadDict setValue:wdId  forKey:@"wdSiteID"];
    [uploadDict setValue:dInfo?dInfo:@"" forKey:@"deviceDescription"];
    [uploadDict setValue:sText.length?sText:@"" forKey:@"subject"];
    
    NSString *toEmailIDs = customerIDS ? [customerIDS componentsJoinedByString:@","] : @"";
    [uploadDict setValue:toEmailIDs forKey:@"toEmail"];
    [uploadDict setValue:offer.offerID?offer.offerID:@"" forKey:@"offerID"];
    
    /*
     NSString *offerName    = offer.offerName.length?offer.offerName:@"";
     // NSString *imageURL    = self.selectedOffer.offerImage? makeImageURL(self.selectedOffer.offerImage, @"offer", NO):nil;
     NSString *imageURL     = offer.offerImage? makeImageURLString(offer.offerImage, @"offer", ImageSizeTypeLarge):nil;
     
     NSString *offerDescription = offer.offerDescription.length?offer.offerDescription:@"";
     NSString *pdfURL  = offer.offerPdf.length?offer.offerPdf:@"";
     
     NSString *infoURL = offer.offerLinks.length?offer.offerLinks:@"";
     
     
     //[uploadDict setValue:rep.email.length?rep.email:@"" forKey:@"repEmailID"];
     [uploadDict setValue:offerName forKey:@"offerName"];
     [uploadDict setValue:pdfURL forKey:@"pdfLink"];
     [uploadDict setValue:imageURL forKey:@"offerImage"];
     [uploadDict setValue:infoURL forKey:@"offerLinks"];
     [uploadDict setValue:offerDescription forKey:@"offerDescription"];
     */
    
    NSString * json = [uploadDict JSONRepresentation];
    
    NSLog(@"submit feed %@",json);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:json HTTPMethod:@"POST"];
    return request;
}
#pragma mark -
+(ASIHTTPRequest *)checkApplicationVersionWithJson:(NSString *)aVersionJson
{
    //if(!aVersionJson) return nil;
    
    NSLog(@" aVersionJson  %@",aVersionJson);
    NSString *connectionURL = makeURL(CHECK_VERSION);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIHTTPRequest *request =[ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:aVersionJson HTTPMethod:@"GET"];
    
    return request;
}

+(ASIHTTPRequest *)createDeviceTokenWithJson:(NSString *)aDeviceTokenJson
{
    //if(!aVersionJson) return nil;
    
    NSLog(@" aVersionJson  %@",aDeviceTokenJson);
    NSString *connectionURL = makeURL(SEND_DEVICE_TOKEN);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:aDeviceTokenJson HTTPMethod:@"POST"];
    
    return request;
}

+(ASIHTTPRequest *)sendDeviceLocation:(CLLocationCoordinate2D)location
{
    NSLog(@"DeviceLocation={%+.8f,%+.8f},",location.latitude,location.longitude);
    
    Rep *rep = [CoreDataHandler currentRep];
    
    NSNumber *repID = rep.repID;
    if(!repID) return nil;
    
    //NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
    [jsonDict setValue:repID?repID:[NSNull null]     forKey: @"repID"];
    [jsonDict setValue:[NSString stringWithFormat:@"%+.8f",location.latitude] forKey:@"latitude"];
    [jsonDict setValue:[NSString stringWithFormat:@"%+.8f",location.longitude] forKey:@"longitude"];
    
    NSString *jsonStr = [jsonDict JSONRepresentation];
    
    NSLog(@"json string=%@",jsonStr);
    NSString *connectionURL = makeURL(SEND_LOCATION_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    [jsonDict release];
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:jsonStr HTTPMethod:@"POST"];
    request.timeOutSeconds = 10.0;
    
     return request;
}


#pragma mark - LOGIN
+(ASIHTTPRequest *)createLoginConnectionWithEmail:(NSString *)email password:(NSString *)password
{
  NSDictionary *jsonDict = [NSDictionary dictionaryWithObjectsAndKeys:email,@"email",password,@"password", nil];
    
/*
NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
[jsonDict setValue: email  ? email  : [NSNull null]  forKey: @"email"];
[jsonDict setValue: password ? password : [NSNull null]  forKey: @"password"];
*/
    
  NSString *jsonStr = [jsonDict JSONRepresentation];
                     
    NSString *connectionURL = makeURL(LOGIN_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
                     
  return  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:jsonStr HTTPMethod:@"POST"];
}

+(ASIHTTPRequest *)createResetPasswordConnectionWithPassword:(NSString *)newPassword forUserID:(NSNumber *)userID
{
    if(!newPassword || !userID) return nil;
    
    NSString *connectionURL = makeURL(CHANGE_PASSWORD_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    NSDictionary *jsonDict = [NSDictionary dictionaryWithObjectsAndKeys:userID,@"userID",newPassword,@"newPassword", nil];
    
    NSString * json = [jsonDict JSONRepresentation];
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:json HTTPMethod:@"POST"];
    
    return request;
}

+(ASIHTTPRequest *)createForgotPasswordConnectionWithEmailID:(NSString *)emailID
{
    NSString *connectionURL = makeURL(FORGOT_PASSWORD_METHOD);
    NSLog(@"CONNECTION URL: %@",connectionURL);
    NSDictionary *jsonDict = [NSDictionary dictionaryWithObjectsAndKeys:emailID,@"email", nil];
    NSString * json = [jsonDict JSONRepresentation];
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:json HTTPMethod:@"POST"];
    
    return request;
}



#pragma mark - NEWS
+(ASIHTTPRequest *)createNewsConnectionWithRepID:(NSNumber *)repID wdSiteID:(NSNumber *)siteID wdID:(NSNumber *)wdID
{
    // app=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    id number=nil;
    
    /*if(app.isSiteIDChange==1)
     {
     if(!number) number = [NSNull null];
     app.isSiteIDChange=0;
     }
     else
     {
     number=[self lastRequestedForConnectionType:LAST_REQUESTED_NEWS];
     
     }*/
    
    number=[self lastRequestedForConnectionType:LAST_REQUESTED_NEWS];
    
    NSDictionary *jsonDict = [NSMutableDictionary dictionary];
    
    [jsonDict setValue: repID  ? repID  : [NSNull null]  forKey: @"repID"];
    [jsonDict setValue: siteID ? siteID : [NSNull null]  forKey: @"wdSiteID"];
    [jsonDict setValue: wdID   ? wdID   : [NSNull null]  forKey: @"parentWdID"];
    [jsonDict setValue:number   forKey:@"lastRequested"];
    
    
    NSString *jsonStr = [jsonDict JSONRepresentation];
    
    NSString *connectionURL = makeURL(NEWS_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    return  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:jsonStr HTTPMethod:@"POST"];
}


+(ASIHTTPRequest *)createNewsViewedConnectionWithJson:(NSString *)json
{
    if(!json) return nil;
    
    NSString *connectionURL = makeURL(NEWS_VIEWED_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:json HTTPMethod:@"POST"];
    
    return request;
}

#pragma mark - OFFERS
+(ASIHTTPRequest *)createOffersConnectionWithWithRepID:(NSNumber *)repID wdSiteID:(NSNumber *)siteID wdID:(NSNumber *)wdID
{
    NSNumber *number = [self lastRequestedForConnectionType:LAST_REQUESTED_OFFERS];
    
    NSDictionary *jsonDict = [NSMutableDictionary dictionary];
    
    [jsonDict setValue: repID  ? repID  : [NSNull null]  forKey: @"repID"];
    [jsonDict setValue: siteID ? siteID : [NSNull null]  forKey: @"wdSiteID"];
    [jsonDict setValue: wdID   ? wdID   : [NSNull null]  forKey: @"parentWdID"];
    
    [jsonDict setValue:number   forKey:@"lastRequested"];
    
    NSString *jsonStr = [jsonDict JSONRepresentation];
    
    NSLog(@" ooffer josn %@",jsonStr);
    NSString *connectionURL = makeURL(OFFERS_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    return  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:jsonStr HTTPMethod:@"POST"];
}

+(ASIHTTPRequest *)createOfferViewedConnectionWithJson:(NSString *)json
{
    if(!json) return nil;
    
    NSString *connectionURL = makeURL(OFFER_VIEWED_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:json HTTPMethod:@"POST"];
    
    return request;
}

#pragma mark - CUSTOMERS
+(ASIHTTPRequest *)createNewCustomerConnectionWithCustomerJson:(NSString *)aCustomerJson
{
    if(!aCustomerJson) return nil;
    
    NSLog(@" aCustomerJson create %@",aCustomerJson);
    NSString *connectionURL = makeURL(CREATE_CUSTOMER_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:aCustomerJson HTTPMethod:@"POST"];
    
    return request;
}

+(ASIHTTPRequest *)createCustomersConnectionWithRepID:(NSNumber *)repID wdSiteID:(NSNumber *)wdId territoryIDs:(NSString *)territoryIDs customerIDs:(NSString *)customerIDs
{
    if(!wdId) return nil;
    // wdId = [NSNumber numberWithInt:1];
    
    NSNumber *number=[self lastRequestedForConnectionType:LAST_REQUESTED_CUSTOMERS];
    // NSString *_wdID = [wdId description];
    
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    
    
    
    NSMutableDictionary *jsonDict = [NSMutableDictionary dictionary];
    [jsonDict setValue:repID forKey:@"repID"];
    [jsonDict setValue:wdId forKey:@"wdSiteID"];
    
    NSString *_territoryID = territoryIDs.length ?territoryIDs :@"";
    [jsonDict setValue:_territoryID  forKey:@"repTerritoryIDs"];
    [jsonDict setValue:customerIDs?customerIDs:@""  forKey:@"customerIDs"];
    [jsonDict setValue:deviceID forKey:@"deviceID"];
    [jsonDict setValue:number forKey:@"lastRequested"];
    
    NSString *jsonStr = [jsonDict JSONRepresentation];
    
    NSString *connectionURL = makeURL(CUSTOMERS_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    return [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:jsonStr HTTPMethod:@"POST"];
}

+(ASIHTTPRequest *)editCustomerConnectionWithCustomerJson:(NSString *)aCustomerJson
{
    if(!aCustomerJson) return nil;
    
    NSLog(@" aCustomerJson edit %@",aCustomerJson);
    NSString *connectionURL = makeURL(EDIT_CUSTOMER_METHOD); //EditCustomerTest
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:aCustomerJson HTTPMethod:@"POST"];
    
    return request;
}

+(ASIHTTPRequest *)saveCustomerNotes:(NSString *)notes forCustomerID:(NSNumber *)customerID
{
    Rep *_currentRep = [CoreDataHandler currentRep];
    
    NSString *connectionURL = makeURL(ADD_CUSTOMER_NOTES_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    NSMutableDictionary *uploadDict = [NSMutableDictionary dictionary];
    [uploadDict setValue:_currentRep.parentWdSite.wdSiteID forKey:@"wdSiteID"];
    [uploadDict setValue:_currentRep.repID forKey:@"repID"];
    [uploadDict setValue:customerID forKey:@"customerID"];
    [uploadDict setValue:notes?notes:@"" forKey:@"customerNotes"];
    
    NSString * json = [uploadDict JSONRepresentation];
    
    NSLog(@"Log JSON %@",json);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:json HTTPMethod:@"POST"];
    
    return request;
    
}



#pragma mark - CUSTOMER CONTACTS

+(ASIHTTPRequest *)createCustomerContactsConnectionForCustomer:(Customer *)aCustomer repID:(NSNumber *)repID
{
    if(!aCustomer) return nil;
    //aCustomerId = [NSNumber numberWithLong:24];
    
    NSLog(@"CustomerID:%@",aCustomer.customerID);
    
    id number = aCustomer.lastRequestedFromServer;
    
    //NSNumber *number=[self lastRequestedForConnectionType:LAST_REQUESTED_CONTACTS];
    
    if(!number) number = [NSNull null];
    
    NSLog(@"Last Requested:%@",[number description]);
    
    NSMutableDictionary *jsonDict = [NSMutableDictionary dictionary];
    [jsonDict setValue:aCustomer.customerID forKey:@"customerID"];
    [jsonDict setValue:repID forKey:@"repID"];
    [jsonDict setValue:aCustomer.wdSiteID?aCustomer.wdSiteID:[NSNull null] forKey:@"wdSiteID"];
    [jsonDict setValue:number forKey:@"lastRequested"];
    
    
    NSString *jsonStr = [jsonDict JSONRepresentation];
    
    NSLog(@"json:%@",jsonStr);
    
    NSString *connectionURL = makeURL(CUSTOMER_CONTACTS_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    return  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:jsonStr HTTPMethod:@"POST"];
}



#pragma mark - CUSTOMER IMAGES

/*
+(ASIHTTPRequest *)uploadImagesForCustomer
{
     if(![app.arrCustomerPhoto count]) return nil;
    
    NSURL *connectionURL = [NSURL URLWithString:IMAGE_UPLOAD_LINK];
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:connectionURL];
    
    NSLog(@" app.arrCustomerPhoto %@",app.arrCustomerPhoto);
    
    for(NSUInteger i=0;i<[app.arrCustomerPhoto count];i++)
    {
        
        //CustomerPhoto *photo=(CustomerPhoto *)[app.arrCustomerPhoto objectAtIndex:i];
        //NSString *imageFilePath =[DirectoryManager customerImagePathForName:photo.imageName];
        
        NSString *imageFilePath =[app.arrCustomerPhoto objectAtIndex:i];
        
        NSLog(@" imageFilePath %@",imageFilePath);
        
        NSString *imageFileName = [imageFilePath lastPathComponent];
        
        NSLog(@" imageFileName %@",imageFileName);
        
        NSString *imageExt = [imageFilePath pathExtension];
        
        NSLog(@" imageExt %@",imageExt);

        NSString *contentType = [NSString stringWithFormat:@"imageName/%@",imageExt];
        [request addFile:imageFilePath withFileName:imageFileName andContentType:contentType forKey:@"imageName"];
    }
    
    request.shouldContinueWhenAppEntersBackground=YES;
    request.shouldStreamPostDataFromDisk = YES;
    [request startAsynchronous];
    
    return request;
}
*/

+(ASIHTTPRequest *)saveNewCustomerImagesConnectionWithCustomerImageJson:(NSMutableArray *)arrCustomerImage customerID:(NSNumber *)customerID wdSiteID:(NSNumber *)siteID
{
    
    if(!arrCustomerImage) return nil;
    
    NSLog(@" aCustomerJson create %@",arrCustomerImage);
    NSString *connectionURL = makeURL(CUSTOMER_SAVE_IMAGE_DETAILS);
    NSMutableDictionary *uploadDict = [NSMutableDictionary dictionary];
    [uploadDict setValue:customerID?customerID:@"" forKey:@"customerID"];
    [uploadDict setValue:siteID?siteID:@"" forKey:@"wdSiteID"];
    [uploadDict setValue:arrCustomerImage?arrCustomerImage:[NSArray array] forKey:@"customerImageDetails"];
    
    NSMutableArray *arrJson=[NSMutableArray array];
    [arrJson addObject:uploadDict];
    
    NSString *jsonStr = [arrJson JSONRepresentation];
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:jsonStr HTTPMethod:@"POST"];
    
    return request;
}

+(ASIHTTPRequest *)saveNewCustomerImagesConnectionWithCustomerImageJson:(NSMutableArray *)arrCustomerImage forCustomer:(Customer *)customer
{
    
    if(!arrCustomerImage) return nil;
    
    Rep *rep = [CoreDataHandler currentRep];
    
    NSNumber *customerID = customer.customerID;
    NSNumber *siteID = customer.wdSiteID;
    NSNumber *repID = rep.repID;
    NSNumber *isCustomerSentToServer = customer.isCustomerSentToServer;
    
    NSLog(@" aCustomerJson create %@",arrCustomerImage);
    NSString *connectionURL = makeURL(CUSTOMER_SAVE_IMAGE_DETAILS);
    NSMutableDictionary *uploadDict = [NSMutableDictionary dictionary];
    
    [uploadDict setValue:customerID?customerID:[NSNull null] forKey:@"customerID"];
    
    [uploadDict setValue:siteID?siteID:[NSNull null] forKey:@"wdSiteID"];
    [uploadDict setValue:repID?repID:[NSNull null] forKey:@"repID"];
    
    [uploadDict setValue:arrCustomerImage?arrCustomerImage:[NSArray array] forKey:@"customerImageDetails"];
    
    [uploadDict setValue:isCustomerSentToServer?isCustomerSentToServer:[NSNumber numberWithBool:NO] forKey:@"isCustomerSentToServer"];
    
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    [uploadDict setValue:deviceID forKey:@"deviceID"];
    
    NSMutableArray *arrJson=[NSMutableArray array];
    [arrJson addObject:uploadDict];
    
    NSString *jsonStr = [arrJson JSONRepresentation];
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:jsonStr HTTPMethod:@"POST"];
    
    return request;
}

+(ASIHTTPRequest *)uploadImagesForCustomer:(NSMutableArray *)arrImages
{
    //  if(![app.arrCustomerPhoto count]) return nil;
    
    NSURL *connectionURL = [NSURL URLWithString:IMAGE_UPLOAD_LINK];
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:connectionURL];
    
    for(NSUInteger i=0;i<[arrImages count];i++)
    {
        //CustomerPhoto *photo=(CustomerPhoto *)[app.arrCustomerPhoto objectAtIndex:i];
        NSString *imageFileName = [arrImages objectAtIndex:i];
        NSString *imageFilePath =[DirectoryManager customerImagePathForName:imageFileName];
        if(![[NSFileManager defaultManager] fileExistsAtPath:imageFilePath])
            continue;
        
        //NSString *imageFilePath =
        
        // NSLog(@" imageFilePath %@",imageFilePath);
        
        NSLog(@" imageFileName %@",imageFileName);
        
        NSString *imageExt = [imageFileName pathExtension];
        
        NSLog(@" imageExt %@",imageExt);
        
        NSString *contentType = [NSString stringWithFormat:@"imageName/%@",imageExt];
        [request addFile:imageFilePath withFileName:imageFileName andContentType:contentType forKey:@"imageName"];
        [request addRequestHeader:@"type" value:@"2"];
        
    }
    
    request.shouldContinueWhenAppEntersBackground=YES;
    request.shouldStreamPostDataFromDisk = YES;
    [request startAsynchronous];
    
    return request;
    
}

+(ASIHTTPRequest *)uploadImagesForCustomerEdit:(NSString *)imageFilePath
{
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:imageFilePath]) return nil;
    
    //  if(![app.arrCustomerPhoto count]) return nil;
    
    
    NSURL *connectionURL = [NSURL URLWithString:IMAGE_UPLOAD_LINK];
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:connectionURL];
    
    //NSString *imageFilePath =
    
    // NSLog(@" imageFilePath %@",imageFilePath);
    
    NSString *imageFileName =[imageFilePath lastPathComponent];
    
    NSLog(@" imageFileName %@",imageFileName);
    
    NSString *imageExt = [imageFileName pathExtension];
    
    NSLog(@" imageExt %@",imageExt);
    
    NSString *contentType = [NSString stringWithFormat:@"imageName/%@",imageExt];
    [request addFile:imageFilePath withFileName:imageFileName andContentType:contentType forKey:@"imageName"];
    
    // NSString *contentType = [NSString stringWithFormat:@"image/%@",imageExt];
    // [request addFile:imageFilePath withFileName:imageFileName andContentType:contentType forKey:@"image"];
    
    [request addRequestHeader:@"type" value:@"2"];
    
    request.shouldContinueWhenAppEntersBackground=YES;
    request.shouldStreamPostDataFromDisk = YES;
    [request startAsynchronous];
    
    return request;
}


+(ASIHTTPRequest *)uploadPendingCustomerImages:(NSArray *)images
{
    NSURL *connectionURL = [NSURL URLWithString:IMAGE_UPLOAD_LINK];
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:connectionURL];
    
    for(CustomerPhoto *photo in images)
    {
        NSString *imageFileName = photo.imageName;
        NSString *imageFilePath = [DirectoryManager customerImagePathForName:imageFileName];
        
        if(![[NSFileManager defaultManager] fileExistsAtPath:imageFilePath]) continue;
        
        NSString *imageExt = [imageFileName pathExtension];
        
        NSLog(@" imageExt %@",imageExt);
        
        NSString *contentType = [NSString stringWithFormat:@"image/%@",imageExt];
        [request addFile:imageFilePath withFileName:imageFileName andContentType:contentType forKey:@"imageName"];
        
    }
    
    [request addRequestHeader:@"type" value:@"2"];
    request.shouldContinueWhenAppEntersBackground=YES;
    request.shouldStreamPostDataFromDisk = YES;
    [request startAsynchronous];
    
    return request;
}



+(ASIHTTPRequest *)getImagesForCustomer:(Customer *)aCustomer  repId:(NSNumber *)repId wdSiteID:(NSNumber *)wdSiteID
{
    
    NSString *connectionURL = makeURL(GET_CUSTOMER_IMAGE_DETAIL);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    NSNumber *lastRequested= aCustomer.photoLastRequestedFromServer;
    
    NSMutableDictionary *uploadDict = [NSMutableDictionary dictionary];
    [uploadDict setValue:aCustomer.customerID forKey:@"customerID"];
    [uploadDict setValue:repId forKey:@"repID"];
    [uploadDict setValue:wdSiteID forKey:@"wdSiteID"];
    
    [uploadDict setValue:lastRequested?lastRequested:[NSNull null] forKey:@"lastRequested"];
    
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    [uploadDict setValue:deviceID forKey:@"deviceID"];
    
    
    NSString * json = [uploadDict JSONRepresentation];
    
    NSLog(@" photo %@",json);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:json HTTPMethod:@"POST"];
    
    return request;
    
}


+(ASIHTTPRequest *)deleteImagesForCustomerID:(NSNumber *)customerID  imageName:(NSString *)imageName
{
    
    NSString *connectionURL = makeURL(DELETE_CUSTOMER_IMAGE);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    NSMutableDictionary *uploadDict = [NSMutableDictionary dictionary];
    [uploadDict setValue:customerID forKey:@"customerID"];
    [uploadDict setValue:imageName forKey:@"imageName"];
    
    NSString * json = [uploadDict JSONRepresentation];
    
    NSLog(@"delete photo %@",json);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:json HTTPMethod:@"POST"];
    
    return request;
    
}




+(ASIHTTPRequest *)deleteImagesForCustomerID:(NSNumber *)customerID siteID:(NSNumber *)siteID photo:(CustomerPhoto *)photo repID:(NSNumber *)repID
{
    
    NSString *connectionURL = makeURL(DELETE_CUSTOMER_IMAGE);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    NSString *imageName = photo.imageName;
    
    NSMutableDictionary *uploadDict = [NSMutableDictionary dictionary];
    [uploadDict setValue:customerID?customerID:@"" forKey:@"customerID"];
    [uploadDict setValue:siteID?siteID:@"" forKey:@"wdSiteID"];
    [uploadDict setValue:repID ? repID :[NSNull null] forKey:@"repID"];
    [uploadDict setValue:imageName?imageName:@"" forKey:@"imageName"];
    [uploadDict setValue:photo.customerImageID forKey:@"customerImageID"];
    
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    [uploadDict setValue:deviceID forKey:@"deviceID"];
    
    NSString * json = [uploadDict JSONRepresentation];
    
    NSLog(@"delete photo %@",json);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:json HTTPMethod:@"POST"];
    
    return request;
    
}





#pragma mark - TASKS

+(ASIHTTPRequest *)createTaskConnectionWithRepID:(NSNumber *)repID wdSiteID:(NSNumber *)siteID wdID:(NSNumber *)wdID
{
    if(!repID) return nil;
    
    NSNumber *number=[self lastRequestedForConnectionType:LAST_REQUESTED_TASKS];
    
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    
    NSMutableDictionary *jsonDict = [NSMutableDictionary dictionary];
    [jsonDict setValue:repID forKey:@"repID"];
    [jsonDict setValue:siteID forKey:@"wdSiteID"];
    [jsonDict setValue:wdID forKey:@"wdID"];
    [jsonDict setValue:deviceID forKey:@"deviceID"];
    [jsonDict setValue:number forKey:@"lastRequested"];
    
    NSString *jsonStr = [jsonDict JSONRepresentation];
    
    NSString *connectionURL = makeURL(TASKS_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    return  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:jsonStr HTTPMethod:@"POST"];
}

+(ASIHTTPRequest *)createNewTaskConnectionWithTaskJson:(NSString *)aTaskJson
{
    if(!aTaskJson) return nil;
    
    NSString *connectionURL = makeURL(CREATE_TASK_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:aTaskJson HTTPMethod:@"POST"];
    
    return request;
}


#pragma mark - SCHEDULE VISITS
+(ASIHTTPRequest *)createUpcommingVisitsConnectionWithRepId:(NSNumber *)repId wdSiteID:(NSNumber *)wdSite
{
    if(!repId) return nil;
    
    // NSString *_repId=@"4";
    
    NSString *_repId = [repId description];
    
    NSNumber *number=[self lastRequestedForConnectionType:LAST_REQUESTED_VISITS];
    
     NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    
    NSMutableDictionary *jsonDict = [NSMutableDictionary dictionary];
    [jsonDict setValue:_repId forKey:@"repID"];
    [jsonDict setValue:wdSite forKey:@"wdSiteID"];
    [jsonDict setValue:deviceID forKey:@"deviceID"];
    [jsonDict setValue:number forKey:@"lastRequested"];
    
    NSString *jsonStr = [jsonDict JSONRepresentation];
    
    NSString *connectionURL = makeURL(UPCOMMING_VISITS_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    return  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:jsonStr HTTPMethod:@"POST"];
}

+(ASIHTTPRequest *)createUpcommingVisitsConnection
{
    Rep *_rep = [CoreDataHandler currentRep];
    NSNumber *repID            = _rep.repID;
    NSNumber *wdSiteID         = _rep.parentWdSite.wdSiteID;

    NSString *_repId = [repID description];
    
    NSNumber *number=[self lastRequestedForConnectionType:LAST_REQUESTED_VISITS];
    
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    
    NSMutableDictionary *jsonDict = [NSMutableDictionary dictionary];
    [jsonDict setValue:_repId forKey:@"repID"];
    [jsonDict setValue:wdSiteID forKey:@"wdSiteID"];
    [jsonDict setValue:deviceID forKey:@"deviceID"];
    [jsonDict setValue:number forKey:@"lastRequested"];
    
    NSString *jsonStr = [jsonDict JSONRepresentation];
    
    NSString *connectionURL = makeURL(UPCOMMING_VISITS_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    return  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:jsonStr HTTPMethod:@"POST"];
}

+(ASIHTTPRequest *)createNewscheduledVisitConnectionWithJson:(NSString *)json
{
    if(!json) return nil;
    
    NSString *connectionURL = makeURL(EDIT_UPCOMMING_VISITS_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:json HTTPMethod:@"POST"];
    
    return request;
}


#pragma mark - REMINDER

+(ASIHTTPRequest *)fetchRemindersWithRepID:(NSNumber *)repId wdSiteID:(NSNumber *)wdSiteID
{
    if(!repId) return nil;
    
    // NSString *_repId=@"4";
    
    NSNumber *number=[self lastRequestedForConnectionType:LAST_REQUESTED_REMINDERS];
    
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    
    
    NSMutableDictionary *jsonDict = [NSMutableDictionary dictionary];
    [jsonDict setValue:repId     forKey: @"repID"];
    [jsonDict setValue:wdSiteID  forKey: @"wdSiteID"];
    [jsonDict setValue:deviceID forKey:@"deviceID"];
    [jsonDict setValue:number    forKey: @"lastRequested"];
    
    NSString *jsonStr = [jsonDict JSONRepresentation];
    
    NSString *connectionURL = makeURL(FETCH_REMINDERS_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    return  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:jsonStr HTTPMethod:@"POST"];
}

+(ASIHTTPRequest *)createRemindersWithJson:(NSString *)aJson
{
    if(!aJson) return nil;
    
    NSString *connectionURL = makeURL(CREATE_REMINDER_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:aJson HTTPMethod:@"POST"];
    
    return request;
}

#pragma mark - VISITS

+(ASIHTTPRequest *)uploadVisitDetailsForVisit:(Visit *)visit visitReports:(NSArray *)reports
{
    if(!visit) return nil;
    
    NSLog(@"%@",reports);
    NSSet *visitPhotos = visit.photos;
    
    NSMutableArray *images=[NSMutableArray array];
    
    for(VisitPhoto *photo in visitPhotos)
    {
        [images addObject:[photo uploadDescription]];
    }
    
    
    NSLog(@" images %@",images);
    
    NSMutableDictionary *uploadDict = [NSMutableDictionary dictionary];
    
    [uploadDict setValue:reports?reports:[NSArray array] forKey:@"visitNotesDetails"];
    
    [uploadDict setValue:images?images:[NSArray array] forKey:@"visitImageDetails"];
    
    
    Rep *_rep=[CoreDataHandler currentRep];
    
    NSNumber *customerID = visit.parentCustomer? visit.parentCustomer.customerID : nil;
    
    [uploadDict setValue:customerID?customerID:[NSNull null] forKey:@"customerID"];
    //[uploadDict setValue:[NSNull null] forKey:@"customerID"];
    
    [uploadDict setValue:visit.parentCustomer?visit.parentCustomer.isCustomerSentToServer:[NSNumber numberWithBool:NO] forKey:@"isCustomerSentToServer"];
    
    [uploadDict setValue:_rep.repID?_rep.repID:[NSNull null] forKey:@"repID"];
    //[uploadDict setValue:[NSNull null] forKey:@"repID"];
    [uploadDict setValue:visit.scheduledVisit? visit.scheduledVisit.scheduleVisitID : [NSNull null] forKey:@"scheduleVisitID"];
    //[uploadDict setValue:[NSNull null] forKey:@"scheduleVisitID"];
    [uploadDict setValue:visit.scheduledVisit? visit.scheduledVisit.isScheduleVisitSentToServer : [NSNumber numberWithBool:NO] forKey:@"isScheduleVisitSentToServer"];
    
    NSTimeInterval _startDateInterval = [visit.startDate GMTTimeIntervalSince1970];
    [uploadDict setValue:[NSString stringWithFormat:@"%lf",_startDateInterval] forKey:@"actualStart"];
    
    NSTimeInterval _completeDateInterval = [visit.reportCompletedDate GMTTimeIntervalSince1970];
    [uploadDict setValue:[NSString stringWithFormat:@"%lf",_completeDateInterval] forKey:@"reportCreatedDate"];
    
    NSTimeInterval _endDateInterval = [visit.visitCompletedDate GMTTimeIntervalSince1970];
    [uploadDict setValue:[NSString stringWithFormat:@"%lf",_endDateInterval] forKey:@"actualEnd"];
    
    //================================ PUBLISH DATE =============================================
    NSTimeInterval _publishDateInterval = [[NSDate date] GMTTimeIntervalSince1970];
    [uploadDict setValue:[NSString stringWithFormat:@"%lf",_publishDateInterval] forKey:@"reportSubmitDate"];
    //===========================================================================================================
    
    
    //============================ SEND TIMEZONE INFO ====================================
    
    NSTimeZone *_timeZone = [NSTimeZone localTimeZone];
    NSString *timeZoneName = [_timeZone name];
    [uploadDict setValue:timeZoneName?timeZoneName:@"" forKey:@"timeZone"];
    
    
    NSTimeInterval _secondsInSite = [visit.visitCompletedDate timeIntervalSinceDate:visit.startDate];
    _secondsInSite-=[visit.inactiveSeconds doubleValue];
    _secondsInSite =fabs(_secondsInSite);
    
    [uploadDict setValue:[NSString stringWithFormat:@"%lf",_secondsInSite] forKey:@"hoursOnSite"];
    
    NSInteger _natureOfVisitIndex =[visit.natureOfVisitID integerValue];
    [uploadDict setValue:[NSNumber numberWithInteger:_natureOfVisitIndex+1] forKey:@"natureOfVisitID"];
    //[uploadDict setValue:[NSNull null] forKey:@"natureOfVisitID"];
    
    
    NSString *strOfferId= [self interestedOfferIdsForVisit:visit];
    NSLog(@" strOfferId %@",strOfferId);
    
    [uploadDict setValue:strOfferId?strOfferId:@"" forKey:@"visitInterestedOfferIDs"];
    
    [uploadDict setValue:visit.sessionStartLat  ? visit.sessionStartLat  : @"0.0"  forKey:@"sessionStartLat"];
    [uploadDict setValue:visit.sessionStartLong ? visit.sessionStartLong : @"0.0"  forKey:@"sessionStartLong"];
    
    [uploadDict setValue:visit.sessionEndLat ? visit.sessionEndLat : @"0.0"  forKey:@"sessionEndLat"];
    [uploadDict setValue:visit.sessionEndLong? visit.sessionEndLong: @"0.0" forKey:@"sessionEndLong"];
    
    CLLocation *currentLocation = [LocationManager sharedManager].currentLocation;
    NSString *locationLat   = [NSString stringWithFormat:@"%lf",currentLocation.coordinate.latitude];
    NSString *locationLong  = [NSString stringWithFormat:@"%lf",currentLocation.coordinate.longitude];
    
    visit.reportPublishLat  = locationLat;
    visit.reportPublishLong = locationLong;
    
    [uploadDict setValue:locationLat forKey:@"publishReportLat"];
    [uploadDict setValue:locationLong forKey:@"publishReportLong"];
    
    
    NSString *jsonStr=[uploadDict JSONRepresentation];
    NSLog(@"CONNECTION JSON: %@",jsonStr);
    
    NSString *connectionURL = makeURL(VISIT_UPLOAD_METHOD);
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:jsonStr HTTPMethod:@"POST"];
    
    return request;
}

+(ASIHTTPRequest *)uploadImagesForVisit:(Visit *)visit
{
    NSSet *visitPhotos = visit.photos;
    
   // if(![visitPhotos count]) return nil;
    
    NSURL *connectionURL = [NSURL URLWithString:IMAGE_UPLOAD_LINK];
     NSLog(@"CONNECTION URL: %@",connectionURL);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:connectionURL];

    for(VisitPhoto *photo in visitPhotos)
    {
        NSString *imageFilePath = photo.imagePath;
        
        NSLog(@" imageFilePath %@",imageFilePath);
        
        NSString *imageFileName = [imageFilePath lastPathComponent];
        
         NSLog(@" imageFileName %@",imageFileName);
        
        NSString *imageExt = [imageFilePath pathExtension];
        
        NSLog(@" imageExt %@",imageExt);

        
        NSString *contentType = [NSString stringWithFormat:@"image/%@",imageExt];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:imageFilePath])
        {
        [request addFile:imageFilePath withFileName:imageFileName andContentType:contentType forKey:@"image"];
        [request addRequestHeader:@"type" value:@"1"];
        }
    }
    
    request.shouldContinueWhenAppEntersBackground=YES;
    request.shouldStreamPostDataFromDisk = YES;
    [request startAsynchronous];

    return request;
}

#pragma mark - FEEDBACK

+(ASIHTTPRequest *)getFeedbackCategory:(NSNumber *)repId wdSiteID:(NSNumber *)wdId
{
    NSString *connectionURL = makeURL(GET_FEEDBACK_CATEGORY);
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    NSMutableDictionary *uploadDict = [NSMutableDictionary dictionary];
    [uploadDict setValue:repId forKey:@"repID"];
    [uploadDict setValue:wdId forKey:@"wdSiteID"];
    
    NSString * json = [uploadDict JSONRepresentation];
    
    NSLog(@"feed %@",json);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:json HTTPMethod:@"POST"];
    
    return request;
}

+(ASIHTTPRequest *)submitFeedbackConnectionWithRepID:(NSNumber *)repId wdSiteID:(NSNumber *)wdId deviceInfo:(NSString *)dInfo  categoryID:
(NSString *)cID subjet:(NSString *)sText description:(NSString *)description categoryaName:(NSString *)categoryaName
{
    
    NSString *connectionURL = makeURL(SUBMIT_FEEDBACK_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    Rep *_rep = [CoreDataHandler currentRep];
    NSNumber *repID            = _rep.repID;
    NSNumber *wdSiteID         = _rep.parentWdSite.wdSiteID;
    NSString *emailAddress     = _rep.email;
    NSString *firstName        = _rep.firstName;
    NSString *lastName         = _rep.lastName;
    
    NSString *deviceType       = [[UIDevice currentDevice] hardwareDescription];
    //[[UIDevice currentDevice] model];
    NSString *deviceIdentifier = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    NSString *iOSVersion       = [[UIDevice currentDevice]systemVersion];
    NSString *appVersion       = APP_VERSION;
    
    
    NSMutableDictionary *uploadDict = [NSMutableDictionary dictionary];
    [uploadDict setValue:repID        forKey:@"repID"];
    [uploadDict setValue:wdSiteID     forKey:@"wdSiteID"];
    [uploadDict setValue:emailAddress forKey:@"email"];
    [uploadDict setValue:firstName    forKey:@"firstName"];
    [uploadDict setValue:lastName     forKey:@"lastName"];
    
    [uploadDict setValue:deviceType ? deviceType : @""             forKey:@"deviceType"];
    [uploadDict setValue:deviceIdentifier ? deviceIdentifier :@""  forKey:@"deviceIdentifier"];
    [uploadDict setValue:iOSVersion ? iOSVersion : @""             forKey:@"iOSVersion"];
    [uploadDict setValue:appVersion ? appVersion : @""             forKey:@"appVersion"];
    
    [uploadDict setValue:dInfo?dInfo:@"" forKey:@"deviceDescription"];
    [uploadDict setValue:cID forKey:@"feedbackCategoryID"];
    //[uploadDict setValue:sText.length?sText:@"" forKey:@"subject"];
    
    [uploadDict setValue:description.length?description:@"" forKey:@"description"];
    [uploadDict setValue:categoryaName forKey:@"categoryName"];
    
    
    NSString * json = [uploadDict JSONRepresentation];
    
    NSLog(@"submit feed %@",json);
    
    ASIHTTPRequest *request =  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:json HTTPMethod:@"POST"];
    
    return request;
    
}

#pragma mark TEST

+(ASIHTTPRequest *)createServerMaintenanceTestConnection
{
    NSString *connectionURL = @"http://192.168.1.143/ServerMaintanence/ServerMaintanenceHandler.ashx";
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    return  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:nil HTTPMethod:@"GET"];
}



+(NSString *)interestedOfferIdsForVisit:(Visit *)aVisit
{
    NSString *offerIdsStr = aVisit.visitedOfferIDs;
    
    if(!offerIdsStr.length) return nil;
    
    NSMutableArray *arr2=[NSMutableArray array];
    NSArray *arr = [offerIdsStr componentsSeparatedByString:@","];
    for(NSString *str in arr)
        {
            if(str && [str integerValue])
                {
                    [arr2 addObject:str];
                    }
            }
    
    return [arr2 componentsJoinedByString:@","];
    
}


#pragma mark - RESOURCE

+(ASIHTTPRequest *)fetchResources
{
    NSNumber *number=[self lastRequestedForConnectionType:LAST_REQUESTED_RESOURCES];
    
    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    Rep *rep = [CoreDataHandler currentRep];
    
    NSMutableDictionary *jsonDict = [NSMutableDictionary dictionary];
    [jsonDict setValue:rep.repID                  forKey: @"repID"];
    [jsonDict setValue:rep.parentWdSite.wdSiteID  forKey: @"wdSiteID"];
    [jsonDict setValue: rep.parentWdSite.parentWdID   ? rep.parentWdSite.parentWdID   : [NSNull null]  forKey: @"parentWdID"];
    [jsonDict setValue:deviceID                   forKey:@"deviceID"];
    [jsonDict setValue:number                     forKey: @"lastRequested"];
    
    NSString *jsonStr = [jsonDict JSONRepresentation];
    
    NSString *connectionURL = makeURL(FETCH_RESOURCES_METHOD);
    
    NSLog(@"CONNECTION URL: %@",connectionURL);
    
    return  [ConnectionManager createWCFConnectionWithUrl:connectionURL jsonParam:jsonStr HTTPMethod:@"POST"];
}

+(ASIHTTPRequest*)requestToDownloadResourceFileForName:(NSString *)fileName toPath:(NSString *)destinationPath
{
    //NSString *connectionURL = @"http://i.i.cbsi.com/cnwk.1d/i/tim2/2013/06/18/ipad-mini-game-small-2.jpg";// [self makeURL:videoName];
    
    NSString *connectionURL = [NSString stringWithFormat:@"%@%@",RESOURCE_FILE_DOWNLOAD_LINK,fileName];
    NSLog(@"link = %@", connectionURL);
    
    NSString *tempDownloadPath = [DirectoryManager tempResourceFilePathWithName:fileName];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:connectionURL]];
    [request setTemporaryFileDownloadPath:tempDownloadPath];
    [request setDownloadDestinationPath:destinationPath];
    request.showAccurateProgress = YES;
    [request setAllowResumeForFileDownloads:YES];
    //[request setShouldContinueWhenAppEntersBackground:YES];

    [request startAsynchronous];
    
    return request;
}




#pragma mark - FETCH AND PROCESS CUSTOMERS

+(ASIHTTPRequest *)fetchAndProcessCustomersOnCompletion:(void(^)(BOOL success, NSMutableArray *insertedResult,NSMutableArray *updatedResult, NSError *error))completionBlock deletionBlock:(void(^)(NSMutableArray *deletedResult))deletionBlock
{
        Rep *currentRep=[CoreDataHandler currentRep];
    
        NSNumber *wdSiteID= currentRep.parentWdSite.wdSiteID;
    
        NSArray *activeTerritories = [Territory fetchAll:nil];
        NSArray *territories = [activeTerritories valueForKey:@"territoryID"];
        NSString *territoryIDs = [territories componentsJoinedByString:@","];
        NSLog(@"Territories=%@",territoryIDs);
        
        NSMutableArray *allCustomers = [Customer fetchWithPredicate:nil onAttributes:[NSArray arrayWithObject:@"customerID"] sortDescriptors:nil limit:0 error:nil];
        NSArray *  _allCustomersIDs = [allCustomers count]? [allCustomers valueForKey:@"customerID"] : nil;
        
        NSString *customerIDString = [_allCustomersIDs count] ? [_allCustomersIDs componentsJoinedByString:@","] : @"";
    
        NSLog(@"CustomerIDs=%@",customerIDString);
    
        __block ASIHTTPRequest *request = [ConnectionManager createCustomersConnectionWithRepID:currentRep.repID wdSiteID:wdSiteID territoryIDs:territoryIDs customerIDs:customerIDString];
    
        [request setCompletionBlock:^{
            
            NSLog(@"Response:%@",[request responseString]);
            
            NSDictionary *jsonDict = [request responseJSON];
            
            jsonDict = NULL_NIL(jsonDict);
            
            BOOL _isValidResponse = [ConnectionManager isValidResponse:jsonDict];
            if(jsonDict && _isValidResponse)
            {
                app.isCustomerLoaded=YES;
                
                NSArray *_customers = [jsonDict valueForKey:JSON_DETAILS_KEY];
                NSArray *updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
                NSArray *deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
                
                NSNumber *_status   = [jsonDict valueForKey:JSON_STATUS_KEY];
                
                int status = [_status intValue];
                
                //status = 2;
                NSLog(@"status=%d",status);
                
                if(status==1)
                {
                    NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
                    [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_CUSTOMERS];
                    
                    //======================== FETCH ACCORDING TO TERRITORY ========================
                    NSArray *currentTerritories = [jsonDict valueForKey:@"repTerritoryDetails"];
                    currentTerritories = NULL_NIL(currentTerritories);
                    
                    NSMutableArray *_currentTerritories = [NSMutableArray array];
                    for(NSDictionary *territoryDict in currentTerritories)
                    {
                        Territory *aTerritory = (Territory *) [Territory createObjectFromDictionary:territoryDict inContext:DEFAULT_CONTEXT];
                        
                        [_currentTerritories addObject:aTerritory];
                        
                    }
                    
                    
                    NSMutableArray *territoryToDelete = [NSMutableArray array];
                    NSArray *delete= [_currentTerritories count] ?  [activeTerritories filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (self.territoryID IN %@)",[_currentTerritories valueForKey:@"territoryID"]]] : nil;
                    
                    if([delete count])
                        [territoryToDelete addObjectsFromArray:[delete valueForKey:@"territoryID"]];
                    
                    //============== REMOVE PREVIOUS TERROTORIES AND ADD CURRENT TERRITORIES ===========
                    
                    if([_currentTerritories count])
                    {
                        for(Territory *territory in activeTerritories)
                        {
                            [currentRep removeTerritoriesObject:territory];
                            
                            [SSCoreDataManager deleteObject:territory];
                        }
                        
                        NSSet *territorySet = [NSSet setWithArray:_currentTerritories];
                        if(territories)
                            [currentRep addTerritories:territorySet];
                    }
                    
                    
                    //============ DELETE CUSTOMER NOT FALLS INTO CURRENT TERRITORY =============== //
                    
                    if([territoryToDelete count])
                    {
                        NSPredicate *deleteCustomerPredicate = [NSPredicate predicateWithFormat:@"self.territoryID IN %@",territoryToDelete];
                        
                        NSMutableArray *customersToDelete = [Customer fetchWithPredicate:deleteCustomerPredicate onAttributes:[NSArray arrayWithObject:@"customerID"] sortDescriptors:nil limit:0 error:nil];
                        
                        // NSMutableArray *customersToDelete = [Customer fetchWithPredicate:deleteCustomerPredicate error:nil];
                        
                        NSArray *  _deletedIDs = [customersToDelete valueForKey:@"customerID"];
                        
                        NSMutableArray *idsToDelete = deletedIDs ? [NSMutableArray arrayWithArray:deletedIDs] : [NSMutableArray array];
                        [idsToDelete addObjectsFromArray:_deletedIDs];
                        
                        deletedIDs =[NSArray arrayWithArray:idsToDelete];
                    
                        //app.isCustomerFetching =NO;
                    }
                }
                else if(status==2)
                {
                    
                    [ConnectionManager setLastRequested:nil forConnectionType:LAST_REQUESTED_CUSTOMERS];
                    
                    _customers = nil;
                    updatedIDs = nil;
                    
                    NSMutableArray *customersToDelete = [Customer fetchWithPredicate:nil onAttributes:[NSArray arrayWithObject:@"customerID"] sortDescriptors:nil limit:0 error:nil];
                    
                    deletedIDs = [customersToDelete valueForKey:@"customerID"];
                    
                   // app.isCustomerFetching =NO;
                }
                
                
                if(status!=0)
                {
                    if([_customers count] || [deletedIDs count])
                    {
                        [Customer populateAsyncPerformingDelete:deletedIDs?deletedIDs:[NSArray array] update:updatedIDs?updatedIDs:[NSArray array] insertOnArray:_customers?_customers:[NSArray array] onAttributes:[NSArray arrayWithObjects:@"customerID",@"oldCustomerID", nil] completion:^( NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
                         {
                            // app.isCustomerFetching =NO;
                             
                             if(completionBlock) completionBlock(YES,insertedResult,updatedResult,error);
                         }
                         
                          deletionBlock:^(NSMutableArray *deletedResult)
                         {
                             if(deletionBlock) deletionBlock(deletedResult);
                             
                         }];
                    }
                    else
                    {
                       // app.isCustomerFetching =NO;
                        
                        if(completionBlock) completionBlock(YES,nil,nil,nil);
                    }
                }
                else
                {
                    //app.isCustomerFetching = NO;
                    if(completionBlock) completionBlock(NO,nil,nil,nil);
                }
            }
            else
            {
               // app.isCustomerFetching =NO;
                
                if(completionBlock) completionBlock(NO,nil,nil,nil);
            }
        }];
        
        [request setFailedBlock:^{
            
          //  app.isCustomerFetching =NO;
            
          if(completionBlock) completionBlock(NO,nil,nil,request.error);
            
        }];
    
    
    return request;
}

#pragma mark - UNDER TESTING
#pragma mark - FETCH AND PROCESS TASKS
+(ASIHTTPRequest *)fetchAndProcessTasksOnCompletion:(void(^)(BOOL success, NSError *error))completionBlock
{
    Rep *_rep = [CoreDataHandler currentRep];
    
  __block  ASIHTTPRequest *request = [ConnectionManager createTaskConnectionWithRepID:_rep.repID wdSiteID:_rep.parentWdSite.wdSiteID wdID:_rep.parentWdSite.parentWdID];
    
    [request setCompletionBlock:^{
        
        NSDictionary *jsonDict = [request responseJSON];
        
        NSLog(@"json=%@",[request responseString]);
        
        if(jsonDict && [jsonDict isKindOfClass:[NSDictionary class]])
        {
            NSArray *_tasks      = [jsonDict valueForKey:JSON_DETAILS_KEY];
            NSArray *_updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
            NSArray *_deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
            
            _tasks      = NULL_NIL(_tasks);
            _updatedIDs = NULL_NIL(_updatedIDs);
            _deletedIDs = NULL_NIL(_deletedIDs);
            
            
            NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
            [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_TASKS];
            
            
            [Task populateAsyncPerformingDelete:_deletedIDs?_deletedIDs:[NSArray array] update:_updatedIDs?_updatedIDs:[NSArray array] insertOnArray:_tasks?_tasks:[NSArray array] onAttribute:@"taskID" completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
             {
                 
                 dispatch_async([app eventQueue], ^{
                     
                     NSManagedObjectContext *context=[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType] ;
                     [context setPersistentStoreCoordinator:DEFAULT_CONTEXT.persistentStoreCoordinator];
                     [context setUndoManager:nil];
                     
                     for(Task *task in insertedResult)
                     {
                         Task *_task =(Task *)[task inContext:context];
                         
                         if(_task.customerID)
                         {
                             
                             NSMutableArray *arr = [Customer fetchOnContext:context predicate:[NSPredicate predicateWithFormat:@"self.customerID = %@",_task.customerID] onAttributes:nil error:nil];
                             
                             Customer *_customer=[arr count]?[arr objectAtIndex:0] : nil;
                             if(_customer) [_customer addTasksObject:_task];
                             else
                             {
                                 //[context deleteObject:_task];
                             }
                             
                         }
                     }
                     
                     for(Task *task in updatedResult)
                     {
                         Task *_task =(Task *)[task inContext:context];
                         
                         if(_task.parentCustomer)
                         {
                             if(![_task.parentCustomer.customerID isEqualToNumber:_task.customerID])
                             {
                                 [_task.parentCustomer removeTasksObject:_task];
                             }
                         }
                         NSLog(@"Customer ID:%@",_task.customerID);
                         
                         if(!_task.parentCustomer || ![_task.parentCustomer.customerID isEqualToNumber:_task.customerID])
                         {
                             NSMutableArray *arr = [Customer fetchOnContext:context predicate:[NSPredicate predicateWithFormat:@"self.customerID = %@",_task.customerID] onAttributes:nil error:nil];
                             
                             Customer *_customer = [arr count]? [arr objectAtIndex:0] : nil;
                             
                             if(_customer)
                             {
                                 //_visit.customer= _customer;
                                 [_customer addTasksObject:_task];
                             }
                             else
                             {
                                 //  [context deleteObject:_visit];
                             }
                         }
                     }
                     
                     ///////////////////////////////////////////////////////////////////
                     [[SSCoreDataManager sharedManager] saveContext:context error:nil];
                     //////////////////////////////////////////////////////////////////
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         
                         if(completionBlock) completionBlock(YES,nil);
                     });
                     
                     
                     
                     for(Task *task  in insertedResult)
                     {
                         Task *_task =(Task *)[task inContext:context];
                         
                         if([task respondsToSelector:NSSelectorFromString(@"eventIdentifier")])
                         {
                             EKEvent * event =nil;
                             NSString *eventIdentifier = _task.eventIdentifier;
                             
                             NSString *customerName =[NSString stringWithFormat:@"Task: %@", _task.subject.length?_task.subject : @"task"];
                             
                             if(eventIdentifier.length)
                                 event = [[CustomEventStore sharedInstance].eventStore eventWithIdentifier:eventIdentifier];
                             
                             if(!event)
                             {
                                 event = [CustomEventStore addEventWithTitle:customerName startDate:_task.dueDate endDate:[[_task.dueDate dateByAddingTimeInterval:60.0] currentDateHourMinute] location:nil notes:nil];
                                 
                                 NSLog(@"Task dueDate=%@ eventDate=%@",_task.dueDate,event.startDate);
                                 [event retain];
                             }
                             else
                             {
                                 event = [CustomEventStore editEventForIdentifier:_task.eventIdentifier withTitle:customerName startDate:_task.dueDate endDate:[[_task.dueDate dateByAddingTimeInterval:60.0] currentDateHourMinute] location:nil notes:nil];
                                 
                                 [event retain];
                             }
                             
                             if(event && event.eventIdentifier.length)
                                 _task.eventIdentifier = event.eventIdentifier;
                             
                             [event release];
                         }
                         
                         
                     }
                     
                     if([_updatedIDs count])
                     {
                         
                         for(Task *task in updatedResult)
                         {
                             Task *_task = (Task *)[task inContext:context];
                             
                             if([_task respondsToSelector:NSSelectorFromString(@"eventIdentifier")])
                             {
                                 NSString *customerName =[NSString stringWithFormat:@"Task: %@", _task.subject.length?_task.subject : @"task"];
                                 
                                 EKEvent *_event =  [CustomEventStore editEventForIdentifier:_task.eventIdentifier withTitle:customerName startDate:_task.dueDate endDate:[[_task.dueDate dateByAddingTimeInterval:60.0] currentDateHourMinute] location:nil notes:nil];
                                 
                                 if(_event && _event.eventIdentifier.length)
                                     _task.eventIdentifier = _event.eventIdentifier;
                             }
                         }
                         
                     }
                     
                     app.eventFetchCount++;
                     [[SSCoreDataManager sharedManager] saveContext:context error:nil];
                     
                     [context reset];
                     [context release];
                 });
             }];
        }

        
    }];
    
    
    [request setFailedBlock:^{
        
        
         if(completionBlock) completionBlock(NO,request.error);
    }];


    return request;

}

#pragma mark - FETCH AND PROCESS ScheduledVisits
+(ASIHTTPRequest *)fetchAndProcessScheduledVisitsOnCompletion:(void(^)(BOOL success, NSError *error))completionBlock
{

    Rep *_rep = [CoreDataHandler currentRep];
    
   __block ASIHTTPRequest *request =  [ConnectionManager createUpcommingVisitsConnectionWithRepId:_rep.repID wdSiteID:_rep.parentWdSite.wdSiteID];
    
   [request setCompletionBlock:^{
       
       NSDictionary *jsonDict = [request responseJSON];
       NSLog(@"json=%@",[request responseString]);
       
       if(jsonDict && [jsonDict isKindOfClass:[NSDictionary class]])
       {
           BOOL _isSiteIDChanged = [[jsonDict valueForKey:JSON_WDSITE_CHANGE_KEY] boolValue];
           if(_isSiteIDChanged)
           {
               [app appDidChangeSiteID];
           }
           else
           {
               
               NSArray *_visits     = [jsonDict valueForKey:JSON_DETAILS_KEY];
               NSArray *_updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
               NSArray *_deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
               
               _visits = NULL_NIL(_visits);
               _updatedIDs = NULL_NIL(_updatedIDs);
               _deletedIDs = NULL_NIL(_deletedIDs);
               
               NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
               [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_VISITS];
               
               
               [ScheduledVisit populateAsyncPerformingDelete:_deletedIDs?_deletedIDs:[NSArray array] update:_updatedIDs?_updatedIDs:[NSArray array] insertOnArray:_visits?_visits:[NSArray array] onAttribute:@"scheduleVisitID" completion:^(NSMutableArray *deletedResult, NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
                {
                    
                    dispatch_async([app eventQueue], ^{
                        
                        NSManagedObjectContext *context=[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType] ;
                        [context setPersistentStoreCoordinator:DEFAULT_CONTEXT.persistentStoreCoordinator];
                        [context setUndoManager:nil];
                        
                        for(ScheduledVisit *visit in insertedResult)
                        {
                            ScheduledVisit *_visit =(ScheduledVisit *)[visit inContext:context];
                            
                            NSLog(@"Customer ID:%@",_visit.customerID);
                            
                            NSMutableArray *arr = [Customer fetchOnContext:context predicate:[NSPredicate predicateWithFormat:@"self.customerID = %@",_visit.customerID] onAttributes:nil error:nil];
                            
                            Customer *_customer = [arr count]? [arr objectAtIndex:0] : nil;
                            
                            if(_customer)
                            {
                                //_visit.customer= _customer;
                                [_customer addScheduledVisitsObject:_visit];
                            }
                            else
                            {
                                //  [context deleteObject:_visit];
                            }
                            
                        }
                        
                        for(ScheduledVisit *visit in updatedResult)
                        {
                            ScheduledVisit *_visit =(ScheduledVisit *)[visit inContext:context];
                            
                            if(_visit.customer)
                            {
                                if(![_visit.customer.customerID isEqualToNumber:_visit.customerID])
                                {
                                    [_visit.customer removeScheduledVisitsObject:_visit];
                                }
                            }
                            NSLog(@"Customer ID:%@",_visit.customerID);
                            
                            if(!_visit.customer || ![_visit.customer.customerID isEqualToNumber:_visit.customerID])
                            {
                                NSMutableArray *arr = [Customer fetchOnContext:context predicate:[NSPredicate predicateWithFormat:@"self.customerID = %@",_visit.customerID] onAttributes:nil error:nil];
                                
                                Customer *_customer = [arr count]? [arr objectAtIndex:0] : nil;
                                
                                if(_customer)
                                {
                                    //_visit.customer= _customer;
                                    [_customer addScheduledVisitsObject:_visit];
                                }
                                else
                                {
                                    //  [context deleteObject:_visit];
                                }
                            }
                        }
                        
                        
                        ///////////////////////////////////////////////////////////////////
                        [[SSCoreDataManager sharedManager] saveContext:context error:nil];
                        //////////////////////////////////////////////////////////////////
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            if(completionBlock) completionBlock(YES,nil);
                            
                        });
                        
                        for(ScheduledVisit *visit in insertedResult)
                        {
                            if([visit respondsToSelector:NSSelectorFromString(@"eventIdentifier")])
                            {
                                ScheduledVisit *_visit =(ScheduledVisit *)[visit inContext:context];
                                
                                EKEvent * event =nil;
                                NSString *eventIdentifier = _visit.eventIdentifier;
                                
                                NSString *title =[NSString stringWithFormat:@"Visit: %@", _visit.customer?_visit.customer.name:_visit.customerName.length?_visit.customerName:@"Visit"];
                                
                                
                                if(eventIdentifier.length)
                                    event = [[CustomEventStore sharedInstance].eventStore eventWithIdentifier:eventIdentifier];
                                
                                if(!event)
                                {
                                    event = [CustomEventStore addEventWithTitle:title startDate:_visit.visitStartDate endDate:_visit.visitEndDate location:nil notes:nil];
                                }
                                else
                                {
                                    event =[CustomEventStore editEventForIdentifier:_visit.eventIdentifier withTitle:title startDate:_visit.visitStartDate endDate:_visit.visitEndDate location:nil notes:nil];
                                }
                                
                                if(event && event.eventIdentifier.length)
                                    _visit.eventIdentifier = event.eventIdentifier;
                            }
                        }
                        
                        if([_updatedIDs count])
                        {
                            for(ScheduledVisit *visit in updatedResult)
                            {
                                ScheduledVisit *_visit =(ScheduledVisit *)[visit inContext:context];
                                
                                if([_visit respondsToSelector:NSSelectorFromString(@"eventIdentifier")] && _visit.eventIdentifier.length)
                                {
                                    NSString *title =[NSString stringWithFormat:@"Visit: %@", _visit.customer?_visit.customer.name:_visit.customerName.length?_visit.customerName:@"Visit"];
                                    
                                    EKEvent *event= [CustomEventStore editEventForIdentifier:_visit.eventIdentifier withTitle:title startDate:_visit.visitStartDate endDate:_visit.visitEndDate location:nil notes:nil];
                                    
                                    if(event && event.eventIdentifier.length)
                                        _visit.eventIdentifier = event.eventIdentifier;
                                }
                            }
                        }
                        app.eventFetchCount++;
                        
                        [[SSCoreDataManager sharedManager] saveContext:context error:nil];
                        
                        [context reset];
                        [context release];
                    });
                }];
           }
       }
       else
       {
           if(completionBlock) completionBlock(NO,request.error);
       }

   }];
    
    
    [request setFailedBlock:^{
        
        if(completionBlock) completionBlock(NO,request.error);
    }];
    
    
    return request;
}

#pragma mark - FETCH AND PROCESS Reminders
+(ASIHTTPRequest *)fetchAndProcessRemindersOnCompletion:(void(^)(BOOL success, NSError *error))completionBlock
{
    Rep *_rep = [CoreDataHandler currentRep];
    
   __block ASIHTTPRequest *request =  [ConnectionManager fetchRemindersWithRepID:_rep.repID wdSiteID:_rep.parentWdSite.wdSiteID];
    
    [request setCompletionBlock:^{
        
        NSDictionary *jsonDict = [request responseJSON];
        
        jsonDict = NULL_NIL(jsonDict);
        NSLog(@"json=%@",[request responseString]);
        
        if(jsonDict && [jsonDict isKindOfClass:[NSDictionary class]])
        {
            
            
            BOOL _isValidResponse = [ConnectionManager isValidResponse:jsonDict];
            if(_isValidResponse)
            {
                NSArray *_reminders  = [jsonDict valueForKey:JSON_DETAILS_KEY];
                NSArray *_updatedIDs = [jsonDict valueForKey:JSON_UPDATED_KEY];
                NSArray *_deletedIDs = [jsonDict valueForKey:JSON_DELETED_KEY];
                
                _reminders  = NULL_NIL(_reminders);
                _updatedIDs = NULL_NIL(_updatedIDs);
                _deletedIDs = NULL_NIL(_deletedIDs);
                
                NSNumber *lastRequested = [jsonDict valueForKey:JSON_LAST_REQUESTED_KEY];
                [ConnectionManager setLastRequested:lastRequested forConnectionType:LAST_REQUESTED_REMINDERS];
                
                
                
                [Reminder populateAsyncPerformingDelete:_deletedIDs?_deletedIDs:[NSArray array] update:_updatedIDs?_updatedIDs:[NSArray array] insertOnArray:_reminders?_reminders:[NSArray array] onAttribute:@"reminderID" completion:^(NSMutableArray *insertedResult, NSMutableArray *updatedResult, NSError *error)
                 {
                     
                     // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     dispatch_async([app eventQueue],^{
                         
                         NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType] ;
                         [context setPersistentStoreCoordinator:DEFAULT_CONTEXT.persistentStoreCoordinator];
                         [context setUndoManager:nil];
                         
                         
                         
                         for(Reminder *reminder in insertedResult)
                         {
                             if([reminder respondsToSelector:NSSelectorFromString(@"eventIdentifier")])
                             {
                                 Reminder *_reminder =(Reminder *)[reminder inContext:context];
                                 
                                 EKReminder * event = nil;
                                 NSString *eventIdentifier = _reminder.eventIdentifier;
                                 
                                 //NSString *title = [NSString stringWithFormat:@"Reminder: %@", _reminder.title?_reminder.title:@"Reminder"];
                                 
                                 
                                 if(eventIdentifier.length)
                                     event =(EKReminder *) [[CustomEventStore sharedInstance].eventStore calendarItemWithIdentifier:eventIdentifier];
                                 
                                 if(!event)
                                 {
                                     event = [CustomEventStore addReminder:_reminder];
                                 }
                                 else
                                 {
                                     event =[CustomEventStore editReminderForIdentifier:_reminder.eventIdentifier reminder:_reminder];
                                 }
                                 
                                 if(event && event.calendarItemIdentifier.length)
                                     _reminder.eventIdentifier = event.calendarItemIdentifier;
                             }
                         }
                         
                         if([_updatedIDs count])
                         {
                             for(Reminder *aRem in updatedResult)
                             {
                                 Reminder *_aRem =(Reminder *)[aRem inContext:context];
                                 
                                 if([_aRem respondsToSelector:NSSelectorFromString(@"eventIdentifier")] && _aRem.eventIdentifier.length)
                                 {
                                     
                                     EKReminder  *event = [CustomEventStore editReminderForIdentifier:_aRem.eventIdentifier reminder:_aRem];
                                     
                                     _aRem.eventIdentifier = event.calendarItemIdentifier;
                                     
                                 }
                             }
                         }
                         
                         [[SSCoreDataManager sharedManager] saveContext:context error:nil];
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                              if(completionBlock) completionBlock(YES,nil);
                             /*
                             if(self.calendarView)
                             {
                                 //[self.calendarView setNeedsLayout];
                                 
                                 [self.calendarView resetReminderCells];
                                 
                                 [self.calendarView fetchRemindersForStartDate:self.calendarView.currentStartDate endDate:self.calendarView.currentEndDate];
                             }
                             */
                             
                         });
                         
                         
                         [context reset];
                         [context release];
                     });
                 }
                  deletionBlock:^(NSMutableArray *deletedResult)
                 {
                     
                     
                 }];
                
            }
            else
            {
                if(completionBlock) completionBlock(NO,request.error);
            }
        }
        else
        {
            if(completionBlock) completionBlock(NO,request.error);
        }

    }];
    
    [request setFailedBlock:^{
        
         if(completionBlock) completionBlock(NO,request.error);
    }];

    return request;
}

@end


