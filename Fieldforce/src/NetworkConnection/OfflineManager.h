//
//  OfflineManager.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 13/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^CustomersLoadCallback) (BOOL isEditMode);

@protocol OfflineManagerDelegate;

@class News,EKEvent,ScheduledVisit,Reminder,Task,Customer,Offer,EKReminder;

@interface OfflineManager : NSObject

+(OfflineManager *)sharedManager;

-(BOOL)isNetworkAvailable;
-(void)startNotifier;
-(void)stopNotifier;

-(void)performOnlineJob;

-(void)addCustomer:(id)customer;
-(void)removeAllCustomers;

-(void)addScheduledVisit:(ScheduledVisit *)visit;
-(void)removeAllScheduledVisits;

-(void)loadFromFile;
-(BOOL)saveToFile;
-(void)sendCustomerPhotos;


@property (nonatomic,assign) id <OfflineManagerDelegate> delegate;
@property (nonatomic,readonly,retain) NSMutableArray *customers;
@property (nonatomic,readonly,retain) NSMutableArray *scheduledVisits;
@property (nonatomic,readonly,retain) NSMutableArray *tasks;
@property (nonatomic,readonly,retain) NSMutableArray *reminders;

@property (nonatomic,readonly,retain) NSMutableDictionary *newsViewedDict;
@property (nonatomic,readonly,retain) NSMutableDictionary *offersViewedDict;

-(void)addCustomer:(Customer *)customer isEditMode:(BOOL)isEditMode;
//-(void)addCustomer:(Customer *)customer isEditMode:(BOOL)isEditMode willDelete:(BOOL)isDeleted;
-(void)addCustomersLoadCallback:(CustomersLoadCallback )callback;
//-(void)addScheduledVisit:(ScheduledVisit *)scheduledVisit willDelete:(BOOL)isDelete;
-(void)addScheduledVisit:(ScheduledVisit *)scheduledVisit willDelete:(BOOL)isDelete isEditMode:(BOOL)isEditMode;
-(void)addTask:(Task *)task willDelete:(BOOL)isDelete isEditMode:(BOOL)isEditMode;
-(void)addReminder:(Reminder *)reminder event:(EKReminder *)event willDelete:(BOOL)isDelete isEditMode:(BOOL)isEditMode;
-(void)addViewedNews:(News *)news;
-(void)addViewedOffer:(Offer *)offer;
-(void)clearOfflineQueue;

@end

@protocol OfflineManagerDelegate <NSObject>

@optional

-(void)offlineManagerDidSendCustomer:(BOOL)isEditMode isDeleted:(BOOL)isDeleted;

@end
