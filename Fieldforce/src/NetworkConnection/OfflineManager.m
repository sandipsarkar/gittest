
//
//  OfflineManager.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 13/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//
#define CALULATE_CUSTOMER_LOCATION 1

#import "OfflineManager.h"
#import "Reachability.h"
#import "OfflineStore.h"
#import "Customer.h"
#import "Task.h"
#import "Rep.h"
#import "WholesaleDealer.h"
#import "ScheduledVisit.h"
#import "News.h"
#import "Offer.h"
#import "ConnectionManager.h"
#import "CustomerContact.h"
#import "Territory.h"
//#import "CoreDataHandler.h"
#import "DirectoryManager.h"
#import "CustomerPhoto.h"
#import "MapConstants.h"
#import "CustomEventStore.h"
#import "ADDReminder.h"
#import "Reminder.h"
#import "CoreDataHandler.h"


@interface OfflineManager()
{
    Reachability *_rechability;
    Customer     *_newCustomer;
   // NSMutableDictionary *_custDict;
    
    BOOL _isNWAlertShown;
}

@property(nonatomic,retain)OfflineStore *offlineCDStore;
@property(nonatomic, readwrite, retain) NSMutableArray *customers;
@property(nonatomic, readwrite, retain) NSMutableArray *scheduledVisits;
@property(nonatomic, readwrite, retain) NSMutableArray *reminders;
@property(nonatomic,readwrite,retain)NSMutableArray *tasks;

@property(nonatomic,readwrite,retain)NSMutableDictionary *newsViewedDict;
@property(nonatomic,readwrite,retain)NSMutableDictionary *offersViewedDict;
@property(nonatomic,copy) CustomersLoadCallback customersLoadCallback;

-(void)performOnlineJob;
-(void)sendCustomerRequest;
-(void)sendEditCustomerRequest;
-(void)sendscheduledVisitRequest;
-(void)sendNewsViewedRequest;
-(void)sendOffersViewedRequest;
-(void)sendReminderRequest;
@end


@implementation OfflineManager
@synthesize customers,scheduledVisits,newsViewedDict,offersViewedDict,tasks,reminders;
@synthesize customersLoadCallback;
@synthesize offlineCDStore;
@synthesize delegate;

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];

    [_rechability stopNotifier];
    
    [offlineCDStore release];
    offlineCDStore=nil;
    
    [_rechability release];
    _rechability=nil;
    
    [customers release];
    customers=nil;

    [scheduledVisits release];
    scheduledVisits=nil;
    
    [newsViewedDict release];
    newsViewedDict=nil;
    
    [offersViewedDict release];
    offersViewedDict=nil;
    
    [tasks release];
    tasks = nil;
    
    [reminders release];
    reminders = nil;
    
   
    [customersLoadCallback release];
    customersLoadCallback = nil;
    
    
    [super dealloc];
}

+(OfflineManager *)sharedManager
{
    static OfflineManager *_manager=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
     _manager = [[OfflineManager alloc] init];
        
    });
    
    return _manager;
}

-(BOOL)isNetworkAvailable
{
    return _rechability.isReachable;
}

-(id)init
{
    if((self = [super init]))
    {
        
        NSString *hostName = [[NSURL URLWithString:@"http://www.google.com"] host];
        _rechability=[[Reachability reachabilityWithHostName:hostName] retain];
        
         //_rechability=[[Reachability reachabilityForInternetConnection] retain];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionDidChange:) name:kReachabilityChangedNotification object:nil];
        
          //[self loadFromFile];
         //offlineCDStore =(OfflineStore *)[[OfflineStore insertNewObject] retain];
    }
    
    return self;
}

-(OfflineStore *)offlineCDStore
{
    if(offlineCDStore) return offlineCDStore;
    
    return [CoreDataHandler offlineStore];
}


-(BOOL)saveToFile
{
    
    return YES;
    
    NSString *offlineDataPath = [DirectoryManager offlineDataPathForName:@"offline.plist"];
    
    if(!offlineDataPath) return NO;
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    if(self.customers)
    [dict setObject:self.customers forKey:@"rep_customers_tool"];
    
    if(self.scheduledVisits)
    [dict setObject:self.scheduledVisits forKey:@"rep_scheduledVisits_tool"];
    
    if(self.newsViewedDict)
    [dict setObject:self.newsViewedDict forKey:@"rep_newsViewedArr_tool"];
    
    if(self.offersViewedDict)
    [dict setObject:self.offersViewedDict forKey:@"rep_offersViewedArr_tool"];
    
    if(self.tasks)
    [dict setObject:self.tasks forKey:@"rep_tasks_tool"];
    
    if(self.reminders)
        [dict setObject:self.reminders forKey:@"rep_reminders_tool"];

    
    NSData * data = [NSKeyedArchiver archivedDataWithRootObject:dict];
    BOOL written  = [data writeToFile:offlineDataPath atomically:YES];
    
    [dict release];
    
    
    if(written)
    {
        NSLog(@"Written to file");
    }
    
    return written;
}

-(void)loadFromFile
{
    if(customers)
    {
     [customers release];
     customers = nil;
    }
    
    if(scheduledVisits)
    {
     [scheduledVisits release];
      scheduledVisits = nil;
    }
    
    if(tasks)
    {
     [tasks release];
     tasks = nil;
    }
    
    if(reminders)
    {
      [reminders release];
       reminders = nil;
    }
    
    if(newsViewedDict)
    {
      [newsViewedDict release];
      newsViewedDict = nil;
    }
    
    if(offersViewedDict)
    {
      [offersViewedDict release];
      offersViewedDict = nil;
    }
    

    
    NSString *offlineDataPath = [DirectoryManager offlineDataPathForName:@"offline.plist"];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:offlineDataPath]) return;
    
    
    NSData * data = [[NSData alloc] initWithContentsOfFile:offlineDataPath options:NSDataReadingMappedIfSafe error:nil];
    //NSData * data = [[NSData alloc] initWithContentsOfMappedFile:offlineDataPath];
    
    NSDictionary *offlineDict = data ?  [NSKeyedUnarchiver unarchiveObjectWithData:data] : nil;
    
    [data release];
    
    if(!offlineDict) return;
    
   // NSDictionary *offlineDict = [NSDictionary dictionaryWithContentsOfFile:offlineDataPath];
    
    NSMutableArray      *_customers        =  [offlineDict valueForKey:@"rep_customers_tool"];
    NSMutableArray      *_scheduledVisits  =  [offlineDict valueForKey:@"rep_scheduledVisits_tool"];
    NSMutableDictionary *_offersViewedDict =  [offlineDict valueForKey:@"rep_offersViewedDict_tool"];
    NSMutableDictionary *_newsViewedDict   =  [offlineDict valueForKey:@"rep_newsViewedDict_tool"];
    NSMutableArray      *_tasks            =  [offlineDict valueForKey:@"rep_tasks_tool"];
    NSMutableArray     *_reminders         =  [offlineDict valueForKey:@"rep_reminders_tool"];
    
    if([_customers count])
    {
        [CoreDataHandler setOfflineArray:_customers forOperationKey:CREATE_CUSTOMER_METHOD];
    }
    
    if([_scheduledVisits count])
    {
         [CoreDataHandler setOfflineArray:_customers forOperationKey:EDIT_UPCOMMING_VISITS_METHOD];
    }
    
    if([_offersViewedDict count])
    {
        [CoreDataHandler setOfflineArray:_offersViewedDict forOperationKey:OFFER_VIEWED_METHOD];
    }
    
    if([_newsViewedDict count])
    {
        [CoreDataHandler setOfflineArray:_newsViewedDict forOperationKey:NEWS_VIEWED_METHOD];
    }
    
    if([_tasks count])
    {
        [CoreDataHandler setOfflineArray:_tasks forOperationKey:CREATE_TASK_METHOD];
    }
    
    if([_reminders count])
    {
        [CoreDataHandler setOfflineArray:_reminders forOperationKey:CREATE_REMINDER_METHOD];
    }
    
    
    if([[NSFileManager defaultManager] fileExistsAtPath:offlineDataPath])
    {
        [[NSFileManager defaultManager] removeItemAtPath:offlineDataPath error:nil];
    }
    
    /*
   customers = _customers ? [[NSMutableArray alloc]initWithArray:_customers] : [[NSMutableArray alloc]init];
    
    tasks   = _tasks ? [[NSMutableArray alloc]initWithArray:_tasks] : [[NSMutableArray alloc]init];
    
   scheduledVisits  = _scheduledVisits  ? [[NSMutableArray alloc]initWithArray:_scheduledVisits] :  [[NSMutableArray alloc]init];
   newsViewedDict   = _offersViewedDict   ? [[NSMutableDictionary alloc]initWithDictionary:_offersViewedDict] :  [[NSMutableDictionary alloc]init];
   offersViewedDict = _offersViewedDict   ? [[NSMutableDictionary alloc]initWithDictionary:_newsViewedDict]   :  [[NSMutableDictionary alloc]init];
    
   reminders = _reminders ? [[NSMutableArray alloc]initWithArray:_reminders] : [[NSMutableArray alloc]init];
    */
}

-(NSMutableArray *)customers
{
    if(!customers)
    {
        NSMutableArray *_arr = [CoreDataHandler offlineArrayForOperationKey:CREATE_CUSTOMER_METHOD];
        customers = _arr ? [[NSMutableArray alloc] initWithArray:_arr] : [[NSMutableArray alloc] init];

    }
    
    return customers;
}

-(NSMutableArray *)tasks
{
    if(!tasks)
    {
        NSMutableArray *_arr = [CoreDataHandler offlineArrayForOperationKey:CREATE_TASK_METHOD];
        tasks = _arr ? [[NSMutableArray alloc] initWithArray:_arr] : [[NSMutableArray alloc] init];
    }
    
    return tasks;

}

-(NSMutableArray *)reminders
{
    if(!reminders)
    {
        NSMutableArray *_arr = [CoreDataHandler offlineArrayForOperationKey:CREATE_REMINDER_METHOD];
        reminders = _arr ? [[NSMutableArray alloc] initWithArray:_arr] : [[NSMutableArray alloc] init];
    }
    
    return reminders;

}

-(NSMutableArray *)scheduledVisits
{
    if(!scheduledVisits)
    {
        NSMutableArray *_arr = [CoreDataHandler offlineArrayForOperationKey:EDIT_UPCOMMING_VISITS_METHOD];
        scheduledVisits = _arr ? [[NSMutableArray alloc] initWithArray:_arr] : [[NSMutableArray alloc] init];
    }
    
    return scheduledVisits;
}


-(NSMutableDictionary *)offersViewedDict
{
    if(!offersViewedDict)
    {
        NSMutableDictionary *_dict = (NSMutableDictionary *) [CoreDataHandler offlineArrayForOperationKey:OFFER_VIEWED_METHOD];
        offersViewedDict = _dict ? [[NSMutableDictionary alloc] initWithDictionary:_dict] : [[NSMutableDictionary alloc] init];
        
    }
    
    return offersViewedDict;
    
}

-(NSMutableDictionary *)newsViewedDict
{
    if(!newsViewedDict)
    {
        NSMutableDictionary *_dict = (NSMutableDictionary *) [CoreDataHandler offlineArrayForOperationKey:NEWS_VIEWED_METHOD];
        newsViewedDict = _dict ? [[NSMutableDictionary alloc] initWithDictionary:_dict] : [[NSMutableDictionary alloc] init];
    }
    
    return newsViewedDict;
}

/*
-(void)saveToFile
{
    @try {
        
    OfflineStore *_offlineCDStore = self.offlineCDStore;
    if([_offlineCDStore isFault]) return;
    
    if(customers)
    _offlineCDStore.customerList       = customers;
        
    if(scheduledVisits)
    _offlineCDStore.scheduledVisitList = scheduledVisits;
        
    if(newsViewedDict)
    _offlineCDStore.newsDict           = newsViewedDict;
        
    if(offersViewedDict)
    _offlineCDStore.offersDict         = offersViewedDict;
        
    if(tasks)
    _offlineCDStore.tasks              = tasks;
        
    if(reminders)
    _offlineCDStore.reminders          = reminders;
    
    
    [[SSCoreDataManager sharedManager] save:nil];
        
    }
    @catch (NSException *exception) 
    {
        [exception print];
    }
    @finally 
    {
        
    }
}


-(void)loadFromFile
{
    self.customers = nil;
    self.scheduledVisits = nil;
    self.tasks = nil;
    self.reminders = nil;
    self.newsViewedDict = nil;
    self.offersViewedDict = nil;

    
    OfflineStore *_offlineCDStore = self.offlineCDStore;
    
    NSMutableArray *_customers             = _offlineCDStore.customerList;
    NSMutableArray *_scheduledVisits       = _offlineCDStore.scheduledVisitList;
    NSMutableDictionary *_offersViewedDict = _offlineCDStore.offersDict;
    NSMutableDictionary *_newsViewedDict   = _offlineCDStore.newsDict;
    NSMutableArray *_tasks                 = _offlineCDStore.tasks;
    NSMutableArray *_reminders             = _offlineCDStore.reminders;
    
    customers = _customers ? [[NSMutableArray alloc]initWithArray:_customers] : [[NSMutableArray alloc]init];
    tasks     = _tasks ? [[NSMutableArray alloc]initWithArray:_tasks] : [[NSMutableArray alloc]init];
    reminders = _reminders ? [[NSMutableArray alloc]initWithArray:_reminders] : [[NSMutableArray alloc]init];
    scheduledVisits  = _scheduledVisits   ? [[NSMutableArray alloc]initWithArray:_scheduledVisits] :  [[NSMutableArray alloc]init];
    newsViewedDict   = _offersViewedDict   ? [[NSMutableDictionary alloc]initWithDictionary:_offersViewedDict] :  [[NSMutableDictionary alloc]init];
    offersViewedDict = _offersViewedDict     ? [[NSMutableDictionary alloc]initWithDictionary:_newsViewedDict]   :  [[NSMutableDictionary alloc]init];
}

*/

-(void)startNotifier
{
     [_rechability startNotifier];
}
-(void)stopNotifier
{
    [_rechability stopNotifier];
}

-(void)connectionDidChange:(NSNotification *)notification
{
    Reachability *rechability = [notification object];
    
    //NetworkStatus _status = [rechability currentReachabilityStatus];
    //BOOL isAvailable =(_status!=kNotReachable);
    
    BOOL isNWAvailable = [rechability isReachable];
    
    if(isNWAvailable)
    {
        NSLog(@"Network is available.");
        [self performOnlineJob];
    }
    else
    {
        NSLog(@"Network is not available.");
        
        if(![AppDelegate isLoggedIn]) return;
        
        
        if(!_isNWAlertShown)
        {
        _isNWAlertShown = YES;
            
         ProgressHUD *progress = [ProgressHUD showHUDAddedTo:app.window animated:YES];
         progress.mode = MBProgressHUDModeText;
         progress.tag=NSIntegerMax-1;
         progress.labelText = POOR_CONNECTION_MSG;        
        
        [self performSelector:@selector(hideNetworkSplashView) withObject:nil afterDelay:3.5];
            
        }
    }
}

-(void)hideNetworkSplashView
{
     [ProgressHUD hideHUDForView:app.window  animated:YES];
    _isNWAlertShown = NO;
}

-(void)addCustomersLoadCallback:(CustomersLoadCallback )callback
{
    self.customersLoadCallback = callback;
}

#pragma mark PERFORM ONLINE JOB
-(void)performOnlineJob
{
    if(![self isNetworkAvailable] || ![AppDelegate isLoggedIn]) return;
    
    [self sendCustomerRequest];
    [self sendEditCustomerRequest];
    [self sendNewsViewedRequest];
    [self sendOffersViewedRequest];
    [self sendTaskRequest];
    [self sendscheduledVisitRequest];
    [self sendReminderRequest];
    
}
#pragma mark -

-(void)addCustomer:(Customer *)customer
{
    [self addCustomer:customer isEditMode:NO];
}

-(void)addCustomer:(Customer *)customer isEditMode:(BOOL)isEditMode
{
    if(!customer) return;
    
    BOOL _isEditMode=isEditMode;
    
    BOOL isNewCustomer = YES;
    
    NSLog(@"territoryPosition=%@",customer.previousTerritory); 
    
    //IF USER EDIT ANY OFFLINE DATA THEN WE NEED TO FIND OUT THE DATA FIRST THEN UPDATE IT WITH NEW VALUES.
    if([self.customers count])
    {
        NSArray *fetchedArr= [self.customers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.customerID=%@",customer.customerID]];
        
        if([fetchedArr count])
        {
            isNewCustomer = NO;
            
            NSMutableDictionary *custDict = [fetchedArr objectAtIndex:0];
            
            NSArray *keysToIgnore = [NSArray arrayWithObjects:@"parentRep",@"tasks",@"visits",@"scheduledVisits",@"completeVisitCount",@"incompleteVisitCount",@"nextVisit",@"modifiedDate",@"isOffline",@"deleted",@"createdDate",@"isActive",@"city",@"photos",@"cPhotos",@"otherOEMs", nil];//@"contacts"
            
             NSNumber *_isEditModeOn = [custDict valueForKey:@"isEditModeOn"];
            
            NSString *allDeletedcontactIDS = [custDict valueForKey:@"deletedContactIDs"];
            
            NSArray *allkeys=[custDict allKeys];
            for(NSString *key in allkeys)
            {
                if([keysToIgnore containsObject:key]) continue;
                
                
                ////====================== EDIT CONTACT =================================
                
                if([key isEqualToString:@"contacts"])
                {
                  NSMutableArray *_allContacts = [NSMutableArray array];
                  NSArray *_contacts = customer.contacts.allObjects;
                    
                  if([_contacts count])
                  {
                      NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate" ascending:YES];
                      _contacts = [_contacts sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
                      [sortDescriptor release];
                  }
                    
                  NSMutableArray *_contactProperties = [NSMutableArray arrayWithObjects:@"direct",@"email",@"mobile",@"name",@"title",@"contactID",@"oldContactID",@"isEditModeOn",@"customerID", nil];
                
                  for(CustomerContact *contact in _contacts)
                   {
                     //if(!isEditMode ||  (contact.isInserted || contact.isUpdated))
                    //if((contact.isInserted || contact.isUpdated))
                      {
                        NSLog(@"isEdited:%d",contact.isEditModeOn);
                        NSDictionary *dict =   [contact dictionaryWithValuesForKeys:_contactProperties];
                        [_allContacts addObject:dict];
                      }
                  }
                    
                    [custDict setValue:_allContacts forKey:@"contacts"];
                    
                    continue;
                }

                
                //====================================================================
                
                id oldVal = [custDict valueForKey:key];
                SEL _selector= NSSelectorFromString(key);
                id newVal = [customer respondsToSelector:_selector] ? [customer valueForKey:key] : oldVal;
                
                               
                newVal = NULL_NIL(newVal);
                
                if(![oldVal isEqual:newVal])
                {
                     newVal = newVal? newVal : [newVal isKindOfClass:[NSString class]] ? @"" : [NSNull null];
                    [custDict setValue:newVal forKey:key];
                }
            }
            
            //--------------------------- UPDATED DELETED OEMS ------------------------------
            NSString *oemIdsString = customer.dealingOEMs;
            [custDict setValue:oemIdsString?oemIdsString:@"" forKey:@"oemIDs"];
            
            [custDict setObject:_isEditModeOn forKey:@"isEditModeOn"];
            
            if(![custDict valueForKey:@"deviceID"])
            {
              NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
              [custDict setValue:deviceID forKey:@"deviceID"];
            }
            
        
            
             //------------------------- UPDATED DELETED CONTACT IDS ---------------------------
            NSMutableSet *_deletedContactsIDS = nil;
            
              if(allDeletedcontactIDS.length)
              {
                  NSArray *_allDeletedIDS = [allDeletedcontactIDS componentsSeparatedByString:@","];
                  
                  _deletedContactsIDS = _allDeletedIDS ? [NSMutableSet setWithArray:_allDeletedIDS] : [NSMutableSet set];
                  if([customer.deletedContactIds count])
                  {
                      [_deletedContactsIDS unionSet:customer.deletedContactIds];
                  }
              }
            
            if(![_deletedContactsIDS count])
            {
                _deletedContactsIDS = customer.deletedContactIds;
            }
            
             NSString *deletedIDS = [_deletedContactsIDS count] ? [_deletedContactsIDS.allObjects componentsJoinedByString:@","] : @"";
           
           // NSString *deletedIDS = [customer.deletedContactIds count] ? [customer.deletedContactIds.allObjects componentsJoinedByString:@","] : @"";
            
            customer.deletedContactIds = nil;
            [custDict setValue:deletedIDS?deletedIDS:@"" forKey:@"deletedContactIDs"];
        }
    }
    
    if(isNewCustomer)
    {
        Customer *newCustomer =[customer retain];
  
       
        Rep *parentRep   = [CoreDataHandler currentRep];
        NSNumber *repID  = parentRep .repID;
        NSNumber *userId = parentRep .userID;
        //NSNumber *siteID =_isEditMode? _newCustomer.wdSiteID : parentRep.parentWdSite.wdSiteID;
        NSNumber *siteID = parentRep.parentWdSite.wdSiteID;
        newCustomer.wdSiteID = siteID;
        
      
        
       // Territory *_territory = [CoreDataHandler territoryForTerritoryPosition:newCustomer.territory];
         Territory *_territory = [CoreDataHandler territoryForTerritoryID:newCustomer.territoryID];
        
        NSString *oemIdsString = newCustomer.dealingOEMs;
        
        NSLog(@"Customer catID:%@",newCustomer.categoryID);
        
        //TO DO : SET CAT ID HERE
        /*
        NSInteger catID =([[newCustomer.categoryName lowercaseString] isEqualToString:@"all"])? 0: ([[newCustomer.categoryName lowercaseString] isEqualToString:@"mechanical"]) ? 1 : 2;
        newCustomer.categoryID = [NSNumber numberWithInteger:catID];
        */
        
        NSInteger catID  = [newCustomer.categoryID integerValue];
        
        NSMutableArray *_allContacts = [NSMutableArray array];
        NSArray *_contacts = newCustomer.contacts.allObjects;
        NSMutableArray *_contactProperties = [NSMutableArray arrayWithObjects:@"direct",@"email",@"mobile",@"name",@"title",@"contactID",@"oldContactID",@"isEditModeOn",@"customerID", nil];
        
        if([_contacts count])
        {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate" ascending:YES];
            _contacts = [_contacts sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            [sortDescriptor release];
        }

        
        for(CustomerContact *contact in _contacts)
        {
           
           // if(!isEditMode ||  (contact.isInserted || contact.isUpdated))
            {
              NSLog(@"isEdited:%d",contact.isEditModeOn);
              NSDictionary *dict =   [contact dictionaryWithValuesForKeys:_contactProperties];
              [_allContacts addObject:dict];
            }
            //else if(contact.isDeleted){}
        }
        
                
        NSString *deletedIDS = [newCustomer.deletedContactIds count] ? [newCustomer.deletedContactIds.allObjects componentsJoinedByString:@","] : @"";
        newCustomer.deletedContactIds=nil;
        
        NSArray *allAttribs = [[[Customer entity] propertiesByName] allKeys];
        NSMutableArray *allProperties = [NSMutableArray arrayWithArray:allAttribs];
        [allProperties removeObjectsInArray:[NSArray arrayWithObjects:@"parentRep",@"tasks",@"visits",@"scheduledVisits",@"completeVisitCount",@"incompleteVisitCount",@"nextVisit",@"modifiedDate",@"isOffline",@"deleted",@"createdDate",@"isActive",@"city",@"photos",@"cPhotos",@"contacts",@"otherOEMs", nil]];
        

        //NSArray* attributes = [NSArray arrayWithObjects:@"address",@"address2",@"categoryName",@"city",@"customerID",@"customerType",@"isDeleted",@"email",@"fax",@"isActive",@"isOffline",@"lastVisit",@"latitude",@"longitude",@"name",@"phone",@"postCode",@"state",@"suburb",@"tollfree",@"wdID", nil];
        
        
        NSDictionary *custDict    = [newCustomer dictionaryWithValuesForKeys:allProperties];//[_newCustomer toDictionary];
         NSMutableDictionary *_custDict = [ NSMutableDictionary dictionaryWithDictionary:custDict];
        [_custDict setValue:repID?repID:[NSNull null]  forKey:@"repID"];
        [_custDict setValue:userId?userId:[NSNull null] forKey:@"userID"];
        [_custDict setValue:siteID?siteID:[NSNull null] forKey:@"wdSiteID"];

        [_custDict setObject:[NSNumber numberWithInteger:catID] forKey:@"categoryID"];
        [_custDict setObject:[NSNumber numberWithInt:_isEditMode?1:0] forKey:@"isEditModeOn"];
 
        [_custDict setObject:newCustomer.customerID forKey:@"customerID"];
        
        
        [_custDict setObject:(_territory)?_territory.territoryID:[NSNumber numberWithInt:0] forKey:@"territory"];
        
        //Territory *_previousTerritory = [CoreDataHandler territoryForTerritoryPosition:customer.previousTerritory];
        Territory *_previousTerritory = [CoreDataHandler territoryForTerritoryID:customer.previousTerritory];
        [_custDict setObject:(_previousTerritory)?_previousTerritory.territoryID:[NSNumber numberWithInt:0] forKey:@"previousTerritory"];    
        
        //[_custDict setObject:(_territory)?_territory.territoryID:_newCustomer.territory forKey:@"territory"];
        
        
        [_custDict setValue:oemIdsString?oemIdsString:@"" forKey:@"oemIDs"];
        
        //[_custDict setValue:@"1,2" forKey:@"oemIds"];
        [_custDict setValue:_allContacts forKey:@"contacts"];
        [_custDict setValue:deletedIDS?deletedIDS:@"" forKey:@"deletedContactIDs"];
        
        NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
        [_custDict setValue:deviceID forKey:@"deviceID"];
        
        NSTimeInterval _dueDateInterval  =  [[NSDate date] GMTTimeIntervalSince1970] ;
        [_custDict setValue:[NSString stringWithFormat:@"%lf",_dueDateInterval] forKey:@"modifiedDate"];
        
        [self.customers addObject:_custDict];
        
        //================== CALCULATE LAT & LONG IF NOT AVAILABLE OR INVALID =========================
#if CALULATE_CUSTOMER_LOCATION
        CLLocationCoordinate2D _coordinate =  CLLocationCoordinate2DMake(newCustomer.latitude.doubleValue, newCustomer.longitude.doubleValue);
        if(!isValidLocation(_coordinate) || newCustomer.isAddressChanged)
        {
          NSString *address = [newCustomer fullAddress];
          CLLocationCoordinate2D _loc = CLLocationCoordinateFromAddress(address);
            
            NSLog(@"LATITUDE=%+.7f LONGITUDE=%+.7f",_loc.latitude,_loc.longitude);
          [_custDict setValue:[NSString stringWithFormat:@"%+.7f",_loc.latitude] forKey:@"latitude"];
          [_custDict setValue:[NSString stringWithFormat:@"%+.7f",_loc.longitude] forKey:@"longitude"];
            
            newCustomer.isAddressChanged = NO;
        }
#endif        
     
        [newCustomer release];
    }
    
    [self sendCustomerRequest];
    [self sendEditCustomerRequest];
    
}

/*
#pragma mark UNDER DEVELOPMENT
-(void)addCustomer:(Customer *)customer isEditMode:(BOOL)isEditMode willDelete:(BOOL)isDeleted
{
    if(!customer) return;
    
    BOOL _isEditMode=isEditMode;
    
    BOOL isNewCustomer = YES;
    
    NSLog(@"territoryPosition=%@",customer.previousTerritory);
    
    //IF USER EDIT ANY OFFLINE DATA THEN WE NEED TO FIND OUT THE DATA FIRST THEN UPDATE IT WITH NEW VALUES.
    if([self.customers count])
    {
        NSArray *fetchedArr= [self.customers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.customerID=%@",customer.customerID]];
        
        if([fetchedArr count])
        {
            isNewCustomer = NO;
            
            NSMutableDictionary *custDict = [fetchedArr objectAtIndex:0];
            
            if(isDeleted)
            {
                [self.customers removeObject:custDict];
            }
            else{
            
            NSArray *keysToIgnore = [NSArray arrayWithObjects:@"parentRep",@"tasks",@"visits",@"scheduledVisits",@"completeVisitCount",@"incompleteVisitCount",@"nextVisit",@"modifiedDate",@"isOffline",@"deleted",@"createdDate",@"isActive",@"city",@"photos",@"cPhotos",@"otherOEMs", nil];//@"contacts"
            
            NSNumber *_isEditModeOn = [custDict valueForKey:@"isEditModeOn"];
            
            NSArray *allkeys=[custDict allKeys];
            for(NSString *key in allkeys)
            {
                if([keysToIgnore containsObject:key]) continue;
                
                
                ////====================== EDIT CONTACT =================================
                
                if([key isEqualToString:@"contacts"])
                {
                    NSMutableArray *_allContacts = [NSMutableArray array];
                    NSSet *_contacts = customer.contacts;
                    NSMutableArray *_contactProperties = [NSMutableArray arrayWithObjects:@"direct",@"email",@"mobile",@"name",@"title",@"contactID",@"oldContactID",@"isEditModeOn", nil];
                    
                    for(CustomerContact *contact in _contacts)
                    {
                        if(!isEditMode ||  (contact.isInserted || contact.isUpdated))
                        {
                            NSLog(@"isEdited:%d",contact.isEditModeOn);
                            NSDictionary *dict =   [contact dictionaryWithValuesForKeys:_contactProperties];
                            [_allContacts addObject:dict];
                        }
                    }
                    
                    [custDict setValue:_allContacts forKey:@"contacts"];
                    
                    continue;
                }
                
                
                //====================================================================
                
                id oldVal = [custDict valueForKey:key];
                SEL _selector=NSSelectorFromString(key);
                id newVal = [customer respondsToSelector:_selector] ? [customer valueForKey:key] : oldVal;
                
                
                newVal = NULL_NIL(newVal);
                
                if(![oldVal isEqual:newVal])
                {
                    newVal = newVal? newVal : [newVal isKindOfClass:[NSString class]] ? @"" : [NSNull null];
                    [custDict setValue:newVal forKey:key];
                }
            }
            
            [custDict setObject:_isEditModeOn forKey:@"isEditModeOn"];
            
            if(![custDict valueForKey:@"deviceID"])
            {
                NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
                [custDict setValue:deviceID forKey:@"deviceID"];
            }
            }
        }
    }
    
    if(isNewCustomer)
    {
        Customer *newCustomer =[customer retain];
        
        //Rep *parentRep   = _newCustomer.parentRep?_newCustomer.parentRep : [CoreDataHandler currentRep];
        Rep *parentRep   = [CoreDataHandler currentRep];
        NSNumber *repID  = parentRep .repID;
        // repID=repID?repID:[NSNumber numberWithInt:1];
        NSNumber *userId = parentRep .userID;
        //NSNumber *siteID =_isEditMode? _newCustomer.wdSiteID : parentRep.parentWdSite.wdSiteID;
        NSNumber *siteID = parentRep.parentWdSite.wdSiteID;
        newCustomer.wdSiteID = siteID;
        
        if(isDeleted)
        {
            NSMutableDictionary *_custDict = [[NSMutableDictionary alloc] init];
            [_custDict setValue:repID?repID:[NSNull null]  forKey:@"repID"];
            [_custDict setValue:userId?userId:[NSNull null] forKey:@"userID"];
            [_custDict setValue:siteID?siteID:[NSNull null] forKey:@"wdSiteID"];
            
            [_custDict setObject:[NSNumber numberWithInt:0] forKey:@"isEditModeOn"];
            [_custDict setObject:[NSNumber numberWithInt:1] forKey:@"isDeleted"];
            
            [_custDict setObject:newCustomer.customerID forKey:@"customerID"];

            [self.customers addObject:_custDict];
            [_custDict release];
        }
        else
        {
        // userId = userId?userId:[NSNumber numberWithInt:4];
        
        // NSArray *oemIDs =(NSArray *)[_newCustomer.parentRep.parentWdSite.parentOem.allObjects valueForKey:@"oemID"];
        // NSString *oemIdsString = oemIDs.count ? [oemIDs  componentsJoinedByString:@","] : nil;
        
        // Territory *_territory = [CoreDataHandler territoryForTerritoryPosition:newCustomer.territory];
        Territory *_territory = [CoreDataHandler territoryForTerritoryID:newCustomer.territoryID];
        
        NSString *oemIdsString = newCustomer.dealingOEMs;
        
        NSLog(@"Customer catID:%@",newCustomer.categoryID);
        
        //TO DO : SET CAT ID HERE
        NSInteger catID =([[newCustomer.categoryName lowercaseString] isEqualToString:@"all"])? 0: ([[newCustomer.categoryName lowercaseString] isEqualToString:@"mechanical"]) ? 1 : 2;
        
        newCustomer.categoryID = [NSNumber numberWithInteger:catID];
        
        NSMutableArray *_allContacts = [NSMutableArray array];
        NSSet *_contacts = newCustomer.contacts;
        NSMutableArray *_contactProperties = [NSMutableArray arrayWithObjects:@"direct",@"email",@"mobile",@"name",@"title",@"contactID",@"oldContactID",@"isEditModeOn", nil];
        
        for(CustomerContact *contact in _contacts)
        {
            if(!isEditMode ||  (contact.isInserted || contact.isUpdated))
            {
                NSLog(@"isEdited:%d",contact.isEditModeOn);
                NSDictionary *dict =   [contact dictionaryWithValuesForKeys:_contactProperties];
                [_allContacts addObject:dict];
            }
            //else if(contact.isDeleted){}
        }
        
        
        NSString *deletedIDS = [newCustomer.deletedContactIds.allObjects componentsJoinedByString:@","];
        newCustomer.deletedContactIds=nil;
        
        NSArray *allAttribs = [[[Customer entity] propertiesByName] allKeys];
        NSMutableArray *allProperties = [NSMutableArray arrayWithArray:allAttribs];
        [allProperties removeObjectsInArray:[NSArray arrayWithObjects:@"parentRep",@"tasks",@"visits",@"scheduledVisits",@"completeVisitCount",@"incompleteVisitCount",@"nextVisit",@"modifiedDate",@"isOffline",@"deleted",@"createdDate",@"isActive",@"city",@"photos",@"cPhotos",@"contacts",@"otherOEMs", nil]];
        
        
        //  NSArray* attributes = [NSArray arrayWithObjects:@"address",@"address2",@"categoryName",@"city",@"customerID",@"customerType",@"isDeleted",@"email",@"fax",@"isActive",@"isOffline",@"lastVisit",@"latitude",@"longitude",@"name",@"phone",@"postCode",@"state",@"suburb",@"tollfree",@"wdID", nil];
        
        
        NSDictionary *custDict    = [newCustomer dictionaryWithValuesForKeys:allProperties];//[_newCustomer toDictionary];
        NSMutableDictionary *_custDict = [ NSMutableDictionary dictionaryWithDictionary:custDict];
        [_custDict setValue:repID?repID:[NSNull null]  forKey:@"repID"];
        [_custDict setValue:userId?userId:[NSNull null] forKey:@"userID"];
        [_custDict setValue:siteID?siteID:[NSNull null] forKey:@"wdSiteID"];
        
        [_custDict setObject:[NSNumber numberWithInteger:catID] forKey:@"categoryID"];
        [_custDict setObject:[NSNumber numberWithInt:_isEditMode?1:0] forKey:@"isEditModeOn"];
        [_custDict setObject:[NSNumber numberWithInt:0] forKey:@"isDeleted"];
        
        [_custDict setObject:newCustomer.customerID forKey:@"customerID"];
        
        
        [_custDict setObject:(_territory)?_territory.territoryID:[NSNumber numberWithInt:0] forKey:@"territory"];
        
        //Territory *_previousTerritory = [CoreDataHandler territoryForTerritoryPosition:customer.previousTerritory];
        Territory *_previousTerritory = [CoreDataHandler territoryForTerritoryID:customer.previousTerritory];
        [_custDict setObject:(_previousTerritory)?_previousTerritory.territoryID:[NSNumber numberWithInt:0] forKey:@"previousTerritory"];
        
        //[_custDict setObject:(_territory)?_territory.territoryID:_newCustomer.territory forKey:@"territory"];
        
        
        [_custDict setValue:oemIdsString?oemIdsString:@"" forKey:@"oemIDs"];
        
        //[_custDict setValue:@"1,2" forKey:@"oemIds"];
        [_custDict setValue:_allContacts forKey:@"contacts"];
        [_custDict setValue:deletedIDS?deletedIDS:@"" forKey:@"deletedContactIDs"];
        
        NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
        [_custDict setValue:deviceID forKey:@"deviceID"];
        
        NSTimeInterval _dueDateInterval  =  [[NSDate date] GMTTimeIntervalSince1970] ;
        [_custDict setValue:[NSString stringWithFormat:@"%lf",_dueDateInterval] forKey:@"modifiedDate"];
            
        
        
        [self.customers addObject:_custDict];
        
        //================== CALCULATE LAT & LONG IF NOT AVAILABLE OR INVALID =========================
#if CALULATE_CUSTOMER_LOCATION
        CLLocationCoordinate2D _coordinate =  CLLocationCoordinate2DMake(newCustomer.latitude.doubleValue, newCustomer.longitude.doubleValue);
        if(!isValidLocation(_coordinate) || newCustomer.isAddressChanged)
        {
            NSString *address = [newCustomer fullAddress];
            CLLocationCoordinate2D _loc = CLLocationCoordinateFromAddress(address);
            
            NSLog(@"LATITUDE=%+.7f LONGITUDE=%+.7f",_loc.latitude,_loc.longitude);
            [_custDict setValue:[NSString stringWithFormat:@"%+.7f",_loc.latitude] forKey:@"latitude"];
            [_custDict setValue:[NSString stringWithFormat:@"%+.7f",_loc.longitude] forKey:@"longitude"];
            
            newCustomer.isAddressChanged = NO;
        }
#endif
        
        [newCustomer release];
            
        }
    }
    
    [self sendCustomerRequest];
    [self sendEditCustomerRequest];
    
}
*/

#pragma mark -

/*-(void)sendCustomerPhotos
{
    ASIHTTPRequest *imageRequest =[ConnectionManager uploadImagesForCustomer];
    imageRequest.showAccurateProgress = YES;
    [imageRequest setUploadProgressDelegate:self];
    [imageRequest setCompletionBlock:^{
        
       NSString *res = [imageRequest responseString];
        res= [res stringByReplacingOccurrencesOfString:@"^%#*" withString:@"="];
        NSString *response= [res stringByReplacingOccurrencesOfString:@"*%@$" withString:@","];
        
        NSArray *arr = [response componentsSeparatedByString:@"originalFileName="];
        
        NSLog(@"Arr= %@",arr);
        
        
        NSMutableSet *allPhotos= [NSMutableSet setWithSet:self.visit.photos];
        
        for(NSString *str in arr)
        {
            if(!str.length) continue;
            
            NSArray *infos = [str componentsSeparatedByString:@","];
            
            if([infos count]< 3) continue;
            
            NSString *originalImageName = [infos objectAtIndex:0];
            NSString *uploadedImageLink = [infos objectAtIndex:2];
            uploadedImageLink = [uploadedImageLink stringByReplacingOccurrencesOfString:@"path=" withString:@""];
            
            
            // ================== LOGIC SHOULD BE OPTIMIZED HERE =====================
            
            CustomerPhoto *currentPhoto=nil;
            for(CustomerPhoto *photo in allPhotos)
            {
                NSString *imageFilePath  = photo.imagePath;
                NSString *imageFileName= [imageFilePath lastPathComponent];
                
                if([imageFileName isEqualToString:originalImageName])
                {
                    //photo.serverImageLink = uploadedImageLink;
                    photo.image = [uploadedImageLink lastPathComponent];
                    currentPhoto = photo;
                    break;
                }
            }
            
            //Remove the already processed object so that we dont need to check it in the above loop.
            if(currentPhoto) [allPhotos removeObject:currentPhoto];
            // ============== LOGIC SHOULD BE OPTIMIZED HERE =====================
            
            NSLog(@"ImageLink = %@",uploadedImageLink);
            NSLog(@"Original File Name = %@",originalImageName);
        }

    }];
}*/

//BY CORE DATA
-(void)sendCustomerRequest
{
    [CoreDataHandler setOfflineArray:self.customers forOperationKey:CREATE_CUSTOMER_METHOD];
    
    if(!self.customers.count) return;
    
    NSArray *customersToBeInserted =[self.customers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.isEditModeOn=%@",[NSNumber numberWithInt:0]]];
    
    NSLog(@"test %@",customersToBeInserted);
    
    if(![customersToBeInserted count]) return;
    
    NSString *json = [customersToBeInserted JSONRepresentation];  //self.customers
    
    NSLog(@"json=%@",json);
    
    if(!json) 
    {
        NSLog(@"Cannot send request due to unknown json error");
        return;
    }
    
    NSLog(@"customersToBeInserted %@",customersToBeInserted);
    
  __block OfflineManager *weakSelf = self;
    
  __block  ASIHTTPRequest *request = [ConnectionManager createNewCustomerConnectionWithCustomerJson:json];
    
   // return;
    
    [request setCompletionBlock:^{
        
    //================================ PROCESS CUSTOMER DATA HERE =====================================
        
    if(weakSelf.customers.count)
    {
        id respone = [request responseJSON];
        
        NSLog(@"Customer Response:%@",[request responseString]);
        NSLog(@"ResponseJson:%@",respone);
        
        if(respone && [respone isKindOfClass:[NSArray class]])
        {
           //if(status && [status intValue]==1)
            {
            BOOL _isDuplicate  = NO;
                
            NSArray *oldIDs    = [respone valueForKey:@"oldCustomerID"];
                
            oldIDs = NULL_NIL(oldIDs);
            // NSArray *actualIDs = [respone valueForKey:@"actualID"];
            
            NSLog(@"old ids:%@",oldIDs);

            if([oldIDs count]) //For inserting customer only.
            {
               // NSPredicate *fetchPredicate      = [NSPredicate predicateWithFormat:@"self.customerID IN %@",oldIDs];
                NSPredicate *fetchPredicate      = [NSPredicate predicateWithFormat:@"self.oldCustomerID IN %@ OR self.customerID IN %@",oldIDs,oldIDs];
                NSMutableArray *fetchedCustomers = [Customer fetchWithPredicate:fetchPredicate onAttributes:nil sortDescriptors:nil limit:0 error:nil];


                NSMutableArray *processedCustomers = [[NSMutableArray alloc] init];
                BOOL isFailed=NO;
                for(NSDictionary *custDict in respone)
                {
                    NSNumber *status = [custDict valueForKey:@"status"];
                    status           = NULL_NIL(status);
                    
                    NSNumber *oldCutomerID     = [custDict valueForKey:@"oldCustomerID"];
                    NSNumber *actualCustomerID = [custDict valueForKey:@"newCustomerID"];
                    
                    oldCutomerID     = NULL_NIL(oldCutomerID);
                    actualCustomerID = NULL_NIL(actualCustomerID);
                    
                    if(status && [status intValue]==0)
                    {
                        isFailed = YES;
                        continue;
                    }
                    else if(oldCutomerID || actualCustomerID)
                    {
                        [processedCustomers addObject:oldCutomerID?oldCutomerID:actualCustomerID];
                    }
                    
                   
                    NSNumber *isDuplicate      = [custDict valueForKey:@"isDuplicate"];
                    isDuplicate      = NULL_NIL(isDuplicate);
                    
                    
                for(Customer *_customer in fetchedCustomers)
                {
                    if(oldCutomerID && ([_customer.oldCustomerID isEqualToNumber:oldCutomerID] || [_customer.customerID isEqualToNumber:oldCutomerID]))
                    {
                        if([isDuplicate boolValue])
                        {
                            [SSCoreDataManager deleteObject:_customer];
                            _isDuplicate = YES;
                            break;
                        }
                        
                        _customer.customerID = actualCustomerID;
                        _customer.isCustomerSentToServer = [NSNumber numberWithBool:YES];
                        
                        break;
                    }
                }
                    
                    if(!_isDuplicate)
                    {
                      NSArray *contacts = [custDict valueForKey:@"Contacts"];
                        
                      contacts = NULL_NIL(contacts);
                    
                  // NSLog(@"contacts=%d",[[CustomerContact fetchAll:nil] count]);
                    for(NSDictionary *contactDict in contacts)
                    {
                        NSNumber *oldContactId = [contactDict valueForKey:@"oldContactID"];
                        NSNumber *contactId    = [contactDict valueForKey:@"contactID"];
                        
                        oldContactId = NULL_NIL(oldContactId);
                        contactId    = NULL_NIL(contactId);
                        
                        NSPredicate *_fetchPredicate = oldContactId ? [NSPredicate predicateWithFormat:@"self.oldContactID = %@",oldContactId] : nil;
                        
                        NSMutableArray *fetchedContacts = _fetchPredicate ? [CustomerContact fetchWithPredicate:_fetchPredicate onAttributes:nil sortDescriptors:nil limit:1 error:nil] : nil;
                        
                        CustomerContact *contact = [fetchedContacts count] ? [fetchedContacts objectAtIndex:0] : nil;
                        contact.contactID = contactId;
                        contact.oldContactID = nil;
                     }
                    }
            }
                
                //===============REMOVE CUTOMER STACK=======================
             
                if([processedCustomers count])
                {
                    //DELETE ONLY PROCESSED OBJECTS
                    NSLog(@"BEFORE Tasks Count =%d",[weakSelf.customers count]);
                    
                    [weakSelf.customers filterUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (self.customerID IN %@)",processedCustomers]];
                    
                    NSLog(@"AFTER Tasks Count =%d",[weakSelf.customers count]);
                    
                      [CoreDataHandler setOfflineArray:weakSelf.customers forOperationKey:CREATE_CUSTOMER_METHOD];
                }
                
                [processedCustomers release];
                
                //if(!isFailed)
                {
                    /*
                   [weakSelf.customers removeObjectsInArray:customersToBeInserted];
                    [CoreDataHandler setOfflineArray:weakSelf.customers forOperationKey:CREATE_CUSTOMER_METHOD];
                    */
                    
                     NSError *error=nil;
                    [[SSCoreDataManager sharedManager] save:error];
                    
                    if(error)
                    {
                         NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                    }
                }
                
                
                for(Customer *_customer in fetchedCustomers)
                {
                  [weakSelf sendPhotosForCustomer:_customer];
                }
                
                if(weakSelf.customersLoadCallback)  weakSelf.customersLoadCallback(!_isDuplicate);
                // if(self.customersLoadCallback)  self.customersLoadCallback(NO);
                
                if(weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(offlineManagerDidSendCustomer: isDeleted:)])
                {
                   [weakSelf.delegate offlineManagerDidSendCustomer:!_isDuplicate isDeleted:NO];
                }
            }
                
            }
        }
        else if([respone isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * _respone =(NSDictionary *)respone;
            
            NSNumber *isUnderMaintenance  = [_respone objectForKey:JSON_UNDER_MAINTENANCE_KEY];
            isUnderMaintenance = NULL_NIL(isUnderMaintenance);
            
            if([isUnderMaintenance boolValue])
            {
                [app showUnderMaintainenceScreen:YES];
            }
        }
            
        }
    }];
    
    [request setFailedBlock:^{
        
        
    }];

}




/*
-(void)sendPhotosForCustomer:(Customer *) customer
{
    NSArray *customerPhotos = customer.photos.allObjects;
    
    if(![customerPhotos count]) return;
    
   // NSArray *arrCustImages  = [customerPhotos valueForKey:@"imageName"];
    
     NSArray *pendingPhotos = [customerPhotos filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.isUploaded=%@",[NSNumber numberWithBool:NO]]];
    
    NSMutableArray *arrImageDetails=[[NSMutableArray alloc]init];
    
       
    for(CustomerPhoto *photo in customerPhotos)
    {
        NSMutableDictionary *dict=[photo uploadDict];
        [arrImageDetails addObject:dict];
    }
    
       NSLog(@" _customerId %@",customer.customerID);
    
    if([pendingPhotos count])
    {

    ASIHTTPRequest *imageRequest =[ConnectionManager uploadPendingCustomerImages:pendingPhotos];
    //imageRequest.showAccurateProgress = YES;
    
   // ASIHTTPRequest *imageRequest =[ConnectionManager uploadImagesForCustomer:arrCustImages];
        
    imageRequest.showAccurateProgress = YES;
    [imageRequest setUploadProgressDelegate:self];
    
    [imageRequest setCompletionBlock:^{
        
       
        ASIHTTPRequest *request =[ConnectionManager saveNewCustomerImagesConnectionWithCustomerImageJson:arrImageDetails forCustomer:customer];
        
        [request setCompletionBlock:^{
            
             NSDictionary *jsonDict =  [request responseJSON];
        
            NSNumber *status = [jsonDict valueForKey:@"status"];
            
            if(status && [status boolValue])
            {
                
             NSArray *customerImageDetails =  [jsonDict valueForKey:@"customerImageDetails"];
             customerImageDetails = NULL_NIL(customerImageDetails);
             
             for(NSDictionary *imageResponseDict in customerImageDetails)
             {
                NSNumber *imageID = [imageResponseDict valueForKey:@"customerImageID"];
                NSNumber *oldImageID = [imageResponseDict valueForKey:@"oldCustomerImageID"];
                
                NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"self.customerImageID=%@",oldImageID];
                
                NSArray *photoObjects =  [customerPhotos filteredArrayUsingPredicate:filterPredicate];
                
                for(CustomerPhoto *photo in photoObjects)
                {
                    photo.customerImageID = imageID;
                    photo.isUploaded = [NSNumber numberWithBool:YES];
                }
                
             }
                
            [[SSCoreDataManager sharedManager] save:nil];
            }
            [arrImageDetails release];
            
        }];
        
        [request setFailedBlock:^{
            
        }];
    }];
    
    [imageRequest setFailedBlock:^{
        
    }];
    
  }
  else
  {
      ASIHTTPRequest *request =[ConnectionManager saveNewCustomerImagesConnectionWithCustomerImageJson:arrImageDetails forCustomer:customer];
      
      [request setCompletionBlock:^{
          
          NSDictionary *jsonDict =  [request responseJSON];
          
          NSArray *customerImageDetails =  [jsonDict valueForKey:@"customerImageDetails"];
          
          customerImageDetails = NULL_NIL(customerImageDetails);
          
          for(NSDictionary *imageResponseDict in customerImageDetails)
          {
              NSNumber *imageID = [imageResponseDict valueForKey:@"customerImageID"];
              NSNumber *oldImageID = [imageResponseDict valueForKey:@"oldCustomerImageID"];
              
              NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"self.customerImageID=%@",oldImageID];
              
              NSArray *photoObjects =  [customerPhotos filteredArrayUsingPredicate:filterPredicate];
              
              for(CustomerPhoto *photo in photoObjects)
              {
                  photo.customerImageID = imageID;
                  photo.isUploaded = [NSNumber numberWithBool:YES];
              }
              
          }

          
          [arrImageDetails release];
          
      }];
      
      [request setFailedBlock:^{
          
      }];
  }
}
*/

-(void)sendPhotosForCustomer:(Customer *) customer
{
    NSArray *customerPhotos = customer.photos.allObjects;
    
    if(![customerPhotos count]) return;
    
    // NSArray *arrCustImages  = [customerPhotos valueForKey:@"imageName"];
    
    NSArray *uploadedPhotos = [customerPhotos filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.isUploaded=%@",[NSNumber numberWithBool:YES]]];
    
    NSMutableArray *arrImageDetails = [[NSMutableArray alloc]init];
    
    
    for(CustomerPhoto *photo in uploadedPhotos)
    {
        NSMutableDictionary *dict=[photo uploadDict];
        [arrImageDetails addObject:dict];
    }
    
    NSLog(@" _customerId %@",customer.customerID);
    
    {
       __block ASIHTTPRequest *request =[ConnectionManager saveNewCustomerImagesConnectionWithCustomerImageJson:arrImageDetails forCustomer:customer];
        
        [request setCompletionBlock:^{
            
            NSDictionary *jsonDict =  [request responseJSON];
            
            NSArray *customerImageDetails =  [jsonDict valueForKey:@"customerImageDetails"];
            
            customerImageDetails = NULL_NIL(customerImageDetails);
            
            for(NSDictionary *imageResponseDict in customerImageDetails)
            {
                NSNumber *imageID = [imageResponseDict valueForKey:@"customerImageID"];
                NSNumber *oldImageID = [imageResponseDict valueForKey:@"oldCustomerImageID"];
                
                NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"self.customerImageID=%@",oldImageID];
                
                NSArray *photoObjects =  [customerPhotos filteredArrayUsingPredicate:filterPredicate];
                
                for(CustomerPhoto *photo in photoObjects)
                {
                    photo.customerImageID = imageID;
                    photo.isUploaded = [NSNumber numberWithBool:YES];
                }
            }
            
            [arrImageDetails release];
            
        }];
        
        [request setFailedBlock:^{
            
            [arrImageDetails release];
        }];
    }
}

-(void)sendCustomerPhotos:(NSArray *)arr  customerId:(NSNumber *) customerId wdSiteID:(NSNumber *)siteID
{
    NSMutableArray *arrCustImages=[NSMutableArray arrayWithArray:arr];
    NSLog(@" arrCustImages %@",arrCustImages);
    NSLog(@" _customerId %@",customerId);
    
  __block  ASIHTTPRequest *imageRequest =[ConnectionManager uploadImagesForCustomer:arrCustImages];
    imageRequest.showAccurateProgress = YES;
    [imageRequest setUploadProgressDelegate:self];
    
    [imageRequest setCompletionBlock:^{
        
        NSMutableArray *arrImageDetails=[[NSMutableArray alloc]init];
        
        for(NSUInteger i=0;i<[arrCustImages count];i++)
        {
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:[arrCustImages objectAtIndex:i] forKey:@"imageName"];
            [arrImageDetails addObject:dict];
            [dict release];
        }
        
    __block    ASIHTTPRequest *request =[ConnectionManager saveNewCustomerImagesConnectionWithCustomerImageJson:arrImageDetails forCustomer:nil];
        
        [request setCompletionBlock:^{
            
            [arrImageDetails release];
            
        }];
        
        [request setFailedBlock:^{
            
        }];
    }];
    
    [imageRequest setFailedBlock:^{
        
    }];
}

-(void)sendEditCustomerRequest
{
    [CoreDataHandler setOfflineArray:self.customers forOperationKey:CREATE_CUSTOMER_METHOD];
    
    if(!self.customers.count) return;
    
    NSArray *customersToBeEdited=[self.customers filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.isEditModeOn=%@",[NSNumber numberWithInt:1]]];
    
    if(![customersToBeEdited count]) return;
    
    NSString *json = [customersToBeEdited JSONRepresentation];
    
    NSLog(@"json=%@",json);
    
    if(!json)
    {
        NSLog(@"Cannot send request due to unknown json error");
        return;
    }
    __block OfflineManager *weakSelf = self;
    
   __block ASIHTTPRequest *request = [ConnectionManager editCustomerConnectionWithCustomerJson:json];
    [request setCompletionBlock:^{
        
        OfflineManager *strongSelf = weakSelf;
        
        if([strongSelf.customers count])
        {
        //PROCESS CUSTOMER DATA HERE
        id respone = [request responseJSON];
            
        respone = NULL_NIL(respone);
        
        NSLog(@"Response:%@",[request responseString]);
        //NSLog(@"ResponseJson:%@",respone);
        
        
          if(respone && [respone isKindOfClass:[NSArray class]])
            {
                NSMutableArray *processedCustomers = [[NSMutableArray alloc] init];
                
                BOOL _isAnyCustomerDeleted = NO;
                
                BOOL isFailed=NO;
                
            for(NSDictionary *custDict in respone)
            {

                NSLog(@"info=%@",custDict);
                
                NSNumber *status = [custDict valueForKey:@"status"];
                status = NULL_NIL(status);
                
                NSNumber *custID  = [custDict valueForKey:@"customerID"];
                custID = NULL_NIL(custID);
                
                NSNumber *oldCustID = [custDict valueForKey:@"oldCustomerID"];
                oldCustID = NULL_NIL(oldCustID);
                
                
                if(status && [status intValue]==0)
                {
                    isFailed = YES;
                    continue;
                }
                else if(custID || oldCustID)
                {
                    [processedCustomers addObject:oldCustID?oldCustID:custID];
                }

                if([[custDict valueForKey:@"isDuplicate"] boolValue])
                {
                    NSPredicate *_fetchPredicate    = custID ? [NSPredicate predicateWithFormat:@"customerID = %@",custID] : nil;
                    
                    NSMutableArray *fetchedCustomers = _fetchPredicate ? [Customer fetchWithPredicate:_fetchPredicate error:nil] : nil;
                    
                    for(Customer *__customer in fetchedCustomers)
                    {
                        _isAnyCustomerDeleted = YES;
                        [SSCoreDataManager deleteObject:__customer];
                    }
                }
                else
                {
                
                    NSArray *contacts = [custDict valueForKey:@"Contacts"];
                    
                    contacts = NULL_NIL(contacts);
            
                   for(NSDictionary *contactDict in contacts)
                    {
                      NSNumber *contactId     = [contactDict valueForKey:@"contactID"];
                      NSNumber *oldContactId  = [contactDict valueForKey:@"oldContactID"];
                        
                        contactId    = NULL_NIL(contactId);
                        oldContactId = NULL_NIL(oldContactId);
                    
                      if([oldContactId intValue]==0) continue;
                    
                        NSPredicate *_fetchPredicate    = oldContactId? [NSPredicate predicateWithFormat:@"self.oldContactID = %@",oldContactId] : nil;
                        
                        NSMutableArray *fetchedContacts = _fetchPredicate? [CustomerContact fetchWithPredicate:_fetchPredicate onAttributes:nil sortDescriptors:nil limit:1 error:nil] : nil;
                    
                      CustomerContact *contact = [fetchedContacts count] ? [fetchedContacts objectAtIndex:0] : nil;
                      contact.contactID = contactId;
                      contact.oldContactID = nil;
                    }
                    
                    ///----------  IF CUSTOMER ID IS NOT UPDATED IN OFFLINE ------------
                    
                    if(oldCustID)
                    {
                         NSPredicate *_fetchPredicate  = oldCustID ? [NSPredicate predicateWithFormat:@"oldCustomerID = %@",oldCustID] : nil;
                    
                         NSMutableArray *fetchedCustomers = _fetchPredicate ? [Customer fetchWithPredicate:_fetchPredicate error:nil] : nil;
                    
                         for(Customer *__customer in fetchedCustomers)
                         {
                             if([__customer.customerID isEqualToNumber:__customer.oldCustomerID])
                             {
                               __customer.customerID = custID;
                               __customer.isCustomerSentToServer = [NSNumber numberWithBool:YES];
                             }
                         }
                        
                        /*
                        for(Customer *_customer in fetchedCustomers)
                        {
                            [weakSelf sendPhotosForCustomer:_customer];
                        }
                        */
                    }

                }
                
            }
            
                
                if([processedCustomers count])
                {
                    //DELETE ONLY PROCESSED OBJECTS
                    NSLog(@"BEFORE Tasks Count =%d",[strongSelf.customers count]);
                    
                    [strongSelf.customers filterUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (self.customerID IN %@)",processedCustomers]];
                    
                    NSLog(@"AFTER Tasks Count =%d",[strongSelf.customers count]);
                    
                    [CoreDataHandler setOfflineArray:strongSelf.customers forOperationKey:CREATE_CUSTOMER_METHOD];
                }
                [processedCustomers release];
                
            //====================  REMOVE CUTOMER STACK   ===============
                
            
            [[SSCoreDataManager sharedManager] save:nil];
                
            if(!isFailed)
            {
               /*
                [strongSelf.customers removeObjectsInArray:customersToBeEdited];

                [CoreDataHandler setOfflineArray:strongSelf.customers forOperationKey:CREATE_CUSTOMER_METHOD];
                 [[SSCoreDataManager sharedManager] save:nil];
               */
         
                
              if(strongSelf.customersLoadCallback)   strongSelf.customersLoadCallback(YES);
                
              if(strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(offlineManagerDidSendCustomer:isDeleted:)])
               {
                  [strongSelf.delegate offlineManagerDidSendCustomer:YES isDeleted:_isAnyCustomerDeleted];
               }
             }
                
           }
            else if([respone isKindOfClass:[NSDictionary class]])
            {
                NSDictionary * _respone =(NSDictionary *)respone;
                
                NSNumber *isUnderMaintenance  = [_respone objectForKey:JSON_UNDER_MAINTENANCE_KEY];
                isUnderMaintenance = NULL_NIL(isUnderMaintenance);
                
                if([isUnderMaintenance boolValue])
                {
                    [app showUnderMaintainenceScreen:YES];
                }

            }
        }
    }];
    
    [request setFailedBlock:^{
        
    }];
}

-(void)addScheduledVisit:(ScheduledVisit *)scheduledVisit willDelete:(BOOL)isDelete isEditMode:(BOOL)isEditMode
{
    if(!scheduledVisit) return;
    
    Rep *rep = [CoreDataHandler currentRep];
    
    BOOL isNewScheduledVisit=YES;
    
   if([self.scheduledVisits count])
    {
        NSArray *fetchedArr = [self.scheduledVisits filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.scheduleVisitID=%@",scheduledVisit.scheduleVisitID]];
        
        if([fetchedArr count])
        {
            isNewScheduledVisit =NO;
            
            NSMutableDictionary *scheduledVisitDict = [fetchedArr objectAtIndex:0];
            
            if(isDelete) 
            {
                // CHECK WHEATHER ALREADY SENT FOR PROCESSING. IF YES UPDATE THE DICT TO TREAT AS DELETE DICT RATHER THAN REMOVING FROM OFFLINE STACK.
                
                BOOL _isProcessing = [[scheduledVisitDict valueForKey:@"isProcessing"] boolValue];
                
                if(_isProcessing)
                {
                    NSNumber *_scheduleVisitID =   [[scheduledVisitDict valueForKey:@"scheduleVisitID"] copy];;
                   NSNumber *_repID = [[scheduledVisitDict valueForKey:@"repID"] copy];
                   NSString *_deviceID =   [[scheduledVisitDict valueForKey:@"deviceID"] copy];
                    NSString  * _oldScheduleVisitID   =   [[scheduledVisitDict valueForKey:@"oldScheduleVisitID"] copy];
                    
                    
                   [scheduledVisitDict removeAllObjects];
                    
                    [scheduledVisitDict setValue:_scheduleVisitID forKey:@"scheduleVisitID"];
                    [scheduledVisitDict setValue:_oldScheduleVisitID forKey:@"oldScheduleVisitID"];
                    [scheduledVisitDict setValue:_repID forKey:@"repID"];
                    [scheduledVisitDict setValue:_deviceID forKey:@"deviceID"];
                    [scheduledVisitDict setValue:[NSNumber numberWithInt:1] forKey:@"isDeleted"];
                    [scheduledVisitDict setValue:[NSNumber numberWithInt:0] forKey:@"isEditModeOn"];
                    [scheduledVisitDict setValue:scheduledVisit.customer?scheduledVisit.customer.isCustomerSentToServer:[NSNumber numberWithBool:YES] forKey:@"isCustomerSentToServer"];
                    
                    [_scheduleVisitID release];
                    [_repID release];
                    [_deviceID release];
                    
                }
                else
                {
                   [self.scheduledVisits removeObject:scheduledVisitDict];
                }
            }
            else
            {
                 NSNumber *_isEditModeOn = [scheduledVisitDict valueForKey:@"isEditModeOn"];
                
                NSArray *keysToIgnore = [NSArray arrayWithObject:@"customer"];
                
                NSArray *allkeys=[scheduledVisitDict allKeys];
                for(NSString *key in allkeys)
                {
                    if([keysToIgnore containsObject:key]) continue;
                    
                    SEL _selector = NSSelectorFromString(key);
                    
                    if(![scheduledVisit respondsToSelector:_selector])
                    {
                        continue;
                    }
                    
                    id oldVal = [scheduledVisitDict valueForKey:key];
                    id newVal = [scheduledVisit valueForKey:key];
                    
                    if(![oldVal isEqual:newVal])
                    {
                        if([newVal isKindOfClass:[NSDate class]])
                        {
                             NSTimeInterval _dateInterval = [newVal GMTTimeIntervalSince1970];
                             newVal = [NSString stringWithFormat:@"%lf",_dateInterval];
                        }
                        
                        newVal = newVal? newVal : [newVal isKindOfClass:[NSString class]] ? @"" : [NSNull null];
                        [scheduledVisitDict setValue:newVal forKey:key];
                    }
                }
                
               
                //[scheduledVisitDict setValue:scheduledVisit.isAutoGenerated forKey:@"isAutoGenerated"];
                //[scheduledVisitDict setValue:[NSNumber numberWithInteger:1] forKey:@"isEditModeOn"];
                
                [scheduledVisitDict setValue:_isEditModeOn forKey:@"isEditModeOn"];
                
                NSTimeInterval _modifiedDateInterval = [[NSDate date] GMTTimeIntervalSince1970];
                
                [scheduledVisitDict setValue:[NSString stringWithFormat:@"%lf",_modifiedDateInterval] forKey:@"modifiedDate"];
                
                [scheduledVisitDict setValue:scheduledVisit.customer?scheduledVisit.customer.isCustomerSentToServer:[NSNumber numberWithBool:NO] forKey:@"isCustomerSentToServer"];
                
                if(![scheduledVisitDict valueForKey:@"deviceID"])
                {
                    NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
                    [scheduledVisitDict setValue:deviceID forKey:@"deviceID"];
                }
                
                BOOL _isNWAvailable = [self isNetworkAvailable];
                [scheduledVisitDict setValue:[NSNumber numberWithBool:_isNWAvailable] forKey:@"isProcessing"];
            }
        }
    }
    
    if(isNewScheduledVisit)
    {
        NSDictionary *_scheduledVisitDict = nil;
        
    if(isDelete)
    {
        _scheduledVisitDict =[[NSMutableDictionary alloc]initWithObjectsAndKeys:scheduledVisit.scheduleVisitID,@"scheduleVisitID",[NSNumber numberWithInt:1],@"isDeleted", nil];
        [_scheduledVisitDict setValue:scheduledVisit.oldScheduleVisitID?scheduledVisit.oldScheduleVisitID:[NSNull null] forKey:@"oldScheduleVisitID"];
        [_scheduledVisitDict setValue:[NSNumber numberWithInteger:0] forKey:@"isEditModeOn"];
        [_scheduledVisitDict setValue:rep.repID forKey:@"repID"];
        
        [_scheduledVisitDict setValue:scheduledVisit.customer?scheduledVisit.customer.isCustomerSentToServer:[NSNumber numberWithBool:YES] forKey:@"isCustomerSentToServer"];
        
        NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
        [_scheduledVisitDict setValue:deviceID forKey:@"deviceID"];
        
        [self.scheduledVisits addObject:_scheduledVisitDict];
        [_scheduledVisitDict release];
        
        NSLog(@"ScheduledVisit:%@",_scheduledVisitDict);
    }
    else
    {
        NSTimeInterval _startDateInterval = [scheduledVisit.visitStartDate GMTTimeIntervalSince1970];
        NSTimeInterval _endDateInterval   = [scheduledVisit.visitEndDate GMTTimeIntervalSince1970];
        
       //NSDictionary *scheduledVisitDict    = [scheduledVisit toDictionary];
        _scheduledVisitDict = [NSMutableDictionary dictionary];
        [_scheduledVisitDict setValue:[NSString stringWithFormat:@"%lf",_startDateInterval] forKey:@"visitStartDate"];
        [_scheduledVisitDict setValue:[NSString stringWithFormat:@"%lf",_endDateInterval] forKey:@"visitEndDate"];
    
        [_scheduledVisitDict setValue:scheduledVisit.scheduleVisitID?scheduledVisit.scheduleVisitID:[NSNull null] forKey:@"scheduleVisitID"];
        [_scheduledVisitDict setValue:scheduledVisit.oldScheduleVisitID?scheduledVisit.oldScheduleVisitID:[NSNull null] forKey:@"oldScheduleVisitID"];
        [_scheduledVisitDict setValue:scheduledVisit.eventIdentifier?scheduledVisit.eventIdentifier:@"" forKey:@"eventIdentifier"];
        [_scheduledVisitDict setValue:scheduledVisit.customer?scheduledVisit.customer.customerID : scheduledVisit.customerID?scheduledVisit.customerID:[NSNull null] forKey:@"customerID"];
        
        //[_scheduledVisitDict setValue:scheduledVisit.customer?scheduledVisit.customer.state:scheduledVisit.stateName forKey:@"stateName"];
         //[_scheduledVisitDict setValue:scheduledVisit.customer?scheduledVisit.customer.name:scheduledVisit.customerName forKey:@"customerName"];
         
        [_scheduledVisitDict setValue:scheduledVisit.isAutoGenerated forKey:@"isAutoGenerated"];
        [_scheduledVisitDict setValue:[NSNumber numberWithInt:0] forKey:@"isDeleted"];
        [_scheduledVisitDict setValue:[NSNumber numberWithInteger:isEditMode] forKey:@"isEditModeOn"];
        [_scheduledVisitDict setValue:rep.repID?rep.repID:[NSNull null] forKey:@"repID"];
        
         [_scheduledVisitDict setValue:scheduledVisit.customer?scheduledVisit.customer.isCustomerSentToServer:[NSNumber numberWithBool:NO] forKey:@"isCustomerSentToServer"];
        
        NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
        [_scheduledVisitDict setValue:deviceID forKey:@"deviceID"];
        
        NSTimeInterval _modifiedDateInterval = [[NSDate date] GMTTimeIntervalSince1970];
        
        [_scheduledVisitDict setValue:[NSString stringWithFormat:@"%lf",_modifiedDateInterval] forKey:@"modifiedDate"];
        
        [_scheduledVisitDict setValue:scheduledVisit.isVisitPublished?scheduledVisit.isVisitPublished:[NSNumber numberWithBool:NO] forKey:@"isVisitPublished"];
        
        NSLog(@"ScheduledVisit:%@",_scheduledVisitDict);
        [self.scheduledVisits addObject:_scheduledVisitDict];
        
        BOOL _isNWAvailable = [self isNetworkAvailable];
             [_scheduledVisitDict setValue:[NSNumber numberWithBool:_isNWAvailable] forKey:@"isProcessing"];
    }
  }
    
    [self sendscheduledVisitRequest];
}

-(void)addTask:(Task *)task willDelete:(BOOL)isDelete isEditMode:(BOOL)isEditMode
{
    if(!task) return;
    
    Rep *rep = [CoreDataHandler currentRep];
    
    BOOL isNewTask=YES;
    
    if([self.tasks count])
    {
        NSArray *fetchedArr = [self.tasks filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.taskID=%@",task.taskID]];
        
        if([fetchedArr count])
        {
            isNewTask =NO;
            
            NSMutableDictionary *taskDict = [fetchedArr objectAtIndex:0];
            
            if(isDelete) 
            {
                
                // CHECK WHEATHER ALREADY SENT FOR PROCESSING. IF YES UPDATE THE DICT TO TREAT AS DELETE DICT RATHER THAN REMOVING FROM OFFLINE STACK.
                
                 BOOL _isProcessing = [[taskDict valueForKey:@"isProcessing"] boolValue];
                
                if(_isProcessing)
                {
                    NSNumber *_taskID =   [[taskDict valueForKey:@"taskID"] copy];;
                    NSNumber *_repID = [[taskDict valueForKey:@"repID"] copy];
                    NSString *_deviceID =   [[taskDict valueForKey:@"deviceID"] copy];
                    
                    NSNumber *_oldTaskID =   [[taskDict valueForKey:@"oldTaskID"] copy];
                    
                    [taskDict removeAllObjects];
                    
                    [taskDict setValue:_taskID forKey:@"taskID"];
                    [taskDict setValue:_oldTaskID?_oldTaskID:[NSNull null] forKey:@"oldTaskID"];
                     [taskDict setValue:_repID forKey:@"repID"];
                    [taskDict setValue:_deviceID forKey:@"deviceID"];
                    
                    [taskDict setValue:[NSNumber numberWithInt:1] forKey:@"isDeleted"];
                    [taskDict setValue:[NSNumber numberWithInteger:0] forKey:@"isEditModeOn"];
                    [taskDict setValue:task.parentCustomer ? task.parentCustomer.isCustomerSentToServer : [NSNumber numberWithBool:YES] forKey:@"isCustomerSentToServer"];
                    
                    [_taskID release];
                    [_repID release];
                    [_deviceID release];
                    [_oldTaskID release];
            
                }
                else
                {
                   [self.tasks removeObject:taskDict];
                }
            }
            else
            {
                 NSNumber *_isEditModeOn = [taskDict valueForKey:@"isEditModeOn"];
                
                NSArray *keysToIgnore = [NSArray arrayWithObject:@"parentCustomer"];
                
                NSArray *allkeys=[taskDict allKeys];
                for(NSString *key in allkeys)
                {
                    if([keysToIgnore containsObject:key]) continue;
                    
                    SEL _selector = NSSelectorFromString(key);
                    
                    if(![task respondsToSelector:_selector])
                    {
                        continue;
                    }
                    
                    id oldVal = [taskDict valueForKey:key];
                    id newVal = [task valueForKey:key];
                    
                    if(![oldVal isEqual:newVal])
                    {
                        if([newVal isKindOfClass:[NSDate class]])
                        {
                            NSTimeInterval _dateInterval = [newVal GMTTimeIntervalSince1970];
                            newVal = [NSString stringWithFormat:@"%lf",_dateInterval];
                        }
                        
                        newVal = newVal? newVal : [newVal isKindOfClass:[NSString class]] ? @"" : [NSNull null];
                        [taskDict setValue:newVal forKey:key];
                    }
                }
                
                //[scheduledVisitDict setValue:scheduledVisit.isAutoGenerated forKey:@"isAutoGenerated"];
               // [taskDict setValue:[NSNumber numberWithInteger:1] forKey:@"isEditModeOn"];
                 [taskDict setValue:_isEditModeOn forKey:@"isEditModeOn"];
                
                NSTimeInterval _modifiedDateInterval = [[NSDate date] GMTTimeIntervalSince1970];
                
                [taskDict setValue:[NSString stringWithFormat:@"%lf",_modifiedDateInterval] forKey:@"modifiedDate"];
                
                [taskDict setValue:task.parentCustomer?task.parentCustomer.isCustomerSentToServer:[NSNumber numberWithBool:YES] forKey:@"isCustomerSentToServer"];
                
                if(![taskDict valueForKey:@"deviceID"])
                {
                NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
                [taskDict setValue:deviceID forKey:@"deviceID"];
                }
                
                BOOL _isNWAvailable = [self isNetworkAvailable];
                [taskDict setValue:[NSNumber numberWithBool:_isNWAvailable] forKey:@"isProcessing"];

            }
        }
    }
    
    if(isNewTask)
    {
        NSDictionary *_taskDict = nil;
        if(isDelete)
        {
            _taskDict = [NSMutableDictionary dictionary];
            
            
            [_taskDict setValue:task.taskID?task.taskID:[NSNull null] forKey:@"taskID"];
            [_taskDict setValue:task.oldTaskID?task.oldTaskID:[NSNull null] forKey:@"oldTaskID"];
            [_taskDict setValue:[NSNumber numberWithInt:1] forKey:@"isDeleted"];
            
            [_taskDict setValue:[NSNumber numberWithInteger:0] forKey:@"isEditModeOn"];
            [_taskDict setValue:rep.repID?rep.repID:[NSNull null] forKey:@"repID"];
            [_taskDict setValue:task.parentCustomer ? task.parentCustomer.isCustomerSentToServer : [NSNumber numberWithBool:YES] forKey:@"isCustomerSentToServer"];
            
            NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
            [_taskDict setValue:deviceID forKey:@"deviceID"];
            
            [self.tasks addObject:_taskDict];
         
            
            NSLog(@"TaskDict:%@",_taskDict);
        }
        else
        {
            NSTimeInterval _dueDateInterval         =  task.dueDate ? [task.dueDate GMTTimeIntervalSince1970] : 0;
            NSTimeInterval _completedDateInterval   = task.isCompleted.boolValue ? [task.completedDate GMTTimeIntervalSince1970] : 0.0;
            
            //NSDictionary *scheduledVisitDict    = [scheduledVisit toDictionary];
            _taskDict = [NSMutableDictionary dictionary];
            
            [_taskDict setValue:(_dueDateInterval!=0)?[NSString stringWithFormat:@"%lf",_dueDateInterval]:@"" forKey:@"dueDate"];
         
                [_taskDict setValue:(_completedDateInterval!=0)?[NSString stringWithFormat:@"%lf",_completedDateInterval]:@"" forKey:@"completedDate"];
            
            [_taskDict setValue:task.taskID?task.taskID:[NSNull null] forKey:@"taskID"];
            [_taskDict setValue:task.oldTaskID?task.oldTaskID:[NSNull null] forKey:@"oldTaskID"];
            
            NSNumber *custID = task.parentCustomer.customerID?task.parentCustomer.customerID : task.customerID?task.customerID:nil;
            
            [_taskDict setValue:custID?custID:[NSNull null] forKey:@"customerID"];
            [_taskDict setValue:task.subject?task.subject:@"" forKey:@"subject"];
            [_taskDict setValue:task.notes.length?task.notes:@"" forKey:@"notes"];
            [_taskDict setValue:task.eventIdentifier?task.eventIdentifier:@"" forKey:@"eventIdentifier"];
            [_taskDict setValue:task.isCompleted forKey:@"isCompleted"];
            [_taskDict setValue:task.isStarred forKey:@"isStarred"];
            
            //[_scheduledVisitDict setValue:scheduledVisit.customer?scheduledVisit.customer.name:scheduledVisit.customerName forKey:@"customerName"];
            
            [_taskDict setValue:[NSNumber numberWithInt:0] forKey:@"isDeleted"];
            [_taskDict setValue:[NSNumber numberWithInteger:isEditMode] forKey:@"isEditModeOn"];
            [_taskDict setValue:rep.repID?rep.repID:[NSNull null] forKey:@"repID"];
        
               
            [_taskDict setValue:task.parentCustomer?task.parentCustomer.isCustomerSentToServer:[NSNumber numberWithBool:YES] forKey:@"isCustomerSentToServer"];
            
            NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
            [_taskDict setValue:deviceID forKey:@"deviceID"];
            
            NSTimeInterval _modifiedDateInterval = [[NSDate date] GMTTimeIntervalSince1970];
            [_taskDict setValue:[NSString stringWithFormat:@"%lf",_modifiedDateInterval] forKey:@"modifiedDate"];
            
            BOOL _isNWAvailable = [self isNetworkAvailable];
            [_taskDict setValue:[NSNumber numberWithBool:_isNWAvailable] forKey:@"isProcessing"];
            
            //SEND YES IF NO CUSTOMER IS ATTACHED TO IT
            
            NSLog(@"TaskDict:%@",_taskDict);
            [self.tasks addObject:_taskDict];
        }
    }
    
    [self sendTaskRequest];
}

-(void)addViewedNews:(News *)news
{
    if(!self.newsViewedDict) newsViewedDict = [[NSMutableDictionary alloc] init];
    
    BOOL _isNew = YES;
    
    NSMutableArray *newsViewedArr = [newsViewedDict objectForKey:@"details"];
    if([newsViewedArr count])
    {
        NSArray *fetchedArr = [newsViewedArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.newsID=%@",news.newsID]];
        
        if([fetchedArr count])
        {
            _isNew = NO;
            
            NSMutableDictionary *_fetchedDict =  [fetchedArr objectAtIndex:0];
            NSNumber *noOfViewedNum    =  [_fetchedDict valueForKey:@"appViewed"];
            
            long long _noOfViewed = [noOfViewedNum longLongValue];
            
             _noOfViewed++;
            noOfViewedNum = [NSNumber numberWithLongLong:_noOfViewed];
            //news.appViewed = noOfViewedNum;
            [_fetchedDict setValue:noOfViewedNum forKey:@"appViewed"];
        }
    }
    else 
    {
        newsViewedArr = [NSMutableArray array];
        [newsViewedDict setObject:newsViewedArr forKey:@"details"];
    }
    
    
    //IF NEW VIEWED
    if(_isNew)
    {
      long long _noOfViewed = 1;
      NSNumber *noOfViewedNum = [NSNumber numberWithLongLong:_noOfViewed];
     // news.appViewed = noOfViewedNum;
    
       NSMutableDictionary *viewedDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:news.newsID,@"newsID",noOfViewedNum,@"appViewed",nil];
       [newsViewedArr addObject:viewedDict];
    }
    
    [self sendNewsViewedRequest];
}

-(void)sendNewsViewedRequest
{
     [CoreDataHandler setOfflineArray:self.newsViewedDict forOperationKey:NEWS_VIEWED_METHOD];
    
    if(!self.newsViewedDict.count) return;
    
    Rep *_rep = [CoreDataHandler currentRep];
    [newsViewedDict setObject:_rep.repID forKey:@"repID"];
    [newsViewedDict setObject:_rep.parentWdSite.wdSiteID forKey:@"wdSiteID"];
    
    NSString *json = [self.newsViewedDict JSONRepresentation];
    NSLog(@"json:%@",json);
    
 __block OfflineManager *weakSelf = self;
 __block  ASIHTTPRequest *request = [ConnectionManager createNewsViewedConnectionWithJson:json];
    
    [request setCompletionBlock:^{
        
        NSLog(@"Response=%@",[request responseString]);
        
          NSDictionary *respone = [request responseJSON];
        
          NSNumber *status = [respone valueForKey:@"status"];
        
        if(status && [status intValue]==1)
        {
           [weakSelf.newsViewedDict removeAllObjects];
            
            [CoreDataHandler setOfflineArray:weakSelf.newsViewedDict forOperationKey:NEWS_VIEWED_METHOD];
            [[SSCoreDataManager sharedManager] save:nil];
        }
    }];

}

-(void)addViewedOffer:(Offer *)offer
{
    if(!self.offersViewedDict) offersViewedDict = [[NSMutableDictionary alloc] init];
    
    BOOL _isNew = YES;
    
    NSMutableArray *offersViewedArr = [offersViewedDict valueForKey:@"details"];
    
    if([offersViewedArr count])
    {
        NSArray *fetchedArr = [offersViewedArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.offerID=%@",offer.offerID]];
        
        if([fetchedArr count])
        {
            _isNew = NO;
            
            NSMutableDictionary *_fetchedDict =  [fetchedArr objectAtIndex:0];
            NSNumber *noOfViewedNum    =  [_fetchedDict valueForKey:@"appViewed"];
            
            long long _noOfViewed = [noOfViewedNum longLongValue];
            
            _noOfViewed++;
            noOfViewedNum = [NSNumber numberWithLongLong:_noOfViewed];
           // offer.appViewed = noOfViewedNum;
            [_fetchedDict setValue:noOfViewedNum forKey:@"appViewed"];
        }
    }
    else 
    {
        offersViewedArr = [NSMutableArray array];
        [offersViewedDict setObject:offersViewedArr forKey:@"details"];
    }
   
    
    //IF NEW VIEWED
    if(_isNew)
    {
        long long _noOfViewed = 1;
        NSNumber *noOfViewedNum = [NSNumber numberWithLongLong:_noOfViewed];
       // offer.appViewed = noOfViewedNum;
        
        NSMutableDictionary *viewedDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:offer.offerID,@"offerID",noOfViewedNum,@"appViewed",nil];
        [offersViewedArr addObject:viewedDict];
    }
    
    [self sendOffersViewedRequest];
}

-(void)sendOffersViewedRequest
{
      [CoreDataHandler setOfflineArray:self.offersViewedDict forOperationKey:OFFER_VIEWED_METHOD];
    
    if(!self.offersViewedDict.count) return;
    
    NSString *json = [self.offersViewedDict JSONRepresentation];
    NSLog(@"json:%@",json);
    
__block OfflineManager *weakSelf = self;
    
  __block  ASIHTTPRequest *request = [ConnectionManager createOfferViewedConnectionWithJson:json];
    
    [request setCompletionBlock:^{
        
        NSDictionary *respone = [request responseJSON];
        
        NSLog(@"%@",respone);
        
        NSNumber *status = [respone valueForKey:@"status"];
        
        if(status && [status intValue]==1)
        {
          [weakSelf.offersViewedDict removeAllObjects];
          [CoreDataHandler setOfflineArray:weakSelf.offersViewedDict forOperationKey:OFFER_VIEWED_METHOD];
        }
    }];
    
}

/*
-(void)sendscheduledVisitRequest
{
    if(!self.scheduledVisits.count) return;
    
      if(![self isNetworkAvailable]) return;
    
    NSString *json = [self.scheduledVisits JSONRepresentation];
    NSLog(@"json:%@",json);
    
    ASIHTTPRequest *request = [ConnectionManager createNewscheduledVisitConnectionWithJson:json];
    
    [request setCompletionBlock:^{
        
    //====================   PROCESS CUSTOMER DATA HERE  ===================
        
    if(self.scheduledVisits.count)
    {
        NSArray *respone = [request responseJSON];
        
        NSLog(@" schedule visit response:%@",[request responseString]);
        
        NSLog(@" my response:%@",respone);
    
        if([respone isKindOfClass:[NSArray class]])
        {
                        
        //if(!oldIDs) oldIDs = [NSArray array];
       // NSArray *actualIDs = [respone valueForKey:@"actualID"];
        
        //NSPredicate *fetchPredicate = [NSPredicate predicateWithFormat:@"self.scheduleVisitID IN %@",oldIDs];
        //NSMutableArray *fetchedArr = [ScheduledVisit fetchWithPredicate:fetchPredicate onAttributes:[NSArray arrayWithObject:@"scheduleVisitID"] sortDescriptors:nil limit:0 error:nil];
        
        
        // ======================= NEED TO BE REFRACTORED ====================
        
            for(NSDictionary *dict in respone)
            {
                NSNumber *localID   =   [dict valueForKey:@"oldScheduleVisitID"];
                NSNumber *actualID  =   [dict valueForKey:@"scheduleVisitID"];
                NSNumber *isDeleted =   [dict valueForKey:@"isDeleted"];
                NSNumber *isEdited  =   [dict valueForKey:@"isEditModeOn"];
                
                 NSNumber *status =  [dict valueForKey:@"status"];
                
                if([isDeleted boolValue] || [isEdited boolValue]) continue;
                
              //  if(status && [status intValue]==0) continue;
                
                NSPredicate *fetchPredicate = NULL_NIL(localID)? [NSPredicate predicateWithFormat:@"self.scheduleVisitID = %@",localID]:nil;
                NSMutableArray *fetchedArr = fetchPredicate ? [ScheduledVisit fetchWithPredicate:fetchPredicate onAttributes:[NSArray arrayWithObject:@"scheduleVisitID"] sortDescriptors:nil limit:1 error:nil] : nil;
                
                // UPDATE scheduleVisitID FOR INSERTION ONLY
                for(ScheduledVisit *_scheduledVisit in fetchedArr)
                {
                    if(actualID && (localID && [_scheduledVisit.scheduleVisitID isEqualToNumber:localID]))
                    {
                        _scheduledVisit.scheduleVisitID = NULL_NIL(actualID);
                        break;
                    }
                }
            }
            
            }
            //===================== REMOVE CUTOMER STACK ===================================
            [self.scheduledVisits removeAllObjects];
            [[SSCoreDataManager sharedManager] save:nil];
    
        // =========================== NEED TO BE REFRACTORED =========================
        }
       
    }];
    
    [request setFailedBlock:^{
        
    }];
}
*/

-(void)sendscheduledVisitRequest
{
    [CoreDataHandler setOfflineArray:self.scheduledVisits forOperationKey:EDIT_UPCOMMING_VISITS_METHOD];
    
    if(!self.scheduledVisits.count) return;
    
    if(![self isNetworkAvailable]) return;
    
    NSString *json = [self.scheduledVisits JSONRepresentation];
    NSLog(@"json:%@",json);
    
 __block OfflineManager *weakSelf = self;
 __block ASIHTTPRequest *request = [ConnectionManager createNewscheduledVisitConnectionWithJson:json];
    
    [request setCompletionBlock:^{
        
        //====================   PROCESS CUSTOMER DATA HERE  ===================
        OfflineManager *strongSelf = weakSelf;
        
        if(strongSelf.scheduledVisits.count)
        {
           __block BOOL isAnyFalied = NO;
            
             NSArray *respone = [request responseJSON];
            
            NSLog(@" schedule visit response:%@",[request responseString]);
            
            NSLog(@" my response:%@",respone);
            
            if([respone isKindOfClass:[NSDictionary class]])
            {
                NSDictionary * _respone =(NSDictionary *)respone;
                
                NSNumber *isUnderMaintenance  = [_respone objectForKey:JSON_UNDER_MAINTENANCE_KEY];
                isUnderMaintenance = NULL_NIL(isUnderMaintenance);
                
                if([isUnderMaintenance boolValue])
                {
                    [app showUnderMaintainenceScreen:YES];
                }
                
            }
            else
            {

            NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
                context.parentContext = DEFAULT_CONTEXT;
                
            [context performBlock:^{
                
           
            if([respone isKindOfClass:[NSArray class]])
            {
                NSMutableArray *processedVisits = [[NSMutableArray alloc] init];
                
                for(NSDictionary *dict in respone)
                {
                    NSNumber *localID   =   [dict valueForKey:@"oldScheduleVisitID"];
                    NSNumber *actualID  =   [dict valueForKey:@"scheduleVisitID"];
                    NSNumber *isDeleted =   [dict valueForKey:@"isDeleted"];
                    NSNumber *isEdited  =   [dict valueForKey:@"isEditModeOn"];
                    NSNumber *status    =   [dict valueForKey:@"status"];
                    
                    isDeleted = NULL_NIL(isDeleted);
                    isEdited  = NULL_NIL(isEdited);
                    
                    localID   = NULL_NIL(localID);
                    actualID  = NULL_NIL(actualID);
                    
                    status    = NULL_NIL(status);
                    
                    
                      if(status && [status intValue]==0) 
                      {
                          if(!isAnyFalied) isAnyFalied = YES;
                          continue;
                      }
                      else if(localID || actualID)
                      {
                          [processedVisits addObject:localID?localID:actualID];
                      }
                    
                     if([isDeleted boolValue] || [isEdited boolValue]) continue;
                    
                    NSPredicate *fetchPredicate = NULL_NIL(localID)? [NSPredicate predicateWithFormat:@"self.scheduleVisitID = %@",localID]:nil;
                    
                    NSMutableArray *fetchedArr = fetchPredicate ? [ScheduledVisit fetchOnContext:context predicate:fetchPredicate onAttributes:nil sortDescriptors:nil limit:1 error:nil] : nil;
                    
                    // UPDATE scheduleVisitID FOR INSERTION ONLY
                    for(ScheduledVisit *_scheduledVisit in fetchedArr)
                    {
                        if(actualID && (localID && [_scheduledVisit.scheduleVisitID isEqualToNumber:localID]))
                        {
                            _scheduledVisit.scheduleVisitID = actualID;
                            _scheduledVisit.isScheduleVisitSentToServer  = [NSNumber numberWithBool:YES];
                            break;
                        }
                    }
                }
                
                if([processedVisits count])
                {
                    //DELETE ONLY PROCESSED OBJECTS
                    NSLog(@"BEFORE  Count =%d",[strongSelf.scheduledVisits count]);
                    
                    [strongSelf.scheduledVisits filterUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (self.scheduleVisitID IN %@)",processedVisits]];
                    
                    NSLog(@"AFTER  Count =%d",[strongSelf.scheduledVisits count]);
                    
                     [CoreDataHandler setOfflineArray:strongSelf.scheduledVisits forOperationKey:EDIT_UPCOMMING_VISITS_METHOD inContext:context];
                }
                
                [processedVisits release];
                
            }
            //===================== REMOVE CUTOMER STACK ===================================
                
                   /*
                    if(!isAnyFalied)
                    {
                      [strongSelf.scheduledVisits removeAllObjects];
                        
                      [CoreDataHandler setOfflineArray:strongSelf.scheduledVisits forOperationKey:EDIT_UPCOMMING_VISITS_METHOD inContext:context];
                    }
                   */
                
            
                    if([context hasChanges] && [context save:nil])
                    {
                        NSManagedObjectContext *parentContext = DEFAULT_CONTEXT;
                        [parentContext performBlockAndWait:^{
                            
                            if([parentContext hasChanges] && ![parentContext save:nil]){}
                        }];
                        
                        /*
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                        [[SSCoreDataManager sharedManager] save:nil];
                            
                        });
                         */
                    }
                
                    [SSCoreDataManager destroyBackgroundContext:context];

                
            }];
                
                
            }
        }
        
    }];
    
    [request setFailedBlock:^{
        
    }];
}



-(void)destroyBackgroundContext:(NSManagedObjectContext *)context
{
      [SSCoreDataManager destroyBackgroundContext:context];
}

-(void)sendTaskRequest
{
     [CoreDataHandler setOfflineArray:self.tasks forOperationKey:CREATE_TASK_METHOD];
    
    if(!self.tasks.count) return;
    
    if(![self isNetworkAvailable]) return;
    
    NSString *json = [self.tasks JSONRepresentation];
   
    NSLog(@"json:%@",json);
    
    __block OfflineManager *weakSelf = self;
    
   __block ASIHTTPRequest *request = [ConnectionManager createNewTaskConnectionWithTaskJson:json];
    
    /*
    [self.tasks enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
    {
        NSDictionary *taskDict = (NSDictionary *)obj;
        [taskDict setValue:[NSNumber numberWithInt:1] forKey:@"syncStatus"];
    }];
    */
    
    [request setCompletionBlock:^{
        
        OfflineManager *strongSelf = weakSelf;
        
    //====================   PROCESS CUSTOMER DATA HERE  ===================
        
        if(strongSelf.tasks.count)
        {
            NSArray *respone = [request responseJSON];
            
            NSLog(@" Task response:%@",[request responseString]);
            
            NSLog(@" my response:%@",respone);
            
            if([respone isKindOfClass:[NSDictionary class]])
            {
               NSDictionary * _respone =(NSDictionary *)respone;
                
               NSNumber *isUnderMaintenance  = [_respone objectForKey:JSON_UNDER_MAINTENANCE_KEY];
               isUnderMaintenance = NULL_NIL(isUnderMaintenance);

              if([isUnderMaintenance boolValue])
              {
                 [app showUnderMaintainenceScreen:YES];
              }

            }
            else
            {
            
            __block BOOL isAnyFalied=NO;
            //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
          //  NSManagedObjectContext *context = [SSCoreDataManager backgroundContext];
                
            NSManagedObjectContext *context=[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType] ;
            context.parentContext = DEFAULT_CONTEXT;
                       
    
            [context performBlock:^{
                
            
            if([respone isKindOfClass:[NSArray class]])
            {
                NSMutableArray *processedTasks = [[NSMutableArray alloc] init];
                
                // ======================= NEED TO BE REFRACTORED ====================
                for(NSDictionary *dict in respone)
                {
                    NSNumber *localID   =   [dict valueForKey:@"oldTaskID"];
                    NSNumber *actualID  =   [dict valueForKey:@"taskID"];
                    NSNumber *isDeleted =   [dict valueForKey:@"isDeleted"];
                    NSNumber *isEdited  =   [dict valueForKey:@"isEditModeOn"];
                    NSNumber *status    =   [dict valueForKey:@"status"];
                    
                    localID   =  NULL_NIL(localID);
                    actualID  =  NULL_NIL(actualID);
                    isDeleted =  NULL_NIL(isDeleted);
                    isEdited  =  NULL_NIL(isEdited);
                    status    =  NULL_NIL(status);
                    
                     if(status && [status intValue]==0)
                     {
                         if(!isAnyFalied) isAnyFalied = YES;
                         continue;
                     }
                    else if(localID || actualID)
                    {
                        [processedTasks addObject:localID?localID:actualID];
                    }
                    
                    if([isDeleted boolValue] || [isEdited boolValue]) continue;
                    
                    NSPredicate *fetchPredicate = localID ? [NSPredicate predicateWithFormat:@"self.taskID = %@",localID] : nil;
                    
                    NSMutableArray *fetchedArr = fetchPredicate ? [Task fetchOnContext:context predicate:fetchPredicate onAttributes:nil sortDescriptors:nil limit:1 error:nil] : nil;
                    
                    
                    // UPDATE scheduleVisitID FOR INSERTION ONLY
                    for(Task *_task in fetchedArr)
                    {
                        if(actualID && (localID && [_task.taskID isEqualToNumber:localID]))
                        {
                            _task.taskID = actualID;
                            break;
                        }
                    }
                }
                
                
                if([processedTasks count])
                {
                    //DELETE ONLY PROCESSED OBJECTS
                    NSLog(@"BEFORE Tasks Count =%d",[strongSelf.tasks count]);
                    
                    [strongSelf.tasks filterUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (self.taskID IN %@)",processedTasks]];
                    
                    NSLog(@"AFTER Tasks Count =%d",[strongSelf.tasks count]);
                    
                    [CoreDataHandler setOfflineArray:strongSelf.tasks forOperationKey:CREATE_TASK_METHOD inContext:context];
                }
                [processedTasks release];
                
            }
            //===================== REMOVE CUTOMER STACK ===================================
                
                //dispatch_async(dispatch_get_main_queue(), ^{
                
                    /*
                    if(!isAnyFalied)
                    {
                       [strongSelf.tasks removeAllObjects];
                       [CoreDataHandler setOfflineArray:strongSelf.tasks forOperationKey:CREATE_TASK_METHOD inContext:context];
                    }
                    */
                
                
                
                       // [[SSCoreDataManager sharedManager] saveContext:context error:nil];
                    
                        if([context hasChanges] && [context save:nil])
                        {
                            
                            NSManagedObjectContext *parentContext = DEFAULT_CONTEXT;
                            [parentContext performBlockAndWait:^{
                                
                                if([parentContext hasChanges] && ![parentContext save:nil]){}
                            }];
                            
                            /*
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [[SSCoreDataManager sharedManager] save:nil];
                                
                            });
                            */
                        }
                    
                        [SSCoreDataManager destroyBackgroundContext:context];
                    
               // });
                
             
            }];
                
          //  });
            }
        }
    }];
    
    [request setFailedBlock:^{
        
    }];
}

-(void)addReminder:(Reminder *)reminder event:(EKReminder *)event willDelete:(BOOL)isDelete isEditMode:(BOOL)isEditMode
{
    if(!event) return;
    
    Rep *rep = [CoreDataHandler currentRep];
    
    BOOL isNewReminder=YES;
    
    if([self.reminders count])
    {
        NSArray *fetchedArr = [self.reminders filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self.reminderID=%@",reminder.reminderID]];
        
        if([fetchedArr count])
        {
            isNewReminder =NO;
            
            NSMutableDictionary *reminderDict = [fetchedArr objectAtIndex:0];
            
            if(isDelete)
            {
                // CHECK WHEATHER ALREADY SENT FOR PROCESSING. IF YES UPDATE THE DICT TO TREAT AS DELETE DICT RATHER THAN REMOVING FROM OFFLINE STACK.
                
                BOOL _isProcessing = [[reminderDict valueForKey:@"isProcessing"] boolValue];
                
                if(_isProcessing)
                {
                    NSNumber *_reminderID =   [[reminderDict valueForKey:@"reminderID"] copy];;
                    NSNumber *_repID = [[reminderDict valueForKey:@"repID"] copy];
                    NSString *_deviceID =   [[reminderDict valueForKey:@"deviceID"] copy];
                    
                    [reminderDict removeAllObjects];
                    
                    [reminderDict setValue:_reminderID forKey:@"reminderID"];
                    [reminderDict setValue:_repID forKey:@"repID"];
                    [reminderDict setValue:_deviceID forKey:@"deviceID"];
                    
                    [reminderDict setValue:[NSNumber numberWithInt:1] forKey:@"isDeleted"];
                    [reminderDict setValue:[NSNumber numberWithInteger:0] forKey:@"isEditModeOn"];
                    
                    
                    [_reminderID release];
                    [_repID release];
                    [_deviceID release];
                    
                }
                else
                {
                  [self.reminders removeObject:reminderDict];
                }
            }
            else
            {
                NSNumber *_isEditModeOn = [reminderDict valueForKey:@"isEditModeOn"];
                
                NSArray *allkeys = [reminderDict allKeys];
                
                for(NSString *key in allkeys)
                {
                    SEL _selector = NSSelectorFromString(key);
                    
                    if(![event respondsToSelector:_selector])
                    {
                        continue;
                    }
                    
                    id oldVal = [reminderDict valueForKey:key];
                    id newVal =   [event valueForKey:key];
                    
                    if([key isEqualToString:@"alarms"])
                    {
                        NSArray *alarms = [ADDReminder dictValueForAlarms:newVal];
                        newVal = [alarms JSONRepresentation];
                    }
                    
                    /*
                     if([key isEqualToString:@"recurrenceRule"])
                     {
                     NSDictionary *recurrenceDict = [ADDReminder dictValueForRecurrence:newVal];
                     newVal = [recurrenceDict JSONRepresentation];
                     }
                     */
                    
                    if(![oldVal isEqual:newVal])
                    {
                        if([newVal isKindOfClass:[NSDate class]])
                        {
                            NSTimeInterval _dateInterval = [newVal GMTTimeIntervalSince1970];
                            newVal = [NSString stringWithFormat:@"%lf",_dateInterval];
                        }
                        
                        newVal = newVal? newVal : [newVal isKindOfClass:[NSString class]] ? @"" : [NSNull null];
                        [reminderDict setValue:newVal forKey:key];
                    }
                }
                
                //[scheduledVisitDict setValue:scheduledVisit.isAutoGenerated forKey:@"isAutoGenerated"];
                // [reminderDict setValue:[NSNumber numberWithInteger:1] forKey:@"isEditModeOn"];
                [reminderDict setValue:_isEditModeOn forKey:@"isEditModeOn"];
                
                NSTimeInterval _modifiedDateInterval = [[NSDate date] GMTTimeIntervalSince1970];
                
                [reminderDict setValue:[NSString stringWithFormat:@"%lf",_modifiedDateInterval] forKey:@"modifiedDate"];
                
                BOOL _isNWAvailable = [self isNetworkAvailable];
                [reminderDict setValue:[NSNumber numberWithBool:_isNWAvailable] forKey:@"isProcessing"];
            }
        }
    }
    
    if(isNewReminder)
    {
        NSDictionary *reminderDict = nil;
        if(isDelete)
        {
            reminderDict =[NSMutableDictionary dictionary];
            [reminderDict setValue:reminder.reminderID forKey:@"reminderID"];
            [reminderDict setValue:[NSNumber numberWithInteger:1] forKey:@"isDeleted"];
            [reminderDict setValue:[NSNumber numberWithInteger:0] forKey:@"isEditModeOn"];
            [reminderDict setValue:rep.repID forKey:@"repID"];
            
            NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
            [reminderDict setValue:deviceID forKey:@"deviceID"];
            
            [self.reminders addObject:reminderDict];
            
            NSLog(@"Reminder:%@",reminderDict);
        }
        else
        {
            NSTimeInterval _dueDateInterval    = [reminder.startDate GMTTimeIntervalSince1970];
            NSTimeInterval _endDateInterval    = [reminder.endDate GMTTimeIntervalSince1970];
            NSMutableDictionary *_reminderDict = [NSMutableDictionary dictionary];
            
            [_reminderDict setValue:reminder.title?reminder.title:@"" forKey:@"title"];
            [_reminderDict setValue:reminder.notes?reminder.notes:@"" forKey:@"notes"];
            [_reminderDict setValue:reminder.location?reminder.location:@"" forKey:@"location"];
            [_reminderDict setValue:reminder.recurrenceRule?reminder.recurrenceRule:@"" forKey:@"recurrenceRule"];
            [_reminderDict setValue:reminder.alarms?reminder.alarms:@"" forKey:@"alarms"];
            [_reminderDict setValue:[NSString stringWithFormat:@"%lf",_dueDateInterval] forKey:@"startDate"];
            [_reminderDict setValue:[NSString stringWithFormat:@"%lf",_endDateInterval] forKey:@"endDate"];
            [_reminderDict setValue:[NSNumber numberWithInt:0] forKey:@"isDeleted"];
            [_reminderDict setValue:[NSNumber numberWithInteger:isEditMode] forKey:@"isEditModeOn"];
            [_reminderDict setValue:rep.repID forKey:@"repID"];
            [_reminderDict setValue:reminder.reminderID forKey:@"reminderID"];
           
            [_reminderDict setValue:reminder.isAllDay forKey:@"isAllDay"];
            [_reminderDict setValue:reminder.eventIdentifier?reminder.eventIdentifier:@"" forKey:@"eventIdentifier"];
            
            NSString *deviceID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
            [_reminderDict setValue:deviceID forKey:@"deviceID"];
            
            NSTimeInterval _modifiedDateInterval = [[NSDate date] GMTTimeIntervalSince1970];
            [_reminderDict setValue:[NSString stringWithFormat:@"%lf",_modifiedDateInterval] forKey:@"modifiedDate"];
            
            BOOL _isNWAvailable = [self isNetworkAvailable];
            [_reminderDict setValue:[NSNumber numberWithBool:_isNWAvailable] forKey:@"isProcessing"];
            
            NSLog(@"Reminder:%@",_reminderDict);
            [self.reminders addObject:_reminderDict];
        }
    }
    
    [self sendReminderRequest];
}



-(void)sendReminderRequest
{
       [CoreDataHandler setOfflineArray:self.reminders forOperationKey:CREATE_REMINDER_METHOD];
    
    if(!self.reminders.count) return;
    
    if(![self isNetworkAvailable]) return;
    
    NSString *json = [self.reminders JSONRepresentation];
    NSLog(@"json:%@",json);
    
  __block OfflineManager *weakSelf = self;
  __block  ASIHTTPRequest *request = [ConnectionManager createRemindersWithJson:json];
    
    [request setCompletionBlock:^{
        
        //====================   PROCESS CUSTOMER DATA HERE  ===================
        OfflineManager *strongSelf = weakSelf;
        
        NSArray *respone = [request responseJSON];
        NSLog(@" Reminder  response:%@",[request responseString]);
        NSLog(@" my response:%@",respone);
        
        if([respone isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * _respone =(NSDictionary *)respone;
            
            NSNumber *isUnderMaintenance  = [_respone objectForKey:JSON_UNDER_MAINTENANCE_KEY];
            isUnderMaintenance = NULL_NIL(isUnderMaintenance);
            
            if([isUnderMaintenance boolValue])
            {
                [app showUnderMaintainenceScreen:YES];
            }
            
        }
        else{

        
        if([weakSelf.reminders count])
        {
            __block  BOOL  isFailed = NO;
            
           // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
               // NSManagedObjectContext *context = [SSCoreDataManager backgroundContext];
            
            NSManagedObjectContext *context=[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType] ;
            context.parentContext = DEFAULT_CONTEXT;

            [context performBlock:^{
                
                if([respone isKindOfClass:[NSArray class]])
                {
                    NSMutableArray *processed = [[NSMutableArray alloc] init];
                    
                    for(NSDictionary *dict in respone)
                    {
                        NSNumber *localID   =   [dict valueForKey:@"oldReminderID"];
                        NSNumber *actualID  =   [dict valueForKey:@"reminderID"];
                        NSNumber *isDeleted =   [dict valueForKey:@"isDeleted"];
                        NSNumber *isEdited  =   [dict valueForKey:@"isEditModeOn"];
                        
                        NSNumber *status =  [dict valueForKey:@"status"];
                        
                        localID   = NULL_NIL(localID);
                        actualID  = NULL_NIL(actualID);
                        isDeleted = NULL_NIL(isDeleted);
                        isEdited  = NULL_NIL(isEdited);
                        status    = NULL_NIL(status);
                        
                        if(status && [status intValue]==0)
                        {
                            if(!isFailed) isFailed = YES;
                            
                            continue;
                        }
                        else if(localID || actualID)
                        {
                            [processed addObject:localID?localID:actualID];
                        }
                        
                        if([isDeleted boolValue] || [isEdited boolValue])
                        {
                            continue;
                        }
                        
                        NSPredicate *fetchPredicate = localID ? [NSPredicate predicateWithFormat:@"self.reminderID = %@",localID] : nil;
                        
                        NSMutableArray *fetchedArr = fetchPredicate ? [Reminder fetchWithPredicate:fetchPredicate onAttributes:nil sortDescriptors:nil limit:1 error:nil] : nil;
                        
                        //========= UPDATE  FOR INSERTION ONLY
                        for(Reminder *_reminder in fetchedArr)
                        {
                            if((localID && actualID) && [_reminder.reminderID isEqualToNumber:localID])
                            {
                                _reminder.reminderID = actualID;
                                
                                NSLog(@"Reminder ID=%@",_reminder.reminderID);

                                break;
                            }
                        }
                    }
                    
                    if([processed count])
                    {
                        //DELETE ONLY PROCESSED OBJECTS
                        NSLog(@"BEFORE Tasks Count =%d",[strongSelf.reminders count]);
                        
                        [strongSelf.reminders filterUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (self.reminderID IN %@)",processed]];
                        
                        NSLog(@"AFTER Tasks Count =%d",[strongSelf.reminders count]);
                        
                         [CoreDataHandler setOfflineArray:strongSelf.reminders forOperationKey:CREATE_REMINDER_METHOD inContext:context];
                    }
                    
                    [processed release];
                }
                
                //======= REMOVE CUTOMER STACK =============
                //dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if(!isFailed)
                    {
                          /*
                          [strongSelf.reminders removeAllObjects];
                          [CoreDataHandler setOfflineArray:strongSelf.reminders forOperationKey:CREATE_REMINDER_METHOD inContext:context];
                          */
                    }
                
                    
                    if([context hasChanges] && [context save:nil])
                    {
                        NSManagedObjectContext *parentContext = DEFAULT_CONTEXT;
                        [parentContext performBlockAndWait:^{
                            
                            if([parentContext hasChanges] && ![parentContext save:nil]){}
                        }];
                        
                        /*
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [[SSCoreDataManager sharedManager] save:nil];
                            
                        });
                        */
                    }
                    
                    [SSCoreDataManager destroyBackgroundContext:context];
              //  });
                

           // });
            
            }];
        }
            
        }
    }];
    
    [request setFailedBlock:^{
        
    }];
}

-(void)removeAllCustomers
{
    [self.customers removeAllObjects];
}

-(void)removeAllScheduledVisits
{
    [self.scheduledVisits removeAllObjects];
}


-(void)clearOfflineQueue
{
    [self.customers removeAllObjects];
    [self.scheduledVisits removeAllObjects];
    [self.tasks removeAllObjects];
    [self.reminders removeAllObjects];
    [self.newsViewedDict removeAllObjects];
    [self.offersViewedDict removeAllObjects];
}

/*
- (id)copyWithZone:(NSZone *)zone
{ 
    return self; 
} 

- (id)retain 
{ 
    return self; 
} 

- (NSUInteger)retainCount 
{ 
    NSAssert1(1==0, @"SynthesizeSingleton: %@ ERROR: -(NSUInteger)retainCount method did not get swizzled.", self); 
    return NSUIntegerMax; 
} 


-(void)release 
{       
    NSAssert1(1==0, @"SynthesizeSingleton: %@ ERROR: -(void)release method did not get swizzled.", self); 
} 

- (id)autorelease 
{ 
    NSAssert1(1==0, @"SynthesizeSingleton: %@ ERROR: -(id)autorelease method did not get swizzled.", self); 
    return self; 
} 
*/



@end
