//
//  ConnectionManager.h
//  ARonFBTest
//
//  Created by RANDEM MAC on 03/05/12.
//  Copyright (c) 2012 RANDEM IT. All rights reserved.


#import <Foundation/Foundation.h>
#import "ASIHTTPRequest+JSON.h"
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>

//http://localhost/AutoProfiler/Resource/Offer/Pdf/qshfWd.pdf


// ======== REMEMBER : DONT INCLUDE HTTP:// HERE ==========
#if LOCAL

#define  SERVER_LINK                   @"192.168.1.143"
#define  DOWNLOAD_SERVER_LINK          @"192.168.1.143"
#define  OEM_WCF_URL                   @"AutoProfiler.Wcf/Service1.svc" //
//#define  OEM_WCF_URL                   @"AutoProfilerDemo.Wcf/Service1.svc"
#define  PDF_LINK                      @"AutoProfiler/Resource"
#define  IMAGE_LINK @"AutoProfiler/Resource"
#define  CUSTOMER_IMAGE_DOWNLOAD_LINK  [@"http://" stringByAppendingFormat:@"%@/%@/Customer/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]
#define  LOGO_IMAGE_URL                [@"http://" stringByAppendingFormat:@"%@/%@/Dealerlogo/ipadlogo/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]
#define  IMAGE_UPLOAD_LINK             [@"http://" stringByAppendingFormat:@"%@/Autoprofiler/uploader/fileupload.ashx",SERVER_LINK]

#define  RESOURCE_FILE_DOWNLOAD_LINK   [@"http://" stringByAppendingFormat:@"%@/%@/ResourceFile/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]

#elif LOCAL2

#define  SERVER_LINK                   @"192.168.1.142"
#define  DOWNLOAD_SERVER_LINK          @"192.168.1.142"
#define  OEM_WCF_URL                   @"AutoProfiler.Wcf/Service1.svc"
#define  PDF_LINK                      @"AutoProfiler/Resource"
#define  IMAGE_LINK @"AutoProfiler/Resource"
#define  CUSTOMER_IMAGE_DOWNLOAD_LINK  [@"http://" stringByAppendingFormat:@"%@/%@/Customer/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]
#define  LOGO_IMAGE_URL                [@"http://" stringByAppendingFormat:@"%@/%@/Dealerlogo/ipadlogo/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]
#define  IMAGE_UPLOAD_LINK             [@"http://" stringByAppendingFormat:@"%@/Autoprofiler/uploader/fileupload.ashx",SERVER_LINK]

#define  RESOURCE_FILE_DOWNLOAD_LINK   [@"http://" stringByAppendingFormat:@"%@/%@/ResourceFile/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]


#elif LIVE_TEST

//fieldforcetest.randemgroup.com.au
//#define  SERVER_LINK                   @"fieldforcetest.randemgroup.com.au"
//#define  DOWNLOAD_SERVER_LINK          @"edge.fieldforcetest.randemgroup.com.au"

#define  SERVER_LINK                   @"app.test.myfieldforce.com.au"
#define  DOWNLOAD_SERVER_LINK          @"edge.test.myfieldforce.com.au"
#define  OEM_WCF_URL                   @"Services/Service1.svc"
#define  PDF_LINK                      @"Resource"
#define  IMAGE_LINK @"Resource"

#define  CUSTOMER_IMAGE_DOWNLOAD_LINK [@"http://" stringByAppendingFormat:@"%@/%@/Customer/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]
#define  LOGO_IMAGE_URL               [@"http://" stringByAppendingFormat:@"%@/%@/Dealerlogo/ipadlogo/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]
#define  IMAGE_UPLOAD_LINK            [@"http://" stringByAppendingFormat:@"%@/uploader/fileupload.ashx",SERVER_LINK]

#define  RESOURCE_FILE_DOWNLOAD_LINK  [@"http://" stringByAppendingFormat:@"%@/%@/ResourceFile/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]

//#define  RESOURCE_FILE_DOWNLOAD_LINK [@"http://" stringByAppendingFormat:@"%@/%@/ResourceFile/",@"edge.test.myfieldforce.com.au",IMAGE_LINK]


#elif GENERIC_DEMO

//#define  SERVER_LINK                  @"fieldforcedemo.randemgroup.com.au"
//#define  DOWNLOAD_SERVER_LINK         @"edge.fieldforcedemo.randemgroup.com.au"

#define  SERVER_LINK                  @"app.demo.myfieldforce.com.au"
#define  DOWNLOAD_SERVER_LINK         @"edge.demo.myfieldforce.com.au"
#define  OEM_WCF_URL                  @"Services/Service1.svc" // @"AutoProfiler.Wcf/Service1.svc"
#define  PDF_LINK                     @"Resource"
#define  IMAGE_LINK                   @"Resource" //@"OEMIPAD/resource/"

#define  CUSTOMER_IMAGE_DOWNLOAD_LINK [@"http://" stringByAppendingFormat:@"%@/%@/Customer/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]
#define  LOGO_IMAGE_URL               [@"http://" stringByAppendingFormat:@"%@/%@/Dealerlogo/ipadlogo/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]
#define  IMAGE_UPLOAD_LINK            [@"http://" stringByAppendingFormat:@"%@/uploader/fileupload.ashx",SERVER_LINK]

#define  RESOURCE_FILE_DOWNLOAD_LINK  [@"http://" stringByAppendingFormat:@"%@/%@/ResourceFile/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]

#elif LIVE

#define  SERVER_LINK                  @"fieldforce.randemgroup.com.au"
#define  DOWNLOAD_SERVER_LINK         @"edge.fieldforce.randemgroup.com.au"
#define  OEM_WCF_URL                  @"Services/Service1.svc" // @"AutoProfiler.Wcf/Service1.svc"
#define  PDF_LINK                     @"Resource"
#define  IMAGE_LINK                   @"Resource" //@"OEMIPAD/resource/"

#define  CUSTOMER_IMAGE_DOWNLOAD_LINK [@"http://" stringByAppendingFormat:@"%@/%@/Customer/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]
#define  LOGO_IMAGE_URL               [@"http://" stringByAppendingFormat:@"%@/%@/Dealerlogo/ipadlogo/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]
#define  IMAGE_UPLOAD_LINK            [@"http://" stringByAppendingFormat:@"%@/uploader/fileupload.ashx",SERVER_LINK]

#define  RESOURCE_FILE_DOWNLOAD_LINK  [@"http://" stringByAppendingFormat:@"%@/%@/ResourceFile/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]


#elif MYFIELDFORCE

#define  SERVER_LINK                  @"app.myfieldforce.com.au"
#define  DOWNLOAD_SERVER_LINK         @"edge.myfieldforce.com.au"
#define  OEM_WCF_URL                  @"Services/Service1.svc" // @"AutoProfiler.Wcf/Service1.svc"
#define  PDF_LINK                     @"Resource"
#define  IMAGE_LINK                   @"Resource" //@"OEMIPAD/resource/"

#define  CUSTOMER_IMAGE_DOWNLOAD_LINK [@"http://" stringByAppendingFormat:@"%@/%@/Customer/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]
#define  LOGO_IMAGE_URL               [@"http://" stringByAppendingFormat:@"%@/%@/Dealerlogo/ipadlogo/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]
#define  IMAGE_UPLOAD_LINK            [@"http://" stringByAppendingFormat:@"%@/uploader/fileupload.ashx",SERVER_LINK]

#define  RESOURCE_FILE_DOWNLOAD_LINK  [@"http://" stringByAppendingFormat:@"%@/%@/ResourceFile/",DOWNLOAD_SERVER_LINK,IMAGE_LINK]

#endif

//#define IMAGE_UPLOAD_LINK  @"http://121.247.130.30:81/tms/fileupload.ashx"


#define  LOGIN_METHOD                  @"RepLogin"
#define  NEWS_METHOD                   @"GetNewsDetails"
#define  OFFERS_METHOD                 @"GetOfferDetails"
#define  CUSTOMERS_METHOD              @"GetCustomerDetails"
#define  UPCOMMING_VISITS_METHOD       @"GetVisitDetails"
#define  FETCH_REMINDERS_METHOD        @"FetchReminderDetails"
#define  EDIT_UPCOMMING_VISITS_METHOD  @"AddScheduledVisit"
#define  TASKS_METHOD                  @"FetchTaskDetails"
#define  FETCH_RESOURCES_METHOD        @"GetResourceDetails"

#define  CUSTOMER_CONTACTS_METHOD   @"GetCustomerContactDetails"
#define  CREATE_CUSTOMER_METHOD     @"CreateCustomer"
#define EDIT_CUSTOMER_METHOD        @"EditCustomer"
#define  CREATE_TASK_METHOD         @"AddTask"
#define CREATE_REMINDER_METHOD      @"AddReminder"

#define  CHANGE_PASSWORD_METHOD     @"ResetPassword"
#define  FORGOT_PASSWORD_METHOD     @"ForgotPassword"
#define  VISIT_UPLOAD_METHOD        @"AddVisit"
#define  NEWS_VIEWED_METHOD         @"UpdateNewsTap"
#define  OFFER_VIEWED_METHOD        @"UpdateOfferTap"

#define CUSTOMER_SAVE_IMAGE_DETAILS @"SaveCustomerImageDetails"
#define DELETE_CUSTOMER_IMAGE       @"DeleteCustomerImageDetails"
#define GET_CUSTOMER_IMAGE_DETAIL   @"GetCustomerImageDetails"
#define GET_FEEDBACK_CATEGORY       @"GetFeedBackCategoryDetails"
#define SUBMIT_FEEDBACK_METHOD      @"SaveFeedBack"
#define FETCH_LOGO_METHOD           @"FetchSiteLogo"
#define ADD_CUSTOMER_NOTES_METHOD   @"UpdateCustomerNotes"
#define SEND_OFFER_EMAIL_METHOD     @"SendOfferEmail"
#define CHECK_VERSION               @"FetchVersionInfo"
#define SEND_DEVICE_TOKEN           @"AddDeviceToken"
#define SEND_LOCATION_METHOD        @"UpdateRepresentativeLocation"

#define JSON_DETAILS_KEY           @"details"
#define JSON_DELETED_KEY           @"deleted"
#define JSON_UPDATED_KEY           @"updated"
#define JSON_STATUS_KEY            @"status"
#define JSON_LAST_REQUESTED_KEY    @"lastRequested"
#define JSON_WDSITE_CHANGE_KEY     @"isSiteIDChanged"
#define JSON_REP_DEACTIVATE_KEY    @"isDeActiveRep"
#define JSON_UNDER_MAINTENANCE_KEY @"isServerUnderMaintenance"

#define LAST_REQUESTED_NEWS        @"_last_request_news"
#define LAST_REQUESTED_OFFERS      @"_last_request_offers"
#define LAST_REQUESTED_CUSTOMERS   @"_last_request_customers"
#define LAST_REQUESTED_VISITS      @"_last_request_visits"
#define LAST_REQUESTED_CONTACTS    @"_last_request_contacts"
#define LAST_REQUESTED_TASKS       @"_last_request_tasks"
#define LAST_REQUESTED_REMINDERS   @"_last_request_reminders"
#define LAST_REQUESTED_SUBURBS     @"_last_request_suburbs"
#define LAST_REQUESTED_RESOURCES   @"_last_request_resources"

#define CONNECTION_TIME_OUT 40.0

@class ASIHTTPRequest,CustomerPhoto;
@interface ConnectionManager : NSObject
{
   // AppDelegate *app;
}


typedef enum
{
    ImageSizeTypeDefault=0,
    ImageSizeTypeLarge,
    ImageSizeTypeMedium,
    ImageSizeTypeThumb,
    ImageSizeTypeMediumLarge,
    
}ImageSizeType;

extern NSString * makeURL(NSString *methodName);
extern NSString * makeImageURL(NSString *imageName,NSString *imageType,BOOL isThumb);
extern NSString * makeImageURLString(NSString *imageName,NSString *imageType,ImageSizeType imageSizeType);
extern NSString * makePDFURL(NSString *pdfName,NSString *sourceType);


+(void)handleError:(NSError *)error;
+(ASIHTTPRequest *)createWCFConnectionWithUrl:(NSString *)aUrl jsonParam:(NSString *)jsonString HTTPMethod:(NSString *)method;
+(ASIHTTPRequest *)createWCFConnectionWithUrl:(NSString *)aUrl jsonParam:(NSString *)jsonString;
+(ASIHTTPRequest *)createWCFConnectionWithUrl:(NSString *)aUrl;

@end

@interface ConnectionManager(RepToolConnection)

#pragma mark - VALIDATION
+(BOOL)isValidResponse:(NSDictionary *)jsonDict;

#pragma mark - LAST REQUESTED
+(NSNumber *)lastRequestedForConnectionType:(NSString *)requestType;
+(void)setLastRequested:(NSNumber *)number forConnectionType:(NSString *)connectionType;

#pragma mark - APPLICATION
+(ASIHTTPRequest *)fetchLogoForWDSiteID:(NSNumber *)wdSiteID repID:(NSNumber *)repID;
+(ASIHTTPRequest *)sendMailWithRep:(Rep *)rep wdSiteID:(NSNumber *)wdId deviceInfo:(NSString *)dInfo  offer:(Offer *)offer subjet:(NSString *)sText customerIDs:(NSArray *)customerIDS;
+(ASIHTTPRequest *)checkApplicationVersionWithJson:(NSString *)aVersionJson;
+(ASIHTTPRequest *)createDeviceTokenWithJson:(NSString *)aDeviceTokenJson;
+(ASIHTTPRequest *)sendDeviceLocation:(CLLocationCoordinate2D)location;

#pragma mark - LOGIN
+(ASIHTTPRequest *)createLoginConnectionWithEmail:(NSString *)email password:(NSString *)password;
+(ASIHTTPRequest *)createResetPasswordConnectionWithPassword:(NSString *)newPassword forUserID:(NSNumber *)userID;
+(ASIHTTPRequest *)createForgotPasswordConnectionWithEmailID:(NSString *)emailID;


#pragma mark - NEWS

+(ASIHTTPRequest *)createNewsConnectionWithRepID:(NSNumber *)repID wdSiteID:(NSNumber *)siteID wdID:(NSNumber *)wdID;
+(ASIHTTPRequest *)createNewsViewedConnectionWithJson:(NSString *)json;


#pragma mark - OFFERS
+(ASIHTTPRequest *)createOffersConnectionWithWithRepID:(NSNumber *)repID wdSiteID:(NSNumber *)siteID wdID:(NSNumber *)wdID;
+(ASIHTTPRequest *)createOfferViewedConnectionWithJson:(NSString *)json;



#pragma mark - CUSTOMERS
+(ASIHTTPRequest *)createNewCustomerConnectionWithCustomerJson:(NSString *)aCustomerJson;
+(ASIHTTPRequest *)createCustomersConnectionWithRepID:(NSNumber *)repID wdSiteID:(NSNumber *)wdId territoryIDs:(NSString *)territoryIDs customerIDs:(NSString *)customerIDs;
//+(ASIHTTPRequest *)createCustomersConnectionWithRepID:(NSNumber *)repID wdSiteID:(NSNumber *)wdId;
+(ASIHTTPRequest *)editCustomerConnectionWithCustomerJson:(NSString *)aCustomerJson;
+(ASIHTTPRequest *)saveCustomerNotes:(NSString *)notes forCustomerID:(NSNumber *)customerID;


#pragma mark - CUSTOMER CONTACTS
+(ASIHTTPRequest *)createCustomerContactsConnectionForCustomer:(Customer *)aCustomer repID:(NSNumber *)repID;


#pragma mark - CUSTOMER IMAGES
//+(ASIHTTPRequest *)uploadImagesForCustomer;

//+(ASIHTTPRequest *)saveNewCustomerImagesConnectionWithCustomerImageJson:(NSMutableArray *)arrCustomerImage customerID:(NSNumber *)customerID wdSiteID:(NSNumber *)siteID;

+(ASIHTTPRequest *)saveNewCustomerImagesConnectionWithCustomerImageJson:(NSMutableArray *)arrCustomerImage forCustomer:(Customer *)customer;

+(ASIHTTPRequest *)uploadImagesForCustomer:(NSMutableArray *)arrImages;

+(ASIHTTPRequest *)uploadPendingCustomerImages:(NSArray *)images;
+(ASIHTTPRequest *)uploadImagesForCustomerEdit:(NSString *)imageFilePath;

+(ASIHTTPRequest *)getImagesForCustomer:(Customer *)aCustomer  repId:(NSNumber *)repId wdSiteID:(NSNumber *)wdSiteID;

+(ASIHTTPRequest *)deleteImagesForCustomerID:(NSNumber *)customerID siteID:(NSNumber *)siteID photo:(CustomerPhoto *)photo repID:(NSNumber *)repID;



#pragma mark - TASKS
+(ASIHTTPRequest *)createTaskConnectionWithRepID:(NSNumber *)repID wdSiteID:(NSNumber *)siteID wdID:(NSNumber *)wdID;
+(ASIHTTPRequest *)createNewTaskConnectionWithTaskJson:(NSString *)aTaskJson;


#pragma mark - SCHEDULE VISITS
+(ASIHTTPRequest *)createUpcommingVisitsConnection;
+(ASIHTTPRequest *)createUpcommingVisitsConnectionWithRepId:(NSNumber *)repId wdSiteID:(NSNumber *)wdSiteID;
+(ASIHTTPRequest *)createNewscheduledVisitConnectionWithJson:(NSString *)json;

#pragma mark - REMINDER
+(ASIHTTPRequest *)createRemindersWithJson:(NSString *)aJson;
+(ASIHTTPRequest *)fetchRemindersWithRepID:(NSNumber *)repId wdSiteID:(NSNumber *)wdSiteID;



#pragma mark - VISITS
+(ASIHTTPRequest *)uploadVisitDetailsForVisit:(Visit *)visit visitReports:(NSArray *)reports;
+(ASIHTTPRequest *)uploadImagesForVisit:(Visit *)visit;


#pragma mark - FEEDBACK

+(ASIHTTPRequest *)getFeedbackCategory:(NSNumber *)repId wdSiteID:(NSNumber *)wdId;
+(ASIHTTPRequest *)submitFeedbackConnectionWithRepID:(NSNumber *)repId wdSiteID:(NSNumber *)wdId deviceInfo:(NSString *)dInfo  categoryID:
(NSString *)cID subjet:(NSString *)sText description:(NSString *)description categoryaName:(NSString *)categoryaName;


#pragma mark TEST
+(ASIHTTPRequest *)createServerMaintenanceTestConnection;
#pragma mark -

#pragma mark - RESOURCE
+(ASIHTTPRequest *)fetchResources;
+(ASIHTTPRequest*)requestToDownloadResourceFileForName:(NSString *)fileName toPath:(NSString *)destinationPath;

#pragma mark - FETCH AND PROCESS CUSTOMERS
+(ASIHTTPRequest *)fetchAndProcessCustomersOnCompletion:(void(^)(BOOL success, NSMutableArray *insertedResult,NSMutableArray *updatedResult, NSError *error))completionBlock deletionBlock:(void(^)(NSMutableArray *deletedResult))deletionBlock;

#pragma mark - FETCH AND PROCESS TASKS
+(ASIHTTPRequest *)fetchAndProcessTasksOnCompletion:(void(^)(BOOL success, NSError *error))completionBlock;

+(ASIHTTPRequest *)fetchAndProcessScheduledVisitsOnCompletion:(void(^)(BOOL success, NSError *error))completionBlock;

+(ASIHTTPRequest *)fetchAndProcessRemindersOnCompletion:(void(^)(BOOL success, NSError *error))completionBlock;

@end