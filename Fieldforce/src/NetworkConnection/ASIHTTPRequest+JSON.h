//
//  ASIHTTPRequest+JSON.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 13/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "ASIHTTPRequest.h"
#import "JSON.h"

@interface ASIHTTPRequest (JSON)

-(id)responseJSON;

@end
