//
//  NSData+SBJSON.h
//  ParseJASON
//
//  Created by SANDIP_ RANDEM on 06/12/10.
//  Copyright 2010 RANDEM IT. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSData(SBJSON) 

- (id)JSONValue;
//- (id)JSONValue:(NSError **)error;
@end
