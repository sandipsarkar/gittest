//
//  NSData+SBJSON.m
//  ParseJASON
//
//  Created by SANDIP_ RANDEM on 06/12/10.
//  Copyright 2010 RANDEM IT. All rights reserved.
//

#import "NSData+SBJSON.h"
#import "NSString+SBJSON.h"

@implementation NSData(SBJSON)

- (id)JSONValue
{
	NSString *jsonString=[[NSString alloc] initWithData:self encoding:NSUTF8StringEncoding];
    id repr = [jsonString JSONValue];
    [jsonString release];
    return repr;
}

/*
- (id)JSONValue:(NSError **)error
{
    NSString *jsonString=[[NSString alloc] initWithData:self encoding:NSUTF8StringEncoding];
    id repr = [jsonString JSONValue:error];
    [jsonString release];
    return repr;
    
}
*/


@end
