//
//  ASIHTTPRequest+JSON.m
//  RepVisitationTool
//
//  Created by RANDEM MAC on 13/06/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "ASIHTTPRequest+JSON.h"


@implementation ASIHTTPRequest (JSON)


-(id)responseJSON
{
    id parsedObj=nil;
    
    NSData *responseData = [self responseData];
    
    if(responseData.length)
    {
        
        if(isClassAvailable(@"NSJSONSerialization"))
        {
            NSError *_error=nil;
            parsedObj=[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&_error];
            
      
        if(_error) NSLog(@"Error:%@",_error);

        }
        else
        {
            parsedObj=[responseData JSONValue];
        }
        
          
           NSLog(@"Json:%@",parsedObj);
 
    }
    
    return parsedObj;
}



@end
