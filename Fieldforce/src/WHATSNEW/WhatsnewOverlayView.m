//
//  WhatsnewOverlayView.m
//  Fieldforce
//
//  Created by Randem IT on 06/11/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "WhatsnewOverlayView.h"

const CGFloat _contenentViewWidth  = 435.0;
 //CGFloat _contenentViewHeight = 255.0;

 CGFloat _contenentViewHeight = 275.0;//270.0

@interface WhatsnewOverlayView()
{
    CGRect originalFrame;
    
    UIView *_contentView;
    UIButton *_dismissButton;
    
    UIView *_linewView;
    
    UIImageView *_logoImageView;
    
    UILabel *_titleLabel;
    UILabel *_versionLabel;
    UILabel *_dateLabel;
}

@property (nonatomic,retain)NSMutableArray *updates;
@end

@implementation WhatsnewOverlayView
@synthesize isShown;

-(id)initWithParentView:(UIView *)parentView
{
     CGRect _overlayFrame = parentView.bounds;
    
    NSString *whatsNewPath = [[NSBundle mainBundle] pathForResource:@"whatsnew" ofType:@"plist"];
    
    NSMutableArray *arr = [NSMutableArray arrayWithContentsOfFile:whatsNewPath];
    
    self.updates = arr ? arr : [NSMutableArray array];
    
    
    self = [self initWithFrame:_overlayFrame];
    
    [parentView addSubview:self];
    
    
    self.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:0.1];
    
    return self;
}

-(UIView *)contentView
{
    if(_contentView)
    {
        
        return _contentView;
    }
    
    
    _contentView = [[UIView alloc] initWithFrame: CGRectZero];
    
    _contentView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    _contentView.backgroundColor = [UIColor whiteColor];
    
    
    
    
    UIImage *_logoImage = [UIImage imageNamed:@"whats_new_autotrade_logo.png"];
    
    _logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20.0, 20.0, _logoImage.size.width, _logoImage.size.height)];
    _logoImageView.image = _logoImage;
    _logoImageView.userInteractionEnabled = YES;
    _logoImageView.backgroundColor = [UIColor clearColor];
    [_contentView addSubview:_logoImageView];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_logoImageView.frame)+10.0, CGRectGetMinY(_logoImageView.frame)+15, 170, 20.0)];
    _titleLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1.0];
    _titleLabel.font = [UIFont subaruBoldFontOfSize:18.0];
    _titleLabel.backgroundColor = [UIColor clearColor];
    [_contentView addSubview:_titleLabel];
    
    _versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_titleLabel.frame), CGRectGetMaxY(_titleLabel.frame)+1, 120, 15.0)];
    _versionLabel.textColor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:1.0];
    _versionLabel.font = [UIFont subaruBookFontOfSize:16.0];
    _versionLabel.backgroundColor = [UIColor clearColor];
    [_contentView addSubview:_versionLabel];
    
    _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_versionLabel.frame), CGRectGetMaxY(_versionLabel.frame)+1, 120, 15.0)];
    _dateLabel.font = _versionLabel.font;
    _dateLabel.textColor = _versionLabel.textColor;
    _dateLabel.backgroundColor = [UIColor clearColor];
    [_contentView addSubview:_dateLabel];
    
    
    
    _linewView =[[UIView alloc] initWithFrame:CGRectZero];
    _linewView.backgroundColor = [UIColor colorWithRed:194.0/255.0 green:194.0/255.0 blue:194.0/255.0 alpha:1.0];
    _linewView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
    [_contentView addSubview:_linewView];
    
    _dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _dismissButton.frame = CGRectMake(0, 0, 10, 10);
    [_dismissButton setTitle:@"OK" forState:UIControlStateNormal];
    [_dismissButton setTitleColor:[UIColor colorWithRed:24.0/255.0 green:89.0/255.0 blue:153.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_dismissButton setBackgroundColor:[UIColor clearColor]];
    [_contentView addSubview:_dismissButton];
    [_dismissButton addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchUpInside];
    
    if(_dismissButton.titleLabel)
    {
        _dismissButton.titleLabel.font = [UIFont subaruBoldFontOfSize:18.0];
        
    }
    
    
    _contentView.layer.borderWidth = 2.0;
    _contentView.layer.borderColor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:1.0].CGColor;
    _contentView.layer.cornerRadius = 3.0;
    
    [_contentView.layer setShadowColor:[UIColor blackColor].CGColor];
    [_contentView.layer setShadowOpacity:0.6];
    [_contentView.layer setShadowRadius:3.0];
    [_contentView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    
    
#if LIVE_TEST
    _titleLabel.text = @"FIELDFORCE TEST";
#elif GENERIC_DEMO
    _titleLabel.text = @"FIELDFORCE DEMO";
#elif MYFIELDFORCE
    _titleLabel.text = @"FIELDFORCE";
#elif LOCAL
    _titleLabel.text  = @"FIELDFORCE LOCAL" ;
    
#elif LOCAL2
    _titleLabel.text = @"FIELDFORCE LOCAL2";
#else
    _titleLabel.text = @" ";
#endif
    
    
    _versionLabel.text =[NSString stringWithFormat:@"Version %@",APP_VERSION];
    
     NSDate *_date = nil;
     NSNumber *dateInterval = [[NSUserDefaults standardUserDefaults] objectForKey:BUILD_VERSION_UPLOADED_DATE];
    
    if(dateInterval && [dateInterval longLongValue])
    {
        int64_t _ticks = [dateInterval longLongValue];
        _date = _ticks ? [NSDate dateWithCLRTicks:_ticks fromTimeZoneName:@"Australia/Sydney"] : nil;
        
       // int64_t __ticks = [_date CLRTicksForTimeZone:nil];
        
    }
    else
    {
       dateInterval = [[NSUserDefaults standardUserDefaults] objectForKey:LAST_VERSION_UPDATED_DATE];
       _date = dateInterval ? [NSDate dateWithTimeIntervalSince1970:[dateInterval doubleValue]] : nil;
    }
    
    _dateLabel.text = _date ? [_date stringFromDateWithFormat:@"d MMM, YYYY"] : @"";
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
    [self addGestureRecognizer:tapGestureRecognizer];
    [tapGestureRecognizer release];
    
    return _contentView;

}

- (id)initWithFrame:(CGRect)frame
{
    //CGRect _applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    self = [super initWithFrame:frame];
    
    if (self)
    {
        // Initialization code
        
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        originalFrame = frame;
    
       [self addSubview:[self contentView]];
    }
    
    return self;
}

-(void)dealloc
{
    
    [_contentView release];
    [_linewView release];
    [_logoImageView release];
    [_titleLabel release];
    [_versionLabel release];
    [_dateLabel release];
    
    
    [super dealloc];
}


-(void)_layoutSubviews
{
    [super layoutSubviews];
    
    CGRect _bounds = self.bounds;
    

//    NSString *allString =[self.updates componentsJoinedByString:@","];
//    CGSize _size =[allString sizeWithFont:[UIFont subaruMediumFontOfSize:16.0] constrainedToSize:CGSizeMake(300, CGFLOAT_MAX)];
//    
//    if(CGRectGetMinY(_linewView.frame)-CGRectGetMaxY(_dateLabel.frame)<_size.height+15)
//    {
//        _contenentViewHeight += _size.height;
//        
//    }
   
    
    CGRect _contentViewFrame = CGRectMake((CGRectGetWidth(_bounds)-_contenentViewWidth)/2.0, (CGRectGetHeight(_bounds)-_contenentViewHeight)/2.0, _contenentViewWidth, _contenentViewHeight);
    
    [self contentView].frame = _contentViewFrame;
    
    _logoImageView.frame =CGRectMake(20.0, 20.0, _logoImageView.image.size.width, _logoImageView.image.size.height);
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_logoImageView.frame)+10.0, CGRectGetMinY(_logoImageView.frame)+15, 170, 20.0)];
    
    _versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_titleLabel.frame), CGRectGetMaxY(_titleLabel.frame)+1, 120, 15.0)];
    
    _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_versionLabel.frame), CGRectGetMaxY(_versionLabel.frame)+1, 120, 15.0)];
    
    _linewView.frame = CGRectMake(0, CGRectGetHeight(_contentViewFrame)-35.0, CGRectGetWidth(_contentViewFrame), 1.0);
    
    _dismissButton.frame = CGRectMake(0, CGRectGetMaxY(_linewView.frame), CGRectGetWidth(_contentViewFrame), 34);
    
    //_dismissButton.titleLabel.font = [UIFont subaruBoldFontOfSize:18.0];
    
   
    CGFloat locY = CGRectGetMaxY(_dateLabel.frame) + 35.0;
    
    int index = 0;
    
    UIImage *bulletImage = [UIImage imageNamed:@"bullet.png"];
    
    CGFloat _points[3] ={0,34,52};
    
    
    for(NSString *updateText in self.updates)
    {
        
//        UITableViewCell *_cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"identifier"];
//        
//        _cell.frame = CGRectMake(CGRectGetMinX(_logoImageView.frame)-15.0, locY+18*index, CGRectGetWidth(_contentView.bounds)-10.0, 18.0);
//        
//        _cell.imageView.image = bulletImage;
//        
//        _cell.textLabel.text = updateText;
//        _cell.textLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1.0];
//        _cell.textLabel.font = [UIFont subaruBookFontOfSize:16.0];
//        
//        [_contentView addSubview:_cell];
//        [_cell release];
        
        
        UIView *_testContainerView = [_contentView viewWithTag:index+1];
        
        if(!_testContainerView)
        {
            CGFloat _point = _points[index];
            
            _testContainerView =[[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(_logoImageView.frame), locY+_point, CGRectGetWidth(_contentView.bounds)-CGRectGetMinX(_logoImageView.frame)*2,  (index==1?18:30.0))];
            _testContainerView.backgroundColor = [UIColor clearColor];
         _testContainerView.tag = index+1; //28*index
        
      
            //CGFloat __offsetY = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")? 0.0 :2.0;
            
            CGFloat __bulletY = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")? 6.7 :3.0;
            
            UIImageView*  bulletView =[[UIImageView alloc] initWithFrame:CGRectMake(1.0, __bulletY , 6.0, 6.0)]; //(CGRectGetHeight(_testContainerView.bounds)-6.0)/2.0)-__offsetY
            bulletView.image = bulletImage;
            bulletView.backgroundColor = [UIColor clearColor];
            bulletView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
            [_testContainerView addSubview:bulletView];
            [bulletView release];

         
            UILabel*  textLabel =[[UILabel alloc] initWithFrame:CGRectMake(11, 0, CGRectGetWidth(_testContainerView.bounds)-10.0, index==1?18: 34.0)];
             textLabel.numberOfLines = 2;
            textLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1.0];
            
            textLabel.text = updateText;
            textLabel.font = [UIFont subaruBookFontOfSize:16.0];
            textLabel.backgroundColor = [UIColor clearColor];
            [_testContainerView addSubview:textLabel];
            [textLabel release];
 
        
           [_contentView addSubview:_testContainerView];
           [_testContainerView release];
        }
        
        index++;
    }

}



-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect _bounds = self.bounds;
    
    
    //NSString *allString = [self.updates componentsJoinedByString:@","];
   // CGSize _size = [allString sizeWithFont:[UIFont subaruBookFontOfSize:16.0] constrainedToSize:CGSizeMake(_contenentViewWidth-20.0*2, CGFLOAT_MAX)];
    
    CGFloat __hh = 0.0 ; // _size.height+117.0+90.0;
    
    CGFloat _h = MAX(_contenentViewHeight, __hh) ;
    
    if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        _h += 25.0;
    }
    
   // CGRect _contentViewFrame = CGRectMake((CGRectGetWidth(_bounds)-_contenentViewWidth)/2.0, (CGRectGetHeight(_bounds)-_contenentViewHeight)/2.0, _contenentViewWidth, _contenentViewHeight);
    
    CGRect _contentViewFrame = CGRectMake((CGRectGetWidth(_bounds)-_contenentViewWidth)/2.0, (CGRectGetHeight(_bounds)-_h)/2.0, _contenentViewWidth, _h);
    
    [self contentView].frame = _contentViewFrame;
    
    _logoImageView.frame =CGRectMake(20.0, 20.0, _logoImageView.image.size.width, _logoImageView.image.size.height);
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_logoImageView.frame)+10.0, CGRectGetMinY(_logoImageView.frame)+15, 170, 20.0)];
    
    _versionLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_titleLabel.frame), CGRectGetMaxY(_titleLabel.frame)+1, 120, 15.0)];
    
    _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_versionLabel.frame), CGRectGetMaxY(_versionLabel.frame)+1, 120, 15.0)];
    
    _linewView.frame = CGRectMake(0, CGRectGetHeight(_contentViewFrame)-35.0, CGRectGetWidth(_contentViewFrame), 1.0);
    
    _dismissButton.frame = CGRectMake(0, CGRectGetMaxY(_linewView.frame), CGRectGetWidth(_contentViewFrame), 34);
    
    //_dismissButton.titleLabel.font = [UIFont subaruBoldFontOfSize:18.0];
    
    CGFloat locY = CGRectGetMaxY(_dateLabel.frame) + 35.0;
    
    int index = 0;
    
    UIImage *bulletImage = [UIImage imageNamed:@"bullet.png"];
    
    CGFloat __locY = 0;
    for(NSString *updateText in self.updates)
    {
        CGFloat levelWidth = CGRectGetWidth(_contentView.bounds)-CGRectGetMinX(_logoImageView.frame)*2;
        
         CGSize _size = [updateText sizeWithFont:[UIFont subaruBookFontOfSize:16.0] constrainedToSize:CGSizeMake(levelWidth, CGFLOAT_MAX)];
        
        UIView *_testContainerView = [_contentView viewWithTag:index+1];
        
        if(!_testContainerView)
        {
            _testContainerView =[[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(_logoImageView.frame), locY+__locY, CGRectGetWidth(_contentView.bounds)-CGRectGetMinX(_logoImageView.frame)*2, _size.height)];
            _testContainerView.backgroundColor = [UIColor clearColor];
            _testContainerView.tag = index+1; //28*index
            
            
            //CGFloat __offsetY = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")? 0.0 :2.0;
            
            CGFloat __bulletY = 6.0;
            
            UIImageView*  bulletView =[[UIImageView alloc] initWithFrame:CGRectMake(1.0, __bulletY , 6.0, 6.0)]; //(CGRectGetHeight(_testContainerView.bounds)-6.0)/2.0)-__offsetY
            bulletView.image = bulletImage;
            bulletView.backgroundColor = [UIColor clearColor];
            bulletView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
            [_testContainerView addSubview:bulletView];
            [bulletView release];
            
            
            UILabel*  textLabel =[[UILabel alloc] initWithFrame:CGRectMake(11, 0, CGRectGetWidth(_testContainerView.bounds)-10.0, _size.height)];
            textLabel.numberOfLines = 3;
            textLabel.textColor = [UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1.0];
            
            textLabel.text = updateText;
            textLabel.font = [UIFont subaruBookFontOfSize:16.0];
            textLabel.backgroundColor = [UIColor clearColor];
            [_testContainerView addSubview:textLabel];
            [textLabel release];
            
            
            [_contentView addSubview:_testContainerView];
            [_testContainerView release];
            
            __locY +=_size.height+2.0;
        }
        
        index++;
    }
    
}


-(void)dismissAction
{
    
    [UIView animateWithDuration:0.3 animations:^{
        //self.transform = CGAffineTransformMakeScale(0.1, 0.1);
        _contentView.alpha = 0;
    } completion:^(BOOL finished) {
        
        [self removeFromSuperview];
        isShown = NO;
    }];

}


- (void)show
{
    NSLog(@"show");
 
    _contentView.alpha = 0.0;
    _contentView.transform = CGAffineTransformMakeScale(0.2, 0.2);
    
    [UIView animateWithDuration:0.2 animations:^{
    
     _contentView.transform = CGAffineTransformMakeScale(1.0, 1.0);
     _contentView.alpha = 1;
        
    }
    completion:^(BOOL finished)
    {
        
           isShown = YES;
        
    }];
}


-(void)tapGestureAction:(UITapGestureRecognizer *)recognizer
{
    CGPoint _tapLocation = [recognizer locationInView:recognizer.view];
    
    CGRect _contentViewRect = _contentView.frame;
    
    BOOL _isInside = CGRectContainsPoint(_contentViewRect, _tapLocation);
    
    if(!_isInside)
    {
      [self dismissAction];
    }
  
    
  
}

@end
