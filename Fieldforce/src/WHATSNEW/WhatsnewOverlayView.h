//
//  WhatsnewOverlayView.h
//  Fieldforce
//
//  Created by Randem IT on 06/11/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WhatsnewOverlayView : UIView

@property(nonatomic,assign,readonly)BOOL isShown;

-(id)initWithParentView:(UIView *)parentView;
- (void)show;

@end
