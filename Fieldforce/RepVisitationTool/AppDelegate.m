//
//  AppDelegate.m
//  RepVisitationTool
//
//  Created by Somsankar Ray on 08/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
//#import "MasterMenuViewController.h"
//#import "DashboardViewController.h"
#import "MenuViewController.h"
#import "ConnectionManager.h"

#import "OfflineManager.h"
#import "CoreDataHandler.h"
#import "DirectoryManager.h"
#import "Rep.h"
#import "TrackEventUIApplication.h"
#import "CustomerContact.h"
#import "Customer.h"
#import "News.h"    
#import "WholesaleDealer.h"
#import "Offer.h"
#import "ScheduledVisit.h"
#import "Territory.h"
#import "Visit.h"
#import "VisitPhoto.h"
#import "CustomEventStore.h"
#import "LocationManager.h"
#import "SSTimer.h"
#import "Reachability.h"
#import "WindowOverlayView.h"

#if !MYFIELDFORCE
#import "mach/mach.h"
#endif




NSString const * VisitDidEndNotification   = @"FF_VisitDidEndNotificationKey";
NSString const * TaskDidEditNotification   = @"FF_TaskDidEditNotification";
NSString const * TaskDidDeleteNotification = @"FF_TaskDidDeleteNotification";
NSString const * CustomerContactsDidLoad   = @"FF_CustomerContactsDidLoad";
NSString const * CustomersDidFinishLoad    = @"FF_CustomersDidLoad";
NSString const * CustomerDidDeletedOnDuplicate    = @"FF_CustomerDidDeletedOnDuplicate";


#define CRITTERCISM_APP_ID @"5146f76546b7c2625c00000d"

#define BUGSENSE_API_KEY        @"d9a1f61d"
#define BUGSENSE_API_KEY_LIVE   @"7d999e9a"
#define BUGSENSE_API_KEY_TEST   @"fdf2aafb"
#define BUGSENSE_API_KEY_DEMO   @"16f21d82"


AppDelegate *app=nil;
BOOL isalert;
@implementation AppDelegate

@synthesize window = _window;
@synthesize splitViewController=_splitViewController;

@synthesize coreDataHandler;
@synthesize shouldAvoidMenuSelection;
@synthesize  isVisitSessionRunning;
@synthesize fromCustomerTask;
@synthesize inCalender;
@synthesize strDealingOemForVisitPhoto;
@synthesize isCustomerLoaded;
@synthesize offerTimeZoneName;
@synthesize isCustomerFetching;
@synthesize canAccessCalendar,canAccessReminder;

@synthesize eventFetchCount;
@synthesize isServerUnderMaintenance;

- (void)dealloc
{
    [_splitViewController release];
    [_window release];
    [coreDataHandler release];
    [offerTimeZoneName release];
    [strDealingOemForVisitPhoto release];
    
    dispatch_release(_eventQueue);
    [super dealloc];
}


//******************** CHECK CUSTOMER LOADING ACCORDING TO LOGIN EMAIL *************************
-(void)setIsCustomerLoaded:(BOOL)_isCustomerLoaded
{
    isCustomerLoaded = _isCustomerLoaded;
    
    [CoreDataHandler setCustomersFetchedFromDashboard:_isCustomerLoaded];
    
    /*
    NSString *loginEmail = [[NSUserDefaults standardUserDefaults] objectForKey:LOGIN_EMAIL_KEY];
    
    if(loginEmail)
    [[NSUserDefaults standardUserDefaults] setBool:_isCustomerLoaded forKey:loginEmail];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    */
}


-(BOOL)isCustomerLoaded
{
   // if(!isCustomerLoaded)
    isCustomerLoaded =  [CoreDataHandler isCustomersFetchedFromDashboard];
    
    return isCustomerLoaded;
    
    /*
    NSString *loginEmail = [[NSUserDefaults standardUserDefaults] objectForKey:LOGIN_EMAIL_KEY];
    
    if(!loginEmail) return isCustomerLoaded;
        
    return   [[NSUserDefaults standardUserDefaults] boolForKey:loginEmail];
    */
}
//**********************************************************************************************

#pragma mark REMOVE ROUNDED RECT FROM SPLITVIEWCONTROLLER
- (void) explode:(id)aView level:(int)level
{
    if ([aView isKindOfClass:[UIImageView class]]) 
    {
        UIImageView* roundedCornerImage = (UIImageView*)aView;
        roundedCornerImage.hidden = YES;
    }
    
    if (level < 2) 
    {
        for (UIView *subview in [aView subviews])
        {
            [self explode:subview level:(level + 1)];
        }
    }
}

- (void) fixRoundedSplitViewCorner
{
    [self explode:[[UIApplication sharedApplication] keyWindow] level:0];
}

#pragma mark -


-(void)setupSplitViewController
{
    
    if(!_splitViewController)
    {
        MenuViewController *masterVc = [[MenuViewController alloc] init];
        #if CUSTOM_SPLITSVC
        _splitViewController = [[ParallelStackViewController alloc] init];
        _splitViewController.masterController=masterVc;
        _splitViewController.masterControllerWidth=260;
        #else 
        
        _splitViewController = [[UISplitViewController alloc] init];
        //_splitViewController.presentsWithGesture = YES;
        _splitViewController.viewControllers = [NSArray arrayWithObjects:masterVc,  nil];
        //_splitViewController.view.opaque = NO;
        #endif
        
       _splitViewController.view.backgroundColor = [UIColor whiteColor];
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            UIView *statusBarOverlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_splitViewController.view.bounds), 20)];
            statusBarOverlayView.backgroundColor = [UIColor blackColor];
            [_splitViewController.view addSubview:statusBarOverlayView];
            statusBarOverlayView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            [statusBarOverlayView release];
            
            _splitViewController.edgesForExtendedLayout = UIRectEdgeNone;
        }
        
        [masterVc release];
    }
    
    //self.window.rootViewController=_splitViewController;
}

-(void)showLogin
{
    BOOL _isLoggedIn = [AppDelegate isLoggedIn];
    
    if(_isLoggedIn)
    {
        [self setupSplitViewController];
        self.window.rootViewController=_splitViewController;
        
        if(_navController)
        {
          [_navController release];
          _navController = nil;
        }
    }
    else
    {
        if(!_navController)
        {
            LoginViewController  *_loginViewController=[[LoginViewController alloc] init];
            _navController=[[UINavigationController alloc] initWithRootViewController:_loginViewController];
            [_loginViewController release];
        }
        
        self.window.rootViewController=_navController;
        
        //OBJC_DESTROY(_navController);
        
        if(_splitViewController)
        {
          [_splitViewController release];
          _splitViewController = nil;
        }
    }
}

-(Rep *)curentRep
{
    if(!_rep)
     _rep = [CoreDataHandler currentRep];
    
    return _rep;
}

#pragma mark -
#pragma mark CoreData Handler

/*
-(void)setupCoreDataHandlerForloginEmail:(NSString *)email
{
    if(!email) return;
    
    NSNumber *hash= [NSNumber numberWithUnsignedInteger:[email hash]];
    NSString *fileName = [hash description];
    [DirectoryManager createLoginDirectoryPathForName:fileName]; 
    NSString *loginDirectoryPath = [DirectoryManager loginDirectoryPath];
    NSString *dbFilePath = [loginDirectoryPath stringByAppendingFormat:@"/%@.sqlite",fileName];
    
    if(! coreDataHandler) 
        coreDataHandler=[[CoreDataHandler alloc] initWithPersistentStorePath:dbFilePath];
    else
        coreDataHandler.storePath = dbFilePath;
}
*/


-(void)setupCoreDataHandlerForloginEmail:(NSString *)email
{
    if(!email.length) return;
    
    NSNumber *hash= [NSNumber numberWithUnsignedInteger:[email hash]];
    NSString *fileName = [NSString stringWithFormat:@"%@",hash]; //[hash description];
    [DirectoryManager createLoginDirectoryPathForName:fileName];
    NSString *loginDirectoryPath = [DirectoryManager loginDirectoryPath];
    
#if USE_DB_MIGRATION
    
    NSString *dbFilePath = nil;
    
    //============== GET THE AVAILABLE SQLITE FILE =============
    
    NSArray	*dirArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:loginDirectoryPath error:nil];
    NSArray *dbfiles = [dirArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self CONTAINS[cd] %@",@".sqlite"]];
    
    for(NSString *fileName in dbfiles)
    {
        dbFilePath = [loginDirectoryPath stringByAppendingPathComponent:fileName];
        break;
    }
    
    if(!dbFilePath)
        dbFilePath = [loginDirectoryPath stringByAppendingFormat:@"/%@.sqlite",fileName];
    
#else
     NSString *dbFilePath = [loginDirectoryPath stringByAppendingFormat:@"/%@.sqlite",fileName];
#endif
    
    
    
      if(! coreDataHandler)
        coreDataHandler  = [[CoreDataHandler alloc] initWithPersistentStorePath:dbFilePath];
      else
        coreDataHandler.storePath = dbFilePath;
    
}


-(void)resetDB
{
    //[[SSCoreDataManager sharedManager] save:nil];
    [[OfflineManager sharedManager] clearOfflineQueue];
    
    self.isCustomerLoaded = NO;
    
    NSError ** error = nil;
    NSString *storePath = coreDataHandler.storePath;
    
    NSURL *fileUrl = [NSURL fileURLWithPath:storePath];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:storePath])
        [[NSFileManager defaultManager] removeItemAtURL:fileUrl error:error];
    
    coreDataHandler.storePath = nil;
    

    NSString *docuDir = [DirectoryManager loginDirectoryPath];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:docuDir])
    {
        [[NSFileManager defaultManager] removeItemAtURL:[NSURL fileURLWithPath:docuDir] error:nil];
    }
}

#pragma mark -

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    /*
    id locationValue = [launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey];
    if(locationValue)
     {
     if(locationValue.coordinate.latitude!=0.0 && locationValue.coordinate.longitude!=0.0);
     {
     [ConnectionManager sendDeviceLocation:locationValue.coordinate];
     }

     }//return YES;
    */
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    [application setStatusBarStyle:UIStatusBarStyleLightContent];
   
    
    application.idleTimerDisabled = NO;
    
    
#if LOCAL
    NSDictionary *pushNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (pushNotif)
    {
         [self application:application didReceiveRemoteNotification:pushNotif];
    }
#endif
    
    
#if TRACK_ERROR
    [self setupErrorTracking];
#endif
    
    if(!app) app=self;
    
    shouldAvoidMenuSelection = NO;
    
    fromCustomerTask=0;
    
    coreDataHandler=[[CoreDataHandler alloc] init];
    NSString *loginEmail = [[NSUserDefaults standardUserDefaults] objectForKey:LOGIN_EMAIL_KEY];

    [self setupCoreDataHandlerForloginEmail:loginEmail];
    
    
if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
{
    [MBPopoverBackgroundView initialize];
    [MBPopoverBackgroundView setArrowImageName:POPOVER_ARROW_IMAGE];
    [MBPopoverBackgroundView setBackgroundImageName:POPOVER_BG_IMAGE];
    [MBPopoverBackgroundView setBackgroundImageCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [MBPopoverBackgroundView setContentViewInsets:UIEdgeInsetsMake(1, 1, 1, 1)];
}
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    self.window.backgroundColor = [UIColor blackColor];
    

      // [self setupSplitViewController];
    
    [self showLogin];
    [self.window makeKeyAndVisible];
    
    //==================    REGISTER FOR PUSH NOTIFICATION =======================
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    

    
    //=========================== CALENDAR ACCESS PERMISSION ============================
    EKEventStore *eventStore = [CustomEventStore sharedInstance].eventStore;
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             
             if(!granted || error!=nil)
             {
                 NSLog(@"Event not granted");
                 NSString *message = CALENDAR_ACCESS_MESSAGE;
                 [UIAlertView showWarningAlertWithTitle:nil message:message];
             }
             
             canAccessCalendar = granted;
         });
         
         
     }];
    
    //======================== REMINDER ACCESS PERMISSION ==============================
    [eventStore requestAccessToEntityType:EKEntityTypeReminder completion:^(BOOL granted, NSError *error)
     {
        dispatch_async(dispatch_get_main_queue(), ^{
              
           if(!granted || error!=nil)
           {
               NSLog(@"Reminder not granted");
               NSString *message = REMINDER_ACCESS_MESSAGE;
               [UIAlertView showWarningAlertWithTitle:nil message:message];
           }
            
            canAccessReminder = granted;
        });
         
     }];

    
    if(![AppDelegate isLoggedIn])
    {
        
    ProgressHUD *progressHUD =[ProgressHUD showHUDAddedTo:self.window animated:YES];
    progressHUD.labelText    = @"Initializing...";
    progressHUD.mode = MBProgressHUDModeIndeterminate;
        
    [self performSelector:@selector(hideInitializerLoading) withObject:nil afterDelay:15.0];
    
    [CustomEventStore deleteAllEvents:^{
        
     [self hideInitializerLoading];
        
    }];
        
    }

    
#if BACKGROUND_GPS_ENABLED
    if([AppDelegate isLoggedIn])
    {
       [self registerLocationService];
    }
#endif
    
    
#if LOCAL || LOCAL2
     [self performSelector:@selector(test) withObject:nil afterDelay:0];
    
    //[self __transferUserDefaultsToKeychain];
#endif
    
    

    
    return YES;

}



-(dispatch_queue_t)eventQueue
{
    //===============
    if(_eventQueue==NULL)
    {
       _eventQueue = dispatch_queue_create("com.randem.fieldforce.eventQueue", DISPATCH_QUEUE_CONCURRENT);
    }
    //=============

    return _eventQueue;
}

#pragma mark ERROR TRACKING
-(void)setupErrorTracking
{
    // *****************    REGISTER CRITTERCISM           ***********************
    //[Crittercism enableWithAppID:CRITTERCISM_APP_ID];
    
    NSString *bugsenseKey = nil;
#if MYFIELDFORCE || LIVE
    bugsenseKey = BUGSENSE_API_KEY_LIVE;
#elif GENERIC_DEMO
    bugsenseKey = BUGSENSE_API_KEY_DEMO;
#elif LIVE_TEST
    bugsenseKey = BUGSENSE_API_KEY_TEST;
#else
    bugsenseKey = BUGSENSE_API_KEY;
#endif
    
    
    if(bugsenseKey)
        [BugSenseController sharedControllerWithBugSenseAPIKey:bugsenseKey];
    
    
  //  NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[[UIDevice currentDevice] name], @"device", nil];
  //  if(bugsenseKey)
  //  [BugSenseController sharedControllerWithBugSenseAPIKey:bugsenseKey
                                        //    userDictionary:dictionary];
    
 

}
#pragma mark -

-(void)__transferUserDefaultsToKeychain
{
    NSUserDefaults *_defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray *keys = [[_defaults dictionaryRepresentation] allKeys];
    
    NSString *bundleIdentifier = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
    
    //LOGIN_EMAIL_KEY
   // LOGIN_PASSWORD_KEY
    
    for(NSString* key in keys)
    {
        id val =  [_defaults valueForKey:key];
        
        if([val isKindOfClass:[NSString class]])
        {
           if(![SSKeychain setPassword:val forService:key account:bundleIdentifier])
           {
               break;
           }
        }
    }
}

#pragma mark - REPORT MEMORY

#if !MYFIELDFORCE
-(void)_report_memory
{
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(),
                                   TASK_BASIC_INFO,
                                   (task_info_t)&info,
                                   &size);
    if( kerr == KERN_SUCCESS )
    {
        NSLog(@"Memory in use (in bytes): %u", info.resident_size);
        
        NSString *_memString = [NSByteCountFormatter stringFromByteCount:info.resident_size countStyle:NSByteCountFormatterCountStyleFile];
        
        [UIAlertView showWarningAlertWithTitle:@"Memory" message:[NSString stringWithFormat:@"Total memory used (in bytes) : %@",_memString]];
        
    } else {
        NSLog(@"Error with task_info(): %s", mach_error_string(kerr));
    }
}
#endif
#pragma MARK -


-(void)test
{

    //@"journal_mode": @"DELETE",@"synchronous": @"NORMAL"
    
    

    return;
    
    NSArray *_arr = [NSArray array];
    
    [_arr objectAtIndex:9];
    
    return;
    NSString *email = @"jlavery@city-automotive.com.nz";
    
        
    BOOL isValid= [email isValidEmail];
    

    
    return;
    
    [self performBlock:^{
     
        NSLog(@"Performed block");
        
    } afterDealy:1.0];

    return;
    //======================= SERVER MAINTENANCE CHECHING ====================================
    __block ASIHTTPRequest *request = [ConnectionManager createServerMaintenanceTestConnection];
    [request setCompletionBlock:^{
        
        NSLog(@"Response=%@",[request responseString]);
        NSDictionary *jsonDict = [request responseJSON];
        
        jsonDict = NULL_NIL(jsonDict);
        

        NSNumber *isUnderMaintenance  = [jsonDict objectForKey:JSON_UNDER_MAINTENANCE_KEY];
        isUnderMaintenance = NULL_NIL(isUnderMaintenance);
        
        if([isUnderMaintenance boolValue])
        {
            [app showUnderMaintainenceScreen:YES];
        }
        
        app.isServerUnderMaintenance = [isUnderMaintenance boolValue];
        
    }];
    return;
    
    
    NSMutableArray *set=[NSMutableArray array];
    [set objectAtIndex:9999999];
    
    for(int i=0;i<1000;i++)
    {
        NSString *str=[self globalString];
        [set addObject:str];
    }
    
    [set objectAtIndex:9999999];
   
    
    NSLog(@"Count=%d",[set count]);
    
    
    return;
    
    // NSDate *_date=   [NSDate dateFromWCFTimeInterval:1359622402630];
    // NSLog(@"Date=%@",_date);

    NSArray *arr = [NSArray arrayWithObjects:@"aab",@"HSV",@"Holden",@"Hoa",@"Hob", nil];
    NSMutableArray *arr2=[NSMutableArray array];
    for(NSString *name in arr)
    {
        NSDictionary *dict = [NSDictionary dictionaryWithObject:name forKey:@"name"];
        [arr2 addObject:dict];
    }
    
    NSSortDescriptor *descriptor  =[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    //[[NSSortDescriptor alloc] initWithKey:@"name" ascending:NO];
    [arr2 sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    NSLog(@"Arr=%@",arr2);

}

- (void)applicationProtectedDataDidBecomeAvailable:(UIApplication *)application
{
    
}

#pragma mark - LOCATION MANAGER
-(void)registerLocationService
{
    __block  LocationManager *_locationManager = [LocationManager sharedManager];
    [_locationManager setDistanceFilter:LOCATION_FILTER_RADIUS accuracy:kCLLocationAccuracyHundredMeters];
    
    [_locationManager addLocationDidUpdateCallback:^(CLLocation *newLocation)
     {
         if(newLocation.coordinate.latitude!=0.0 && newLocation.coordinate.longitude!=0.0);
         {
           [ConnectionManager sendDeviceLocation:newLocation.coordinate];
         }
         
    }];
    
     [_locationManager setEnableBackgroundTracking:YES];
     [_locationManager startUpdatingLocation];
    

    /*
     bgTask = [[UIApplication sharedApplication]
     beginBackgroundTaskWithExpirationHandler:
     ^{
     [[UIApplication sharedApplication} endBackgroundTask:bgTask];
     }];
     
     // ANY CODE WE PUT HERE IS OUR BACKGROUND TASK
     
     // For example, I can do a series of SYNCHRONOUS network methods (we're in the background, there is
     // no UI to block so synchronous is the correct approach here).
     
     // ...
     
     // AFTER ALL THE UPDATES, close the task
     
     if (bgTask != UIBackgroundTaskInvalid)
     {
     [[UIApplication sharedApplication} endBackgroundTask:bgTask];
     bgTask = UIBackgroundTaskInvalid;
     }
     */
}

-(void)unregisterLocationService
{
    [[LocationManager sharedManager] stopUpdatingLocation];
    [[LocationManager sharedManager] resetLocation];
}

#pragma mark -

-(void)hideInitializerLoading
{
     if([NSThread isMainThread])
     {
       [ProgressHUD hideHUDForView:self.window animated:YES];
     }
     else
     {
        [self performSelector:@selector(hideInitializerLoading) onThread:[NSThread mainThread] withObject:nil waitUntilDone:YES];
     }
}

-(NSString *)globalString
{
    NSUUID  *identifierForVendor  = [[UIDevice currentDevice] identifierForVendor];
    //NSString *deviceID   = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
   // NSUInteger _dateHash = [[NSDate date] hash];
    
    CFUUIDRef uuidObj = CFUUIDCreate(nil);
    NSString *newUUID = (NSString*)CFUUIDCreateString(nil, uuidObj);

   // NSString  *uuistr   = [[NSProcessInfo processInfo] globallyUniqueString];
    NSString  *uuistr   = newUUID; 
    NSUInteger intHash  = [uuistr hash];

    NSString *uniqueStr = [NSString stringWithFormat:@"%u%u",[identifierForVendor hash] ,intHash];
    
     NSLog(@"uniqueName=%@",uniqueStr);
    
    CFRelease(uuidObj);
    [newUUID release];
    
    return uniqueStr;
}

-(void)removeAllEventsFromCalendar
{
    
}


-(void)setShouldAvoidMenuSelection:(BOOL)_shouldAvoidMenuSelection
{
    if(shouldAvoidMenuSelection!=_shouldAvoidMenuSelection)
    {
        shouldAvoidMenuSelection = _shouldAvoidMenuSelection;
        
        UIViewController *masterController = app.splitViewController.masterController;
        masterController.view.userInteractionEnabled = !shouldAvoidMenuSelection;
    }
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    
    [[OfflineManager sharedManager] stopNotifier];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
    
    NSLog(@"Papp state : %d", [[UIApplication sharedApplication] applicationState]);
    lastAppState = [[UIApplication sharedApplication] applicationState];
    
     if(isLoggedIn)
     {
         /*
         UIBackgroundTaskIdentifier backgroundTaskIdentifier = [application beginBackgroundTaskWithExpirationHandler:^(void) 
         {
             [[OfflineManager sharedManager] saveToFile];
             
             [application endBackgroundTask:backgroundTaskIdentifier];
             
         }];
         */
         
        [[OfflineManager sharedManager] saveToFile];
     }
      //=======================================================
   // [[CustomEventStore sharedInstance].eventStore commit:nil];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"app state : %d", [[UIApplication sharedApplication] applicationState]);
    [[OfflineManager sharedManager] startNotifier];
    
    [self sendRequestForApplicationDownloadFromPushNotification:NO];
    
    [self checkAndEnableWhatsNew];
    
#if ENABLE_BACKUP
    if([AppDelegate isLoggedIn])
    [DirectoryManager createBackupZipWithPassword:nil];
#endif
    
}

#pragma mark - Whats new
-(void)checkAndEnableWhatsNew
{
    NSString *lastVersion = [[NSUserDefaults standardUserDefaults] objectForKey:LAST_VERSION_KEY];
    NSString *currentVersion = APP_VERSION;
    
    
    BOOL isHigherVersion= ([currentVersion compare:lastVersion options:NSNumericSearch] == NSOrderedDescending);
    
    if(isHigherVersion)
    {
        NSTimeInterval gmtTimeInterval = [[[NSDate date] dateEnd] GMTTimeIntervalSince1970];
       // NSTimeInterval gmtTimeInterval = [[NSDate date] GMTTimeIntervalSince1970];
        [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithDouble:gmtTimeInterval] forKey:LAST_VERSION_UPDATED_DATE];
        
        [[NSUserDefaults standardUserDefaults] setObject:currentVersion forKey:LAST_VERSION_KEY];
    }
}

#pragma mark SHOW MAINTENANCE ALERT
-(void)showUnderMaintainenceScreen:(BOOL)shouldLogout
{
    /*
    WindowOverlayView *containerView = (WindowOverlayView *) [self.window viewWithTag:99];
    
    if(!containerView)
    {
        containerView = [[WindowOverlayView alloc] initWithFrame:self.window.bounds];
        containerView.tag = 99;
        containerView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        [self.window addSubview:containerView];
        [containerView release];
    }
    */

    if(self.isServerUnderMaintenance) return;
    
     self.isServerUnderMaintenance = YES;
    
    NSString *message = [NSString stringWithFormat:@"%@",FIELDFORCE_DOWN_MESSAGE];

    UIAlertView *alert = [UIAlertView alertViewWithTitle:@"" message:message cancelButtonTitle:@"Ok" otherButtonTitles:nil onDismiss:^(int buttonIndex)
            {
                               
            }
            onCancel:^{
                                                     
            if(isServerUnderMaintenance && shouldLogout)
                [self appDidLogOut];
                                                     
        }];
    
    [alert release];


    /*
    [self performBlock:^{

        self.isServerUnderMaintenance = NO;

    } afterDealy:SERVER_MAINTENANCE_DELAY];
    */
}


#pragma mark -
- (void)sendRequestForApplicationDownloadFromPushNotification:(BOOL)IsFromPushNotification
{
   __block ASIHTTPRequest *request = [ConnectionManager checkApplicationVersionWithJson:nil];
    [request setCompletionBlock:^{
        
        NSLog(@"%@",request.responseString);
        id respone =  [request responseJSON];
        
        respone = NULL_NIL(respone);
        
        if(respone)
        {
            NSString *latestVersion =  [respone valueForKey:@"appVersion"];

            
            NSLog(@" latestVersion = %@",latestVersion);
            
           // float currentVersion  = [APP_VERSION floatValue];
           // float newVersion = [latestVersion floatValue];
            
             BOOL isHigherVersion = ([latestVersion compare:APP_VERSION options:NSNumericSearch] == NSOrderedDescending);
            
            //----------------------- Added on 6DEC,2013 ------------------------------
              NSNumber *buildVersionDate = [respone valueForKey:@"versionDate"];
              if(buildVersionDate)
              {
                [[NSUserDefaults standardUserDefaults] setObject:buildVersionDate forKey:BUILD_VERSION_UPLOADED_DATE];
              }
            //-----------------------------------------------------------------------------------------
            
           // if (currentVersion < newVersion)
            if(isHigherVersion)
            {
                @try
                {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:BUILD_VERSION_UPDATED];
                    
              
                    NSString* appDownloadURLString = [respone valueForKey:@"appDownloadLink"];
                    
                    if (!IsFromPushNotification)
                    {
                        
                    UIAlertView *alert=[UIAlertView alertViewWithTitle:nil message:APP_DOWNLOAD_MSG cancelButtonTitle:@"OK" otherButtonTitles:nil onDismiss:^(int buttonIndex)
                        {
                            
                        }
                        onCancel:^{
                          
                          [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                           NSLog(@"appDownloadURLString = %@", appDownloadURLString);
                          
                          NSURL* appDownloadURL = nil;
                          if ([appDownloadURLString length])
                              appDownloadURL = [NSURL URLWithString:appDownloadURLString];
                          if (appDownloadURL)
                          {
                              [self appDidLogOut];
                              
                              [[UIApplication sharedApplication] openURL:appDownloadURL];
                          }
                      }];
                    [alert release];
                    }
                    else
                    {
                        NSLog(@"appDownloadURLString = %@", appDownloadURLString);
                        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                        NSURL* appDownloadURL = nil;
                        
                        if ([appDownloadURLString length])
                            appDownloadURL = [NSURL URLWithString:appDownloadURLString];
                        
                        if (appDownloadURL)
                        {
                            [self appDidLogOut];
                            
                            [[UIApplication sharedApplication] openURL:appDownloadURL];
                        }
                    }
                    
                }
                @catch (NSException* e)
                {
                    
                }
            }
            else
            {
                [UIApplication sharedApplication].applicationIconBadgeNumber=0;
            }
        }
    }];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
   // [self saveContext];
    
    
}

-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    Class class = NSClassFromString(@"TakePhotoViewController");
    
    UINavigationController *navController = (UINavigationController *)self.splitViewController.modalViewController;
    
   if( [navController.topViewController isMemberOfClass:class])
   {
       return UIInterfaceOrientationMaskAll;;
   }
    /*
   else 
    {   
        class = NSClassFromString(@"CalendarViewController");
        
        if( [navController.topViewController isMemberOfClass:class])
        {
             return [UIApplication sharedApplication].statusBarOrientation;
        }
    }
    */
    return UIInterfaceOrientationMaskLandscape;
    
    //return UIInterfaceOrientationLandscapeRight | UIInterfaceOrientationLandscapeLeft;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

#pragma mark PUSH NOTIFICATION DELEGATES

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{

    NSString *oldToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    
    NSString* newToken = [deviceToken description];
    newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    if(oldToken && [oldToken isEqualToString:newToken]) return;
        
    //==============  NOW SEND DEVICE TOKEN ======================
    
    //[UIAlertView showWarningAlertWithTitle:@"notification send" message:[NSString stringWithFormat:@"Push Notification registered for token:%@",newToken]];
  
    //NSLog(@"My newToken is: %@", newToken);
    
    NSMutableDictionary* deviceTokenDict = [NSMutableDictionary dictionary];
    [deviceTokenDict setValue:newToken?newToken:@"" forKey:@"deviceToken"];
    [deviceTokenDict setValue:[[UIDevice currentDevice] uniqueGlobalDeviceIdentifier] forKey:@"deviceID"];
    
    NSString* jsonString = [deviceTokenDict JSONRepresentation];
    
    ASIHTTPRequest *request = [ConnectionManager createDeviceTokenWithJson:jsonString];
    
    [request setCompletionBlock:^{
        
        //NSLog(@"%@",request.responseString);
        //id respone = [request responseJSON];
        
        if(newToken)
        {
            [[NSUserDefaults standardUserDefaults] setObject:newToken forKey:@"deviceToken"];
        }
    }];
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
    
#if LOCAL
    [UIAlertView showWarningAlertWithTitle:@"APNS failed" message:@""];
#endif
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //if (![UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
   //
    //}
    
    
#if LOCAL
    NSString *json = [userInfo JSONRepresentation];
    [UIAlertView showWarningAlertWithTitle:@"notification" message:json];
#endif
    
    NSString *messageType = [userInfo valueForKey:@"type"];
    
    if([messageType isEqualToString:@"maintenance"])
    {
        //======== SHOW SERVER MAINTENANCE NOTIFICATION ALERT ==============
        
        NSString *message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        UIAlertView *alert =  [UIAlertView alertViewWithTitle:nil message:message cancelButtonTitle:@"Ok" otherButtonTitles:nil onDismiss:^(int buttonIndex) {
            
        } onCancel:^{
            
            
        }];
        
        [alert release];
    }
    else //======== SHOW APP DOWNLOAD NOTIFICATION ALERT ==============
    {

    
        if (lastAppState != UIApplicationStateBackground)
        {
           
            [self showMessageFromRemoteNotification:userInfo];
        }
        
        lastAppState = [[UIApplication sharedApplication] applicationState];
            
    }
}

- (void)showMessageFromRemoteNotification:(NSDictionary*)userInfo
{
      
       UIAlertView *alert =  [UIAlertView alertViewWithTitle:nil message:APP_DOWNLOAD_MSG cancelButtonTitle:@"Ok" otherButtonTitles:nil onDismiss:^(int buttonIndex) {
      
        } onCancel:^{
        [self sendRequestForApplicationDownloadFromPushNotification:YES];
        
       }];

       [alert release];
    
}
#pragma mark -

#pragma mark - Core Data stack


#pragma mark - Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
@end

NSString  *  AppDidLoginNotification  = @"SS_AppDidLoginNotificationKey";
NSString  *  AppDidLogoutNotification = @"SS_AppDidLogoutNotificationKey";
NSString  *  AppDidChangeSiteNotification = @"SS_AppDidChangeSiteNotificationKey";
NSString  *  RepDidDeactivateNotification = @"SS_RepDidDeactivateNotificationKey";

@implementation AppDelegate (LoginManager)

+(BOOL)isLoggedIn
{
    isLoggedIn=[[[NSUserDefaults standardUserDefaults] objectForKey:LOGIN_CHECK_KEY] boolValue];
    return isLoggedIn;
}

+(NSUInteger)loginHash
{
    NSString *loginEmail = [[NSUserDefaults standardUserDefaults] objectForKey:LOGIN_EMAIL_KEY];
    
    return [loginEmail hash];
}

+(NSString *)loginEmail
{
    NSString *_loggedinEmail = [[NSUserDefaults standardUserDefaults] objectForKey:LOGIN_EMAIL_KEY];
    return _loggedinEmail;
}

+(NSString *)eventLocationString
{
    NSString *str=[[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleDisplayName"];
   
    // NSUInteger _loginHash=[AppDelegate loginHash];
    //NSString *locationStr=[NSString stringWithFormat:@"%@%d",str,_loginHash];
    
    NSString *_loginEmail = [AppDelegate loginEmail];
    NSString *locationStr = [NSString stringWithFormat:@"%@.%@",str,_loginEmail];
    
     NSLog(@"str]------%@",locationStr);

    return locationStr;
}

-(void)appDidLogOut
{
    if(!isLoggedIn) return;
    
    isLoggedIn=NO;
    
    app.isCustomerFetching = NO;
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LOGIN_CHECK_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LOGIN_EMAIL_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LOGIN_PASSWORD_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:AppDidLogoutNotification object:nil];
    
    [self performSelector:@selector(showLogin)];
    
    [self.window animateWithTransition:UIViewAnimationTransitionCurlUp  duration:0.5];
    
    [[OfflineManager sharedManager] saveToFile];
    
    [self unregisterLocationService];
}


-(void)appDidLogInWithEmail:(NSString *)email password:(NSString *)password
{
    /*
    isLoggedIn=YES;
    
     NSString *lastLoginEmail = [[NSUserDefaults standardUserDefaults] objectForKey:LOGIN_EMAIL_KEY];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:isLoggedIn] forKey:LOGIN_CHECK_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:email forKey:LOGIN_EMAIL_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:password forKey:LOGIN_PASSWORD_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:AppDidLoginNotification  object:nil];
    
    
   
    /// ============ create login directory and setup persistent store coordinator ===========
    
    if(!lastLoginEmail.length || ![lastLoginEmail isEqualToString:email])
    {
      NSNumber *hash= [NSNumber numberWithUnsignedInteger:[email hash]];
      NSString *fileName = [hash description];
      [DirectoryManager createLoginDirectoryPathForName:fileName]; 
    
    NSString *loginDirectoryPath = [DirectoryManager loginDirectoryPath];
    NSString *dbFilePath = [loginDirectoryPath stringByAppendingFormat:@"/%@.sqlite",fileName];
    
    if(! coreDataHandler) 
        coreDataHandler=[[CoreDataHandler alloc] initWithPersistentStorePath:dbFilePath];
    else
        coreDataHandler.storePath = dbFilePath;
    //==============================================================================================
    }
    
    
    [self performSelector:@selector(showLogin)];
    [self.window animateWithTransition:UIViewAnimationTransitionCurlDown  duration:0.5];
    */
}

-(void)appDidLogIn
{
    _siteIDChanged    = NO;
    _isRepDeactivated = NO;
    isLoggedIn=YES;
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:isLoggedIn] forKey:LOGIN_CHECK_KEY];
 
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:AppDidLoginNotification  object:nil];
    
    [self performSelector:@selector(showLogin)];
    [self.window animateWithTransition:UIViewAnimationTransitionCurlDown  duration:0.5];
    
#if BACKGROUND_GPS_ENABLED
    [self registerLocationService];
#endif
    
}

-(void)appDidChangeSiteID
{
    if(_siteIDChanged) return;
    
    _siteIDChanged=YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:AppDidChangeSiteNotification object:nil];
    

    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:APP_DIDCHANGE_SITE_MESSAGE
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles: nil];
    [alert show];
    [alert release];
    
    /*
    [UIAlertView alertViewWithTitle:@"" message:APP_DIDCHANGE_SITE_MESSAGE cancelButtonTitle:@"Ok" otherButtonTitles:nil onDismiss:^(int buttonIndex) 
    {
       
    } 
    onCancel:^{
        
    @try {
            
     _siteIDChanged=NO;
    [self  resetDB];
            
    }
    @catch (NSException *exception) {
            
    }
    @finally {
      
        [self appDidLogOut];
    }
    
        
    }];
    */
}

-(void)repDidDeactivate
{
    if(_isRepDeactivated) return;
        
    _isRepDeactivated = YES;
    
     [[NSNotificationCenter defaultCenter] postNotificationName:RepDidDeactivateNotification object:nil];
    
   UIAlertView *alert =  [UIAlertView alertViewWithTitle:@"" message:REP_DID_DEACTIVATE_MESSAGE cancelButtonTitle:@"Ok" otherButtonTitles:nil onDismiss:^(int buttonIndex)
     {
         
     }
    onCancel:^{
     
    if(_isRepDeactivated)
    [self appDidLogOut];
    
    }];
    
    [alert release];

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(!_siteIDChanged) return;
    
    @try
    {
       
      //  [[OfflineManager sharedManager] saveToFile];
        [self  resetDB];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception name=%@ reason=%@",exception.name,  exception.reason);
        
    }
    @finally 
    {
        [self appDidLogOut];
        //_siteIDChanged=NO;
    }

}
@end

@implementation AppDelegate (LocalNotification)


+ (void) scheduleNotificationOn:(NSDate*) fireDate
                           text:(NSString*) alertText
                      hasAction:(BOOL)hasAction
                         action:(NSString*) alertAction
                          sound:(NSString*) soundfileName
                    launchImage:(NSString*) launchImage 
                        andInfo:(NSDictionary*) userInfo

{
	UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = fireDate;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];	
	
    if(alertText)
        localNotification.alertBody = alertText;
    localNotification.hasAction=hasAction;
    if(alertAction)
        localNotification.alertAction = alertAction;	
	
	if(soundfileName == nil)
	{
		localNotification.soundName = UILocalNotificationDefaultSoundName;
	}
	else 
	{
		localNotification.soundName = soundfileName;
	}
    
    
    if(launchImage)
        localNotification.alertLaunchImage = launchImage;
	
	//self.badgeCount ++;
    //localNotification.applicationIconBadgeNumber = self.badgeCount;	
    if(userInfo)
        localNotification.userInfo = userInfo;
	
	// Schedule it with the app
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    [localNotification autorelease];//ADDED BY SUBHO
}

+(void)cancelLastLocalNotification
{
    NSArray *arr=[[UIApplication sharedApplication] scheduledLocalNotifications];
    if(![arr count]) return;
    
    UILocalNotification *localNotification= [arr lastObject];
    [[UIApplication sharedApplication] cancelLocalNotification:localNotification];
}

+(void)cancelAllLocalNotifications
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
}

+(void)didPostLocalNotification:(UILocalNotification *)notification
{
    [[NSNotificationCenter defaultCenter]
	 postNotificationName:kApplicationDidTimeoutNotification object:notification];
}


+(void)resetIdleTimer
{
    
   // isalert=[self doesAlertViewExist];
    
   // NSLog(@" isalert %d",isalert);
    
   // [AppDelegate cancelLastLocalNotification];
    [AppDelegate cancelAllLocalNotifications];
	
	// Schedule a timer to fire in kApplicationTimeoutInMinutes * 60
	int timeout = kApplicationTimeoutInMinutes * 60;
    
    NSDate *fireDate = [[NSDate date] dateByAddingTimeInterval:timeout];
    
    
    [AppDelegate scheduleNotificationOn:fireDate text:@"Visit is about to expire.\nDo you want to continue?" hasAction:YES action:@"YES" sound:nil launchImage:nil andInfo:nil];
    
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification 
{
   // NSLog(@"All local notificatrions:%d",[[[UIApplication sharedApplication] scheduledLocalNotifications] count]);
   // NSDate lastDate = notification.fireDate;
   // [AppDelegate didPostLocalNotification:notification];
    
}

/*
-(void)setIsVisitSessionRunning:(BOOL)_isVisitSessionRunning
{
    if(isVisitSessionRunning!=_isVisitSessionRunning)
    {
        isVisitSessionRunning = _isVisitSessionRunning;
        
        [AppDelegate cancelLastLocalNotification];
        
        if(_isVisitSessionRunning)
        {
            int timeout = kApplicationTimeoutInMinutes * 60;
            
            NSDate *fireDate = [[NSDate date] dateByAddingTimeInterval:timeout];
            //isalert=[self doesAlertViewExist];
            
            NSLog(@" isalert %d",isalert);

            [AppDelegate scheduleNotificationOn:fireDate text:@"Visit is about to expire.\nDo you want to continue this session?" hasAction:YES action:@"YES" sound:nil launchImage:nil andInfo:nil];
        }
        else
        {
           // [AppDelegate cancelLastLocalNotification];
        }
    }
}
*/
/*
 //-----------------
 if(loginEmail)
 {
 NSNumber *isDeletedNum= [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_isDeleted",loginEmail]];
 
 if(![isDeletedNum boolValue])
 {
 NSNumber *hash= [NSNumber numberWithUnsignedInteger:[loginEmail hash]];
 NSString *fileName = [NSString stringWithFormat:@"%@",hash];
 [DirectoryManager createLoginDirectoryPathForName:fileName];
 NSString *loginDirectoryPath = [DirectoryManager loginDirectoryPath];
 NSString *dbFilePath = [loginDirectoryPath stringByAppendingFormat:@"/%@.sqlite",fileName];
 
 if([[NSFileManager defaultManager] fileExistsAtPath:dbFilePath])
 {
 BOOL _removed =  [[NSFileManager defaultManager] removeItemAtPath:dbFilePath error:nil];
 }
 
 [[NSUserDefaults standardUserDefaults] removeObjectForKey:loginEmail];
 [[NSUserDefaults standardUserDefaults] removeObjectForKey:LOGIN_EMAIL_KEY];
 [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:[NSString stringWithFormat:@"%@_isDeleted",loginEmail]];
 
 loginEmail = nil;
 }
 }
 */
//-------------

@end
