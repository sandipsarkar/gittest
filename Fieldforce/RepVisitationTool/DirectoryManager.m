//
//  ImageDirectory.m
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import "DirectoryManager.h"
#import "ZipArchive.h"


//static NSString *curentLoginDirPath=nil;

@implementation DirectoryManager

+(NSString *)documentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); 
    NSString *documentsDirectory = [paths objectAtIndex:0]; 
    
    return documentsDirectory;
}

+(NSString *)libraryDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libDirectory = [paths objectAtIndex:0];
    
    return libDirectory;
}




+(BOOL)createLoginDirectoryPathForName:(NSString *)name
{
    NSString *docuDir    = [self documentsDirectory];
    NSString *libDir     = [self libraryDirectory];
   
    
#if ENABLE_BACKUP
    
    NSString *newDirPath = [libDir stringByAppendingPathComponent:@"rep_directory"];
    NSString *oldDirPath = [docuDir stringByAppendingPathComponent:@"rep_directory"];
        
#else
    
    NSString *newDirPath = [docuDir stringByAppendingPathComponent:@"rep_directory"];
    NSString *oldDirPath = [libDir stringByAppendingPathComponent:@"rep_directory"];
    
#endif
    
    //========== MOVE "rep_directory" FOLDER FROM 'old Directory' TO 'new Directory' ============
    BOOL isDirectory;
    if([[NSFileManager defaultManager] fileExistsAtPath:oldDirPath isDirectory:&isDirectory] && isDirectory)
    {
        [[NSFileManager defaultManager] moveItemAtPath:oldDirPath toPath:newDirPath error:nil];
    }

        
   // [self addSkipBackupAttributeToItemAtPath:newDirPath];
    
    newDirPath=[newDirPath stringByAppendingPathComponent:name];
    
    //curentLoginDirPath = [newDirPath copy];
    
    if(newDirPath)
    [[NSUserDefaults standardUserDefaults] setObject:newDirPath forKey:@"rep_directorypath_tool"];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:newDirPath])
    {
        BOOL b =([[NSFileManager defaultManager] createDirectoryAtPath:newDirPath withIntermediateDirectories:YES attributes:nil error:nil]);
        
        return b;
    }
    else
    {
        return YES;
    }
    
    
    return NO;
}

+(NSString *)loginDirectoryPath
{
    NSString *loginDirectoryPath= [[NSUserDefaults standardUserDefaults] objectForKey:@"rep_directorypath_tool"];
    
    return loginDirectoryPath;
}

+(NSString *)customerImagePathForName:(NSString *)name
{
    NSString *docuDir=[self loginDirectoryPath];
    
    NSString *dataPath = [docuDir stringByAppendingPathComponent:@"customerImages"];
    NSError *error=nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    return error ? nil : [dataPath stringByAppendingPathComponent:name];
}

+(NSString *)offlineDataPathForName:(NSString *)name
{
    NSString *docuDir=[self loginDirectoryPath];
    
    NSString *dataPath = [docuDir stringByAppendingPathComponent:@"offlineData"];
    NSError *error=nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    return error ? nil : [dataPath stringByAppendingPathComponent:name];
}

+(NSString *)drawingDirectoryPathForName:(NSString *)name
{
    if(!name.length) return nil;
    
    NSString *docuDir=[self loginDirectoryPath];

    NSString *dataPath = [docuDir stringByAppendingPathComponent:@"RepToolDrawingPath"];
    NSError *error=nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    return error ? nil : [dataPath stringByAppendingPathComponent:name];
}
+(NSString *)notesDirectoryPathForName:(NSString *)name
{
    if(!name.length) return nil;

    NSString *docuDir=[self loginDirectoryPath];
    NSString *dataPath = [docuDir stringByAppendingPathComponent:@"RepToolNotesPath"];
    NSError *error=nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    return error ? nil : [dataPath stringByAppendingPathComponent:name];
}


+(NSString *)imageDirectoryPath
{ 
    NSString *documentsDirectory =[self loginDirectoryPath];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"RepToolImages"];
    
    NSError *error=nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    return error ? nil : dataPath;
}

+(NSString *)pdfDirectoryPath
{
    NSString *documentsDirectory = [self loginDirectoryPath];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"RepToolPDFs"];
    
    NSError *error=nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    return error ? nil : dataPath;
}


+(NSString *)thumbImageDirectoryPath
{
    NSString *documentsDirectory =[self loginDirectoryPath];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"ThumbImages"];
    
    NSError *error=nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:YES attributes:nil error:&error];
    
    }
    
    return error ? nil : dataPath;
}

+(NSString *)newSuburbsPath
{
    NSString *docuDir    = [self documentsDirectory];
    NSString *newSuburbPath = [docuDir stringByAppendingPathComponent:@"newSuburbs.plist"];
    
    return newSuburbPath;
}

+(NSString *)suburbsPath
{
    NSString *docuDir    = [self documentsDirectory];
    NSString *newSuburbPath = [docuDir stringByAppendingPathComponent:@"suburbs.plist"];
    
    return newSuburbPath;
}

+(NSString *)recoredAudioPathForName:(NSString *)name
{
    if(!name.length) return nil;
    
    NSString *docuDir = [self documentsDirectory];
    
    NSString *dataPath =[docuDir stringByAppendingPathComponent:@"SessionAudio"];;
    NSError *error=nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    return error ? nil : [dataPath stringByAppendingPathComponent:name];
}

+(NSString *)oldDatabasePathForName:(NSString *)name
{
    NSString *docuDir=[self loginDirectoryPath];
    
    NSString *dataPath = [docuDir stringByAppendingPathComponent:@"oldDB"];
    NSError *error=nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    return error ? nil : [dataPath stringByAppendingPathComponent:name];
}

#pragma mark PREVENT FILE FROM BACKUP TO iTunes/iCloud
+(BOOL)addSkipBackupAttributeToItemAtPath:(NSString *)path
{
    NSURL *URL = [NSURL fileURLWithPath:path];
    
   // assert([[NSFileManager defaultManager] fileExistsAtPath:path]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}
#pragma mark -

+(void)createBackupZipWithPassword:(NSString *)password
{
#if ENABLE_BACKUP
    
    NSString *lastBackupDateString = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastBackupDate"];
    if(lastBackupDateString)
    {
        NSDate *lastBackupDate = [NSDate dateFromString:lastBackupDateString withFormat:@"ddMMyyhhmmssSS"];
        
        NSTimeInterval timeDiff = [[NSDate date] timeIntervalSinceDate:lastBackupDate];
        
       //******* TAKE BACKUP ONCE IN A DAY ************//
        if(timeDiff<60*60*24) /*** 24 hours ***/
        {
            return;
        }
    }
    
    NSString *docuDir    = [self documentsDirectory];
    //NSString *loginEmail = [[NSUserDefaults standardUserDefaults] objectForKey:LOGIN_EMAIL_KEY];
    NSString *loginDirectoryPath = [DirectoryManager loginDirectoryPath];

    NSString *dateString = [[NSDate date] stringFromDateWithFormat:@"ddMMyyhhmmssSS"];
    NSString *targetPath = [docuDir stringByAppendingFormat:@"/backup_%@.zip",dateString];
    
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    __block  BOOL ret = [zipArchive CreateZipFile2:targetPath Password:password?password:dateString];
    
    NSString *parentdirectory = [loginDirectoryPath stringByDeletingLastPathComponent];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        if(ret)
            ret = [zipArchive addFolderToZip:parentdirectory pathPrefix:nil];
        
        if(ret && ![zipArchive CloseZipFile2] )
        {
            NSLog(@"Error Generating zip");
        }
        else
        {
             NSLog(@"Backup zip generated");
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[NSUserDefaults standardUserDefaults] setObject:dateString forKey:@"lastBackupDate"];
             [zipArchive release];
        });

    });
    
#endif
    
}

+(NSMutableArray *)getRecentChangedFilesForDirectory:(NSString *)directoryPath
{
  
    NSMutableArray *changedFiles = [NSMutableArray array];
    NSDirectoryEnumerator *directoryEnumerator = [[NSFileManager defaultManager] enumeratorAtPath:directoryPath];
    
    NSDate *yesterday = [NSDate dateWithTimeIntervalSinceNow:(-60*60*24)]; //CHECK WITHIN 24 HOURS
    
    for (NSString *path in directoryEnumerator)
    {
        
        if ([[path pathExtension] isEqualToString:@"rtfd"])
        {
            // Don't enumerate this directory.
            [directoryEnumerator skipDescendents];
        }
        else
        {
            
            NSDictionary *attributes = [directoryEnumerator fileAttributes];
            NSDate *lastModificationDate = [attributes objectForKey:NSFileModificationDate];
            
            if ([yesterday earlierDate:lastModificationDate] == yesterday)
            {
                [changedFiles addObject:path];
                NSLog(@"%@ was modified within the last 24 hours", path);
            }
        }
    }
    
    return changedFiles;
}

#pragma mark - RESOURCE FILE PATH
+(NSString *)resourceFilePathWithName:(NSString *)fileName
{
    NSString *libraryDirectory = [self libraryDirectory];
    
    NSString *dataPath = [libraryDirectory stringByAppendingPathComponent:@"Resources"];
    NSError *error=nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    return error ? nil : [dataPath stringByAppendingPathComponent:fileName];
    
}

+(NSString *)tempResourceFilePathWithName:(NSString *)fileName
{
    NSString *tempDirectory = NSTemporaryDirectory();
    
    NSString *_fileName = [fileName stringByAppendingString:@".download"];
    
   return  [tempDirectory stringByAppendingPathComponent:_fileName];
    
}


@end
