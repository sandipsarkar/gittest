//
//  WindowOverlayView.m
//  Fieldforce
//
//  Created by Sandip Sarkar on 30/05/13.
//  Copyright (c) 2013 RandemIT. All rights reserved.
//

#import "WindowOverlayView.h"

@implementation WindowOverlayView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSString *text =[@"Fieldforce is currently down for maintenance" uppercaseString];
        UIFont *font = [UIFont subaruBoldFontOfSize:28.0];
        CGSize labelSize = [text sizeWithFont:font];
        
       containerLabel = [[UILabel alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.bounds)-labelSize.width-40)/2.0, (CGRectGetHeight(self.bounds)-60.0)/2.0, labelSize.width+40, 60.0)];
        
        containerLabel.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        containerLabel.font  = font;
        containerLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        containerLabel.textAlignment = NSTextAlignmentCenter;
        containerLabel.textColor = [UIColor whiteColor];
        containerLabel.text  = text;
        [self addSubview:containerLabel];
        containerLabel.layer.cornerRadius =8.0;

        
        [self setTransformForCurrentOrientation:YES];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange:)
                                                     name:UIDeviceOrientationDidChangeNotification object:nil];

    }
    return self;
}

- (void)deviceOrientationDidChange:(NSNotification *)notification
{
	if ([self.superview isKindOfClass:[UIWindow class]])
    {
		[self setTransformForCurrentOrientation:YES];
	}

	self.frame = self.superview.bounds;
}

- (void)setTransformForCurrentOrientation:(BOOL)animated
{
	UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
	NSInteger degrees = 0;
	
	if (UIInterfaceOrientationIsLandscape(orientation))
    {
		if (orientation == UIInterfaceOrientationLandscapeLeft) { degrees = -90; }
		else { degrees = 90; }
	} else
    {
		if (orientation == UIInterfaceOrientationPortraitUpsideDown) { degrees = 180; }
		else { degrees = 0; }
	}
	
   CGAffineTransform rotationTransform = CGAffineTransformMakeRotation(((degrees * M_PI) / 180.0));
    
	if(animated)
    {
      [UIView beginAnimations:nil context:nil];
       self.transform = rotationTransform;
      [UIView commitAnimations];
    }
    else
    {
        self.transform = rotationTransform;
    }
	
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [containerLabel release];
    [super dealloc];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
