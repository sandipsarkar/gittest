//
//  main.m
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 08/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrackEventUIApplication.h"
#import "AppDelegate.h"

#define ENABLE_EVENT_TRACKING 0

int main(int argc, char *argv[])
{
    
       
    @autoreleasepool {
#if ENABLE_EVENT_TRACKING
        return UIApplicationMain(argc, argv, NSStringFromClass([TrackEventUIApplication class]), NSStringFromClass([AppDelegate class]));
#else
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
#endif
    }
}
