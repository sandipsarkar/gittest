//  AppDelegate.h
//  RepVisitationTool

//uname : info@randem.com.au
//pwd : R@nd3mxy10


//http://121.247.130.30:81/tms/fileupload.ashx

// TO DO:


//1- there's black screen which appears momentarily between image transitions in the photo gallery.
//2- Visit session Recording bar position to centre.
//3- 
// 

// ==================================== TODO  ====================================
//NSLayoutConstraint

// =================================== TODO  =====================================
#define  CalendarShouldUpdate  @"SS_CalendarShouldUpdate"

#define CUSTOM_SPLITSVC 0

#define SERVER_MAINTENANCE_DELAY 1

#import <UIKit/UIKit.h>
#import "UISplitViewController+DetailController.h"

#if CUSTOM_SPLITSVC
#import "ParallelStackViewController.h"
#endif

extern NSString *  AppDidLoginNotification;
extern NSString *  AppDidLogoutNotification;
extern NSString *  AppDidChangeSiteNotification;
extern NSString *  RepDidDeactivateNotification;
extern NSString *  VisitDidEndNotification;
extern NSString *  TaskDidEditNotification;
extern NSString *  TaskDidDeleteNotification;
extern NSString *  CustomerContactsDidLoad;
extern NSString *  CustomersDidFinishLoad;
extern NSString *  CustomerDidDeletedOnDuplicate;


@class AppDelegate;
extern AppDelegate *app;

@class LoginViewController;
@class CoreDataHandler;
@class Rep;
@class Customer;
@class SSTimer;

@interface AppDelegate : UIResponder <UIApplicationDelegate,UIAlertViewDelegate>
{
    
    // ************************ last application state *******************
    UIApplicationState lastAppState;
    
#if CUSTOM_SPLITSVC
    ParallelStackViewController *_splitViewController;
#else
    UISplitViewController *_splitViewController;
#endif
    
    //LoginViewController   *_loginViewController;
    UINavigationController * _navController;
    
    CoreDataHandler *coreDataHandler;
    Rep *_rep ;
    
    // ======================ADDED BY SUBHO=====================

    int fromCustomerTask;
    int inCalender;
    
    NSString *strDealingOemForVisitPhoto;
    
    BOOL _siteIDChanged;
    BOOL _isRepDeactivated;
    
    dispatch_queue_t _eventQueue ;
  
}
@property (nonatomic, retain) NSString *strDealingOemForVisitPhoto;
@property(nonatomic,assign)  int inCalender;
@property(nonatomic,assign)  int fromCustomerTask;

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,assign) BOOL isVisitSessionRunning;
@property (nonatomic,assign) BOOL isCustomerLoaded;
@property (nonatomic,assign) BOOL isCustomerFetching;
@property (nonatomic,copy)   NSString *offerTimeZoneName;

@property (nonatomic,assign) BOOL canAccessCalendar;
@property (nonatomic,assign) BOOL canAccessReminder;
@property (nonatomic,assign) int  eventFetchCount;
@property (nonatomic,assign) BOOL isServerUnderMaintenance;



#if CUSTOM_SPLITSVC
@property(strong,nonatomic)ParallelStackViewController *splitViewController;
#else
@property(strong,nonatomic)UISplitViewController *splitViewController;
#endif

@property(nonatomic,retain)CoreDataHandler *coreDataHandler;
@property(nonatomic,assign) BOOL shouldAvoidMenuSelection;


//-(void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

-(void)setupCoreDataHandlerForloginEmail:(NSString *)email;
-(void)resetDB;

-(void) fixRoundedSplitViewCorner;
-(Rep *)curentRep;
-(NSString *)globalString;

-(dispatch_queue_t)eventQueue;

-(void)registerLocationService;
-(void)unregisterLocationService;

@end


BOOL isLoggedIn;
@interface AppDelegate (LoginManager)

+(BOOL)isLoggedIn;
-(void)appDidLogOut;
-(void)appDidLogIn;

+(NSUInteger)loginHash;
+(NSString *)loginEmail;
+(NSString *)eventLocationString;

-(void)appDidChangeSiteID;
-(void)repDidDeactivate;
//-(void)appDidLogInWithEmail:(NSString *)email password:(NSString *)password;
-(void)showUnderMaintainenceScreen:(BOOL)shouldLogout;

@end

@interface AppDelegate (LocalNotification)

#define kApplicationTimeoutInMinutes 95
#define kApplicationDidTimeoutNotification @"ApplicationDidTimeout"


+ (void) scheduleNotificationOn:(NSDate*) fireDate
                           text:(NSString*) alertText
                      hasAction:(BOOL)hasAction
                         action:(NSString*) alertAction
                          sound:(NSString*) soundfileName
                    launchImage:(NSString*) launchImage 
                        andInfo:(NSDictionary*) userInfo;

+(void)cancelLastLocalNotification;
+(void)cancelAllLocalNotifications;

+(void)didPostLocalNotification:(UILocalNotification *)notification;
+(void)resetIdleTimer;
+(BOOL) doesAlertViewExist;

@end



