//
//  ImageDirectory.h
//  RepVisitationTool
//
//  Created by SANDIP SARKAR on 30/11/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DirectoryManager : NSObject

+(NSString *)documentsDirectory;
+(NSString *)libraryDirectory;
+(NSString *)loginDirectoryPath;
+(BOOL)createLoginDirectoryPathForName:(NSString *)name;

+(NSString *)imageDirectoryPath;
+(NSString *)pdfDirectoryPath;
+(NSString *)thumbImageDirectoryPath;
+(NSString *)drawingDirectoryPathForName:(NSString *)name;
+(NSString *)notesDirectoryPathForName:(NSString *)name;

+(NSString *)customerImagePathForName:(NSString *)name;
+(NSString *)offlineDataPathForName:(NSString *)name;
+(NSString *)recoredAudioPathForName:(NSString *)name;
+(NSString *)oldDatabasePathForName:(NSString *)name;

+(NSString *)resourceFilePathWithName:(NSString *)fileName;
+(NSString *)tempResourceFilePathWithName:(NSString *)fileName;

+(BOOL)addSkipBackupAttributeToItemAtPath:(NSString *)path;

+(NSMutableArray *)getRecentChangedFilesForDirectory:(NSString *)directoryPath;


+(void)createBackupZipWithPassword:(NSString *)password;

+(NSString *)newSuburbsPath;
+(NSString *)suburbsPath;

@end
