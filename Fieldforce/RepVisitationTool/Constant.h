// Default-Landscape~ipad
//  Constant.h
//  RepVisitationTool
//
//  Created by RANDEM MAC on 10/05/12.
//  Copyright (c) 2012 RandemIT. All rights reserved.
//http://appedge.randemgroup.com.au/app/SubaruAutoProfiler/RepVisitationTool220113.ipa

#ifndef RepVisitationTool_Constant_h
#define RepVisitationTool_Constant_h


#endif


//===========================================================
#define FIELDFORCE_DOWN_MESSAGE @" FIELDFORCE is currently down\nfor routine maintenance.\nPlease check back later."
//#define FIELDFORCE_DOWN_MESSAGE @"We're doing maintenance on FIELDFORCE.\n Sorry for the inconvenience, Please try again later!"

#define APP_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]

#define LOCATION_FILTER_RADIUS 100 /* in meter */
#define REMINDER_BG_COLOR  CELL_BACKGROUND_COLOR



//===========================================================
//                      CALENDAR CONSTANTS
//===========================================================

#define CALENDER_IDENTIFIER_KEY @"calender_identifier"
#define CALENDAR_VIEW_TEXT @"View"
#define CALENDAR_VIEW_TEXT_FONT_SIZE 12.0

//===========================================================
//                      BACKGROUND COLOR
//===========================================================

#define LOGIN_BG_COLOR [UIColor colorWithRed:25.0/255.0 green:91.0/255.0 blue:155.0/255.0 alpha:1]
#define TEXT_LENGTH 100

#define CELL_BACKGROUND_COLOR [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1]
#define SELECTED_CELL_BACKGROUND_COLOR [UIColor colorWithRed:215.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1]

//===========================================================
//                      DATE FORMAT 
//===========================================================

#define NEWS_DATE_FORMAT  @"EEEE, dd MMM yyyy"
#define OFFER_DATE_FORMAT @"dd/MM/yyyy"
//#define TASK_DATE_FORMAT  @"EEE dd MMM, yyyy, h:mm a"
#define TASK_DATE_FORMAT  @"dd/MM \nh:mm a"
//#define TASK_DATE_FORMAT  @"dd/MM"

//#define VISIT_DATE_FORMAT @"EEEE, dd MMM, yyyy h:mm a"
#define VISIT_DATE_FORMAT @"EEEE, dd MMM yyyy"
#define CUSTOMER_LAST_VISIT_FORMAT @"dd/MM/yyyy"
#define DATE_PICKER_FORMAT @"EEE dd MMM, yyyy, h:mm a"


//===========================================================
//             TEXT INPUT VALIDATION CHARSET
//===========================================================

#define MAX_PASSWORD_LENGTH 10
#define MIN_PASSWORD_LENGTH 6

#define PASSWORD_REGX @"(?=^.{6,10}$)((?=.*\\d)(?=.*[\\W_]+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
#define ESCAPE_CHARSET @"\\~`;"
#define NUMBER_CHARSET @"0123456789+()"

//============================================
//    LOGIN
//============================================

#define LOGIN_EMAIL_KEY    @"rep_email_tool"
#define LOGIN_PASSWORD_KEY @"rep_pass_tool"
#define LOGIN_CHECK_KEY    @"rep_isLoggedIn_tool"

#define FILTER_BUTTON_IMAGE @"segmented_button_selected_ticked.png"

//===========================================
// DASHBOARD
//================================================

extern NSString * const DashBoardWillRefresh;

#define VisitReviewDidUpdate @"_VisitReviewDidUpdate_"

//================================================


//============================================
//    APPDELEGATE
//===========================================

#define APP_DIDCHANGE_SITE_MESSAGE @"Your Dealer site has been changed.\nYou will be logged out automatically."
//#define REP_DID_DEACTIVATE_MESSAGE @"Representative has been deactivated.\nYou will be logged out automatically."
#define REP_DID_DEACTIVATE_MESSAGE @"Your account has been deactivated.\nPlease contact the administrator."

//#define CALENDAR_ACCESS_MESSAGE  @"The app does not have access to calendar.\nYou can enable access in privacy settngs."

#define CALENDAR_ACCESS_MESSAGE @"FIELDFORCE cannot access your calendar. Visit your iPad Privacy settings to enable."

//#define REMINDER_ACCESS_MESSAGE  @"The app does not have access to reminder.\nYou can enable access in privacy settngs."

#define REMINDER_ACCESS_MESSAGE @"FIELDFORCE cannot access your reminders. Visit your iPad Privacy settings to enable."

#define LAST_VERSION_KEY @"_ff_last_version"
#define LAST_VERSION_UPDATED_DATE   @"_ff_last_version_updated_date"
#define BUILD_VERSION_UPLOADED_DATE @"_ff_build_version_uploaded_date"

#define BUILD_VERSION_UPDATED @"_ff_build_version_updated_key"
//================================================
//           CUSTOMER
//================================================

#define CUSTOMER_EXIT_MESSAGE @"Are you sure you want to go back?\nAny unsaved changes will be discarded"
#define CUSTOMER_GALLERY_MAX_IMAGE_MESSAGE @"There is a maximum limit of %d images. Please delete one or more and try again."
#define CUSTOMER_CONTACT_DELETE_MESSAGE @"Are you sure to delete this contact ?"

#define CUSTOMER_CONTACT_MAXLIMIT_MESSAGE_FORMAT @"You have crossed max limit of %d contacts."
//================================================
//           TASK
//================================================

#define TASK_COMPLETE_MESSAGE @"Mark this task as Complete?" 



//extern NSString const * VisitReviewStateDidChange = @"_VisitReviewStateDidChange_";
#define CANNOT_SAVE_REMINDER_HEADER @"Cannot Save Reminder"
#define CANNOT_SAVE_REMINDER_MSG @"The start date must be before the end date"

#define REPORT_COMPLETE_MSG @"Publish this Report?"
#define VISIT_COMPLETE_MSG @"End this Visit?"
#define VISIT_TAG_SELECTION_MSG @"Please select at least one text Note to tag."
#define VISIT_DELETE_CONTAINER_MSG @"Please select at least one entry to delete."
#define TASKNAME_VALIDATE_MSG @"Please enter Task Name."
#define TASKDATE_VALIDATE_MSG @"Please select Due Date"
#define INVALID_FIELD_HEADER @"Invalid field"
#define SCHEDULE_VISIT_MSG @"You already have a visit scheduled during this period.\nDo you still want to Save this visit?"
#define VISIT_EXIT_MESSAGE @"Exiting an Active Visit will end the session.\nAre you sure you want to exit?"
#define VISIT_DELETE_MESSAGE @"Are you sure you want to delete this session?" 

#define CAMERA_VIEW_LEAVE_ALERT_MESSAGE @"The image taken will be discarded.\nDo you wish to continue?"
#define CAMERA_VIEW_LEAVE_ALERT_HEADER @"Alert"

//================================
// OFFER
//===================

#define OFFER_EMAIL_ALERT_FORMAT @"An email address does not exist for %@."
#define OFFER_EMAIL_ALERT_FORMAT_MULTIPLE @"No email addresses exist for %@."
#define NO_EMAIL_CLIENT_MESSAGE @"The email cannot be sent.\nPlease check your iPad mail account settings."

#define POOR_CONNECTION_MSG @"Network Connection has been lost."

//#define TO_EMAILID @"Fieldforce Customers<donotreply@myfieldforce.com.au>"
#define OFFER_TO_EMAILID_FORMAT @"%@ Customers<donotreply@myfieldforce.com.au>"

//=================================================
//        VISIT SESSION
//=================================================

#define POOR_CONNECTION_MSG_FOR_DATA_LOAD_FAILED @"Poor connection, data failed to load"
#define POOR_CONNECTION_MSG_FOR_DATA_UPLOAD_FAILED @"Poor connection, data failed to upload"
#define POOR_CONNECTION_MSG_REPORT_COULDNOT_BE_PUBLISHED @"Network Connection has been lost.\nTry again when your connection is restored."
#define POOR_CONNECTION_MSG_TRY_AGAIN_LATER @"Network Connection has been lost."
#define IMAGE_NOT_AVAILABLE_HEADER_MSG @"Image not available"
#define POOR_CONNECTION_MSG_IMAGE_UPLOAD_FAILED_MSG @"Network Connection has been lost.\nClick Resend when your connection is restored."
#define POOR_CONNECTION_MSG_IMAGE_DELETION_FAILED_MSG @"Network Connection has been lost."
#define POOR_CONN_MSG_UNABLE_TO_RETRIEVE_IMAGES @"No network, unable to load images."

//=================================================
#define CREATE_CUSTOMER_OFFLINE_MESSAGE @"A network connection is required to create a new customer record."
//==================================================
// VIEW ON MAP
#define POOR_CONNECTION_MSG_UNABLE_TO_LOCATE @"No network. Map unavailable."
//==================================================

//===================================================
// TAKE PHOTO VIEW
#define NO_CAMERA_FOUND_MSG @"No camera device found"
#define NO_CAMERA_HEADER @"No Camera"
//===================================================


//============================================================
// SCHEDULE VISIT
//===============================================================
#define SCHEDULE_VISIT_FAIL_MSG @"Please select atleast one OEM for the customer to schedule a visit."
#define SCHEDULE_VISIT_FAIL_TITLE @"Cannot schedule a visit"


//===================================================
//PRFILE INFO VIEW
//===================================================
#define GET_DIRECTIONS_MSG @"You are about the exit this application. Would you like to continue?"
#define VALID_EMAIL_ID_MSG @"Please enter a valid email address"
#define INVALID_EMAIL @"Invalid email address."
#define PLEASE_SELECT_LOC_MSG @"Please select Location"
#define PLZ_SEL_TERRITORY_MSG @"Please select Territory"
#define PLZ_SEL_BTYPE @"Please select Business Type."
//#define DUP_CUS_DETAILS_MSG @"Please provide a different name/email/contact number."
#define DUP_CUS_DETAILS_MSG @"Please provide a different name/email/contact number/address."
#define DUP_CUS_DETAILS_HEADER @"Duplicate customer details"
#define PLZ_ENTER_PC_MSG @"Please enter Postcode"
#define PLZ_ENTER_CITY_SUBURB_MSG @"Please enter City/Suburb"
#define ACCT_NO_BLANK_MSG @"The Account Number field cannot be blank as the customer status has been Confirmed."
#define PLZ_ENTER_BNAME_MSG @"Please enter Business name"
//===================================================

//===================================================
// NEWS DETAIL
#define CANNOT_OPEN_URL_MSG @"The URL is invalid."
#define CANNOT_OPEN_URL_HEADER @"Cannot open URL"
#define PAGE_NOT_FOUND_MESSAGE @"Sorry, the page you were looking for could not be found.\nThe page you requested may have been moved or deleted, or the URL you followed may not be correct."

//===================================================
//LOGIN VIEW
//===================================================
#define NETWORK_NOT_AVAILABLE_MSG @"Network Connection has been lost."
#define INVALID_LOGIN_DETAILS_MSG @"Invalid login details.\nPlease try again."
#define LOGIN_FAILED_HEADER @"Login failed!"
//===================================================
//FORGOT PASSWORD
//===================================================
#define EMAIL_ADDRESS_NOT_VALID INVALID_EMAIL, @"The entered email address is not valid.\nPlease enter a valid email address."
#define CONN_NOT_AVBLE_HEADER @"Connection not available"
#define MAIL_SENT_SUCCESSFULLY_TO_RESET_PWD @"Please check your email to reset your password."
#define PLZ_ENTER_VALID_EMAIL_ADDRESS_MSG @"Please provide a registered email address."
//===================================================
//FEEDBACK VIEW
//===================================================
#define FEEDBACK_SENT_MSG @"Thank you for your feedback."
#define PLZ_ENTER_DESCRIPTION_MSG @"Please enter Description."
#define PLEASE_SELECT_SECTION_MSG @"Please select Section."
#define FEEDBACK_CANNOT_BE_SENT_DUE_TO_UNAVAILABILITY_OF_NETWORK @"Feedback cannot be sent.\n Check your Network Connecton and try again."
//===================================================
//AUDIO MANAGER
//===================================================
#define AUDIO_INPUT_HARDWARE_UNAVAILABLE @"Audio input hardware not available"
#define AUDIO_WARNING @"Warning"
#define AUDIO_WARNING_MSG @"The audio recording will be stopped before exiting this session.\nContinue?"
//===================================================
//ADD PHOTO CAPTION VIEW
//===================================================
#define PHOTO_REMOVAL_DELETION_IMAGE @"Are you sure you want to delete the photo?"
//===================================================
//ACCT SETTINGS VIEW
//===================================================
#define ERROR_IN_CHANGING_PWD @"Error in changing password"
#define ERROR_IN_CHANGING_PWD_HEADER @"Error"
//===================================================

#define APP_DOWNLOAD_MSG @"A new version of FIELDFORCE is available for download."
#define NAV_DONE_BUTTON  @"close_button_blank.png"
#define NAV_BACK_BUTTON  @"back_button_blank.png"

//************************************ LOCATION-MANAGER **************************************
#define LOCATION_SERVICE_DISABLED_HEADING @"Location Service Disabled"
#define LOCATION_SERVICE_DISABLED_MSG @"Turn On Location Services to determine your location.\nTo re-enable, please go to Settings and turn on Location Services."
//********************************************************************************************

//********************** POPOVER *************************
#define POPOVER_ARROW_IMAGE @"arrow_popover.png"
#define POPOVER_BG_IMAGE @"popover-background_dark-gray.png" // @"popover-background-gray_border.png" // @"popover-background-gray.png"

//===================================================
//      RESOURCE
//===================================================

//#define RESOURCE_CORRUPTED_FILE_MESSAGE @"This file may be corrupted.\nPlease try later."
#define RESOURCE_CORRUPTED_FILE_MESSAGE  @"An error has occurred.\nPlease try again."
//#define RESOURCE_FILE_UNAVAILABLE_MESSAGE @"%@ is not availble.\nFile may be removed or replaced."
#define RESOURCE_FILE_UNAVAILABLE_MESSAGE @"%@ might have been removed or is temporarily unavailable."
//The resource you are looking for might have been removed, had its name changed, or is temporarily unavailable

#define RESOURCE_DELETE_MESSAGE @"Some resources might have been removed.\nDo you want to continue?"


