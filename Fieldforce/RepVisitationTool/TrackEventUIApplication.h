//
//  ELCUIApplication.h
//
//  Created by Brandon Trebitowski on 9/19/11.
//  Copyright 2011 ELC Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TrackEventUIApplication : UIApplication {
	NSTimer *_idleTimer;
}

@end
